Setting up your Citrix development environment:

Eclipse:
	- Eclipse 3.7 "Indigo" is the recommended version. Use other versions at your own risk.
	- subclipse plugin for eclipse version 1.7 or higher
	- maven plugin m2eclipse
	- Set up eclipse resource filters for ".vlt files"
		- this needs to be done per project, including the parent project, and individual modules/projects
	- Set up eclipse SVN filter for "*/vlt", this is done from Preferences->Team->Ignored Resources
	
	- After checking out the main/parent project, right click on the project folder, select "Import"
		- Choose Maven->Existing maven Projects, click next
		- Expand "Advanced"
		- under Profiles, add "eclipse
		- under Name Templates, choose: [group-id, artifact-id]
		- This will create new maven projects for each module
	
Subversion 1.7 or higher:
	- Modify your svn "config" file:
		- add *.vlt to your global ignores
		- Configure your end-of-line following settings from http://dev.day.com/docs/en/crx/current/how_to/how_to_use_the_vlttool.html , 
			section "CONFIGURING THE END OF LINE CHARACTER"

VLT (CRX Vault Tool):
	- Unzip the latest version of VLT for CQ5.5 (https://dev.day.com/packageshare/packages/public/day/filevault.html)
	- create and export an environment variable: export VLT_HOME=/path/to/vlt_home
	- export the PATH variable with VLT_HOME/bin appended: export PATH=$PATH:$VLT_HOME/bin
	
	- Check-out vlt on your local file system:
		- from your command line, cd to ui/src/content/jcr_root
		- vlt --credentials admin:admin co --force http://localhost:4502/crx
		