package com.sharefile.utils;

import com.citrixosd.SiteUtils;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.sling.api.resource.Resource;
import com.day.cq.wcm.api.Page;

public class SfUtilities extends SiteUtils {
	
	public static Node getGeoSegmentationNode(Page page) {
		Node segmentNode = null;
		try {
			if (page == null)
				return segmentNode;
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
					return contentNode.hasNode("geoSegments") ? contentNode.getNode("geoSegments") : segmentNode;
				}
			}
			return SfUtilities.getGeoSegmentationNode(page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return segmentNode;
		}
	}
}