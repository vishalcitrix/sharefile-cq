package com.sharefile.service.threeShips;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;


/*
 * Get dynamic content from 3 ships 
 * Function : reads URL and if /article present then this service is invoked
 * @author - vishal.gupta@citrix.com
*/

@SlingFilter(
        label = "Sharefile - 3 Ships Filter",
        description = "Reads URL and if /articles present then this service is invoked",
        metatype = false,
        generateComponent = true, // True if you want to leverage activate/deactivate or manage its OSGi life-cycle
        generateService = true, // True; required for Sling Filters
        order = 0,
        scope = SlingFilterScope.REQUEST)

public class dynamicContent implements Filter {
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletResponse response = (HttpServletResponse) res;
		HttpServletRequest request = (HttpServletRequest) req;
		String requestURL = request.getRequestURL().toString();
		
		if(requestURL.contains("/articles") && requestURL.contains("sharefile.com")) {
			response.setContentType("text/html;charset=UTF-8");
			requestURL = requestURL.replaceAll("/content/sf/[a-z]{2}_[A-Z]{2}", "").replaceAll(".html", "");
						
			StringBuffer finalURL = new StringBuffer();
			finalURL.append(requestURL);
			
	        // Your customer name to be used in the API call
	        final String NAVIGATOR_CUSTOMER = "sharefile.com";
	        final String CHARSET = "utf-8";

            response.setContentType("text/html;charset=UTF-8");

            try {
                // Get data about the orginal request
                String userAgent = request.getHeader("User-Agent");
                String referer = request.getHeader("Referer");
                
                //using this because of netscaler layer in the middle
                String ipAddress = request.getHeader("ClientHost");

                // Get the orginal request URL and query string
                String queryString = request.getQueryString();

                if (queryString != null) {
                	finalURL.append('?').append(queryString).toString();
                }

                String query = String.format("url=%s", URLEncoder.encode(finalURL.toString(), CHARSET));

                // Set the location of the Navigatoe linking server
                String navigatorURL = "https://content.demandsignals.com/api/content";
                URLConnection connection = new URL(navigatorURL + "?" + query + "&customer=" + NAVIGATOR_CUSTOMER).openConnection();

                // Set headers if available. Do not send nulls.
                connection.setRequestProperty("X-Customer-Identifier", NAVIGATOR_CUSTOMER);                
                if (userAgent != null) {
                    connection.setRequestProperty("X-Original-User-Agent", userAgent);
                }
                if (referer != null) {
                    connection.setRequestProperty("X-Original-Referer", referer);
                }
                if (ipAddress != null) {
                    connection.setRequestProperty("X-Original-IP", ipAddress);
                }

                // Get the response stream
                InputStream navigatorResponse = connection.getInputStream();

                // Create the variable to hold the returned Navigator content
                StringBuilder htmlBuffer = new StringBuilder();

                BufferedReader reader = new BufferedReader(new InputStreamReader(navigatorResponse, CHARSET));
                try {
                    for (String line; (line = reader.readLine()) != null;) {
                        htmlBuffer.append( line );
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

                // Write out the returned Navigator content
                response.getWriter().write(htmlBuffer.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
	        
			return;
		}
		
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
}