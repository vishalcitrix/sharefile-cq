package com.sharefile.service.predictivealgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.jcr.RepositoryException;

import org.apache.commons.codec.binary.Hex;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sharefile.constants.ShareFileWebHitConst;
import com.citrixosd.service.trustedHostConnection.trustedHostHttpConnection;

/**
 * Class to send data to the ShareFile API to store web events
 * 
 * Here are the parameters for the RecordWebHit API call:
 * UserID, SessionID, BrowserType, BrowserVersion, OS, IPAddress, ReferrerRaw, ReferrerDomain  
 * 
 * leon.simmonds@citrix.com
 * vishal.gupta@citrix.com
 */

@SlingServlet(paths = "/bin/citrix/shareFileWebHit", methods = "POST")
@Properties({ @Property(name = "service.description", value = "Posts to ShareFile Web Hit API") })

public class ShareFileWebHit extends SlingAllMethodsServlet {
	@Reference
	private ConfigurationAdmin configAdmin;

	private static final long serialVersionUID = 8378639096140269836L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ShareFileWebHit.class);
	private static String clientID;
	private static String clientSecret;
	private static String apiUrl;
	private static String apiUrlPath;

	@SuppressWarnings("unchecked")
	private JSONObject getParamJson(SlingHttpServletRequest request) {
		JSONObject paramJson = new JSONObject();
		
		final Set<Map.Entry<String, String[]>> reqParams = request.getParameterMap().entrySet();
		for(Map.Entry<String, String[]> entry : reqParams) {
			String key = entry.getKey();
			String[] value = entry.getValue();
			try {
				paramJson.put(key,value[0]);
			} catch (JSONException e) {
				LOGGER.error("Unable to construct parameter JSON for SF RecordWebHit: " + e.getMessage());
			}
		}
		return paramJson;
	}

	//Using Sharefile algo to compute Signature (required by Sharefile)
	private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException,UnsupportedEncodingException {
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(keyString.getBytes("UTF-8"),"HmacSHA256");
		sha256_HMAC.init(secret_key);
		String hash = Hex.encodeHexString(sha256_HMAC.doFinal(baseString.getBytes("UTF-8")));
		String signature = hash.trim().replace("-","").toLowerCase();

		return signature;
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		response.setContentType("text/html");
		
		final String inputParameters = getParamJson(request).toString().replace("\\","");
		final String base = apiUrlPath + inputParameters;
		String strSig = null;
		StringBuilder payloadString = new StringBuilder();
		StringBuilder successPayloadString = new StringBuilder();
		
		try {
			strSig = computeSignature(base,clientSecret);
		} catch(UnsupportedEncodingException e) {
			LOGGER.error("Unable to compute signature for SF RecordWebHit: " + e.getMessage());
			payloadString.append("{\"Error\": \"Unable to compute signature for SF RecordWebHit\"}");
		} catch (GeneralSecurityException e) {
			LOGGER.error("Unable to compute signature for SF RecordWebHit: " + e.getMessage());
			payloadString.append("{\"Error\": \"Unable to compute signature for SF RecordWebHit\"}");
		}

		if(strSig != null) {
			try {
				//Creating trusted connection in case CERTS not present on Server
				trustedHostHttpConnection.trustHost();
				
				URL url = new URL(apiUrl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type","application/json");
				conn.setRequestProperty("Authorization","SFClientHash " + strSig);
				conn.setRequestProperty("Accept","application/json");
				conn.setRequestProperty("X-SFAPI-ClientId",clientID);
					
				OutputStream os = conn.getOutputStream();
				os.write(inputParameters.getBytes());
				os.flush();
				
				if((conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) && (conn.getResponseCode() != HttpURLConnection.HTTP_OK)) {
					LOGGER.error("Failed : HTTP error code : " + conn.getResponseCode());
					payloadString.append("{\"Error\": \"Failed to submit data\"}");
				}else {					
					LOGGER.info("Data Submitted to Service");
					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
					successPayloadString.append("\nResponse:\n");
					for(String line = null; (line = br.readLine()) != null;) {
						successPayloadString.append(line).append("\n");
					}
				}
				conn.disconnect();
			} catch (MalformedURLException e) {
				LOGGER.error("Exception: " + e.getMessage());
				payloadString.append("{\"Error\": \"" + e.getMessage() + "\"}");
			} catch (IOException e) {
				LOGGER.error("Exception: " + e.getMessage());
				payloadString.append("{\"Error\": \"" + e.getMessage() + "\"}");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				LOGGER.error("Exception: " + e.getMessage());
				payloadString.append("{\"Error\": \"" + e.getMessage() + "\"}");
			}
			
			try {
				response.getWriter().print(successPayloadString == null ? payloadString : successPayloadString);
			} catch (IOException e) {
				e.printStackTrace();
				LOGGER.error("Error displaying response");
			}
		}
	}
	
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		response.getWriter().print("This Service only supports POST");
	}
	
    /**
     * The activate method for OSGi.
     * @throws RepositoryException 
     * @throws MalformedURLException 
     */
    @Activate
    protected void activate(ComponentContext context) throws RepositoryException, MalformedURLException {
        if(LOGGER.isDebugEnabled())
        	LOGGER.info("Activating "+this.getClass());
        
		Configuration sharefileProperties = null;
		try {
			sharefileProperties = configAdmin.getConfiguration(ShareFileWebHitConst.SF_WHIT_OSGI_CONF);
		} catch (IOException e) {
			LOGGER.error("Could not read config file: " + e.getMessage());
		}
		
		clientID = sharefileProperties.getProperties().get(ShareFileWebHitConst.SF_WHIT_CLIENT_ID).toString();
		clientSecret = sharefileProperties.getProperties().get(ShareFileWebHitConst.SF_WHIT_CLIENT_SECRET).toString();
		apiUrl = sharefileProperties.getProperties().get(ShareFileWebHitConst.SF_WHIT_CLIENT_URL).toString();
		
		try {
			apiUrlPath = new URL(apiUrl).getPath();
		} catch (MalformedURLException e) {
			LOGGER.error("API URL is malformed (" + apiUrl + "): " + e.getMessage());
			throw e;
		}
    }

    /**
     * The modified method for OSGi.
     * @throws RepositoryException 
     * @throws MalformedURLException 
     */
    @Modified
    protected void modified(ComponentContext context) throws RepositoryException, MalformedURLException {
        if(LOGGER.isDebugEnabled())
        	LOGGER.info("Modified "+this.getClass());
        activate(context);
    }
}