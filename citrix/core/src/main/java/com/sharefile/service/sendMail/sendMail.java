package com.sharefile.service.sendMail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.ServerException;
import java.util.Map;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

//sendGrid API
import com.sendgrid.SendGrid;
import com.sharefile.constants.SendGridConst;

@SlingServlet(paths = "/bin/citrix/sharefileSendMail", methods = "POST", metatype=true)
/*
 * sendMail class 
 * Function : Receives the user inputs from the form and implements the
 * email functionality. 
 * Also has the ability to handle file upload
 * @author - Krishna.Selvaraj@citrix.com
 */

public class sendMail extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 2598426539166789515L;
	public static File fileToSend;
	public static String extensionFile;
	public static int maxAttempt = 0;

	private static void returnJsonResponse(SlingHttpServletResponse response, String status, String message) throws JSONException {
		response.setContentType("text/html");
		try {
			JSONObject json = new JSONObject();
			json.put(status, message);
			response.getWriter().write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
		
		//send object
		SendGrid sendgrid = new SendGrid(SendGridConst.SF_SENDGRID_USERNAME, SendGridConst.SF_SENDGRID_PASSWORD);
		SendGrid.Email email = new SendGrid.Email();

		//email details from request
		final String fromEmail = request.getParameter("fromEmail");
		final String toEmail = request.getParameter("toEmail");
		final String emailSubject = request.getParameter("emailSubject");
		final String emailMessage = request.getParameter("emailMessage");

		//email details added to sendgrid object
		email.addTo(SendGridConst.toEmailMap.get(toEmail));
		email.setFrom(SendGridConst.fromEmailMap.get(fromEmail));
		email.setSubject(emailSubject);
		email.setHtml(emailMessage);

		try {
			final boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if(isMultipart) {
				final Map<String, RequestParameter[]> params = request.getRequestParameterMap();
				for (final Map.Entry<String, RequestParameter[]> pairs : params.entrySet()) {
					final RequestParameter[] pArr = pairs.getValue();
					final RequestParameter param = pArr[0];

					//to check content type here param.getContentType()
					if(!param.isFormField()) {
						final InputStream stream = param.getInputStream();
						email.addAttachment(param.getFileName(), stream);
					}   
				}
			}

			//Sending Email
			SendGrid.Response emailResponse = sendgrid.send(email);

			if(emailResponse.getMessage().toString().contains("success")) {
				returnJsonResponse(response,"status","success");
			}
			else{
				returnJsonResponse(response,"status","error");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			try {
				returnJsonResponse(response,"status","error");
			}catch(JSONException e1) {
				//TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
		response.setContentType("text/html");
		response.getWriter().print("This Service only supports POST");
	}
}