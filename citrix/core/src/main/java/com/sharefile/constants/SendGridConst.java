package com.sharefile.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SendGridConst {
	public static final String SF_SENDGRID_USERNAME = "citrixsaas";
	public static final String SF_SENDGRID_PASSWORD = "Mktmail357!";

	public static final Map<String, String> fromEmailMap;
	static {
		Map<String, String> tmp = new HashMap<String, String>();
		tmp.put("sf-notification", "support@sharefile.com");
		fromEmailMap = Collections.unmodifiableMap(tmp);
	}

	public static final Map<String, String> toEmailMap;
	static {
		Map<String, String> tmp = new HashMap<String, String>();
		tmp.put("sf-contactSupport", "sharefileemailtest@gmail.com");
		tmp.put("sf-rightsignature", "rightsignature@citrix.com");
		tmp.put("sf-partners", "SF-partners@citrix.com");
		tmp.put("sf-support", "support@sharefile.com");
		tmp.put("sf-financialdemo", "SFFinancialdemo@citrix.com");
		toEmailMap = Collections.unmodifiableMap(tmp);
	}
}