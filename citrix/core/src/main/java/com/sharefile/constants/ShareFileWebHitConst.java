package com.sharefile.constants;

public class ShareFileWebHitConst {
	public static final String SF_WHIT_CLIENT_ID = "sharefilewebhit.client.id";
	public static final String SF_WHIT_CLIENT_SECRET = "sharefilewebhit.clientSecret";
	public static final String SF_WHIT_CLIENT_URL = "sharefilewebhit.url";
	public static final String SF_WHIT_OSGI_CONF = "com.citrixosd.service.sharefile.ShareFileWebHit"; 
}