package com.siteworx.brightcove;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The sling service for handling Brightcove Configuration in CQ. Handles the configuration of 
 * API tokens and welcome screen display preferences.
 *
 * @author rboll@siteworx.com
 *
 */
@Component(immediate=true, metatype=false)
@Service(value={BrightcoveConfigurationService.class, EventHandler.class})
@Properties({
    @Property(name="event.topics", value={SlingConstants.TOPIC_RESOURCE_CHANGED, SlingConstants.TOPIC_RESOURCE_ADDED, SlingConstants.TOPIC_RESOURCE_REMOVED})
})
public class BrightcoveConfigurationService implements EventHandler {

    private static final Logger log = LoggerFactory.getLogger(BrightcoveConfigurationService.class);
    
    // Configuration Node Locations
    public static final String BC_CONFIG_PATH = "/etc/cloudservices/brightcove/jcr:content";
    public static final String BC_CONSOLE_CONFIG_PATH = "/apps/brightcove/console/brightcove";
    
    // Attribute Names
    public static final String CONSOLE_CONFIG_ATTR = "showConsole";
    private static final String READ_API_KEY_ATTR = "readToken";
    private static final String WRITE_API_KEY_ATTR = "writeToken";
    
    // Token Values
    private String readToken;
    private String writeToken;
    
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    /**
     * Updates the in memory cache of API tokens for Brightcove.
     * 
     * @throws Exception If the resourceResolver fails to login to the repository.
     */
    public synchronized void updateApiTokenCache() throws Exception {
        if(log.isDebugEnabled())
        	log.debug("Updating Brightcove Keys.");
        
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
            
            final ValueMap configResource = resourceResolver.resolve(BC_CONFIG_PATH).adaptTo(ValueMap.class);//.adaptTo(Node.class);
            if(configResource == null){
                throw new Exception("The Brightcove configuration node "+BC_CONFIG_PATH+" is missing.");
            }

            final String readApiToken = configResource.get(READ_API_KEY_ATTR, null);
            final String writeApiToken = configResource.get(WRITE_API_KEY_ATTR, null);
            
            if(readApiToken == null || writeApiToken == null){
                throw new Exception("The Brightcove configuration node "+BC_CONFIG_PATH+" is missing one or more properties.");
            }
            
            this.setReadToken(readApiToken);
            this.setWriteToken(writeApiToken);
            
            if(log.isDebugEnabled())
                log.debug("Brightcove Config Updated.\n");
            
        } catch (LoginException e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " unable to log into JCR.");
        } finally {
            if (resourceResolver != null) resourceResolver.close();
        }
    }
    
    /**
     * Updates the console preferences by toggling the 'cq:Console' mixin on the appropriate configuration node. This determines
     * whether Brightcove is displayed on the login screen.
     * 
     * @throws Exception If the resourceResolver fails to login to the repository.
     */
    public void updateConsolePreferences() throws Exception {
    	if(log.isDebugEnabled())
    		log.debug("Updating Brightcove Console Preferences.");
    	
    	ResourceResolver resourceResolver = null;
    	try {
            resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
            final Session session = resourceResolver.adaptTo(Session.class);
            final Node consoleConfigNode = resourceResolver.resolve(BC_CONSOLE_CONFIG_PATH).adaptTo(Node.class);
            final Node configNode = resourceResolver.resolve(BC_CONFIG_PATH).adaptTo(Node.class);
            
            if(consoleConfigNode == null) {
                throw new Exception("The Brightcove configuration node "+BC_CONSOLE_CONFIG_PATH+" is missing.");
            }
            else if(configNode == null) {
                throw new Exception("The Brightcove configuration node "+BC_CONFIG_PATH+" is missing.");
            }
            
            
            if (configNode.hasProperty(CONSOLE_CONFIG_ATTR)) {
	            final boolean showConsole = configNode.getProperty(CONSOLE_CONFIG_ATTR).getBoolean();
	            
	            if (showConsole) {
	            	if (consoleConfigNode.canAddMixin("cq:Console")) {
	            		consoleConfigNode.addMixin("cq:Console");
	            	} else 
	            		throw new Exception("Unable to add 'cq:Console' mixin to node " + consoleConfigNode.getPath() + ".");
	            }
	            
	        } else if (consoleConfigNode.isNodeType("cq:Console")) {	        	
            	consoleConfigNode.removeMixin("cq:Console");
	        }
            
            if (session.hasPendingChanges())
            	session.save();
            
            if(log.isDebugEnabled())
                log.debug("Brightcove Console Configuration Updated.\n");
            
        } catch (LoginException e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " unable to log into JCR.");
        } finally {
            if (resourceResolver != null) resourceResolver.close();
        }
    	
    }
    
    /**
     * The activate method for OSGi. Updates all in memory caches, and console preferences.
     */
    @Activate
    protected void activate(ComponentContext context) throws RepositoryException {
        if(log.isDebugEnabled())log.debug("Activating "+this.getClass());
        try {
            updateApiTokenCache();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
        	updateConsolePreferences();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
    
    /**
     * The modified method for OSGi. Updates all in memory caches, and console preferences.
     */
    @Modified
    protected void modified(ComponentContext context) throws RepositoryException {
        if(log.isDebugEnabled())log.debug("Modified "+this.getClass());
        activate(context);
    }
    
    /**
     * The handleEvent method for OSGi. Updates the API keys.
     */
    @Override
    public void handleEvent(Event event) {
        final String path = (String) event.getProperty("path");
        if (path.startsWith(BC_CONFIG_PATH)) {
            if(log.isDebugEnabled())log.debug("Handling event in path "+BC_CONFIG_PATH);
            try {
                updateApiTokenCache();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
            	updateConsolePreferences();
            } catch (Exception e) {
            	e.printStackTrace();
            }
        }
    }

	public String getReadToken() {
		return readToken;
	}

	protected void setReadToken(String readKey) {
		this.readToken = readKey;
	}

	public String getWriteToken() {
		return writeToken;
	}

	protected void setWriteToken(String writeKey) {
		this.writeToken = writeKey;
	}
    
}
