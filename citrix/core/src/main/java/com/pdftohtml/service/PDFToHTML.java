package com.pdftohtml.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.api.SlingRepository;
import javax.jcr.Session;
import org.apache.sling.jcr.resource.JcrResourceResolverFactory;
import javax.jcr.Node;
import javax.activation.MimetypesFileTypeMap;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.sling.event.EventUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.dam.api.DamEvent;
import com.day.cq.dam.api.DamEvent.Type;
import com.day.cq.dam.api.Asset;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFText2HTML;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

/**
 * This is pdf to html conversion listener that listens for pdf upload events and
 * generates html and images.
 */
@Component(immediate = true)
@Service
@Property(name = EventConstants.EVENT_TOPIC, value = DamEvent.EVENT_TOPIC)
public class PDFToHTML implements EventHandler {
    
    /**
     * default logger
     */
    private static final Logger log = LoggerFactory.getLogger(PDFToHTML.class);
    
    
    @Reference 
    private SlingRepository repository;
    @Reference(policy = ReferencePolicy.STATIC) 
    private JcrResourceResolverFactory resolverFactory;
    
    private static Session session = null;    
    
    private String mimeType = Constants.MIMETYPE;
    PDFParser parser;
    PDFTextStripper pdfStripper = null;
    PDFText2HTML pdfHTMLStripper = null;
    PDDocument pdDoc = null;
    COSDocument cosDoc = null;
    
    /**
     * @see EventHandler#handleEvent(Event)
     */
    public void handleEvent(Event event) {
    
        if (EventUtil.isLocal(event)) {
            final DamEvent damEvent = DamEvent.fromEvent(event);
            if (damEvent != null) {
                try{
                    session = repository.loginAdministrative(null); 
	                    if(session != null){
		                    ResourceResolver resourceResolver = resolverFactory.getResourceResolver(session);
		                    if(Type.ASSET_CREATED == damEvent.getType()){
		                        String assetPath = damEvent.getAssetPath();
		                        Asset pdfAsset=null;
		                        String assetMimeType="";
		                        
								if (resourceResolver != null) {
									Resource assetResource = resourceResolver.getResource(assetPath);
									pdfAsset = assetResource.adaptTo(Asset.class);
									if(pdfAsset != null){
										String assetName = pdfAsset.getName();
										assetMimeType = pdfAsset.getMimeType();
									}
									
								}
								
		                        
		                        /*
		                        * Code will execute if uploaded asset mimetype is pdf.
		                        */
		                        if(mimeType.equals(assetMimeType)){
		                            Node rootNode = session.getRootNode();
		                            String nodeRelPath = assetPath.substring(1);
		                            Node pdfMetaDataNode = rootNode.getNode(nodeRelPath+Constants.METADATA_NODE);
		                            if(null != pdfMetaDataNode){
		                                
		                                InputStream pdfStream = pdfAsset.getOriginal().getStream();
		                                String parsedText = null;;
		                                String parsedHTML = "";
		                                                                
		                                /*
		                                * Parsing uploaded PDF to extract text.
		                                */
		                                parser = new PDFParser(pdfStream);
		                                parser.parse();
		                                cosDoc = parser.getDocument();
		                                //pdfStripper = new PDFTextStripper();
		                                pdfHTMLStripper = new PDFText2HTML("UTF-8");
		                                pdfHTMLStripper.setSortByPosition(true);
		                                
		                                pdDoc = new PDDocument(cosDoc);
		                                
		                                String tempParsedHTML = pdfHTMLStripper.getText(pdDoc);
		                                parsedHTML = "<div class='pdfhtml'>" + tempParsedHTML.substring(tempParsedHTML.indexOf("<body>")+6,tempParsedHTML.indexOf("</body>")) + "</div>";
		                                                                
		                                /*
		                                * Set parsed text in JCR property named as html.
		                                */
		                                pdfMetaDataNode.setProperty(Constants.HTML_PROPERTY,parsedHTML);
		                                pdfMetaDataNode.save();
		                                pdfMetaDataNode.refresh(true);
		                            }    
		                        }
		                        else{
		                        	log.debug("Ignore processing-PDF to HTML,as it is not valid PDF file, Asset Mime-type received is :"+assetMimeType);
		                        }
		                    }
		                    if(session != null){
		                    	session.logout(); //cleaning session after processiong
			                    session = null; 
		                    }
		                     
	                    }
	                    else{
	                    	 log.debug("PDF to HTML conversion fails as unable to get repository session, Session value is :"+session);
	                    }
                }
                catch(Exception e){
                    log.error("Exception caught PDFToHTML::::", e);
                    e.printStackTrace();
                }
                finally{
                    if(session!=null)
                    session.logout();
                    
                    try {
                        if (cosDoc != null)
                            cosDoc.close();
                        if (pdDoc != null)
                            pdDoc.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    protected void bindRepository(SlingRepository repository){
        this.repository = repository;
            
    }   
    protected void unbindRepository(SlingRepository repository){       
        if(this.repository == repository)
        {
            repository = null;
        }  
        if (session!=null && session.isLive())
                    session.logout();       
        session = null;
    }
    
    protected void bindResolverFactory(JcrResourceResolverFactory resolverFactory){
        this.resolverFactory = resolverFactory;
            
    }   
    protected void unbindResolverFactory(JcrResourceResolverFactory resolverFactory){       
        if(this.resolverFactory == resolverFactory)
        {
            resolverFactory = null;
        }  
        
    }
}
