package com.pdftohtml.service;

public class Constants 
{
    
    public static String MIMETYPE = "application/pdf";
    public static String METADATA_NODE = "/jcr:content/metadata";
    public static String HTML_PROPERTY = "html";
    
}
