package com.pdftohtml.service;

import java.io.PrintWriter;
import java.io.InputStream;
import java.io.IOException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.api.SlingRepository;
import javax.servlet.ServletException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import javax.jcr.Session;
import org.apache.sling.jcr.resource.JcrResourceResolverFactory;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import com.day.cq.dam.api.Asset;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFText2HTML;



/**
* This is a service which will take dam path from request and will extract
* text with html tags from pdf and set into html property of metanode of pdf.
*/
 
@SlingServlet(paths="/bin/pdf-to-html-script",methods = "GET", metatype=true)

@Properties({
    @Property(name="service.pid", value="com.pdftohtml.service.PDFToHTMLScript",propertyPrivate=false),
    @Property(name="service.description",value="G2M PDF To HTML Service", propertyPrivate=false),
    @Property(name="service.vendor",value="Citrix Online LLC", propertyPrivate=false)
})
public class PDFToHTMLScript extends SlingAllMethodsServlet {
    
    /**
     * default logger
     */
    private static final Logger log = LoggerFactory.getLogger(PDFToHTMLScript.class);
    
    
    @Reference 
    private SlingRepository repository;
    @Reference(policy = ReferencePolicy.STATIC) 
    private JcrResourceResolverFactory resolverFactory;
    
    
    private BundleContext bundleContext;
    private static Session session = null; 
    private String mimeType = "pdf";
    PDFParser parser;
    PDFText2HTML pdfHTMLStripper = null;
    PDDocument pdDoc = null;
    COSDocument cosDoc = null;
    private int count = 0;
    
    /**
     * This method is called when the bundle for this service is deployed in the CRX.
     * @param context
     * @throws Exception
     */
    protected void activate(ComponentContext ctx) throws Exception {
        bundleContext = ctx.getBundleContext();
    }
    

    /**
     * The doGet method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to get.
     * 
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    @Override
    public void doGet(SlingHttpServletRequest request,SlingHttpServletResponse response) throws ServletException,IOException {
            
            try{
                PrintWriter out = response.getWriter();
                session = repository.loginAdministrative(null);
                ResourceResolver resourceResolver = resolverFactory.getResourceResolver(session);
                
                /*
                * HTML to generate pdf to html conversion form
                */
                out.println("<html><body style='font-family: verdana,sans-serif,arial;font-size:14px;'>");
                out.println("<h3 style='text-align:center;text-decoration:underline;font-size:20px;'>PDF To HTML Utility</h3><br>");
                out.println("<p style='margin-top:-20px;text-align:center;'>");
                out.println("<i>This PDF To HTML Utility will only extract ext with html tags from existing pdfs uploaded into DAM.</i>");
                out.println("</p>");
                out.println("<hr style='margin-top:-8px;'>");
    
                out.println("<form name='pdftohtmlscript' method='get'>");
                out.println("<br><b>&nbsp;Enter the dam path for the site:&nbsp;&nbsp;</b>");
                out.println("<input type='text' name='pdfdamrootpath' value='' size='60' style='height:23px;'/>");
                out.println("<input type='submit' value='Convert PDF To HTML' style='font-family:verdana,sans-serif;font-weight:bold;color:#FFFFFF;background-color:#0077BB;border:0;border-radius:5px;height:23px;'/>");
                out.println("<br><label style='margin-left:264px;font-size:12px;color:#808080;'>&nbsp;The path should start with '/content/dam'</label>");
                out.println("<br><br><hr>");
                out.println("</form>");
                out.println("</body></html>");
                
                String pdfDAMPath = "";
                if(null != request.getParameter("pdfdamrootpath")){
                    pdfDAMPath = request.getParameter("pdfdamrootpath");
                    out.println("<b>Extracting text from pdf existing under path :</b> " + pdfDAMPath + "<br><br>");
                    out.println("<div>");
                    out.println("<table width='100%' border='0' cellpadding='0' cellspacing='0' style='border-width: 1px 1px 1px 1px;border-spacing: 0px;border-style: solid solid solid solid;border-color: black black black black;border-collapse: collapse;'>");
                    out.println("<tr>");
                    out.println("<th style='font-weight:bold;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;background-color: #EEEEEE;color:#802A2A;font-size: 12px;word-break:break-word;'>S. No</th>");
                    out.println("<th style='font-weight:bold;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;background-color: #EEEEEE;color:#802A2A;font-size: 12px;word-break:break-word;'>Asset URL</th>");
                    out.println("<th style='font-weight:bold;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;background-color: #EEEEEE;color:#802A2A;font-size: 12px;word-break:break-word;'>Asset Name</th>");
                    out.println("<th style='font-weight:bold;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;background-color: #EEEEEE;color:#802A2A;font-size: 12px;word-break:break-word;'>Status</th>");
                    
                    /**
                    * The setPDFTextToHTML method of the servlet for extracting text from pdf. <br>
                    *
                    * This method is called when a pdfdamrootpath request parameter is not null.
                    * 
                    * @param pdfDAMPath path under which pdfs have to be scanned to fetch text and set into html property
                    * @param resourceResolver the ResourceResolver object to resolve pdf node
                    * @param PrintWriter out object to print status on page.
                    */
                    setPDFTextToHTML(pdfDAMPath,resourceResolver,out);
                    
                    out.println("</tr></table></div><br>");
                    count = 0;
                }
                
                
                session.logout();
                session = null;
            }   
            catch(Exception ex){
                log.error("******* Exception Occured in PDF TO HTML Script",ex);
                System.out.println(ex);
            }
            finally{
                if(session!=null)session.logout();
                session = null;
            }
    }
    
    public void setPDFTextToHTML(String pdfDAMPath,ResourceResolver resourceResolver,PrintWriter out){
        try{
            if(!"".equals(pdfDAMPath.trim())){
                Resource pdfResources = resourceResolver.resolve(pdfDAMPath);
                Node pdfRootNode = pdfResources.adaptTo(Node.class); 
                NodeIterator childrenNodes = pdfRootNode.getNodes();
                while(childrenNodes.hasNext()){
                    Node childNode = childrenNodes.nextNode();
                    String nodeName = childNode.getName();
                    /*
                    * Check for jcr:content node. If it is then skip it.
                    */
                    if(!"jcr:content".equalsIgnoreCase(nodeName)){
                        /*
                        * Execute the logic if scanned document is pdf document 
                        * otherwise skip it.
                        */
                    	String assetMimeType = childNode.getPath().substring(childNode.getPath().lastIndexOf(".")+1);
                    	if(mimeType.equals(assetMimeType)){
                            Resource assetResource = resourceResolver.getResource(childNode.getPath());
                            Asset pdfAsset = assetResource.adaptTo(Asset.class);
                            String assetName = pdfAsset.getName();
                            String assetPath = pdfAsset.getPath();
                            Node rootNode = session.getRootNode();
                            String nodeRelPath = assetPath.substring(1);
                            Node pdfMetaDataNode = rootNode.getNode(nodeRelPath+Constants.METADATA_NODE);
                            if(null != pdfMetaDataNode){
                            
                                InputStream pdfStream = pdfAsset.getOriginal().getStream();
                                String parsedHTML = "";
                                
                                /*
                                * Parsing existing PDF to extract text with html tags.
                                */
                                parser = new PDFParser(pdfStream);
                                parser.parse();
                                cosDoc = parser.getDocument();
                                pdfHTMLStripper = new PDFText2HTML("UTF-8");
                                pdfHTMLStripper.setSortByPosition(true);
                                pdDoc = new PDDocument(cosDoc);
                                
                                String tempParsedHTML = pdfHTMLStripper.getText(pdDoc);
                                parsedHTML = "<div class='pdfhtml'>" + tempParsedHTML.substring(tempParsedHTML.indexOf("<body>")+6,tempParsedHTML.indexOf("</body>")) + "</div>";
                                out.println("<tr>");
                                out.println("<td style='font-size: 11px;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;white-space:normal;word-break:break-word;'>" + ++count + "</td>");
                                out.println("<td style='font-size: 11px;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;white-space:normal;word-break:break-word;'>" + assetPath + "</td>");
                                out.println("<td style='font-size: 11px;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;white-space:normal;word-break:break-word;'>" + assetName + "</td>");
                                
                                /*
                                * Set parsed text in JCR property named as html.
                                */
                                pdfMetaDataNode.setProperty(Constants.HTML_PROPERTY,parsedHTML);
                                pdfMetaDataNode.save();
                                pdfMetaDataNode.refresh(true);
                                
                                out.println("<td style='font-size: 11px;border-width: 1px 1px 1px 1px;padding: 5px 5px 5px 5px;border-style: solid solid solid solid;white-space:normal;word-break:break-word;'> Done </td>");
                                out.println("</tr>");
                            }   
                        }
                        /*
                        * reverse loop to iterate all child folders having pdfs to fetch
                        * and set pdf text into html property.
                        */
                        if(childNode.hasNodes()){
                            setPDFTextToHTML(childNode.getPath(),resourceResolver,out);
                        }
                    }
                }
            }
        }
        catch(Exception ex){
            log.error("******* Exception Occured in PDF TO HTML Script",ex);
            System.out.println(ex);
        }
    }
    
    protected void bindRepository(SlingRepository repository){
        this.repository = repository;
            
    }   
    protected void unbindRepository(SlingRepository repository){       
        if(this.repository == repository)
        {
            repository = null;
        }  
        if (session!=null && session.isLive())
                    session.logout();       
        session = null;
    }
    
    protected void bindResolverFactory(JcrResourceResolverFactory resolverFactory){
        this.resolverFactory = resolverFactory;
            
    }   
    protected void unbindResolverFactory(JcrResourceResolverFactory resolverFactory){       
        if(this.resolverFactory == resolverFactory)
        {
            resolverFactory = null;
        }  
        
    }
    
}