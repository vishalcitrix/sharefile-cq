package com.citrixosdRedesign.constants;

import com.day.cq.wcm.api.components.DropTarget;

public class ResourceConstants {
    public static final String RESOURCE = "resource";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LANGUAGE = "resourceLanguage";
    public static final String RESOURCE_TYPE = "resourceType";
    public static final String RESOURCE_CATEGORIES = "resourceCategories";
    public static final String CATEGORIES = "categories";
    public static final String DEFAULT_PRODUCT = "defaultproduct";
    public static final String PRODUCTS = "products";
    public static final String FEATURED = "featured";
    public static final String INDUSTRIES = "industries";
    public static final String TOPICS = "topics";
    public static final String RENDITION = "rendition";
    public static final String FILE_REFERENCE = "fileReference";
    public static final String DEFAULT_IMAGE = "/css/static/images/resources/default.png";
    public static final String SMALL_IMAGE = "/jcr:content/renditions/cq5dam.web.370.250";
    public static final String DATE = "date";
    public static final String END_DATE = "endDate";
    public static final String GATED = "gated";
    
    public class StandardResources {
    	public static final String AUTHOR_NAME = "authorName";
    	public static final String AUTHOR_TITLE = "authorTitle";
    	public static final String DOWNLOAD_TYPE = "downloadExternal";
    	public static final String EXTERNAL_LINK = "externalLinkPath";
    	public static final String EXTERNAL_LINK_TITLE = "externalLinkTitle";
    }

    public class Document {
    	public static final String DAM_ASSET_PATH = "damAssetPath";
        public static final String DEFAULT_IMAGE = "/css/static/images/resources/document.png";
        public static final String PDF_MIMETYPE = "application/pdf";
        public static final String PPT_MIMETYPE = "application/vnd.ms-powerpoint";
    }
    
    public class Video {
        public static final String DEFAULT_IMAGE = "/css/static/images/resources/video.png";
        public static final String DD_CLASS_NAME = DropTarget.CSS_CLASS_PREFIX + "brightcove";
        public static final String PLAYER_ID = "playerId";
        public static final String PLAYER_KEY = "playerKey";
        public static final String VIDEO_PLAYER = "videoPlayer";
        public static final String VIDEO_TITLE = "videoTitle";
        public static final String VIDEO_LENGTH = "videoLength";
        public static final String VIDEO_THUMBNAIL_URL = "videoThumbnailUrl";
    }
    
    public class CustomerStory {
        public static final String DEFAULT_IMAGE = "/css/static/images/resources/customerStory.png";
    }

    public class PressAward {
        public static final String DEFAULT_IMAGE = "/css/static/images/resources/pressAward.png";
        
    }

    public class CaseStudy {
    	public static final String DEFAULT_IMAGE = "/css/static/images/resources/caseStudy.png";
    }

    public class Webinar {
        public static final String DEFAULT_IMAGE = "/css/static/images/resources/webinar.png";
        public static final String WEBINAR_DESCRIPTION = "webinarDescription";
        public static final String SPEAKERS = "speakers";
        public static final String REGISTER_PATH = "registerPath";
        public static final String ON_DEMAND = "onDemand";
        public static final String TIME_ZONE = "timeZone";
    }

    public class Speaker {
        public static final String NAME = "name";
        public static final String TITLE = "title";
        public static final String COMPANY = "company";
        public static final String CITY = "city";
        public static final String STATE = "state";
        public static final String THUMBNAIL = "thumbnail";
    }
    
    public class Events {
    	public static final String EVENT_CITY = "eventCities";
    	public static final String EVENT_COUNTRY = "eventCountry";
    	public static final String EVENT_START_DATE = "eventStartDate";
    	public static final String EVENT_END_DATE = "eventEndDate";
    	public static final String EVENT_TIME_ZONE = "eventTimeZone";
    }
}