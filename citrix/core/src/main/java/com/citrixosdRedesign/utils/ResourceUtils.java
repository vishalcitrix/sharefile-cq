package com.citrixosdRedesign.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.brightcove.proserve.mediaapi.wrapper.ReadApi;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.Video;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.VideoFieldEnum;
import com.brightcove.proserve.mediaapi.wrapper.exceptions.BrightcoveException;
import com.citrixosd.utils.ContextRootTransformUtil;
import com.citrixosd.utils.Utilities;
import com.citrixosdRedesign.constants.ResourceConstants;
import com.citrixosdRedesign.models.resource.Pagination;
import com.citrixosdRedesign.models.resource.Speaker;
import com.citrixosdRedesign.models.resource.StandardDetail;
import com.citrixosdRedesign.models.resource.Webinar;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.personalization.ClientContextUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Design;

public class ResourceUtils {
    /**
     * Converts milliseconds to hours, minutes, and seconds ex:(1:20:32)
     */
    public static String getHumanVideoTime(long milliseconds) {
        final StringBuilder builder = new StringBuilder();
        if(milliseconds < 60000L) {
            builder.append("0").append(":"); //Append the 0 minutes for ui
        }
        if(milliseconds >= 3600000) {
            int hours = (int)(milliseconds / 3600000L);
            milliseconds = milliseconds - (hours * 3600000);
            builder.append(hours).append(":");
        }
        if(milliseconds >= 60000) {
            int minutes = (int)(milliseconds / 60000L);
            milliseconds = milliseconds - (minutes * 60000);
            builder.append(minutes).append(":");
        }
        int seconds = (int)(milliseconds / 1000L);
        if(seconds  < 10) {
            builder.append("0");
        }
        milliseconds = milliseconds - (seconds * 1000);
        builder.append(seconds);

        return builder.toString();
    }

    public static List<Tag> getTags(String[] array, TagManager tagManager) {
        if(array != null) {
            final List<Tag> tags = new ArrayList<Tag>();
            for(int i = 0; i < array.length; i++) {
                final Tag tag = tagManager.resolve(array[i]);
                if(tag != null) {
                    tags.add(tag);
                }
            }
            return tags;
        }else {
            return null;
        }
    }

    /**
     * Recursively search for the landing page
     */
    public static Page findLandingPage(Resource resource, String path) throws RepositoryException {
        final Node node = resource.getResourceResolver().getResource(path).adaptTo(Node.class);
        if(node == null) {
            return null;
        }else if(node.hasProperty(JcrConstants.JCR_PRIMARYTYPE)) {
            if(node.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString().equals(NameConstants.NT_PAGE)) {
                return resource.getResourceResolver().getResource(node.getPath()).adaptTo(Page.class);
            }else {
                return findLandingPage(resource, node.getPath());
            }
        }else {
            return null;
        }
    }

    /**
     * Checks if the tag already exist in the original tag and if it does then
     * return the original tag with the new query tag instead for filtering
     */
    public static String[] filterTag(String[] originalTags, String queryTag) {
        if(queryTag != null && queryTag.trim().length() > 0 && !(queryTag.equals("all"))) {
        	originalTags = new String[1];
            originalTags[0] = queryTag;
        }
        return originalTags;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Pagination getResourceList(Resource resource, Page currentPage, HttpServletRequest request, String searchText, Design currentDesign) throws RepositoryException {
        final ValueMap properties = resource.adaptTo(ValueMap.class);
        final ResourceResolver resourceResolver = resource.getResourceResolver();
        //final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        final Session session = resourceResolver.adaptTo(Session.class);
        final QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);

        //Initialize the return type
        final Pagination resourceList = new Pagination<Resource>();
        final List<com.citrixosdRedesign.models.resource.Resource> resources = new ArrayList<com.citrixosdRedesign.models.resource.Resource>();
        
        final String uniqueId = ClientContextUtil.getId(resource.getPath());
        
        //Get pagination
        final String paginationIndexStr = (String)request.getAttribute(uniqueId + "-page-index");

        //Get the query
        final String queryProductsStr = (String)request.getAttribute(uniqueId + "-" + ResourceConstants.PRODUCTS);
        final String queryCategoriesStr = (String)request.getAttribute(uniqueId + "-" + ResourceConstants.CATEGORIES);
        final String queryTopicsStr = (String)request.getAttribute(uniqueId + "-" + ResourceConstants.TOPICS);
        final String queryIndustriesStr = (String)request.getAttribute(uniqueId + "-" + ResourceConstants.INDUSTRIES);

        //Calculate the pagination
        final int paginationLimit = properties.get("limit", 6);
        final int paginationIndex = (paginationIndexStr != null ? paginationIndexStr.matches("-?\\d+") ? Integer.parseInt(paginationIndexStr) : 0 : 0);
        final int currentPaginationIndex = paginationIndex * paginationLimit;

        //setting absolute path so that search for nodes is not in the entire repository
        final String path = currentPage.getAbsoluteParent(2).getPath();

        //Parse the resource types
        final String[] types = properties.get("types", String[].class);
       
        //Get Language
        final String[] language = properties.get("resourceLanguage", String[].class);
        
        System.out.println("Setting Query Tags");
        
        //Get the tags and filter them if there is a query for it
        final String[] products = ResourceUtils.filterTag(properties.get(ResourceConstants.PRODUCTS, String[].class), queryProductsStr);
        final String[] categories = ResourceUtils.filterTag(properties.get(ResourceConstants.CATEGORIES, String[].class), queryCategoriesStr);
        final String[] topics = ResourceUtils.filterTag(properties.get(ResourceConstants.TOPICS, String[].class), queryTopicsStr);
        final String[] industries = ResourceUtils.filterTag(properties.get(ResourceConstants.INDUSTRIES, String[].class), queryIndustriesStr);
        final String[] defaultProduct = properties.get(ResourceConstants.DEFAULT_PRODUCT, String[].class);
                
        //Start query builder parameters
        final Map<String, String> map = new HashMap<String, String>();
        map.put("path", path);
        map.put("type", "cq:Page");

        //Type
        if(types != null && types.length > 0) {
            map.put("2_property", "jcr:content/resource/" + ResourceConstants.RESOURCE_CATEGORIES);
            if(types.length > 1) {
                for(int i = 0; i < types.length; i++) {
                    map.put("2_property." + (i + 1) + "_value", types[i]);
                }
            }else {
                map.put("2_property.value", types[0]);
            }
        }
        
        //Products
        if((queryProductsStr == null || queryProductsStr.isEmpty()) && defaultProduct != null && defaultProduct.length > 0){
        	map.put("3_property", "jcr:content/resource/" + ResourceConstants.PRODUCTS);
        	map.put("3_property.value", defaultProduct[0]);
        }else if(products != null && products.length > 0) {
            map.put("3_property", "jcr:content/resource/" + ResourceConstants.PRODUCTS);
            if(products.length > 1) {
                for(int i = 0; i < products.length; i++) {
                    map.put("3_property." + (i + 1) + "_value", products[i]);
                }
            }else {
            	map.put("3_property.value", products[0]);
            }
        }

        //Categories
        if(categories != null && categories.length > 0) {
            map.put("4_property", "jcr:content/resource/" + ResourceConstants.CATEGORIES);
            if(categories.length > 1) {
                for(int i = 0; i < categories.length; i++) {
                    map.put("4_property." + (i + 1) + "_value", categories[i]);
                }
            }else {
                map.put("4_property.value", categories[0]);
            }
        }

        //Topic
        if(topics != null && topics.length > 0) {
            map.put("6_property", "jcr:content/resource/" + ResourceConstants.TOPICS);
            if(topics.length > 1) {
                for(int i = 0; i < topics.length; i++) {
                    map.put("6_property." + (i + 1) + "_value", topics[i]);
                }
            }else {
                map.put("6_property.value", topics[0]);
            }
        }

        //Industries
        if(industries != null && industries.length > 0) {
            map.put("7_property", "jcr:content/resource/" + ResourceConstants.INDUSTRIES);
            if(industries.length > 1) {
                for(int i = 0; i < industries.length; i++) {
                    map.put("7_property." + (i + 1) + "_value", industries[i]);
                }
            }else {
                map.put("7_property.value", industries[0]);
            }
        }
        
        //Language
  		if(language != null && language.length > 0) {
  			map.put("8_property", "jcr:content/resource/resourceLanguage");
  			if(language.length > 1) {
  				for(int i = 0; i < language.length; i++) {
                    map.put("8_property." + (i + 1) + "_value", language[i]);
                }
  			}else {
      			map.put("8_property.value", language[0]);
  			}
  		}
      		
        //Created
        map.put("9_property", "jcr:content/resource/created");
        map.put("9_property.value", "true");

        //Limit
        map.put("p.limit", "" + paginationLimit);

        //Pagination Index
        map.put("p.offset", "" + currentPaginationIndex);

        map.put("orderby", "@jcr:content/cq:lastModified");
        map.put("orderby.index", "true");
        map.put("orderby.sort", "desc");

        //Debugger
        Set<String> sets = map.keySet();
        for(String set : sets) {
            System.out.print(set + ":");
            System.out.println(map.get(set));
        }

        //Execute the query
        final Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
        final SearchResult searchResult = query.getResult();
        final long total = searchResult.getTotalMatches();

        final Iterator<Node> nodeIter = searchResult.getNodes();
        
        //Calculate the pagination indexes
        final long paginationStart = searchResult.getStartIndex();
        final long paginationEnd = paginationStart + paginationLimit;
        final int paginationTotalPages = (int) Math.ceil((double)total / (double)paginationLimit);
        final int paginationCurrentPage = (int) Math.floor((double)paginationStart / (double)paginationLimit);
        
        while(nodeIter.hasNext()) {
            final Node pageNode = nodeIter.next();
        	final Node pageJcrNode = pageNode.getNode(NameConstants.NN_CONTENT);
            final Node resourceNode = pageJcrNode.getNode(ResourceConstants.RESOURCE);
            final String resourceIdentifier = resourceNode.getProperty("resourceIdentifier").getString();
            final Resource currentResource = resourceResolver.getResource(resourceNode.getPath());
            
            System.out.println("Entering Condition and Resource Type is: " + resourceIdentifier);
            
            if(resourceIdentifier != null && resourceIdentifier.equals("Webinar")){
            	System.out.println("In Webinar Detail");
            	final Webinar webinarResource = ResourceUtils.getWebinar(currentResource, pageManager.getPage(pageNode.getPath()),currentDesign, request);
        		resources.add(webinarResource);
            }else if(resourceIdentifier != null && resourceIdentifier.equals("Standard")){
            	System.out.println("In Standard Detail");
            	final StandardDetail standardResource = ResourceUtils.getStandardDetails(currentResource, pageManager.getPage(pageNode.getPath()),currentDesign, request);
        		resources.add(standardResource);
            }
        }
        
        resourceList.setId(uniqueId);
        resourceList.setList(resources);
        resourceList.setStart(paginationStart);
        resourceList.setEnd(paginationEnd);
        resourceList.setTotal(total);
        resourceList.setTotalPages(paginationTotalPages);
        resourceList.setCurrentPage(paginationCurrentPage);
        resourceList.setExecutionTime(searchResult.getExecutionTime());
    
        final StringBuilder suffixPathBuilder = new StringBuilder();
        suffixPathBuilder.append(".startQuery");
        suffixPathBuilder.append(".products." + (queryProductsStr != null ? queryProductsStr : ""));
        suffixPathBuilder.append(".categories." + (queryCategoriesStr != null ? queryCategoriesStr : ""));
        suffixPathBuilder.append(".topics." + (queryTopicsStr != null ? queryTopicsStr : ""));
        suffixPathBuilder.append(".industries." + (queryIndustriesStr != null ? queryIndustriesStr : ""));
        suffixPathBuilder.append(".endQuery.html");

        resourceList.setSuffixPath(suffixPathBuilder.toString());
        resourceList.setSuffixPathWithQuery(suffixPathBuilder.toString() + (searchText != null ? "?q=" + searchText : ""));

        return resourceList;
    }

    public static Resource getResourcePath(Resource resource, String path) throws RepositoryException {
        final ResourceResolver resourceResolver = resource.getResourceResolver();
        final Node pageNode = resourceResolver.getResource(path).adaptTo(Node.class);
        if(pageNode != null && pageNode.hasNode(NameConstants.NN_CONTENT)) {
            final Node pageJcrNode = pageNode.getNode(NameConstants.NN_CONTENT);
            if(pageJcrNode.hasNode(ResourceConstants.RESOURCE)) {
                final Node resourceNode = pageJcrNode.getNode(ResourceConstants.RESOURCE);
                return resourceResolver.getResource(resourceNode.getPath());
            }
        }
        return null;
    }

    /**
     * Returns the <code>StandardDetail</code> object based on the resource and current page.
     * Takes care of Document, Awards, Customer Story, Video, Case Studies
     * @throws RepositoryException 
     * @throws IllegalStateException 
     * @throws PathNotFoundException 
     * @throws ValueFormatException 
     */
    public static StandardDetail getStandardDetails(Resource resource, Page page, Design currentDesign, HttpServletRequest request) throws ValueFormatException, PathNotFoundException, IllegalStateException, RepositoryException {
        final ValueMap properties = resource.adaptTo(ValueMap.class);
        final String BC_API_TOKEN = "DDj92gJNZ3ArRB5Wtwry2S-ofSMj4FTLuyOOzgHmbvQ0ALh_UORCmg..";
        ReadApi bcApi = new ReadApi();
        
        if(properties != null) {
            final StandardDetail standardDetails = new StandardDetail();

            //Resource Type
            final ResourceResolver resourceResolver = resource.getResourceResolver();
            final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            standardDetails.setResourceType((tagManager.resolve(properties.get(ResourceConstants.RESOURCE_TYPE, String.class)).getTitle()));
            
            //Resource
            standardDetails.setDate(properties.get(ResourceConstants.DATE, Date.class) != null ? properties.get(ResourceConstants.DATE, Date.class) : null);
            standardDetails.setId(ClientContextUtil.getId(resource.getPath()));
            standardDetails.setPath(ContextRootTransformUtil.transformedPath(page.getPath(),request));
            standardDetails.setTitle(properties.get(ResourceConstants.TITLE, String.class) != null ? properties.get(ResourceConstants.TITLE, String.class) : (standardDetails.getDamAssetTitle() != null ? standardDetails.getDamAssetTitle() : page.getTitle()));
            standardDetails.setDescription(properties.get(ResourceConstants.DESCRIPTION, String.class) != null ? properties.get(ResourceConstants.DESCRIPTION, String.class) : page.getDescription());
            standardDetails.setImagePath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? ContextRootTransformUtil.transformedPath(properties.get(ResourceConstants.FILE_REFERENCE, String.class),request) : ContextRootTransformUtil.transformedPath(currentDesign.getPath() + ResourceConstants.Document.DEFAULT_IMAGE,request));
            
            //Setting small image
            final String imageExtension = properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? (standardDetails.getImagePath().substring(standardDetails.getImagePath().lastIndexOf('.'), standardDetails.getImagePath().length()).equals(".jpg")) ? ".jpeg" : standardDetails.getImagePath().substring(standardDetails.getImagePath().lastIndexOf('.'), standardDetails.getImagePath().length()) : null;            
            standardDetails.setSmallImagePath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? ContextRootTransformUtil.transformedPath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) + ResourceConstants.SMALL_IMAGE + imageExtension,request) : ContextRootTransformUtil.transformedPath(currentDesign.getPath() + ResourceConstants.DEFAULT_IMAGE,request));
              
            standardDetails.setAuthorName(properties.get(ResourceConstants.StandardResources.AUTHOR_NAME, String.class) != null ? properties.get(ResourceConstants.StandardResources.AUTHOR_NAME, String.class) : null);
            standardDetails.setAuthorTitle(properties.get(ResourceConstants.StandardResources.AUTHOR_TITLE, String.class) != null ? properties.get(ResourceConstants.StandardResources.AUTHOR_TITLE, String.class) : null);
            standardDetails.setDownloadType(properties.get(ResourceConstants.StandardResources.DOWNLOAD_TYPE, String.class) != null ? properties.get(ResourceConstants.StandardResources.DOWNLOAD_TYPE, String.class) : null);
            standardDetails.setExternalLink(properties.get(ResourceConstants.StandardResources.EXTERNAL_LINK, String.class) != null ? properties.get(ResourceConstants.StandardResources.EXTERNAL_LINK, String.class) : null);
            standardDetails.setExternalLinkTitle(properties.get(ResourceConstants.StandardResources.EXTERNAL_LINK_TITLE, String.class) != null ? properties.get(ResourceConstants.StandardResources.EXTERNAL_LINK_TITLE, String.class) : null);
            standardDetails.setLanguage(properties.get(ResourceConstants.LANGUAGE, String.class));
            
            //Document Resource
            standardDetails.setDamAssetPath(properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) != null ? properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) : null);
            if(standardDetails.getDamAssetPath() != null) {
                final Resource assetResource = resource.getResourceResolver().getResource(standardDetails.getDamAssetPath());
                final Asset asset = assetResource != null ? assetResource.adaptTo(Asset.class) : null;
                if(asset != null) {
                    standardDetails.setDamAssetTitle(asset.getMetadataValue("dc:title"));
                    if(standardDetails.getDamAssetTitle() != null && standardDetails.getDamAssetTitle().trim().length() == 0) {
                    	standardDetails.setDamAssetTitle(null);
                    }
                    //setting mime type
                    standardDetails.setDamAssetType(asset.getMimeType());
                }
            }
            
            //Video Resource
            if(properties.get(ResourceConstants.Video.VIDEO_THUMBNAIL_URL, String.class) == null ||
               properties.get(ResourceConstants.Video.VIDEO_LENGTH, String.class) == null){
                //Update properties with Information from Brightcove
                try {
                    Video vid = bcApi.FindVideoById(
                        BC_API_TOKEN,
                        Long.parseLong(
                            properties.get(ResourceConstants.Video.VIDEO_PLAYER, String.class)
                        ),
                        EnumSet.of(
                            VideoFieldEnum.LENGTH,
                            VideoFieldEnum.LINKURL,
                            VideoFieldEnum.THUMBNAILURL
                        ),
                        null
                    );
                    standardDetails.setThumbnailPath(vid.getThumbnailUrl());
                    standardDetails.setVideoLength(String.valueOf(vid.getLength()));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (BrightcoveException e) {
                    e.printStackTrace();
                }
            } else {
                standardDetails.setVideoLength(ResourceUtils.getHumanVideoTime(properties.get(ResourceConstants.Video.VIDEO_LENGTH, 0)));
                standardDetails.setThumbnailPath(properties.get(ResourceConstants.Video.VIDEO_THUMBNAIL_URL, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_THUMBNAIL_URL, String.class) : null);
            }
            
            //Event Resource
            standardDetails.setEventCities(properties.get(ResourceConstants.Events.EVENT_CITY, String.class) != null ? properties.get(ResourceConstants.Events.EVENT_CITY, String.class) : null);
            standardDetails.setEventCountry(properties.get(ResourceConstants.Events.EVENT_COUNTRY, String.class) != null ? properties.get(ResourceConstants.Events.EVENT_COUNTRY, String.class) : null);
            standardDetails.setEventStartDate(properties.get(ResourceConstants.Events.EVENT_START_DATE, Date.class) != null ? properties.get(ResourceConstants.Events.EVENT_START_DATE, Date.class) : null);
            standardDetails.setEventEndDate(properties.get(ResourceConstants.Events.EVENT_END_DATE, Date.class) != null ? properties.get(ResourceConstants.Events.EVENT_END_DATE, Date.class) : null);
            standardDetails.setEventTimeZone(properties.get(ResourceConstants.Events.EVENT_TIME_ZONE, "PST"));
            
            //General Properties
            standardDetails.setPlayerId(properties.get(ResourceConstants.Video.PLAYER_ID, String.class) != null ? properties.get(ResourceConstants.Video.PLAYER_ID, String.class) : null);
            standardDetails.setPlayerKey(properties.get(ResourceConstants.Video.PLAYER_KEY, String.class) != null ? properties.get(ResourceConstants.Video.PLAYER_KEY, String.class) : null);
            standardDetails.setVideoTitle(properties.get(ResourceConstants.Video.VIDEO_TITLE, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_TITLE, String.class) : null);
            standardDetails.setVideoPlayer(properties.get(ResourceConstants.Video.VIDEO_PLAYER, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_PLAYER, String.class) : null);
            
            //Filters
            standardDetails.setCategories(properties.get(ResourceConstants.CATEGORIES, String[].class));
            standardDetails.setProducts(properties.get(ResourceConstants.PRODUCTS, String[].class));
            standardDetails.setIndustries(properties.get(ResourceConstants.INDUSTRIES, String[].class));
            standardDetails.setTopics(properties.get(ResourceConstants.TOPICS, String[].class));
            
            //Product Titles
            String[] productTagsTitle = new String[standardDetails.getProducts().length];
            if(standardDetails.getProducts().length != 0) {
            	for(int i=0; i < standardDetails.getProducts().length; i++) {
        	        productTagsTitle[i] = (tagManager.resolve(standardDetails.getProducts()[i]).getTitle());
        	    }
            }
            standardDetails.setProductsTitle(productTagsTitle);
            
            //Image Uploaded
            standardDetails.setHasImage(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? true : false);
            
            //Contains Video
            standardDetails.setHasVideo(ArrayUtils.contains(properties.get(ResourceConstants.RESOURCE_CATEGORIES, String[].class),"resource-category-type:video") ? true : false);
            
            //Gated
            standardDetails.setIsGated(properties.get(ResourceConstants.GATED, false));
            
            return standardDetails;
        }else {
            return null;
        }
    }
    
    /**
     * Returns the <code>Webinar</code> object based on the resource and current page.
     */
    public static Webinar getWebinar(Resource resource, Page page, Design currentDesign, HttpServletRequest request) throws RepositoryException{
        final ValueMap properties = resource.adaptTo(ValueMap.class);
        if(properties != null) {
            final Webinar webinar = new Webinar();
            
            //Resource Type
            final ResourceResolver resourceResolver = resource.getResourceResolver();
            final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            webinar.setResourceType("Webinar");
            
            //Resource
            webinar.setDate(properties.get(ResourceConstants.DATE, Date.class) != null ? properties.get(ResourceConstants.DATE, Date.class) : null);
            webinar.setEventStartDate(properties.get(ResourceConstants.DATE, Date.class) != null ? properties.get(ResourceConstants.DATE, Date.class) : null);
            webinar.setEventEndDate(properties.get(ResourceConstants.END_DATE, Date.class) != null ? properties.get(ResourceConstants.END_DATE, Date.class) : null);
            webinar.setEventTimeZone(properties.get(ResourceConstants.Webinar.TIME_ZONE, "PST"));
            webinar.setId(ClientContextUtil.getId(resource.getPath()));
            webinar.setPath(ContextRootTransformUtil.transformedPath(page.getPath(),request));
            webinar.setTitle(properties.get(ResourceConstants.TITLE, String.class) != null ? properties.get(ResourceConstants.TITLE, String.class) : page.getTitle());
            webinar.setDescription(properties.get(ResourceConstants.DESCRIPTION, String.class) != null ? properties.get(ResourceConstants.DESCRIPTION, String.class) : page.getDescription());
            webinar.setCategories(properties.get(ResourceConstants.CATEGORIES, String[].class));
            webinar.setProducts(properties.get(ResourceConstants.PRODUCTS, String[].class));
            webinar.setIndustries(properties.get(ResourceConstants.INDUSTRIES, String[].class));
            webinar.setTopics(properties.get(ResourceConstants.TOPICS, String[].class));
            webinar.setImagePath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? ContextRootTransformUtil.transformedPath(properties.get(ResourceConstants.FILE_REFERENCE, String.class),request) : ContextRootTransformUtil.transformedPath(currentDesign.getPath() + ResourceConstants.Webinar.DEFAULT_IMAGE,request));
            
            //Setting small image
            final String imageExtension = properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? (webinar.getImagePath().substring(webinar.getImagePath().lastIndexOf('.'), webinar.getImagePath().length()).equals(".jpg")) ? ".jpeg" : webinar.getImagePath().substring(webinar.getImagePath().lastIndexOf('.'), webinar.getImagePath().length()) : null;            
            webinar.setSmallImagePath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? ContextRootTransformUtil.transformedPath(properties.get(ResourceConstants.FILE_REFERENCE, String.class) + ResourceConstants.SMALL_IMAGE + imageExtension,request) : ContextRootTransformUtil.transformedPath(currentDesign.getPath() + ResourceConstants.Webinar.DEFAULT_IMAGE,request));
            
            //Webinar Resource and Speakers
            webinar.setRegisterPath(properties.get(ResourceConstants.Webinar.REGISTER_PATH, String.class));
            final Node webinarNode = resource.adaptTo(Node.class);
            if(webinarNode.hasNode(ResourceConstants.Webinar.SPEAKERS)) {
                final Node speakersNode = webinarNode.getNode(ResourceConstants.Webinar.SPEAKERS);
                final ArrayList<Map<String, Property>> parsedSpeakers = Utilities.parseStructuredMultifield(speakersNode);
                for(Map<String, Property> parsedSpeaker: parsedSpeakers) {
                    final Speaker speaker = new Speaker();
                    speaker.setName(parsedSpeaker.containsKey(ResourceConstants.Speaker.NAME) ? parsedSpeaker.get(ResourceConstants.Speaker.NAME).getString() : null);
                    speaker.setTitle(parsedSpeaker.containsKey(ResourceConstants.Speaker.TITLE) ? parsedSpeaker.get(ResourceConstants.Speaker.TITLE).getString() : null);
                    speaker.setCompany(parsedSpeaker.containsKey(ResourceConstants.Speaker.COMPANY) ? parsedSpeaker.get(ResourceConstants.Speaker.COMPANY).getString() : null);
                    speaker.setCity(parsedSpeaker.containsKey(ResourceConstants.Speaker.CITY) ? parsedSpeaker.get(ResourceConstants.Speaker.CITY).getString() : null);
                    speaker.setState(parsedSpeaker.containsKey(ResourceConstants.Speaker.STATE) ? parsedSpeaker.get(ResourceConstants.Speaker.STATE).getString() : null);
                    speaker.setThumbnail(parsedSpeaker.containsKey(ResourceConstants.Speaker.THUMBNAIL) ? parsedSpeaker.get(ResourceConstants.Speaker.THUMBNAIL).getString() : null);
                    webinar.addSpeaker(speaker);
                }
            }
            
            //Video Resource
            webinar.setPlayerId(properties.get(ResourceConstants.Video.PLAYER_ID, String.class) != null ? properties.get(ResourceConstants.Video.PLAYER_ID, String.class) : null);
            webinar.setPlayerKey(properties.get(ResourceConstants.Video.PLAYER_KEY, String.class) != null ? properties.get(ResourceConstants.Video.PLAYER_KEY, String.class) : null);
            webinar.setVideoTitle(properties.get(ResourceConstants.Video.VIDEO_TITLE, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_TITLE, String.class) : null);
            webinar.setVideoLength(ResourceUtils.getHumanVideoTime(properties.get(ResourceConstants.Video.VIDEO_LENGTH, 0)));
            webinar.setThumbnailPath(properties.get(ResourceConstants.Video.VIDEO_THUMBNAIL_URL, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_THUMBNAIL_URL, String.class) : null);
            webinar.setVideoPlayer(properties.get(ResourceConstants.Video.VIDEO_PLAYER, String.class) != null ? properties.get(ResourceConstants.Video.VIDEO_PLAYER, String.class) : null);

            //Image Uploaded
            webinar.setHasImage(properties.get(ResourceConstants.FILE_REFERENCE, String.class) != null ? true : false);
            
            //Contains Video
            webinar.setHasVideo(ArrayUtils.contains(properties.get(ResourceConstants.RESOURCE_CATEGORIES, String[].class),"resource-category-type:video") ? true : false);
            
            //Gated
            webinar.setIsGated(properties.get(ResourceConstants.GATED, false));
            
            //On Demand
            webinar.setOnDemand(properties.get(ResourceConstants.Webinar.ON_DEMAND, false));
            
            //Product Titles
            String[] productTagsTitle = new String[webinar.getProducts().length];
            if(webinar.getProducts().length != 0) {
            	for(int i=0; i < webinar.getProducts().length; i++) {
        	        productTagsTitle[i] = (tagManager.resolve(webinar.getProducts()[i]).getTitle());
        	    }
            }
            webinar.setProductsTitle(productTagsTitle);
            
            return webinar;
        }else {
            return null;
        }
    }
    
    /**
     * Returns current page theme based on Secondary Nav for Resource center.
     * @throws RepositoryException 
     * @throws PathNotFoundException 
     */
    public static String ResourceCenterProductTheme(Resource resource, Page currentPage) throws PathNotFoundException, RepositoryException {
    	String theme = "";
    	final Node node = resource.getResourceResolver().getResource(currentPage.getPath()).adaptTo(Node.class).getNode("jcr:content");
    	if(node != null) {
    		theme = node.hasNode("resourceHeader") ? node.getNode("resourceHeader").hasNode("secondarynavigation") ? node.getNode("resourceHeader").getNode("secondarynavigation").hasProperty("theme") ? node.getNode("resourceHeader").getNode("secondarynavigation").getProperty("theme").getString() : "" : "" : "";
    	}
    	return theme;
    }
    
    /**
     * Returns nodes that are selected as Featured Resource Path
     * @throws RepositoryException 
     */
    
    public static List<com.citrixosdRedesign.models.resource.Resource> getFeaturedResourcesFromLinks(Resource resource, Node baseFeaturedNode, Design currentDesign, HttpServletRequest request) throws RepositoryException {
    	final List<com.citrixosdRedesign.models.resource.Resource> resources = new ArrayList<com.citrixosdRedesign.models.resource.Resource>();
    	
    	if(baseFeaturedNode != null) {
    		final ResourceResolver resourceResolver = resource.getResourceResolver();
            final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            
        	NodeIterator childFeaturedNodes =  baseFeaturedNode.getNodes();
        	
        	while(childFeaturedNodes.hasNext()) {
                final Node pageNode = childFeaturedNodes.nextNode();
                final Node featuredPageNode = resourceResolver.getResource(pageNode.getProperty("resourcePath").getString()).adaptTo(Node.class);
                final Node featuredPageJcrNode = featuredPageNode.getNode(NameConstants.NN_CONTENT);                
                final Node resourceNode = featuredPageJcrNode.getNode(ResourceConstants.RESOURCE);
                final String resourceIdentifier = resourceNode.getProperty("resourceIdentifier").getString();
                final Resource currentResource = resourceResolver.getResource(resourceNode.getPath());
                
                System.out.println("Entering Condition and Resource Type is: " + resourceIdentifier);
                
                if(resourceIdentifier != null && resourceIdentifier.equals("Webinar")){
                	System.out.println("In Webinar Detail");
                	final Webinar webinarResource = ResourceUtils.getWebinar(currentResource,pageManager.getPage(featuredPageNode.getPath()),currentDesign,request);
            		resources.add(webinarResource);
                }else if(resourceIdentifier != null && resourceIdentifier.equals("Standard")){
                	System.out.println("In Standard Detail");
                	final StandardDetail standardResource = ResourceUtils.getStandardDetails(currentResource,pageManager.getPage(featuredPageNode.getPath()),currentDesign,request);
            		resources.add(standardResource);
                }
            }
    	}
    	return resources;
    }
}