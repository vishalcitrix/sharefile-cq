package com.citrixosdRedesign.models.resource;

import java.util.List;

public class Pagination<E> {
    private String id;
	private long start;
	private long end;
	private long total;
	private int totalPages;
	private int currentPage;
    private String executionTime;
	private List<com.citrixosdRedesign.models.resource.Resource> list;
    private String suffixPath;
    private String suffixPathWithQuery;
	
	public Pagination() {
		
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}
	
	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

    public String getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

    public List<com.citrixosdRedesign.models.resource.Resource> getList() {
		return list;
	}

	public void setList(List<com.citrixosdRedesign.models.resource.Resource> list) {
		this.list = list;
	}

    public String getSuffixPath() {
        return suffixPath;
    }

    public void setSuffixPath(String suffixPath) {
        this.suffixPath = suffixPath;
    }

    public String getSuffixPathWithQuery() {
        return suffixPathWithQuery;
    }

    public void setSuffixPathWithQuery(String suffixPathWithQuery) {
        this.suffixPathWithQuery = suffixPathWithQuery;
    }

}