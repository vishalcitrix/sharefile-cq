package com.citrixosdRedesign.models.resource;

import java.util.Date;

public abstract class Resource {
	private String id;
	private String path;
	private Date date;
	private Date eventStartDate;
	private Date eventEndDate;
	private String eventTimeZone;
    private String title;
    private String description;
    private String language;
    private String[] featured;
	private String[] categories;
    private String[] products;
    private String[] productsTitle;
    private String[] topics;
    private String[] industries;
	private String resourceType;
    private String imagePath;
    private String smallImagePath;
    private String playerId;
    private String playerKey;
    private String thumbnailPath;
    private String videoTitle;
    private String videoLength;
    private String videoPlayer;
    private boolean hasImage;
    private boolean hasVideo;
    private boolean isGated;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
    
	public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
	public Date getEventStartDate() {
        return eventStartDate;
    }
	
	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }
    
    public String getEventTimeZone() {
		return eventTimeZone;
	}

	public void setEventTimeZone(String eventTimeZone) {
		this.eventTimeZone = eventTimeZone;
	}
	
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

	public String[] getCategories() {
		return categories;
	}

	public void setCategories(String[] categories) {
		this.categories = categories;
	}

    public String[] getProducts() {
        return products;
    }

    public void setProducts(String[] products) {
        this.products = products;
    }
    
    public String[] getProductsTitle() {
        return productsTitle;
    }

    public void setProductsTitle(String[] productsTitle) {
        this.productsTitle = productsTitle;
    }

    public String[] getFeatured() {
        return featured;
    }

    public void setFeatured(String[] featured) {
        this.featured = featured;
    }

    public String[] getTopics() {
        return topics;
    }

    public void setTopics(String[] topics) {
        this.topics = topics;
    }

    public String[] getIndustries() {
        return industries;
    }

    public void setIndustries(String[] industries) {
        this.industries = industries;
    }

    public String getResourceType() {
        return resourceType;
    }

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    
    public String getSmallImagePath() {
        return smallImagePath;
    }

    public void setSmallImagePath(String smallImagePath) {
        this.smallImagePath = smallImagePath;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerKey() {
        return playerKey;
    }

    public void setPlayerKey(String playerKey) {
        this.playerKey = playerKey;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }

    public String getVideoPlayer() {
        return videoPlayer;
    }

    public void setVideoPlayer(String videoPlayer) {
        this.videoPlayer = videoPlayer;
    }
    
    public boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }
    
    public boolean getHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }
    
    public boolean getIsGated() {
        return isGated;
    }

    public void setIsGated(boolean isGated) {
        this.isGated = isGated;
    }
}