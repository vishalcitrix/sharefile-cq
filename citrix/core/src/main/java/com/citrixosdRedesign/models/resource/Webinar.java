package com.citrixosdRedesign.models.resource;

import java.util.ArrayList;
import java.util.List;

public class Webinar extends Resource {
    private List<Speaker> speakers = new ArrayList<Speaker>();
    private String registerPath;
    private Boolean onDemand;

	public Webinar() {
		
	}

    public List<Speaker> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(List<Speaker> speakers) {
        this.speakers = speakers;
    }

    public void addSpeaker(Speaker speaker) {
        this.speakers.add(speaker);
    }

    public String getRegisterPath() {
        return registerPath;
    }

    public void setRegisterPath(String registerPath) {
        this.registerPath = registerPath;
    }
    
    public Boolean getOnDemand() {
        return onDemand;
    }

    public void setOnDemand(Boolean onDemand) {
        this.onDemand = onDemand;
    }
}