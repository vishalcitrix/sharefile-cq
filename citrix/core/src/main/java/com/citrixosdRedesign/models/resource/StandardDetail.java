package com.citrixosdRedesign.models.resource;

public class StandardDetail extends Resource {
	private String damAssetPath;
    private String damAssetTitle;
    private String damAssetType;
    private String authorName;
    private String authorTitle;
    private String downloadExternal;
    private String externalLinkPath;
    private String externalLinkTitle;
    private String eventCities;
    private String eventCountry;
    
	public StandardDetail() {
		
	}

    public String getDamAssetPath() {
        return damAssetPath;
    }

    public void setDamAssetPath(String damAssetPath) {
        this.damAssetPath = damAssetPath;
    }
    
    public String getDamAssetTitle() {
        return damAssetTitle;
    }

    public void setDamAssetTitle(String damAssetTitle) {
        this.damAssetTitle = damAssetTitle;
    }
    
    public String getDamAssetType() {
        return damAssetType;
    }

    public void setDamAssetType(String damAssetType) {
        this.damAssetType = damAssetType;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    
    public String getAuthorTitle() {
        return authorTitle;
    }

    public void setAuthorTitle(String authorTitle) {
        this.authorTitle = authorTitle;
    }
    
    public String getDownloadType() {
        return downloadExternal;
    }

    public void setDownloadType(String downloadExternal) {
        this.downloadExternal = downloadExternal;
    }
    
    public String getExternalLink() {
        return externalLinkPath;
    }

    public void setExternalLink(String externalLinkPath) {
        this.externalLinkPath = externalLinkPath;
    }
    
    public String getExternalLinkTitle() {
        return externalLinkTitle;
    }

    public void setExternalLinkTitle(String externalLinkTitle) {
        this.externalLinkTitle = externalLinkTitle;
    }
    
    public String getEventCities() {
		return eventCities;
	}

	public void setEventCities(String eventCities) {
		this.eventCities = eventCities;
	}
	
	public String getEventCountry() {
		return eventCountry;
	}

	public void setEventCountry(String eventCountry) {
		this.eventCountry = eventCountry;
	}
}