package com.citrixosd.cdn.workflow;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Dictionary;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.RepositoryException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.siteworx.rewrite.transformer.ContextRootTransformer;

@Component(immediate = true, metatype = true, label = "CitrixOSD CDN Purge Process", description = "Process to purge CDN of outdated contents.")
@Service(value = { WorkflowProcess.class, CDNPurgeProcess.class })
@Properties({ @Property(name = "process.label", propertyPrivate = true, value = "CitrixOSD CDN Purge") })
public class CDNPurgeProcess implements WorkflowProcess {

	private static final Logger log = LoggerFactory.getLogger(CDNPurgeProcess.class);

	private static final String PROPERTY_PURGE_ENDPOINT = "cdnpurgeprocess.domainEndpoint";

	@Property(name = PROPERTY_PURGE_ENDPOINT, label = "CDN Purge HTTP Endpoint", value = "", description = "Include protocol.")
	private String domainEndpoint = "";

	@Reference
	private ContextRootTransformer transformer;
    
	private static final Pattern PATTERN_PROTOCOL = Pattern.compile("^(http|https|ftp|ftps)://.*$");
    
	public CDNPurgeProcess() {
		if (log.isTraceEnabled()) {
			log.trace("Instantiating a CDNPurgeProcess.");	
		}
	}

	public void execute(WorkItem item, WorkflowSession session, MetaDataMap args) throws WorkflowException {
		if (log.isTraceEnabled()) {
			log.trace("Executing CDNPurgeProcess using domain endpoint " + domainEndpoint + ".");
		}

		if (item.getWorkflowData().getPayloadType().equals("JCR_PATH")) {
			String nodePath = item.getWorkflowData().getPayload().toString();
			try {
				String transformedPath = transformer.transformIgnoreDisabled(nodePath);
				if (transformedPath != null) {
					String finalEndpoint = domainEndpoint + transformedPath;
					log.trace("Final endpoint for CDN Purge HTTP request is  " + finalEndpoint + ".");

					HttpClient httpClient = new DefaultHttpClient();
					HttpGet getRequest = new HttpGet(finalEndpoint);
					getRequest.addHeader(new BasicHeader("Accept", "*/*"));
					HttpResponse response = httpClient.execute(getRequest);
					if (response.getStatusLine().getStatusCode() != 200) {
						log.error("Error purging the CDN. Status code of " + response.getStatusLine().getStatusCode() + " on request to  " + finalEndpoint + ".");
					}
					log.trace("Response code for purge request: " + response.getStatusLine());
				} else {
					log.warn("Transformed path was null, not purging.");
				}

			} catch (RepositoryException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				log.error("The domain endpoint for the CDN purge process is unreachable. Check your configuration.");
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			log.warn("CDNPurgeProcess unable to handle payload type of " + item.getWorkflowData().getPayloadType() + ".");
		}
	}

	@Activate
	public void activate(ComponentContext context) {
		if (log.isTraceEnabled()) {
			log.trace("CDNPurgeProcess Activated.");
		}

		@SuppressWarnings("rawtypes")
		Dictionary properties = context.getProperties();

		this.domainEndpoint = PropertiesUtil.toString(properties.get(PROPERTY_PURGE_ENDPOINT), "");
		
		//Check of the domain has a protocol and add http as default
		final Matcher protocolMatcher = PATTERN_PROTOCOL.matcher(domainEndpoint);
		if(!protocolMatcher.matches()) {
			if(domainEndpoint.startsWith("//")) {
				domainEndpoint = "http:" + domainEndpoint;
			}else {
				domainEndpoint = "http://" + domainEndpoint;
			}
		}
    }

	@Modified
	public void modified(ComponentContext context) {
		if (log.isTraceEnabled()) {
			log.trace("CDNPurgeProcess Modified: Reactivating.");	
		}
		activate(context);
	}
}