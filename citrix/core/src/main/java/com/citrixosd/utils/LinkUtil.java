package com.citrixosd.utils;

import javax.jcr.RepositoryException;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.siteworx.rewrite.transformer.ContextRootTransformer;

public class LinkUtil {
	
	public LinkUtil() {
		
	}
	
	/**
	 * Shortcut to retrieve the transformed URL from the path of the content and using sling to get the 
	 * <code>ContextRootTransformer</code> service to use the <code>ContextRootTransformer.transform</code>
	 * method.
	 */
	public static String getTransformedUrl(String url, SlingScriptHelper sling) { 
		if(sling == null || url == null) {
			return url;
		}else {
			try {
				ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
				final String transformed = transformer.transform(url);
				if(transformed != null) {
					return transformed;
				}else {
					return url;
				}
			} catch (RepositoryException e) {
				e.printStackTrace();
				return url;
			}
		}
	}
	
	/**
	 * Shortcut to retrieve the transformed URL from the path of the content and using transformer.
	 */
	public static String getTransformedUrl(String url, ContextRootTransformer transformer) { 
		if(transformer == null || url == null) {
			return url;
		}else {
			try {
				final String transformed = transformer.transform(url);
				if(transformed != null) {
					return transformed;
				}else {
					return url;
				}
			} catch (RepositoryException e) {
				e.printStackTrace();
				return url;
			}
		}
	}

}