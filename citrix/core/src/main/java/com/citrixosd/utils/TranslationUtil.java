package com.citrixosd.utils;

import java.util.ResourceBundle;

public class TranslationUtil {
	public TranslationUtil() {
		
	}
	
	/**
	 * Translate a list of strings. If the array or resource bundle is null, this will return 
	 * either the array back or null if array is null.
	 */
	public static String[] translateArray(String[] array, ResourceBundle resourceBundle) {
		if(array != null && resourceBundle != null) {
			final String[] translatedArray = new String[array.length];
			for(int i = 0; i < array.length; i++) {
				translatedArray[i] = resourceBundle.getString(array[i]);
			}
			return translatedArray;
		}else {
			return array;
		}
	}
}