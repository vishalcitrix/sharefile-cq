package com.citrixosd.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;

public class Utilities {
	public Utilities() {
		
	}
	
	/**
	 * Search through the string array of tags and return the first tag. If there is no 
	 * tag in the string array or if the tag is not found, this will return null.
	 */
	public static Tag getFirstTag(String[] tags, TagManager tagManager) {
		if(tagManager != null && tags != null && tags.length > 0) {
			return tagManager.resolve(tags[0]);
		}else {
			return null;
		}
	}
	
	/**
	 * Search through the string array of tags and return the <code>List<code> of tag. 
	 * If there is no tag in the string array this will empty list of tags. If the tag 
	 * can not be resolved, it will not be added to the list of tags.
	 */
	public static List<Tag> getTags(String[] tagsStr, TagManager tagManager) {
		List<Tag> tags = new ArrayList<Tag>();
		if(tagManager != null && tagsStr != null && tagsStr.length > 0) {
			for(String tag : tagsStr) {
				final Tag currentTag = tagManager.resolve(tag);
				if(currentTag != null) {
					tags.add(currentTag);
				}
			}
		}
		return tags;
	}
	
	/**
	 * Converts the property value from a <code>Value</code> to a <code>String[]<code> format.
	 * This function will return null if the node does not have a valid propertyName.
	 */
	public static String[] getStringArrayFromArrayProperty (Node node, String propertyName) throws ValueFormatException, PathNotFoundException, IllegalStateException, RepositoryException {
    	if(node.hasProperty(propertyName)) {
    		final Value[] values = node.getProperty(propertyName).getValues();
    		final String[] array = new String[values.length];
    		for(int i = 0; i < values.length; i++) {
    			array[i] = values[i].getString();
    		}
    		return array;
    	}else {
    		return null;
    	}
	}
	
	/**
	 * This used the <code>com.citrixosd.utils.Utilities.getStringArrayFromNodeProperty</code> and 
	 * retrieves the first array string. If the array is null or empty, this will return null.
	 */
	public static String getFirstStringArrayFromArrayProperty (Node node, String propertyName) throws ValueFormatException, PathNotFoundException, IllegalStateException, RepositoryException {
		final String[] array = getStringArrayFromArrayProperty(node, propertyName);
		if(array != null) {
			if(array.length > 0) {
				return array[0];
			}else {
				return null;
			}
		}else {
			return null;
		}
	}
	
	/**
	 * This function will parse structured multifield.
	 * Base node should be passed to this function and a ArrayList of Hash map will be 
	 * passed with property name and its corresponding values
	 * vishal.gupta@citrix.com
	 */
	public static ArrayList<Map<String, Property>> parseStructuredMultifield(Node node) {
        ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
        try {
            NodeIterator childNodes = node.getNodes();
            while(childNodes.hasNext()){
                Map<String, Property> hm = new HashMap<String, Property>();
                Node currNode = childNodes.nextNode();
                PropertyIterator currNodeproperties = currNode.getProperties();
                String currpropertyName = null;
                while(currNodeproperties.hasNext()){
                    Property currproperty = currNodeproperties.nextProperty();
                    currpropertyName = currproperty.getName();
                    hm.put(currpropertyName, currproperty);
                }
                values.add(hm);
            }
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return values;
    }
	
	/**
	 * This function will return locale <code>locale</code> for current page
	 * vishal.gupta@citrix.com
	 */
	public static Locale getLocale(Page currentPage, HttpServletRequest request) {
		Locale locale = null;
		final Page localePage = currentPage.getAbsoluteParent(2);
		if (localePage != null) {
	        try {
	            locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
	        } catch (Exception e) {
	            locale = request.getLocale();
	        }
	    }
	    else{
	        locale = request.getLocale();
	    }
		
		return locale;
	}
	
	/**
	 * This function will return child tags under a given NameSpace
	 * vishal.gupta@citrix.com
	 */
	public static List<String> getChildTags(String tagsNameSpace, TagManager tagManager) {
		List<String> tagsList = new ArrayList<String>();
		Tag themeNameSpace = tagManager.resolve(tagsNameSpace);
		Iterator<Tag> theNameSpaceIterator = themeNameSpace.listChildren();
		while(theNameSpaceIterator.hasNext()){
			tagsList.add(theNameSpaceIterator.next().getTitle());
		}
		return tagsList;
	}

	public static String getVisibilityClass(boolean showForLarge, boolean showForMedium, boolean showForSmall){
        
        String visibilityClass = "";
        
        if(showForLarge && showForMedium && !(showForSmall)){
            visibilityClass = "show-for-medium-up";
        }
        
        else if(!(showForLarge) && !(showForMedium) && showForSmall){
            visibilityClass = "show-for-small-only";
        } 
        
        else if(showForLarge && !(showForMedium) && showForSmall){
            visibilityClass = "hide-for-medium-only";
        }
        
        else if(!(showForLarge) && showForMedium && !(showForSmall)){
            visibilityClass = "show-for-medium-only";
        }
        
        else if(!(showForLarge) && showForMedium && showForSmall){
            visibilityClass = "hide-for-large-up";
        } 
    
        else if(showForLarge && !(showForMedium) && !(showForSmall)){
            visibilityClass = "show-for-large-up";
        } 
        
        else if(!(showForLarge) && !(showForMedium) && !(showForSmall)){
            visibilityClass = "hide";
        } 
        
        else {
            visibilityClass = "";
        }
        return visibilityClass;
    }
}