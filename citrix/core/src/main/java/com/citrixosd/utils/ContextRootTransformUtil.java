package com.citrixosd.utils;

import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletRequest;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import com.siteworx.rewrite.transformer.ContextRootTransformer;

public class ContextRootTransformUtil {
	public ContextRootTransformUtil() {
		
	}
	
	/**
	 * This used the <code>com.citrixosd.utils.ContextRootTransformUtil.transformedPath</code> and 
	 * retrieves the context root transformed path for the author path.
	 * vishal.gupta@citrix.com
	 * @throws RepositoryException 
	 */
	public static String transformedPath (String authorNodePath, HttpServletRequest request) throws RepositoryException {
		if(authorNodePath != null && !authorNodePath.isEmpty()) {
			SlingBindings bindings = (SlingBindings) request.getAttribute(SlingBindings.class.getName());
			SlingScriptHelper scriptHelper = bindings.getSling();
			
			final ContextRootTransformer transformer = scriptHelper.getService(ContextRootTransformer.class);
			String transformed = null;
			transformed = transformer.transform(authorNodePath);
			if (transformed != null) {
				authorNodePath = transformed;
		    }
		}
		return authorNodePath;
	}
}