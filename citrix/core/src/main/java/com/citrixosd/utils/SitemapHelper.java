package com.citrixosd.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import com.brightcove.proserve.mediaapi.wrapper.ReadApi;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.Video;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.VideoFieldEnum;
import com.brightcove.proserve.mediaapi.wrapper.exceptions.BrightcoveException;
import com.citrixosd.utils.ContextRootTransformUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

public class SitemapHelper {
	private static final String BC_API_TOKEN = "DDj92gJNZ3ArRB5Wtwry2S-ofSMj4FTLuyOOzgHmbvQ0ALh_UORCmg..";
	private ReadApi bcApi = new ReadApi();
	private static final String W3CDTF_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	private LinkedList<Link> links = new LinkedList<Link>();
	private StringBuffer sitemapXML = null;

	/**
	 * @author sugupta,vishal.gupta@citrix.com
	 * inner class to hold page details
	 */
	public class Link {
		private String path;
		private String title;
		private int level;
		private String lastmod;
		private String changefreq;
		private String priority;
		
		private LinkedList<AlternateHref> ahrefs = new LinkedList<AlternateHref>();
		private LinkedList<VideoModel> videos = new LinkedList<VideoModel>();
		
		/**
		 * 
		 * @author Michael Jostmeyer
		 * inner class to hold alternate href link data
		 *
		 */
		public class AlternateHref {
			private String languageCode;
			private String href;
			
			public AlternateHref(String lang, String href) {
				this.setLanguageCode(lang);
				this.setHref(href);
			}
			
			public void setLanguageCode(String lang){
				this.languageCode = lang;
			}
			
			public String getLanguageCode(){
				return this.languageCode;
			}
			
			public void setHref(String href){
				this.href = href;
			}
			
			public String getHref(){
				return this.href;
			}
		}
		
		public Link(String path, String title, String lastmod, String changefreq, String priority, int level) {
			this.path = path;
			this.level = level;
			this.setTitle(title);
			this.setLastmod(lastmod);
			this.setChangefreq(changefreq);
			this.setPriority(priority);
		}

		public String getPath() {
			return path;
		}

		public int getLevel() {
			return level;
		}

		public String getLastmod() {
			return lastmod;
		}

		public void setLastmod(String lastmod) {
			this.lastmod = lastmod;
		}

		public String getChangefreq() {
			return changefreq;
		}

		public void setChangefreq(String changefreq) {
			this.changefreq = changefreq;
		}

		public String getPriority() {
			return priority;
		}

		public void setPriority(String priority) {
			this.priority = priority;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
		
		public Boolean hasAlternateHrefs() {
			return (ahrefs.size() > 0);
		}
		
		public Boolean hasVideos() {
			return (videos.size() > 0);
		}
		
		public void addAlternateHref(String lang, String href) {
			ahrefs.add(new AlternateHref(lang, href));
		}
		
		public StringBuffer drawAlternateHrefXML() {
			StringBuffer xml = new StringBuffer("");
			for(AlternateHref aHref: ahrefs) {
				xml.append("<xhtml:link rel=\"alternate\" ");
				xml.append("hreflang=\"" + aHref.getLanguageCode() + "\" ");
				xml.append("href=\"" + aHref.getHref() + "\" />\n");
			}
			return xml;
		}
		
		public void addBrightcoveVideo(String playerID, String playerKey, 
			String videoID, String contentLocation, String thumbnailLocation, String title,
			String description, Long length) {
			BrightcoveVideoModel vid = new BrightcoveVideoModel(playerID, playerKey, videoID);
			vid.setThumbnailLocation(thumbnailLocation);
			vid.setContentLocation(contentLocation);
			vid.setTitle(title);
			vid.setDescription(description);
			vid.setDuration(String.valueOf(length));
			videos.add(vid);
		}
		
		public StringBuffer drawVideoXML() {
			StringBuffer xml = new StringBuffer("");
			for(VideoModel vid: videos){
				xml.append(vid.drawVideoXML());
			}
			return xml;
		}
	}
	
	/**
	 * @author Michael Jostmeyer
	 * inner class to hold video information for one page
	 */
	public class VideoModel {
		private String thumbnailLocation;
		private String title;
		private String description;
		private String contentLocation;
		private String playerLocation;
		private String duration;
		private String expirationDate;
		private String rating;
		private String viewCount;
		private String publicationDate;
		private String familyFriendly;
		private String restriction;
		private String restrictionRelationship = "allow";
		private String galleryLocation;
		private String price;
		private String priceCurrency;
		private String requiresSubscription = "no";
		private String uploader;
		private String live = "no";
		
		public VideoModel(){}
		
		protected String makeVideoSubTag(String which, String value){
			return "<video:" + which + ">" + value + "</video:" + which + ">\n";
		}
		
		protected String makeVideoSubTag(String which, String value, Boolean cdata){
			return "<video:" + which + "><![CDATA[" + value + "]]></video:" + which + ">\n";
		}
		
		public String getTitle() {
			return this.makeVideoSubTag("title", title, true);
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return this.makeVideoSubTag("description", description, true);
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getContentLocation() {
			return this.makeVideoSubTag("content_loc", contentLocation);
		}

		public void setContentLocation(String contentLocation) {
			this.contentLocation = contentLocation;
		}

		public String getPlayerLocation() {
			return this.makeVideoSubTag("player_loc", playerLocation);
		}

		public void setPlayerLocation(String playerLocation) {
			this.playerLocation = playerLocation;
		}

		public String getDuration() {
			return this.makeVideoSubTag("duration", duration);
		}

		public void setDuration(String duration) {
			this.duration = duration;
		}

		public String getExpirationDate() {
			return this.makeVideoSubTag("expirationDate", expirationDate);
		}

		public void setExpirationDate(String expirationDate) {
			this.expirationDate = expirationDate;
		}

		public String getRating() {
			return rating;
		}

		public void setRating(String rating) {
			this.rating = rating;
		}

		public String getViewCount() {
			return viewCount;
		}

		public void setViewCount(String viewCount) {
			this.viewCount = viewCount;
		}

		public String getPublicationDate() {
			return publicationDate;
		}

		public void setPublicationDate(String publicationDate) {
			this.publicationDate = publicationDate;
		}

		public String getFamilyFriendly() {
			return familyFriendly;
		}

		public void setFamilyFriendly(String familyFriendly) {
			this.familyFriendly = familyFriendly;
		}

		public String getRestriction() {
			return restriction;
		}

		public void setRestriction(String restriction) {
			this.restriction = restriction;
		}

		public String getRestrictionRelationship() {
			return restrictionRelationship;
		}

		public void setRestrictionRelationship(String restrictionRelationship) {
			this.restrictionRelationship = restrictionRelationship;
		}

		public String getGalleryLocation() {
			return galleryLocation;
		}

		public void setGalleryLocation(String galleryLocation) {
			this.galleryLocation = galleryLocation;
		}

		public String getPrice() {
			return price;
		}

		public void setPrice(String price) {
			this.price = price;
		}

		public String getPriceCurrency() {
			return priceCurrency;
		}

		public void setPriceCurrency(String priceCurrency) {
			this.priceCurrency = priceCurrency;
		}

		public String getRequiresSubscription() {
			return requiresSubscription;
		}

		public void setRequiresSubscription(String requiresSubscription) {
			this.requiresSubscription = requiresSubscription;
		}

		public String getUploader() {
			return uploader;
		}

		public void setUploader(String uploader) {
			this.uploader = uploader;
		}

		public String getLive() {
			return live;
		}

		public void setLive(String live) {
			this.live = live;
		}

		public String getThumbnailLocation() {
			return this.makeVideoSubTag("thumbnail_loc", thumbnailLocation, true);
		}

		public void setThumbnailLocation (String href){
			this.thumbnailLocation = href;
		}
		
		public StringBuffer drawVideoXML(){
			StringBuffer xml = new StringBuffer("<video:video>\n");
			xml.append(this.getContentLocation());
			xml.append(this.getDescription());
			xml.append(this.getTitle());
			xml.append(this.getThumbnailLocation());
			xml.append(this.getDuration());
			xml.append("</video:video>\n");
			
			return xml;
		}
	}
	
	public class BrightcoveVideoModel extends VideoModel {
		private final String brightcoveURL = "http://c.brightcove.com/services/viewer/federated_f9";
		
		private String playerID;
		private String playerKey;
		private String videoID;
		
		public BrightcoveVideoModel(String playerID, String playerKey, String videoID) {
			super();
			this.setPlayerID(playerID);
			this.setPlayerKey(playerKey);
			this.setVideoID(videoID);
		}
		public void setPlayerKey(String playerKey) {
			this.playerKey = playerKey;
		}
		public void setPlayerID(String playerID) {
			this.playerID = playerID;
		}
		public void setVideoID(String videoID) {
			this.videoID = videoID;
		}
		
		public String getContentLocation(){
			String result = this.brightcoveURL + 
					        "?playerID=" + this.playerID + 
					        "&playerKey=" + this.playerKey +
					        "&isVid=1&isUI=1" + 
					        "&videoID=" + this.videoID;
			
			return this.makeVideoSubTag("content_loc", result, true);
		}
	}

	/**
	 * @param rootPage
	 * @throws LoginException 
	 * @throws RepositoryException 
	 * @throws PathNotFoundException 
	 * @throws ValueFormatException 
	 */
	public SitemapHelper(Resource resource, Page rootPage, String defaultPriority, String defaultChangeFreq, HttpServletRequest request) throws LoginException, ValueFormatException, PathNotFoundException, RepositoryException {
		buildLinkAndChildren(resource, rootPage, 0, defaultPriority, defaultChangeFreq, request);
	}
	
	/**
	 * @param resource.getResourceResolver().getResource 
	 * @param page
	 * @param level
	 * This method generate Page and children details  
	 * @param defaultChangeFreq 
	 * @param defaultPriority 
	 * @throws RepositoryException 
	 * @throws PathNotFoundException 
	 * @throws ValueFormatException 
	 */
	private void buildLinkAndChildren(Resource resource, Page page, int level, String defaultPriority, String defaultChangeFreq, HttpServletRequest request) throws ValueFormatException, PathNotFoundException, RepositoryException {
		if(page != null) {
			Node node = resource.getResourceResolver().getResource(page.getPath()).adaptTo(Node.class).getNode("jcr:content");
			String lastmod = getW3CDTFDate(page.getLastModified().getTime()); //This date should be in W3C Datetime format
			String hideInSiteMap = node.hasProperty("hideInSiteMap") ? node.getProperty("hideInSiteMap").getString() : "false";
			String priority = node.hasProperty("priority") ? node.getProperty("priority").getString() : defaultPriority;
			String changefreq = node.hasProperty("changefreq") ? node.getProperty("changefreq").getString() : defaultChangeFreq;
			String template = node.hasProperty("cq:template") ? node.getProperty("cq:template").getString() : "";
			String path = ContextRootTransformUtil.transformedPath(page.getPath(),request);
			
			if(!hideInSiteMap.equals("true") && !template.contains("sitemap") && !path.contains("redirects") && !path.contains("references")) {
				Link crtLink = new Link(
					path,
					page.getTitle() != null ? page.getTitle() : page.getName(),
					lastmod,
					changefreq,
					priority,
					level
				);
				if(node.hasNode("link")) {
					final Node linkNode = node.getNode("link");
					NodeIterator iter2 = linkNode.getNodes();
					while(iter2.hasNext()) {
						Node linkItemNode = iter2.nextNode();
						
						if(linkItemNode.hasProperty("linkRel") && 
							linkItemNode.getProperty("linkRel").getString().equals("alternate") && 
							linkItemNode.hasProperty("linkHref") && 
							linkItemNode.hasProperty("linkHrefLang")) {
							crtLink.addAlternateHref(
								linkItemNode.getProperty("linkHrefLang").getString(),
								linkItemNode.getProperty("linkHref").getString()
							);
						}
					}
				}
				if(node.hasNode("resource")) {
					final Node resourceNode = node.getNode("resource");
					if(resourceNode.hasProperty("contentType") && resourceNode.getProperty("contentType").getString().equals("video")) {
						try {
							if(resourceNode.hasProperty("videoPlayer")) {
								Video vid = bcApi.FindVideoById(
									BC_API_TOKEN,
									Long.parseLong(resourceNode.getProperty("videoPlayer").getString()),
									EnumSet.of(
										VideoFieldEnum.LENGTH,
										VideoFieldEnum.LINKURL,
										VideoFieldEnum.THUMBNAILURL
									),
									null
								);
								crtLink.addBrightcoveVideo(
									resourceNode.getProperty("playerId").getString(),
									resourceNode.getProperty("playerKey").getString(),
									resourceNode.getProperty("videoPlayer").getString(),
									vid.getLinkUrl(),
									vid.getThumbnailUrl(),
									resourceNode.hasProperty("title")?resourceNode.getProperty("title").getString():"",
									resourceNode.hasProperty("description")?resourceNode.getProperty("description").getString():"",
									vid.getLength()/1000
								);
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (BrightcoveException e) {
							e.printStackTrace();
						}
					}
				}
				links.add(crtLink);
				Iterator<Page> children = page.listChildren(new PageFilter());
				
				while(children.hasNext()) {
					Page child = children.next();
					buildLinkAndChildren(resource, child, level+1, defaultPriority, defaultChangeFreq, request);
				}
			}
		}
	}
	
	/**
	 * @param w
	 * @param request
	 * This method generate Sitemap XML
	 * @throws RepositoryException 
	 */
	public void drawXML(Writer w , boolean hidePriority, boolean hideChangeFreq) throws IOException, RepositoryException{
		this.drawXML(w, -1, hidePriority, hideChangeFreq);
	}
	
	/**
	 * @param w
	 * @param maxlevel
	 * This method generate Sitemap XML for the pages those page level is less than provide Max page level  
	 * @throws RepositoryException 
	 */
	public void drawXML(Writer w, int maxlevel , boolean hidePriority, boolean hideChangeFreq) throws IOException, RepositoryException{
		PrintWriter out = new PrintWriter(w);
		sitemapXML = new StringBuffer(
			"<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " +
			"xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\" " +
			"xmlns:bc=\"http://www.brightcove.com\" " +
			"xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n"
		);

		for(Link aLink: links) {
			if(maxlevel != -1 && aLink.getLevel() > maxlevel) continue; 
			sitemapXML.append("<url>\n");
			if(aLink.getPath() != null) {
				sitemapXML.append("<loc>" + aLink.getPath() + "</loc>\n");
			}
			if(aLink.getLastmod() != null) {
				sitemapXML.append("<lastmod>" + aLink.getLastmod() + "</lastmod>\n");
			}
			if(!hideChangeFreq) {
				sitemapXML.append("<changefreq>" + aLink.getChangefreq() + "</changefreq>\n");
			}
			if(!hidePriority) {
				sitemapXML.append("<priority>" + aLink.getPriority() + "</priority>\n");
			}
			if(aLink.hasAlternateHrefs()) {
				sitemapXML.append(aLink.drawAlternateHrefXML());
			}
			if(aLink.hasVideos()) {
				sitemapXML.append(aLink.drawVideoXML());
			}
			sitemapXML.append("</url>\n");
		}
		sitemapXML.append("</urlset>");
		out.print(sitemapXML.toString()); 
	}
	
	/**
	 * @param date
	 * @return W3C Datetime format String
	 */
	public static String getW3CDTFDate(Date date) {
		String str = new SimpleDateFormat(W3CDTF_FORMAT).format(date);
		str = str.substring(0, str.length() - 2) + ":" + str.substring(str.length() - 2);
		return str;
	}
	
	/**
	 * @return Links
	 */
	public LinkedList<Link> getLinks() {
		return links;
	}
}