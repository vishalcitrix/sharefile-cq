package com.citrixosd.tags;

import java.util.Iterator;
import java.util.Set;

import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefineObjectsTag extends com.day.cq.wcm.tags.DefineObjectsTag {
	private static final long serialVersionUID = 1L;

	protected static Logger log = LoggerFactory.getLogger(DefineObjectsTag.class);
	
	private static final String DEV_SERVER = "dev";
	private static final String QA_SERVER = "ed1";
	private static final String STAGE_SERVER = "stage";
	private static final String PRODUCTION_SERVER = "live";
	
	public int doEndTag() {
		int retVal = super.doEndTag();
		
		if (pageContext.getAttribute("CitrixObjectsDefined") == null) {
			SlingBindings bindings = (SlingBindings)pageContext.getRequest().getAttribute(SlingBindings.class.getName());
			SlingScriptHelper sling = bindings.getSling();
			SlingSettingsService settings = sling.getService(SlingSettingsService.class);
			Set<String> currentRunModes = settings.getRunModes();

			boolean isDev = false;
			boolean isQA = false;
			boolean isStage = false;
			boolean isProd = false;

			if (currentRunModes.contains(DEV_SERVER)) {
				isDev = true;
			}
			if (currentRunModes.contains(QA_SERVER)) {
				isQA = true;
			}
			if (currentRunModes.contains(STAGE_SERVER)) {
				isStage = true;
			}
			if (currentRunModes.contains(PRODUCTION_SERVER)) {
				isProd = true;
			}

			pageContext.setAttribute("isDev", isDev);
			pageContext.setAttribute("isQA", isQA);
			pageContext.setAttribute("isStage", isStage);
			pageContext.setAttribute("isProd", isProd);			
			pageContext.setAttribute("CitrixObjectsDefined", "true");
		}

		return retVal;
    }
}