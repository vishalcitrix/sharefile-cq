package com.citrixosd.service.sms;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.citrixosd.service.sms.SmsConstants;
import com.citrixosd.service.sms.telesign.phoneid.PhoneId;
import com.citrixosd.service.sms.telesign.verify.Verify;
import com.citrixosd.service.sms.telesign.verify.response.VerifyResponse;

/**
 * Class is used to send SMS to mobile and return status message if
 * the message was delivered successfully or if failed. It also stops user to send SMS more
 * than 3 times within 2 hour period.
 */

@SlingServlet(paths = "/bin/citrix/smsSender", methods = "GET")
@Properties({ @Property(name = "service.description", value = "Sms Sender Servlet") })

public class SmsSender extends SlingSafeMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static VerifyResponse verificationResponse = null;
	private static PhoneId phoneID = null;
	private static Verify verification = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(SmsSender.class);

	private static void returnJsonResponse(SlingHttpServletResponse response, String userBlocked, boolean smsStatus) throws JSONException {
		response.setContentType("text/html");
		try {
			JSONObject json = new JSONObject();
			json.put("status", smsStatus);
			json.put("isUserBlocked", userBlocked);
			LOGGER.info("Message JSON  " + json.toString());
			response.getWriter().write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean sendSMS(String phoneNumber, String messageText) {
		phoneID = new PhoneId(SmsConstants.CUSTOMER_ID,SmsConstants.GENERATE_KEY);
		verification = new Verify(SmsConstants.CUSTOMER_ID,SmsConstants.GENERATE_KEY);
		
		LOGGER.info("PhoneNumber" + phoneID.standard(phoneNumber));
		
		if(phoneNumber != null && messageText != null) {
			verificationResponse = verification.sms(phoneNumber, "", "",messageText);
		}
		
		LOGGER.info("VerificationResponse" + verificationResponse);

		if (verificationResponse.status.code == 290){
			LOGGER.info("Message Delivered Successfully "+ verificationResponse.status.code);
			return true;
		}else {
			LOGGER.info("Message Not Delivered "+ verificationResponse.status.code);
			return false;
		}
	}
	
	private HttpSession getOldSession(String ip) {
		HttpSession httpSession = null;
		ServletContext ctx = getServletContext();
		if(ctx != null) {
			@SuppressWarnings("unchecked")
			Map<String, HttpSession> userIPsessions = (Map<String, HttpSession>) ctx.getAttribute(SmsConstants.IP_SESSION_MAP);
			if(userIPsessions != null) {
				httpSession = userIPsessions.get(ip);
				if(httpSession != null) {
					LOGGER.info("Session found in servlet context for ip: " + ip + " and httpSession value is: " + httpSession);
				}else {
					LOGGER.info("Session not found in servlet context for ip: " + ip + " and httpSession value is null");
				}
			}else {
				LOGGER.info("Session not found in servlet context for ip: " + ip + " and session map empty");
			}
		}
		return httpSession;
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		final String phoneNumber = request.getParameter("phoneNumber");
		final String messageText = request.getParameter("MessageText");
		final String clientIP = request.getParameter("ipAddress");
		
		if(clientIP != null) {
			boolean smsStatus = false;
			session = getOldSession(clientIP);
			if(session == null) {
				session = request.getSession();
				session.setAttribute(SmsConstants.USER_IP_KEY, clientIP);
				session.setAttribute(SmsConstants.VISIT_COUNT_KEY, 1);
				LOGGER.info("Old session not found so creating a new session for IP  : "+ clientIP);
			}else if(session.getAttribute(SmsConstants.USER_IP_KEY) == null) {
				session.setAttribute(SmsConstants.USER_IP_KEY, clientIP);
				session.setAttribute(SmsConstants.VISIT_COUNT_KEY, 1);
				LOGGER.info("Old session found, but attributes not set");
			}else {
				LOGGER.info("Old session found for IP: "+ (String) session.getAttribute(SmsConstants.USER_IP_KEY));
			}

			Integer visitCount = (Integer) session.getAttribute(SmsConstants.VISIT_COUNT_KEY);
			
			LOGGER.info("Visit counts: " + visitCount);
			LOGGER.info("Session Value: " + session.toString());
			
			if(visitCount > SmsConstants.USER_MAX_ATTEMPT) {
				LOGGER.info("Calculating visit counts" + visitCount);
				session.setMaxInactiveInterval(SmsConstants.USER_BLOCK_PERIOD_SECONDS);
				try {
					returnJsonResponse(response,SmsConstants.USER_BLOCKED_JSON, smsStatus);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return;
			}else if(phoneNumber != null && !phoneNumber.isEmpty() && !phoneNumber.trim().isEmpty()) {
				LOGGER.info("IP found and Phone no. not empty");
				smsStatus = sendSMS(phoneNumber, messageText);
				session.setAttribute(SmsConstants.VISIT_COUNT_KEY,visitCount + 1);
				LOGGER.info("Visit count now is: " + (Integer) session.getAttribute(SmsConstants.VISIT_COUNT_KEY));
				try {
					returnJsonResponse(response,SmsConstants.USER_NOT_BLOCKED_JSON,smsStatus);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return;
			}
		}else if(phoneNumber != null && messageText != null) {
			LOGGER.info("IP not found");
			sendSMS(phoneNumber, messageText);
		}else {
			try {
				returnJsonResponse(response,"IP address or Message Text is null", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}