package com.citrixosd.service.filesActivator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.nodetype.NodeType;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * Listens to page activation and then activates sitemap, css, js etc.
 * Templates should have mix:replicateDefault
 * author: vishal.gupta@citrix.com
 */

//@Component(metatype = false, immediate = true)
//@Service(value={EventHandler.class})
//@Property(name="event.topics",value= {ReplicationAction.EVENT_TOPIC})

public class activateMixinReplicateDefault implements EventHandler {
	private static final Logger log = LoggerFactory.getLogger(activateMixinReplicateDefault.class);
	
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	@Reference
    private Replicator replicator;
	
	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub
		//String n[] = event.getPropertyNames();
		//log.info("Event properties: ");
		//for(String s : n) {
			//log.info(s + " = " + event.getProperty(s));
		//}

		ReplicationAction action = ReplicationAction.fromEvent(event);

		if(action.getType().getName().equals("Activate")) {
			log.info("Replication occured on {} ", action.getPath());
			ResourceResolver resourceResolver;
			try {
				resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
				final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
				
				Page currPage = pageManager.getPage(action.getPath());
				Resource res = currPage != null ? currPage.hasChild("jcr:content") ? resourceResolver.getResource(resourceResolver.getResource(currPage.getPath()).getChild("jcr:content").getPath()) : null : action.getPath().endsWith("jcr:content") ? resourceResolver.getResource(action.getPath()) : null;
				
				if(res != null) {
					boolean hasReplicateDefault = false;
					final Node currJcrNode = res.adaptTo(Node.class);
					currPage = pageManager.getPage(currJcrNode.getParent().getPath());
					
					final Page parentPage = currPage != null ? currPage.getAbsoluteParent(2) : null;
					res = parentPage != null ? resourceResolver.getResource(parentPage.getPath()).getChild("jcr:content") : null;
					final Node parentJcrNode = parentPage != null ? res.adaptTo(Node.class) : null;
					
					final NodeType[] mixinNodeTypes = currJcrNode.getMixinNodeTypes();
					
					for(NodeType mixinNodeType : mixinNodeTypes) {
						if(mixinNodeType.getName().equals("mix:replicateDefault")) {
							hasReplicateDefault = true;
							break;
						}
					}
					
					log.info("Parent node is: " + parentJcrNode.getPath());
					
					if(parentJcrNode != null && !hasReplicateDefault) {						
						final Session session = res.getResourceResolver().adaptTo(Session.class);
				    	final QueryBuilder builder = res.getResourceResolver().adaptTo(QueryBuilder.class);
				    	final Map<String, String> map = new HashMap<String, String>();
				    	
				        map.put("path",parentPage.getPath());
				        map.put("type","cq:Page");
				        
				        map.put("1_property", "jcr:content/jcr:mixinTypes");
				        map.put("1_property.value", "mix:replicateDefault");
				        
				        map.put("p.limit", "-1");
				        
				        final Query query = builder.createQuery(PredicateGroup.create(map), session);
				        final SearchResult searchResult = query.getResult();
				        final Iterator<Node> nodeList = searchResult.getNodes();
				        if(searchResult.getTotalMatches() > 0) {
				        	while(nodeList.hasNext()) {
				        		Node tempNode = nodeList.next();
				        		log.info("Page Replcicated: {} ", tempNode.getNode("jcr:content").getProperty("jcr:title").getString());
				        		replicator.replicate(session, ReplicationActionType.ACTIVATE, tempNode.getNode("jcr:content").getPath());
				        	}
				        }
					}
				}
				resourceResolver.close();
			} catch (LoginException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PathNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ReplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}