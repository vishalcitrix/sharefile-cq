package com.citrixosd.service.rssfeed;

import java.io.IOException;
import java.io.StringReader; 

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.citrixosd.enums.RssFeedConstants;
import com.citrixosd.models.RssFeedChannel;
import com.citrixosd.models.RssFeedItem;
import com.citrixosd.models.RssFeedItemCollection;

/**
 * RSSFeedHandler parse XML data using SAX parser . This service will give the
 * information about channel and items present in RSS XML as POJO
 * 
 * 
 * @author sugupta
 * 
 */
public class RssFeedHandler extends DefaultHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RssFeedHandler.class);
	private RssFeedChannel channel;

	/**
	 * @return the channel
	 */
	public RssFeedChannel getChannel() {
		return channel;
	}

	/**
	 * @param channel
	 *            the channel to set
	 */
	public void setChannel(RssFeedChannel channel) {
		this.channel = channel;
	}

	/**
	 * @return the items
	 */
	public RssFeedItemCollection getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(RssFeedItemCollection items) {
		this.items = items;
	}

	private RssFeedItemCollection items;
	private RssFeedItem item;
	private boolean inItem = false;

	private StringBuilder content;

	public RssFeedHandler() {
		items = new RssFeedItemCollection();
		content = new StringBuilder();
	}

	/**
	 * This method will parse XML using SAX parser API
	 * 
	 * @param xml
	 *            - XML string to hold XML data
	 * 
	 */
	public void parse(String xml) {

		SAXParserFactory spf = null;
		SAXParser sp = null;

		try {

			spf = SAXParserFactory.newInstance();

			if (spf != null) {
				sp = spf.newSAXParser();
				sp.parse(new InputSource(new StringReader(xml)), this);

			}
		} catch (ParserConfigurationException e) {

			
			LOGGER.info("There is ParserConfig error during rss xml parsing process", e);

		} catch (SAXException e) {

			
			LOGGER.info("There is SAXException : xml not well formed", e);

		} catch (IOException e) {

			
			LOGGER.info("IO error : during RSS XML parsing", e);

		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		content = new StringBuilder();
		if (qName.equalsIgnoreCase(RssFeedConstants.CHANNEL.getString())) {
			channel = new RssFeedChannel();
		} else if (qName.equalsIgnoreCase(RssFeedConstants.ITEM.getString())) {
			inItem = true;
			item = new RssFeedItem();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase(RssFeedConstants.TITLE.getString())) {
			if (inItem) {
				item.setTitle(content.toString());
			} else {
				channel.setTitle(content.toString());
			}
		} else if (qName.equalsIgnoreCase(RssFeedConstants.LINK.getString())) {
			if (inItem) {
				item.setLink(content.toString());
			} else {
				channel.setLink(content.toString());
			}
		} else if (qName.equalsIgnoreCase(RssFeedConstants.PUBDATE.getString())) {
			if (inItem) {
				item.setPubDate(content.toString());
			} else {
				//do nothing channel does not have pubdate
			}
		} else if (qName.equalsIgnoreCase(RssFeedConstants.DESCRIPTION.getString())) {
			if (inItem) {
				item.setDescription(content.toString());
			} else {
				channel.setDescription(content.toString());
			}
        } else if(qName.equalsIgnoreCase(RssFeedConstants.CONTENT_ENCODED.getString())) {
            if (inItem) {
                item.setContent(content.toString());
            }
		} else if (qName.equalsIgnoreCase(RssFeedConstants.LASTBUILDDATE.getString())) {
			channel.setLastBuildDate(content.toString());
		} else if (qName.equalsIgnoreCase(RssFeedConstants.DOCS.getString())) {
			channel.setDocs(content.toString());
		} else if (qName.equalsIgnoreCase(RssFeedConstants.LANGUAGE.getString())) {
			channel.setLanguage(content.toString());
		} else if (qName.equalsIgnoreCase(RssFeedConstants.ITEM.getString())) {
			inItem = false;
			items.add(item);
		} else if (qName.equalsIgnoreCase(RssFeedConstants.CHANNEL.getString())) {
			channel.setItems(items);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		content.append(ch, start, length);
	}

}
