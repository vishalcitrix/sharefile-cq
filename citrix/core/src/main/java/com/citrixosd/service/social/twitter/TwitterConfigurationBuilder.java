package com.citrixosd.service.social.twitter;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * TwitterConfigurationBuilder is doing required configuration to call Twitter4J API
 * It requires twitter auth credentials  
 * it creates a twitter instance 
 * 
 * @author sugupta
 * 
 */
public class TwitterConfigurationBuilder {
    private ConfigurationBuilder cb; 
    private Twitter twitter;
    
    public TwitterConfigurationBuilder( String ConsumerKey, String ConsumerSecret,String  AccessToken ,String AccessTokenSecret ) {
        cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey(ConsumerKey)
          .setOAuthConsumerSecret(ConsumerSecret)
          .setOAuthAccessToken(AccessToken)
          .setOAuthAccessTokenSecret(AccessTokenSecret);
        TwitterFactory tf = new TwitterFactory(cb.build());
        setTwitter(tf.getInstance());
      }

    /**
	 * @return the twitter
	 */
	public Twitter getTwitter() {
		
		
		return twitter;
	}
	/**
	 * @param twitter
	 *            the twitter to set
	 */
	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}
}