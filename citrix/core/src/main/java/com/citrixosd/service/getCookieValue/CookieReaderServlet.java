package com.citrixosd.service.getCookieValue;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sling Servlet to get cookie value
 * @author sugupta
 * @author krishna.selvaraj@citrix.com
 */

@SlingServlet(paths = "/bin/citrix/getCookieValue", methods = "GET")
@Properties({ @Property(name = "service.description", value = "This sling servlet provides http cookie value  when cookie name param : 'q' is provided") })
public class CookieReaderServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = -697572759745495182L;
	private static Logger LOGGER = LoggerFactory.getLogger(CookieReaderServlet.class);

	protected void doGet(SlingHttpServletRequest request,SlingHttpServletResponse response) throws ServletException,IOException {
		handleRequest(request, response);
	}

	protected void doPost(SlingHttpServletRequest request,SlingHttpServletResponse response) throws ServletException,IOException {
		handleRequest(request, response);
	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */

	private void handleRequest(SlingHttpServletRequest request,SlingHttpServletResponse response) throws IOException,ServletException {
		java.io.PrintWriter out = response.getWriter();
		try {
			String cookieValue = null;
			
			//Get an array of Cookies associated with this domain
			Cookie[] cookies = request.getCookies();
			boolean setCookie = false;

			final String requested_cookie = request.getParameter("cookieName");
			response.setContentType("text/html");
			
			if(requested_cookie != null && !requested_cookie.trim().isEmpty() && cookies.length > 0) {
				for(Cookie cookieTemp : cookies) {					
					if (cookieTemp.getName().equals(requested_cookie)) {
						cookieValue = cookieTemp.getValue();
						response.getWriter().print(cookieValue);
						setCookie = true;
						LOGGER.info("Cookie found in the request : Cookie Name :"+ requested_cookie +"   Cookie Value : "+ cookieValue);
						break;
					}
				}

				if(!setCookie) {
					response.getWriter().print("noCookieSet");
				}
			}else {
				response.getWriter().print("noCookieSet");
			}

		} catch (Exception e) {
			LOGGER.info("Error : There is problem while reading cookies  :" + e.getStackTrace());
		} finally {
			if(out != null)
				out.close();
		}
	}
}