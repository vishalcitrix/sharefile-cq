/**
 * 
 */
package com.citrixosd.service.rssfeed;

import javax.ws.rs.core.MediaType;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citrixosd.models.RssFeedChannel;
import com.citrixosd.service.rssfeed.RssFeedReader;
import com.citrixosd.service.trustedHostJerseyClient.JerseyClientHelper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * One implementation of the {@link RssFeedReader}.
 * @author sugupta
 */

@Service(value = RssFeedReader.class)
@Component(metatype = true, immediate = true)
public class RssFeedReaderImpl implements RssFeedReader{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RssFeedReaderImpl.class);
	/**
	 * This method will Read XMLusing Jersey API
	 * 
	 * @param url
	 *            - URL of RSS XML
	 * 
	 */
	public String getFeedXML(String url) {
		LOGGER.info("provided RSS URL ", url);
		String responseString = null;
		try {
			// using jersey
			//Client client = Client.create();
			Client client =JerseyClientHelper.createClient();
			WebResource resource = client.resource(url.toString());
			ClientResponse clientResponse = resource.accept(
					MediaType.APPLICATION_XML_TYPE).get(ClientResponse.class);
			if (clientResponse.getStatus() == 200) {
				// jersey API returns string as response
				responseString = clientResponse.getEntity(String.class);
			}
		} catch (Exception e) {
			LOGGER.info("There is a problem to read RSS XML using URL", e);
		}
		return responseString;

	}
    @Override
	public RssFeedChannel getChannel(String url) {
		RssFeedChannel ch = null;
		String xml = this.getFeedXML(url);
		ch = this.populateChannel(xml);

		return ch;
	}
    /**
	 * This method will call custom RSSFeedHandler to parse XML and return channel information as POJO . 
	 * @param XML -  XML String
	 * @return RSS channel channel
	 */
	public RssFeedChannel populateChannel(String XML) {

		RssFeedHandler customeHandler= new RssFeedHandler();
		customeHandler.parse(XML);
		RssFeedChannel ch = customeHandler.getChannel();
		return ch;

	}
}
