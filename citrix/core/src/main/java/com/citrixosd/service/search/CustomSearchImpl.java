package com.citrixosd.service.search;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citrixosd.utils.ContextRootTransformUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * One implementation of the {@link CustomSearch}.
 * @author sugupta,vgupta
 */

@Component(metatype = true, immediate = true)
@Service(value = CustomSearch.class)

public class CustomSearchImpl implements CustomSearch {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomSearchImpl.class);

	private static final String QUERY_PARAM_NAME = "q";
	private static final String SEARCH_DIR = "dir";
	private static final String OFFSET = "offset";
	private static final String RESULTS_PER_PAGE = "resultsPerPage";
	private static final String GET_ALL_RESULT_HITS = "-1";

	private String query;
	private long  totalMachesPagesCount;
	private long activeResultPageIndex=1;

	@Reference
	QueryBuilder qbuilder;

	@Override
	public SearchResult search(SlingHttpServletRequest request) {
		// TODO Auto-generated method stub
		SearchResult result = executeSearchQuery(request);
		LOGGER.info("Executing Search Query for: " + this.getQuery());
		return result;
	}

	@Override
	public String getSearchResultHTML(SlingHttpServletRequest request) {
		// execute query
		SearchResult result = executeSearchQuery(request);
		LOGGER.info("Executing Search Query for :" + this.getQuery());
		this.setTotalMachesPagesCount(request,result);
		return writeHTMLsearchResult(request,result);
	}

	private SearchResult executeSearchQuery(SlingHttpServletRequest request) {
		SearchResult result = null;
		Map <String, String> map = setSearchProperty(request);
		if(map != null && !map.isEmpty()) {
			Session session = request.getResource().getResourceResolver().adaptTo(Session.class);
			Query query = qbuilder.createQuery(PredicateGroup.create(map),session);
			result = query.getResult();
			LOGGER.info("Query Executed :" + result.getQueryStatement());
			LOGGER.info("Total maches found for query  :" + result.getTotalMatches());
			LOGGER.info("Time(ms) taken by serch Query :" + result.getExecutionTimeMillis());
		}
		return result;
	}

	private Map <String,String> setSearchProperty(SlingHttpServletRequest request) {
		final String fulltextSearchTermRaw = StringEscapeUtils.escapeHtml(request.getParameter(QUERY_PARAM_NAME));
		String fulltextSearchTerm = decodeURLParam(fulltextSearchTermRaw);
		final String offset = request.getParameter(OFFSET);
		final String searchdir = (String) request.getAttribute(SEARCH_DIR);
		LOGGER.info("Search Directory info comming from request:" + searchdir);
		final String resultsPerPage= request.getAttribute(RESULTS_PER_PAGE) !=null ? ((String) request.getAttribute(RESULTS_PER_PAGE)).trim(): GET_ALL_RESULT_HITS;
		LOGGER.info("Maximun results per page info comming from request :" + resultsPerPage);
		//get current page index
		this.setActiveResultPageIndex(offset, resultsPerPage);

		//prevent wild card search
		if(fulltextSearchTerm == null || fulltextSearchTerm.trim().isEmpty() || fulltextSearchTerm.trim().startsWith("*") || fulltextSearchTerm.trim().endsWith("*")) {
			return null;
		}

		this.setQuery(fulltextSearchTerm);

		LOGGER.info("Executing Search Query under directory: " + searchdir);

		//create query description as hash map (simplest way, same as form post)
		Map <String,String> map = new HashMap <String,String>();
		map.put("path",searchdir);
		map.put("path.self","true");
		map.put("type","cq:Page");
		map.put("group.p.or","true");

		//search jcr:title with query string
		map.put("group.1_property", "fn:lower-case(@jcr:content/jcr:title)");
		map.put("group.1_property.value", "%" + fulltextSearchTerm.toLowerCase() + "%");
		map.put("group.1_property.operation", "like");

		//search jcr:decription with query string
		map.put("group.2_property","fn:lower-case(@jcr:content/jcr:description)");
		map.put("group.2_property.value", "%" + fulltextSearchTerm.toLowerCase() + "%");
		map.put("group.2_property.operation", "like");

		//can be done in map or with Query methods
		map.put("p.offset",offset);
		map.put("p.limit",resultsPerPage);
		map.put("orderby","@jcr:content/cq:lastModified");
		map.put("orderby.sort","desc");
		return map;
	}

	private String writeHTMLsearchResult(SlingHttpServletRequest request,SearchResult result) {
		StringBuffer sb = new StringBuffer();
		final String fulltextSearchTermRaw = StringEscapeUtils.escapeHtml(request.getParameter(QUERY_PARAM_NAME));
		String fulltextSearchTerm = decodeURLParam(fulltextSearchTermRaw);
		long totalMatches = result.getTotalMatches();
		String resultsPerPage = request.getAttribute(RESULTS_PER_PAGE) != null ? ((String) request.getAttribute(RESULTS_PER_PAGE)).trim(): GET_ALL_RESULT_HITS;
		int resultsPerPage_count = Integer.parseInt(resultsPerPage);
		String searchMetaInfo=this.getSearchResutMetaInfo(resultsPerPage_count, totalMatches);
		if(totalMatches > 0) {
			sb.append("<div class='searchMeta'>"+searchMetaInfo+"</div>");
			sb.append("<div class='resultFound'>Results for '" + fulltextSearchTerm + "'</div>");
			sb.append("<ul class='searchResult'>");
			for(Hit hit: result.getHits()) {
				try {
					final String path = hit.getPath();
                    final String pageJCRTitle = hit.getTitle();
                    final ValueMap properties = hit.getProperties();
                    final String desc = properties.get("jcr:description",String.class) != null ? properties.get("jcr:description",String.class) : "";
					final String siteURLforPage = ContextRootTransformUtil.transformedPath(path, request);
					final String linkOfpage = "<li><div class='serch-link'><a href='" + siteURLforPage + "?q=" + fulltextSearchTermRaw + "&source=search'>" + pageJCRTitle + " </a></div><p class='link-teaser'>" + desc + "</p></li>";
					sb.append(linkOfpage);
				} catch (RepositoryException e) {
					LOGGER.info("There is some issue while creating search html :" + e.getStackTrace());
				}
			}
			sb.append("</ul>");
		} else {
			sb.append("<div class='searchMeta'>RESULTS 0</div>");
			sb.append("<div class='resultFound'>Sorry no results for '<span>" + fulltextSearchTermRaw + "</span>' were found.</div>");
			sb.append("<div class='searchSuggestions'>Search suggestions:");
				sb.append("<span>Check your spelling</span>");
				sb.append("<span>Try more general words</span>");
				sb.append("<span>Try different words that mean the same thing</span>");
			sb.append("</div>");
		}
		return sb.toString();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * @return the totalMachesPagesCount
	 */
	@Override
	public long getTotalMachesPagesCount() {
		return totalMachesPagesCount;
	}

	/**
	 * @param totalMachesPagesCount the totalMachesPagesCount to set
	 */
	public void setTotalMachesPagesCount(SlingHttpServletRequest request,SearchResult result) {
		String resultsPerPage = request.getAttribute(RESULTS_PER_PAGE) != null ? ((String) request.getAttribute(RESULTS_PER_PAGE)).trim(): GET_ALL_RESULT_HITS;
		if (result != null && resultsPerPage != null && !resultsPerPage.equalsIgnoreCase(GET_ALL_RESULT_HITS)) {
			int resultsPerPage_count = Integer.parseInt(resultsPerPage);
			if (resultsPerPage_count != 0 && resultsPerPage_count > 0) {
				int maches = (int) result.getTotalMatches() / resultsPerPage_count;
				float fraction = result.getTotalMatches() % resultsPerPage_count;
				if (fraction > 0)maches = maches + 1;
				this.totalMachesPagesCount = maches;
			} else {
				this.totalMachesPagesCount = 1; // all the result in single page
			}

		} else {
			this.totalMachesPagesCount = 1; // all the result in single page
		}
	}

	/**
	 * @return the activeResultPageIndex
	 */
	public long getActiveResultPageIndex() {
		return activeResultPageIndex;
	}

	/**
	 * @param activeResultPageIndex the activeResultPageIndex to set
	 */
	public void setActiveResultPageIndex(String currentOffset,String resultsPerPage) {
		if(currentOffset != null && resultsPerPage != null) {
			try {
				int currentOffsetInt = Integer.parseInt(currentOffset);
				int resultsPerPageInt = Integer.parseInt(resultsPerPage);
				if(resultsPerPageInt > 0) {
					activeResultPageIndex=currentOffsetInt/resultsPerPageInt;
				}
			} catch (Exception e) {
				LOGGER.info("There is some issue while setting  ActiveResultPageIndex:" + e.getStackTrace());
			}
		}
	}

	public String getSearchResutMetaInfo(int resultsPerPage_count ,long totalMatches) {
		String Info = null;
		if(resultsPerPage_count > 0) {
			long currentPageIndex = this.getActiveResultPageIndex();
			long resultStartPage = activeResultPageIndex * resultsPerPage_count + 1;
			long resultEndPage = currentPageIndex * resultsPerPage_count + resultsPerPage_count;
			resultEndPage = resultEndPage > totalMatches ? totalMatches : resultEndPage;//check for max limit
			Info= "Results " + resultStartPage +"-" + resultEndPage + " of "+ totalMatches;
		}
		else{
			Info= "Results 1-" + totalMatches + " of "+ totalMatches;
		}
		return Info;
	}
	static String decodeURLParam(String param)
	   {
	     try
	     {
	       return URLDecoder.decode(param, "UTF-8");
	     }
	     catch (UnsupportedEncodingException e) {}
	     return param;
	   }

}