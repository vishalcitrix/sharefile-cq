package com.citrixosd.service.social.twitter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * TwitterHelper uses TwitterConfigurationBuilder to get twitter instance it
 * returns tweets data
 * 
 * @author sugupta
 * 
 */
public class TwitterHelper {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TwitterHelper.class);

	private TwitterConfigurationBuilder twitterConfigurationBuilder = null;

	/**
	 * it creates required TwitterConfigurationBuilder instance
	 * 
	 * @param ConsumerKey
	 * @param ConsumerSecret
	 * @param AccessToken
	 * @param AccessTokenSecret
	 */
	public void initTwitterCredentialsConfig(String ConsumerKey,
			String ConsumerSecret, String AccessToken, String AccessTokenSecret) {
		if (null != ConsumerKey && null != ConsumerSecret
				&& null != AccessToken && null != AccessTokenSecret)
			twitterConfigurationBuilder = new TwitterConfigurationBuilder(
					ConsumerKey, ConsumerSecret, AccessToken, AccessTokenSecret);
	}

	/**
	 * it creates required TwitterConfigurationBuilder instance
	 * @param AccountType
	 */
	public void initTwitterCredentialsConfig(String AccountType) {

		if (null != AccountType) {

			if (AccountType.trim().equalsIgnoreCase(
					TwitterFeedConstants.G2M.getString())) {
				twitterConfigurationBuilder = new TwitterConfigurationBuilder(
						TwitterFeedConstants.G2M_CONSUMERKEY.getString(),
						TwitterFeedConstants.G2M_CONSUMERSECRET.getString(),
						TwitterFeedConstants.G2M_ACCESSTOKEN.getString(),
						TwitterFeedConstants.G2M_ACCESSTOKENSECRET.getString());
			} else if (AccountType.trim().equalsIgnoreCase(
					TwitterFeedConstants.G2W.getString())) {
				twitterConfigurationBuilder = new TwitterConfigurationBuilder(
						TwitterFeedConstants.G2W_CONSUMERKEY.getString(),
						TwitterFeedConstants.G2W_CONSUMERSECRET.getString(),
						TwitterFeedConstants.G2W_ACCESSTOKEN.getString(),
						TwitterFeedConstants.G2W_ACCESSTOKENSECRET.getString());
			} else if (AccountType.trim().equalsIgnoreCase(
					TwitterFeedConstants.G2T.getString())) {
				twitterConfigurationBuilder = new TwitterConfigurationBuilder(
						TwitterFeedConstants.G2T_CONSUMERKEY.getString(),
						TwitterFeedConstants.G2T_CONSUMERSECRET.getString(),
						TwitterFeedConstants.G2T_ACCESSTOKEN.getString(),
						TwitterFeedConstants.G2T_ACCESSTOKENSECRET.getString());
			} else {
				return;
			}
		}
	}

	/**
	 * it reads twitter status
	 * 
	 * @return twitter Status
	 */
	public List<Status> getStatus() {

		List<Status> statuses = new ArrayList<Status>();
		try {

			if (null != twitterConfigurationBuilder) {
				Twitter twitter = twitterConfigurationBuilder.getTwitter();
				statuses = twitter.getHomeTimeline();
			}
		} catch (TwitterException e) {
			LOGGER.info("There is exception while reading tweets", e);
		}

		return statuses;

	}

	/**
	 * it parse twitter feeds status data
	 * 
	 * @param tweetLimit
	 * @return formatedStatuses - it contains formated tweets html
	 */
	public List<String> getFormatedStatus(int tweetLimit) {

		List<String> formatedStatuses = new ArrayList<String>();
		List<Status> statuses = this.getStatus();
		try {
			int count = 0;
			for (Status status : statuses) {
				if (count < tweetLimit) {
					formatedStatuses.add(this.parseFeed(status.getText()));
					count++;
				} else {
					break;
				}
			}

		} catch (Exception e) {
			LOGGER.info("There is exception while reading tweets", e);
		}

		return formatedStatuses;

	}

	private String parseFeed(String tweetText) {

		// Search for URLs
		if (null != tweetText && tweetText.contains("http:")) {
			int indexOfHttp = tweetText.indexOf("http:");
			int endPoint = (tweetText.indexOf(' ', indexOfHttp) != -1) ? tweetText
					.indexOf(' ', indexOfHttp) : tweetText.length();
			String url = tweetText.substring(indexOfHttp, endPoint);
			String targetUrlHtml = "<a href='" + url + "' target='_blank'>"
					+ url + "</a>";
			tweetText = tweetText.replace(url, targetUrlHtml);
		}

		String patternStr = "(?:\\s|\\A)[##]+([A-Za-z0-9-_]+)";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(tweetText);
		String result = "";

		// Search for Hashtags
		while (matcher.find()) {
			result = matcher.group();
			result = result.replace(" ", "");
			String search = result.replace("#", "");
			String searchHTML = "<a href='"
					+ TwitterFeedConstants.TWITTER_SERACH_URL.getString()
					+ search + "'>" + result + "</a>";
			tweetText = tweetText.replace(result, searchHTML);
		}

		// Search for Users
		patternStr = "(?:\\s|\\A)[@]+([A-Za-z0-9-_]+)";
		pattern = Pattern.compile(patternStr);
		matcher = pattern.matcher(tweetText);
		while (matcher.find()) {
			result = matcher.group();
			result = result.replace(" ", "");
			String rawName = result.replace("@", "").trim();
			String userHTML = "<a href='"
					+ TwitterFeedConstants.TWITTER_URL.getString() + rawName
					+ "'>" + rawName + "</a>";
			tweetText = tweetText.replace(result, userHTML);
		}
		return tweetText;
	}

}
