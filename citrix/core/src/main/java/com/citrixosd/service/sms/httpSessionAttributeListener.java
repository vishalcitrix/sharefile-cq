package com.citrixosd.service.sms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext; 

import com.citrixosd.service.sms.SmsConstants;

@Component(immediate = true)
@Properties({ @Property(name = "service.description", value = "This is http session listener which store session based on user IP and this IP detail should be present in created session.") })
@Service
public class httpSessionAttributeListener implements HttpSessionAttributeListener {
	private static  Map<String, HttpSession> userIPsessions = new HashMap<String, HttpSession>();
	
	ServletContext ctx = null; 

	@Activate
	protected void activate(final ComponentContext context) {
		userIPsessions = new HashMap<String, HttpSession>();
	}

	@Deactivate
	protected void deactivate() {
		userIPsessions = null;
	}

	@Override
	public void attributeAdded(HttpSessionBindingEvent  httpSessionEvent) {
		HttpSession session = httpSessionEvent.getSession();
		if(session != null && session.getAttribute(SmsConstants.USER_IP_KEY) != null && userIPsessions.get(session.getAttribute(SmsConstants.USER_IP_KEY)) == null) {
			userIPsessions.put((String)session.getAttribute(SmsConstants.USER_IP_KEY), session);
			ctx = session.getServletContext();
			if(ctx != null)
				ctx.setAttribute(SmsConstants.IP_SESSION_MAP, userIPsessions);
		}
		System.out.println("Uesr IP added in the session map");
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent httpSessionEvent) {
		HttpSession session = httpSessionEvent.getSession();
		if(session != null && session.getAttribute(SmsConstants.USER_IP_KEY) != null) {
			userIPsessions.remove((String)session.getAttribute(SmsConstants.USER_IP_KEY));
			ctx = session.getServletContext();
			if(ctx != null)
				ctx.setAttribute(SmsConstants.IP_SESSION_MAP, userIPsessions);  
		}
		System.out.println("Attribute is removed from session map");
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
	}
}