package com.citrixosd.service.sms;

public class SmsConstants {
	public static String CUSTOMER_ID  = "E83B45F1-86A7-4843-8C63-919CCD3AA25F";
	public static String GENERATE_KEY  = "W5wk/2vRFHS9ZT4jVwJ/tpwqzJOMZGHvWma7Js/KDRDxr8+V99Nvn8h2hy0dhRu/bNgK7z1S9Q2LaA57UUP3tA==";
	public static int USER_BLOCK_PERIOD_SECONDS = 7200; //used 300 for testing
	public static int USER_MAX_ATTEMPT = 3;
	public static String USER_IP_KEY = "userIP";
	public static String IP_SESSION_MAP = "IPSession";
	public static String VISIT_COUNT_KEY = "visitCountKey";
	public static String USER_BLOCKED_JSON = "true";
	public static String USER_NOT_BLOCKED_JSON  = "false";
}
