package com.citrixosd.service.rssfeed;

import com.citrixosd.models.RssFeedChannel;

/**
 * RssFeedReader Service will read RSS XML form provided URL and it will parse XML data using SAX parser  .
 * This service will give the information about channel and items present in RSS XML as POJO
 * 
 * 
 * @author sugupta
 * 
 */

public interface RssFeedReader {
	
	/**
	 * This method will get the URL of RSS XML . 
	 * @param url - RSS XML URL
	 * @return RSS channel channel
	 */
	public RssFeedChannel getChannel(String url);
}
