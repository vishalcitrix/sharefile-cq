package com.citrixosd.enums;

public enum ResourceType {
	ResourceDocument,
	ResourceWebinar,
	ResourceVideo;

	public static ResourceType getEnum(String name) {
		for (ResourceType resourceType : ResourceType.values()) {
	        if (resourceType.name().equals(name)) {
	        	return resourceType;
	        }
	    }
	    return null;
	}
}