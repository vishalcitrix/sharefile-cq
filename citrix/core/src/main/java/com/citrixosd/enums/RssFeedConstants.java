package com.citrixosd.enums;

public enum RssFeedConstants {
	
	CHANNEL("channel"),
	ITEM("item"),
	TITLE("title"),
	LINK("link"), 
	DESCRIPTION("description"),
	LASTBUILDDATE("lastBuildDate"),
	DOCS("docs"),
	PUBDATE("pubDate"),
	LANGUAGE("language"),
    CONTENT_ENCODED("content:encoded");
	

	// enum related stuff
	private String key;

	private RssFeedConstants(String key) {
		this.key = key;
	}

	public String getString() {
		return this.key;
	}

}
