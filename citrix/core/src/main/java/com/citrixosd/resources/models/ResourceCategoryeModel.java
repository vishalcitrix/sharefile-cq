package com.citrixosd.resources.models;

public class ResourceCategoryeModel {
    private String resourceType;
    private String categories;
    private String defaultLanguage;
    private boolean webinarUpcomingOnly;
    
    public ResourceCategoryeModel() {
        
    }
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }
    public String getResourceType() {
        return this.resourceType;
    }
    public void setCategories(String categories) {
        this.categories = categories;
    }
    public String getCategories() {
        return this.categories;
    }
    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
    public String getDefaultLanguage() {
        return this.defaultLanguage;
    }
    public void setUpComingOnly(boolean webinarUpcomingOnly) {
        this.webinarUpcomingOnly = webinarUpcomingOnly;
    }
    public boolean getUpComingOnly() {
        return this.webinarUpcomingOnly;
    }
}