package com.citrixosd.resources;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.jackrabbit.util.ISO8601;
import org.apache.sling.api.SlingHttpServletRequest;
import org.eclipse.jdt.internal.compiler.lookup.UpdatedMethodBinding;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.citrixosd.enums.ResourceType;
import com.citrixosd.resources.models.*;

public class ResourceUtils {
	
	public ResourceUtils() {
		
	}
	
	/**
	 * Search through the string array of tags and return the first tag. If there is no 
	 * tag in the string array or if the tag is not found, this will return null.
	 */
	public static List<Node> getAllResources(ResourceCategoryeModel resourcesSet, SlingHttpServletRequest request) {
		
		//Resolve resourceSet
		String resourceType = resourcesSet.getResourceType();
		String resourceCategory = resourcesSet.getCategories();
		String resourceLanguage = resourcesSet.getDefaultLanguage();
		boolean webinarUpcomingOnly = resourcesSet.getUpComingOnly();
		
		final Session session = request.getResourceResolver().adaptTo(Session.class);
		final QueryBuilder builder = request.getResource().getResourceResolver().adaptTo(QueryBuilder.class);
		
    	final Map<String, String> map = new HashMap<String, String>();
		map.put("path", "/content");
		map.put("type", "cq:Page");
		
		//Resource Type
		if(resourceType != null && resourceType.trim().length() > 0) {
			map.put("1_property", "jcr:content/jcr:mixinTypes");
			if(resourceType.contains(",")) {
				final String[] resourceTypes = resourceType.split(",");
				for(int i = 0; i < resourceTypes.length; i++) {
					map.put("1_property." + (i + 1) + "_value", "mix:" + resourceTypes[i]);
				}
			}else {
				map.put("1_property.value", "mix:" + resourceType);
			}
		}

		//Language
		if(resourceLanguage != null && resourceLanguage.trim().length() > 0) {
			map.put("2_property", "jcr:content/resourceLanguage");
			if(resourceLanguage.contains(",")) {
				final String[] resourceLanguages = resourceLanguage.split(",");
				for(int i = 0; i < resourceLanguages.length; i++) {
					map.put("2_property." + (i + 1) + "_value", resourceLanguages[i]);
				}
			}else {
    			map.put("2_property.value", resourceLanguage);
			}
		}
		
		//Categories
		if(resourceCategory != null && resourceCategory.trim().length() > 0) {
			map.put("3_property", "jcr:content/resourceCategories");
			if(resourceCategory.contains(",")) {
				final String[] categoriesResourceCategories = resourceCategory.split(",");
				for(int i = 0; i < categoriesResourceCategories.length; i++) {
					map.put("3_property." + (i + 1) + "_value", categoriesResourceCategories[i]);
				}
			}else {
    			map.put("3_property.value", resourceCategory);
			}
		}
		
		//Webinar upcoming events
		if(ResourceType.ResourceWebinar.equals(ResourceType.getEnum(resourceType))) {
			if(webinarUpcomingOnly) {
				final String currentDateStr = ISO8601.format(Calendar.getInstance());
				map.put("4_daterange.property", "jcr:content/resourceContainer/webinarStartDate");
				map.put("4_daterange.lowerBound", currentDateStr);
				map.put("4_daterange.lowerOperation", ">=");
			}else {
				final Calendar currentCalendar = Calendar.getInstance();
				currentCalendar.add(Calendar.DATE, -1);
				final String currentDateWithOneExtraDayStr = ISO8601.format(currentCalendar);
				map.put("4_daterange.property", "jcr:content/resourceContainer/webinarStartDate");
				map.put("4_daterange.upperBound", currentDateWithOneExtraDayStr);
				map.put("4_daterange.upperOperation", "<");

				//TODO: add brightcove video check
			}
		}
		
		//Order
		if(webinarUpcomingOnly) {
			map.put("orderby", "@jcr:content/resourceContainer/webinarStartDate");
			map.put("orderby.index", "true");
			map.put("orderby.sort", "asc");	
		}
		
		map.put("p.limit", "-1");
		
		//Execute the query
		Query query = builder.createQuery(PredicateGroup.create(map), session);
		SearchResult searchResult = query.getResult();
		
		long total =  searchResult.getTotalMatches();
		Iterator<Node> nodeIter = null;
		
		List<Node> jcrNode = new ArrayList<Node>();
		
		if(total > 0) {
			//Search through the query
    		nodeIter = searchResult.getNodes();
    		while (nodeIter.hasNext()){
    			Node currNode = nodeIter.next();
    			try {
					if(currNode.hasNode("jcr:content")) {
						Node currJcrNode = currNode.getNode("jcr:content");
						jcrNode.add(currJcrNode);
					}
				} catch (PathNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ValueFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
		}
		
		return jcrNode;
		//String txt = "defaultLanguage = " + resourceLanguage + " resourceType = " + resourceType + " resourceCategory = " + resourceCategory + " total = " + total;
	}
	
	/**
	 * @author Michael Jostmeyer
	 * @param path
	 * @param request
	 * @param upcomingOnly
	 * @return Node
	 */
	public static Node getUpcomingWebinar(String path, SlingHttpServletRequest request, boolean upcomingOnly) {
		final Session session = request.getResourceResolver().adaptTo(Session.class);
		final QueryBuilder builder = request.getResource().getResourceResolver().adaptTo(QueryBuilder.class);
		
    	final Map<String, String> map = new HashMap<String, String>();
    	map.put("1_orderby","date");
    	map.put("1_orderby.sort","asc");
    	map.put("2_path",path);
    	map.put("3_property","resourceIdentifier");
    	map.put("3_property.operation","equals");
    	map.put("3_property.value","Webinar");
    	if(upcomingOnly){
	    	map.put("4_daterange.lowerBound",ISO8601.format(Calendar.getInstance()));
	    	map.put("4_daterange.lowerOperation",">=");
	    	map.put("4_daterange.property","date");
    	}
    	map.put("p.hits","full");
    	
		Query query = builder.createQuery(PredicateGroup.create(map), session);
		SearchResult searchResult = query.getResult();
		
		Iterator<Node> allWebinars = searchResult.getNodes();
		
		if(allWebinars.hasNext()){
			Node nextWebinar = allWebinars.next();
			if(upcomingOnly){
				Node tmpWebinar;
				while(allWebinars.hasNext()){
					tmpWebinar = allWebinars.next();
					
					try {
						Calendar nextDate = nextWebinar.getProperty("date").getDate();
						Calendar tmpDate = tmpWebinar.getProperty("date").getDate();
						if(tmpDate.before(nextDate)){
							nextWebinar = tmpWebinar;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return nextWebinar;
		} else {
			return null;
		}
	}
	
	public static Node getDefaultWebinar(String path, SlingHttpServletRequest request) {
		return getUpcomingWebinar(path, request, false);
	}
}