/**
 * 
 */
package com.citrixosd.models;

import java.io.Serializable;

/**
 * This is channel model class.
 * 
 * @author sugupta
 * 
 */

public class RssFeedChannel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4870299726819340554L;
	private RssFeedItemCollection items;
	private String title;
	private String link;
	private String description;
	private String lastBuildDate;
	private String docs;
	private String language;

	public RssFeedChannel() {
		setItems(null);
		setTitle(null);
		// set every field to null in the constructor
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(RssFeedItemCollection items) {
		this.items = items;
	}

	/**
	 * @return the items
	 */
	public RssFeedItemCollection getItems() {
		return items;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the lastBuildDate
	 */
	public String getLastBuildDate() {
		return lastBuildDate;
	}

	/**
	 * @param lastBuildDate
	 *            the lastBuildDate to set
	 */
	public void setLastBuildDate(String lastBuildDate) {
		this.lastBuildDate = lastBuildDate;
	}

	/**
	 * @return the docs
	 */
	public String getDocs() {
		return docs;
	}

	/**
	 * @param docs
	 *            the docs to set
	 */
	public void setDocs(String docs) {
		this.docs = docs;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
}
