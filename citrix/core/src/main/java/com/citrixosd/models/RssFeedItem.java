package com.citrixosd.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RssFeedItem {
	private String title;
	private String description;
	private String link;
    private String content;
    private String image;
    private String pubDate;
    private Date pubDateObg;

	public RssFeedItem() {

	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
		convertPubDateObg(pubDate);
	}

	public Date getPubDateObg() {
		return pubDateObg;
	}

	public void setPubDateObg(Date pubDateObg) {
		this.pubDateObg = pubDateObg;
		
	}
	private void convertPubDateObg(String pubDateString) {
		if(pubDateString != null && !pubDateString.isEmpty()){
			DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
			try {
				Date date = formatter.parse(pubDateString);
				this.setPubDateObg(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
	}

}