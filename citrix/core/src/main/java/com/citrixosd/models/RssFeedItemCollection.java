/**
 * 
 */
package com.citrixosd.models;

import java.util.ArrayList;

/**
 * This is Items collection which hold item model class. 
 * 
 * @author sugupta
 * 
 */

public class RssFeedItemCollection extends ArrayList<RssFeedItem> {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2866241002404066494L;

	public RssFeedItemCollection() {
        super();
    }
}
