package com.citrixosd.models;

import com.citrixosd.enums.ResourceType;

public abstract class Resource {
	private String path;
	private String featured;
	private String[] categories;
	private boolean gated;
	private ResourceType resourceType;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public String getFeatured() {
		return featured;
	}

	public void setFeatured(String featured) {
		this.featured = featured;
	}

	public String[] getCategories() {
		return categories;
	}

	public void setCategories(String[] categories) {
		this.categories = categories;
	}

	public boolean isGated() {
		return gated;
	}

	public void setGated(boolean gated) {
		this.gated = gated;
	}

	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}
	
}