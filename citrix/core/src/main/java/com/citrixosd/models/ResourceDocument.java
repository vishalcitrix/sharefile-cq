package com.citrixosd.models;

public class ResourceDocument extends Resource {
	private String title;
	private String language;
	private String credit;
	private String creditPath;
	private String thumbnailPath;
	
	public ResourceDocument() {
		
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getCreditPath() {
		return creditPath;
	}

	public void setCreditPath(String creditPath) {
		this.creditPath = creditPath;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}
}