package com.citrixosd.models;

public class ResourceWebinar extends Resource {
	private String title;
	private String language;
	private String authorThumbnailPath;
	private String authorTitle;
	private String authorDescription;
	private String upcoming;
	private String upcomingTime;
	private String upcomingLong;
	private boolean registration;
	
	public ResourceWebinar() {
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getAuthorThumbnailPath() {
		return authorThumbnailPath;
	}

	public void setAuthorThumbnailPath(String authorThumbnailPath) {
		this.authorThumbnailPath = authorThumbnailPath;
	}

	public String getAuthorTitle() {
		return authorTitle;
	}

	public void setAuthorTitle(String authorTitle) {
		this.authorTitle = authorTitle;
	}

	public String getAuthorDescription() {
		return authorDescription;
	}

	public void setAuthorDescription(String authorDescription) {
		this.authorDescription = authorDescription;
	}

	public String getUpcoming() {
		return upcoming;
	}

	public void setUpcoming(String upcoming) {
		this.upcoming = upcoming;
	}

	public String getUpcomingTime() {
		return upcomingTime;
	}

	public void setUpcomingTime(String upcomingTime) {
		this.upcomingTime = upcomingTime;
	}

	public String getUpcomingLong() {
		return upcomingLong;
	}

	public void setUpcomingLong(String upcomingLong) {
		this.upcomingLong = upcomingLong;
	}

	public boolean isRegistration() {
		return registration;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}
	
}
