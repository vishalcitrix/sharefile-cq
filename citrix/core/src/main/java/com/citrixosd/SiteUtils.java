package com.citrixosd;

import java.util.Arrays;
import java.util.List;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import javax.jcr.Node;
import javax.jcr.RepositoryException;


public class SiteUtils {

	public static String getSiteName(Page page) {
		try {
			if (page == null)
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
					return contentNode.hasProperty("sitename") ? contentNode.getProperty("sitename").getString() : "";
				} else if (contentNode.isNodeType("mix:CitrixProductRoot")) {
					return contentNode.hasProperty("product-sitename") ? contentNode.getProperty("product-sitename").getString() : "";
				}
			}
			return SiteUtils.getSiteName(page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getFavIcon(Page page) {
		try {
			if(page == null) {
				return null;
			}else {
				final Resource contentResource = page.getContentResource();
				if (contentResource != null) {
					final Node contentNode = page.getContentResource().adaptTo(Node.class);
					if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
						return contentNode.hasProperty("favicon") ? contentNode.getProperty("favicon").getString() : null;
					} else if (contentNode.isNodeType("mix:CitrixProductRoot")) {
						return contentNode.hasProperty("product-favicon") ? contentNode.getProperty("product-favicon").getString() : null;
					}
				}
				return SiteUtils.getFavIcon(page.getParent());
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getProductTheme(Page page) {
		try {
			if(page == null) {
				return null;
			} else {
				final Resource contentResource = page.getContentResource();
				if (contentResource != null) {
					final Node contentNode = page.getContentResource().adaptTo(Node.class);
					if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
						return "";
					} else if (contentNode.isNodeType("mix:CitrixProductRoot")) {
						return contentNode.hasProperty("product-theme") ? contentNode.getProperty("product-theme").getString() : null;
					}
				}
				return SiteUtils.getProductTheme(page.getParent());
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getUnityNavProductUrl(Page page) {
		try {
			if(page == null) {
				return null;
			} else {
				final Resource contentResource = page.getContentResource();
				if (contentResource != null) {
					final Node contentNode = page.getContentResource().adaptTo(Node.class);
					if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
						return "";
					} else if (contentNode.isNodeType("mix:CitrixProductRoot")) {
						return contentNode.hasProperty("unitynav-product-url") ? contentNode.getProperty("unitynav-product-url").getString() : null;
					}
				}
				return SiteUtils.getUnityNavProductUrl(page.getParent());
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getMobileRedirectURL(Page page) {
		try {
			if (page == null)
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
					return contentNode.hasProperty("mobileRedirect") ? contentNode.getProperty("mobileRedirect").getString() : "";
				}
			}
			return SiteUtils.getMobileRedirectURL(page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "";
		}
	}
	public static String getAnalyticsURL(Page page) {
		return SiteUtils.getAnalyticsURL(page,null);
	}
	public static String getAnalyticsURL(Page page, String analyticsEnv) {
		try {
			if (page == null)
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
					if(contentNode.hasProperty("analyticsProfile") && analyticsEnv != null) {
						StringBuilder link = new StringBuilder();
						if(contentNode.hasProperty("analyticsLink"))
							link.append(contentNode.getProperty("analyticsLink").getString());
						else
							link.append("//tags.tiqcdn.com/utag/citrix/");
						link.append(contentNode.getProperty("analyticsProfile").getString()+"/");
						if(contentNode.hasProperty("analyticsEnv"))
							link.append(contentNode.getProperty("analyticsEnv").getString());
						else
							link.append(analyticsEnv);
						link.append("/utag.js");
						return link.toString();
					} else
						return contentNode.hasProperty("analyticsUrl") ? contentNode.getProperty("analyticsUrl").getString() : "";
				}
			}
			return SiteUtils.getAnalyticsURL(page.getParent(),analyticsEnv);
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getTrackingObjectNames(Page page) {
		try {
			if (page == null)
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixProductSiteRoot")) {
					if (contentNode.hasProperty("channelTrackingObjectNames")) {
						ValueMap vm = contentResource.adaptTo(ValueMap.class);
						String[] channelTrackingObjectNames = vm.get("channelTrackingObjectNames", String[].class);
						// we want to return this as a string, that is in js array notation.
						StringBuilder arr = new StringBuilder();
						arr.append("[");
						for (String str : channelTrackingObjectNames) {
							arr.append("\"" + str + "\",");
						}
						arr.deleteCharAt(arr.length()-2);
						arr.append("]");
						return arr.toString();
					}
					return "[]";
				}
			}
			return SiteUtils.getTrackingObjectNames(page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "[]";
		}
	}

	public static String[] getTrackingObjectNamesAsArray(Page page) {
		try {
			if (page == null)
				return null;
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixProductSiteRoot")) {
					if (contentNode.hasProperty("channelTrackingObjectNames")) {
						ValueMap vm = contentResource.adaptTo(ValueMap.class);
						String[] channelTrackingObjectNames = vm.get("channelTrackingObjectNames", String[].class);
						return channelTrackingObjectNames;
					}
					return null;
				}
			}
			return SiteUtils.getTrackingObjectNamesAsArray(page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getTrackingDomains(Page page) {
		String exclude = "";
		String mixin = "mix:CitrixSiteRoot";
		return getTrackingDomains(page, exclude, mixin);
	}

	public static String getTrackingDomains(Page page, String exclude, String mixin) {
		try {
			if (page == null)
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType(mixin)) {
					if (contentNode.hasProperty("channelTrackingDomains")) {
						ValueMap vm = contentResource.adaptTo(ValueMap.class);
						String[] channelTrackingDomains = vm.get("channelTrackingDomains", String[].class);
						// we want to return this as a string, that is in js array notation.
						StringBuilder arr = new StringBuilder();
						arr.append("[");
						for (String str : channelTrackingDomains) {
							if (!exclude.endsWith(str)) {
								arr.append("\"" + str + "\",");
							}
						}
						arr.deleteCharAt(arr.length()-2);
						arr.append("]");
						return arr.toString();
					}
					return "[]";
				}
			}
			return SiteUtils.getTrackingDomains(page.getParent(), exclude, mixin);
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "[]";
		}
	}

	public static String[] getTrackingDomainsAsArray(Page page) {
		String exclude = "";
		String mixin = "mix:CitrixSiteRoot";
		return getTrackingDomainsAsArray(page,exclude,mixin);
	}

	public static String[] getTrackingDomainsAsArray(Page page, String exclude, String mixin) {
		try {
			if (page == null)
				return null;
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType(mixin)) {
					if (contentNode.hasProperty("channelTrackingDomains")) {
						ValueMap vm = contentResource.adaptTo(ValueMap.class);
						String[] channelTrackingDomains = vm.get("channelTrackingDomains", String[].class);
						if (exclude != "") {
							List<String> channelTrackingExclude = Arrays.asList(channelTrackingDomains);
							for (String str : channelTrackingExclude) {
								if (exclude.endsWith(str)) {
									channelTrackingExclude.remove(str);
								}
							}
							channelTrackingDomains = channelTrackingExclude.toArray(new String[channelTrackingExclude.size()]);
						}
						return channelTrackingDomains;
					}
					return null;
				}
			}
			return SiteUtils.getTrackingDomainsAsArray(page.getParent(),exclude,mixin);
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getPropertyFromSiteRoot(String propertyName, Page page) {
		try {
			if (page == null)
				return "";
			if (propertyName == null || "".equals(propertyName))
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixSiteRoot")) {
					return contentNode.hasProperty(propertyName) ? contentNode.getProperty(propertyName).getString() : "";
				}
			}
			return SiteUtils.getPropertyFromSiteRoot(propertyName, page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getPropertyFromProductSiteRoot(String propertyName, Page page) {
		try {
			if (page == null)
				return "";
			if (propertyName == null || "".equals(propertyName))
				return "";
			Resource contentResource = page.getContentResource();
			Node contentNode;
			if (contentResource != null) {
				contentNode = page.getContentResource().adaptTo(Node.class);
				if (contentNode.isNodeType("mix:CitrixProductSiteRoot")) {
					return contentNode.hasProperty(propertyName) ? contentNode.getProperty(propertyName).getString() : "";
				}
			}
			return SiteUtils.getPropertyFromProductSiteRoot(propertyName, page.getParent());
		} catch (RepositoryException e) {
			e.printStackTrace();
			return "";
		}
	}

}
