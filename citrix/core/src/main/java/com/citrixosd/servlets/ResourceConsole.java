package com.citrixosd.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.AccessDeniedException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NodeType;
import javax.jcr.version.VersionException;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.citrixosd.enums.ResourceType;
import com.citrixosd.utils.Utilities;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;

@SlingServlet(paths = "/bin/resourceconsole",
			methods = "GET", 
			metatype=true)
@Properties({
	@Property(name = "service.description", value = "Servlet to handle resource pages console")
})

public class ResourceConsole extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		//retrieve request parameters
    	final String listType = request.getParameter("listType");
    	
    	if(listType == null || listType.trim().length() == 0){
    		//Start query builder parameters
        	final Map<String, String> map = new HashMap<String, String>();
    		map.put("path", "/content");
    		map.put("type", "cq:Page");
    		
    		// query for different resource types
    		map.put("1_property", "jcr:content/jcr:mixinTypes");
    		map.put("1_property.1_value", "mix:ResourceDocument");
    		map.put("1_property.2_value", "mix:ResourceWebinar");
    		map.put("1_property.3_value", "mix:ResourceVideo");
    		map.put("orderby", "@jcr:content/cq:lastModified");
    		map.put("orderby.sort","asc");
    		map.put("p.limit", "100000");
    		
			//Debugger
			//Set<String> sets = map.keySet();
			//for(String set : sets) {
				//System.out.print(set + ":");
				//System.out.println(map.get(set));
			//}
			
    		//create local session
    		Session session = request.getResource().getResourceResolver().adaptTo(Session.class);
    		
    		//Execute the query
    		final QueryBuilder builder = request.getResource().getResourceResolver().adaptTo(QueryBuilder.class);
    		final Query query = builder.createQuery(PredicateGroup.create(map), session);
    		SearchResult searchResult = query.getResult();
    		final long total = searchResult.getTotalMatches();
    		
    		if(total > 0){
    			String result = null;
    			final Gson gson = new Gson();
    			final List<Resource> resources = new ArrayList<Resource>();
    			final TagManager tagManager = request.getResourceResolver().adaptTo(TagManager.class);
        		
    			//Get Results
        		final Iterator<Node> nodeIter = searchResult.getNodes();
        		while(nodeIter.hasNext()) {
        			Resource resource = new Resource();
        			
        			final Node resourceTypeNode  = nodeIter.next();
        			try {
						final Node resourceTypeJcrNode = resourceTypeNode.hasNode("jcr:content") ? resourceTypeNode.getNode("jcr:content") : null;
						if(resourceTypeJcrNode != null) {	
							resource.setPath(resourceTypeJcrNode.getPath());
							resource.setTitle(resourceTypeJcrNode.hasProperty("jcr:title") ? resourceTypeJcrNode.getProperty("jcr:title").getString() : null);
							
							NodeType[] types = resourceTypeJcrNode.getMixinNodeTypes();
							
							ResourceType rt = null;
							for(NodeType type : types) {
								final String convertedResourceType = type.getName().contains("mix:") ? type.getName().split("mix:")[1] : type.getName();
								rt = ResourceType.getEnum(convertedResourceType);
								if(rt != null)
									break;
							}
							
							resource.setResourceType(rt);
							resource.setCategories(Utilities.getStringArrayFromArrayProperty(resourceTypeJcrNode, "resourceCategories"));
							
							Tag languageTag = tagManager.resolve(Utilities.getFirstStringArrayFromArrayProperty(resourceTypeJcrNode, "resourceLanguage"));
							resource.setLanguage(languageTag.getDescription());
							resource.setActivated(resourceTypeJcrNode.hasProperty("cq:lastReplicationAction") ? resourceTypeJcrNode.getProperty("cq:lastReplicationAction").getString() : null);
							
							//messing with getting dialog properties - using "path" - START
							final String superResourceType = resourceTypeJcrNode.hasProperty("sling:resourceType") ? resourceTypeJcrNode.getProperty("sling:resourceType").getString() : null;
							//superResourceType = /apps/.../twocolumnleftdocment
							
							//System.out.println("----- superResourceType is " + superResourceType + "-----" + resourceTypeJcrNode.getPath());
							
							if(superResourceType != null && !superResourceType.isEmpty()){
								resource.setSuperResourceType(superResourceType);
								//System.out.println("----- superResourceType passed -----");
								//adds /apps before the path
								final String superResourceTypePath = request.getResourceResolver().getResource(superResourceType).getPath();
								final Node superResourceTypeNode = session.getNode(superResourceTypePath);
								//superResourceTypeNode = /apps/.../twocolumnleftdocment
								//superResourceTypeNode.getProperty("sling:resourceSuperType") = /apps/.../twocolumnleft
								
								if(superResourceTypeNode != null){
									
									final Node resourceTypeDialogNode = superResourceTypeNode.hasNode("dialog") ? superResourceTypeNode.getNode("dialog") : null;
									if(resourceTypeDialogNode == null) {
										System.err.println("Resource type page does not have a dialog: " + superResourceTypeNode.getPath() + " from " + resourceTypeJcrNode.getPath());
									}else {
										resource.setDialogPath(resourceTypeDialogNode.getPath());
									}
								}else{
									System.err.println("Resource type page node not present: " + superResourceTypePath);
								}
							}else{
								System.err.println("superResourceType not set for: " + resourceTypeJcrNode.getPath());
							}
							
							resources.add(resource);
						}
					} catch (PathNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (RepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
        		
        		result = gson.toJson(resources);
        		response.setCharacterEncoding("UTF-8");
        		response.setContentType("application/json");
        		response.getWriter().write(result);
    		}
    	}
	}

	@Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
	    response.setContentType("text/html;charset=UTF-8");
	    
		//retrieve request parameters
		final String method = request.getParameter("method");
		
		//create local session
		Session session = request.getResource().getResourceResolver().adaptTo(Session.class);
		
		if(method != null && !method.isEmpty()){
			final String path = request.getParameter("path");			
			if(method.equals("delete")){
				String parentPath = path.replace("/jcr:content", "");
				parentPath = request.getResourceResolver().getResource(parentPath).getPath();
				
				Boolean error = true;
				try {
					//delete the node
					final Node deleteNode = session.getNode(parentPath);
					if(deleteNode != null){
						deleteNode.remove();
						session.save();
						session.getNode(parentPath);
					}
				} catch (PathNotFoundException e) {
					error = false;
					e.printStackTrace();
				} catch (RepositoryException e) {
					error = true;
					e.printStackTrace();
				}
			    
			    if(error){	
					response.getWriter().write("Could not delete node");
				}else{
					response.getWriter().write("Node has been deleted");
				}
			}else if(method.equals("add")){
				try {
					final Node etcResourceNode = session.getNode("/etc/resources/jcr:content");
					final String title = etcResourceNode.hasProperty("title") ? etcResourceNode.getProperty("title").getString() : null;
					final String nodeTitle = title.replaceAll("\\s", "_").toLowerCase();
					final String parentPath = etcResourceNode.getProperty("pagePath").getString();
					
					try {
						final Node parentNode = session.getNode(parentPath);
						System.out.println("Adding a page with the parent path: " + parentNode.getPath());
						if(parentNode.hasNode(nodeTitle)) {
							//Page is already created with this node title
							response.getWriter().write("Resource Page already exists.");
							System.out.println("Page already exist: " + parentNode.getNode(nodeTitle).getPath());
						}else {
							//Create new page since this is not in JCR
							final Node newPageNode = JcrUtils.getOrAddNode(parentNode, nodeTitle, "cq:Page");
							System.out.println("New resource page created: " + newPageNode.getPath());
							
							//Create jcr:content for this new page since this is not in JCR
							final Node newPageNodeJcr = newPageNode.addNode("jcr:content", "cq:PageContent");
							System.out.println("New resource page jcr:content created: " + newPageNodeJcr.getPath());
							
							//get top node - getting error here
					        final Node topNodeJcrNode = session.getNode(request.getResourceResolver().getResource(newPageNode.getParent().getPath()).getPath() + "/jcr:content");
					        
					        String[] topNodeJcrNodeTemplateParts = (topNodeJcrNode.getProperty("cq:template").getString()).split("templates");
					        
							//Adding Mixins
					         final String resourceType = etcResourceNode.getProperty("resourceType").getString();
					         if(resourceType.equals("document")){
					        	 newPageNodeJcr.addMixin("mix:ResourceDocument");
					        	 newPageNodeJcr.setProperty("sling:resourceType", "citrixosd/components/page/twoColumnLeftDocument");
					        	 newPageNodeJcr.setProperty("cq:template", topNodeJcrNodeTemplateParts[0] + "templates/twoColumnLeftDocument");
					         }else if(resourceType.equals("webinar")){
					        	 newPageNodeJcr.addMixin("mix:ResourceWebinar");
					        	 newPageNodeJcr.setProperty("sling:resourceType", "citrixosd/components/page/twoColumnLeftWebinar");
					        	 newPageNodeJcr.setProperty("cq:template", topNodeJcrNodeTemplateParts[0] + "templates/twoColumnLeftWebinar");
					         }else{
					        	 newPageNodeJcr.addMixin("mix:ResourceVideo");
					        	 newPageNodeJcr.setProperty("sling:resourceType", "citrixosd/components/page/twoColumnLeftVideo");
					        	 newPageNodeJcr.setProperty("cq:template", topNodeJcrNodeTemplateParts[0] + "templates/twoColumnLeftVideo");
					         }
					         
							//Add the properties to the jcr:content for the new page
					        //newPageNodeJcr.setProperty("jcr:created", Calendar.getInstance());
							newPageNodeJcr.setProperty("cq:lastModified", Calendar.getInstance());
							//newPageNodeJcr.setProperty("jcr:createdBy", session.getUserID());
							newPageNodeJcr.setProperty("cq:lastModifiedBy", session.getUserID());
							newPageNodeJcr.setProperty("cq:designPath", topNodeJcrNode.hasProperty("cq:designPath") ? topNodeJcrNode.getProperty("cq:designPath").getString() : null);
							newPageNodeJcr.setProperty("jcr:title", title);
							newPageNodeJcr.setProperty("resourceProduct", Utilities.getStringArrayFromArrayProperty(etcResourceNode, "resourceProduct"));
							newPageNodeJcr.setProperty("relevantResourceProducts",Utilities.getStringArrayFromArrayProperty(etcResourceNode, "relevantResourceProducts"));
							newPageNodeJcr.setProperty("resourceLanguage",Utilities.getStringArrayFromArrayProperty(etcResourceNode, "resourceLanguage"));
							newPageNodeJcr.setProperty("resourceCategories",Utilities.getStringArrayFromArrayProperty(etcResourceNode, "resourceCategories"));
							newPageNodeJcr.setProperty("resourceFeatured",Utilities.getStringArrayFromArrayProperty(etcResourceNode, "resourceFeatured"));
							newPageNodeJcr.setProperty("resourceGatedPath", etcResourceNode.hasProperty("resourceGatedPath") ? etcResourceNode.getProperty("resourceGatedPath").getString() : null);
							newPageNodeJcr.setProperty("resourceGatedTill", etcResourceNode.hasProperty("resourceGatedTill") ? etcResourceNode.getProperty("resourceGatedTill").getDate() : null);
						
							//Removing original values so it can be re-added in the next add page
							List<String> etcResourceNodeProperties = new ArrayList<String>(Arrays.asList("title","resourceProduct","relevantResourceProducts","resourceLanguage","resourceCategories","resourceFeatured","resourceGatedPath","resourceGatedTill"));
							resetEtcResourceNodeProperties(etcResourceNode,etcResourceNodeProperties);
							
							session.save();
							response.getWriter().write("Resource Page Added.");
						}
						
					}catch(PathNotFoundException e) {
						response.getWriter().write("Error while adding Resource Page.");
					}catch (RepositoryException e) {
						e.printStackTrace();
						response.getWriter().write("Wierd Error while adding Resource Page.");
					}
					
				}catch(PathNotFoundException e) {
					System.err.println("Resource console path is not found: /etc/resources/jcr:content");
				}catch(Exception e) {
					e.printStackTrace();
				}

			}
		}
    }

	private void resetEtcResourceNodeProperties(Node etcResourceNode,
			List<String> etcResourceNodeProperties) throws AccessDeniedException, VersionException, LockException, ConstraintViolationException, PathNotFoundException, RepositoryException {
		// TODO Auto-generated method stub
		for(String item : etcResourceNodeProperties ){
			if(etcResourceNode.hasProperty(item)) {
				etcResourceNode.getProperty(item).remove();
			}
		}
	}
}

class Resource {
	private String path;
	private String title;
	private String[] categories;
	private ResourceType resourceType;
	private String language;
	private String activated;
	private String superResourceType;
	private String dialogPath;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String[] getCategories() {
		return categories;
	}
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	public ResourceType getResourceType() {
		return resourceType;
	}
	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getActivated() {
		return activated;
	}
	public void setActivated(String activated) {
		this.activated = activated;
	}
	public String getSuperResourceType() {
		return superResourceType;
	}
	public void setSuperResourceType(String superResourceType) {
		this.superResourceType = superResourceType;
	}
	public String getDialogPath() {
		return dialogPath;
	}
	public void setDialogPath(String dialogPath) {
		this.dialogPath = dialogPath;
	}
}