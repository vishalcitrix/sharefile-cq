package com.citrixosd.servlets;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.nodetype.NodeType;
import javax.servlet.ServletException;

import com.adobe.granite.i18n.LocaleUtil;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.util.ISO8601;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import com.citrixosd.enums.ResourceType;
import com.citrixosd.models.Pagination;
import com.citrixosd.models.Resource;
import com.citrixosd.models.ResourceDocument;
import com.citrixosd.models.ResourceVideo;
import com.citrixosd.models.ResourceWebinar;
import com.citrixosd.utils.LinkUtil;
import com.citrixosd.utils.TranslationUtil;
import com.citrixosd.utils.Utilities;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.siteworx.rewrite.transformer.ContextRootTransformer;

@SlingServlet(
	    resourceTypes = "sling/servlet/default",
	    selectors = "resourceservlet",
	    extensions = "html",
	    methods = "GET")

@Properties({
	@Property(name = "service.description", value = "Servlet to handle all incoming widget modification")
})
public class ResourceServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;

	@Reference
	private ContextRootTransformer transformer;
	
	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		//Retrieve the tag manager to resolve tags
		final TagManager tagManager = request.getResourceResolver().adaptTo(TagManager.class);
		
		//Retrieve the request parameters
    	final String resourceType = request.getParameter("resourceType");
    	final String resourceLanguage = request.getParameter("resourceLanguages");
    	final String resourceCategory = request.getParameter("resourceCategories");
    	final String resourceFeatured = request.getParameter("resourceFeatured");
    	final String resourceLimit = request.getParameter("resourceLimit");
    	final String paginationIndex = request.getParameter("paginationIndex");
    	final String orderBy = request.getParameter("orderBy");
    	final String webinarUpcomingOnly = request.getParameter("webinarUpcomingOnly");
    	final String timeZone = request.getParameter("timeZone");
    	final String currentLocale = request.getParameter("currentLocale");
    	
    	//Set the locale
    	Locale locale = request.getLocale();
    	if(currentLocale != null && currentLocale.trim().length() > 0) {
    		locale = LocaleUtil.parseLocale(currentLocale);
    	}
    	
		//Set the translation for dates
		final ResourceBundle resourceBundle = request.getResourceBundle(locale);
		
		final DateFormat longDateFormatter = DateFormat.getDateInstance(DateFormat.LONG, locale);
		final DateFormat fullDateTimeFormatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT, locale);
		final DateFormat shortTimeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		
		//Set the time zone for the dates
    	if(timeZone != null && timeZone.trim().length() > 0) {
    		longDateFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
    		fullDateTimeFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
    		shortTimeFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
    	}
    	
    	//Set default limit and pagination index
    	int resourceLimitNumber = 0;
    	int paginationIndexNumber = 0;

    	//Start query builder parameters
    	final Map<String, String> map = new HashMap<String, String>();
		map.put("path", "/content");
		map.put("type", "cq:Page");
		
		//Resource Type
		if(resourceType != null && resourceType.trim().length() > 0) {
			map.put("1_property", "jcr:content/jcr:mixinTypes");
			if(resourceType.contains(",")) {
				final String[] resourceTypes = resourceType.split(",");
				for(int i = 0; i < resourceTypes.length; i++) {
					map.put("1_property." + (i + 1) + "_value", "mix:" + resourceTypes[i]);
				}
			}else {
				map.put("1_property.value", "mix:" + resourceType);
			}
		}

		//Language
		if(resourceLanguage != null && resourceLanguage.trim().length() > 0) {
			map.put("2_property", "jcr:content/resourceLanguage");
			if(resourceLanguage.contains(",")) {
				final String[] resourceLanguages = resourceLanguage.split(",");
				for(int i = 0; i < resourceLanguages.length; i++) {
					map.put("2_property." + (i + 1) + "_value", resourceLanguages[i]);
				}
			}else {
    			map.put("2_property.value", resourceLanguage);
			}
		}
		
		//Categories
		if(resourceCategory != null && resourceCategory.trim().length() > 0) {
			map.put("3_property", "jcr:content/resourceCategories");
			if(resourceCategory.contains(",")) {
				final String[] categoriesResourceCategories = resourceCategory.split(",");
				for(int i = 0; i < categoriesResourceCategories.length; i++) {
					map.put("3_property." + (i + 1) + "_value", categoriesResourceCategories[i]);
				}
			}else {
    			map.put("3_property.value", resourceCategory);
			}
		}
		
		//Featured
		if(resourceFeatured != null && resourceFeatured.trim().length() > 0) {
			map.put("4_property", "jcr:content/resourceFeatured");
			if(resourceFeatured.contains(",")) {
				final String[] resourceFeatures = resourceFeatured.split(",");
				for(int i = 0; i < resourceFeatures.length; i++) {
					map.put("4_property." + (i + 1) + "_value", resourceFeatures[i]);
				}
			}else {
    			map.put("4_property.value", resourceFeatured);
			}
		}

		//Limit
		if(resourceLimit != null && resourceLimit.matches("-?\\d+")) {
			resourceLimitNumber = Integer.parseInt(resourceLimit);
			map.put("p.limit", "" + resourceLimitNumber);
		}
		
		//Pagination Index
		if(paginationIndex != null && paginationIndex.matches("-?\\d+")) {
			paginationIndexNumber = Integer.parseInt(paginationIndex) * resourceLimitNumber;
			map.put("p.offset", "" + paginationIndexNumber);
		}
		
		//Webinar upcoming events
		if(ResourceType.ResourceWebinar.equals(ResourceType.getEnum(resourceType))) {
			if(webinarUpcomingOnly != null && webinarUpcomingOnly.trim().length() > 0 && webinarUpcomingOnly.equals("true")) {
				final String currentDateStr = ISO8601.format(Calendar.getInstance());
				map.put("5_daterange.property", "jcr:content/resourceContainer/webinarStartDate");
				map.put("5_daterange.lowerBound", currentDateStr);
				map.put("5_daterange.lowerOperation", ">=");
			}else {
				final Calendar currentCalendar = Calendar.getInstance();
				currentCalendar.add(Calendar.DATE, -1);
				final String currentDateWithOneExtraDayStr = ISO8601.format(currentCalendar);
				map.put("5_daterange.property", "jcr:content/resourceContainer/webinarStartDate");
				map.put("5_daterange.upperBound", currentDateWithOneExtraDayStr);
				map.put("5_daterange.upperOperation", "<");

				//TODO: add brightcove video check
			}
		}
		
		//Order
		if(orderBy != null && orderBy.trim().length() > 0) {
			if(webinarUpcomingOnly != null && webinarUpcomingOnly.trim().length() > 0 && webinarUpcomingOnly.equals("true")) {
				map.put("orderby", "@jcr:content/resourceContainer/webinarStartDate");
			}else {
				map.put("orderby", "@jcr:content/cq:lastModified");
			}
			map.put("orderby.index", "true");
			map.put("orderby.sort", orderBy);
		}else if(webinarUpcomingOnly != null && webinarUpcomingOnly.trim().length() > 0 && webinarUpcomingOnly.equals("true")) {
			map.put("orderby", "@jcr:content/resourceContainer/webinarStartDate");
			map.put("orderby.index", "true");
			map.put("orderby.sort", "asc");
		}
		
		//Debugger
		Set<String> sets = map.keySet();
		for(String set : sets) {
			System.out.print(set + ":");
			System.out.println(map.get(set));
		}
    	
    	try {
    		//Execute the query
    		final QueryBuilder builder = request.getResource().getResourceResolver().adaptTo(QueryBuilder.class);
    		final Query query = builder.createQuery(PredicateGroup.create(map), request.getResource().getResourceResolver().adaptTo(Session.class));
    		SearchResult searchResult = query.getResult();
    		final long total = searchResult.getTotalMatches();
    		
    		//If there is a valid result but returns a empty result because of pagination index  
    		if(total > 1 && paginationIndexNumber > total - 1) {
    			paginationIndexNumber = (int)(total - 1L);
    			map.put("p.offset", "" + paginationIndexNumber);
    			final Query reQuery = builder.createQuery(PredicateGroup.create(map), request.getResource().getResourceResolver().adaptTo(Session.class));
    			searchResult = reQuery.getResult();
    		}
    		
    		//Search through the query
    		final Iterator<Node> nodeIter = searchResult.getNodes();
    		final long start = searchResult.getStartIndex();
    		final long end = start + resourceLimitNumber;
    		final int totalPages = (int) Math.ceil((double)total / (double)resourceLimitNumber);
    		final int currentPage = (int) Math.floor((double)start / (double)resourceLimitNumber);
    		
    		//Set return objects
			String result = null;
			final Gson gson = new Gson();
			
			//Multiple resource
			if(resourceType != null && resourceType.trim().length() > 0 && resourceType.contains(",")) {
				final Pagination<Resource> resourcePagination = new Pagination<Resource>();
				final List<Resource> resources = new ArrayList<Resource>();
				while(nodeIter.hasNext()) {
					final Node resourceTypeNode  = nodeIter.next();
					final Node resourceTypeJcrNode = resourceTypeNode.hasNode("jcr:content") ? resourceTypeNode.getNode("jcr:content") : null;
					NodeType[] types = resourceTypeJcrNode.getMixinNodeTypes();
					for(NodeType type : types) {
						final String convertedResourceType = type.getName().contains("mix:") ? type.getName().split("mix:")[1] : type.getName();
						final ResourceType rt = ResourceType.getEnum(convertedResourceType);
						if(rt != null) {
							switch(rt) {
							case ResourceDocument:
								final ResourceDocument resourceDocument = getResourceDocument(resourceTypeNode, tagManager, resourceBundle, transformer);
								resources.add(resourceDocument);
								break;
							case ResourceWebinar:
								final ResourceWebinar resourceWebinar = getResourceWebinar(resourceTypeNode, tagManager, resourceBundle, transformer, longDateFormatter, fullDateTimeFormatter, shortTimeFormatter, webinarUpcomingOnly);
								resources.add(resourceWebinar);
								break;
							case ResourceVideo:
								final ResourceVideo resourceVideo = getResourceVideo(resourceTypeNode, tagManager, resourceBundle, transformer);
								resources.add(resourceVideo);
								break;
							default:
								System.out.println("Resource is not defined in the ResourceServlet");
								break;
							}
							break;
						}
					}
				}
				resourcePagination.setStart(start);
				resourcePagination.setEnd(end);
				resourcePagination.setTotal(total);
				resourcePagination.setTotalPages(totalPages);
				resourcePagination.setCurrentPage(currentPage);
				resourcePagination.setList(resources);
				result = gson.toJson(resourcePagination);
			}
			//Single Resource
			else {
				//Check which type of resource to return
				final ResourceType rt = ResourceType.valueOf(resourceType);
				switch(rt) {
				case ResourceDocument:
					final Pagination<ResourceDocument> documentPagination = new Pagination<ResourceDocument>();
					final List<ResourceDocument> resourceDocuments = new ArrayList<ResourceDocument>();
					while(nodeIter.hasNext()) {
						final Node documentPageNode = nodeIter.next();
						final ResourceDocument resourceDocument = getResourceDocument(documentPageNode, tagManager, resourceBundle, transformer);
	    				resourceDocuments.add(resourceDocument);
					}
					documentPagination.setStart(start);
					documentPagination.setEnd(end);
					documentPagination.setTotal(total);
					documentPagination.setTotalPages(totalPages);
					documentPagination.setCurrentPage(currentPage);
					documentPagination.setList(resourceDocuments);
					result = gson.toJson(documentPagination);
					
					break;
				case ResourceWebinar:
					final Pagination<ResourceWebinar> webinarPagination = new Pagination<ResourceWebinar>();
					final List<ResourceWebinar> resourceWebinars = new ArrayList<ResourceWebinar>();
					while(nodeIter.hasNext()) {
						final Node webinarPageNode = nodeIter.next();
						final ResourceWebinar resourceWebinar = getResourceWebinar(webinarPageNode, tagManager, resourceBundle, transformer, longDateFormatter, fullDateTimeFormatter, shortTimeFormatter, webinarUpcomingOnly);
						resourceWebinars.add(resourceWebinar);
					}
					webinarPagination.setStart(start);
					webinarPagination.setEnd(end);
					webinarPagination.setTotal(total);
					webinarPagination.setTotalPages(totalPages);
					webinarPagination.setCurrentPage(currentPage);
					webinarPagination.setList(resourceWebinars);
					result = gson.toJson(webinarPagination);
					
					break;
				case ResourceVideo:
					final Pagination<ResourceVideo> videoPagination = new Pagination<ResourceVideo>();
					final List<ResourceVideo> resourceVideos = new ArrayList<ResourceVideo>();
					while(nodeIter.hasNext()) {
						final Node videoPageNode = nodeIter.next();
						final ResourceVideo resourceVideo = getResourceVideo(videoPageNode, tagManager, resourceBundle, transformer);
	    				resourceVideos.add(resourceVideo);
					}
					videoPagination.setStart(start);
					videoPagination.setEnd(end);
					videoPagination.setTotal(total);
					videoPagination.setTotalPages(totalPages);
					videoPagination.setCurrentPage(currentPage);
					videoPagination.setList(resourceVideos);
					result = gson.toJson(videoPagination);
					
					break;
				default:
					System.out.println("Resource can not be found");
					break;
				}
			}
			
			response.setCharacterEncoding("UTF-8");
	    	response.setContentType("application/json");
	    	response.getWriter().write(result);
    	}catch (Exception e) {
    		e.printStackTrace();
		}
    }
	
	/**
	 * Populates the resource document object
	 */
	public static ResourceDocument getResourceDocument(Node documentPageNode, TagManager tagManager, ResourceBundle resourceBundle, ContextRootTransformer transformer) throws PathNotFoundException, ValueFormatException, IllegalStateException, RepositoryException {
		final ResourceDocument resourceDocument = new ResourceDocument();
		if(documentPageNode.hasNode("jcr:content")) {
			final Node documentNode = documentPageNode.getNode("jcr:content");
			if(documentNode.hasNode("resourceContainer")) {
				final Node node = documentNode.getNode("resourceContainer");
				resourceDocument.setTitle(node.hasProperty("documentTitle") ? node.getProperty("documentTitle").getString() : null);
				resourceDocument.setCredit(node.hasProperty("credit") ? node.getProperty("credit").getString() : null);
				resourceDocument.setCreditPath(node.hasProperty("creditPath") ? node.getProperty("creditPath").getString() : null);
				resourceDocument.setThumbnailPath(node.hasProperty("documentPath") ? node.getProperty("documentPath").getString() + "/jcr:content/renditions/cq5dam.web.200.300.png" : null);
			}
			final Tag featured = tagManager.resolve(Utilities.getFirstStringArrayFromArrayProperty(documentNode, "resourceFeatured"));
			resourceDocument.setFeatured(featured != null ? featured.getName() : null);
			resourceDocument.setCategories(Utilities.getStringArrayFromArrayProperty(documentNode, "resourceCategories"));
			resourceDocument.setLanguage(Utilities.getFirstStringArrayFromArrayProperty(documentNode, "resourceLanguage"));
			resourceDocument.setGated(documentNode.hasProperty("resourceGatedPath") ? documentNode.hasProperty("resourceGatedTill") ? documentNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false);
			resourceDocument.setPath(resourceDocument.isGated() && documentNode.hasProperty("resourceGatedPath") ? documentNode.getProperty("resourceGatedPath").getString() : documentPageNode.getPath());
		}
		resourceDocument.setResourceType(ResourceType.ResourceDocument);
		
		//Resolve transform paths for context root
		resourceDocument.setPath(LinkUtil.getTransformedUrl(resourceDocument.getPath(), transformer));
		resourceDocument.setCreditPath(LinkUtil.getTransformedUrl(resourceDocument.getCreditPath(), transformer));
		resourceDocument.setThumbnailPath(LinkUtil.getTransformedUrl(resourceDocument.getThumbnailPath(), transformer));
		
		return resourceDocument;
	}
	
	/**
	 * Populates the resource webinar object
	 */
	public static ResourceWebinar getResourceWebinar(Node webinarPageNode, TagManager tagManager, ResourceBundle resourceBundle, ContextRootTransformer transformer, DateFormat longDateFormatter, DateFormat fullDateTimeFormatter, DateFormat shortTimeFormatter, String webinarUpcomingOnly) throws PathNotFoundException, ValueFormatException, IllegalStateException, RepositoryException {
		final ResourceWebinar resourceWebinar = new ResourceWebinar();
		if(webinarPageNode.hasNode("jcr:content")) {
			final Node webinarNode = webinarPageNode.getNode("jcr:content");
			if(webinarNode.hasNode("resourceContainer")) {
				final Node node = webinarNode.getNode("resourceContainer");
				resourceWebinar.setTitle(node.hasProperty("videoTitleOverride") ? node.getProperty("videoTitleOverride").getString() : node.hasProperty("videoTitle") ? node.getProperty("videoTitle").getString() : null);
		    	resourceWebinar.setAuthorThumbnailPath(node.hasProperty("fileReference") ? node.hasProperty("rendition") ? node.getProperty("rendition").getString() : node.getProperty("fileReference").getString() : node.hasProperty("videoThumbnailUrl") ? node.getProperty("videoThumbnailUrl").getString(): null);
		    	resourceWebinar.setAuthorTitle(node.hasProperty("authorTitle") ? node.getProperty("authorTitle").getString() : null);
		    	resourceWebinar.setAuthorDescription(node.hasProperty("authorDescription") ? node.getProperty("authorDescription").getString() : null);
		    	if(node.hasProperty("webinarStartDate")) {
		    		final Date currentDate = new Date();
		    		final Date webinarDate = node.getProperty("webinarStartDate").getDate().getTime();
		    		if(currentDate.before(webinarDate) || currentDate.equals(webinarDate)) {
		    			resourceWebinar.setPath(node.hasProperty("registrationPath") ? node.getProperty("registrationPath").getString() : null);
		    			if(resourceWebinar.getPath() != null) {
		    				resourceWebinar.setRegistration(true);
		    			}
		    		}
		    	}
		    	if(webinarUpcomingOnly != null && webinarUpcomingOnly.equals("true")) {
                    Date upcomingDate = node.getProperty("webinarStartDate").getDate().getTime();

		    		resourceWebinar.setUpcoming(node.hasProperty("webinarStartDate") ? longDateFormatter.format(upcomingDate) : null);
		    		resourceWebinar.setUpcomingTime(node.hasProperty("webinarStartDate") ? shortTimeFormatter.format(upcomingDate) + " (" + shortTimeFormatter.getTimeZone().getDisplayName(shortTimeFormatter.getTimeZone().inDaylightTime(upcomingDate), TimeZone.SHORT) + ")" : null);
		    		resourceWebinar.setUpcomingLong(node.hasProperty("webinarStartDate") ? fullDateTimeFormatter.format(upcomingDate) + " (" + fullDateTimeFormatter.getTimeZone().getDisplayName(fullDateTimeFormatter.getTimeZone().inDaylightTime(upcomingDate), TimeZone.SHORT) + ")" : null);
		    	}
		    			
			}
			final Tag featured = tagManager.resolve(Utilities.getFirstStringArrayFromArrayProperty(webinarNode, "resourceFeatured"));
			resourceWebinar.setFeatured(featured != null ? featured.getName() : null);
			resourceWebinar.setCategories(TranslationUtil.translateArray(Utilities.getStringArrayFromArrayProperty(webinarNode, "resourceCategories"), resourceBundle));
			resourceWebinar.setLanguage(Utilities.getFirstStringArrayFromArrayProperty(webinarNode, "resourceLanguage"));
			resourceWebinar.setGated(webinarNode.hasProperty("resourceGatedPath") ? webinarNode.hasProperty("resourceGatedTill") ? webinarNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false);
			resourceWebinar.setPath(resourceWebinar.getPath() != null ? resourceWebinar.getPath() : resourceWebinar.isGated() && webinarNode.hasProperty("resourceGatedPath") ? webinarNode.getProperty("resourceGatedPath").getString() : webinarPageNode.getPath());
		}
		resourceWebinar.setResourceType(ResourceType.ResourceWebinar);
		
		//Resolve transform paths for context root
		resourceWebinar.setPath(LinkUtil.getTransformedUrl(resourceWebinar.getPath(), transformer));
		resourceWebinar.setAuthorThumbnailPath(LinkUtil.getTransformedUrl(resourceWebinar.getAuthorThumbnailPath(), transformer));
		
		return resourceWebinar;
	}
	
	/**
	 * Populates the resource video object
	 */
	public static ResourceVideo getResourceVideo(Node videoPageNode, TagManager tagManager, ResourceBundle resourceBundle, ContextRootTransformer transformer) throws PathNotFoundException, ValueFormatException, IllegalStateException, RepositoryException {
		final ResourceVideo resourceVideo = new ResourceVideo();
		if(videoPageNode.hasNode("jcr:content")) {
			final Node videoNode = videoPageNode.getNode("jcr:content");
			if(videoNode.hasNode("resourceContainer")) {
				final Node node = videoNode.getNode("resourceContainer");
				resourceVideo.setTitle(node.hasProperty("videoTitleOverride") ? node.getProperty("videoTitleOverride").getString() : node.hasProperty("videoTitle") ? node.getProperty("videoTitle").getString() : null);
				resourceVideo.setThumbnailPath(node.hasProperty("fileReference") ? node.hasProperty("rendition") ? node.getProperty("rendition").getString() : node.getProperty("fileReference").getString() : node.hasProperty("videoThumbnailUrl") ? node.getProperty("videoThumbnailUrl").getString(): null);
				resourceVideo.setVideoLength(node.hasProperty("videoHumanLength") ? node.getProperty("videoHumanLength").getString() : null);
			}
			
			final Tag featured = tagManager.resolve(Utilities.getFirstStringArrayFromArrayProperty(videoNode, "resourceFeatured"));
			resourceVideo.setFeatured(featured != null ? featured.getName() : null);
			resourceVideo.setCategories(TranslationUtil.translateArray(Utilities.getStringArrayFromArrayProperty(videoNode, "resourceCategories"), resourceBundle));
			resourceVideo.setLanguage(Utilities.getFirstStringArrayFromArrayProperty(videoNode, "resourceLanguage"));
			resourceVideo.setGated(videoNode.hasProperty("resourceGatedPath") ? videoNode.hasProperty("resourceGatedTill") ? videoNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false);
			resourceVideo.setPath(resourceVideo.isGated() && videoNode.hasProperty("resourceGatedPath") ? videoNode.getProperty("resourceGatedPath").getString() : videoPageNode.getPath());
		}
		resourceVideo.setResourceType(ResourceType.ResourceVideo);
		
		//Resolve transform paths for context root
		resourceVideo.setPath(LinkUtil.getTransformedUrl(resourceVideo.getPath(), transformer));
		resourceVideo.setThumbnailPath(LinkUtil.getTransformedUrl(resourceVideo.getThumbnailPath(), transformer));
		
		return resourceVideo;
	}
	
	@Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	
    }
}