package com.citrixosd.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;


/*
 * Get dynamic content from 3 ships 
 * Function : reads URL and if /article/learn-more present then this service is invoked
 * @author - vishal.gupta@citrix.com
*/

@SlingFilter(
        label = "Sharefile - 3 Ships Filter",
        description = "Reads URL and if /article/learn-more present then this service is invoked",
        metatype = false,
        generateComponent = true, // True if you want to leverage activate/deactivate or manage its OSGi life-cycle
        generateService = true, // True; required for Sling Filters
        order = 0,
        scope = SlingFilterScope.REQUEST)

public class canocalizePDF implements Filter {
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		Resource resource = slingRequest.getResource();
		if(resource.getPath().contains("pdf")) {
			File file = new File("http://localhost:4502/content/dam/sf/pdf-sample.pdf");
	        return;
		}
		
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
}