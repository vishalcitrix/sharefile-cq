package com.citrixosd.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

/*
 * Webook handler for Chats 
 * Function : Updates database when a chat starts
 * @author - vishal.gupta@citrix.com
*/

@SlingServlet(
	    resourceTypes = "sling/servlet/default",
	    selectors = "chatstart",
	    extensions = "json",
	    methods = {"GET", "POST"})

@Properties({
	@Property(name = "service.description", value = "Servlet to handle all incoming widget modification")
})

public class HandleChat extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
		response.setContentType("text/html");
    	response.getWriter().write("aa11ws4ee3e");
    }
	
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	response.getWriter().write("Nice try!");
    }
}