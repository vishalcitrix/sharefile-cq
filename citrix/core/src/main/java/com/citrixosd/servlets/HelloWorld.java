package com.citrixosd.servlets;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;

@SlingServlet(
	    resourceTypes = "sling/servlet/default",
	    selectors = "helloworld",
	    extensions = "html",
	    methods = "GET")
public class HelloWorld extends SlingSafeMethodsServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	response.getWriter().write("hello world");
    }
}