<VirtualHost *:80>
	ServerName cmsdevweb1.cs.qai
	DocumentRoot /var/www/html

	RewriteEngine on
	# Set handler for cache clear to dispatcher
	RewriteRule ^/dispatcher/invalidate.cache$ - [H=dispatcher-handler,L]
	# Dont rewrite for health check.
	RewriteRule ^/cgi-bin/system-status$ - [L]

	# Rewrite all other requests (other than /server-status to the long name
	RewriteCond %{REQUEST_URI} !^/server-status$
	RewriteRule (.*) http://cmsdevweb1.cs.qai.expertcity.com/$1 [R=302]
</VirtualHost>

<VirtualHost *:80>
	ServerName cmsdevweb.citrixonline.com
	ServerAlias cmsdevweb1.cs.qai.expertcity.com

	RewriteEngine on

	# Allow other domains to access the fonts and use them in firefox.
	# totally unrestrictive.. consider locking down, but needs to include javascript files as well 
	#<FilesMatch "\.(ttf|otf|eot|woff)$">
  		<IfModule mod_headers.c>
    		Header set Access-Control-Allow-Origin "*"
  		</IfModule>
	#</FilesMatch>

	# G2PC - All requests to the design, mobile design, DAM
	RewriteRule ^/remote-access/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/remote-access/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/remote-access/design(.*)$ /etc/designs/g2pc$1 [PT,L]
	RewriteRule ^/remote-access/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/remote-access/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/remote-access/mobile-design(.*)$ /etc/designs/g2pc-mobile$1 [PT,L]
	RewriteRule ^/remote-access/dam(.*)$ /content/dam/g2pc$1 [PT,L]
	RewriteRule ^/remote-access/campaigns(.*)$ /content/campaigns/g2pc$1 [PT,L]
	
	# Netviewer - All DAM requests to the Netviewer DAM
	RewriteRule ^/online/dam/nv(.*)$ /content/dam/netviewer$1 [PT,L]
	RewriteRule ^/online/campaigns/nv(.*)$ /content/campaigns/netviewer$1 [PT,L]
	
	# G2M Minisites - All requests to the design, mobile design, DAM
	RewriteRule ^/online/design/minisite/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online/design/minisite/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online/design/minisite(.*)$ /etc/designs/g2m-minisite$1 [PT,L]
	RewriteRule ^/online/dam/minisite(.*)$ /content/dam/g2m-minisite$1 [PT,L]
	RewriteRule ^/online/campaigns/g2m-minisite(.*)$ /content/campaigns/g2m-minisite$1 [PT,L]
	
	# G2MWT - All requests to the design, mobile design, DAM
	RewriteRule ^/online/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online/design(.*)$ /etc/designs/g2m$1 [PT,L]
	RewriteRule ^/online/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online/mobile-design(.*)$ /etc/designs/g2m-mobile$1 [PT,L]
	RewriteRule ^/online/dam-mobile(.*)$ /content/dam/g2m-mobile$1 [PT,L]
	RewriteRule ^/online/collaboration/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online/collaboration/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online/collaboration/design(.*)$ /etc/designs/g2m-redesign$1 [PT,L]
	RewriteRule ^/online/dam(.*)$ /content/dam/g2mwt$1 [PT,L]
	RewriteRule ^/online/campaigns/g2m(.*)$ /content/campaigns/g2m$1 [PT,L]
	RewriteRule ^/online/campaigns/g2t(.*)$ /content/campaigns/g2t$1 [PT,L]
	RewriteRule ^/online/campaigns/g2w(.*)$ /content/campaigns/g2w$1 [PT,L]

	# G2A - All requests to the design, mobile design, DAM
	RewriteRule ^/remote-support/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/remote-support/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/remote-support/design(.*)$ /etc/designs/g2a$1 [PT,L]
	RewriteRule ^/remote-support/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/remote-support/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/remote-support/mobile-design(.*)$ /etc/designs/g2a-mobile$1 [PT,L]
	RewriteRule ^/remote-support/dam(.*)$ /content/dam/g2a$1 [PT,L]
	RewriteRule ^/remote-support/campaigns(.*)$ /content/campaigns/g2a$1 [PT,L]

	# COL - All requests to the design, mobile design, DAM
	RewriteRule ^/online-collaboration/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online-collaboration/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online-collaboration/design(.*)$ /etc/designs/col$1 [PT,L]
	RewriteRule ^/online-collaboration/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/online-collaboration/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/online-collaboration/mobile-design(.*)$ /etc/designs/col-mobile$1 [PT,L]
	RewriteRule ^/online-collaboration/dam(.*)$ /content/dam/col$1 [PT,L]
	RewriteRule ^/online-collaboration/campaigns(.*)$ /content/campaigns/col$1 [PT,L]

	# OpenVoice - All requests to the design, mobile design, DAM
	RewriteRule ^/ov/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/ov/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/ov/design(.*)$ /etc/designs/openvoice$1 [PT,L]
	RewriteRule ^/ov/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/ov/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/ov/mobile-design(.*)$ /etc/designs/openvoice-mobile$1 [PT,L]
	RewriteRule ^/ov/dam(.*)$ /content/dam/openvoice$1 [PT,L]
	RewriteRule ^/ov/campaigns(.*)$ /content/campaigns/openvoice$1 [PT,L]
	
	# SORRY - All requests to the design, mobile design, DAM
	RewriteRule ^/sorry/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/sorry/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/sorry/design(.*)$ /etc/designs/sorryPage$1 [PT,L]
	RewriteRule ^/sorry/mobile-design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/sorry/mobile-design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/sorry/mobile-design(.*)$ /etc/designs/sorryPage-mobile$1 [PT,L]
	RewriteRule ^/sorry/dam(.*)$ /content/dam/sorryPage$1 [PT,L]
	RewriteRule ^/sorry/campaigns(.*)$ /content/campaigns/sorryPage$1 [PT,L]
	
	# ShareFile - All requests to the design and DAM
	RewriteRule ^/share/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/share/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/share/design(.*)$ /etc/designs/sf$1 [PT,L]
	RewriteRule ^/share/dam(.*)$ /content/dam/sf$1 [PT,L]
	RewriteRule ^/share/campaigns(.*)$ /content/campaigns/sharefile$1 [PT,L]
	RewriteRule ^/google48dc952f30df6a47\.html$ /content/dam/sf/google48dc952f30df6a47.html [PT,L]
	
	# CRM - All requests to DAM
	RewriteRule ^/crm(.*)$ /content/dam/crm$1 [PT,L]

	 # Landing pages - All requests to the design and DAM
	 RewriteRule ^/online/pages/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
  	 RewriteRule ^/online/pages/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
  	 RewriteRule ^/online/pages/design(.*)$ /etc/designs/g2mwt-lp$1 [PT,L]
  	 RewriteRule ^/online/pages/dam(.*)$ /content/dam/collab-lps$1 [PT,L]

	# ShareConnect
	RewriteRule ^/shareconnect/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
	RewriteRule ^/shareconnect/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
	RewriteRule ^/shareconnect/design(.*)$ /etc/designs/shareconnect$1 [PT,L]
	RewriteRule ^/shareconnect/dam(.*)$ /content/dam/shareconnect$1 [PT,L]

	SetHandler dispatcher-handler
</VirtualHost>

SetEnv HEALTHCHECK_HOST cmsdevpub1.cs.qai:4503