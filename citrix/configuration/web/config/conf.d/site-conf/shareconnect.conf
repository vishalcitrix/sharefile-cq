<VirtualHost *:80>
    #########################################################
    #       ShareConnect US                                 #
    #       www.sharefile.com                               #
    #########################################################
    ServerName www.shareconnect.com
    ServerAlias shareconnect.com
    ServerAlias stage.shareconnect.com
    ServerAlias qa1.shareconnect.com
    ServerAlias qa2.shareconnect.com
    ServerAlias qa3.shareconnect.com
    ServerAlias qa4.shareconnect.com
    ServerAlias i1.shareconnect.com
    ServerAlias i2.shareconnect.com
    ServerAlias i3.shareconnect.com
    ServerAlias i4.shareconnect.com
    ServerAlias i1.sc.test.expertcity.com

    DocumentRoot /var/www/html

    RewriteEngine On
    RewriteOptions inherit

    ################################################################
    #       All inbound requests should be to /shareconnect        #
    #       /shareconnect(.*)                                      #
    #                                        #
    ################################################################

    # OLD Landing page redirects
    RewriteRule ^/members/scVerticals.tmpl$ https://app.shareconnect.com/members/scVerticals.tmpl [R,L]
    RewriteRule ^/members/realEstateLogin.tmpl$ https://app.shareconnect.com/members/realEstateLogin.tmpl [R,L]
    RewriteRule ^/members/healthcareLogin.tmpl$ https://app.shareconnect.com/members/healthcareLogin.tmpl [R,L]
    RewriteRule ^/members/constructionLogin.tmpl$ https://app.shareconnect.com/members/constructionLogin.tmpl [R,L]
    RewriteRule ^/members/governmentLogin.tmpl$ https://app.shareconnect.com/members/governmentLogin.tmpl [R,L]
    RewriteRule ^/members/legalLogin.tmpl$ https://app.shareconnect.com/members/legalLogin.tmpl [R,L]
    RewriteRule ^/members/softwareLogin.tmpl$ https://app.shareconnect.com/members/softwareLogin.tmpl [R,L]
    RewriteRule ^/members/othersLogin.tmpl$ https://app.shareconnect.com/members/othersLogin.tmpl [R,L]
    RewriteRule ^/members/login.tmpl$ https://app.shareconnect.com/members/login.tmpl [R,L]
    RewriteRule ^/industries/real-estate/?$ https://app.shareconnect.com/industries/real-estate/ [R,L]
    RewriteRule ^/industries/healthcare/?$ https://app.shareconnect.com/industries/healthcare/ [R,L]
    RewriteRule ^/industries/construction/?$ https://app.shareconnect.com/industries/construction/ [R,L]
    RewriteRule ^/industries/government/?$ https://app.shareconnect.com/industries/government/ [R,L]
    RewriteRule ^/industries/legal/?$ https://app.shareconnect.com/industries/legal/ [R,L]
    RewriteRule ^/industries/software-development/?$ https://app.shareconnect.com/industries/software-development/ [R,L]
    RewriteRule ^/industries/others/?$ https://app.shareconnect.com/industries/others/ [R,L]
    RewriteRule ^/industries/verticals/?$ https://app.shareconnect.com/industries/verticals/ [R,L]

    # If TLD requested, forward to www
    RewriteCond %{HTTP_HOST} ^shareconnect.com$
    RewriteRule ^(/.*)$ https://www.shareconnect.com$1 [R,L]

    # http to https for all pages
    #RewriteCond %{HTTP:X-Forwarded-Proto} !=https
    #RewriteRule ^/(.*) https://%{HTTP_HOST}/$1 [NC,R=301,L]

    # All context-rooted URLs without any file path
    RewriteRule ^/try$ /try/ [R=301,L]
    RewriteRule ^/try/$ /content/sc-lp/en_US.html [PT,L]
    RewriteRule ^/$ /content/shareconnect/en_US.html [PT,L]
	
	# All requests to the design, DAM, campaigns, segmentation, clientcontext - LP
    RewriteRule ^/shareconnect/try/dam(.*)$ /content/dam/shareconnect-lp$1 [PT,L]
    RewriteRule ^/shareconnect/try/campaigns(.*)$ /content/campaigns/shareconnect-lp$1 [PT,L]
	
    # All requests to the design, DAM, campaigns, segmentation, clientcontext
    RewriteRule ^/shareconnect/design/segmentation(.*)$ /etc/segmentation$1 [PT,L]
    RewriteRule ^/shareconnect/design/clientcontext(.*)$ /etc/clientcontext$1 [PT,L]
    RewriteRule ^/shareconnect/design(.*)$ /etc/designs/shareconnect$1 [PT,L]
    RewriteRule ^/shareconnect/dam(.*)$ /content/dam/shareconnect$1 [PT,L]
    RewriteRule ^/shareconnect/campaigns(.*)$ /content/campaigns/shareconnect$1 [PT,L]
    
    # Handle request for /bin/citrix/*
	RewriteRule ^/bin/citrix/(.*)$ /bin/citrix/$1 [PT,L]
	
	# All URLs for landing pages                                                                                                                                                                             
    RewriteRule ^/try/(.*).html$ /content/sc-lp/en_US/$1.html [PT,L]
    RewriteRule ^/try/(.*)/$ /content/sc-lp/en_US/$1.html [PT,L]
    RewriteRule ^/try/(.*)$ /content/sc-lp/en_US/$1.html [PT,L]

    # All URLs with a .html extension, trailing slash, no extension
    RewriteRule ^/(.*).html$ /content/shareconnect/en_US/$1.html [PT,L]
    RewriteRule ^/(.*)/$ /content/shareconnect/en_US/$1.html [PT,L]
    RewriteRule ^/(.*)$ /content/shareconnect/en_US/$1.html [PT,L]

    SetHandler dispatcher-handler
</VirtualHost>