#! /bin/bash

#######
#
# backup-author.sh
#
# This script executes a CQ hot backup on the local
# CQ instance. Once complete, the remote (target) 
# instance is stopped, and the backup is sent over
# the net via rsync. After the transfer is complete,
# the remote host is cleaned of cluster information and
# restarted.
#
#######


usage() {
    echo "Use -t <target_host> to provide target host."
    echo "Failed to execute backup.";
    exit 1;
}

# check that proper user is executing
# necessary for file permissions and for ssh
if [ "$USER" != "crx" ]
then
    echo "[ $(date +%c) ] ERROR: Execute with user crx."
    echo "[ $(date +%c) ] ERROR: Failed to create backup."
    exit 1
fi

LOGFILE=/opt/cq-backup/logs/backup.log
exec >> $LOGFILE 2>&1

# check that target host option is passed
while getopts t: option; do
    case "${option}" in
        t) TARGET_HOST=${OPTARG};;
        *) usage;;
    esac
done

CQ_SHUTDOWN_SCRIPT="/opt/cq-backup/scripts/shutdown-cq.sh"
CQ_STARTUP_SCRIPT="/opt/cq-backup/scripts/startup-cq.sh"
CQ_CLEAN_STARTUP_SCRIPT="/opt/cq-backup/scripts/clean-startup-cq.sh"

CQ_AUTH_FILE=/opt/cq/crx-quickstart/conf/admin.txt

CQ5_HOME=/opt/cq
QUICKSTART_DIR=$CQ5_HOME/crx-quickstart

BACKUP_NAME=`date +"%m-%d-%Y"`
DAILY_BACKUP_DIR=/opt/cq-backup/backups/daily
BACKUP_DEST=$DAILY_BACKUP_DIR/$BACKUP_NAME
QUICKSTART_DEST=$BACKUP_DEST/crx-quickstart

PROGRESS_INDICATOR=$DAILY_BACKUP_DIR/backupInProgress

RSYNC_LOG="/opt/cq-backup/logs/backup-rsync.log"
RSYNCARGS="-e ssh --delete -a -v --log-file=$RSYNC_LOG"

delay=0
hostname=localhost
port=4502


echo "[ $(date +%c) ] Backup script starting (backup-author.sh)."
while [ -f $PROGRESS_INDICATOR ];
do
    # another backup may be going on now to the destination
    # wait for completion to avoid competition for disk resources
    echo "[ $(date +%c) ] Disk is busy; waiting for $PROGRESS_INDICATOR to be removed."
    sleep 30;
done

# set CQ backup delay - necessary to complete backup in fastest way possible
curl  --config $CQ_AUTH_FILE -X POST -d "value=$delay" http://$hostname:$port/system/console/jmx/com.adobe.granite%3Atype%3DRepository/a/BackupDelay

# execute CQ backup
echo "[ $(date +%c) ] Beginning CQ Backup"
touch $PROGRESS_INDICATOR
cq_start_time_sec=$(date +%s)
curl_output=$(curl -s -S --config $CQ_AUTH_FILE -d "action=add&targetDir=$QUICKSTART_DEST&zipFileName=" http://$hostname:$port/crx/config/backup.jsp)

# check for errors in execution. if any, fail out.
if [ $? -ne 0 ]
then
    echo "[ $(date +%c) ] ERROR: Error starting CQ backup: $curl_output"
    echo "[ $(date +%c) ] ERROR: Failed to create backup."
    echo "[ $(date +%c) ] ERROR: Removing $PROGRESS_INDICATOR."
    rm $PROGRESS_INDICATOR
    exit 1
fi

sleep 10

# ongoing status indicator until backup is complete
while [ -f $PROGRESS_INDICATOR ];
do
    echo "[ $(date +%c) ]  Waiting `expr $(date +%s) - $cq_start_time_sec` seconds for CQ backup..."
    sleep 30
done

echo "[ $(date +%c) ] CQ Backup Took `expr $(date +%s) - $cq_start_time_sec` seconds"
echo "[ $(date +%c) ] CQ Backup Complete."
echo "[ $(date +%c) ] Removing $PROGRESS_INDICATOR.  [ date +%c ] "
rm $PROGRESS_INDICATOR

# execute the remote script for shutting down CQ
ssh $TARGET_HOST '/bin/sh $CQ_SHUTDOWN_SCRIPT'

# determine if the remote CQ instance was shut down
if [ $? -ne 0 ]
then
    # remote CQ didn't shut down - exiting
    echo "[ $(date +%c) ] ERROR: Error stopping remote CQ instance."
    echo "[ $(date +%c) ] ERROR: Failed to transfer backup to remote host."
    exit 1
fi

# rsync the backup to the target host
/usr/bin/rsync $RSYNCARGS $QUICKSTART_DEST $TARGET_HOST:$QUICKSTART_DIR

# Execute the remote startup script.
ssh $TARGET_HOST '/bin/sh $CQ_CLEAN_STARTUP_SCRIPT'

# determine if the remote CQ instance was started up
if [ $? -ne 0 ]
then
    # remote CQ didn't shut down - exiting
    echo "[ $(date +%c) ] ERROR: Error starting remote CQ instance."
    echo "[ $(date +%c) ] ERROR: Try manually starting CQ."
    exit 1
fi

exit 0
