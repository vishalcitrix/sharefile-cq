#!/bin/bash

#######
#
# shutdown-cq.sh
# This script shuts down the local CQ instance.
#
#######

service cq5 stop

#TODO logic for checking if it shut down, and if not, trying agian or exiting 0

exit 0