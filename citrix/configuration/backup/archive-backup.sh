#!/bin/bash

#######
#
# archive-backup.sh
#
# This script is executed daily by cron and archives
# the backup from the working directory to the daily 
# backup directory, with a dated name. It then checks
# each archive directory and moves files as necessary
# to maintain the correct setup according to the weeks
# and months parameters.
#
#######

usage() {
	echo "Use -m <number> to override number of months."
	echo "Use -w <number> to override number of weeks."
	exit 1;
}

LOGFILE=/opt/cq-backup/logs/archive.log
exec >> $LOGFILE 2>&1

# default number of weeks to keep backups of.
NUMBER_OF_WEEKS=4
# default number of months to keep backups of.
NUMBER_OF_MONTHS=6

while getopts m::w:: option; do
    case "${option}" in
		m) NUMBER_OF_MONTHS=${OPTARG};;
		w) NUMBER_OF_WEEKS=${OPTARG};;
		*) usage;;
    esac
done

exec >> $LOGFILE 2>&1

BACKUP_DIR=/opt/cq-backup/backups
DAILY_BACKUP_DIR=$BACKUP_DIR/daily
WEEKLY_BACKUP_DIR=$BACKUP_DIR/weekly
MONTHLY_BACKUP_DIR=$BACKUP_DIR/monthly

TRASH_DIR=$BACKUP_DIR/trash

process_directory() {
	DIR_NAME=$1
	echo "[ $(date +%c) ] INFO: Running process_directory on $DIR_NAME"
	case "$DIR_NAME" in
		daily) 
			working_dir=$DAILY_BACKUP_DIR;
			max_records=6;
			advance_dir=$WEEKLY_BACKUP_DIR;;
		weekly) 
			working_dir=$WEEKLY_BACKUP_DIR;
			max_records=$NUMBER_OF_WEEKS;
			advance_dir=$MONTHLY_BACKUP_DIR;;
		monthly) 
			working_dir=$MONTHLY_BACKUP_DIR;
			max_records=$NUMBER_OF_MONTHS;
			advance_dir=$TRASH_DIR;;
		*) 
			echo "[ $(date +%c) ] ERROR: process_directory called with invalid argument."
			echo "[ $(date +%c) ] ERROR: Failed to complete archive processing.";;
	esac

	echo "[ $(date +%c) ] INFO: Processing $working_dir with max of $max_records records."

	number_of_backups=`ls -1t $working_dir | grep -v ^total | wc -l`;

	if [ $number_of_backups -gt $max_records ] ; then
		# grab the file name
		file_name=`ls -1t $working_dir | tail -n 1`

		if [ $DIR_NAME == "daily" ] ; then
			# if daily, we want to promote oldest and delete rest
			# tar the directory, and move it to advance dir

			echo "[ $(date +%c) ] INFO: Archiving $file_name to $advance_dir."

			#remove the compression from the tar
			#tar -cvzf $advance_dir/$file_name.tar.gz $working_dir/$file_name
			# verify this tar command without compression
			tar -cvf $advance_dir/$file_name.tar $working_dir/$file_name

			# clear the directory
			# do this by creating a trash directory that can be cleared by a separate process

			echo "[ $(date +%c) ] INFO: Moving all daily backups into trash."

			mv $working_dir/* $TRASH_DIR
		else
			# if monthly or weekly move oldest only
			# dont tar, dont delete others

			echo "[ $(date +%c) ] INFO: Moving $file_name to $advance_dir."

			mv $working_dir/$file_name $advance_dir/$file_name
		fi
	fi
	exit 0
}

process_directory daily
process_directory weekly
process_directory monthly

/bin/sh backup-author.sh

exit 0