#!/bin/bash

#######
#
# startup-cq.sh
# This script starts the local CQ instance.
#
#######

service cq5 start

exit 0