#!/bin/bash

#######
#
# clean-startup-cq.sh
# This script removes cluster information from the file system,
# then starts the local CQ instance.
#
#######


#TODO clean logic
# remove cluster files and start up cq out of cluster

# CQ is clean. Start the local instance.
servicq cq5 start

exit 0