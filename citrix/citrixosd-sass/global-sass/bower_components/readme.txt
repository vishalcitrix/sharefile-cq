- To use bower components in your SASS architecture, use something like this:

	add_import_path "../global-sass/bower_components/foundation/scss"
	
- You still need to create foundation.scss to refer to individual bower components. Refer: "/citrixosd-sass/g2a-lp/scss/foundation.scss". 
  This is necessary to use bower components.
  
  vishal.gupta@citrix.com