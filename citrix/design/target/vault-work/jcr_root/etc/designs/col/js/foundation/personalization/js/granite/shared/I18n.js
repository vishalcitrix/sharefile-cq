/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2012 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 *
 */

/**
 * A helper class providing a set of utilities related to internationalization
 * (i18n). Note: for cq localization, make sure to use _g.I18n.get().
 * @static
 * @singleton
 * @class _g.I18n
 */
_g.shared.I18n = function() {

    /**
     * The map where the dictionaries are stored under their locale.
     * @private
     * @type Object
     */
    var dicts = new Object();

    /**
     * The initialization state of the internationalization.
     * @private
     * @type Boolean
     */
    var initialized = false;

    /**
     * The prefix for the URL used to request dictionaries from the server.
     * @private
     * @type String
     */
    var urlPrefix = "/libs/cq/i18n/dict.";

    /**
     * The suffix for the URL used to request dictionaries from the server.
     * @private
     * @type String
     */
    var urlSuffix = ".json";

    /**
     * The current locale.
     * @private
     * @static
     * @type String
     */
    var currentLocale = null;

    var languages = null;

    return {

        /**
         * The default locale (en).
         * @static
         * @final
         * @type String
         */
        LOCALE_DEFAULT: "en",

        /**
         * Initializes the i18n.
         * Example config:
         * <pre>{
         *   "locale": "en",
         *   "urlPrefix": "/apps/i18n/dict.",
         *   "urlSuffix": ".json"
         * }</pre>
         * @param {Object} config The config
         */
        init: function(config) {
            if (!config) {
                config = new Object();
            }
            if (config.locale) {
                _g.I18n.setLocale(config.locale);
            }
            if (config.urlPrefix) {
                _g.I18n.setUrlPrefix(config.urlPrefix);
            }
            if (config.urlSuffix) {
                _g.I18n.setUrlSuffix(config.urlSuffix);
            }
            initialized = true;
        },

        /**
         * Returns the current locale or the default locale if none is defined.
         * @static
         * @return {String} The locale
         */
        getLocale: function() {
            return currentLocale ? currentLocale : _g.I18n.LOCALE_DEFAULT;
        },

        /**
         * Sets the current locale.
         * @static
         * @param {String} locale The locale
         */
        setLocale: function(locale) {
            currentLocale = locale;
        },

        /**
         * Sets the prefix for the URL used to request dictionaries from
         * the server. The locale and URL suffix will be appended.
         * @static
         * @param {String} prefix The URL prefix
         */
        setUrlPrefix: function(prefix) {
            urlPrefix = prefix;
        },

        /**
         * Sets the suffix for the URL used to request dictionaries from
         * the server. It will be appended to the URL prefix and locale.
         * @static
         * @param {String} suffix The URL suffix
         */
        setUrlSuffix: function(suffix) {
            urlSuffix = suffix;
        },

        /**
         * Returns the dictionary for the specified locale. This method
         * will request the dictionary using the URL prefix, the locale,
         * and the URL suffix. If no locale is specified, the current
         * locale is used.
         * @static
         * @param {String} locale (optional) The locale
         * @return {Object} The dictionary
         */
        getDictionary: function(locale) {
            if (!locale) {
                locale = _g.I18n.getLocale();
            }
            if (!dicts[locale]) {
                // CQ.Log.debug("_g.I18n#getDictionary: loading dictionary for locale '{0}'", locale);
                var url = urlPrefix + locale + urlSuffix;
                try {
                    var response = _g.HTTP.get(url);
                    if (_g.HTTP.isOk(response)) {
                        dicts[locale] = _g.Util.eval(response);
                        // CQ.Log.debug("_g.I18n#getDictionary: dictionary for locale '{0}' loaded", locale);
                    }
                } catch (e) {
                    // CQ.Log.warn("_g.I18n#getDictionary: {0}", e.message);
                }
                if (!dicts[locale]) {
                    // CQ.Log.warn("_g.I18n#getDictionary: failed to load dictionary from {0}", url);
                    dicts[locale] = {};
                }
            }
            return dicts[locale];
        },

        /**
         * Translates the specified text into the current language.
         * @static
         * @param {String} text The text to translate
         * @param {String[]} snippets The snippets replacing <code>{n}</code> (optional)
         * @param {String} note A hint for translators (optional)
         * @return {String} The translated text
         * @deprecated use _g.I18n.get instead
         */
        getMessage: function(text, snippets, note) {
            return this.get(text, snippets, note);
        },

        /**
         * Translates the specified text into the current language. Use this
         * method to translate String variables, e.g. data from the server.
         * @static
         * @param {String} text The text to translate
         * @param {String} note A hint for translators (optional)
         * @return {String} The translated text
         * @deprecated use _g.I18n.getVar instead
         */
        getVarMessage: function(text, note) {
            return this.getVar(text, note);
        },

        /**
         * Translates the specified text into the current language.
         * @static
         * @param {String} text The text to translate
         * @param {String[]} snippets The snippets replacing <code>{n}</code> (optional)
         * @param {String} note A hint for translators (optional)
         * @return {String} The translated text
         */
        get: function(text, snippets, note) {
            var dict, newText, lookupText;
            lookupText = note ? text + " ((" + note + "))" : text;
            if (initialized) {
                dict = _g.I18n.getDictionary();
            }
            if (dict) {
                newText = dict[lookupText];
            }
            if (!newText) {
                newText = text;
            }
            //CQ.Log.trace("_g.I18n#get: translating '{0}' with '{1}'", [text, newText]);
            return _g.Util.patchText(newText, snippets);
        },

        /**
         * Translates the specified text into the current language. Use this
         * method to translate String variables, e.g. data from the server.
         * @static
         * @param {String} text The text to translate
         * @param {String} note A hint for translators (optional)
         * @return {String} The translated text
         */
        getVar: function(text, note) {
            if (!text) {
                return null;
            }
            return this.get(text, null, note);
        },

        /**
         * Returns the available languages, including a "title" property with a display name:
         * for instance "German" for "de" or "German (Switzerland)" for "de_ch".
         * @static
         * @return {Object} An object with language codes as keys and an object with "title",
         *                  "language", "country" and "defaultCountry" members.
         * @since 5.4
         */
        getLanguages: function() {
            if (!languages) {
                try {
                    // use overlay servlet so customers can define /apps/wcm/core/resources/languages
                    var json = _g.HTTP.eval("/libs/wcm/core/resources/languages.overlay.infinity.json");
                    _g.$.each(json, function(name, lang) {
                        lang.title = _g.I18n.getVar(lang.language);
                        if (lang.title && lang.country && lang.country != "*") {
                            lang.title += " ("+_g.I18n.getVar(lang.country)+")";
                        }
                    });
                    languages = json;
                } catch (e) {
                    // CQ.Log.error("CQ.I18n#getLanguages failed: " + e.message);
                    languages = {};
                }
            }
            return languages;
        },

        /**
         * Parses a language code string such as "de_CH" and returns an object with
         * language and country extracted. The delimiter can be "_" or "-".
         * @static
         * @param {String} langCode a language code such as "de" or "de_CH" or "de-ch"
         * @return {Object} an object with "code" ("de_CH"), "language" ("de") and "country" ("CH")
         *                  (or null if langCode was null)
         * @since 5.4
         */
        parseLocale: function(langCode) {
            if (!langCode) {
                return null;
            }
            var pos = langCode.indexOf("_");
            if (pos < 0) {
                pos = langCode.indexOf("-");
            }
            
            var language, country;
            if (pos < 0) {
                language = langCode;
                country = null;
            } else {
                language = langCode.substring(0, pos);
                country = langCode.substring(pos + 1);
            }
            return {
                code: langCode,
                language: language,
                country: country
            };
        }

    };

}();

// shortcut
_g.I18n = _g.shared.I18n;