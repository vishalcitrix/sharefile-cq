var ExternalLink = {
    selector: "a[rel=external]",
        
    init: function() {
        $(ExternalLink.selector).unbind("click").click(function() {
            window.open(this.href);
            return false;
        });
    }
};