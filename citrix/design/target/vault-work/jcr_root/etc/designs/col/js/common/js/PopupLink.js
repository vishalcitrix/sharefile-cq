var PopupLink = {
    selector: "a[rel=popup]",
    
    init: function() {
        $(PopupLink.selector).each(function() {
            $(this).unbind("click").click(function() {
                window.open(this.href,'','width=992,height=653');
                return false;
            });
        });
    }
};