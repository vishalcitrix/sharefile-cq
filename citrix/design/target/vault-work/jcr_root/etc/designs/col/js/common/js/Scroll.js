var Scroll = Scroll || {
	init: function() {
		if($("body").hasClass("cq-wcm-edit")) {
			//Do not show the scroll
		}else {
			$(".scrollContainer-container").jScrollPane({showArrows: true});
		}
	}
}
