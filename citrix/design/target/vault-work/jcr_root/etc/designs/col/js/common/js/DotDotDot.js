var DotDotDot = DotDotDot || {
	init: function() {
		$(".ellipsis").each(function() {
			$(this).dotdotdot({
				ellipsis: "... ",
				debug: false
			});
			
			$(this).removeClass("ellipsis");
		});
	}
};