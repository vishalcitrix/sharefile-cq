var LocaleSelector = {
	selector: "#localeSelector",
	
	init: function() {
		$(LocaleSelector.selector).click(function() {
			LocaleSelector.autoOpen = setTimeout('$("#localeSelector").trigger("click");',500);
		 }, function() {
		 	clearTimeout(LocaleSelector.autoOpen);
	 	});
	},

	autoOpen: null
};