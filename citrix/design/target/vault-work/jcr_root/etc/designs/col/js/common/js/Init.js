var Common = Common || {
    init: function() {
        
        Accordion.init();
        CaseStudy.init();
        UtilityNavigation.init();
        Tab.init();
        Print.init();
        Scroll.init();
        
        //Resource
        DotDotDot.init();
        Resource.init();
        
        //Dev only
        Pricing.init();
        ComparePlans.init();
        
        //Global
        StickyHeader.init();
        
        //Links
        CurrentPageReference.init();
        ExternalLink.init();
        Lightbox.init();
        LocaleSelector.init();
        PopupLink.init();
        ToolTip.init();
        ScrollToTop.init();
        Hero.init();
    }
};

jQuery(function() {
    Common.init();
});