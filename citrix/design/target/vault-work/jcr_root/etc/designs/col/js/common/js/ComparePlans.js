var ComparePlans = {
	init: function() {
		ComparePlans.hideAll();
		$('.compare a.plus').each( function() {
	        $(this).click( function() {
	            var indicator = jQuery(this)[0];
	            if(indicator.className.indexOf(Accordion.classNames.categories.active) >= 0)
	                $(this).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
	            else
	                $(this).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
	            $("#" + this.id + "_expand").slideToggle();
	            return false;
	        });
	    });
	},

	hideAll: function() {
        jQuery('.compare > .subgroup').hide();
        jQuery('.compare > a.plus').removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
	}
}