var Tab = Tab || {
	
	selector: "a[rel='tab']",
	
	init: function() {
		$(Tab.selector).each(function() {
			//Set the parent's hover state
			$(this).parent().hover(function() {
				$(this).toggleClass("hover");
			});
			
			//Set the click handler
			$(this).unbind("click").click(function() {
				var anchorTab = $(this);
				var containerId = anchorTab.attr("tab-container-id-link");
				var index = anchorTab.attr("index");
				
				//Update the tab
				$("[tab-container-id-link='" + containerId +"']").each(function() {
					if($(this).attr("index") === index) {
						$(this).parent().addClass("active");
					}else {
						$(this).parent().removeClass("active");
					}
				});
				
				//Update the content
				$("[tab-container-id-content='" + containerId + "']").each(function() {
					if($(this).attr("index") === index) {
						$(this).show();
					}else {
						$(this).hide();
					}
				});
				return false;
			});
		});
	}
}