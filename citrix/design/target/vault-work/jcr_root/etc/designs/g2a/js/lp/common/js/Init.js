var Common = Common || {
	init: function() {
		carousel.init();
		backgroundVideo.init();
		formPlaceHolder.init();
		hashLinkScroll.init();
		Lightbox.init();
		Link.init();
		slideBar.init();
		StickyHeader.init();
		Tab.init();
		videospotlight.init();
	}
};

jQuery(function() {
	setTimeout(Common.init, 250);
});
