var Common = Common || {
	init: function() {
		
	    Accordion.init();
	    backgroundVideo.init();
	    CaseStudy.init();
	    UtilityNavigation.init();
	    Tab.init();
	    
	    //Resource
	    DotDotDot.init();
	    Resource.init();
	    
	    //Dev only
	    Pricing.init();
	    ComparePlans.init();
	    
	    //Global
	    StickyHeader.init();
	    
	    //Links
	    CurrentPageReference.init();
	    ExternalLink.init();
	    Lightbox.init();
	    LocaleSelector.init();
	    PopupLink.init();
		ToolTip.init();
	    ScrollToTop.init();
	    Hero.init();
	    
	    //formBuilder
	    FormBuilder.init();
	},
	
	lightboxInit: function(){
		//formBuilder
	    FormBuilder.init();
	}
};

jQuery(function() {
	Common.init();
});


function detectDevice() {
    var mobRegEx = RegExp(".*(android|iphone|ipad|ipod|iemobile|blackberry).*");
    return mobRegEx.test(navigator.userAgent.toLowerCase());
}