//----------------------------------------------------------
// Videospotlight Component
//----------------------------------------------------------

var videospotlight = videospotlight || { init : function() {
		var videospotlightTrigger = function() {
			$("div.videos-spotlight-container").each(function() {
				var enablejs = $(this).attr("data-enablejs");
				if (enablejs=='true') {
					if (ssize == 'med'){
						$(this).find('.hovercontent').show();
						$(this).unbind('hover');
					} else if (ssize == 'small'){
						$(this).find('.hovercontent').show();
						$(this).unbind('hover');
					} else {
						$(this).find('.hovercontent').hide();
						$(this).hover(function() {
							$(this).find('.hovercontent').fadeIn();
						},
						function() {
							$(this).find('.hovercontent').fadeOut();
						})
					}
				}
			});
		};
		videospotlightTrigger();
		$(window).resize(videospotlightTrigger);
	}
};