// -----------------------------------
// Top Nav + Slidebar Menu 
// vishal.gupta@citrix.com
// -----------------------------------

var slideBar = slideBar || {
	slideBarContainer: "#slidebar-container",
    sliceBarClick: "#menu",
    slideBarMenu: "#slidebar-menu",
    slideBarClose: "#slidebar-menu .icon-close",
    slideBarTimer: 500,
    slideBarRight: "-335px",
    slideBarSubLinks: ".sidebarmenu .sub",
    
    init: function() {
        $(slideBar.sliceBarClick).click(function() {
        	$(slideBar.slideBarContainer).fadeIn();
            $(slideBar.slideBarMenu).animate({right:'0'}, slideBar.slideBarTimer);
        });
        
        $(slideBar.slideBarClose).click(function() {
        	$(slideBar.slideBarContainer).fadeOut();
            $(slideBar.slideBarMenu).animate({right:slideBar.slideBarRight}, slideBar.slideBarTimer);
        });
        
        $(slideBar.slideBarContainer).click(function (event) {
        	$(slideBar.slideBarContainer).fadeOut();
        	$(slideBar.slideBarMenu).animate({right:slideBar.slideBarRight}, slideBar.slideBarTimer);
        });
        
        $(slideBar.slideBarSubLinks).click(function (event) {
        	$(slideBar.slideBarContainer).fadeOut();
        	$(slideBar.slideBarMenu).animate({right:slideBar.slideBarRight}, slideBar.slideBarTimer);
        });
    }, 
    
    defaultHide: function(){
    	 $(slideBar.slideBarMenu).css("right",slideBar.slideBarRight);
    }
};