(function(jQuery) {
    jQuery.fn.stationaryTip = function(options) {
        jQuery.fn.stationaryTip.defaults = {
            tipContent: jQuery(this).attr('href'),
            tipClass: 'stationary',
            hideEvent: 'mouseout',
            hideDelay: 250,
            isFixed: true,
            tipPositionAdjust: { x: 5, y: -12 },
            tipPositionAt: 'right center',
            tipPositionMy: 'left top',
            tipPositionTarget: false,
            tipViewPort: jQuery(window),
            showDelay: 500,
            caretStyle: { border: 2, corner: true, height: 13, mimic: 'left center', offset: 8, width: 7 }
        };
        var opts = jQuery.extend({}, jQuery.fn.stationaryTip.defaults, options);

        // Create the shared properties object for the tooltips
        var shared = {
            content: {
                text: jQuery(opts.tipContent).html()
            },
            events: {
                render: function() {
                    jQuery('.' + opts.tipClass);
                }
            },
            hide: {
                delay: opts.hideDelay,
                event: opts.hideEvent,
                fixed: opts.isFixed
            },
            position: {
                adjust: opts.tipPositionAdjust,
                at: opts.tipPositionAt,
                my: opts.tipPositionMy,
                target: opts.tipPositionTarget,
                viewport: opts.tipViewPort
            },
            show: {
                delay: opts.showDelay,
                solo: true
            },
            style: {
                classes: opts.tipClass,
                tip: opts.caretStyle
            }
        };
        // Render the Tooltip
        jQuery(this).qtip(jQuery.extend(true, shared, {}));
    }
})(jQuery);