var Pricing = {	    
    rsQuantities : "1",
    sdQuantities : "0",
    moQuantities : "0",
    cycle : "mo",
    
    init : function() {
    	Pricing.remoteSupportSlider();
    	$('#rs-slider .ui-slider-handle').attr('id','rs-info');
    	$('#rs-info').html('<div class="info">1 Technician<\/div><div class="price"><input type="text" id="remoteSupportAmount">subscription<\/div>');
    	Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
    	$( "input:radio#monthly-plan" ).attr("checked","true");

    	$(".rs-mid-unselected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$(".rs-top-unselected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$(".rs-top-selected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$(".mo-mid-unselected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$(".mo-top-unselected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$(".mo-top-selected").hover(function(){
    		$(this).addClass('hover-hand')
    	},
    	function () {
    		$(this).removeClass("hover-hand");
    	});
    	$("#yearly-plan").click(function(){
    		Pricing.cycle = 'yr';
    		Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
    	});
    	$("#monthly-plan").click(function(){
    		Pricing.cycle = 'mo';
    		Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
    	});
    	
    	$( "#rs-slider" ).css('width','250px');
    	$( "#rs-slider" ).css('height','50px');
    	$( "#sd-slider" ).css('width','250px');
    	$( "#sd-slider" ).css('height','50px');
    	$( "#mo-slider" ).css('width','80px');
    	$( "#mo-slider" ).css('height','50px');

    	$(".select").click(function(){
    		var podMod = $(this).attr('id');
    		if($("div."+podMod).hasClass("inactive")){
    			$("div."+podMod).removeClass("inactive");
    			$("div."+podMod).addClass("active");
    			if(podMod == "rs"){				
    				$('.ui-slider #rs-info.ui-slider-handle').removeClass('rs-right');
    				if($("#yearly-plan").attr("checked") == true){
    					$( "#sliderRemoteSuppport" ).slider("destroy");
    					Pricing.remoteSupportSliderYearly();
    				}else{
    					Pricing.remoteSupportSlider();
    				}
    				$('#rs-slider .ui-slider-handle').attr('id','rs-info');
    				$('#rs-info').html('<div class="info">1 Technician<\/div><div class="price"><input type="text" id="remoteSupportAmount">subscription<\/div>');
    				Pricing.rsQuantities = '1'
    			}else if(podMod == "sd"){
    				$('.ui-slider #sd-info.ui-slider-handle').removeClass('sd-right');
    				if($("#yearly-plan").attr("checked") == true){
    					$( "#sliderServiceDesk").slider("destroy");
    					Pricing.serviceDeskSliderYearly();
    				}else{
    					Pricing.serviceDeskSlider();
    				}
    				$('#sd-slider .ui-slider-handle').attr('id','sd-info');
    				$('#sd-info').html('<div class="info">1 Technician<\/div><div class="price"><input type="text" id="serviceDeskAmount">subscription<\/div>');
    				Pricing.sdQuantities = '1';
    			}else{
    				if($("#yearly-plan").attr("checked") == true){
    					$( "#mo-slider" ).slider("destroy");
    					Pricing.monitoringSliderYearly();
    				}else{
    					Pricing.monitoringSlider();
    				}
    				$('#mo-slider .ui-slider-handle').attr('id','monitoringInfo');
    				$('#monitoringInfo').html('<div class="info">10 Servers<br>90 Network Devices<\/div><div class="price"><input type="text" id="monitoringAmount">subscription<\/div>');
    				Pricing.moQuantities = '10';
    			}
    			Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
    		}else{
    			if(podMod == "rs"){
    				Pricing.rsQuantities = 0;
    				$("div."+podMod).addClass("inactive");
    			}else if(podMod == "sd"){
    				Pricing.sdQuantities = 0;
    			}else{
    				Pricing.moQuantities = 0;
    			}
    			Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
    			$("div."+podMod).removeClass("active");
    			$("div."+podMod).addClass("inactive");
    		}
    	});
    },

	remoteSupportPricing: function(quantities){
		var remoteSupportPrice= new Array();
		remoteSupportPrice['mo'] = (mo1t * quantities);
		remoteSupportPrice['yr'] = (yr1t * quantities);
			if(region == "GB" && Pricing.cycle == "yr" || region == "AU" && Pricing.cycle == "yr"){
				remoteSupportPrice['yr'] = (parseInt(yr1t) * parseInt(quantities))+parseInt(quantities);
			}
		remoteSupportPrice['savings'] = ((remoteSupportPrice['mo'] * 12) - remoteSupportPrice['yr']);
		return remoteSupportPrice;
	},
 
	serviceDeskPricing: function(quantities){
		var serviceDeskPrice=new Array();
		serviceDeskPrice['mo'] = (mo1sd * quantities);
		serviceDeskPrice['yr'] = (yr1sd * quantities);
		serviceDeskPrice['savings'] = ((serviceDeskPrice['mo'] * 12) - serviceDeskPrice['yr']);
		return serviceDeskPrice;
    },
    
	monitoringPricing: function(quantities){
		var monitoringPrice=new Array();
		monitoringPrice['mo0'] = 0;
		monitoringPrice['mo10'] = mo10s;
		monitoringPrice['mo25'] = mo25s;
		monitoringPrice['yr0'] = 0;
		monitoringPrice['yr10'] = yr10s;
		monitoringPrice['yr25'] = yr25s;
		moQuantity = 'mo'+quantities;
		yrQuantity = 'yr'+quantities;
		monitoringPrice['mo'] = (monitoringPrice[moQuantity]);
		monitoringPrice['yr'] = (monitoringPrice[yrQuantity]);
		monitoringPrice['savings'] = ((monitoringPrice['mo'] * 12) - monitoringPrice['yr']);
		return monitoringPrice;
	},
	
	totalPrice: function(rsQuantities,sdQuantities,moQuantities,cycle){
		rsPrice = Pricing.remoteSupportPricing(rsQuantities);
		sdPrice = Pricing.serviceDeskPricing(sdQuantities);
		moPrice = Pricing.monitoringPricing(moQuantities);
	
		var totals=new Array();
	
		totals['price'] = ((rsPrice[cycle] + sdPrice[cycle]) + moPrice[cycle]);
		totals['savings'] = ((rsPrice['savings'] + sdPrice['savings']) + moPrice['savings']);
		totals['params'] = "?region="+region+"&language="+language;
		if(rsPrice[cycle] != 0 && sdPrice[cycle] != 0 || rsPrice[cycle] != 0 && moPrice[cycle] != 0 || sdPrice[cycle] != 0 && moPrice[cycle] != 0){
			totals['params'] += "&catalog=20";
			if(rsPrice[cycle] != 0){
				if(cycle == "yr"){
					totals['params'] += "&planKeys=27&quantities="+rsQuantities;
				}else{
					totals['params'] += "&planKeys=24&quantities="+rsQuantities;
				}
			}
			if(sdPrice[cycle] != 0){
				if(cycle == "yr"){
					totals['params'] += "&planKeys=67&quantities="+sdQuantities;
				}else{
					totals['params'] += "&planKeys=64&quantities="+sdQuantities;
				}
			}
			if(moPrice[cycle] != 0){
				if(cycle == "yr"){
					if(moQuantities == "25"){
						totals['params'] += "&planKeys=32";
					}else{
						totals['params'] += "&planKeys=31";
					}
				}else{
					if(moQuantities == "25"){
						totals['params'] += "&planKeys=22";
					}else{
						totals['params'] += "&planKeys=21";
					}
				}
			}
		}else if(rsPrice[cycle] != 0 && sdPrice[cycle] == 0 && moPrice[cycle] == 0){
			if(cycle == "yr"){
				totals['params'] += "&catalog=14&planKeys=27&quantities="+rsQuantities;
			}else{
				totals['params'] += "&catalog=14&planKeys=24&quantities="+rsQuantities;
			}
		}else if(sdPrice[cycle] != 0 && rsPrice[cycle] == 0 && moPrice[cycle] == 0){
			if(cycle == "yr"){
				totals['params'] += "&catalog=25&planKeys=67&quantities="+sdQuantities;
			}else{
				totals['params'] += "&catalog=25&planKeys=64&quantities="+sdQuantities;
			}
		}else if(moPrice[cycle] != 0 && sdPrice[cycle] == 0 && rsPrice[cycle] == 0){
			if(cycle == "yr"){
				if(moQuantities == "25"){
					totals['params'] += "&catalog=12&planKeys=32";
				}else{
					totals['params'] += "&catalog=12&planKeys=31";
				}
			}else{
				if(moQuantities == "25"){
					totals['params'] += "&catalog=12&planKeys=22";
				}else{
					totals['params'] += "&catalog=12&planKeys=21";
				}
			}
		}
		if(totals['price'] != 0){
			$( ".buy-it-now, .buy-it-now-text" ).attr('href', 'https://secure.citrixonline.com/commerce/buy'+totals['params']);
			$( '#amount' ).html(pricePrefix+Pricing.delimitNumbers(totals['price'])+"/"+cycle);
			$( ".manage-plans-pricing h5.error" ).hide();
		}else{
			$( ".buy-it-now, .buy-it-now-text" ).attr('href', '#');
			$( "#amount" ).html("");
			$( ".manage-plans-pricing h5.error" ).show();
		}
		$( "#savings" ).html(pricePrefix + Pricing.delimitNumbers(totals['savings']));
		$( "#remoteSupportAmount" ).val(pricePrefix + Pricing.delimitNumbers(parseInt(rsPrice[cycle])) + "/" + cycle);
		$( "#monitoringAmount" ).val(pricePrefix + Pricing.delimitNumbers(parseInt(moPrice[cycle])) + "/" + cycle);
		$( "#serviceDeskAmount" ).val(pricePrefix + Pricing.delimitNumbers(parseInt(sdPrice[cycle])) + "/" + cycle);
	},
	
	remoteSupportSlider: function(){
		$( "#rs-slider" ).slider({
			range: "max",
			value: parseInt(mo1t),
			min: parseInt(mo1t),
			max: parseInt((mo1t*10)),
			step: parseInt(mo1t),
			slide: function( event, ui ) {
				if(parseInt(ui.value) >= parseInt((mo1t*6))){
					$('.ui-slider #rs-info.ui-slider-handle').addClass('rs-right');
				}else{
					$('.ui-slider #rs-info.ui-slider-handle').removeClass('rs-right');
				}
				for(i=1; i <= 10; i++){
					if(parseInt(ui.value) == parseInt((mo1t*i))){
						$( "#rs-info" ).html('<div class="info">'+i+' Technicians<\/div><div class="price"><input type="text" id="remoteSupportAmount">subscription<\/div>');
						Pricing.rsQuantities = i;
					}
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle);
			}
		});
	},
	
	remoteSupportSliderYearly: function(){
		$( "#rs-slider" ).slider("destroy");
		$( "#rs-slider" ).slider({
			range: "max",
			value: parseInt(yr1t),
			min: parseInt(yr1t),
			max: parseInt((yr1t*10)),
			step: parseInt(yr1t),
			slide: function( event, ui ) {
				if(parseInt(ui.value) >= parseInt((yr1t*6))){
					$('.ui-slider #rs-info.ui-slider-handle').addClass('rs-right');
				}else{
					$('.ui-slider #rs-info.ui-slider-handle').removeClass('rs-right');
				}
				for(i=1; i <= 10; i++){
					if(parseInt(ui.value) == parseInt((yr1t*i))){
						$( "#rs-info" ).html('<div class="info">'+i+' Technicians<\/div><div class="price"><input type="text" id="remoteSupportAmount">subscription<\/div>');
						Pricing.rsQuantities = i;
					}
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle)
			}
		});
	},

	serviceDeskSlider: function(){
		$( "#sd-slider" ).slider({
			range: "max",
			value: parseInt(mo1sd),
			min: parseInt(mo1sd),
			max: parseInt((mo1sd*10)),
			step: parseInt(mo1sd),
			slide: function( event, ui ) {
				if(parseInt(ui.value) >= parseInt((mo1sd*6))){
					$('.ui-slider #sd-info.ui-slider-handle').addClass('sd-right');
				}else{
					$('.ui-slider #sd-info.ui-slider-handle').removeClass('sd-right');
				}
				for(i=1; i <= 10; i++){
					if(parseInt(ui.value) == parseInt((mo1sd*i))){
						$( "#sd-info" ).html('<div class="info">'+i+' Technicians<\/div><div class="price"><input type="text" id="serviceDeskAmount">subscription<\/div>');
						Pricing.sdQuantities = i;
					}
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle)
			}
		});
	},
	
	serviceDeskSliderYearly: function(){
		$( "#sd-slider" ).slider("destroy");
		$( "#sd-slider" ).slider({
			range: "max",
			value: parseInt(yr1sd),
			min: parseInt(yr1sd),
			max: parseInt((yr1sd*10)),
			step: parseInt(yr1sd),
			slide: function( event, ui ) {
				if(parseInt(ui.value) >= parseInt((yr1sd*6))){
					$('.ui-slider #sd-info.ui-slider-handle').addClass('sd-right');
				}else{
					$('.ui-slider #sd-info.ui-slider-handle').removeClass('sd-right');
				}
				for(i=1; i <= 10; i++){
					if(parseInt(ui.value) == parseInt((yr1sd*i))){
						$( "#sd-info" ).html('<div class="info">'+i+' Technicians<\/div><div class="price"><input type="text" id="serviceDeskAmount">subscription<\/div>');
						Pricing.sdQuantities = i;
					}
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle)
			}
		});
	},
	
	monitoringSlider: function (){
		$( "#mo-slider" ).slider({
			range: "min",
			value: parseInt(mo10s),
			min: parseInt(mo10s),
			max: parseInt(mo25s),
			step: parseInt((mo25s-mo10s)),
			slide: function( event, ui ) {
				if(parseInt(ui.value) == parseInt(mo25s)){
					$( "#monitoringInfo" ).html('<div class="info">25 Servers<br>225 Network Devices<\/div><div class="price"><input type="text" id="monitoringAmount">subscription<\/div>');
					Pricing.moQuantities = '25';
				}else{
					$( "#monitoringInfo" ).html('<div class="info">10 Servers<br>90 Network Devices<\/div><div class="price"><input type="text" id="monitoringAmount">subscription<\/div>');
					Pricing.moQuantities = '10';
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle)
			}
		});
	},
	
	monitoringSliderYearly: function (){
		$( "#mo-slider" ).slider("destroy");
		$( "#mo-slider" ).slider({
			range: "min",
			value: parseInt(yr10s),
			min: parseInt(yr10s),
			max: parseInt(yr25s),
			step: parseInt((yr25s-yr10s)),
			slide: function( event, ui ) {
				if(parseInt(ui.value) == parseInt(yr25s)){
					$( "#monitoringInfo" ).html('<div class="info">25 Servers<br>225 Network Devices<\/div><div class="price"><input type="text" id="monitoringAmount">subscription<\/div>');
					Pricing.moQuantities = '25';
				}else{
					$( "#monitoringInfo" ).html('<div class="info">10 Servers<br>90 Netowork Devices<\/div><div class="price"><input type="text" id="monitoringAmount">subscription<\/div>');
					Pricing.moQuantities = '10';
				}
				Pricing.totalPrice(Pricing.rsQuantities,Pricing.sdQuantities,Pricing.moQuantities,Pricing.cycle)
			}
		});
	},
	
	delimitNumbers: function (str) {
		return (str + "").replace(/(\d)(?=(\d{3})+(\.\d+|)\b)/g, "$1,");
	}
}
// Default variables for Pricing -- US 
// This javascript is used with the dev only component. Replace varaibles in the javascript section to
// match specific locale for page

var mo1t = parseInt("69");
var yr1t = parseInt("660");
var mo10s = parseInt("89");
var mo25s = parseInt("219");
var yr10s = parseInt("854");
var yr25s = parseInt("2102");
var mo1sd = parseInt("49");
var yr1sd = parseInt("468");
var pricePrefix = "$";
var region = "US";
var language = "en";
