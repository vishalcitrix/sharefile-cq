//----------------------------------------------------------
//Carousel Components trigger script
//----------------------------------------------------------

var carousel = carousel || {
    init : function () {
        var carouselTrigger = function () {
            $(".carousel-primary").each(function () {
                var itemCount = $(this).attr("data-itemCount");
                var showSingleItemOnSmallViewport = $(this).attr("data-showsingleitemonsmallviewport");
                var desktopItems = $(this).attr("data-itemslarge");
                var tabletItems = $(this).attr("data-itemsmedium");
                var mobileItems = $(this).attr("data-itemssmall");
                var navigation = $(this).attr("data-navigation");
                var disableNavInSmallViewport = $(this).attr("data-disablenavinsmallviewport");
                var disableNavInMediumViewport = $(this).attr("data-disablenavinmediumviewport");
                var autoPlay = $(this).attr("data-autoplay");
                var rewindNavigation = $(this).attr("data-rewindnav");
                var theme = $(this).attr("data-theme");
                var noSpaceoption = $(this).attr("data-nospace");
                var disablePaginationOverlap = $(this).attr("data-disableoverlap");
                var disableNavigationOverlap = $(this).attr("data-disablenavoverlap");
                var id = "#" + $(this).attr("id");
                var play = false;
                var rewindC = false;
                var nav= false;
                
                if(navigation == 'true') {
                    nav= true;
                } 
                if(ssize == 'small' && disableNavInSmallViewport == 'true') {
                	nav= false;
                }
                if(ssize == 'med' && disableNavInMediumViewport == 'true') {
                	nav= false;
                }
                
                if(autoPlay == 'true') {
                    play  = true;
                }

                if(rewindNavigation == 'true') {
                    rewindC = true;
                }
                
                if (disableNavigationOverlap) {
                    theme=theme+'  owl-theme-features';
                }
                
                if (ssize == 'small' && showSingleItemOnSmallViewport == 'true') //Initialize Owl Carousel Plugin for Small viewport while first item needs to be displayed
                {
						var owl =  $(id).data('owlCarousel');
						if(owl) {
							owl.destroy(); // destroy if any existing Owl plugin config data is present, Since in window resize configuration could be changed for Viewport   
						}
 
						$(id).owlCarousel({
							 items : 1,
							 singleItem : true,
							 mouseDrag : false,
							 touchDrag : false,
							 theme : theme,
							 navigation : false,
							 pagination : false,
							 autoPlay : false,
							 rewindNav : false,
							 responsive : true
						});
                   }
                  else //Initialize Owl Carousel Plugin 
                	  {
						var owl =  $(id).data('owlCarousel');
						if(owl) {
							owl.destroy();// destroy if any existing Owl plugin config data is present, Since in window resize configuration could be changed for Viewport
						}
                     
						$(id).owlCarousel({
							 items : desktopItems,
						     itemsDesktop : [1921, desktopItems],
						     itemsTablet : [1024, tabletItems],
						     itemsMobile : [640, mobileItems],
						     theme : theme,
						     navigation : nav,
						     slideSpeed : 300,
						     paginationSpeed : 400,
						     autoPlay : play,
						     rewindNav : rewindC,
						     responsive : true,
						     navigationText : false,
						     stopOnHover : true
						});
                   }

                if (disablePaginationOverlap) {
                    $(id).find('div.owl-pagination').css("bottom", "0");
                }

                if (noSpaceoption) {
                    var windowWidth = $(window).width();
                    var itemWidth = windowWidth / itemCount;
                    if (ssize != 'small') {
                        $(id).find(".owl-item").each(function () {
                            $(this).css("width", itemWidth + 'px');
                        });
                    }
                }
            });
        };
        
        carouselTrigger();
        $(window).resize(carouselTrigger);
    }
};
