var Common = Common || {
    init: function() {
        Accordion.init();
        categoryLinkSelector.init();
        formBuilder.init();
        formPlaceHolder.init();
        hashLinkScroll.init();
        Link.init();
        styleSwitcher.init();
        footerNav.init();
        utilityNav.init();
        if((typeof dnt !== 'undefined' && typeof dnt.dntTrue === 'function') ? !dnt.dntTrue() : true) {
			Common.dntInit();
		}
    },
    lightboxInit: function() {
        styleSwitcher.init();
    },
    dntInit: function() {
        formUrlParam.init();
    }
};

jQuery(function() {
    Common.init();
});