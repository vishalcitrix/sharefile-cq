var formBuilder = formBuilder || {
	formError: false,
	init: function() {
		var formName = '';
		
		$('.custom-form #password').focus(function() {
			$(this).prop("type","password");
		});		
		$('.custom-form #password').focusout(function() {
			if($(this).val() == "") {
				$(this).prop("type","text");
			}
		});	
		$(".custom-form form").submit(function() {
			//remove all warning text and error class
			formBuilder.formError = false;
			formName = $(this).attr('name');

			//remove all warning text and error class
			$('input,select,textarea',this).removeClass('error');
			$('.err',this).remove();
			$('.required',this).each(function() {
				if($(this).is(':visible')) {
					// it's visible, do something
					if((!$(this).val()) || ($(this).is(':checkbox') && !$(this).is(':checked'))){
						formBuilder.formError = true;
						$(this).addClass("error");
						if($(this).is(':checkbox')) {
							$('<div class="err">This field is required</div>').insertAfter($('.check-box'));
						}else {
							$('<div class="err">This field is required</div>').insertAfter($(this));
						}
					}
				}
			});
			
			$('input[type=text],input[type=password]',this).each(function() {
				//do constraint verification
				formBuilder.checkTextFieldConstraint($(this), formName);
			});
			
			if($('.supportForm').length && !$(".supportForm form input[type='radio']:checked").val()){
				$("#formErrorMsg").show();
				$('<div class="err">This field is required</div>').insertBefore($('.radioGroup'));
				formBuilder.formError = true;
            }
			
			if(formBuilder.formError) {
				$("#formErrorMsg").show();
				return false;
			}else {
				$("#formErrorMsg").hide();
				$(this).find('input[type=submit]').attr('disabled', 'disabled');
				if($.browser.msie && $.browser.version < 8) {
					$(this).find('input[type=submit]').attr('disabled', true);
					$(this).find('input[type=submit]').addClass('disabled');
				}
			}

			//check for form field names
			formBuilder.checkFormNames();
		});
	},

	//function to check textfield constraints
	checkTextFieldConstraint: function(tfObj, formName) {
		var tfValue = tfObj.val();
		var tfName = tfObj.attr('name');
		var tfConstraint = tfObj.attr('constraint');
		var tfTrackError = tfObj.attr('trackerror');
		var tfErrorMsg = tfObj.attr('errormsg');
		var fieldError = false;
		if(tfValue){
			switch(tfConstraint) {
			case "email":
				var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(!emailRegex.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter valid email address';
					}
				}
				break;
			case "contact":
				var emailRegexContact = /((([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)|(1?\W*([2-9][0-8][0-9])\W*([2-9][0-9]{2})\W*([0-9]{4})(\se?x?t?(\d*))?))/;
				if(!emailRegexContact.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter a valid Email or Number';
					}
				}
				break;
			case "numeric":
				var numberRegex = /[0-9 -()+]+$/;
				if(!numberRegex.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter a number';
					}
				}
				break;
			case "date":
				var dateRegex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
				if(!dateRegex.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter valid date';
					}
				}
				break;
			case "eventId":
				var eventIdRegex = /^\d{9}$/;
				$(tfObj).val(tfValue.replace(/-/g, ""));
				tfValue = tfObj.val();
				if(!eventIdRegex.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter a valid 9 digit ID';
					}
				}
				break;
			case "phoneNumber":
				var phoneRegEx = /^\(\d{3}\) ?\d{3}( |-)?\d{4}|^\d{3}( |-)?\d{3}( |-)?\d{4}$}/;
				if(!phoneRegEx.test(tfValue)) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter a valid phone number';
					}
				}
				break;
			case "password":
				var pwdRegex = /^(?=.{8,})(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).*$/;
				if( !(pwdRegex.test(tfValue) && tfValue.length >= 8) ) {
					fieldError = true;
					tfObj.addClass("error");
					if(tfErrorMsg == ''){
						tfErrorMsg = 'Please enter a valid password';
					}
				}
				break;
			default:
				if(tfObj.hasClass("required")) {
					if(tfObj.hasClass("prefill")) {
						fieldError = true;
						tfObj.addClass("error");
						if(tfErrorMsg == ''){
							tfErrorMsg = 'Please enter a text';
						}
					}
				}
			break;
			}

			if(fieldError && tfTrackError == 'true') {
				ga('send', 'GAevent', 'form errors', formName, tfName);
			}
			if(fieldError) {
				$('<div class="err">'+tfErrorMsg+'</div>').insertAfter(tfObj);
			}
			if(fieldError || this.formError) {

				this.formError = true;
			}
		}
	},

	//function to show errors to authors
	checkFormNames: function() {
		var hmap = new Object();
		var msg = "";
		$('form').each(function(){
			$('input, select',this).each(function() {
				var inputName = $(this).attr('name');
				if(typeof(hmap[inputName]) == 'undefined') {
					hmap[inputName] = 1;
				}else {
					hmap[inputName] = hmap[inputName] + 1;
				}
			});

			$.map(hmap, function(value,key) {
				if(value > 1){
					if(key == ''){
						msg = msg + "There are " + value + " fields with no name <br />";
					}else{
						msg = msg + "There are " + value + " fields with name " + key + "<br />";
					}
				}
			});

			//display error messages
			$('.warning',this).append(msg);
		});
	}
};

function clearElement(theObject, defaultText, refill) {
	theAction = ( refill && ( ( theObject.value == ""   ) || ( theObject.value == defaultText ) ) ) ? 'add' : 'remove';
	eventHandlerClass( theAction, theObject, 'prefill' );
	if(refill && theObject.value == "")
		theObject.value = defaultText;
	else if( theObject.value == defaultText )
		theObject.value = "";
}

function eventHandlerClass(theAction,theObj,class1,class2) {
	switch( theAction ) {
	case 'add':
		if(!eventHandlerClass('check',theObj,class1)){theObj.className+=theObj.className?' '+class1:class1;}
		break;
	case 'remove':
		var rep = theObj.className.match(' '+class1) ? ' ' + class1 : class1;
		theObj.className = theObj.className.replace(rep, '');
		break;
	case 'check':
		return new RegExp('\\b'+class1+'\\b').test(theObj.className)
		break;
	}
}