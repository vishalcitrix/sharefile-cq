var Link = Link || {
	init: function() {
		if(typeof CurrentPageReference != 'undefined') {
			CurrentPageReference.init();
		}
		if(typeof ExternalLink != 'undefined') {
			ExternalLink.init();
		}
		if(typeof Lightbox != 'undefined') {
			Lightbox.init();
		}
		if(typeof LocaleSelector != 'undefined') {
			LocaleSelector.init();
		}
		if(typeof PopupLink != 'undefined') {
			PopupLink.init();
		}
		if(typeof ToolTip != 'undefined') {
			ToolTip.init();
		}
		if(typeof ScrollToTop != 'undefined') {
			ScrollToTop.init();
		}
		if(typeof Hero != 'undefined') {
			Hero.init();
		}
	}
};