var Common = Common || {
	init: function() {
	    UtilityNavigation.init();
	    
	    //Links
	    ExternalLink.init();
	    Lightbox.init();
	    LocaleSelector.init();
	    FormBuilder.init();
	}
};

jQuery(function() {
	Common.init();
});