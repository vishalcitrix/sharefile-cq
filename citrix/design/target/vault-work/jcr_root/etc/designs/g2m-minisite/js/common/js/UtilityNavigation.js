var UtilityNavigation = UtilityNavigation || {
	
	selector: ".utilityNavigation",
	
	salesChatAsyncSelector: ".utility-navigation-sales-chat-async",
	
	init: function() {
		var asyncLink = $(UtilityNavigation.salesChatAsyncSelector).attr("href");
		$(UtilityNavigation.selector).find("a.scheduled-sales-chat").each(function() {
			var scheduleSalesChat = $(this);
			if(scheduleSalesChat.attr("initialized") != "true") {
				$.getJSON(asyncLink, function(data) {
					if(data != null) {
						if(data.scheduled === "false" || data.scheduled === false) {
							var isEditMode = $(UtilityNavigation.salesChatAsyncSelector).attr("editmode");
							if(isEditMode === "true" || isEditMode === true) {
								//Do nothing; This is edit mode, allow authors to see the link
                                scheduleSalesChat.addClass("offtime");
							}else {
								scheduleSalesChat.hide();
								var scheduleSalesChatParent = scheduleSalesChat.parent();
								var possibleSplitter = scheduleSalesChatParent.next();
                                if(possibleSplitter != null && (possibleSplitter.hasClass("splitter") || possibleSplitter.find('a').html().charAt(0) == "|")) {
									possibleSplitter.hide();
								}
							}
						}else {
							//Do nothing; Current time is valid
						}
					}
				});
				scheduleSalesChat.attr("initialized", "true");
			}
		});
	}
};