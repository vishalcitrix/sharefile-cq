var FormBuilder = FormBuilder || {
    init: function() {
    var error = false;
    $(".custom-form form").submit(function() {
        //remove all warning text and error class
        error = false;
        //remove all warning text and error class
        $('input,select,textarea',this).removeClass('error');
        $('.err',this).remove();
        $('.required',this).each(function() {
            if( $(this).is(':visible') ) {
                // it's visible, do something
                if((!$(this).val()) || ($(this).is(':checkbox') && !$(this).is(':checked'))){
                    error = true;
                    $(this).addClass("error");
                    if($(this).is(':checkbox')){
                        $('<div class="err">This field is required</div>').insertAfter($('.check-box'));
                    }else{
                        $('<div class="err">This field is required</div>').insertAfter($(this));
                    }
                }
            }
        });

        $('input[type=text]',this).each(function() {
            //do constraint verification
           error = FormBuilder.checkTextFieldConstraint($(this),error);
        });

        if (error) {
                return false;
        }

        //check for form field names
        FormBuilder.checkFormNames();
    });

     $(function() {
          $(".datePicker").datepicker();
     });
   },

   //function to check textfield constraints
    checkTextFieldConstraint: function(tfObj,error) {
        var tfValue = tfObj.val();
        var tfName = tfObj.attr('name');
        var tfConstraint = tfObj.attr('constraint');
        if(tfValue){
            switch(tfConstraint) {
                case "email":
                    var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(!emailRegex.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        $('<div class="err">Please enter valid email address</div>').insertAfter(tfObj);
                    }
                    break;
                case "numeric":
                    var numberRegex = /[0-9 -()+]+$/;
                    if(!numberRegex.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        $('<div class="err">Please enter a number </div>').insertAfter(tfObj);
                    }
                    break;
                case "date":
                    var dateRegex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
                    if(!dateRegex.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        $('<div class="err">Please enter valid date</div>').insertAfter(tfObj);
                    }
                    break;
                case "eventId":
                	var eventIdRegex = /^\d{9}$/;
                	$(tfObj).val(tfValue.replace(/-/g, ""));
                	tfValue = tfObj.val();
                	if(!eventIdRegex.test(tfValue)) {
                		error = true;
                		tfObj.addClass("error");
                		$('<div class="err">Please enter a valid 9 digit ID</div>').insertAfter(tfObj);
                	}
                	break;
                default:
                    break;
            }
        }
        return error;
    },

    //function to show errors to authors
    checkFormNames: function() {
        var hmap = new Object();
        var msg = "";
        $('form').each(function(){
            $('input, select',this).each(function() {
                var inputName = $(this).attr('name');
                if(typeof(hmap[inputName]) == 'undefined'){
                    hmap[inputName] = 1;
                }else{
                    hmap[inputName] = hmap[inputName] + 1;
                }
            });

            $.map(hmap, function(value,key) {
                if(value > 1){
                    if(key == ''){
                        msg = msg + "There are " + value + " fields with no name <br />";
                    }else{
                        msg = msg + "There are " + value + " fields with name " + key + "<br />";
                    }
                }
            });

            //display error messages
            $('.warning',this).append(msg);
        });
    }
};

function clearElement(theObject, defaultText, refill) {
    theAction = ( refill && ( ( theObject.value == ""   ) || ( theObject.value == defaultText ) ) ) ? 'add' : 'remove';
    eventHandlerClass( theAction, theObject, 'prefill' );
    if( refill && theObject.value == "" )
        theObject.value = defaultText;
    else if( theObject.value == defaultText )
        theObject.value = "";
}

function eventHandlerClass(theAction,theObj,class1,class2) {
    switch( theAction ) {
        case 'add':
            if(!eventHandlerClass('check',theObj,class1)){theObj.className+=theObj.className?' '+class1:class1;}
            break;
        case 'remove':
            var rep = theObj.className.match(' '+class1) ? ' ' + class1 : class1;
            theObj.className = theObj.className.replace(rep, '');
            break;
        case 'check':
            return new RegExp('\\b'+class1+'\\b').test(theObj.className)
            break;
    }
}