// ---------------------------------------
// Column control seperator || sets max available height to all columns
// ---------------------------------------
var columnControl = {
    setColHeight: function(){
        $(".col-control").each(function(){
            var columns = $(this).find(".row .columns");
            if( columns.eq(1).hasClass("colSeparator") ){ // only affects to which col-control has separator
                columns.css("height",""); // resets on resize
                var maxHeight = Math.max.apply( Math, columns.map( function(){ return $(this).height();}) );
                $(".row").addClass("clearfix");
                $(columns).each(function(){
                    if ( (ssize =="med" && $(this).attr("class").indexOf("medium-") < 0) || (ssize =="small" && $(this).attr("class").indexOf("small-") < 0) ) // this is to check the columns are stack
                        $(this).css("height","");
                    else
                        $(this).height(maxHeight);
                });
            }
        });
    },
    init: function() {
        if(ssize != "small" && $(".col-control .colSeparator").length > 0 ){
            columnControl.setColHeight();
        }
        
        var colCtrlT;
            $(window).bind('resize',function(){
                clearTimeout(colCtrlT);
                colCtrlT = setTimeout(function(){
                    columnControl.setColHeight();
                },300)
            });
        
    }
};