var Common = Common || {
    init: function() {
        columnControl.init();
        dotDotDot.init();
        showMore.init();
    }
};

jQuery(function() {
    Common.init();
});