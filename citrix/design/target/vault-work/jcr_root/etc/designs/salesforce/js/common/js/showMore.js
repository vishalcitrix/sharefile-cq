//Partner's page Read more accordian

var showMore = showMore ||  {
    init : function() {
        $('.show-more').click(function() {
           if($(this).text() != 'Learn More'){
				$(this).parent().prevAll('.show-more-snippet').animate({height: '70px'}, 200);
				$(this).parent().prevAll('.fadeout').animate({height: '21px'}, 200); 
				$(this).css({marginTop: '-14px'}, 200);
				$(this).text('Learn More');
            }else{
	            $(this).parent().prevAll('.show-more-snippet').css({height:'100%'});
	            $(this).parent().prevAll('.show-more-snippet').css({width:'100%'});
	            $(this).parent().prevAll('.fadeout').animate({height: '0'}, 200);
	            $(this).css({marginTop: '0'}, 200);  
	            $(this).text('Less');
             }
        });     
    },
};