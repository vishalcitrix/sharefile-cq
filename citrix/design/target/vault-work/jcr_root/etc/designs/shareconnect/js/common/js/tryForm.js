var tryForm = tryForm || {
    init: function() {
        var error = false,
            theForm,
            formName = '',
            baseUrl = 'https://app.shareconnect.com',
            browser = navigator.userAgent,
            IEversion = 99;
        
        if(browser.indexOf("MSIE") > 1) {
            IEversion = parseInt(browser.substr(browser.indexOf("MSIE") + 5, 5));
        }
        
        if(window.location.hostname.indexOf('www.shareconnect.com') == -1){
            baseUrl = 'https://i1app.sc.test.expertcity.com';
        }

       $('.trial-form #RegPassword').focus(function(){
            $(this).prop("type","password");
            $(".error-message").hide();
            $(this).prev('.password-message').slideDown(500, function(){
                $(this).css('overflow', 'visible');
            });
        });

        $('.trial-form #RegPassword').prop("type","text");
        $('.trial-form #RegPassword').focusout(function(){
            if($(this).val() == "") {
                $(this).prop("type","text");
            }
        });
        
        $('.trial-form #RegPassword').on('keyup', function(){
            var pwdRegex = /^(?=.{8,})(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).*$/,
            password = $(this).val(),
            passed = pwdRegex.test( password );

            if(password.length >= 8 && passed) {
               $(this).prev('.password-message').addClass('passed').delay(3200).fadeOut(300);
            }else {
                $(this).prev('.password-message').removeClass('passed');
                return false;
            }
        });
                
        var scFormSubmit = function (){
            //remove all warning text and error class
            error = false;
            theForm = $(this);
            formName = $(this).attr('name');

            $(this).find('#RegFormSubmit').attr('disabled','disabled');

            //remove all warning text and error class
            $(this).find('input,select,textarea').removeClass('error');
            $('.err',this).remove();
            $(this).find('.required').each(function() {
                if( $(this).is(':visible') ) {
                    // it's visible, do something
                    if((!$(this).val()) || ($(this).is(':checkbox') && !$(this).is(':checked'))){
                        error = true;
                        $(this).addClass("error");
                        if($(this).is(':checkbox')){
                            $('<div class="err">This field is required</div>').insertAfter($('.check-box'));
                        }else{
                            $('<div class="err">This field is required</div>').insertAfter($(this));
                        }
                    }
                }
            });

            $('input[type=text],input[type=password]',this).each(function() {
                //do constraint verification
                error = tryForm.checkTextFieldConstraint($(this),error,formName);
            });

            if(error) {
                $(".error-message").show();
                $(theForm).find('#RegFormSubmit').removeAttr('disabled');
                return false;
            }else {
                if($(theForm ).find('.err').length == 0) {
                    $(theForm).find('#RegFormSubmit').addClass('disabled');
                }
                $(theForm).find(".error-message").hide();

                var firstName = $(theForm).find('#FirstName').val(),
                    lastName = $(theForm).find('#LastName').val(),
                    password = $(theForm).find('#RegPassword').val(),
                    email = $(theForm).find('#Email').val(),
                    trackSrc = '',
                    trackAllSrc = '';

                if( typeof(formUrlParam) == 'object' ){
                    if( typeof(formUrlParam.src) != 'undefined') 
                        trackSrc = formUrlParam.src;
                    if( typeof(formUrlParam.allSources) != 'undefined') 
                        trackAllSrc = formUrlParam.allSources;
                }
                    
                var regUrl = baseUrl + '/rest/registration';   
                
                if(IEversion < 10) { 
                    xdr = new XDomainRequest();
                    xdr.open("POST", regUrl);
                    xdr.send('operation=post&email='+email+'&firstName='+firstName+'&lastName='+lastName+'&clientOS=web&password='+ password+'&src='+trackSrc+'&allSources='+trackAllSrc);
                    xdr.onload = function () { 
                        tryForm.parseSuccessResponse($.parseJSON(xdr.responseText),theForm,baseUrl);
                    };    
                    xdr.onerror = function () {
                        tryForm.parseErrorResponse(theForm);
                    }
                }else {
                    $.ajax({
                        type: "POST",
                        url: regUrl,
                        data: {'operation': 'post', 'email': email, 'firstName' : firstName, 'lastName' : lastName, 'clientOS' : 'web', 'password' : password, 'src' : trackSrc, 'allSources' : trackAllSrc},
                        crossDomain: 'false',
                        dataType: 'json',
                        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                        jsonpCallback: 'loaded',
                        success: function(data) {
                            tryForm.parseSuccessResponse(data,theForm,baseUrl);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            tryForm.parseErrorResponse(theForm);
                        }
                    });
                }
                return false;
            }
        }
        
        $(".lpTryForm,.tryForm form").submit(scFormSubmit);
    },

    addLightboxContent: function(content){
        $("#lightbox-dynamic").empty();
        $("#lightbox-dynamic").append('<div>' + content + '</div>');
    },

    showLightbox: function(){
        Common.lightboxInit(); 
        $("#lightbox-dynamic").show();
        $("#lightbox-border").addClass($("#lightbox-content").attr("class"));
        tryForm.bindHideLightbox();
        $("#lightbox-container").fadeIn(100);
    },

    bindHideLightbox: function(){
        $("div#lightbox-close").unbind("click").click(function() {
            $("#lightbox-container").fadeOut(100, function() {
                $("#lightbox-content").remove();
                $("#lightbox-dynamic").hide();
                $("#lightbox-border").removeClass().removeAttr("style");
            });
        });
        $("div#lightbox-container").unbind("click").click(function() {
            $("#lightbox-container").fadeOut(100, function() {
                $("#lightbox-content").remove();
                $("#lightbox-dynamic").hide();
                $("#lightbox-border").removeClass().removeAttr("style");
            });
        });
        $("div#lightbox-container div").not(".brightcoveVideo div").click(function(e) {
            e.stopPropagation();
        });
   },
   
   checkTextFieldConstraint: function(tfObj, error, formName) {
        var tfValue = tfObj.val();
        var tfName = tfObj.attr('name');
        var tfConstraint = tfObj.attr('constraint');
        var tfTrackError = tfObj.attr('trackerror');
        var tfErrorMsg = tfObj.attr('errormsg');
        if(tfValue){
            switch(tfConstraint) {
                case "email":
                    var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(!emailRegex.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        if(tfErrorMsg == ''){
                            tfErrorMsg = 'Please enter valid email address';
                        }
                    }
                    break;
                case "password":
                    var pwdRegex = /^(?=.{8,})(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).*$/;
                    if( !(pwdRegex.test(tfValue) && tfValue.length >= 8) ) {
                        error = true;
                        tfObj.addClass("error");
                        if(tfErrorMsg == ''){
                            tfErrorMsg = 'Please enter a valid password';
                        }
                    }
                    break;
              
               case "phoneNumber":
                    var phoneRegEx = /^\b\d{3}[-.]?\d{3}[-.]?\d{4}\b$/;
                    if(!phoneRegEx.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        if(tfErrorMsg == ''){
                            tfErrorMsg = 'Please enter a valid phone number';
                        }
                    }
                break;
                
              case "name":
                    var nameRegEx = /^[a-zA-Z][a-zA-Z]+$/;
                    if(!nameRegEx.test(tfValue)) {
                        error = true;
                        tfObj.addClass("error");
                        if(tfErrorMsg == ''){
                            tfErrorMsg = 'Please enter a valid name';
                        }
                    }
                break;
            }

            if(error && tfTrackError == 'true'){
                ga('send', 'GAevent', 'form errors', formName, tfName);
            }
           
            if(error){
                $('<div class="err">'+tfErrorMsg+'</div>').insertAfter(tfObj);
            }
        }
        return error;
    },
    parseSuccessResponse: function(data,theForm,baseUrl) {
        if(typeof data.Responses !== 'undefined') {
            if (data.Responses[0].responseCode == 'Success') {
                var regResponse = data.Responses[1];
                var domain = regResponse.domain;
                var subDomain = regResponse.subDomain;
                var accessCode = regResponse.accessCode;
                var loginUrl = baseUrl + '/members/socialLogin.tmpl?state=processLogin%2cshareFile&signUpFlow=true&code=' + accessCode + '&subdomain=' + subDomain + '&appcp=' + domain+ '&apicp=' + domain;

                // add tracking
                dataLayer.push({
                    'transactionId': subDomain,
                    'transactionTotal': '0.00',
                    'transactionProducts': [{ 'id': subDomain, 'sku': 'free', 'category': '', 'name': 'ShareConnect Trial', 'price': '0.00', 'quantity': '1' }]
                });
                window.location = loginUrl;
            }else if ((data.Responses[0].responseCode == 'MemberExists')) {
                email = $(theForm).find('#Email').val();
                tryForm.addLightboxContent('<b>' + email + '</b> is already registered with ShareFile. You can sign into ShareConnect with the same user name and password that you use for ShareFile.');
                tryForm.showLightbox();
                $(theForm).find('#RegFormSubmit').removeClass('disabled');
                $(theForm).find('#RegFormSubmit').removeAttr('disabled');
                $(theForm).find('#RegFormSubmit').text('SIGN UP');
            }else if ((data.Responses[0].responseCode == 'IncorrectPasswordFormat')) {
                tryForm.addLightboxContent("Password must contain 8 characters, at least 1 uppercase letter, 1 lowercase letter and 1 digit.");
                $(theForm).find('#RegFormSubmit').removeClass('disabled');
                tryForm.showLightbox();
                $('#RegPassword').val("");
            }else if ((data.Responses[0].responseCode == 'ShareFileUserCreationDisabled')) {
                tryForm.addLightboxContent('Sign Up Failed.  New users cannot be created at this time. Please try again in a few minutes.');
                $('#RegFormSubmit').removeClass('disabled');
                tryForm.showLightbox();
                $(theForm).find('#RegFormSubmit').removeClass('disabled');
                $(theForm).find('#RegFormSubmit').removeAttr('disabled');
                $(theForm).find('#RegFormSubmit').text('SIGN UP');
            }else {
                tryForm.addLightboxContent('Sign Up Failed.  Your request could not be processed at this time. Please try again later.');
                tryForm.showLightbox();
                $(theForm).find('#RegFormSubmit').removeClass('disabled');
                $(theForm).find('#RegFormSubmit').removeAttr('disabled');
                $(theForm).find('#RegFormSubmit').text('SIGN UP');
            }
        }
    },
    parseErrorResponse: function(theForm) {
        tryForm.addLightboxContent('Sign Up Failed.  Your request could not be processed at this time. Please try again later.');
        tryForm.showLightbox();
        $('#RegFormSubmit').removeClass('disabled');
        $(theForm).find('#RegFormSubmit').removeAttr('disabled');
        $(theForm).find('#RegFormSubmit').text('SIGN UP');
    }
};

function clearElement(theObject, defaultText, refill) {
    theAction = ( refill && ( ( theObject.value == ""   ) || ( theObject.value == defaultText ) ) ) ? 'add' : 'remove';
    eventHandlerClass( theAction, theObject, 'prefill' );
    if(refill && theObject.value == "")
        theObject.value = defaultText;
    else if( theObject.value == defaultText )
        theObject.value = "";
}

function eventHandlerClass(theAction,theObj,class1,class2) {
    switch( theAction ) {
    case 'add':
        if(!eventHandlerClass('check',theObj,class1)){theObj.className+=theObj.className?' '+class1:class1;}
        break;
    case 'remove':
        var rep = theObj.className.match(' '+class1) ? ' ' + class1 : class1;
        theObj.className = theObj.className.replace(rep, '');
        break;
    case 'check':
        return new RegExp('\\b'+class1+'\\b').test(theObj.className)
        break;
    }
}