var Common = Common || {
	init: function() {
		Accordion.init();
		Link.init();
		styleSwitcher.init();
		floatingFooterScroll.init();
		formPlaceHolder.init();
		formUrlParam.init();
		tryForm.init();
		floatingFooter.init();
		backgroundVideo.init();
		redirect.init();
		categoryLinkSelector.init();
		hashLinkScroll.init();
	},
	lightboxInit: function(){
		styleSwitcher.init();
	}
};

jQuery(function() {
	Common.init();
});


