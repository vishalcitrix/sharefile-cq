jQuery("a.annualPlan").click(function() {
    jQuery('.annualPlan').addClass('annualSelected');
    jQuery('.annualPrice').removeClass('hiddenContent');
    jQuery('.monthlyPrice').addClass('hiddenContent');
    jQuery('.monthlyPlan').addClass('monthlyDefault');
});

jQuery("a.monthlyPlan").click(function() {
    jQuery('.monthlyPrice').removeClass('hiddenContent');
    jQuery('.monthlyPlan').removeClass('monthlyDefault');
    jQuery('.annualPrice').addClass('hiddenContent');
    jQuery('.annualPlan').removeClass('annualSelected');
});

var Pricing = {
    init : function() {
        var myPricing = 'div#moreCompPers, div#moreCompPro';
        var persId = 'persNumber', proId = 'proNumber';
        if (jQuery('#persPrices').length > 0) {
            var persDataEl = JSON.parse( jQuery.trim(jQuery('#persPrices').text()) );
            var proDataEl =  JSON.parse( jQuery.trim(jQuery('#proPrices').text()) );
            var priceObj = {
                pers: persDataEl,
                pro: proDataEl
            };
    
            jQuery(myPricing).overlay({
                expose: { color:'#fff', opacity:0.01 },
                fixed: false,
                top: 333,
                left: 659
            });
    
            jQuery('select').bind('change keyup', function() {
                var monthlyTextEl, monthlyCompEl, annualTextEl, annualSaveEl, pricingSalesEl, buttonEl, persOrPro;
                if(this.id == persId) {
                    monthlyTextEl = '#persMoAmount';
                    monthlyCompEl = '#persPerComp';
                    annualTextEl = '#persYrAmount';
                    annualSaveEl = '#persYrSave';
                    pricingSalesEl = '#persContSales';
                    buttonEl = '.row7col2';
                    persOrPro = 'pers';
                    if(this.value == 21) {
        				jQuery('.more-comp-pers-link').click();
    					jQuery("#lightbox-content").css("overflow", "hidden");
    					this.value = 1;
                    }
                }
                else if(this.id == proId) {
                    monthlyTextEl = '#proMoAmount';
                    monthlyCompEl = '#proPerComp';
                    annualTextEl = '#proYrAmount';
                    annualSaveEl = '#proYrSave';
                    pricingSalesEl = '#proContSales';
                    buttonEl = '.row7col4';
                    persOrPro = 'pro';
                    if(this.value == 51) {
                    	jQuery('.more-comp-pro-link').click();
    					jQuery("#lightbox-content").css("overflow", "hidden");
    					this.value = 2;
                    }
                }
                var monthlyPrice = priceObj[persOrPro].col[this.value-1].monthly;
                var monthlyPerComp = priceObj[persOrPro].col[this.value-1].computer;
                var annualPrice = priceObj[persOrPro].col[this.value-1].annual;
                var annualSave = priceObj[persOrPro].col[this.value-1].save;
                var pricingSales = priceObj[persOrPro].col[this.value-1].contact;
    
                jQuery(monthlyTextEl).text(monthlyPrice);
                jQuery(monthlyCompEl).text(monthlyPerComp);
                jQuery(annualTextEl).text(annualPrice);
                jQuery(annualSaveEl).text(annualSave);
                jQuery(pricingSalesEl).text(pricingSales);
                if(priceObj[persOrPro].col[this.value-1].buttonSwap) {
                    jQuery(buttonEl).addClass('pricingSales');
                    jQuery(buttonEl).removeClass('buyItNow');
                }
                else {
                    jQuery(buttonEl).addClass('buyItNow');
                    jQuery(buttonEl).removeClass('pricingSales');
                }
            });
    
            jQuery('select').trigger('change');
        }
    }
};