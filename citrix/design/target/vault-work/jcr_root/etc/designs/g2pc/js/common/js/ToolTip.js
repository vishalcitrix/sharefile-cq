var ToolTip = ToolTip || {
	selector: "a[rel='tooltip']",
	
    init: function() {    
        $(ToolTip.selector).each(function() {
            $(this).stationaryTip();
        });
    }
};