var Validator = Class.create({
    initialize : function(className, error, test, options) {
        if (typeof test === 'function') {
            this.options = $H(options);
            this._test = test;
        } else {
            this.options = $H(test);
            this._test = Prototype.K;
        }
        this.error = error || 'Validation failed.';
        this.className = className;
    },
    test : function(v, elm) {
        return (this._test(v, elm) && this.options.all(function(p) {
            return Validator.methods[p.key] ? Validator.methods[p.key](v, elm, p.value) : true;
        }));
    }
});

Validator.methods = {
    pattern : function(v, elm, opt) {
        return Validation.get('IsEmpty').test(v) || opt.test(v);
    },
    minLength : function(v, elm, opt) {
        return v.length >= opt;
    },
    maxLength : function(v, elm, opt) {
        return v.length <= opt;
    },
    min : function(v, elm, opt) {
        return v >= parseFloat(opt);
    },
    max : function(v, elm, opt) {
        return v <= parseFloat(opt);
    },
    notOneOf : function(v, elm, opt) {
        return $A(opt).all(function(value) {
            return v !== value;
        });
    },
    oneOf : function(v, elm, opt) {
        return $A(opt).any(function(value) {
            return v === value;
        });
    },
    'is' : function(v, elm, opt) {
        return v === opt;
    },
    isNot : function(v, elm, opt) {
        return v !== opt;
    },
    equalToField : function(v, elm, opt) {
        return v === $F(opt);
    },
    notEqualToField : function(v, elm, opt) {
        return v !== $F(opt);
    },
    include : function(v, elm, opt) {
        return $A(opt).all(function(value) {
            return Validation.get(value).test(v, elm);
        });
    }
};

var Validation = Class.create({
    initialize : function(form, options) {
        this.options = Object.extend({
            onSubmit : true,
            stopOnFirst : false,
            immediate : false,
            focusOnError : true,
            useTitles : false,
            onFormValidate : function(result, form) {},
            onElementValidate : function(result, elm) {},
            translations: {}
        }, options || {});
        this.form = $(form);
        if (this.options.onSubmit) {
            this.form.observe('submit', this.onSubmit.bind(this), false);
        }
        if (this.options.immediate) {
            this.form.getElements().invoke('observe', 'blur', (function(ev) {
                Validation.validate(Event.element(ev), this.options);
            }).bind(this));
        }
    },
    onSubmit :  function(ev) {
        if (!this.validate()) {
            ev.stop();
        }
    },
    validate : function() {
        var result = false;
        if (this.options.stopOnFirst) {
            result = this.form.getElements().all(function(elm) {
                return elm.type == 'hidden' || Validation.validate(elm, this.options);
            }, this);
        } else {
            result = this.form.getElements().collect(function(elm) {
                return elm.type == 'hidden' || Validation.validate(elm, this.options);
            }, this).all();
        }
        if (!result && this.options.focusOnError) {
			try {
				this.form.select('input.validation-failed, select.validation-failed').first().focus();
			} catch(e) { }
        }
        this.options.onFormValidate(result, this.form);
        return result;
    },
    reset : function() {
        this.form.getElements().each(Validation.reset);
    }
});

Object.extend(Validation, {
    validate : function(elm, options) {
        Validation.reset(elm);
        options = Object.extend({
            useTitles : false,
            onElementValidate : function(result, elm) {}
        }, options || {});
        options.getMessage = function(name, elm, def) {
            return this.translations && this.translations[name] ? this.translations[name] :
                (this.useTitles && elm.title ? elm.title : def)
            };
        var result = $(elm).classNames().collect(function(value) {
            return Validation.test(value, elm, options);
        }).all();
        if (result) {
            elm.addClassName('validation-passed');
        }
        return result;
    },
    test : function(name, elm, options) {
        var needsTest = Validation.isVisible(elm)
                && (elm.hasClassName('validate') || elm.hasClassName('required'));
        var v = Validation.get(name);
        if (!needsTest || !v || v.isFake) return true;
        var result = v.test($F(elm), elm);
        options.onElementValidate(result, elm);
        if (result) {
            Validation.hideAdvice(name, elm);
            return true;
        } else {
            Validation.showAdvice(name, elm, options.getMessage(name, elm, v.error));
        }
    },
    isVisible: function(elm) {
        return $(elm) && elm.visible() && elm.getHeight() != 0;
    },
    getAdvice: function(name, elm, createIfMissing) {
        var adviceID = 'advice-' + name + '-' + elm.identify();
        var adviceElement = $(adviceID) || $('advice-' + elm.identify());
        if (!adviceElement && createIfMissing) {
            var adviceHTML = '<div class="validation-advice" style="display:none" id="#{id}"></div>'.interpolate({id: adviceID});
            switch (elm.type.toLowerCase()) {
                case 'checkbox':
                case 'radio':
                    if (elm.up()) { // TODO: how could there ever NOT be a parent?
                        elm.up().insert({bottom: adviceHTML});
                        break;
                    } // else fall thru
                default:
                    elm.insert({after: adviceHTML});
            }
            return $(adviceID);
        }
        return adviceElement;
    },
    showAdvice: function(name, elm, message) {
        elm.addClassName('validation-failed');
        try {
            var advice = Validation.getAdvice(name, elm, true).update(message);
            if (typeof Effect == 'undefined') {
                advice.show();
            } else {
                new Effect.Appear(advice, {duration : 1 });
            }
            elm[Validation.getAdvicePropName(name)] = true;
        } catch (e) {}
    },
    hideAdvice: function(name, elm) {
        try {
            elm[Validation.getAdvicePropName(name)] = false;
            Validation.getAdvice(name, elm).hide();
         } catch (e) {}
    },
    getAdvicePropName: function(name) {return '__advice' + name.camelize()},
    reset : function(elm) {
        elm = $(elm);
        elm.classNames().each(function(cls) {
            var prop = Validation.getAdvicePropName(cls);
            if (elm[prop]) {
                Validation.hideAdvice(cls, elm);
            }
        });
        elm.removeClassName('validation-failed').removeClassName('validation-passed');
    },
    add : function(vt) {
        if (arguments.length > 1) {
            $A(arguments).each(function f(a) {Validation.add(a);});
            return;
        }
        Validation.methods[vt.className] =  new Validator(vt.className, vt.error, vt.test, vt.options);
    },
    get : function(name) {
        return  Validation.methods[name] ? Validation.methods[name] : {test: Prototype.K, isFake: true};
    },
    methods : { }
});
// one call to .add would be enough; this is just to show that you can add a single or multiple validators using the same method
Validation.add({className: 'IsEmpty', error: '', test: function(v) {
    return  ((v == null) || (v.length == 0)); // || /^\s+$/.test(v));
}});
Validation.add(
    {className: 'required', error: 'This is a required field.', test: function(v) {
        return !Validation.get('IsEmpty').test(v);
    }},
    {className: 'email', error: 'Please enter a valid email address. For example fred@example.com .', test: function (v) {
        return Validation.get('IsEmpty').test(v) || /^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$/.test(v);
    }},
    {className: 'number', error: 'Please enter a valid number in this field.', test: function (v) {
        return Validation.get("IsEmpty").test(v) || !isNaN(v) && !/^\s+$/.test(v);
    }},
    {className: 'digits', error: 'Please use numbers only in this field. please avoid spaces or other characters such as dots or commas.', test: function (v) {
        return Validation.get("IsEmpty").test(v) || !/[^\d]/.test(v);
    }},
    {className: 'phone', error: 'Please enter a valid phone number', test: function(v){
        if (Validation.get('IsEmpty').test(v)) return true;
        if (jQuery('#Country').val() == "United States" || jQuery('#Country').val() == "Canada"){
        	phone = v.replace(/\D/g, '');
	        if(phone.indexOf("1") == 0){
 	      		phone = phone.substr(1);
 	      	}
		     	var regex = /^(\d{10,15})$/;
		      if (!regex.test(phone)){ return false; }
	 	      jQuery("#Phone").val(function() {
	 	      	if(phone.length >= 11){
					    return phone.replace(/(\d{3})(\d{3})(\d{4})(\d)/, "+1 ($1) $2-$3 ext.$4");
					  }else{
					    return phone.replace(/(\d{3})(\d{3})(\d{4})/, "+1 ($1) $2-$3");
					  }
					});
  	    }else{
  	    	phone = v.replace(/\D/g, '');
	        if(phone.indexOf("1") == 0){
 	      		phone = phone.substr(1);
 	      	}
	        var regex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4,5})(([xX]|[eE][xX][tT])\d{1,5}){0,1}$/;
	 	      if (!regex.test(phone)){ return false; }
	      }
        return true;
    }},
    {className: 'alpha', error: 'Please use letters only (a-z) in this field.', test: function (v) {
        return Validation.get("IsEmpty").test(v) || /^[a-zA-Z]+$/.test(v);
    }},
    {className: 'alphanum', error: 'Please use only letters (a-z) or numbers (0-9) only in this field. No spaces or other characters are allowed.', test: function (v) {
        return Validation.get("IsEmpty").test(v) || !/\W/.test(v);
    }},
    {className: 'date', error: 'Please enter a valid date.', test: function (v) {
        if (Validation.get("IsEmpty").test(v)) { return true; }
        var regex = /^(\d{2})\/(\d{2})\/(\d{4})$/;
        if (!regex.test(v)) { return false; }
        var d = new Date(v);
        return parseInt(RegExp.$2, 10) == 1 + d.getMonth()
                && parseInt(RegExp.$1, 10) == d.getDate()
                && parseInt(RegExp.$3, 10) == d.getFullYear();
    }},
    {className: 'creditcard', error: 'Please enter a valid credit card number.', test: function (v) {
        return Validation.get("IsEmpty").test(v)
            || /^4[0-9]{12}(?:[0-9]{3})?$/.test(v) // visa
            || /^5[1-5][0-9]{14}$/.test(v) //mc
            || /^3[47][0-9]{13}$/.test(v) //amex
            || /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(v) //diners
            || /^6(?:011|5[0-9]{2})[0-9]{12}$/.test(v) //disc
            || /^(?:2131|1800|35\\d{3})\\d{11}$/.test(v) //jcb
            ;
    }},
    {className: 'url', error: 'Please enter a valid URL.', test: function (v) {
        return Validation.get("IsEmpty").test(v) || /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i.test(v);
    }},
    {className: 'date-au', error: 'Please use this date format: dd/mm/yyyy. For example 17/03/2006 for the 17th of March, 2006.', test: function (v) {
        if (Validation.get("IsEmpty").test(v)) { return true; }
        var regex = /^(\d{2})\/(\d{2})\/(\d{4})$/;
        if (!regex.test(v)) { return false; }
        var d = new Date(v.replace(regex, "$2/$1/$3"));
        return parseInt(RegExp.$2, 10) == 1 + d.getMonth()
                && parseInt(RegExp.$1, 10) == d.getDate()
                && parseInt(RegExp.$3, 10) == d.getFullYear();
    }},
    {className: 'currency-dollar', error: 'Please enter a valid $ amount. For example $100.00 .', test: function (v) {
        // [$]1[##][,###]+[.##]
        // [$]1###+[.##]
        // [$]0.##
        // [$].##
        return Validation.get("IsEmpty").test(v) || /^\$?\-?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}\d*(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/.test(v);
    }},
    {className: 'selection', error: 'Please make a selection', test: function (v, elm) {
        return elm.options ? elm.selectedIndex > 0 : !Validation.get("IsEmpty").test(v);
    }},
    {className: 'repeatedField', error: 'Please enter the same value as before', test: function (v, elm) {
        try {
            var tn = elm.tagName.toLocaleLowerCase();
            var p = $$(tn + "." + elm.classNames().toString().replace(/ /g, '.'));
            if (p && p.size()) {
               p = p.reject(function(e) {return e.identify() == elm.identify(); }).last();
               if (p) {
                   return p.getValue() == elm.getValue();
               }
            }
        } catch(e) {}
        return true;
    }},
    {className: 'one-required', error: 'Please select one of the above options.', test: function (v, elm) {
        return $(elm).up().select('input').any(function (elm) {return elm.getValue();}); // TODO: Pretty sure this can be done without closure at all
    }}
);

document.observe("dom:loaded", function() {
    var options = {useTitles : true};
    if (typeof ValidationOptions == 'object') {
        options = Object.extend(options, ValidationOptions);
    }
    // TODO: we might want to clone options for each for, so behaviour of one validator can be changed dynamically without effecting others
    $$('form').each(function(elm) { new Validation(elm, options); });
});