var Lightbox = Lightbox || {
    selector: "a[rel='lightbox']",

    lightboxContainer: "#lightbox-container",
    lightboxBorder: "#lightbox-border",

    init: function() {

        $(Lightbox.selector).click(function() { //Brightcove lightbox
            var link = $(this).attr("href");
            if(link.indexOf('#') === 0) {
                var divId = link.split("#")[1];
                if(Lightbox.currentLightbox !== null && divId === Lightbox.currentLightbox.attr("id")) {
                    $(Lightbox.lightboxBorder).css("width", Lightbox.currentLightbox.css("width"));
                    Lightbox.currentLightbox.show();

                    BCL.createVideo(Lightbox.currentLightbox.attr("brightcove-width"),
                        Lightbox.currentLightbox.attr("brightcove-height"),
                        Lightbox.currentLightbox.attr("brightcove-player-id"),
                        Lightbox.currentLightbox.attr("brightcove-player-key"),
                        Lightbox.currentLightbox.attr("brightcove-video-player"),
                        Lightbox.currentLightbox.attr("id"));
                    brightcove.createExperiences();
                }else {
                    $("#lightbox-border div").each(function() {
                        if(divId ===  $(this).attr("id")) {
                            Lightbox.currentLightbox = $(this);

                            $(Lightbox.lightboxBorder).css("width", Lightbox.currentLightbox.css("width"));
                            Lightbox.currentLightbox.show();

                            BCL.createVideo(Lightbox.currentLightbox.attr("brightcove-width"),
                                Lightbox.currentLightbox.attr("brightcove-height"),
                                Lightbox.currentLightbox.attr("brightcove-player-id"),
                                Lightbox.currentLightbox.attr("brightcove-player-key"),
                                Lightbox.currentLightbox.attr("brightcove-video-player"),
                                Lightbox.currentLightbox.attr("id"));
                            brightcove.createExperiences();

                        }else {
                            //Div not found
                        }
                    });
                }

                $(Lightbox.lightboxContainer).unbind("click").click(function() {
                    BCL.pausePlayerByObjectId(Lightbox.currentLightbox.find("object").attr("id"));
                    $(Lightbox.lightboxBorder).css("width", "");
                    $(Lightbox.lightboxContainer).fadeOut(100);
                    Lightbox.currentLightbox.hide();
                    Lightbox.currentLightbox.empty();
                });

                $("div#lightbox-close").unbind("click").click(function() {
                    BCL.pausePlayerByObjectId($(Lightbox.currentLightbox).find("object").attr("id"));
                    $(Lightbox.lightboxBorder).css("width", "");
                    $(Lightbox.lightboxContainer).fadeOut(100);
                    Lightbox.currentLightbox.hide();
                    Lightbox.currentLightbox.empty();
                });

                $("div#lightbox-container div").click(function(e) {
                    e.stopPropagation();
                });

                $(Lightbox.lightboxContainer).fadeIn(100);

            }else if($(this).attr("id") == "localeSelector") { //Country Selector lightbox
                var leftOffSet = $(this).offset().left - 30;
                var topOffSet = $(this).offset().top + 30;
                $("#lightbox-border").css("margin-left",leftOffSet).css("margin-top", topOffSet);
                $("#lightbox-container").addClass("locale-selector").height($('body').height());

                $("#lightbox-border div").each(function() {
                    if("localeContainer" === $(this).attr("id")) {
                        Lightbox.currentLightbox = $(this);
                        $(Lightbox.currentLightbox).show();
                    }else {
                        //Div not found
                    }
                });

                $("div#lightbox-close").unbind("click").click(function() {
                    $("#lightbox-container").fadeOut(100, function() {
                        $("#lightbox-container").removeAttr("style");
                        $("#lightbox-container").removeClass();
                        $("#lightbox-border").removeAttr("style");
                        $(Lightbox.currentLightbox).hide();
                    });
                });
                $("div#lightbox-container").unbind("click").click(function() {
                    $("#lightbox-container").fadeOut(100, function() {
                        $("#lightbox-container").removeAttr("style");
                        $("#lightbox-container").removeClass();
                        $("#lightbox-border").removeAttr("style");
                        $(Lightbox.currentLightbox).hide();
                    });
                });
                $("div#lightbox-container div").click(function(e) {
                    e.stopPropagation();
                });

                $("#lightbox-container").fadeIn(100);
            }else if(link.match("^//") || link.match("^http")) { //External Link
                var height = Lightbox.getParameterByNameFromURL('height', link);
                var width = Lightbox.getParameterByNameFromURL('width', link);
                if(!height) {height = 670;}
                if(!width) {width = 530;}
                $("#lightbox-dynamic").empty();
                $("#lightbox-dynamic").append("<iframe src=" + link + " height=" + height + "px width=" + width + "px frameBorder='0'></iframe>");
                $("#lightbox-dynamic").show();

                $("#lightbox-border").height(height);
                $("#lightbox-border").width(width);

                $("#lightbox-dynamic a[rel='external']").each(function(){
                    $(this).click(function(){window.open($(this).attr('href')); return false;});
                });

                $("div#lightbox-close").unbind("click").click(function() {
                    $("#lightbox-container").fadeOut(100, function() {
                        $("#lightbox-border").removeAttr("style");
                        $("#lightbox-dynamic").empty();
                        $("#lightbox-dynamic").hide();
                    });
                });
                $("div#lightbox-container").unbind("click").click(function() {
                    $("#lightbox-container").fadeOut(100, function() {
                        $("#lightbox-border").removeAttr("style");
                        $("#lightbox-dynamic").empty();
                        $("#lightbox-dynamic").hide();
                    });
                });
                $("div#lightbox-container div").click(function(e) {
                    e.stopPropagation();
                });

                $("#lightbox-container").fadeIn(100);
            }else { //Regular lightbox
                $("#lightbox-dynamic").load($(this).attr("href") + " #lightbox-content", function() {

                    //Common.lightboxInit();
                    BCL.playInLightbox();   /* allows brightcove video to play in lightbox */

                    $("#lightbox-dynamic").show();

                    $("#lightbox-border").addClass($("#lightbox-content").attr("class"));

                    $("#lightbox-content a[rel='external']").each(function(){
                        $(this).click(function(){window.open($(this).attr('href')); return false;});
                    });

                    $("div#lightbox-close").unbind("click").click(function() {
                        $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-border").removeAttr("style");
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass();
                        });
                    });
                    $("div#lightbox-container").unbind("click").click(function() {
                        $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-border").removeAttr("style");
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass();
                        });
                    });
                    $("div#lightbox-container div").not(".brightcoveVideo div").click(function(e) {
                        e.stopPropagation();
                    });

                    $("#lightbox-container").fadeIn(100);
                });
            }
            return false;
        });
    },

    currentLightbox: null,

    getParameterByNameFromURL: function(name, url) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if(results == null) {
            return "";
        }
        else {
            return results[1];
        }
    }

};