var Common = Common || {
	init: function() {
	    Accordion.init();
	    CaseStudy.init();
	    
	    //Resource
	    DotDotDot.init();
	    
	    //Dev only
	    Pricing.init();
	    ComparePlans.init();
	    
	    //Global
	    StickyHeader.init();
	    
	    //Links
	    CurrentPageReference.init();
	    ExternalLink.init();
	    Lightbox.init();
	    LocaleSelector.init();
	    PopupLink.init();
		ToolTip.init();
	    ScrollToTop.init();
	    Hero.init();
	}
};

jQuery(function() {
	Common.init();
});