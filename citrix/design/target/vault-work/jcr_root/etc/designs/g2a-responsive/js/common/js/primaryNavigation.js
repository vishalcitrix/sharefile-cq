// -----------------------------------
// Primary Navigation
// rick.potsakis@citrix.com
// -----------------------------------

var primaryNavigation = primaryNavigation || {
    timer: null,
    primaryNavSel: ".primary-nav",
    primarySecondaryNavSel: ".primary-secondary-list .secondaryLinkSet",
    
    init: function() {
        var windowResizeEvent = function () {
            if (ssize == 'small') {
                $(this.primarySecondaryNavSel).css('display','none');
                $('.primary-ham-nav').removeClass('forceHide');
            } else {
                $(this.primarySecondaryNavSel).css('display','inherit');
                $('.primary-ham-nav').addClass('forceHide');      
            }
        };
        
        
        $(document).ready(function(){
            // Mobile Nav action
            $('.primary-ham-button').click(function(e){
                var $nav = $('.primary-ham-nav'),
                    $anchor = $nav.find('a');
                e.preventDefault();

                if( $nav.is(":visible") ){
                    // hide
                    $anchor.removeClass('active');
                    $nav.slideUp({
                        complete: function(){
                            $('.primary-ham-nav').css({
                                'overflow-y':'initial'
                            });

                        }
                    });
                } else {
                    // show
                    $anchor.addClass('active');
                    $nav.slideDown({
                        complete: function(){
                            $('.primary-ham-nav').css({
                                'overflow-y':'auto'
                            });
                        }
                    });
                }

            });
            
            // clone dropdown links to Mobile nav
            $('.secondaryLinkList .secondaryLinkSet').clone().appendTo('.primary-secondary-list');
            $('.primary-secondary-list .menu a:first-child').click(function(e){
                var $nav = $('.primary-secondary-list .secondaryLinkSet'),
                    $anchor = $nav.find('a').eq(0);
                e.preventDefault();

                if( $nav.is(":visible") ){
                    // hide
                    $anchor.removeClass('active');
                    $nav.slideUp({
                        complete: function(){
                        }
                    });
                } else {
                    // show
                    $anchor.addClass('active');
                    $nav.slideDown({
                        complete: function(){
                        }
                    });
                }
            });

            // clone primary nav links to the target link in the mobile nav (optional)
            var $priSecNav = $('.primary-secondary-list');
            if( $priSecNav.length > 0 && $priSecNav.data('target') != undefined ){
                var priSecTarget = $priSecNav.data('target');
                $('.linkSet1').first().clone().insertAfter('.primary-secondary-list a[href="'+priSecTarget+'"]');
            }

            // Active state - highlight current page
            $('.primary-list-nav a[href="'+ window.location.pathname +'"]').first().addClass('active');

            // pages without the primary nav need whitespace removed
            if( $('.primaryNavigation').length == 0 ){
                $('.mainContent').css('padding-top', 0);
            }

            // Touch device (tablet) fix for hover
            if( mobile ) {
                $('.topNav .show-for-medium-up li.menu a').click(function(e){
                    var $dropdownMenu = $('.topNav .show-for-medium-up li.menu .secondaryLinkSet').first();
                    if( $dropdownMenu.is(':visible') ){
                        $dropdownMenu.hide();
                    } else {
                        $dropdownMenu.show();
                    }
                });
            }

            windowResizeEvent();
        });
        $(window).resize(windowResizeEvent);

        // sticky primary-nav on scroll
        $(document).on('scroll', function(){
            clearTimeout(primaryNavigation.timer);
            primaryNavigation.timer = setTimeout(function(){
                var $navBar = $('.primary-nav');
                var $navBarParent = $('.primaryNavigation');
                var scrollTop = $(document).scrollTop();

                if( $navBar.length > 0 ){
                    if( scrollTop >= ($navBarParent.offset().top+$navBarParent.height()) && scrollTop != 0){
                        $navBar.addClass('sticky');
                    } else {
                        $navBar.removeClass('sticky');
                    }
                }
            }, 100);
        });
        $(document).trigger('scroll');
    }
};
