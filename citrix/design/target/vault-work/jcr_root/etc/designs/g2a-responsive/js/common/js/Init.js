var Common = Common || {
    config: function() {
        // Set toaster hide locations
        toaster.footerCTA = '#lower-free-trial';
        toaster.footerSection = '#static-footer';
    },
    init: function() {
		backgroundVideo.init();
        primaryNavigation.init();
    	countrySelector.init();
        carousel.init();
        contactSalesHandler.init();
        Accordion.init();
        salesChat.init();
        footerNav.init();
        utilityNav.init();
        toaster.init();
        beltNavigation.init();
        searchRefinements.init();
        searchAutoSuggest.init();

        // Links - Popup, external, lightbox
        Link.init();
        hashLinkScroll.init();

    },
    lightboxInit: function(){
        styleSwitcher.init();
    }
};

jQuery(function() {
    Common.config();
    Common.init();

    var currentPage = window.location.pathname.split('/').filter(function(item) {
        return item.length > 0;
    });
    if ($.inArray('service-desk', currentPage) > 0) {
        $('.tertiaryNav').find('a[href*="/remote-support/service-desk"]').addClass('current');
    }
    else if ($.inArray('seeit', currentPage) > 0){
        $('.tertiaryNav').find('a[href*="/remote-support/seeit"]').addClass('current');
    }
    else if ($.inArray('concierge', currentPage) > 0){
        $('.tertiaryNav').find('a[href*="/remote-support/concierge"]').addClass('current');
    }
    else if ($.inArray('corporate-overview', currentPage) > 0){
        $('.tertiaryNav').find('a[href*="/remote-support/corporate-overview"]').addClass('current');
    }
    else $('.tertiaryNav').find('a[href$="/remote-support"]').addClass('current');
});
