// -----------------------------------
// Country Selector
// -----------------------------------

var countrySelector = countrySelector || {
	init: function() {
		
		var $opt = $('footer select').find("option:selected"),
			$span = $('<span>').addClass('tempOpt').text($opt.text());
		$span.css({
			'font-family': $opt.css('font-family'),
			'font-size': $opt.css('font-size')
		});
		$('body').append($span);
		
		// The 30px is for select open icon - it may vary a little per-browser
		$('footer select').width($span.width() + 30);
		$span.remove();

		$(".footer-country-selector").click(function(){
			var leftPos = $(this).width() - 35;
			//jQuery(".footer .arrow").css("left",leftPos);
			jQuery(".locale-selector .arrow").toggleClass('open');
			jQuery(".locale-selector").toggle();
			return false;
		});
		
		$(".locale-selector .icon-close").click(function(){
			$(".locale-selector").hide();
		});
		
		$('footer select').change(function() {
			var $opt = $(this).find("option:selected"),
				optVal = $opt.val();

			if(optVal){
				window.location.href = optVal;
			}
		});

	}
};
