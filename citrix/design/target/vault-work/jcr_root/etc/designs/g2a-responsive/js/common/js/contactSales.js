// -----------------------------------
// Contact Sales Marketo
// rick.potsakis@citrix.com
// -----------------------------------

var contactSalesHandler = contactSalesHandler || {
	init: function() {
		if( $('.contactSales').length > 0){
			$(document).ready(function(){
				$('.contactSales').each(function() {
					var defaultID = $(this).attr('data-defaultID'),
						cookieStr = $(this).attr('data-cookieStr'),
						website = $(this).attr('data-website'),
						sfid = '',
						$theForm = $(this).find('form[name="contactSales"]');

					mktCookie = contactSalesHandler.getmktCookie(cookieStr); 
					if(mktCookie != '') {
						sfid = contactSalesHandler.getCookieSFID(mktCookie, website);
					}
					if($theForm.find('input[name="sfdc_campaign_id"]').val() == defaultID && sfid !='')
						$theForm.find('input[name="sfdc_campaign_id"]').val(sfid);
				});
			});
		}
	},
	getCookieSFID: function(mktCookie) {
		var sfid = '';
		var sfStart = -1;
		if(mktCookie.indexOf('LST_cmp') > -1) {
			sfStart = mktCookie.indexOf('LST_cmp')+10;
		} else if(mktCookie.indexOf('FIS_cmp') > -1) {
			sfStart = mktCookie.indexOf('FIS_cmp')+10;
		}
		if (sfStart > -1) {
			var temp = mktCookie.substring(sfStart);
			if(temp.indexOf('%26')>-1) {
				var sfEnd = temp.indexOf('%26');
				temp = temp.substring(0,sfEnd);
			}
			if(temp.indexOf('sf-') == 0) {
				sfid = temp.substring(3)
			}
		}
		return sfid;
	},
	getmktCookie: function(name) {
		var mktCookie = '';
		if(document.cookie.indexOf(name) >-1){
			var start = document.cookie.indexOf(name)+name.length+1;
			mktCookie = document.cookie.substring(start);
			if(mktCookie.indexOf(';') >-1){
				var end = mktCookie.indexOf(';')
				mktCookie = mktCookie.substring(0,end);
			}
		}
		return mktCookie;
	}
};
