var PopupLink = {
	selector: "a[rel=popup]",

	init: function() {
		$(PopupLink.selector).each(function() {
			$(this).unbind("click").click(function() {
				window.open(this.href,'','width=500,height=400,menubar=0,status=0,location=0,toolbar=0,scrollbars=yes,resizable=yes');
				return false;
			});
		});
	}
};