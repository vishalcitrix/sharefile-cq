// -----------------------------------
// Background Video
// rick.potsakis@citrix.com
// -----------------------------------

var backgroundVideo = backgroundVideo || {
    init : function () {
    
        // for mobile devices that do not support auto play videos, display fallback image
        if( backgroundVideo.hasBackgroundVideo() ){
            if(detectDevice()){
                var videoTimer = setTimeout(function(){
                    $('.backgroundVideo').each(function(){
                        $(this).find('video').hide();
                        if( $(this).find('.backgroundVideoContent').attr('data-mobile-bg') != undefined ){
                            var bgUrl = $(this).find('.backgroundVideoContent').attr('data-mobile-bg');
                            $(this).find('.backgroundVideoContent').css({ 'background': 'url(\''+bgUrl+'\') repeat 50% 50% transparent' });
                        }
                    });
                }, 500);
                $('video').on('loadstart', function(){ clearTimeout(videoTimer); });
            }
        }
        
        
    },
    hasBackgroundVideo: function(){
        return ( $('.backgroundVideo').length > 0 );
    },
    supports_video : function() {
      return !!document.createElement('video').canPlayType;
    },
    supports_h264_baseline_video: function() {
      if (!supports_video()) { return false; }
      var v = document.createElement("video");
      return v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
    },
    videoPlaying: function(video) {
        return !(video.paused || video.ended || video.seeking || video.readyState < video.HAVE_FUTURE_DATA);
    }
};