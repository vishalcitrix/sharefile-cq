var Common = Common || {
    init: function() {
		backgroundVideo.init();
		menuScrollBar.init();
    	menuSlideBar.init();
        primaryNavigation.init();
    	countrySelector.init();
        carousel.init();
        Accordion.init();

        // Links - Popup, external, lightbox
        Link.init();
        hashLinkScroll.init();

    	// comes from clientlib
    	slideBar.init();
    	slideBar.defaultHide();
    },
    lightboxInit: function(){
        styleSwitcher.init();
    }
};

jQuery(function() {
    Common.init();
});
