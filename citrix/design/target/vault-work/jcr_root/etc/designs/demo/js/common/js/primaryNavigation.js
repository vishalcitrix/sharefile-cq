// -----------------------------------
// Primary Navigation
// rick.potsakis@citrix.com
// -----------------------------------

var primaryNavigation = primaryNavigation || {
    primaryNavSel: ".primary-nav",
    primarySecondaryNavSel: ".primary-secondary-list .secondaryLinkSet",
    
    init: function() {
        var windowResizeEvent = function () {
            if (ssize == 'small') {
                $(this.primarySecondaryNavSel).css('display','none');
            } else {
                $(this.primarySecondaryNavSel).css('display','inherit');            
            }
        };
        
        
        $(document).ready(function(){
            // Mobile Nav action
            $('.primary-ham-button').click(function(e){
                var $nav = $('.primary-ham-nav'),
                    $anchor = $nav.find('a');
                e.preventDefault();

                if( $nav.is(":visible") ){
                    // hide
                    $anchor.removeClass('active');
                    $nav.slideUp({
                        complete: function(){
                            
                        }
                    });
                } else {
                    // show
                    $anchor.addClass('active');
                    $nav.slideDown({
                        complete: function(){
                            
                        }
                    });
                }

            });
            
            // clone dropdown links to Mobile nav
            $('.secondaryLinkList .secondaryLinkSet').clone().appendTo('.primary-secondary-list');
            $('.primary-secondary-list .menu a:first-child').click(function(e){
                var $nav = $('.primary-secondary-list .secondaryLinkSet'),
                    $anchor = $nav.find('a').eq(0);
                e.preventDefault();

                if( $nav.is(":visible") ){
                    // hide
                    $anchor.removeClass('active');
                    $nav.slideUp({
                        complete: function(){
                            
                        }
                    });
                } else {
                    // show
                    $anchor.addClass('active');
                    $nav.slideDown({
                        complete: function(){
                            
                        }
                    });
                }
            });

            // clone primary nav links to the target link in the mobile nav (optional)
            var $priSecNav = $('.primary-secondary-list');
            if( $priSecNav.length > 0 && $priSecNav.data('target') != undefined ){
                var priSecTarget = $priSecNav.data('target');
                $('.linkSet1').first().clone().insertAfter('.primary-secondary-list a[href="'+priSecTarget+'"]');
            }

            // Active state - highlight current page
            $('.primary-list-nav a[href="'+ window.location.pathname +'"]').first().addClass('active');

            windowResizeEvent();
        });
        $(window).resize(windowResizeEvent);

        // sticky primary-nav on scroll
        $(window).on('scroll', function(){
            var $navBar = $('.primary-nav');
            var scrollTop = $('body').scrollTop();

            if( $navBar.length > 0 ){
                if( scrollTop >= $navBar.offset().top && scrollTop != 0){
                    $navBar.addClass('sticky');
                } else {
                    $navBar.removeClass('sticky');
                }
            }
        });
    }
};
