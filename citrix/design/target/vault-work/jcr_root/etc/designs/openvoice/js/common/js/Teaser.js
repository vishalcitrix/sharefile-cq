var Teaser = {
	init: function() {
		//Nothing to initialize
	},
	afterTeaserLoad: function() {
		Carousel.init();
		Hero.init();
		Lightbox.init();
		ExternalLink.init();
		PopupLink.init();
	}
};