var ScrollToTop = ScrollToTop || {
	selector: ".gotop",
		
	init: function() {
		$(ScrollToTop.selector).unbind("click").click(function() {
		    $('html,body').animate({scrollTop: 0}, 1000);
		});
	}	
};