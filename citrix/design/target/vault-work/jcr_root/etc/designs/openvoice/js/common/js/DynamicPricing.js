	jQuery("a.annualPlan").click(function() {
    jQuery('.annualPlan').addClass('annualSelected');
    jQuery('.annualPrice').removeClass('hiddenContent');
    jQuery('.monthlyPrice').addClass('hiddenContent');
    jQuery('.monthlyPlan').addClass('monthlyDefault');
});

jQuery("a.monthlyPlan").click(function() {
    jQuery('.monthlyPrice').removeClass('hiddenContent');
    jQuery('.monthlyPlan').removeClass('monthlyDefault');
    jQuery('.annualPrice').addClass('hiddenContent');
    jQuery('.annualPlan').removeClass('annualSelected');
});


var planCost = {
	"g2m":{"mo":"49","yr":"468"},
	"g2w-tier1":{"mo":"99","yr":"948"},
	"g2w-tier2":{"mo":"399","yr":"3,828"},
	"g2w-tier3":{"mo":"499","yr":"4,788"},
	"g2t-tier1":{"mo":"149","yr":"1,428"},
	"g2t-tier2":{"mo":"349","yr":"3,348"}
}
	
var Pricing = {
    init : function() {
    	jQuery("select.seats option").each( function(){
    		if(jQuery(this).val() != 1){
    			jQuery(this).removeAttr('selected')
    			}
    		else{
    			jQuery(this).attr('selected','selected')
    		}
    	});
	    	jQuery("select.seats").change(function(){
	        var prodSeats = jQuery(this).val();
	        var prodName = jQuery(this).attr('name').substring(0,jQuery(this).attr('name').indexOf('-'));
	        if(prodSeats < 6) {
	            if((prodName == "g2w" || prodName == "g2t")) {
	                if(prodSeats > 1) {
	                    jQuery('.'+prodName+'-buy-link').removeClass(prodName+'-price')
	                    jQuery('.'+prodName+'-buy-link').addClass('hiddenContent')
	                    jQuery('.'+prodName+'-try-link').addClass(prodName+'-price')
	                } else {
	                    jQuery('.'+prodName+'-try-link').removeClass(prodName+'-price')
	                    jQuery('.'+prodName+'-try-link').addClass('hiddenContent')
	                    jQuery('.'+prodName+'-buy-link').addClass(prodName+'-price')
	                }
	            }
	            jQuery('.'+prodName+'-price').removeClass('hiddenContent')
	            jQuery('.'+prodName+'-sales').addClass('hiddenContent')
	            Pricing.updateCommerceLinks(prodName,prodSeats);
	            Pricing.updatePrice(prodName,prodSeats);
	        } else {
	            jQuery('.'+prodName+'-price').addClass('hiddenContent')
	            jQuery('.'+prodName+'-sales').removeClass('hiddenContent')
	        }
	    });

    },

	updateCommerceLinks : function(prodName,prodSeats) {
	    jQuery('a').each(function() {
	        var aLink = this.href;
	        var seatParam = "quantities=";
	        var param = '?';
	        if(aLink.indexOf('?')>-1)
	            param = '&';
	        if( aLink.indexOf('secure.citrixonline.com') > -1 ) {
	            var catalog = Pricing.getParameterByNameFromURL('catalog',aLink);
	            var pricingProd ='g2m';
	            if( catalog != '' )
	                if( catalog==8 || catalog==9 )
	                    pricingProd = 'g2w';
	                else if( catalog==6 || catalog==7 )
	                    pricingProd = 'g2t';
	            else if( aLink.indexOf('/gotowebinar/') > -1 )
	                pricingProd = 'g2w';
	            else if( aLink.indexOf('/gototraining/') > -1 )
	                pricingProd = 'g2t';
	            if( prodName==pricingProd ) {
	                qtyIndex = aLink.indexOf(seatParam);
	                if( qtyIndex > -1 ) {
	                    cQty = aLink.substring(qtyIndex,qtyIndex+12);
	                    this.href = aLink.replace(cQty,seatParam+prodSeats);
	                } else
	                    this.href=aLink+param+seatParam+prodSeats;
	            }
	        }
	    });
	},
    
    updatePrice : function(prodName,prodSeats) {
    	if( prodName == 'g2m' ) {
            var mo = Pricing.currency(parseInt(planCost[prodName].mo)*prodSeats);
            var yr = Pricing.currency(parseInt(planCost[prodName].yr)*prodSeats);
            jQuery('#'+prodName+'-month').html(mo)
            jQuery('#'+prodName+'-year').html(yr)
        } else if( prodName == 'g2w' ) {
            for(i=1;i<=3;i++){
                var mo = Pricing.currency(parseInt(planCost[prodName+'-tier'+i].mo)*prodSeats);
                var yr = planCost[prodName+'-tier'+i].yr;
                yr = Pricing.currency(parseInt(yr.replace(',',''))*prodSeats);
                jQuery('#'+prodName+'-tier'+i+'-month').html(mo)
                jQuery('#'+prodName+'-tier'+i+'-year').html(yr)
            }
        } else if( prodName == 'g2t' ) {
            for(i=1;i<=2;i++){
                var mo = Pricing.currency(parseInt(planCost[prodName+'-tier'+i].mo)*prodSeats);
                var yr = planCost[prodName+'-tier'+i].yr;
                yr = Pricing.currency(parseInt(yr.replace(',',''))*prodSeats);
                jQuery('#'+prodName+'-tier'+i+'-month').html(mo)
                jQuery('#'+prodName+'-tier'+i+'-year').html(yr)
            }
        }
    },
	
	currency : function(num){
		var decimal="";
	    num = ""+num;
	    if( num.indexOf('.')>-1 ){
	        decimal = num.slice(num.indexOf('.'),num.indexOf('.')+3);
	        num=num.slice(0,num.indexOf('.'));
	    }
	    var length = num.length;
	    while( length > 4 ) {
	        length = length-3;
	        num = num.slice(0,length)+","+num.slice(length);
	    }
	    num=num+decimal;
	    return(num);
	},
    
    getParameterByNameFromURL : function(name, url) {
    	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    	var regexS = "[\\?&]" + name + "=([^&#]*)";
    	var regex = new RegExp(regexS);
    	var results = regex.exec(url);
    	if(results == null) {
    		return "";
    	}
    	else {
    		return results[1];
    	}
    }
}