var userInfoModal = {


    init : function() {
        
          var billinList = JSON.parse(sessionStorage.getItem("billingAccounts"));
          var billinID = billinList.billingAccounts[0].billingAccountId;


         userInfoModal.getUserData(billinID);
        


    },
    
    cancelButton : function(){
            
            
                    $(".userButtons").click(function() {
                        $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass().removeAttr("style");
                        });
                    });
    
    
    },
    
    closeLightbox : function() {
    
             $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass().removeAttr("style");
                        });
    
    
    },

    getUserData : function(billinID) {

      




        return $.ajax({
          type: "GET",
          url: '/bin/citrix/selfservice/getBillingAccountDetails',
          data: {parameterOne: billinID},
          dataType: "json"
      }).done(function(data){
         
        userData = data;
        if(data) {


            var accountName = data.billToContacts[0].firstName + " " +data.billToContacts[0].lastName;
            var accountBusName = data.name;
            var accountAddress1 = data.billToAddress.addressLine1;
            var accountAddress2 = data.billToAddress.addressLine2;
            var accountAddress = data.billToAddress.city +" "+data.billToAddress.state+" "+data.billToAddress.country+" "+data.billToAddress.postalCode;
            var accountEmail = data.billToContacts[0].email;
            var accountPhone = data.billToContacts[0].phone;
            
          
                       
            $('#account-form-name')[0].value = accountName;
            $('#account-form-busname')[0].value = accountBusName;
            $('#account-form-phone')[0].value = accountPhone;
            $('#account-form-email')[0].value = accountEmail;
            $('#account-form-address1')[0].value = accountAddress1;
            $('#account-form-address2')[0].value = accountAddress2;
            $('#account-form-city')[0].value = data.billToAddress.city ;
            $('#account-form-state')[0].value = data.billToAddress.state;
            $('#account-form-zipcode')[0].value = data.billToAddress.postalCode;
            $('#account-form-country')[0].value = data.billToAddress.country;
            
        }
        $('input:text').each(function(){
               
                var labelstyle = $(this);
 
               // console.log(labelstyle.val())
               
               
                labelstyle.prev('label').addClass('focused');
                labelstyle.change(function(e){
        
                           labelstyle.prev('label').addClass('focused');
                
                    })
                    
                labelstyle.focus(function(){

                labelstyle.prev('label').addClass('focused');
                labelstyle.addClass('formInput');
        })
                labelstyle.blur(function(){
                           if(labelstyle.val() === null || labelstyle.val() === ""){
                           labelstyle.prev('label').removeClass('focused');
                           labelstyle.removeClass('formInput');
                           }
        })
        
        
                if(labelstyle.val() === null || labelstyle.val() === ""){
      
                labelstyle.prev('label').removeClass('focused');
                labelstyle.removeClass('formInput');
                    }
                    
                    $('#address2focus').addClass('focused');
             
     
                       
    })
    
     
    
    })

    },
    
    postUserData : function(){
    
    
   
    
    
    
    var postData = userData;
     //   console.log("before" + postData);
             accountName = $('#account-form-name')[0].value;
             postData.name = $('#account-form-busname')[0].value;
             postData.billToContacts[0].phone = $('#account-form-phone')[0].value;
             postData.billToContacts[0].email = $('#account-form-email')[0].value;
             postData.billToAddress.addressLine1 = $('#account-form-address1')[0].value ;
             postData.billToAddress.addressLine2 = $('#account-form-address2')[0].value ;
             postData.billToAddress.city = $('#account-form-city')[0].value ;
             postData.billToAddress.state = $('#account-form-state')[0].value;
             postData.billToAddress.postalCode = $('#account-form-zipcode')[0].value;
             postData.billToAddress.country = $('#account-form-country')[0].value;
                
                // var accountName = data.billToContacts[0].firstName + " " +data.billToContacts[0].lastName;
           // console.log(postData);
    
     
     
     //   return $.ajax({
       //   type: "POST",
        //  url: "/bin/citrix/selfservice/POST",
      //    data: { parameterOne: userid, cartPayload: JSON.stringify(cartPayload) },
      ///    dataType: "json"
      //  }).done(function(data, statusText, error){
          
               userInfoModal.closeLightbox(); 
    //    })
     //   .fail(function(data, textStatus, error){
      //    console.log('post methods fail: '+error);
      //  });
    
     
     
    }

};