/*global $, Loader, sessionStorage, Currency, DateFormat, console*/
var Quote = {
    selector: {
        table: ".quoteTable",
        body: ".quoteTable .tbody",
        expired: ".quoteTable .expired"
    },
    quoteTotal: 0.0,
    quoteDiscountedTotal: 0.0,
    data: null,
    init: function (passedId) {
        'use strict';
        Loader.add($(Quote.selector.table));
        if (passedId === "null") {
            Quote.buildTable(sessionStorage.getItem("lastQuoteId"));
        } else {
            sessionStorage.setItem("lastQuoteId", passedId);
            Quote.buildTable(passedId);
        }
    },
    get: function (quoteKey) {
        'use strict';
        return $.ajax({
            type: "GET",
            url: '/bin/citrix/selfservice/quoteDetails',
            data: { parameterOne: quoteKey },
            dataType: "json"
        });
        //Quote.addError('The requested quote was not found. Please contact support.');
    },
    buildTable: function (reqData) {
        'use strict';
        Quote.get(reqData)
            .done(function (data) {
                Loader.remove($(Quote.selector.table));
                Quote.data = data;
                sessionStorage.setItem("currentQuote", JSON.stringify(Quote.data));
                Quote.setEndDate();
                if (!data.hasOwnProperty("quoteName")) {
                    Quote.addError('The quote could not be retrieved. Please contact support.');
                } else {
                    if (Quote.isExpired()) {
                        $(Quote.selector.expired).addClass('show');
                    }

                    Quote.addProduct(); // add charges related to individual products
                    //Quote.addCharge(data); // add global/miscellaneous charges
                    Quote.addTotal();
                }
            })
            .fail(function () {
                Loader.remove($(Quote.selector.table));
                Quote.addError('The quote could not be retrieved. Please contact support.');
            });
    },
    addCharge: function (productData) {
        'use strict';
        var chargeList = [];
        $.each(productData.charges, function (index, charge) {
            if (charge.chargeName === "Month Service Discount" || charge.chargeName === "Service Discount") {
                chargeList.msd = charge;
            } else {
                chargeList.push(charge);
            }
        });

        var totalPrice = chargeList[0].unitPrice * chargeList[0].quantity,
            startPeriod = (Quote.data.startDate) ? new Date(Quote.data.startDate) : null,
            endPeriod = (Quote.data.endDate) ? new Date(Quote.data.endDate) : null,
            effectiveDate = DateFormat.formatPeriodDateString(startPeriod, endPeriod),
            chargeRow = '';

        chargeRow += '<div class="trow"><div class="row"><div class="small-5 columns service-desc"><h4>' + chargeList[0].chargeName + '</h4></div><div class="small-2 columns">' + effectiveDate + '</div><div class="small-1 columns">' + parseInt(chargeList[0].quantity, 10) + '</div><div class="small-2 columns">' + Currency.format(chargeList[0].unitPrice) + '</div><div class="small-2 columns">' + Currency.format(totalPrice) + '</div></div>';

        if (chargeList.msd) {
            chargeRow += '<div class="row"><div class="small-5 columns service-discount"><p>' + chargeList.msd.chargeName + '</p></div><div class="small-2 columns">' + effectiveDate + '</div><div class="small-1 columns">' + parseInt(chargeList.msd.quantity === null ? 0 : chargeList.msd.quantity, 10) + '</div><div class="small-2 columns">(' + Currency.format(chargeList.msd.unitPrice) + ')</div><div class="small-2 columns">(' + Currency.format(chargeList.msd.unitPrice * chargeList.msd.quantity) + ')</div></div>';
            console.log("TODO: Month Service Discount end date should be coming from API");
        }

        chargeRow += '</div>';
        $(Quote.selector.body).append(chargeRow);

    },
    addProduct: function () {
        'use strict';
        $.each(Quote.data.plans, function (index, product) {
            Quote.addCharge(product);
        });
    },
    addError: function (quoteData) {
        'use strict';
        $(Quote.selector.body).append('<div class="trow"><div class="row"><div class="small-12 columns">' + quoteData + '</div></div></div>');
    },
    addTotal: function () {
        'use strict';
        $(Quote.selector.body).append('<div class="trow totals"><div class="row"><div class="small-3 small-offset-7 columns text-right">Subtotal Excl Tax & Fees</div><div class="small-2 columns">' + Currency.format(Quote.data.subTotal, Quote.data.currency) + '</div></div></div>');
        $(Quote.selector.body).append('<div class="trow totals no-top"><div class="row"><div class="small-3 small-offset-7 columns text-right">Taxes & Fees</div><div class="small-2 columns">' + Currency.format(Quote.data.estimatedTax, Quote.data.currency) + '</div></div></div>');
        $(Quote.selector.body).append('<div class="trow totals bold no-top"><div class="row"><div class="small-3 small-offset-7 columns text-right">Total Price</div><div class="small-2 columns">' + Currency.format(Quote.data.subTotal + parseFloat(Quote.data.estimatedTax === null ? 0 : Quote.data.estimatedTax), Quote.data.currency) + '</div></div></div>');
    },
    isExpired: function () {
        'use strict';
        var now = new Date(),
            boolExpired = null;
        if (Quote.data.expired === "Expired") {
            boolExpired = true;
        } else {
            boolExpired = (Date.parse(Quote.data.validUntil) <= Date.parse(now));
        }
        return boolExpired;
    },
    setEndDate: function () {
        'use strict';
        console.log('TODO: End date should come from API due to business logic requirements');
        var startDate = new Date(Quote.data.startDate),
            term = parseInt(Quote.data.term, 10);
        Quote.data.endDate = startDate.setMonth(startDate.getMonth() + term);
    }
};

