var Currency = {
    currencies: {
        "generic": "&#164;",
        "USD": "&#36;",
        "AUD": "&#36;",
        "EUR": "&#8364;",
        "GBP": "&#163;",
        "NZD": "&#36;",
        "CHF": "&#67;&#72;&#70;",
        "SEK": "&#107;&#114;",
        "CAD": "&#36;",
        "JPY": "&#165;"
    },
    format: function (amount, currency) {
        'use strict';
        amount = (amount === null || amount === "null" || amount === "undefined" || isNaN(amount)) ? "0" : amount;
        return (currency !== undefined) ? parseFloat(amount).toFixed(2) + ' ' + currency : parseFloat(amount).toFixed(2);
    }
};