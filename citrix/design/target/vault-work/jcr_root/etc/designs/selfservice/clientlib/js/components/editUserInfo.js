var editUserInfo = {

init: function(){

var sessionData = sessionStorage.getItem("billingAccounts");
//console.log(typeof(sessionData));

   if(typeof(sessionData) == "string" && sessionData != ""  ) {
    var billList = JSON.parse(sessionData);
     var billID = billList.billingAccounts[0].billingAccountId;
   }else {
       billList = null;
       billID = null;
       }
 
 
     // var billID ="";   
       
   if(billID === null || billID === "" || typeof(billID) === "undefined") {
    
      
        $('.editInfoButton').hide();
        
        $('.noBillID').show();
    
    }else { 
    editUserInfo.getUserData(billID);
    }
 
},

 

myValidateFunction: function() {



  $('#userForm').validate({
   // initialize the plugin
    rules: { 
      
       name: { 
                required: true,
                minlength: 5
            },
      busname: {
                required: true,
                minlength: 5
            },
      city: {
                required: true,
                minlength: 2
            },
      state: {
                required: true,
                minlength: 2
            },
      country: {
                required: true,
                
            },
      address1: {
                required: true,
              }, 

      email: {
        required: {
          depends: function() {
            $(this).val($.trim($(this).val()));
            return true;
          }
        },
        customemail: true
      },
      
      phone: {
        required: {
          depends: function() {
            $(this).val($.trim($(this).val()));
            return true;
          }
        },
        customphone: true
      },
      
      zip: {
        required: {
          depends: function() {
            $(this).val($.trim($(this).val()));
            return true;
          }
        },
        customzip: true,
        minlength: 5
      }
      
    },
    submitHandler: function(){ 
        
          userInfoModal.postUserData();
           
      
      }
    

  })

  //Adding custom validation methos 
  $.validator.addMethod("customemail",
    function(value, element) {
      return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value);
    },
    "Please enter a valid email"
  );

  $.validator.addMethod("customphone",
    function(value, element) {
      return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value);
      },
    "Please enter a valid phone"
  );

  $.validator.addMethod("customzip",
    function(value, element) {
      return /^\d{5}(?:[-\s]\d{4})?$/.test(value);
      
    },
    "Please enter a valid zip"
  );



   },

getUserData : function(billID) {




      return $.ajax({
      type: "GET",
      url: '/bin/citrix/selfservice/getBillingAccountDetails',
      data: {parameterOne: billID},
      dataType: "json"
    }).done(function(data){
    
    
              if(data) {
    
            
            if(data.billToContacts[0].firstName === null)
                    data.billToContacts[0].firstName="";
            if(data.billToContacts[0].lastName === null)
                    data.billToContacts[0].lastName = "";
            var accountName = data.billToContacts[0].firstName + " " +data.billToContacts[0].lastName;
            var accountBusName = data.name;
            var accountAddress1 = data.billToAddress.addressLine1;
            var accountAddress2 = data.billToAddress.addressLine2;
            if(data.billToAddress.city === null)
                data.billToAddress.city="";
            if(data.billToAddress.state === null)
                data.billToAddress.state="";
            if(data.billToAddress.country === null)
                data.billToAddress.country="";
            if(data.billToAddress.postalCode === null)
                data.billToAddress.postalCode="";
            
            var accountAddress = data.billToAddress.city +" "+data.billToAddress.state+" "+data.billToAddress.country+" "+data.billToAddress.postalCode;
            var accountEmail = data.billToContacts[0].email;
            var accountPhone = data.billToContacts[0].phone;
            
            $('#account-name').html("");
            $('#account-bus-name').html("");
            $('#account-address1').html("");
            $('#account-address2').html("");
            $('#account-address').html("");
            $('#account-email').html("");
            $('#account-phone').html("");
          
            $('#page-account-name').html("");
            $('#page-account-bus-name').html("");
            $('#page-account-address1').html("");
            $('#page-account-address2').html("");
            $('#page-account-address').html("");
            $('#page-account-email').html("");
            $('#page-account-phone').html("");
          
          
                      
             if (accountName === null){
            
                $('#account-name').hide();
            }else{
                $('#account-name').append(accountName);
                $('#page-account-name').append(accountName);
            }
            
              if (accountBusName === null){
            
                $('#account-bus-name').hide();
            }else{
                $('#account-bus-name').append(accountBusName);
                $('#page-account-bus-name').append(accountBusName);
            
            }
            
              if (accountAddress1 === null){
            
                $('#account-address1').hide();
            }else{
                $('#account-address1').append(accountAddress1);
                
                $('#page-account-address1').append(accountAddress1);
            }
            if (accountAddress2 === null){
            
                $('#account-address2').hide();
            }else{
            $('#account-address2').append(accountAddress2);
            $('#page-account-address2').append(accountAddress2);
            }
              if (accountAddress === null){
            
                $('#account-address').hide();
            }else{
            $('#account-address').append(accountAddress);
            $('#page-account-address').append(accountAddress);
            }
              if (accountEmail === null){
            
                $('#account-email').hide();
            }else{
                $('#account-email').append(accountEmail);
                $('#page-account-email').append(accountEmail);
            }
            $('#account-email').attr("href","mailto:"+ accountEmail);
            $('#page-account-email').attr("href","mailto:"+ accountEmail);
   
              if (accountPhone === null){
            
                $('#account-phone').hide();
            }else{
                $('#account-phone').append(accountPhone);
                $('#page-account-phone').append(accountPhone);
            }
            
        }
    })

   }
 };