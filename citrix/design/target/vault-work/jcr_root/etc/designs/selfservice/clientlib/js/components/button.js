
var quoteKey= sessionStorage.getItem("lastQuoteId") ;
var userid= sessionStorage.getItem("userid") ;

$( "#declineQuote" ).click(function() {
    var declineReason = $( "#declineReason" ).val();
    $.ajax({
        type: "POST",
        async: "false",
        url: "/bin/citrix/selfservice/postQuoteDetails",
        data:'parameterOne='+quoteKey+'&parameterTwo=rejected&reason={ "reason": "'+declineReason+'" }',
        success: function(msg) { 
        var json = JSON.parse(msg);      
        var responseCode = json.responseCode; 
        console.log(responseCode);    
        if(responseCode == "200")
         {
           window.location.href = "/content/selfservice/en_us/homepage/quotelanding/quote-decline-confirmation";
            }else {
             console.log(msg);
                  }             
        },
        failure: function(msg) { 
        var json = jQuery.parseJSON(msg);
        var responseCode = json.responseCode;
        console.log(responseCode);  
        }
      }); 
});


$( "#placeOrder" ).click(function() {
  console.log("INSIDE PLACE ORDER");
    $.ajax({
        type: "POST",
        async: "false",
        url: "/bin/citrix/selfservice/placeOrder",
        data:'parameterOne='+userid,
        success: function(msg) { 
        var json = JSON.parse(msg);      
        var responseCode = json.responseCode; 
        console.log(responseCode);    
        if(responseCode == "200")
         {
           window.location.href = "/content/selfservice/en_us/homepage/quotelanding/quote-decline-confirmation";
            }else {
             console.log(msg);
                  }             
        },
        failure: function(msg) { 
        var json = jQuery.parseJSON(msg);
        var responseCode = json.responseCode;
        console.log(responseCode);  
        }
      }); 
});