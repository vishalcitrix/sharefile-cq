/*global $, Loader, sessionStorage, Currency, DateFormat, console*/
var QuoteList = {
    init: function (userID) {
        'use strict';
        if (userID === 'null') { userID = sessionStorage.getItem("userid"); }
        Loader.add($('.quoteListTable'));
        QuoteList.buildTable(userID);
    },
    get: function (userID) {
        'use strict';
        return $.ajax({
            type: "GET",
            url: '/bin/citrix/selfservice/quoteList',
            data: { parameterOne: userID },
            dataType: "json"
        });
    },
    buildTable: function (reqData) {
        'use strict';
        QuoteList.get(reqData)
            .done(function (data) {
                Loader.remove($(".quoteListTable"));
                if (!data.hasOwnProperty("quotes")) {
                    QuoteList.addError('The quote list could not be retrieved. Please contact support.');
                } else {
                    QuoteList.addQuote(data); // add charges related to individual products
                }
            })
            .fail(function () {
                Loader.remove($(".quoteListTable"));
                QuoteList.addError('The quote could not be retrieved. Please contact support.');
            });
    },
    addQuote: function (quoteData) {
        'use strict';
        $.each(quoteData.quotes, function (index, quote) {
            var expiry = (quote.validUntil !== null && quote.validUntil !== "null" && quote.validUntil !== "undefined" && quote.validUntil !== "undefined") ? '<span class="standard">Exp. </span>' + DateFormat.format(quote.validUntil) : '<span class="standard">No Expiration</span>',
                colorClass = "standard";
            if (quote.status === "New") {
                colorClass = "positive";
            } else if (quote.status === "Expired") {
                colorClass = "negative";
            }

            $('.quoteListTable .tbody').append('<div class="trow"><div class="row">' +
                '<div class="small-7 columns title"><a href="/content/selfservice/en_us/homepage/quotelanding.html?quoteKey=' + quote.id + '">' + quote.quoteName + '</a></div>' +
                '<div class="small-3 columns">' + expiry + '</div>' +
                '<div class="small-2 columns ' + colorClass + '">' + quote.status + '</div>' +
                '</div></div>'
                );
        });
    },
    addError: function (quoteData) {
        'use strict';
        $('.quoteListTable tbody').append('<div class="trow"><div class="row"><div class="small-12 columns">' + quoteData + '</div></div></div>');
    }
};
