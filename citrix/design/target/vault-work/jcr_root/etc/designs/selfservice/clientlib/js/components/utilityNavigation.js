var utilityNavigation = utilityNavigation || {
    init: function(){
        var submenu = $(".dropdown-menu");
        $(".dropdown-toggle").click(function() {

            if (submenu.is(":visible")) {
                submenu.fadeOut(200);
            } else {
                submenu.fadeIn(200);
            }
        });
        var submenu_active = false;

        submenu.mouseenter(function() {
            submenu_active = true;
        });

        submenu.mouseleave(function() {
            submenu_active = false;
            setTimeout(function() { if (submenu_active === false) submenu.fadeOut(); }, 400);
        });
    }
};