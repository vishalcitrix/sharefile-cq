var Loader = {
    add: function (targetDom) {
        var $target = targetDom.parents(".boxContainer").append('<div class="loader invisible"><i class="icon-circle-load"></i></div>');
        $target.children(".loader").removeClass('invisible');
    },
    remove: function (targetDom) {
        var $target = targetDom.parents(".boxContainer").children(".loader");
        $target.addClass("invisible").delay(750).remove();
    }
};