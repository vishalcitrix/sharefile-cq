var Mediator = {
    run: function(){
        if (Mediator.register.length > 0){
            Mediator.register.forEach(function(current, index, arr){
                if (typeof current === "object"){
                    current[0].apply(null, current[1]);
                }else{
                    current.call();
                }
            });
        }
    },
    add: function(funcToAdd, dataToRunWith){
        if (typeof dataToRunWith === "object"){
            Mediator.register.push([funcToAdd, dataToRunWith]);
        }else{
            Mediator.register.push(funcToAdd);
        }
    },
    register: []
};