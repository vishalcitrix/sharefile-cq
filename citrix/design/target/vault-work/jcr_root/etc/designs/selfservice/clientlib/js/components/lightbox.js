var Lightbox = Lightbox || {
    selector: "a[rel='lightbox']",

    lightboxContainer: "#lightbox-container",
    lightboxBorder: "#lightbox-border",

    init: function() {

        $(Lightbox.selector).click(function() { //Brightcove lightbox
            var link = $(this).attr("href");
             //Regular lightbox
            
                $("#lightbox-dynamic").load($(this).attr("href") , function() {
                    
                    /* allows brightcove video to play in lightbox */

                    $("#lightbox-dynamic").show();

                    $("#lightbox-border").addClass($("#lightbox-content").attr("class"));

                    $("#lightbox-content a[rel='external']").each(function(){
                        $(this).click(function(){window.open($(this).attr('href')); return false;});
                    });

                    $("div#lightbox-close").unbind("click").click(function() {
                        $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass().removeAttr("style");
                        });
                    });
                    $("div#lightbox-container").unbind("click").click(function() {
                        $("#lightbox-container").fadeOut(100, function() {
                            $("#lightbox-content").remove();
                            $("#lightbox-dynamic").hide();
                            $("#lightbox-border").removeClass().removeAttr("style");
                        });
                    });
                    $("div#lightbox-container div").not(".brightcoveVideo div").click(function(e) {
                        e.stopPropagation();
                    });

                    $("#lightbox-container").fadeIn(800);
                });
            
            return false;
        });
    },

    currentLightbox: null,

   
};
