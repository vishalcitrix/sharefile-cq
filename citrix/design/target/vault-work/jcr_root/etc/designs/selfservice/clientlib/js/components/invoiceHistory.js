var InvoiceHistory = {
    selector: {
        container: '#invoiceHistory',
        tabLinks: '#invoiceHistory .tab-links',
        tabContent: '#invoiceHistory .tab-content'
    },
    prefix: "invoiceYear",
    data: null,
    currency: null,
    init: function (passedId) {
        'use strict';
        InvoiceHistory.addTabs();
        InvoiceHistory.bindEvents();

        Loader.add($(InvoiceHistory.selector.container));
        if (passedId === undefined && sessionStorage.getItem("billingAccounts") === "") {
            InvoiceHistory.addError("Your invoice history could not be retrieved. Please contact support.");
        } else if (passedId === undefined) {
            var accountList = JSON.parse(sessionStorage.getItem("billingAccounts"));
            InvoiceHistory.bindData(accountList.billingAccounts[0].billingAccountId); //TODO: THIS SHOULD NOT BE SET TO 0, IT SHOULD BE BASED ON THE ACTIVE BILLING ID
        } else {
            InvoiceHistory.bindData(passedId);
        }
    },
    get: function (billingAccountId) {
        'use strict';
        return $.ajax({
            type: "GET",
            url: '/bin/citrix/selfservice/getBillingAccountDetails',
            data: {
                parameterOne: billingAccountId,
                parameterTwo: 'invoices'
            },
            dataType: "json"
        });
    },
    bindData: function (reqData) {
        'use strict';
        InvoiceHistory.get(reqData)
            .done(function (data) {
                InvoiceHistory.data = data;
                InvoiceHistory.currency = InvoiceHistory.data.invoices[0].currency;
                InvoiceHistory.populate();
                Loader.remove($(InvoiceHistory.selector.container));
            })
            .fail(function () {
                Loader.remove($(InvoiceHistory.selector.container));
            });
    },
    addTabs: function () {
        'use strict';
        var currentYear = new Date().getFullYear(),
            i;
        for (i = 0; i < 4; i += 1) {
            $(InvoiceHistory.selector.tabLinks).append(
                '<li' + (i === 0 ? ' class="active"' : '') + '><a href="#' + InvoiceHistory.prefix + currentYear + '">' + currentYear + '</a></li>'
            );
            InvoiceHistory.addTabContentBlock(InvoiceHistory.prefix + currentYear, (i === 0));
            currentYear -= 1;
        }
    },
    addTabContentBlock: function (id, isActive) {
        'use strict';
        var active = (isActive) ? 'active' : '';
        $(InvoiceHistory.selector.tabContent).append('<div id="' + id + '" class="tab animated fadeIn ' + active + '"><ul><li class="no-invoice">There are no invoices available for this year</li></ul></div>');
    },
    populate: function () {
        'use strict';
        $.each(InvoiceHistory.data.invoices, function (index, invoice) {
            InvoiceHistory.addRow(invoice);
        });
    },
    addRow: function (invoice) {
        'use strict';
        var balance = Currency.format(invoice.balance, InvoiceHistory.currency),
            dueDate = new Date(Date.parse(invoice.dueDate)),
            thisRow = '<li><div class="invoice-title"><div class="row">';
        thisRow += '<div class="small-8 columns"><h3 class="text-left"> <a href="">' + invoice.invoiceNumber + '</a></h3>';
        thisRow += '<p>Date issued: ' + DateFormat.format(dueDate) + '</p></div>';
        if (invoice.status === "paid") {
            thisRow += '<div class="small-2 columns"><p class="text-right">Paid</p></div>';
        } else if (new Date(Date.parse(invoice.dueDate)) < new Date()) {
            thisRow += '<div class="small-2 columns"><p class="text-right">Past Due:</p></div>';
            thisRow += '<div class="small-2 columns"><h3 class="red-txt">' + balance + '</h3></div>';
        } else {
            thisRow += '<div class="small-2 columns"><p class="text-right">Due:</p></div>';
            thisRow += '<div class="small-2 columns"><h3 class="green-txt">' + balance + '</h3></div>';
        }
        thisRow += '</div></div></li>';

        var $target = $(InvoiceHistory.selector.tabContent + " #" + InvoiceHistory.prefix + dueDate.getFullYear() + " ul");
        $target.children('.no-invoice').hide();
        $target.append(thisRow);
    },
    showTab: function ($targetTab) {
        'use strict';
        var currentAttrValue = $targetTab.attr('href');
        // Show/Hide Tabs
        $(InvoiceHistory.selector.tabContent + ' ' + currentAttrValue).addClass('active').siblings().removeClass('active');
        // Change/remove current tab to active
        $targetTab.parent('li').addClass('active').siblings().removeClass('active');
        return;
    },
    bindEvents: function () {
        'use strict';
        $('.tabs .tab-links a').on('click', function (e) {
            InvoiceHistory.showTab($(this));
        });
    }
};