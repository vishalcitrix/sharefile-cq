/*global $, Loader, sessionStorage, Currency, DateFormat, console*/
var ProductList = {
    selector: {
        table: ".productListTable",
        body: ".productListTable .tbody"
    },
    data: null,
    currency: null,
    icons: {
        "gotomeeting": "icon-gotomeeting",
        "gototraining": "icon-gototraining",
        "gotowebinar": "icon-gotowebinar",
        "openvoice": "icon-openvoice",
        "sharefile": "icon-sharefile",
        "podio": "icon-podio",
        "shareconnect": "icon-shareconnect",
        "gotopc": "icon-gotopc"
    },
    init: function (passedId) {
        'use strict';
        Loader.add($(ProductList.selector.table));
        if (passedId === undefined && sessionStorage.getItem("billingAccounts") === "") {
            ProductList.addError("Your subscriptions could not be retrieved. Please contact support.");
        } else if (passedId === undefined) {
            var accountList = JSON.parse(sessionStorage.getItem("billingAccounts"));
            ProductList.buildTable(accountList.billingAccounts[0].billingAccountId); //TODO: THIS SHOULD NOT BE SET TO 0, IT SHOULD BE BASED ON THE ACTIVE BILLING ID
        } else {
            ProductList.buildTable(passedId);
        }
    },
    get: function (billingAccountId) {
        'use strict';
        return $.ajax({
            type: "GET",
            url: '/bin/citrix/selfservice/getBillingAccountDetails',
            data: {
                parameterOne: billingAccountId,
                parameterTwo: 'subscriptions'
            },
            dataType: "json"
        });
    },
    buildTable: function (reqData) {
        'use strict';
        ProductList.get(reqData)
            .done(function (data) {
                console.log("successful subscription API call");
                Loader.remove($(ProductList.selector.table));
                ProductList.data = data;

                ProductList.setCurrency();
                ProductList.addProducts(); // add subscription line item
            })
            .fail(function () {
                console.log("failed subscription API call");
                Loader.remove($(ProductList.selector.table));
                ProductList.addError('Your subscriptions could not be retrieved. Please contact support.');
            });
    },
    addProductRow: function (productData) {
        'use strict';
        var productRow = '',
            product = productData.productRatePlan[0],
            name = product.invoiceDisplayName,
            units = (isNaN(parseInt(product.quantity, 10))) ? '0 Units' : parseInt(product.quantity, 10) + ' Units',
            icon = (product.productDescription) ? '<span class="' + ProductList.getProductIconClass(product.productDescription) + '"></span>' : '';
        productRow += '<div class="trow"><div class="row"><div class="small-10 columns product">' + icon + ' ' + name + '</div><div class="small-2 columns">' + units + '</div></div>';

        productRow += '</div>';
        $(ProductList.selector.body).append(productRow);

    },
    addProducts: function () {
        'use strict';
        $.each(ProductList.data.subscriptions, function (index, productPlan) {
            ProductList.addProductRow(productPlan);
        });
    },
    addError: function (errorMessage) {
        'use strict';
        Loader.remove($(ProductList.selector.table));
        $(ProductList.selector.body).append('<div class="trow"><div class="row"><div class="small-12 columns">' + errorMessage + '</div></div></div>');
    },
    setCurrency: function () {
        'use strict';
        var currSymbol = null;
        if (Currency.currencies.hasOwnProperty(ProductList.data.currency)) {
            currSymbol = Currency.currencies[ProductList.data.currency];
        } else {
            currSymbol = Currency.currencies.generic;
        }
        ProductList.currency = currSymbol;
    },
    getProductIconClass: function (productName) {
        'use strict';
        var productId = productName.toLowerCase();
        if (ProductList.icons.hasOwnProperty(productId)) {
            return ProductList.icons[productId];
        }
    }
};

