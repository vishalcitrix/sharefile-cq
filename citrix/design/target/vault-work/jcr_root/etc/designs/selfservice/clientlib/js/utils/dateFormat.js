var DateFormat = {
    monthNames: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ],
    formatPeriodDateString: function (startDate, endDate) {
        'use strict';
        var startString = (startDate !== null) ? DateFormat.format(startDate) : '?',
            endString = (endDate !== null) ? DateFormat.format(endDate) : '?',
            fullString = startString + ' - ' + endString;

        return fullString;
    },
    format: function (date) {
        'use strict';
        if (date instanceof Date === false) { date = new Date(Date.parse(date)); }
        var dateObj = {
            "day": date.getDate(),
            "month": DateFormat.monthNames[date.getMonth()],
            "year": date.getFullYear()
        };
        return dateObj.month + " " + dateObj.day + ", " + dateObj.year;
    }
}