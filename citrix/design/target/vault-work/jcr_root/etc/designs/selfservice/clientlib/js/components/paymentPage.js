var PaymentPage = {

	init: function(){
		
	  //if(Loader) {
		  //Loader.add($("#paymentPage"));
	  //}
	  $('#nextPage').html("Pay by Credit Card & Next Step");
	  $('#addCard').hide();
	  $('#nextPage').addClass('disabled');
	  $('#nextPage').prop("disabled",true);
	  PaymentPage.tabsInner();
	  PaymentPage.tabsOuter();
	  PaymentPage.accordion();
	  //Bind new buttons
	  $("#newCard").click(function(){
		PaymentPage.doModal("ccform");
	  });
	  $("#newDD-US").click(function(){
		PaymentPage.doModal("achform");
	  });		  
	  $("#newDD-FR").click(function(){
		PaymentPage.doModal("ddform");
	  });	
	  $("#newDD-DE").click(function(){
		PaymentPage.doModal("ddform");
	  });	
	  $("#newDD-UK").click(function(){
		PaymentPage.doModal("ddform");
	  });	
	  //Bind choosing existing payment methods
	  $('ul.existing').on('change','input[type=radio]', function(){
		PaymentPage.savePaymentMethodId($(this).data("paymentid"));
		PaymentPage.savePaymentMethodDetails($(this).data("paymentdetails"));
	  });
	  //Bind agreement box functionality
	  $(".agreementBox").click(function(){
		PaymentPage.paymentMethodAgreementCheck($(this));
	  });
	  //Load payment methods
	  PaymentPage.loadPaymentMethods();
	  //Next button functionality
	  $("#nextPage").click(function(){
		PaymentPage.createCart();
	  });
	}, 

	tabsInner: function(){
	  $('.tabs .tab-links a.innerTabs').on('click', function(e)  {		
		var id = $(this).attr('id').replace("_Tab", "");
		// Show/Hide Tabs
		$("#" + id).show().siblings().hide();
		// Change/remove current tab to active
		$(this).parent('li').addClass('active').siblings().removeClass('active');
	   });
	},

	tabsOuter: function(){
	  $(".tabs .tab-links a.outerTabs").on('click', function(e)  {
		var id = $(this).attr('id').replace("_Tab", ""),
			activeContainer = $("#" + id),
			buttonName = 'Pay by ' + $(this).html() + ' & Next Step';

		// Show/Hide Tabs
		activeContainer.show().siblings().hide();

		// Change/remove current tab to active
		$(this).parent('li').addClass('active').siblings().removeClass('active');

		$('#nextPage').html(buttonName);

		//When they click a tab, trigger a click on the existing payment method radio button
		activeContainer.find("ul.existing li input:checked").trigger("change");
		
		//Make sure they have checked the agreement box corresponding to their current tab
		switch(id) {
			case "creditCard": agreeBox = "agreeCredit"; break;
			case "directDebit": agreeBox = "agreeDD"; break;
			case "check": agreeBox = "agreeCheck"; break;
			case "bankTransfer": agreeBox = "agreeBT"; break;
			default: break;
		}			
		PaymentPage.paymentMethodAgreementCheck($("#" + agreeBox));
	  });
	},

	accordion: function(){
	  function close_accordion_section() {
		$('.accordion .accordion-section-title').removeClass('active');
			$('.accordion .accordion-section-content').slideUp(300).removeClass('open');
			$('.accordion .accordion-section-title').children().removeClass();
		   $('.accordion .accordion-section-title').children().addClass('icon-down-open');
		  }

		$('.accordion-section-title').click(function(e) {
		// Grab current anchor value
		var currentAttrValue = $(this).attr('href');
	   
		$(this).children().removeClass();

		if($(e.target).is('.active')) {
		  close_accordion_section();
		  $(this).children().removeClass();
		$(this).children().addClass('icon-down-open')

		} else {
			close_accordion_section();
			// Add active class to section title
			$(this).addClass('active');
			// Open up the hidden content panel
			$('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
			$(this).children().addClass('icon-up-open')
		  }
	  });
	},
		
	paymentMethodAgreementCheck: function(cbox){         
	  if(cbox.is(":checked")) {
		$('#nextPage').prop("disabled",false);
		$('#nextPage').removeClass('disabled');
	  } else {
		$('#nextPage').prop("disabled",true);
		$('#nextPage').addClass('disabled');      
	  }          
	},

	accountId : "",

	paymentMethodId : "",

	handlePostAlreadyCalled : false,

	loadPaymentMethods : function() {
		PaymentPage.setBillingAccountId();
		PaymentPage.getPaymentMethods(); //Temporary 8813018168738322698 8589108124320792586
		PaymentPage.getCredentials()  
		.done(function(data, statusText, error){
		  PaymentPage.doLoadHostedPage(data, "cc", "ccform");
		  PaymentPage.doLoadHostedPage(data, "ach", "achform");
		  PaymentPage.doLoadHostedPage(data, "dd", "ddform");
		})
		.fail(function(data, textStatus, error){
		 console.log('Get hosted form credentials fail: '+error);
		}).always(function() {
			//if(Loader) {
				//Loader.remove($("#paymentPage"));
			//}
		});
	},

	setBillingAccountId : function() {
		var billingAccounts = sessionStorage.getItem("billingAccounts"),
			accountId;
		if(billingAccounts) {
			billingAccounts = JSON.parse(billingAccounts);
			accountId = billingAccounts[0];
			PaymentPage.accountId = accountId;
		}
	},

	getPaymentMethods : function() {

		//Add default payment methods for check and wire transfer
		PaymentPage.buildExistingCheckPaymentMethods([{ id: "CHECK", status: "Active"}]);
		PaymentPage.buildExistingBankTransferPaymentMethods([{ id: "WIRE", status: "Active"}]);

		if(PaymentPage.accountId == "") {
			return false;
		}
		return $.ajax({
		  type: "GET",
		  url: '/bin/citrix/selfservice/billingAccountPaymentMethod',
		  data: {parameterOne: PaymentPage.accountId},
		  dataType: "json"
		}).done(function(data, statusText, error){
			if(data) {
				var paymentMethods = data.paymentMethods[0];
				PaymentPage.buildExistingCCPaymentMethods(paymentMethods.creditCards);
				PaymentPage.buildExistingACHPaymentMethods(paymentMethods.ach);
				PaymentPage.buildExistingCheckPaymentMethods(paymentMethods.check);
				PaymentPage.buildExistingBankTransferPaymentMethods(paymentMethods.bankTransfer);
			}
		})
		.fail(function(data, textStatus, error){
		  console.log('Get payment methods fail: '+error);
		});
	},

	buildExistingCCPaymentMethods : function(creditCardList) {
	var creditCard,
		creditCardListLength = creditCardList.length,
		details,
		container = $("#creditCard ul.existing"),
		radio,
		i,
		iconClass = "",
		paymentDetails = "";         
		
		//Empty container
		container.children().remove();

		if(creditCardListLength > 0) {
			$("#creditCard .noexisting").hide();
			container.show();
		}
		
		for(i=0; i<creditCardListLength; i++){
			creditCard = creditCardList[i];
			if(creditCard.status == "Active") {
				paymentDetails = { paymentType: "Credit Card", number: creditCard.cardNumber, cardType: creditCard.cardType };
				radio = $("<input type='radio' name='cardSelected'>").data("paymentid", creditCard.id).data("paymentdetails",  JSON.stringify(paymentDetails)).prop("checked", creditCard.defaultPaymentMethod);
				//Get icon type
				switch(creditCard.cardType.toLowerCase()) {
					case "amex": iconClass = "icon-cc-amex"; break;
					case "mastercard": iconClass = "icon-cc-mastercard"; break;
					case "discover": iconClass = "icon-cc-discover"; break;
					default: iconClass = "icon-cc-visa";
				}
				details = $('<li class="large-12 columns"> <i class="' + iconClass + '"> </i>' + creditCard.cardNumber + "&nbsp;&nbsp;&nbsp;" + "  Exp:" + creditCard.expirationMonth + "/" + creditCard.expirationYear + '</li>');
				details.prepend(radio);

				container.append(details);
			}
		}
	},

	buildExistingACHPaymentMethods : function(achList) {
		var ach,
			achListLength = achList.length,
			details,
			container = $("#directDebit ul.existing"),
			radio,
			i,
			paymentDetails = "";
		
		//Empty container
		container.children().remove();

		if(achListLength > 0) {
			$("#directDebit .noexisting").hide();
			container.show();
		}

		for(i=0; i<achListLength; i++){
			ach = achList[i];
			if(ach.status == "Active") {
				paymentDetails = { paymentType: "Direct Debit", number: ach.ahAccountNumberMask, cardType: "" };
				radio = $("<input type='radio' name='achSelected'>").data("paymentid", ach.id).data("paymentdetails",  JSON.stringify(paymentDetails)).prop("checked", true);
				details = $('<li class="large-12 columns"> ' + ach.achAccountType + ': ' + ach.ahAccountNumberMask + '</li>');
				details.prepend(radio);
				container.append(details);
			}
		}
	},

	buildExistingCheckPaymentMethods : function(checkList) {
		var check,
			checkListLength,
			details,
			container = $("#check ul.existing"),
			radio,
			i,
			paymentDetails = "";

		if(!checkList) {
			return false;
		}

		checkListLength = checkList.length,
		
		//Empty container
		container.children().remove();

		for(i=0; i<checkListLength; i++){
			check = checkList[i];
			if(check.status == "Active") {
				paymentDetails = { paymentType: "Check", number: "", cardType: "" };
				radio = $("<input type='radio' name='checkSelected'>").data("paymentid", check.id).data("paymentdetails",  JSON.stringify(paymentDetails)).prop("checked", true);
				details = $('<li class="large-12 columns"></li>');
				details.prepend(radio);
				container.append(details);
			}
		}
	},

	buildExistingBankTransferPaymentMethods : function(wireList) {
		var wire,
			wireListLength,
			details,
			container = $("#bankTransfer ul.existing"),
			radio,
			i,
			paymentDetails = "";

		if(!wireList) {
			return false;
		}

		wireListLength = wireList.length,
		
		//Empty container
		container.children().remove();

		for(i=0; i<wireListLength; i++){
			wire = wireList[i];
			if(wire.status == "Active") {
				paymentDetails = { paymentType: "Wire Transfer", number: "", cardType: "" };
				radio = $("<input type='radio' name='wireSelected'>").data("paymentid", wire.id).data("paymentdetails",  JSON.stringify(paymentDetails)).prop("checked", true);
				details = $('<li class="large-12 columns"></li>');
				details.prepend(radio);
				container.append(details);
			}
		}
	},

	getCredentials : function() {
		return $.ajax({
		  type: "GET",
		  url: '/bin/citrix/selfservice/paymentMethods',
		  dataType: "json"
		})
	},
	
	loadHostedPage : function(params, prepopulateFields) {
		Z.renderWithErrorHandler(
		  params,
		  prepopulateFields,
		  PaymentPage.loadHostedPageCallback,
		  PaymentPage.errorMessageCallback
		  );
	},
  
	loadHostedPageCallback : function(response) {
		if (response != null) {
		  if (response.success) {
			  //Zuora seems to make this callback multiple times. This handles that.
			  if(!PaymentPage.handlePostAlreadyCalled) {
				PaymentPage.handlePostAlreadyCalled = true;
				PaymentPage.handleSuccessfulPaymentPostResponse(response.refId);
				setTimeout(function() {
					PaymentPage.handlePostAlreadyCalled = false;
				}, 500);
			  }

		  } else {
			//Show user an error message
			console.log('Load hosted page call back failure');
		  }
		} else {
			//Show user an error message
			console.log('Load hosted page response null');
		}
	},

	getUserId : function(id) {
		//Save payment method id in session
		return sessionStorage.getItem("userid");
	},

	getCurrentQuote : function() {
		var currentQuote = sessionStorage.getItem("currentQuote");
		return JSON.parse(currentQuote);
	},

	savePaymentMethodId : function(id) {
		//Save payment method id in session
		PaymentPage.paymentMethodId = id;
		sessionStorage.setItem("paymentMethod", id);
	},

	savePaymentMethodDetails : function(obj) {
		sessionStorage.setItem("paymentMethodDetails", obj);
	},

	handleSuccessfulPaymentPostResponse : function(id) {
		var successfulPaymentResponse = true,
			paymentDetails,
			paymentType;
		
		//Create object for saving into session storage
		//based on current tab
		paymentType = $("#tabList li.active a").prop("id").replace("_Tab", "");
		switch(paymentType) {
			case "directDebit": paymentType = "Direct Debit"; break;
			case "check": paymentType = "Check"; break;
			case "bankTransfer": paymentType = "Bank Transfer"; break;
			default: paymentType = "Credit Card"; break;
		}
		paymentDetails = { paymentType: paymentType, number: "", cardType: "" };

		//Save payment method id in session
		PaymentPage.savePaymentMethodId(id);
		PaymentPage.savePaymentMethodDetails(JSON.stringify(paymentDetails));

		//Close payment accordians and modal containers
		$("ul.accordion a.accordion-section-title.active").trigger("click");
		$("div#lightbox-close").trigger("click");

		//Hide no existing cards/dd
		$("div.noexisting").hide();
		
		//If user has an account id, update the payment methods list
		//Get payment methods to update list
		if(PaymentPage.accountId) {
			PaymentPage.getPaymentMethods();
		} else{        
			$("#creditCard .existing-cards").hide();
			$("#directDebit .existing-cards").hide();        
		}

		$("div.alert").hide();
		$("#paymentMethodSuccess").fadeIn();
	},

	errorMessageCallback : function(key, code, message) {
		var errorMessage = message;

		switch(key) {
			// Overwrite error messages generated by client-side validation.
			case "creditCardNumber":
			if(code == '001') {
			  errorMessage = 'Card number required. Please input the card number first.';
			}else if(code == '002') {
			  errorMessage = 'Number does not match credit card. Please try again.';
			}
			break;
			case "cardSecurityCode": break;
			case "creditCardExpirationYear": break;
			case "creditCardExpirationMonth": break;
			case "creditCardType": break; 
			case "creditCardCountry": break; 
			case "creditCardHolderName": break; 
			case "creditCardCountry": break; 
			case "creditCardState": break; 
			case "creditCardAddress1": break; 
			case "creditCardAddress2": break; 
			case "creditCardCity": break; 
			case "creditCardPostalCode": break; 
			case "phone": break; 
			case "email": break;  
			// Overwrite error messages generated by the server-side validation.   
			case "error":
			errorMessage ="Validation failed on server-side, Please check your input values.";
			break;
		  }

		// Use this function to show customized error message based on the key of the given field.
		Z.sendErrorMessageToHpm(key, errorMessage);   

		return;
	},

	doLoadHostedPage : function(data, type, boxId) {
		var data = data[type],
		prepopulateFields = {}, 
		params,
		box;
		if(data) {
			//Change id of payment method box
			box = $("#" + boxId);
			box.prop("id", "zuora_payment");
			params = { 
			  tenantId: data.tenantId,
			  id: data.id,
			  token: data.token,
			  signature: data.signature,
			  key: data.key,
			  url: data.url,
			  style:"inline",
			  submitEnabled:"true",
			  locale:"en_US",
			  field_accountId:PaymentPage.accountId,
			  creditCardHolderName:""
			};
			PaymentPage.loadHostedPage(params, prepopulateFields);
			//Change id of box back
			box.prop("id", boxId);
		}
	},

	doModal : function(paymentContainer) {
		var paymentContainer = $("#"+ paymentContainer);
		paymentContainer.show();

		$("#lightbox-dynamic").show();

		$("#lightbox-border").addClass($("#lightbox-content").attr("class"));

		$("div#lightbox-close").unbind("click").click(function() {
			$("#lightbox-container").fadeOut(100, function() {
				$("#lightbox-content").remove();
				$("#lightbox-dynamic").hide();
				paymentContainer.hide();
				$("#lightbox-border").removeClass().removeAttr("style");
			});
		});
		$("div#lightbox-container").unbind("click").click(function() {
			$("#lightbox-container").fadeOut(100, function() {
				$("#lightbox-content").remove();
				$("#lightbox-dynamic").hide();
				paymentContainer.hide();
				$("#lightbox-border").removeClass().removeAttr("style");
			});
		});
		$("div#lightbox-container div").click(function(e) {
			e.stopPropagation();
		});

		$("#lightbox-container").fadeIn(800);
	},

	createCart : function() {
		var userid = PaymentPage.getUserId(),
			paymentMethodId = PaymentPage.paymentMethodId,
			currentQuote,
			i,
			productObj,
			products,
			plans,
			plan,
			plansLength,
			planCharges,
			planCharge,
			planChargesLength,
			charges;

		if(!paymentMethodId) {
			$("div.alert").hide();
			$("#paymentMethodError").fadeIn();
			return false;
		}

		currentQuote = PaymentPage.getCurrentQuote();

	

		if(!currentQuote) {
			$("div.alert").hide();
			$("#quoteError").fadeIn();
			return false;
		}
		
		//Fix products so it matches the needed cart payload
		plans = currentQuote.plans;
		plansLength = plans.length;
		products = [];
		for(i=0; i<plansLength; i++) {
			plan = plans[i];
			planCharges = plan.charges;
			planChargesLength = planCharges.length;
			charges = [];
			for(e=0; e<planChargesLength; e++) {
				planCharge = planCharges[e];
				chargeObj = {
					"lineType" : "New",
                    "quoteChargeId": planCharge.id,
                    "chargeName": planCharge.chargeName,
                    "chargeType": planCharge.chargeType,
                    "model": planCharge.model,
                    "unitPrice": PaymentPage.nullToNumber(planCharge.unitPrice),
                    "listPrice": PaymentPage.nullToNumber(planCharge.listPrice),
                    "quantity": PaymentPage.nullToNumber(planCharge.quantity),
                    "total": PaymentPage.nullToNumber(planCharge.total),
                    "uom": planCharge.uom,
                    "discount": PaymentPage.nullToNumber(planCharge.discount),
                    "poNumber": planCharge.poNumber,
                    "poNumberExpiry": planCharge.poNumberExp,
                    "bundle": planCharge.bundle,
                    "period": planCharge.period
				};
				charges.push(chargeObj);
			}

			productObj = {
				"productName": plan.productName,
				"productDescription": plan.productDesc,
				"ratePlanDescription": plan.ratePlanDesc,
				"charges": charges
			};

			products.push(productObj);
		}
		
		var cartPayload = {
			"userId": userid,
			"status": currentQuote.status,
			"statusDate": PaymentPage.nullToDate(currentQuote.statusDate),
			"paymentMethodId": paymentMethodId,
			"expiryDate": PaymentPage.nullToDate(currentQuote.validUntil),
			"currency": currentQuote.currency,
			"products": products
		 }

			console.log(JSON.stringify(cartPayload));

		return $.ajax({
		  type: "POST",
		  url: "/bin/citrix/selfservice/createCart",
		  data: { parameterOne: userid, cartPayload: JSON.stringify(cartPayload) },
		  dataType: "json"
		}).done(function(data, statusText, error){
			if(data) {
				window.location = "/content/selfservice/en_us/homepage/quotelanding/approvequote";
			}
		})
		.fail(function(data, textStatus, error){
		  console.log('Create cart methods fail: '+error);
		});
	},

	nullToNumber : function(num) {
		if(!num) {
			num = "0.0";
		}
		return num;
	},

	nullToDate : function(str) {
		var date,
			day,
			month,
			year;
		if(!str) {
			date = new Date();
			day = PaymentPage.paddNums(date.getDate());
			month = PaymentPage.paddNums(date.getMonth() + 1);
			year = date.getFullYear();
			str = year + "-" + month + "-" + day;
			//"2015-02-04";
		}
		return str;
	},

	paddNums : function(num) {
		num = num.toString();
		if(num.length == 1) {
			num = "0" + num;
		}
		return num;
	}

};