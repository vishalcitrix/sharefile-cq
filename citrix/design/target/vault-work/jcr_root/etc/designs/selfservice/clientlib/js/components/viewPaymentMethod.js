window.onload = function() {

$('#response').click(function () {



    var res1 = {
        
                   "quoteName":"Quote-2/2/2015-G2M",
                   "quoteType":"Amendment",
                   "status":"New",
                   "statusDate":"2015-01-29T12:58:56.368-08:00",
                   "validUntil":"2015-01-29T12:58:56.368-08:00",
                   "paymentTerm":"Net 30/monthly",
                   "universalId":"6000112345",
                   "subscriptionId":"a4tR000000003RzIAZ",
                   "billingContact":"neil.mukerje@citrix.com",
                   "userId":"100000",
                   "products":[ 
                      { 
                         "productName":"GoToMeeting",
                         "productDesc":"GoToMeeting - 25 Attendees",
                         "ratePlan":"Corporate Service - 25 Attendees ARH - Monthly Plan",
                         "ratePlanDesc":"Corporate Service - 25 Attendees ARH - Monthly Plan",
                         "charges":[ 
                            { 
                               "id":"a4tR000000003RzIAI",
                               "chargeName":"GoToMeeting Corporate Service - 25 Attendees ARH",
                               "chargeType":"Recurring",
                               "model":"Volume Pricing",
                               "unitPrice":"39.0000000",
                               "listPrice":"39.0",
                               "total":"40.0",
                               "quantity":"1.0",
                               "uom":"Seat",
                               "poNumber":null,
                               "poNumberExp":null,
                               "credit":"FALSE",
                               "bundle":null,
                               "discount":"0.0",
                               "period":"Month"
                            },
                            { 
                               "id":"a4tR000000003S0IAI",
                               "chargeName":"GoToMeeting - 25 Attendees",
                               "chargeType":"Recurring",
                               "model":"Discount-Percentage",
                               "unitPrice":"0.0",
                               "listPrice":"0.0",
                               "total":30,
                               "quantity":"10.0",              
                               "UOM":null,
                               "poNumber":null,
                               "poNumberExp":null,
                               "credit":"FALSE",
                               "bundle":null,
                               "discount":null,
                               "period":"Month"
                            }
                         ]
                      }
                   ]
               }

    var res2= {
                  "cardSelected":[
                                  {
                                   "cardHolderInfo":{
                                      "zipCode":null,
                                      "addressLine2":null,
                                      "addressLine1":null,
                                      "phone":"4082021111",
                                      "state":null,
                                      "country":null,
                                      "city":null,
                                      "cardHolderName":"Leo",
                                      "email":"test@zuora.com"
                                   },
                                   "id":"2c92c8f83dabf9cf013daef12dd303b0",
                                   "defaultPaymentMethod":true,
                                   "status":"Active",
                                   "expirationMonth":10,
                                   "cardNumber":"************1111",
                                   "expirationYear":2020,
                                   "cardType":"Visa",
                                   "lastTransactionStatus":"Approved"
                                }
                             ]
                                         
                }
                     
                     
    var nextPayDate= res1.statusDate;
    var paymentFrequency = res1.paymentTerm;
    var paymentAmount = res1.products[0]
    var paymentCard = res2.cardSelected[0].cardNumber;
    var cardExpiryMonth = res2.cardSelected[0].expirationMonth;
    var cardExpiryYear = res2.cardSelected[0].expirationYear;
    var totalCharge = null;        
      
    for(i=0;i<res1.products[0].charges.length;i++) {
        
        totalCharge = totalCharge + Number(res1.products[0].charges[i].total);
             
    }
    
    $('#next-payment').html("");
    $('#payment-freq').html("");
    $('#payment-amount').html("");
    $('#payment-card').html("");
    
    $('#next-payment').append(nextPayDate);
    $('#payment-freq').append(paymentFrequency);
    $('#payment-card').append(paymentCard + " exp " +cardExpiryMonth + "/" + cardExpiryYear);
    $('#payment-amount').append("$" + totalCharge);
    
});

};