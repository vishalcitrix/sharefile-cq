var ResourceNew = ResourceNew || {
    resourceTabSelector: ".resource-type-hash",
        
    init: function() {
        //show first tab
        var i = 0;
        $(ResourceNew.resourceTabSelector).each(function(){
            i = i + 1;
            if(i == 1){
                $(this).addClass("disabled");
                var a_href = $(this).attr('href');
                $('.resourceContainer > div').each(function(){
                    var resourceType = $(this).attr('resource-type-container');
                    if(resourceType == a_href) {
                        $(this).show();
                    }
                })
            }
        })
        
        $(ResourceNew.resourceTabSelector).click(function() {
            var a_href = $(this).attr('href');
            $(ResourceNew.resourceTabSelector).each(function(){
                var a_href_sub = $(this).attr('href');
                if(a_href == a_href_sub){
                    $(this).addClass("disabled");
                }else{
                    $(this).removeClass("disabled");
                }
            })
            
            $('.resourceContainer > div').each(function(){
                var resourceType = $(this).attr('resource-type-container');
                if(resourceType == a_href) {
                    $(this).show();
                }else {
                    if(resourceType != undefined){
                        $(this).hide();
                    }
                }
            })
            return false;
        });
        
        $('.pagination-container a').click(function() {
            var a_href = $(this).attr('href');
            
            $(this).closest('.pagination-container').find('a').each(function(){
            	var a_href_sub = $(this).attr('href');
                if(a_href == a_href_sub){
                    $(this).addClass("selected");
                }else{
                    $(this).removeClass("selected");
                }
            })
             
            $(this).closest('.resourceCategories').find('div > ul').each(function(){
                if($(this).attr('data-resource') == a_href) {
                    $(this).fadeIn();
                } else if($(this).attr('data-resource') != undefined){
                    $(this).hide();
                }
            })
            return false;
        });
    }
};