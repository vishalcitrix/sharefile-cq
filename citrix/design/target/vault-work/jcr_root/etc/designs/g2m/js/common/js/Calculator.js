var Calculator = {
	init : function() {
		jQuery("input[type='text'],input[type='radio'],.costCompare select").change(function () {
			Calculator.calculateCost(name);
		});
		
		jQuery("a[rel='clear']").click(function() {
			var theForm = document.forms[0];
			jQuery(theForm).find(':input').each(function() {
				switch(this.type) {
					case 'text': jQuery(this).val('');
								 break;
				}
			});
			Calculator.calculateCost();
			return false;
		});
	},

	formatNumber : function(num) {
		var numberFilter =  /(\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\_|\+|\-|\=|\{|\}|\[|\]|\||\\|\:|\"|\;|\'|\<|\>|\,|\?|\/|[a-z])*/ig;
		return( num.replace(numberFilter,'') )
	},
	
	currency : function(num){
		var decimal="";
		num = "$"+num;
		if(num.indexOf('.')>-1){
			decimal = num.slice(num.indexOf('.'),num.indexOf('.')+3);
			num=num.slice(0,num.indexOf('.'));
		}
		var length = num.length;
		while(length > 4) {
			length = length-3
			num = num.slice(0,length)+","+num.slice(length)
		}
		num=num+decimal
		return(num);
	},
	
	calculateCost : function(name) {
		var theForm = document.forms[0];
		var subtotal = 0;
		var total = 0;
		var missedOpp = 31800;
		var totalMissed = 0;
		var productCost = jQuery('input:radio[name=g2tPrice]:checked').val();
		for( i=0; i < theForm.length; i++ ) {
			var value = Calculator.formatNumber(theForm.elements[i].value);
			if( value == '' ) {
				value = 0;
			}
			if( theForm.elements[i].name != "timeAway" && theForm.elements[i].type == "text" ) {
				value = parseFloat(value);
				if( theForm.elements[i].name != "missedOpp" )
					subtotal+= value;
				else {
					if(theForm.timeAway.value != "") {
						var flag = Calculator.formatNumber(theForm.missedOpp.value);
						if(flag=='')
							flag=0;
						var temp = flag % 31800;
						if ( temp == 0 ) {
							if( name != "missedOpp" ) {
								switch ( theForm.timeUnit.value ) {
									case 'Day': totalMissed = missedOpp * Calculator.formatNumber(theForm.timeAway.value);
													break;
									case 'Week': totalMissed = missedOpp* Calculator.formatNumber(theForm.timeAway.value)*7;
													break;
									case 'Month': totalMissed = missedOpp* Calculator.formatNumber(theForm.timeAway.value)*30;
													break;
								}
							}
						} else {
							totalMissed = parseFloat(flag);
						}
						theForm.missedOpp.value = Calculator.currency(totalMissed);
					}
				}
			}
		}
		total = subtotal+totalMissed
		subtotal = Calculator.currency(subtotal);
		total = Calculator.currency(total);
		jQuery("#trainingSub,#trainingTotal").text(productCost);
		jQuery("#subtotal").text(subtotal);
		jQuery("#total").text(total);
	},
	
	clearElement : function(theObject, defaultText, refill) {
		theAction = ( refill && ( ( theObject.value == ""	) || ( theObject.value == defaultText ) ) ) ? 'add' : 'remove';
		Calculator.eventHandlerClass( theAction, theObject, 'prefill' );
		if( refill && theObject.value == "" )
			theObject.value = defaultText;
		else if( theObject.value == defaultText )
			theObject.value = "";
	},
	
	eventHandlerClass : function(theAction,theObj,class1,class2){
		switch( theAction )	{
		case 'add':
			if(!eventHandlerClass('check',theObj,class1)){theObj.className+=theObj.className?' '+class1:class1;}
			break;
		case 'remove':
			var rep = theObj.className.match(' '+class1) ? ' ' + class1 : class1;
			theObj.className = theObj.className.replace(rep, '');
			break;
		case 'check':
			return new RegExp('\\b'+class1+'\\b').test(theObj.className)
			break;
		}
	}
};
