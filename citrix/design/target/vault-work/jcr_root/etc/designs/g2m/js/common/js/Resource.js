var Resource = Resource || {
	resourceServletPath : null,
	resourceServletPathSelector: "resource-servlet-path",
	resourceContainerSelector: "resource-container",
	resourceTypeContainerSelector: "resource-type-container",
	resourceTypeLinkSelector: ".resource-type-link",
	defaultLanguage: "default-language",
	currentLocale: "current-locale",
	resourceLanguageSelector: ".resource-language-link",
	resourceServletPathSelector: "resource-servlet-path",
	categoriesContainerSelector: "resource-categories-container",
	featuredContainerSelector: "resource-featured-container",
	nextWebinarContainerSelector: "resource-next-webinar-container",
	resourceTypeSelector: "resource-type",
	resourceCategoriesSelector: "resource-categories",
	resourceFeaturedSelector: "resource-featured",
	resourceLimitSelector: "resource-limit",
	paginationIndexSelector: "pagination-index",
	orderBy: "order-by",
	resourceLanguages: "allowed-language",
	timeZone: "time-zone",
	webinarUpcomingOnly: "webinar-upcoming-only",
	
	init: function(reinitialize) {
		$("[" + Resource.resourceContainerSelector + "]").each(function () {
			var resourceContainer = $(this);
			if(resourceContainer.attr("initialized") != "true" || reinitialize === true) { 
			
				//Set the Resource Servlet Path
				if(Resource.resourceServletPath == null || Resource.resourceServletPath == "") {
					Resource.resourceServletPath = $(this).attr(Resource.resourceServletPathSelector);
				}
				
				//Retrieve the resource container settings
				var defaultLanguage = $(this).attr(Resource.defaultLanguage);
				var defaultTimeZone = $(this).attr(Resource.timeZone);
				var defaultCurrentLocale = $(this).attr(Resource.currentLocale);
				
				var resourceTypeContainers = $(this).find("[" + Resource.resourceTypeContainerSelector + "]");
				
				//Anchor resource type handler
				$(this).find(Resource.resourceTypeLinkSelector).each(function () {
					$(this).unbind("click").click(function() {
						//Check if the current anchor clicked is the current one
						var clickedItem = $(this);
						if(clickedItem.hasClass("disabled")) {
							//Do nothing, this is disabled
						}else {
							//Update which anchor is currently selected
							$(resourceContainer).find(Resource.resourceTypeLinkSelector).each(function() {
								if($(this).attr("href") === $(clickedItem).attr("href")) {
									$(this).addClass("disabled");
								}else {
									$(this).removeClass("disabled");
								}
							});
							
							//Toggle the resouce type container
							var divId = $(this).attr("href").split("#")[1];
							$(resourceTypeContainers).each(function() {
								if($(this).attr("id") === divId) {
									Resource.refreshResourceCategories($(this));
									Resource.refreshResourceFeatured($(this));
									Resource.refreshResourceNextWebinar($(this));
									$(this).show();
								}else {
									$(this).hide();
								}
							});
						}
						return false;
					});
				});
				
				//Language checkbox handler
				var resourceLanguageContainer = $(this).find(Resource.resourceLanguageSelector);
				$(resourceLanguageContainer).each(function () {
					$(this).unbind("click").click(function() {
						var languages = defaultLanguage;
						$(resourceLanguageContainer).each(function() {
							if($(this).is(':checked')) {
								languages += "," + $(this).val();
							}
						});
						$(resourceTypeContainers).each(function() {
							currentResouceTypeContainer = $(this);
							$(currentResouceTypeContainer).attr(Resource.resourceLanguages, languages);
							if($(this).is(":visible")) {
								Resource.refreshResourceCategories(currentResouceTypeContainer);
								Resource.refreshResourceFeatured(currentResouceTypeContainer);
								Resource.refreshResourceNextWebinar(currentResouceTypeContainer);
							}
						});
					});
				});
				
				//Check if the url contains a specific resource type 
				var showFirstContainer = true;
				$(resourceTypeContainers).each(function () {
					//Get hashed resource type 
					var resourceTypeLink = window.location.hash;
					if($(this).attr(Resource.resourceTypeContainerSelector) === resourceTypeLink.split('#')[1]) {
						var selectedResourceTypeContainer = $(this);
						showFirstContainer = false;
						Resource.refreshResourceCategories(selectedResourceTypeContainer);
						Resource.refreshResourceFeatured(selectedResourceTypeContainer);
						Resource.refreshResourceNextWebinar(selectedResourceTypeContainer);
						$(selectedResourceTypeContainer).show();
						
						//Update the resource type anchor 
						$(resourceContainer).find(Resource.resourceTypeLinkSelector).each(function() {
							if($(this).attr("href").split("#")[1] === selectedResourceTypeContainer.attr("id")) {
								$(this).addClass("disabled");
							}
						});
						
					}
					//Set default language
					$(this).attr(Resource.resourceLanguages, defaultLanguage);
					//Set default timezone
					$(this).attr(Resource.timeZone, defaultTimeZone);
					//Set default locale
					$(this).attr(Resource.currentLocale, defaultCurrentLocale);
				});
				
	            //Check if the resource is already highlighted
	            $(resourceContainer).find(Resource.resourceTypeLinkSelector).each(function() {
	                if($(this).hasClass("disabled")) {
	                    var link = $(this).attr("href").split("#")[1];
	                    $(resourceTypeContainers).each(function () {
	                        if($(this).attr("id") === link) {
	                            var selectedResourceTypeContainer = $(this);
	                            showFirstContainer = false;
	                            Resource.refreshResourceCategories(selectedResourceTypeContainer);
	                            Resource.refreshResourceFeatured(selectedResourceTypeContainer);
	                            Resource.refreshResourceNextWebinar(selectedResourceTypeContainer);
	                            $(selectedResourceTypeContainer).show();
	                        }
	                    });
	                }
	            });
				
				//Show first container because there is no container given in url
				if(showFirstContainer) {
					$(resourceTypeContainers).each(function() {
						var selectedResourceTypeContainer = $(this);
						Resource.refreshResourceCategories(selectedResourceTypeContainer);
						Resource.refreshResourceFeatured(selectedResourceTypeContainer);
						Resource.refreshResourceNextWebinar(selectedResourceTypeContainer);
						$(selectedResourceTypeContainer).show();
						
						//Update the resource type anchor 
						$(resourceContainer).find(Resource.resourceTypeLinkSelector).each(function() {
							if($(this).attr("href").split("#")[1] === selectedResourceTypeContainer.attr("id")) {
								$(this).addClass("disabled");
							}
						});
						
						return false;
					});
				}
				
				//Remove this from getting called from another initialization.
				$(this).attr("initialized", "true");
			}
		});
	},
	
	refreshResourceCategories: function(resourceTypeContainer) {
		var resourceLanguages = $(resourceTypeContainer).attr(Resource.resourceLanguages);
		var timeZone = $(resourceTypeContainer).attr(Resource.timeZone);
		var currentLocale = $(resourceTypeContainer).attr(Resource.currentLocale);
		
		$(resourceTypeContainer).find("[" + Resource.categoriesContainerSelector + "]").each(function() {
			Resource.refreshCategoryContainer($(this), resourceLanguages, timeZone, currentLocale);
		});
	},
	
	refreshResourceFeatured: function(resourceTypeContainer) {
		var resourceLanguages = $(resourceTypeContainer).attr(Resource.resourceLanguages);
		var timeZone = $(resourceTypeContainer).attr(Resource.timeZone);
		var currentLocale = $(resourceTypeContainer).attr(Resource.currentLocale);
		
		$(resourceTypeContainer).find("[" + Resource.featuredContainerSelector + "]").each(function() {
			Resource.refreshFeaturedContainer($(this), resourceLanguages, timeZone, currentLocale);
		});
	},
	
	refreshResourceNextWebinar: function(resourceTypeContainer) {
		var resourceLanguages = $(resourceTypeContainer).attr(Resource.resourceLanguages);
		var timeZone = $(resourceTypeContainer).attr(Resource.timeZone);
		var currentLocale = $(resourceTypeContainer).attr(Resource.currentLocale);
		
		$(resourceTypeContainer).find("[" + Resource.nextWebinarContainerSelector + "]").each(function() {
			Resource.refreshNextWebinarContainer($(this), resourceLanguages, timeZone, currentLocale);
		});
	},
	
	refreshCategoryContainer: function (categoriesContainer, resourceLanguages, timeZone, currentLocale) {
		var categoriesContainerId = $(categoriesContainer).attr(Resource.categoriesContainerSelector);
		var resourceType = $(categoriesContainer).attr(Resource.resourceTypeSelector);
		var resourceCategories = $(categoriesContainer).attr(Resource.resourceCategoriesSelector);
		var resourceFeatured = $(categoriesContainer).attr(Resource.resourceFeaturedSelector);
		var resourceLimit = $(categoriesContainer).attr(Resource.resourceLimitSelector);
		var paginationIndex = $(categoriesContainer).attr(Resource.paginationIndexSelector);
		var orderBy = $(categoriesContainer).attr(Resource.orderBy);
		var webinarUpcomingOnly = $(categoriesContainer).attr(Resource.webinarUpcomingOnly);
		Resource.categoriesQuery(categoriesContainerId, resourceType, resourceCategories, resourceFeatured, resourceLanguages, timeZone, currentLocale, resourceLimit, paginationIndex, orderBy, webinarUpcomingOnly);
	},
	
	refreshFeaturedContainer: function (featuredContainer, resourceLanguages, timeZone, currentLocale) {
		var featuredContainerId = $(featuredContainer).attr(Resource.featuredContainerSelector);
		var resourceType = $(featuredContainer).attr(Resource.resourceTypeSelector);
		var resourceCategories = $(featuredContainer).attr(Resource.resourceCategoriesSelector);
		var resourceFeatured = $(featuredContainer).attr(Resource.resourceFeaturedSelector);
		var resourceLimit = $(featuredContainer).attr(Resource.resourceLimitSelector);
		var paginationIndex = $(featuredContainer).attr(Resource.paginationIndexSelector);
		var orderBy = $(featuredContainer).attr(Resource.orderBy);
		Resource.featuredQuery(featuredContainerId, resourceType, resourceCategories, resourceFeatured, resourceLanguages, timeZone, currentLocale, resourceLimit, paginationIndex, orderBy);
	},
	
	refreshNextWebinarContainer: function(nextWebinarContainer, resourceLanguages, timeZone, currentLocale) {
		var nextWebinarContainerId = $(nextWebinarContainer).attr(Resource.nextWebinarContainerSelector);
		var resourceType = $(nextWebinarContainer).attr(Resource.resourceTypeSelector);
		var resourceCategories = $(nextWebinarContainer).attr(Resource.resourceCategoriesSelector);
		var resourceLimit = $(nextWebinarContainer).attr(Resource.resourceLimitSelector);
		var orderBy = $(nextWebinarContainer).attr(Resource.orderBy);
		var webinarUpcomingOnly = $(nextWebinarContainer).attr(Resource.webinarUpcomingOnly);
		Resource.nextWebinarQuery(nextWebinarContainerId, resourceType, resourceCategories, resourceLanguages, timeZone, currentLocale, resourceLimit, orderBy, webinarUpcomingOnly);
	},
	
	categoriesQuery: function (categoriesContainerId, resourceType, resourceCategories, resourceFeatured, resourceLanguages, timeZone, currentLocale, resourceLimit, paginationIndex, orderBy, webinarUpcomingOnly) {
		$.getJSON(Resource.resourceServletPath, {"resourceType": resourceType, "resourceCategories" : resourceCategories, "resourceFeatured" : resourceFeatured, "resourceLanguages": resourceLanguages, "timeZone": timeZone, "currentLocale": currentLocale, "resourceLimit" : resourceLimit, "paginationIndex" : paginationIndex, "orderBy": orderBy, "webinarUpcomingOnly": webinarUpcomingOnly}, function(data) {
			switch(resourceType) {
			case "ResourceDocument":
				Resource.ResourceDocument(categoriesContainerId, data);
				break;
			case "ResourceWebinar":
				Resource.ResourceWebinar(categoriesContainerId, data);
				break;
			case "ResourceVideo":
				Resource.ResourceVideo(categoriesContainerId, data);
				break;
			default: 
				console.error("Resource: Data retrieved but there is no matching resource type.");
				break;
			}
		}).error(function (data) {
			console.error("Resource: Could not get resource data.");
		});
	},
	
	featuredQuery: function (featuredContainerId, resourceType, resourceCategories, resourceFeatured, resourceLanguages, timeZone, currentLocale, resourceLimit, paginationIndex, orderBy) {
		$.getJSON(Resource.resourceServletPath, {"resourceType": resourceType, "resourceCategories" : resourceCategories, "resourceFeatured" : resourceFeatured, "resourceLanguages": resourceLanguages, "timeZone": timeZone, "currentLocale": currentLocale, "resourceLimit" : resourceLimit, "paginationIndex" : paginationIndex, "orderBy": orderBy}, function(data) {
			Resource.Featured(featuredContainerId, data);
		}).error(function (data) {
			console.error("Resource: Could not get resource data.");
		});
	},
	
	nextWebinarQuery: function (nextWebinarContainerId, resourceType, resourceCategories, resourceLanguages, timeZone, currentLocale, resourceLimit, orderBy, webinarUpcomingOnly) {
		$.getJSON(Resource.resourceServletPath, {"resourceType": resourceType, "resourceCategories" : resourceCategories, "resourceLanguages": resourceLanguages, "timeZone": timeZone, "currentLocale": currentLocale, "resourceLimit" : resourceLimit, "orderBy": orderBy, "webinarUpcomingOnly": webinarUpcomingOnly}, function(data) {
			Resource.NextWebinar(nextWebinarContainerId, data);
		}).error(function (data) {
			console.error("Resource: Could not get resource data.");
		});
	}, 
	
	calculatePagination: function(data) {
		var paginationHtml = "<div class='pagination-border'><ul class='pagination-container'>";
		//Don't bother modifying pagination because it's less than 7 
		if(data.totalPages < 7) {
			for(var i = 0; i < data.totalPages; i++) {
				if(data.currentPage == i) {
					paginationHtml += "<li class='selected'><span>" + (data.currentPage + 1) + "</span></li>";	
				}else {
					paginationHtml += "<li><a href='#" + i + "'>" + (i + 1) + "</a></li>";
				}
			}
		}else { //Pagination should be towards the end 
			if((data.currentPage + 5) > data.totalPages) {
				paginationHtml += "<li><a href='#" + (data.totalPages - 5)  + "'><span class='small-arrow-left'></span></a></li>";
				paginationHtml += "<li><a href='#0'><span>1</span></a></li>";
				paginationHtml += "<li><span>...</span></li>";
				for(var i = data.totalPages - 5; i < data.totalPages; i++) {
					if(data.currentPage == i) {
						paginationHtml += "<li class='selected'><span>" + (data.currentPage + 1) + "</span></li>";	
					}else {
						paginationHtml += "<li><a href='#" + i + "'>" + (i + 1) + "</a></li>";
					}
				}
			}else if((data.currentPage - 4) < 0) { //Pagination should be towards the beginning 		
				for(var i = 0; i < 5; i++) {
					if(data.currentPage == i) {
						paginationHtml += "<li class='selected'><span>" + (data.currentPage + 1) + "</span></li>";	
					}else {
						paginationHtml += "<li><a href='#" + i + "'>" + (i + 1) + "</a></li>";
					}
				}
				paginationHtml += "<li><span>...</span></li>";
				paginationHtml += "<li><a href='#" + (data.totalPages - 1) + "'><span>" + data.totalPages + "</span></a></li>";
				paginationHtml += "<li><a href='#5'><span class='small-arrow-right'></span></a></li>";					
			}else { //Pagination should be in the middle 
				paginationHtml += "<li><a href='#" + (((data.currentPage - 2) < 0) ? 0 : (data.currentPage - 2)) + "'><span class='small-arrow-left'></span></a></li>";
				paginationHtml += "<li><a href='#0'><span>1</span></a></li>";
				paginationHtml += "<li><span>...</span></li>";
				for(var i = data.currentPage - 1; i < data.currentPage + 2; i++) {
					if(data.currentPage == i) {
						paginationHtml += "<li class='selected'><span>" + (data.currentPage + 1) + "</span></li>";	
					}else {
						paginationHtml += "<li><a href='#" + i + "'>" + (i + 1) + "</a></li>";
					}
				}
				paginationHtml += "<li><span>...</span></li>";
				paginationHtml += "<li><a href='#" + (data.totalPages - 1) + "'><span>" + data.totalPages + "</span></a></li>";
				paginationHtml += "<li><a href='#" + (((data.currentPage + 2) > data.totalPages) ? (data.totalPages - 1) : (data.currentPage + 2)) + "'><span class='small-arrow-right'></span></a></li>";
			}
		}
		paginationHtml += "</ul><div class='clearBoth'></div></div>";
		paginationHtml += "<div class='clearBoth'></div>";
		
		return paginationHtml;
	},
	
	Featured: function(featuredContainerId, data) {
		var featuredContainer = $("#" + featuredContainerId);
		
		if(data.list != null && data.list.length > 0) {
			var html = "<div class='featured-container'>";
			html += "<div class='start-gradient'></div>";
			html += "<div class='end-gradient'></div>";
			html += "<ul class='content-container'>";
			for(var i = 0; i < data.list.length; i++) {
				html += "<li class='" + data.list[i].resourceType + "'>";
				switch(data.list[i].resourceType) {
				case "ResourceDocument":
					html += "<a href='" + data.list[i].path + "'>";
						html += "<div class='thumbnail-container'>";
							html += "<img src='" + data.list[i].thumbnailPath + "' />";
							html += "<div class='featured-sprite-container'><div class='featured-sprite " + data.list[i].featured + "'></div></div>";
						html += "</div>";
					html += "</a>";
					html += "<div class='arrow-container'><span class='small-right-arrow-orange'></span></div>";
					html += "<div class='description-container'><p><a href='" + data.list[i].path + "' class='featured-link'><strong>" + data.list[i].title + "<strong></a></p>";
					html += "<div class='clearBoth'></div>";
					break;
				case "ResourceWebinar":
					html += "<a href='" + data.list[i].path + "'>";
						html += "<div class='thumbnail-container'>";
							html += "<img src='" + data.list[i].authorThumbnailPath + "'/>";
							html += "<div class='featured-sprite-container'><div class='featured-sprite " + data.list[i].featured + "'></div></div>";
						html += "</div>";
					html += "</a>";
					html += "<div class='arrow-container'><span class='small-right-arrow-orange'></span></div>";
					html += "<div class='description-container'><p><a href='" + data.list[i].path + "' class='featured-link'><strong>" + data.list[i].title + "<strong></a></p>";
					html += "<div class='clearBoth'></div>";
					break;
				case "ResourceVideo":
					html += "<a href='" + data.list[i].path + "'>";
						html += "<div class='thumbnail-container'>";
							html += "<img src='" + data.list[i].thumbnailPath + "' />";
							html += "<div class='video-play-sprite'></div>";
							html += "<div class='video-duration'>" + data.list[i].videoLength + "</div>";
							html += "<div class='featured-sprite-container'><div class='featured-sprite " + data.list[i].featured + "'></div></div>";
						html += "</div>";
					html += "</a>";
					html += "<div class='arrow-container'><span class='small-right-arrow-orange'></span></div>";
					html += "<div class='description-container'><p><a href='" + data.list[i].path + "' class='featured-link'><strong>" + data.list[i].title + "<strong></a></p>";
					html += "<div class='clearBoth'></div>";
					break;
				default:
					break;
				}
				html += "</li>";
			}
			html += "<div class='clearBoth'></div></ul></div>";
			featuredContainer.html(html);
			if(typeof DotDotDot != 'undefined') {
				DotDotDot.init();
			}
		}else {
			//Hide featured container if there is no results
			featuredContainer.hide();
		}
	},
	
	NextWebinar: function(nextWebinarContainerId, data) {
		var nextWebinarContainer = $("#" + nextWebinarContainerId);
		var webinarTitleContainer = nextWebinarContainer.find(".webinar-title-placeholder");
		var webinarAuthorTitleContainer = nextWebinarContainer.find(".webinar-author-title-placeholder");
		var webinarAuthorDescriptionContainer = nextWebinarContainer.find(".webinar-author-description-placeholder");
		var webinarThumbnailContainer = nextWebinarContainer.find(".webinar-thumbnail-container");
		var webinarUpcomingLongContainer = nextWebinarContainer.find(".webinar-upcoming-long-placeholder");
		var webinarRegistrationButton = nextWebinarContainer.find(".webinar-registration-button");
		
		if(data.list != null && data.list.length > 0) {
			webinarTitleContainer.html(data.list[0].title);
			webinarAuthorTitleContainer.html(data.list[0].authorTitle);
			webinarAuthorDescriptionContainer.html(data.list[0].authorDescription);
			
			webinarThumbnailContainer.html("<img src='" + data.list[0].authorThumbnailPath + "'/>");
			webinarUpcomingLongContainer.html(data.list[0].upcomingLong);
			webinarRegistrationButton.attr("href", data.list[0].path);
			if(data.list[0].gated || data.list[0].registration) {
				webinarRegistrationButton.attr("target", "_blank");
			}
		}
		if(typeof DotDotDot != 'undefined') {
			DotDotDot.init();
		}
	},
	
	ResourceDocument: function(categoriesContainerId, data) {
		var categoryContainer = $("#" + categoriesContainerId);
		var categoryPaginationContainer = $("#" + categoriesContainerId + "_pagination");
		$(categoryPaginationContainer).show();
		
		if(data.totalPages > 1) {
			$(categoryPaginationContainer).html(Resource.calculatePagination(data));
			//Set the click handlers to the pagination items 
			$(categoryPaginationContainer).find("a").each(function() {
				$(this).click(function() {
					$(categoryContainer).attr(Resource.paginationIndexSelector, $(this).attr("href").split("#")[1]);
                    var resourceContainer = $(categoryContainer).closest("[" + Resource.resourceTypeContainerSelector + "]");
					var resourceLanguages = resourceContainer.attr(Resource.resourceLanguages);
                    var timeZone = resourceContainer.attr(Resource.timeZone);
                    var currentLocale = resourceContainer.attr(Resource.currentLocale);
					Resource.refreshCategoryContainer(categoryContainer, resourceLanguages, timeZone, currentLocale);
					return false;
				});
			});				
		}else {
			//Do not display pagination
			$(categoryPaginationContainer).html("");
			$(categoryPaginationContainer).hide();
		}
		
		//Generate document markup
		var html = "<ul class='document-container'>";
		for(var i = 0; i < data.list.length; i++) {
			if(i % 2 == 0) {
				html += "<li class='even'>";	
			}else {
				html += "<li class='odd'>";
			}
			html += "<div class='title'>";
			if(data.list[i].gated) {
				html += "<a href='" + data.list[i].path + "' target='_blank'>" + data.list[i].title + "</a>";
			}else {
				html += "<a href='" + data.list[i].path + "'>" + data.list[i].title + "</a>";
			}
			html += "</div>";
			html += "<div class='credit'>";
			if(typeof data.list[i].credit !== 'undefined') {
				html += data.list[i].credit;
			}
			html += "</div>";
			
			if(typeof data.list[i].gated !== 'undefined' && data.list[i].gated == true) {
				html += "<div class='gated'><span class='lock'></span></div>";
			}
			html += "<div class='clearBoth'></div>";
			html += "</div></li>";
		}
		html += "</ul>";
		
		if(data.list.length != null && data.list.length > 0) {
			categoryContainer.html(html);
			if(typeof DotDotDot != 'undefined') {
				DotDotDot.init();
			}
		}else {
			//Hide container, there is nothing to show
			categoryContainer.parent().hide();
		}
	},
	
	ResourceWebinar: function(categoriesContainerId, data) {
		var categoryContainer = $("#" + categoriesContainerId);
		var categoryPaginationContainer = $("#" + categoriesContainerId + "_pagination");
		$(categoryPaginationContainer).show();
		
		if(data.totalPages > 1) {
			$(categoryPaginationContainer).html(Resource.calculatePagination(data));
			//Set the click handlers to the pagination items 
			$(categoryPaginationContainer).find("a").each(function() {
				$(this).click(function() {
					$(categoryContainer).attr(Resource.paginationIndexSelector, $(this).attr("href").split("#")[1]);
					//var resourceLanguages = $(categoryContainer).closest("[" + Resource.resourceTypeContainerSelector + "]").attr(Resource.resourceLanguages);
					//Resource.refreshCategoryContainer(categoryContainer, resourceLanguages);


                    var resourceContainer = $(categoryContainer).closest("[" + Resource.resourceTypeContainerSelector + "]");
                    var resourceLanguages = resourceContainer.attr(Resource.resourceLanguages);
                    var timeZone = resourceContainer.attr(Resource.timeZone);
                    var currentLocale = resourceContainer.attr(Resource.currentLocale);
                    Resource.refreshCategoryContainer(categoryContainer, resourceLanguages, timeZone, currentLocale);

					return false;
				});
			});
		}else {
			//Do not display pagination
			$(categoryPaginationContainer).html("");
			$(categoryPaginationContainer).hide();
		}
		
		var html = "<ul class='webinar-container'>";
		for(var i = 0; i < data.list.length; i++) {
			if(i % 2 == 0) {
				html += "<li class='even'>";	
			}else {
				html += "<li class='odd'>";
			}
			html += "<div class='author-container'>";
			
				html += "<div class='author-thumbnail-container'>";
					html += "<img src='" + data.list[i].authorThumbnailPath + "'/>";
				html += "</div>";
				html += "<div class='author-description-container'>";
					html += "<div class='title'>";
                    if(data.list[i].gated != null && data.list[i].gated) {
                        html += "<a href='" + data.list[i].path + "' target='_blank'>" + data.list[i].title + "</a>";
                    }else if(data.list[i].registration != null && data.list[i].registration) {
                        html += "<a href='" + data.list[i].path + "' target='_blank'>" + data.list[i].title + "</a>";
                    }else {
                        html += "<a href='" + data.list[i].path + "'>" + data.list[i].title + "</a>";
					}
					html += "</div>";
					
					if(typeof data.list[i].authorDescription !== 'undefined') {
						html += "<div class='author-title'><strong>" + data.list[i].authorTitle + "</strong></div>";	
					}
					
					if(typeof data.list[i].authorDescription !== 'undefined') {
						html += "<div class='author-description ellipsis'>" + data.list[i].authorDescription + "</div>";	
					}
					
					html += "</div>";
				html += "<div class='clearBoth'></div>";
			
			html += "</div>";
			
			html += "<div class='categories-container'><div class='categories-container-margin'>";
			if(typeof data.list[i].upcoming !== 'undefined') {
				html +="<div>";
				html +="<strong>" + data.list[i].upcoming + "</strong>";
				if(typeof data.list[i].upcomingTime !== 'undefined') {
					html += "<br/>";
					html +="<strong>" + data.list[i].upcomingTime + "</strong>";
				}
				html +="</div>";
			}
			if(typeof data.list[i].categories !== 'undefined') {
				for(var j = 0; j < data.list[i].categories.length; j++) {
					html +="<div>" + data.list[i].categories[j] + "</div>";
				}
			}
			html += "</div></div>";	

			html += "<div class='clearBoth'></div>";
			html += "</div></li>";
		}
		html += "</ul>";
		
		if(data.list.length != null && data.list.length > 0) {
			categoryContainer.html(html);
			if(typeof DotDotDot != 'undefined') {
				DotDotDot.init();
			}
		}else {
			//Hide container, there is nothing to show
			categoryContainer.parent().hide();
		}
	},
	
	ResourceVideo: function(categoriesContainerId, data) {
		var categoryContainer = $("#" + categoriesContainerId);
		var categoryPaginationContainer = $("#" + categoriesContainerId + "_pagination");
		$(categoryPaginationContainer).show();
		
		if(data.totalPages > 1) {
			$(categoryPaginationContainer).html(Resource.calculatePagination(data));
			//Set the click handlers to the pagination items 
			$(categoryPaginationContainer).find("a").each(function() {
				$(this).click(function() {
					$(categoryContainer).attr(Resource.paginationIndexSelector, $(this).attr("href").split("#")[1]);
					//var resourceLanguages = $(categoryContainer).closest("[" + Resource.resourceTypeContainerSelector + "]").attr(Resource.resourceLanguages);
					//Resource.refreshCategoryContainer(categoryContainer, resourceLanguages);

                    var resourceContainer = $(categoryContainer).closest("[" + Resource.resourceTypeContainerSelector + "]");
                    var resourceLanguages = resourceContainer.attr(Resource.resourceLanguages);
                    var timeZone = resourceContainer.attr(Resource.timeZone);
                    var currentLocale = resourceContainer.attr(Resource.currentLocale);
                    Resource.refreshCategoryContainer(categoryContainer, resourceLanguages, timeZone, currentLocale);


					return false;
				});
			});				
		}else {
			//Do not display pagination
			$(categoryPaginationContainer).html("");
			$(categoryPaginationContainer).hide();
		}
		
		
		var html = "<ul class='video-container'>";
		for(var i = 0; i < Math.ceil(((data.list.length) / 3)); i++) {
			html += "<li><ul class='video-row'>";
			for(var j = (i * 3); j < ((data.list.length - (i * 3)) > 3 ? ((i + 1) * 3) : (data.list.length)) ; j++) {
				html += "<li>";
				if(data.list[j].gated) {
					html += "<a href='" + data.list[j].path + "' target='_blank'>";
				}else {
					html += "<a href='" + data.list[j].path + "'>";
				}
				html += "<div class='thumbnail-container'>";
					html += "<img src='" + data.list[j].thumbnailPath + "'/>";
					html += "<div class='video-play-sprite'></div>";
					html += "<div class='video-duration'>" + data.list[j].videoLength + "</div>";
				html += "</div>";
				html += "</a>";
				
				html += "<div class='title-container'>";
					html += "<span class='small-right-arrow-orange'></span>";
					if(data.list[j].gated) {
						html += "<a href='" + data.list[j].path + "' class='video-title ellipsis' target='_blank'>" + data.list[j].title + "</a>";
					}else {
						html += "<a href='" + data.list[j].path + "' class='video-title ellipsis'>" + data.list[j].title + "</a>";
					}
					html += "<div class='clearBoth'>";
				html += "</div>";
				
				html += "</li>";
			}
			html += "</ul></li>";
		}
		html += "</ul><div class='clearBoth'></div>";
		
		if(data.list.length != null && data.list.length > 0) {
			categoryContainer.html(html);
			if(typeof DotDotDot != 'undefined') {
				DotDotDot.init();
			}
		}else {
			//Hide container, there is nothing to show
			categoryContainer.parent().hide();
		}
	}
};
	