var CaseStudy = {
	selector : "#use-case",

	init : function() {
		$(CaseStudy.selector).change(function(event) {
			var useCaseValue = this.value;
			$('#case-study-filter-body > div > div').fadeOut();
			$('.hideContent').addClass('hiddenContent');
			if (useCaseValue == 'viewAll') {
				$('#case-study-filter-body .parsys').children().fadeIn();
				$('.hideContent').removeClass('hiddenContent');
			} else {
				$('#' + useCaseValue).parent().fadeIn();
			}
			$('#oo_feedback_float').parent().css('height', '0');
		});
	}
};