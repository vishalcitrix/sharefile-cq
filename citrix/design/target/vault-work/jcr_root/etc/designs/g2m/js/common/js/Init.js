var Common = Common || {
	init: function() {
		Accordion.init();
	    CaseStudy.init();
	    UtilityNavigation.init();
	    Tab.init();
	    
	    //Resource
	    DotDotDot.init();
	    Resource.init();
	    ResourceNew.init();
	    //Dev only
	    Calculator.init();
	    ComparePlans.init();
	    ITF_Countries.init();
	    Pricing.init();
	    
	    //Global
		StickyHeader.init();
	    
	    //Links
	    CurrentPageReference.init();
	    ExternalLink.init();
	    Lightbox.init();
	    LocaleSelector.init();
	    PopupLink.init();
	    ToolTip.init();
	    ScrollToTop.init();
	    Hero.init();
	    FormBuilder.init();
	},
	
	lightboxInit: function(){
		Calculator.init();
		ITF_Countries.init();
		FormBuilder.init();
	}
};

jQuery(function() {
	Common.init();
});