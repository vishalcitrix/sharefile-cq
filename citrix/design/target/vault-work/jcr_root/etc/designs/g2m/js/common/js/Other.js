function registerLinks() {
    var productArr = ["external", "parent", "new_popup", "g2m_login", "g2t_login", "tos_popup", "top", "popup", "g2m_pop", "g2w_pop", "g2t_pop"];
    jQuery.each( productArr, function(i) {
        switch (i) {
            case 0 : jQuery("a[rel='external']").click(function() {
                window.open(this.href);
                return false;
            });
            break;
            case 1 : jQuery("a[rel='parent']").click(function() {
                window.opener.location = this.href;
                window.close();
                return false;
            });
            break;
            case 2 : jQuery("a[rel='new_popup']").click(function() {
                var delim = (this.href.indexOf('?') >= 0) ? '&' : '?';
                var h = parseInt(getParameterByNameFromURL('height', this.href));
                var w = parseInt(getParameterByNameFromURL('width',  this.href));
                if (!h) { h = 400; }
                if (!w) { w = 500; }
                var features = "height=" + h + ",width=" + w + ",menubar=0,status=0,location=0,toolbar=0,scrollbars=yes,resizable=yes";
                newwindow=window.open(this.href , 'newPopup', features);
                return false;
            });
            break;
            case 3 : jQuery("a[rel='g2m_login']").click(function() {
                document.getElementById('mktgPixelImg').src = 'https://ad.doubleclick.net/activity;src=2870544;type=confi177;cat=g2m_l318;ord=1;num=1?';
            });
            break;
            case 4 : jQuery("a[rel='g2t_login']").click(function() {
                document.getElementById('mktgPixelImg').src = 'https://ad.doubleclick.net/activity;src=2870549;type=confi794;cat=g2t_l203;ord=1;num=1?';
            });
            break;
            case 5 : jQuery('a[rel="tos_popup"]').click(function() {
                var delim = (this.href.indexOf('?') >= 0) ? '&' : '?';
                var features = "height=653,width=992,menubar=0,status=0,location=0,toolbar=0,scrollbars=yes,resizable=yes";
                newwindow=window.open(this.href + delim + 'popup=true', 'tos_popup', features);
                return false;
            });
            break;
            case 6 : jQuery("a[rel='top']").click(function() {
                window.top.location = this.href;
            });
            break;
            default : jQuery("a[rel=" + productArr[i] + "]").click(function() {
                var delim = (this.href.indexOf('?') >= 0) ? '&' : '?';
                var h = parseInt(getParameterByNameFromURL('height', this.href));
                var w = parseInt(getParameterByNameFromURL('width',  this.href));
                if (!h) { h = 400; }
                if (!w) { w = 500; }
                var features = "height=" + h + ",width=" + w + ",menubar=0,status=0,location=0,toolbar=0,scrollbars=yes,resizable=yes";
                var productVar = ( i == 1 ) ? "" : productArr[i];
                newwindow=window.open(this.href + delim + 'popup=true#' + productVar, 'Popup', features);
                return false;
            });
        }
    });
}

var videoLoadCount = 0;

function handler(event) {
    event.stopPropagation();
}

function checkAll(theObject) {
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length;i++ )
        theNodeList[i].checked=theObject.checked;
}

function unCheck(theObject) {
    var flag=true;
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length-1;i++ ) {
        if(!theNodeList[i].checked) {
            flag=false;
            break;
        }
    }
    theNodeList[theNodeList.length-1].checked = flag;
}

function preFillElements(theObject, flag) {
    if(flag) {
        for(i=0;i<theObject.length;i++) {
            theTarget = document.getElementById(theObject[i][0]);
            (flag == "prefill" && ( theTarget.value == theObject[i][1] ) ) ? eventHandlerClass('add', theTarget, flag) : clearElement(theTarget, theObject[i][1]);
        }
    }
    return true;
}

function clearElement(theObject, defaultText, refill) {
    theAction = ( refill && ( ( theObject.value === ""   ) || ( theObject.value == defaultText ) ) ) ? 'add' : 'remove';
    eventHandlerClass( theAction, theObject, 'prefill' );
    if( refill && theObject.value === "" )
        theObject.value = defaultText;
    else if( theObject.value == defaultText )
        theObject.value = "";
}

function eventHandlerClass(theAction,theObj,class1,class2) {    
    switch( theAction ) {
        case 'add':
        if(!eventHandlerClass('check',theObj,class1)){theObj.className+=theObj.className?' '+class1:class1;}
        break;
        case 'remove':
        var rep = theObj.className.match(' '+class1) ? ' ' + class1 : class1;
        theObj.className = theObj.className.replace(rep, '');
        break;
        case 'check':
        return new RegExp('\\b'+class1+'\\b').test(theObj.className);
    }
}

function getParameterByNameFromURL(name, url) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    if(results === null) {
        return "";
    }
    else {
        return results[1];
    }
}

function objCopy(obj){
    var objCopy={};
    for(var i in obj){
        objCopy[i]=obj[i];
    }
    return objCopy;
}

