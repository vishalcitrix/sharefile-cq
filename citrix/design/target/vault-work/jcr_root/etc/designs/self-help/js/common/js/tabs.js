var tabs = tabs || {
	init: function(){
		$("#parentHorizontalTab .resp-tabs-container").show();
		(function(){
			$.fn.autowidth = function() {
				return this.each(function() {        
					$('li', this).css({'width' : ((100-$('li', this).length)/ $('li', this).length) + '%'})
				});
			};
			$('#parentHorizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				tabidentify: 'hor_1', // The tab groups identifier
				activate: function(event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
			$('.tabsection').autowidth();
			$(document).on("click", ".resp-tab-content .close-btn", function(){         
				$(this).closest("#parentHorizontalTab").find(".resp-tab-content-active, .resp-tab-active").removeClass("resp-tab-content-active resp-tab-active").removeAttr('style');
				 $('.tabsection').autowidth();
			});
		}).call(this);
	}
};