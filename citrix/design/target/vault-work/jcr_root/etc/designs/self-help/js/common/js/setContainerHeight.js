var setContainerHeight = setContainerHeight || {
    init: function(){
		if( $("#whiteBgContainer").length != 0 ){
			var divheight, diff;                
			reCalcHeight();
			$(window).resize(function() {
				reCalcHeight();
			});             
		}
		function reCalcHeight() {
			diff =  jQuery(window).height()- jQuery("body").height();
			divheight = (diff > 0) ? $("#whiteBgContainer").height() + diff : 'auto';            
			$("#whiteBgContainer").height(divheight);
		}
    }
};