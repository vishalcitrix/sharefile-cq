// -----------------------------------
// Unity Nav
// -----------------------------------

var utilityNav = utilityNav || {
    bodyOverlay: "#body-overlay",
    selector: ".unityNavigation",
    topNav: ".unityNavigation .topNav",
    header: "header",
    menuId: ".unityNavigation #menu",
    menuClass: ".unityNavigation .menu",
    menuSmall: ".unityNavigation .menu-small",
    productList: ".unityNavigation > .products",
    productListClose: ".unityNavigation .products .close",
    secondaryListExpand: ".unityNavigation .secondary-link-group .more",
    mediumUptertiaryNav: ".unityNavigation .secondary-link-group .tertiaryNav",
    productListSmall: ".unityNavigation .product-drawer-small",
    productListSmallExpand: ".unityNavigation .product-drawer-small .more",
    currSize: "",
    iScrollPos: 0,
    slideUpDone: false,

     navTransition: function(){
            $(utilityNav.selector).removeClass('topNavTransition');
            if(ssize != "small") {
                $('body .mainContent').first().css({'margin-top':$(utilityNav.header).height() +'px'});
                $(utilityNav.selector).addClass('topNavTransition');
                var timer;
                $(window).scroll(function () {
                    clearTimeout(timer);
                    timer = setTimeout(utilityNav.handleNavOnScroll, 25);
                });
            }
    },
    init: function() {
        //for medium up
        $(utilityNav.menuClass+', '+ utilityNav.productListClose).click(function() {
            $(utilityNav.menuId).toggleClass("active");
            if($(utilityNav.menuId).hasClass('active')) {
                $('body .mainContent').first().css({'margin-top':'0'});
                $(utilityNav.selector).removeClass('topNavTransition');
            }else {
                $('body .mainContent').first().animate({'margin-top':$(utilityNav.header).height() +'px'},300,function(){
                    if(!$(utilityNav.selector).hasClass('topNavTransition')){
                        $(utilityNav.selector).addClass('topNavTransition');
                    }
                });
            }
            $(utilityNav.productList).stop().slideToggle("slow");
            utilityNav.closeTertiary();
        });
        
        $(utilityNav.secondaryListExpand).each(function(index) {
            $(this).click(function() {
                $(utilityNav.menuId).removeClass("active");
                $(utilityNav.productList).slideUp("slow");
                
                $(utilityNav.secondaryListExpand).not(':eq('+index+')').addClass('expand').removeClass('less')
                $(utilityNav.mediumUptertiaryNav).not(':eq('+index+')').hide();
                $(this).toggleClass('expand').toggleClass('less');
                $(utilityNav.mediumUptertiaryNav).eq(index).stop().slideToggle("slow");
            });
        });

        //for small
        $(utilityNav.menuSmall).click(function() {
            $(utilityNav.menuId).toggleClass("active");
            $(utilityNav.bodyOverlay).fadeToggle();
            $(utilityNav.selector).toggleClass('topNavTransition');
            $(utilityNav.productListSmall).slideToggle("slow");
        });

        //handle show/hide for sub nav products - small
        $(utilityNav.productListSmallExpand).click(function() {
            $(this).toggleClass("expand").toggleClass("less").next().slideToggle("slow", function() {
                utilityNav.menuScrollHeight();
            });
        });
        //handle top nav transition
        this.navTransition();
        $(window).resize(utilityNav.navTransition);
    },
    menuScrollHeight: function(expandedDivHeight){
        var totalHeight = $(utilityNav.topNav).height();
        if ($(utilityNav.productListSmall).has('.mCSB_container').length > 0) {
            totalHeight = $('.mCSB_container').height();
        }
        if(totalHeight > $(window).height()) {
            $(utilityNav.productListSmall).height($(window).height());
            $(utilityNav.productListSmall).mCustomScrollbar({
                theme:"light",
                mouseWheel:{preventDefault: true},
                contentTouchScroll: 100,
                scrollInertia: 100
            });
        }else {
            $(utilityNav.productListSmall).css({'height':'auto'});
            if(typeof $(utilityNav.productListSmall).mCustomScrollBar !== "undefined") {
                $(utilityNav.productListSmall).mCustomScrollBar('destroy');
            }
        }
    },
    closeTertiary: function(){
        $(utilityNav.secondaryListExpand).each(function(index) {
            if($(this).hasClass("less")) {
                $(this).removeClass("less").addClass("expand");
                $(utilityNav.mediumUptertiaryNav).eq(index).hide();
            }
        });
    },
    handleNavOnScroll: function(){
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > utilityNav.iScrollPos) {
            if(!$(utilityNav.selector).hasClass('topNavTransition')){
                    $(utilityNav.selector).addClass('topNavTransition');
            }

            if(iCurScrollPos > $(utilityNav.header).height()) {

                // Close the product list if open
                if($(utilityNav.menuId).hasClass('active')) {
                    $(utilityNav.menuId).removeClass("active");
                    $(utilityNav.productList).slideUp("slow");
                }

                $(utilityNav.selector).slideUp("slow",function(){
                    $(utilityNav.selector).removeClass('topNavTransition');
                    utilityNav.closeTertiary();
                    utilityNav.slideUpDone = true;
                });
            }
        } else {
            if(utilityNav.slideUpDone){
                if(!$(utilityNav.selector).hasClass('topNavTransition'))
                    $(utilityNav.selector).toggleClass('topNavTransition').slideDown("slow", function(){
                        utilityNav.slideUpDone = false;
                    });
            }
        }
        utilityNav.iScrollPos = iCurScrollPos;
    }
};