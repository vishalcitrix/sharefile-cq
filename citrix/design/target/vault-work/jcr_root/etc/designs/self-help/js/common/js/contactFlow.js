$(document).ready(function() {
    $('#selfServiceCategory ul li').click(function() {
        var contactflow = $(this).attr('contactflow');
        var categoryName = $(this).text();
        var category = categoryName.replace(/\s+/g, '');
        var breadcrumb = ' <i class="icon-chevron left-arrow-icon"></i>' + categoryName;
        var dataPrev = '#selfServiceCategory';
        var dataCurrent = '';
        var subCategory = '';
        $("#selfServiceSubCategory").find("ul").css("display", "none");
        if (contactflow == 'call') {
            $('#selfServiceContactIcons,#selfServicecreateTicket,#selfServiceCategory,.resp-accordion').hide();
            $('#selfServiceContactIcons').removeClass('show-for-large-up');
            $('.resp-accordion').removeClass('show-for-medium-down');
            $('#selfServiceContact,#selfServiceContactDetails,#selfServiceCall,#call'+category).show();
            dataCurrent = '#selfServiceContact,#selfServiceCall';
            subCategory = "false";
        } else if (contactflow == 'ticket') {
            $('#selfServiceContactIcons,#selfServiceCall,#selfServiceCategory,.resp-accordion').hide();
            $('#selfServiceContactIcons').removeClass('show-for-large-up');
            $('.resp-accordion').removeClass('show-for-medium-down');
            $('#selfServiceContact,#selfServiceContactDetails,#selfServicecreateTicket').show();
            dataCurrent = '#selfServiceContact,#selfServicecreateTicket';
            subCategory = 'false';
            $('#casecategory').val(categoryName);
        } else if (contactflow == 'callticket') {
            $('#selfServiceCategory').hide();
            $('#selfServiceContact,#selfServiceContactDetails,#call'+category).show();
            $('#selfServiceContactIcons').addClass('show-for-large-up');
            $('.resp-accordion').addClass('show-for-medium-down');          
            dataCurrent = '#selfServiceContact,#selfServiceContactIcons';
            subCategory = 'false';
            $('#casecategory').val(categoryName);
        } else {
            $('#selfServiceCategory').hide();
            $('#selfServiceSubCategory,#' + category).show();
            dataCurrent = '#selfServiceSubCategory';
            subCategory = 'true';
            $('#casecategory').val(categoryName);
        }
        $('.crumb p').html(breadcrumb).attr({
            'data-prev': dataPrev,
            'data-current': dataCurrent,
            'category-Name': categoryName,
            'sub-category': subCategory
        });
    });
    $(".crumb p").click(function() {
        var dataPrev = $(this).attr('data-prev');
        var dataCurrent = $(this).attr('data-current');
        var categoryName = $(this).attr('category-Name');
        var category = categoryName ? categoryName.replace(/\s+/g, '') : categoryName;
        var subCategory = $(this).attr('sub-category');
        var breadcrumb = ' <i class="icon-chevron left-arrow-icon"></i>' + categoryName;
        $('#call'+category).hide();
        if (subCategory == 'true') {
            $('.crumb p').html(breadcrumb).attr({
                'data-prev': dataPrev,
                'data-current': dataCurrent,
                'category-Name': categoryName,
                'sub-category': 'false'
            });
            $(dataCurrent).hide();
            $(dataPrev).show();
        } else if (subCategory == 'false') {
            $('.crumb p').html(breadcrumb).attr({
                'data-prev': dataPrev,
                'data-current': dataCurrent,
                'category-Name': categoryName,
                'sub-category': 'None'
            });
            $('#selfServiceCategory').show();
            $('#selfServiceSubCategory,#' + category).hide();
            $(dataCurrent).hide();
        } else {
            $(dataCurrent).hide();
            $(dataPrev).show();
            $('.crumb p').html(breadcrumb).attr({
                'data-prev': dataPrev,
                'data-current': dataCurrent
            });
        }

    });
    $('#selfServiceSubCategory ul li').click(function() {
        var contactflow = $(this).attr('contactflow');
        var subcategoryName = $(this).text();
        var breadcrumb = ' <i class="icon-chevron left-arrow-icon"></i>' + subcategoryName;
        var dataPrev = '#selfServiceSubCategory';
        var dataCurrent = '';
        var categoryName = $(this).attr('category-Name');
        var category=  categoryName.replace(/\s+/g, '');
        if (contactflow == 'call') {
            $('#selfServiceContactIcons,#selfServicecreateTicket,#selfServiceCategory,#selfServiceSubCategory,.resp-accordion').hide();
            $('#selfServiceContactIcons').removeClass('show-for-large-up');
            $('.resp-accordion').removeClass('show-for-medium-down');
            $('#selfServiceContact,#selfServiceContactDetails,#selfServiceCall,#call'+category).show();
            dataCurrent = '#selfServiceContact,#selfServiceCall';
        } else if (contactflow == 'ticket') {
            $('#selfServiceContactIcons,#selfServiceCall,#selfServiceCategory,#selfServiceSubCategory,.resp-accordion').hide();
            $('#selfServiceContactIcons').removeClass('show-for-large-up');
            $('.resp-accordion').removeClass('show-for-medium-down');
            $('#selfServiceContact,#selfServiceContactDetails,#selfServicecreateTicket').show();
            dataCurrent = '#selfServiceContact,#selfServicecreateTicket';
        } else if (contactflow == 'callticket') {
        
            $('#selfServiceCategory,#selfServiceSubCategory').hide();
            $('#selfServiceContact,#selfServiceContactDetails,#call'+category).show();
            $('#selfServiceContactIcons').addClass('show-for-large-up');
            $('.resp-accordion').addClass('show-for-medium-down');          
            dataCurrent = '#selfServiceContact,#selfServiceContactIcons';
        } else {
        }
        $('.crumb p').html(breadcrumb).attr({
            'data-prev': dataPrev,
            'data-current': dataCurrent,
            'category-Name': categoryName,
            'sub-category': 'true'
        });

    });
    $('#selfServiceCategory ul li, #selfServiceSubCategory ul li').click(function() {
        var contactflow = $(this).attr('contactflow');
       if( contactflow == '' ){
            $(".resp-tab-content").hide()
            $(".active").removeClass("active resp-tab-active");
        }
        formReset();
    });
    $('.crumb p').click(function() {
            $(".resp-tab-content").hide();
            $(".active").removeClass("active resp-tab-active");
    });
    $('.case-tab, .prod-dropdown .crumb').click( function(){
       formReset();
       /* Only audio */
       if( $(".not-only-audio #onlyAudio").length != 0 ){
           $(".not-only-audio, .only-audio, .audio-24").hide();
       }
       else{
           $(".not-only-audio").show();
       }
    });
    function formReset() {
        $(".emailbox #emailForm").get(0).reset();
        $(".emailbox #emailForm .error.icon-attention").remove();
        $(".emailbox #emailForm li").removeClass('error-field');
        $(".emailbox #emailForm input, #emailForm textarea").removeClass('invalid');
        $(".emailbox #emailForm label").removeClass('value animated fadeIn');
        $("#createTicketResult, .callResult, .ui-autocomplete").hide();
        $(".emailbox #emailForm").show();    
    }
});