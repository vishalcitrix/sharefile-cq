var Common = Common || {
    shLocale: ( utag_data['language'] ) ? utag_data['language'] : "en",
    init: function() {
        shCookie.init();
        callApi.init();
        catLeftNav.init();        
        contactUs.init();
        countrySelector.init();
        dropdown.init();
        Link.init();
        tabs.init();
        utilityNav.init();
        Lightbox.init();        
    }   
};

var localeString = {
    "en":{"callPlaceholder":"Enter country name or scroll through the list below","backproduct":"Back to all Products","change":"Change","changeProduct":"Change Product","contact":"Contact","contactSupport":"Contact Support","helpMessage":"Help requests generally takes 1-3 business days to complete.","longDistance":"Long Distance","searchSupport":"Search Support","tollFree":"Toll-free","trackMsg":"To track your request use ticket # ","recommended":"Additional Resources", "most-viewed":"Training Videos"},
    "de":{"callPlaceholder":"Ländernamen eingeben oder Bildlauf durch die Liste unten durchführen","backproduct":"Zurück zu allen Produkten","change":"Ändern","changeProduct":"Produkt wechseln","contact":"kontaktieren","contactSupport":"Support kontaktieren","helpMessage":"Supportanfragen nehmen in der Regel 1 bis 3 Werktage in Anspruch.","longDistance":"Direktwahl","searchSupport":"Support suchen","tollFree":"Gebührenfrei","trackMsg":"Zum Nachverfolgen Ihrer Anfrage verwenden Sie Ticket Nr. ","recommended":"Empfohlen", "most-viewed":"Meistgesehen"},
    "es":{"callPlaceholder":"Escriba el nombre del país y desplácese por la lista que hay a continuación.","backproduct":"Volver a todos los productos","change":"Cambiar","changeProduct":"Cambiar de producto","contact":"Contacto con","contactSupport":"Contacto con Asistencia","helpMessage":"Las solicitudes de asistencia suelen tardar entre uno y tres días laborables en completarse.","longDistance":"Larga distancia","searchSupport":"Buscar en Asistencia","tollFree":"Audio gratuito","trackMsg":"Para realizar un seguimiento de su solicitud, use el tíquet n.º ","recommended":"Recomendado", "most-viewed":"Popular"},
    "fr":{"callPlaceholder":"Enter country name or scroll through the list below","backproduct":"Back to all Product","change":"Change","changeProduct":"Change Product","contact":"Contact","contactSupport":"Contact Support","helpMessage":"Help requests generally takes 1-3 business days to complete.","longDistance":"Long Distance","searchSupport":"Search Support","tollFree":"Toll-free","trackMsg":"To track your request use ticket # ","recommended":"Recommandés", "most-viewed":"Populaires"},
    "it":{"callPlaceholder":"Enter country name or scroll through the list below","backproduct":"Back to all Product","change":"Change","changeProduct":"Change Product","contact":"Contact","contactSupport":"Contact Support","helpMessage":"Help requests generally takes 1-3 business days to complete.","longDistance":"Long Distance","searchSupport":"Search Support","tollFree":"Toll-free","trackMsg":"To track your request use ticket # ","recommended":"Consigliati", "most-viewed":"Articoli visualizzati più frequentemente"},
    "ja":{"callPlaceholder":"Enter country name or scroll through the list below","backproduct":"Back to all Product","change":"Change","changeProduct":"Change Product","contact":"Contact","contactSupport":"Contact Support","helpMessage":"Help requests generally takes 1-3 business days to complete.","longDistance":"Long Distance","searchSupport":"Search Support","tollFree":"Toll-free","trackMsg":"To track your request use ticket # ","recommended":"おすすめ", "most-viewed":"人気"},
    "pt":{"callPlaceholder":"Enter country name or scroll through the list below","backproduct":"Back to all Product","change":"Change","changeProduct":"Change Product","contact":"Contact","contactSupport":"Contact Support","helpMessage":"Help requests generally takes 1-3 business days to complete.","longDistance":"Long Distance","searchSupport":"Search Support","tollFree":"Toll-free","trackMsg":"To track your request use ticket # ","recommended":"Recomendado", "most-viewed":"Popular"}
}

var urlObj = {
    "meeting": { "signinurl": "https://global.gotomeeting.com", "serviceurl":"/meeting/redirect/service-status" },
    "webinar": { "signinurl": "https://global.gotowebinar.com", "serviceurl":"/webinar/redirect/service-status" },
    "training": { "signinurl": "https://global.gototraining.com", "serviceurl":"/training/redirect/service-status" },
    "openvoice": { "signinurl": "https://global.openvoice.com", "serviceurl":"http://status.gotomeeting.com/" },
    "gotomypc": { "signinurl": "https://www.gotomypc.com/members/login.tmpl", "serviceurl":"/gotomypc/redirect/service-status" },
    "concierge": { "signinurl": "http://concierge.cloud.com/login", "serviceurl":"#" },
	"shareconnect": { "signinurl": "https://app.shareconnect.com/members/socialLogin.tmpl?state=initLogin&authProvider=shareFile", "serviceurl":"#" },
	"sharefile": { "signinurl": "https://secure.sharefile.com/login/web", "serviceurl":"http://status.sharefile.com/" },
    "gotoassist": { "signinurl": "http://app.gotoassist.com", "serviceurl":"http://status.gotoassist.com/" },
    "gotoassistmonitoringlite": { "signinurl": "http://app.gotoassist.com", "serviceurl":"http://status.gotoassist.com/" },
    "gotoassistremotesupport": { "signinurl": "http://app.gotoassist.com", "serviceurl":"http://status.gotoassist.com/" },
    "gotoassistservicedesk": { "signinurl": "http://app.gotoassist.com", "serviceurl":"http://status.gotoassist.com/" },
    "gotoassistcorporate": { "signinurl": "http://app.gotoassist.com", "serviceurl":"http://status.gotoassist.com/" },
	"wordpresscloud": { "signinurl": "", "serviceurl":"" }
}

$(document).foundation();
jQuery(function() {
    setTimeout(Common.init, 250);
});

/* Handling empty search */
$(document).ready(function() {
    $('.search form button.icon-search').attr('disabled','disabled');
    $('.search input#tags').keyup(function() {
        if($(this).val() != '') {
            $('.search form button.icon-search').removeAttr('disabled');
        }
        else{
            $('.search form button.icon-search').attr('disabled','disabled');
        }
     });
     $('#gsfn_content, #gsfn_search_results').on( "click", ".gsfn_link", function(e) {
        e.preventDefault();
        window.open(jQuery(this).attr('href'), '_blank');
    });

    if( $(".help-tabs").length != 0  ) {
        
        $(".help-tabs button").on('click', function() {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            var tab = $(this).attr("data-tab");
            $("input[name=user]").val(tab.replace("#", ""));            
            $('section[name="instant-help"]').not(tab).hide();
            $(tab).fadeIn();
        });             
    }

    if (jQuery("#join-help-home").length != 0) {
    
        $('body').on('click', '#join-help-home .large-6:first-child', function(){
            $('#join-help-attendee').show();
            $('#join-help-home').hide();
            shCookie.setCookie('sh_user', 'attendee', 30);
        });
            
        $('body').on('click', '#join-help-home .large-6:last-child', function(){
            $('#join-help-organizer').show();
            $('#join-help-home').hide();
            shCookie.setCookie('sh_user', 'organizer', 30);
        });
            
        $('body').on('click', '.join-help-change a', function(){
            $('#join-help-organizer, #join-help-attendee').hide();
            $('#join-help-home').show();
        });
        
        if( shCookie.getCookie('sh_user') != '' ){
            $('.categorySpotlight.'+ shCookie.getCookie('sh_user')).parents('.large-6').trigger('click');
        }
    }
 });
