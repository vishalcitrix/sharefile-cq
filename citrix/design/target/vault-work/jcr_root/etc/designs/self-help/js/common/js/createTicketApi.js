var createTicketApi = createTicketApi || {
    init: function() { 
        $(document).ready(function() {
            var inst_join;
            var form_container = ( createTicketApi.form ) ? "div[data-form="+createTicketApi.form+"]" : ''; //get the form which is used
            var form = form_container +' #emailForm';
            $(form).hide();
            $( "<div class='loader'></div>" ).insertAfter( $( form ) );           
            $(form_container+' #createTicketResult .errormessage').hide();
            $(form_container+' #createTicketResult').hide();
            $(".web2call_header").hide(); 

            var inst_join="";
            /* Instant join */
            if( $("#IJFree").length != 0 ){
                inst_join = "IJFree - ";
            }
            else if( $("#IJPaid").length != 0 ){
                inst_join = "IJPaid - ";
            }

            /* Only Audio */
            var audioData = '';
            if( $("#audio_num").length != 0 && $("#audio_num").val() != '' ){ 
                var phoneCalled = "Phone Called:"+$("#audio_num").val()+"&";
                var phoneFrom = "Phone From:"+$("#audio_num_frm").val()+"&";
                var accessCode= "Access Code:"+$("#audio_access").val()+"&";
                var issueDateTime= "Issue Date:"+$(".audio-date").val()+" "+$(".audio-time").val()+"&";
                var issueCountyZone= "Country:"+$(".audio-country").val()+" "+$(".audio-timezone").val();
                audioData = '['+phoneCalled+phoneFrom+accessCode+issueDateTime+issueCountyZone+'] ';
                inst_join =  "Audio Escalation - " + inst_join;
            }
            
            var catData = "=>Cat:"+$(form+' #casecategory').val() + "&subCat:"+$(form+' #casesubcategory').val();
            
            var queryParams = {
                'product': $(form+' #caseproduct').val(),
                'category': $(form+' #casecategory').val(),
                'subject': inst_join + $(form+' #subject').val(),
                'question': audioData +$(form+' #question').val()+catData,
                'fname': $(form+' #fname').val(),
                'emailAddress': $(form+' #emailAddress').val(),
                'phoneNumber': $(form+' #phoneNumber').val(),
                'universalId': "6000123456",
                'language': "en_US",
                'recordType': "BillingSupport",
                'companyName': $(form+' #companyName').val(),
                'accountNumber': $(form+' #accountNumber').val(),
                'invoiceNumber': $(form+' #invoiceNumber').val(),
                'action': "createTicket",
                'web2call': $(form+' #from_call').val()
            }
            $.ajax({            
                url: "/bin/citrix/selfhelp/restapicall",
                data: queryParams,
                success: function(data) {
                    renderResponse(data)
                },
                error: function(data, status, error) {
                    renderResponse(data)
                }
            });
            function renderResponse(data) {
                if(data.errorCode == 'SUCCESS') {
                    $('.loader').hide();
                    $(form_container+' #createTicketResult p').not("p.errormessage").show();                    
                    $(form_container+' #createTicketResult').show();
                    if( $(form+' #from_call').val() == 1 ){
                        $(".web2call-header").hide();
                        $(".casenumber").empty().append(data.caseNumber);
                        $(".calldetail").find(".rfont16").text("United States ");
                        $(".calldetail").find(".call-free").text($(".web2call-header").attr('data-success-toll'));
                        $(".calldetail").find(".call-long").text($(".web2call-header").attr('data-success-long'));    
                        $(".calldetail").show();
                    }
                    else{
                        $(".casenumber").empty().append(localeString[''+Common.shLocale].trackMsg+data.caseNumber);                    
                    }
                    /* For Channel Tracking */
                    utag_data['template']= jQuery(form_container+' #casesubcategory').val()+'-submitted';
                    utag_data['sub-section']= 'create ticket';
                    updateUtagObj(utag_data);                   
                } else {
                    $('.loader').hide();
                    $('#createTicketResult p').hide(); 
                    if( $(form+' #from_call').val() == 1 ){
                        $(".web2call-header").hide();
                        $(".calldetail").find(".rfont16").text("United States ");
                        $(".calldetail").find(".call-free").text($(".web2call-header").attr('data-success-toll'));
                        $(".calldetail").find(".call-long").text($(".web2call-header").attr('data-success-long'));                                        
                        $(".calldetail").show();
                    }
                    else{
                        $('#createTicketResult, #createTicketResult .errormessage').show();
                    }
                }
            }
        });
    }
};