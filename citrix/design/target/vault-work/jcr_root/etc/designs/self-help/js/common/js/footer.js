var footer = footer || {
    footerlang: {
                    'support':[ 'English','Deutsch','Español','Français','Italiano','日本語','Português' ],
                    'g2m':[ 'English','Deutsch','Español','Français','Italiano' ],
                    'g2w':[ 'English','Deutsch','Español','Français','Italiano' ],
                    'g2t':[ 'English','Deutsch','Español','Français','Italiano' ],
                    'g2pc':[ 'English','日本語' ],
                    'ov':[ 'English','Deutsch','Español','Français' ],
                    'sharefile':[ 'English','日本語' ],
                    'shareconnect':[ 'English' ],
                    'g2a-rs':[ 'English','Deutsch','Español','Français','Italiano','Português' ],
                    'g2a-sd':[ 'English' ],
                    'g2a-corp':[ 'English','Deutsch','Español','Français' ],
                    'g2a-mo':[ 'English','Deutsch','Español','Français','Italiano','Português' ],
                    'g2a_con':[ 'English' ],
                },
    init: function() {
                var footerlang = footer.footerlang;
                page = utag_data['product'];
                var langarr = ( page!='' && (page in footerlang) ) ?  footerlang[page] : [ 'English' ] ;
                
                if( utag_data['product'] == 'citrix' ) {
                   if( utag_data['section'] == 'support' ) {
                       langarr = [ 'English','Deutsch','Español'];
                   }
                   else if( utag_data['section'] == 'self-service' ){
                       langarr = [ 'English'];
                   }        
                }                
                else if( utag_data['template'] == 'support' ){
                    langarr = footerlang['support']
                }                
                else if( page=='g2m' && utag_data['template'] != 'home' ){
                    langarr = [ 'English' ];
                }
                
                $('.footer-lang .citrix-rg-12px-lang').find('span').not('span:first-child').each(function(){                    
                    
                        var lang = ( jQuery(this).find('b').text() ) ? jQuery(this).find('b').text() : jQuery(this).find('a').attr('title');
                        
                        if( jQuery.inArray( lang , langarr ) < 0 ){
                                jQuery(this).remove();
                        }
                    
                });
                
        }
};