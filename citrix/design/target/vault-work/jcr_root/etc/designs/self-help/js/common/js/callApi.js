var callApi = callApi || {
    init: function() {  
        (function() {
             $('.changecountry').live('click', function(e){
                 e.preventDefault();
                 $('.contact-category .calldetail').hide();
                 $('.ui-autocomplete-input').val('').removeClass('call_error_highlight');
                 $('.call_error').hide();
                 $('.contact-category .call .ui-widget,.contact-category .ui-widget .icon-closes').show();
                 $('.ui-autocomplete-input').autocomplete("search", " ");
             });

             $('#country-selector').selectToAutocomplete();
             $('.ui-autocomplete-input').focus( function(){                       
               $(this).autocomplete("search", " ");
             });
             $('#country-selector').live('change', function() {
                var countryName=this.value;
                
                if( $(".ui-autocomplete:visible").length != 0 ){
                    $.ajax({
                        url: '/bin/citrix/selfhelp/restapicall',
                        data: "product=" + $("#caseproduct").val()+ "&country=" + countryName+"&action=call",
                        success: function(result) {
                            renderHTML(result, countryName);
                        },
                        error: function(result, status, error) {
                            renderHTML(result);
                        }
                    });
                }
                else{
                    renderHTML("");
                }
                $('.contact-category .icon-closes').show();
            });

            function renderHTML(data, country) {
                data=JSON.stringify(data);
                var jsondata = $.parseJSON(data);
                if(jsondata.errorCode == 'SUCCESS') {
                    $('.contact-category .calldetail').show();
                    $('.contact-category .call .ui-widget,.contact-category .ui-widget .icon-closes').hide();
                    var html= "<p><span class='rfont16'>"+country+" </span><span><a href='#' class='changecountry'>"+localeString[''+Common.shLocale].change+"</a></span></p><p><span>"+localeString[''+Common.shLocale].tollFree+": </span><span class='bfont12 call-free'>"+jsondata['Toll Free Phone Number']+"</span></p><p><span>"+localeString[''+Common.shLocale].longDistance+": </span><span class='bfont12 call-long'>"+jsondata['Long Distance Phone Number']+"</span></p>"; 
                    $(".calldetail").empty().append(html);
                }else{
                     /*var html= "<p><span class='rfont16'>Kindly enter correct country name.</span></p>"*/
                     
                }  
                
            }

            $('.contact-category .ui-widget .icon-closes').click(function(){
                $('.contact-category .ui-widget,#country-selector,.contact-category .ui-widget .icon-closes, .ui-autocomplete').hide();
                $('.contact-category .calldetail').show();
            });
        }).call(this);
    }
};