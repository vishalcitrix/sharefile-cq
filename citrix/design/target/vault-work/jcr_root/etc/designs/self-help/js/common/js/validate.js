var invalid = [];
var invalidSpecialChar = [];
var isValid = false;
var isSpecialChar = false;

/* Validate form onsubmit*/
jQuery('.emailbox form, .callbox form').submit(function(e) {
    e.preventDefault();
    invalid = [];
/* Check validity of required fields */
    jQuery(this).find(".required").each( function() {
        if( !isRequired(this) && $(this).is(":visible")) {
            jQuery(this).addClass("invalid")
            jQuery(this).parent('li').find('div.error').show();
            jQuery(this).parent('li').find('.form-helper-text').hide();            
            invalid.push(this)
        } else{
            jQuery(this).removeClass("invalid")
            jQuery(this).parent('li').find('div.error').hide();
            jQuery(this).parent('li').find('.form-helper-text').show();
        }
    });
/* Check for email validity */
    jQuery(this).find(".email").each( function() {
        if( !isValidEmail(this) ) {
            jQuery(this).addClass("invalid")
            invalid.push(this)
        } else {
            jQuery(this).removeClass("invalid")
        }
    });
    
/* Check for special character validity */
    jQuery(this).find(".special-char").each( function() {
        if( !isValidChar(this) ) {
            jQuery(this).addClass("invalid");
        }else{
            jQuery(this).removeClass("invalid");
        }
    });

    var flag = invalid.length;
    invalid = jQuery.unique(invalid);
    
    if(flag == 0 && invalidSpecialChar.length == 0) {
        var attr = $(this).parent().attr('data-form');        
        createTicketApi.form = ( typeof attr !== typeof undefined && attr !== false ) ? $(this).parent().attr('data-form') : '';        
            
        createTicketApi.init();
    } else {
        displayError();
    }
});

/* Check only number */
$('input.phone').bind('keypress', function (event) {
    var regex = new RegExp("^[-0-9 \( \) + . # , \b ext]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    var phone = jQuery(this).val().length;
    if ((!regex.test(key)) || (phone > 15 && key!='\b')) {
       event.preventDefault();
       return false;
    }
});

/* Check only alphabet*/
$('input.fname').bind('keypress', function (e) {  
    var key = ( e.charCode != 0 ) ? e.charCode : e.keyCode;
    if ( ( key >= 65 && key <= 90 ) || ( key >= 97 && key <= 122 ) || key == 8 || key == 46 || key == 37  || key == 39 || key == 32 || key == 9  ) {
       return true;
    }
    e.preventDefault();
    return false;
});

/* Check Required fields */
function isRequired(theObj) {
    if( ((theObj.type == "text"|| theObj.type == "select-one" || theObj.type == "textarea") && theObj.value == "") || (theObj.type == "checkbox" && !theObj.checked) ) {
        return false;
    } else {
        return true;
    }
}
/* Validate Email Address */
function isValidEmail(theObj) {
    var emailRegEx = RegExp('^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$');
    return emailRegEx.test(theObj.value)
}

/* Validate Special Character */
function isValidChar(theObj) {
    var text = $(theObj).val();
    if(text !=""){
        if(check(text) == false){
            // Code that needs to execute when none of the above is in the string
            return true;
        }else{
            //alert('Your search string contains illegal characters.');
            invalidSpecialChar.push(theObj)
            return false;
        }
    }
    else{
        return true;
    }
}

/* Validate phone number. Return error if phone field is required */
function isValidPhone(theObj) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;  
    return phoneNumberPattern.test(theObj.value); 
}

/* Check validity of required fields on Focusout */
jQuery('input[type="text"],textarea').on('blur', function() {
    if(!jQuery(this).hasClass('novalidate')){
        if( !isRequired(this) && $(this).is(":visible")) {
            jQuery(this).addClass("invalid");
            invalid.push(this)
            jQuery(this).parent('li').find('div.error').show();
            jQuery(this).parent('li').find('.form-helper-text').hide();
        } else{
            jQuery(this).removeClass("invalid");
            jQuery(this).parent('li').find('div.error').hide();
            jQuery(this).parent('li').find('.form-helper-text').show();
        }
        displayError(jQuery(this));
    }
});

var specialChars = "`~^|><{}[]"
var check = function(string){
    for(i = 0; i < specialChars.length;i++){
        if(string.indexOf(specialChars[i]) > -1){
            return true
        }
    }
    return false;
}

function displayError(theObj){
    if(typeof(theObj) != "undefined" ) {
        var objParent = jQuery(theObj).parent();
        objParent.removeClass("error-field");
        jQuery(objParent).find("div.error").remove();
    } else {
        jQuery(".error-field").removeClass("error-field");
        jQuery(".error").remove();
    }
    
    if(jQuery(".invalid").length < 1) {
        jQuery(".error").hide();
    }
    
    var flag = invalid.length;
    invalid = jQuery.unique(invalid);
    while( invalid.length>0 ) {
        var theObj = invalid.pop();
        var parentObj = jQuery(theObj).parent();
        
        parentObj.addClass("error-field");
        parentObj.prepend('<div class="hidden error icon-attention">'+theObj.title+"</div>")
    }
    
    var flag = invalidSpecialChar.length;
    invalidSpecialChar = jQuery.unique(invalidSpecialChar);
    while( invalidSpecialChar.length>0 ) {
        var theObj = invalidSpecialChar.pop();
        var parentObj = jQuery(theObj).parent();
        
        parentObj.addClass("error-field");
        parentObj.prepend('<div class="hidden error icon-attention">Special Characters are not allowed!!</div>')
    }
}