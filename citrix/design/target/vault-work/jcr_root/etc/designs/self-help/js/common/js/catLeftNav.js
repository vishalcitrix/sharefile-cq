var catLeftNav = catLeftNav || {
    init: function() {
            if( $(".cat-drpdown").length != 0 ){
		        jQuery(".cat-drpdown").on('click', function(event){
		                $(this).toggleClass('active');
		                event.stopPropagation();
		        });
		        $(document).click(function() {        
		            $('.cat-drpdown').removeClass('active');
		        }); 
			}
        }
};