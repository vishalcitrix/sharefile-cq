$('document').ready( function() {

     if(  $( "#join-download" ).length != 0 ){
        $( ".sh-accordion" ).accordion({
            header: "> div > h3",
            collapsible: true,
            active: false,
            heightStyle: "content"
        });
        
        $('body').on('change', '.download-os-broswer select', function() {
            var classOS = $('.download-os').find('option:selected').val().replace(" ", "").toLowerCase();
            var classBrowser = $('.download-browser').find('option:selected').val().replace(" ", "").toLowerCase();
            $("#join-download .group").show().not('.'+classOS+'.'+classBrowser).hide();
        });
        
        if( typeof sh !== 'undefined' ){            

            if( sh.os == "Mac OS" ){
                $('.download-browser option[value="IE"]').remove();
                sh.browser = (sh.browser == 'IE') ? 'other': sh.browser;
            }
            else if( sh.os == 'Windows'){
                $('.download-browser option[value="safari"]').remove();
                sh.browser = (sh.browser == 'safari') ? 'other': sh.browser;
            }
            else{
                $('.download-browser option[value="safari"], .download-browser option[value="IE"]').remove(); 
                if( sh.mobile ){
                    sh.browser = 'other';
                }    
            }
            
            var os = (sh.os != 'Windows' && sh.os != 'Mac OS') ? 'otheros' : sh.os;
            var browsr =    (sh.browser != 'Firefox' && sh.browser != 'Chrome' && sh.browser != 'IE' && sh.browser != 'safari') ? 'other' : sh.browser;     
            
            $('.download-os option[value="'+os+'"]').attr('selected', 'selected').trigger('change');
            $('.download-browser option[value="'+browsr+'"]').attr('selected', 'selected').trigger('change');
        }
    }
  
});