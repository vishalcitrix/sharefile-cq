jQuery( document ).ready( function(){
    jQuery('.recommended a span').live('click', function(){
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = localeString[''+$("meta[http-equiv=content-language]").attr('content').substring(0,2)].recommended;
        updateUtagObj();
    });

    jQuery('.most-viewed a span').live('click', function(){
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = localeString[''+$("meta[http-equiv=content-language]").attr('content').substring(0,2)]["most-viewed"];
        updateUtagObj();
    });
 
    $('.unityChooseProducts ul li a').click( function(e){
        utag_data['template'] = $(this).text().trim()+' -Support For More Products';
        utag_data['sub_section'] = 'Choose Product';    
        updateUtagObj();
    });
    
    jQuery('.tabsection #parentHorizontalTab li').click( function(){
        var index = jQuery(this).index()+1;
        utag_data['template'] = "HorizontalTab"+index;    
        utag_data['sub_section'] = '';
        updateUtagObj();
    });
    
    jQuery('.resp-tabs-container a span').live('click', function(){
        var tabIndex = parseInt($('.resp-tab-active').attr('aria-controls').replace('hor_1_tab_item-','')) + 1;
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = 'HorizontalTab-'+tabIndex;
        updateUtagObj();
    });
    
    jQuery('.additional-resources a span').live('click', function(){
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = 'Additional Resources';
        updateUtagObj();
    });
    
    jQuery('.imageRenditions a').live('click', function(){
        utag_data['template'] = $(this).attr('title');    
        utag_data['sub_section'] = 'Training Videos';
        updateUtagObj();
    });

    jQuery('.training-videos a span').live('click', function(){
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = 'Training Videos';
        updateUtagObj();
    });

    jQuery('.categorySpotlight a').live('click', function(){
        utag_data['template'] = $(this).text().trim();    
        utag_data['sub_section'] = $(this).siblings('h2').text().trim();
        updateUtagObj();
    });

    jQuery('.footer .citrix-rg-12px-lang a').live('click', function(){        
        if( $(this).attr('href').indexOf('de_DE') != -1) {
            utag_data['language'] = 'de';
        } else if( $(this).attr('href').indexOf('fr_FR') != -1 ) {
            utag_data['language'] = 'fr';
        } else if( $(this).attr('href').indexOf('es_ES') != -1 ) {
            utag_data['language'] = 'es';
        } else if( $(this).attr('href').indexOf('it_IT') != -1 ) {
            utag_data['language'] = 'it';
        } else if( $(this).attr('href').indexOf('jp_JP') != -1 ) {
            utag_data['language'] = 'jp';
        } else {
            utag_data['language'] = 'en';
        }
        updateUtagObj();
    });
 
    /* Self service contact */
    if( jQuery('#selfServiceCategory').length != 0 ){
        jQuery('#selfServiceCategory .prod-dropdown ul li').click( function() {
            utag_data['template'] = jQuery(this).text();
            utag_data['sub_section'] = jQuery(this).text();
            updateUtagObj();
        });
    }
    if( jQuery('#selfServiceSubCategory').length != 0 ){
        jQuery('#selfServiceSubCategory .prod-dropdown ul li').click( function() {
            utag_data['template'] = jQuery(this).text();
            utag_data['sub_section'] = jQuery(this).attr('category-name');
            updateUtagObj();
        });
    }
    
    /* Generic for Ask Community, Create Ticket, Call */
    jQuery('#selfServiceCall, .call-tab').click( function() {
        utag_data['template']= 'Call';
        utag_data['sub_section']= jQuery('.contact-category').attr('data-parent');
        updateUtagObj();
    });
    jQuery("#com_linkpath").parent().click( function() {
        utag_data['template']= 'Ask Community';
        utag_data['sub_section']= jQuery('.contact-category').attr('data-parent');
        updateUtagObj();
    });
    
    /* Contact US */
    jQuery('#contact-list').delegate(".catLink p", "click", function () {
        utag_data['template']= jQuery(this).find('span').text();
        utag_data['sub_section']= jQuery(this).parent('.catLink').attr('data-parent');
        updateUtagObj();
    });
    
    /* Support */
    if( jQuery('#contact-list li a.product-selectorlogos').length != 0 ){
        jQuery('#contact-list li a.product-selectorlogos').click( function () {
            template = jQuery(this).attr('class').split(' ')[0].replace('product', '' );
            utag_data['template']= template;
            utag_data['sub_section']= 'Support';
            updateUtagObj();
        });
        jQuery('#contact-list #sub-productlist .g2a-products li a').click( function () {
            utag_data['template']= jQuery(this).text();
            utag_data['sub_section']= 'gotoassist';
            updateUtagObj();
        });
    }
    /* MyAccount */
    if( jQuery('#myaccount .servicelist').length != 0 ){
        jQuery('#myaccount .servicelist a').click( function () {
            utag_data['template']= jQuery(this).siblings('h2').text();
            utag_data['sub_section']= 'myaccount';
            updateUtagObj();
        });        
    }


    /* Category Page */
    var accountType;

    if (jQuery(".category-help").length != 0) {
        jQuery(".article-list li a").click(function() {
            utag_data.template = $.trim(jQuery(this).text());
            utag_data.sub_section = $.trim(jQuery(".category-help h1").text())+" - "+$.trim(jQuery(this).parents("ul").prev("h2").text());
            if( typeof( $('button.active').attr('data-tab') ) != 'undefined' ) {
                accountType = (window.location.pathname.indexOf('webapp/free') >= 0) ? 'Free' : 'Paid';
                utag_data.content_type = accountType+' '+$('button.active').attr('data-tab');            
            }
            updateUtagObj();
        });
        jQuery("body").on("click", ".gsfn_question a,  .gsfn_no_results a, .gsfn_submit a", function() {
            utag_data.template = $.trim(jQuery(this).text());
            utag_data.sub_section = $.trim(jQuery(".category-help h1").text()) +" - "+$.trim(jQuery(".category-community h2").text());
            updateUtagObj();
        });
        jQuery(".menu-container a.browse, .sh-contact a").click(function() {
            utag_data.template = $.trim(jQuery(this).text());
            utag_data.sub_section = $.trim(jQuery(".category-help h1").text());
            updateUtagObj();
        });
        jQuery("#jump-menu li a").click(function() {
            utag_data.template = $.trim(jQuery(this).text());
            utag_data.sub_section = $.trim(jQuery("#jump-menu li.nav-selected a").text()) + " - Jump Links";
            updateUtagObj();
        });
        jQuery(".searchsection button[type = 'submit']").click(function() {
            utag_data.template = "Search";
            utag_data.sub_section = $.trim(jQuery(".category-help h1").text());
            updateUtagObj();
        });
        jQuery("#gsfn_search button#continue").click(function() {
            utag_data.template = "Ask";
            utag_data.sub_section = $.trim(jQuery(".category-help h1").text());
            updateUtagObj();
        });
        jQuery(".instant.searchsection button[type = 'submit']").click(function() {
            if( $('input.form-control').val() != '') {
                utag_data.template = $('input.form-control').val();
                utag_data.sub_section = $.trim(jQuery(".category-help h1").text());
                updateUtagObj();
            }
        });
    }

    /* Join Help Page */
    if (jQuery("#join-help-home").length != 0) {
        jQuery(".categorySpotlight").click(function() {
            utag_data.template = jQuery(this).find('.servicelist h2').text();
            utag_data.sub_section = 'Join Help';            
            updateUtagObj();
        });
        jQuery(".join-help-list li a").click(function() {
            utag_data.template = jQuery(this).text();
            utag_data.sub_section = jQuery(this).parents('section').attr('id');            
            updateUtagObj();
        });
        jQuery(".join-help-change a").click(function() {
            utag_data.template = jQuery(this).text();           
            updateUtagObj();
        });
    }

    /* Join Download Issue Page */
    if(  $( "#join-download" ).length != 0 ){
     jQuery(".join-download-title-head").click(function() {
            utag_data.template = jQuery(this).text();
            utag_data.sub_section = $('.download-os').find('option:selected').val()+'-'+$('.download-browser').find('option:selected').val(); 
            utag_data.content_type = 'Join Help Download';           
            updateUtagObj();
        });

        jQuery(".article-list a").click(function() {
            utag_data.template = jQuery(this).text();
            utag_data.sub_section = 'Still Need help';
            utag_data.content_type = 'Join Help Download';             
            updateUtagObj();
        });

        jQuery("button .icon-search").click(function() {
            utag_data.template = jQuery(this).parents('.searchsection').find('#tags').val();
            utag_data.sub_section = 'Search';
            utag_data.content_type = 'Join Help Download';             
            updateUtagObj();
        });
    }

    /* Header : service status*/
    jQuery('.cssStatusLink').parent('a').on('click', function(){        
        utag_data.template = 'SERVICE STATUS';
        updateUtagObj();
    });
    /* Header : Sign In*/
    jQuery('.link-group #signIn').on('click', function(){
        utag_data.template = 'Sign In';
        updateUtagObj();
    });
    /* Header : More From citrix*/
    jQuery('#menu .menu').on('click', function(){
        utag_data.template = 'More From Citrix';
        updateUtagObj();
    });
    /* Header : Logo from More From citrix*/
    jQuery('.unityNavigation .products ul li a').on('click', function(){
        utag_data.template = jQuery(this).text()+' -More from citrix';
        updateUtagObj();
    });
    /* footer Links */
    jQuery('.footer-links .links a').on('click', function(){
        utag_data.template = jQuery(this).text();
        updateUtagObj();
    });
});
/* To delete after testing */
function updateUtagObj(udata){   
    utag.view(utag_data);
    //console.log('Template: '+utag_data['template']+'\nContent: '+utag_data['content_type']+'\nSection: '+utag_data['section']+'\nsub_section: '+utag_data['sub_section']+'\nProduct: '+utag_data['product']);
}

/* Channel Tracking for Contactus - Contactus.js */
function contactusTracking( tag, istag ){
    if( istag == 1 ){
        channel_products = { 'meeting': 'g2m', 'gotoassist':'g2a','sharefile':'sharefile','gotomypc':'g2pc','training':'g2t','webinar': 'g2w','openvoice':'ov','shareconnect':'shareconnect', 'concierge': 'g2a_con' };
        utag_data['template']=tag;
        utag_data['product']=channel_products[tag] ;
        utag_data['sub_section']= '' ;
        updateUtagObj();
    }
    else if( istag==2 ){
        udata = tag.split('/'); 
        tag_length= udata.length-1;
        utag_data['template']=udata[tag_length];
        utag_data['sub_section']=udata[tag_length-1];
        updateUtagObj();
    }
    else if( istag == 4 ){
        channel_products = { 'gotoassistcorporate':'g2a-corp', 'gotoassistremotesupport':'g2a-rs', 'gotoassistmonitoring':'g2a-mo', 'gotoassistservicedesk':'g2a-sd' };
        utag_data['template']=tag;
        utag_data['product']=channel_products[tag] ;
        updateUtagObj();
    }
}