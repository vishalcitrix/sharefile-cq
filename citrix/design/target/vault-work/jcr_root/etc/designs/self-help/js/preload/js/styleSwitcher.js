//----------------------------------------------------------
//Style switcher js, switch element styles based on view port
//----------------------------------------------------------
var styleSwitcher = styleSwitcher || {
    init : function () {
        var styleswitch = function () {
            $(".styleswitch").each(function () {
                var styleLarge = $(this).attr('data-large');
                var styleMedium = $(this).attr('data-medium');
                var styleSmall = $(this).attr('data-small');
                var lastElement = $(this).attr('data-lastelement');
                var winHeight = $(window).height();

                if(lastElement == "true" && lastElement != "undefined") {
                    winHeight = winHeight - $('#footerSection').height();
                }
                if (ssize == 'large') {
                    $(this).attr('style', styleLarge);
                    
                    if(mobile && $(this).hasClass('parallax'))
                    {
                       $(this).hide();
                    }else{
                        var desktopHeight = $(this).attr('data-dtautoheight');
                        if (desktopHeight == "true") {
                            styleSwitcher.resize(this,winHeight);
                        }
                    }
                } else if (ssize == 'med') {
                    $(this).attr('style', styleMedium);
                    var tabHeight = $(this).attr('data-tabautoheight');
                    if (tabHeight == "true") {
                        styleSwitcher.resize(this,winHeight);
                    }
                } else if (ssize == 'small') {
                    $(this).attr('style', styleSmall);
                    var mobHeight = $(this).attr('data-mobautoheight');
                    if (mobHeight == "true") {
                        styleSwitcher.resize(this,winHeight);
                    }
                }
            });
        };
        styleswitch();
        $(window).resize(styleswitch);
    },
    resize: function(obj,winHeight){
        var childHeight = $(obj).children().eq(0).outerHeight();
        if($(obj).parents().hasClass('owl-wrapper')){
            var objParent = $(obj).parents('.owl-wrapper');
            var i=0;
            var h=winHeight;
            var cItem = new Array();
            $(objParent).find('.owl-item').each(function(){
                cItem[i]=this;
                $(this).find("div.backgroundImage > div").each(function(){
                    if($(this).hasClass("styleswitch"))
                        $(this).css('height','auto');
                })
                if($(this).height() > h)
                    h=$(this).height();
                i++;
            });
            for(i=0; i<cItem.length; i++) {
                $(cItem[i]).find("div.backgroundImage > div").each(function(){
                    if($(this).hasClass("styleswitch"))
                        $(this).css('height',h);
                })
            }
         } else if (childHeight < winHeight) {
             $(obj).css('height', winHeight);
         }
    }
};