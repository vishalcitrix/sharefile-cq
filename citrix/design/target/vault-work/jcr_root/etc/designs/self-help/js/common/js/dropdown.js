var dropdown = dropdown || {
    init: function(){
        $(document).ready(function() {
            $("input:text, textarea").blur(function() {
                "" === $(this).val() ? $(this).siblings().removeClass("value animated fadeIn") : void 0;
            });
             $("input:text, textarea").focus(function() {
                $(this).siblings().addClass("value animated fadeIn");
            });
            $(".contact-options .tabs li").click(function() {
                 $(".contact-options .tabs li").removeClass("active");
                $(this).addClass("active");
                $("html, body").animate({
                    scrollTop: $("#cont-opt").offset().top
                }, 700);
            });
            $(".ask-tab").click(function() {
                  $(".ask-community").siblings().removeClass("show animated fadeIn");
                $(".ask-community").addClass("show animated fadeIn");
            });
            $(".case-tab").click(function() {
                $(".emailbox").siblings().removeClass("show animated fadeIn");
                $(".emailbox").addClass("show animated fadeIn");    
            });
            $(".call-tab").click(function() {
                  $(".call").siblings().removeClass("show animated fadeIn");
                $(".call").addClass("show animated fadeIn");
            });
            $('.showdropdown').click(function(e){
                e.preventDefault();
                $(this).parent('.prd-list').find('ul').toggle();
                $(this).parent('.prd-list').find('.resp-arrow').toggleClass('arrow-top')
            });
        })
    }
};
