var CurrentPageReference = CurrentPageReference || {

    init : function() {
        var shareLinks = $('.share.email').parent();
        var googlePlusLinks = $('.share.googlePlusSmall').parent();
        var facebookLinks = $('.share.facebookSmall').parent();
        
        CurrentPageReference.replaceCurrentPage(shareLinks);
        CurrentPageReference.replaceCurrentPage(googlePlusLinks);
        CurrentPageReference.replaceCurrentPage(facebookLinks);
    },

	replaceCurrentPage: function(links) {
		if(links != null && links.length > 0) {
			links.each(function() {
				var link = $(this);
				var linkPath= link.attr("href");
				if(linkPath != null && linkPath.length > 0) {
					link.attr("href", linkPath.replace("((currentPage))", window.location.href));
				}
			});
		}
	}
};