var PopupLink = {
	selector: "a[rel=popup]",
	
	init: function() {
		$(PopupLink.selector).each(function() {
			$(this).unbind("click").click(function() {
				window.open($(this).attr("href"));
				return false;
			});
		});
	}
};