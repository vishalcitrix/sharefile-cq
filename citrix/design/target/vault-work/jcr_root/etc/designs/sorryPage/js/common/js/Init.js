var Common = Common || {
	init: function() {
	    //Links
	    CurrentPageReference.init();
	    ExternalLink.init();
	    Lightbox.init();
	    PopupLink.init();
	}
};

jQuery(function() {
	Common.init();
});