// -----------------------------------
// Lazy Load Initialization
// -----------------------------------

$(document).ready(function() {
	$("img.lazy").unveil(500);
});