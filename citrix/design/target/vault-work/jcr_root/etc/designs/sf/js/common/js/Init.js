var Common = Common || {
    init: function() {
        Accordion.init();
        categoryLinkSelector.init();
        columnControl.init();
        carousel.init();
        dotDotDot.init();
        dropDownLink.init();
        events.init();
        formBuilder.init();
        formPlaceHolder.init();
        hashLinkScroll.init();
        iconIllustration.init();
        sfEvents();
        Link.init();
        menuScrollBar.init();
        menuSlideBar.init();
        planAndPricing.init();
        blockContainerFlip.init();
        searchAutoSuggest.init();
        searchRefinements.init();
        slideBar.init();
        styleSwitcher.init();
        trialForm.init();
        enterpriseForm.init();
        showMore.init();
        supportForm.init();
        floatingFooterScroll.init();
        footerNav.init();
        vdrForm.init();
        floatingFooter.init();
        dataLayerPush.init();
        utilityNav.init();
        if((typeof dnt !== 'undefined' && typeof dnt.dntTrue === 'function') ? !dnt.dntTrue() : true) {
			Common.dntInit();
		}
    },
    lightboxInit: function() {
        styleSwitcher.init();
    },
    dntInit: function() {
        formUrlParam.init();
    }
};

jQuery(function() {
    Common.init();
});