//Sets the value field of locale checkbox to true or false based on selection

var localeCheckbox = localeCheckbox ||  {
    init : function() {
         $('#locale').change(function() {
             $(this).val($(this).prop('checked'));
         });
    },
};