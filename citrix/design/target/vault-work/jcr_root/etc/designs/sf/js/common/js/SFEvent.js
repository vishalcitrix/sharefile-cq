//------------------------------------
// Sending Data to Sharefile
//------------------------------------

sfEvents = function() {
	//do this for publish only
	if((!CQ.WCM && !wcmmode) || (!CQ.WCM && wcmmode && _debug)) {
		sendEventJson();
		$(".sfEvent").click(sendEventJson);
	}
}

function getEventJson(addedContext) {
	var sessionId = getSessionId();
	var source = window.location.href;
	var searchKws = "";
	
	queries = formUrlParam.urlParam != null ? formUrlParam.urlParam.split("&") : "";
	
	for(i = 0, l = queries.length; i < l; i++) {
		k_v = queries[i].split('=');
		if (( (k_v[0] == "q") || (k_v[0] == "p")) && k_v[1] != "" && k_v[1] != "undefined") {
			searchKws = k_v[1];
		}
	}
	if(searchKws == "") {
		//search query not available, try from url query
		if(getURLParameter("kw")) {
			searchKws = getURLParameter("kw");
		}
	}

	if(typeof addedContext != "undefined") {
		source = source + "+" + addedContext;
	}
	
	eventData = {"UserID": getUU(),
	             "SessionID": sessionId,
	             "Page": source,
	             "ReferrerDomain": formUrlParam.referrerHostName,
	             "ReferrerRaw": formUrlParam.referrer,
	             "BrowserType": formUrlParam.BrowserType,
	             "BrowserVersion": formUrlParam.BrowserVersion,
	             "OS": formUrlParam.OSName,
	             "IPAddress": typeof ip_address != "undefined" ?  ip_address : "not available",
	             "useragent": navigator.userAgent,
	             "SearchKeywords": searchKws 
	          	};
	return eventData;
}

function sendEventJson() {
	//To enable SF tracking for a non-page load event, such as a click on an bookmark anchor,
	// wrap it in a DOM element with an "class" property set to "sfEvent".
	// Ex: <div class="sfEvent">Why Sharefile?</div>
	
	var sfEventContext;
	if(typeof $(this).get(0).innerHTML != "undefined") {
		sfEventContext = $(this).get(0).innerHTML.replace(/[^A-z0-9]+/g, "");
	}
	eventJSON = getEventJson(sfEventContext);
	if(_debug) { 
		console.log("eventData: " + JSON.stringify(eventJSON)); 
	}
	nop = function() {};
	succeedFunction = _debug ? function(msg) { console.log("Success: " + JSON.stringify(msg)); } : nop;
	failureFunction = _debug ? function(msg) { console.log("Failure: " + JSON.stringify(msg)); } : nop;
	finishFunction = _debug ? function(msg) { console.log("Finished: " + JSON.stringify(msg)); } : nop;
	$.ajax( {
		type: "POST",
		url: "/bin/citrix/shareFileWebHit",
		data: eventJSON,
		success: succeedFunction,
		failure: failureFunction,
		complete: finishFunction
	});
}