function sendSms(theForm,returnMessageDelivered,returnMessageFail,errorMessageMaxAttempt) {
	var MessageText = theForm.MessageText.value
	var phoneNumber = theForm.mobNumber.value.replace(/[^\d]/g,'');

	if(phoneNumber == '') {
		$('.icon-Warning').removeClass("success icon-Check").html("<span class='bottom-arrow'></span> Phone Number cannot be empty");
		$('.icon-Warning').show();
		$('#mobNumber').addClass('text-error');
	}else {
		$('#submit').attr('disabled','disabled').addClass('disabled');
		$('.icon-Warning').hide();
		$.ajax({
            type: "GET",
            url: "/bin/citrix/smsSender",
            data:'phoneNumber='+phoneNumber+'&MessageText='+MessageText+'&ipAddress='+ip_address,
            success: function(msg) {
                $('#submit').removeAttr('disabled').removeClass('disabled');
                var json = jQuery.parseJSON(msg);
                var statusId = json.status;
                var isUserBlocked = json.isUserBlocked;

                if(isUserBlocked == "true") {
                	$('.icon-Warning').removeClass("success icon-Check").html("<span class='bottom-arrow'></span>"+errorMessageMaxAttempt);
                	$('.icon-Warning').show();
                }else if(statusId) {
                	$('.icon-Warning').addClass("success icon-Check").html("<span class='bottom-arrow'></span>"+returnMessageDelivered);
                	$('.icon-Warning').show();
                	$("#mobNumber").removeClass("text-error");
                }else {
                	$('.icon-Warning').removeClass("success icon-Check").html("<span class='bottom-arrow'></span>"+returnMessageFail);
                	$('.icon-Warning').show();
				}
            },failure: function(data) {
				$('#submit').removeAttr('disabled').removeClass('disabled');
				$('.icon-Warning').removeClass("success icon-Check").html("<span class='bottom-arrow'></span> Please enter a valid phone number");
				$('.icon-Warning').show();
            }, complete: function() {
            	$('#submit').removeAttr('disabled').removeClass('disabled');
            }
        });
	}
}

