// -----------------------------------
// Scrollbar for Slidebar
// -----------------------------------

var menuScrollBar = menuScrollBar || {
    slideBarMenu: "#slidebar-menu",
    slideBarContent: '#slidebar-content',
    
    init: function() {
        // Binding event on navbar content for scroll-bar functionality
		$(menuScrollBar.slideBarContent).height($(window).height()-50);
		$(menuScrollBar.slideBarContent).mCustomScrollbar({
		        theme:"light",
		    mouseWheel:{preventDefault: true},
		    contentTouchScroll: 100,
		    scrollInertia: 100
		});  
    },
    menuScrollResize: function(){
    	$('#slidebar-content').height($(window).height()-50)
    }
};

$(window).bind('resize', menuScrollBar.menuScrollResize);