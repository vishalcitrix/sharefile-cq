var categoryLinkSelector = categoryLinkSelector || {
  init: function() {
        $(window).resize(function() {
            if($(this).width() > 641 ) {
                $(".drop-down-list").show();
             }else{
                 $(".drop-down-list").hide();
             } 
         });
            
        $(".filter-data").click(function(){
            $(this).parents(".categoryLinkSelector ").find(".drop-down-list").toggle();
        });
        $(document).click(function(event) {
            if($(document).width() < 768 && $(".drop-down-list").is(':visible') && ($(event.target).parents('.categoryLinkSelector').length <= 0) ) {
                $(".drop-down-list").hide();
            }
        });
        $(".drop-down-list ul li a").click(function(){
            var objParent = $(this).parents(".categoryLinkSelector");
            objParent.find(".filter-data span.drop-down-header-text").text($(this).text());
            objParent.find(".drop-down-list ul li").removeClass("active");
            $(this).parent().addClass("active");
             if($(window).width() < 641 ) {
                 objParent.find(".drop-down-list").hide();
             }
        });
    }
};