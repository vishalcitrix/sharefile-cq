var supportForm = supportForm || {
    init: function() {
        var emailMessage = "";

        //check fie size - Happens before submit - Max file size is 10MB
        $('#upload').bind('change', function() {
            if(this.files[0].size > 10485760){
                alert ("File oversize. Handle error here instead of alert");
                if($.browser.msie) {
                    $('#upload').replaceWith($('#file_info').clone());
                }else{
                    $("input#upload").val(""); 
                    $('.formField #preupload').addClass('show');
                }
            }
        });

        // File Upload wrapper
        $('#fileButton').click(function(){
            $('.formField #upload').click();
        });

        $('.formField #upload').change(function(){
            $('.formField #preupload').removeClass('hide');
            $('.formField #postupload').removeClass('show');
            $('#fileErrorMessage').hide();
            $('#fileErrorMessage').removeClass('alignment');

            var fileName=$('#upload').val();
            $('.formField #preupload').addClass('hide');
            $('.formField #filepath'). html($('#upload').val());
            $('.formField #postupload').addClass('show');
        });

        //Description drop down for form-radio elements
        $(".link-section").fadeOut(1000);
        $('input[type=radio]').on('change', function() {
            $(".link-section").hide();
            $(this).parent().next(".link-section").fadeIn(1000);
        });

        // Form submission
        $('#support').on('submit', function(e) {
            e.preventDefault();
            
            if (!formBuilder.formError) {            
                if(supportForm.checkCookie()){
                	$(this).parent().prepend('<div class="form-mask"></div>');
                    $('input[type=text],input[type=radio]:checked,input[type=checkbox],textarea', this).each(function() {
                        if($(this).val()){
                            emailMessage += '<span><h1>' + $(this).attr('name') + ':</h1>' + $(this).val() + '</span>';
                        }
                    });

                    //FormData Object is created and send to the Servlet
                    var formData = new FormData(this);  
                    formData.append('emailMessage',emailMessage);

                    supportForm.sendXHRequest(formData);
                }
                else{
                     vdrForm.addLightboxContent($('input#maxAttemptMesssage').val());
                     vdrForm.showLightbox();
                     $("#lightbox-container #lightbox-border").addClass("supportFormWidth");
                }
            }
            else{
            	$('html, body').animate({scrollTop: $("#support").offset().top-230}, 'slow');
            }
        });      
    },  

    //Formdata posted to the servlet and handles the response using ajax
    sendXHRequest: function(formData) {
        $.ajax({
            type: 'POST',
            url: '/bin/citrix/sharefileSendMail',
            processData: false,
            contentType: false,
            data: formData,
            success: function(msg) {

                //display the data returned by the servlet based on the status
                var json = jQuery.parseJSON(msg);
                var status = json.status;

                if(status == "success") {      
                    if($('input#successUrl').val()){
                        window.location = $('input#successUrl').val();
                    } else {
                    	$('.form-mask').remove();
                        $(form).parent().prepend('<div class="confirm-mask">Thank you for your submission.</div>');
                    }                    }
                else {
                     vdrForm.addLightboxContent($('input#errorMessage').val());
                     vdrForm.showLightbox();
                     $("#lightbox-container #lightbox-border").addClass("supportFormWidth");
                }
            }
        });
    },

    //gets the cookie detail - Ip_Path_Key : ipaddress+pageUrl]
    getCookie: function(ip_Path_Key) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + ip_Path_Key + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    },

    //check if a cookie exist for a specific user 
    //sets the cookie for a new user, increments the visit count otherwise
    checkCookie: function() {
        var ip_Path_Key =window.location.href+ip_address;
        var user = this.getCookie(ip_Path_Key);
        if (user != "" && user < 3) {
            user++;       
            document.cookie = ip_Path_Key + "=" + user + "; " + 1;
            return true;
        } 
        else if (user != "" && user > 2){
            return false;
        }
        else {
            user = 1;
            ip_Path_Key =window.location.href+ip_address;
            if (user != "" && user != null) {
                document.cookie = ip_Path_Key + "=" + user + "; " + 1;
            }
            return true;
        }
    }
};
