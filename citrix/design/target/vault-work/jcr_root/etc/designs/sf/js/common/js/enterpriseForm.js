// Custom form submission handlers
var enterpriseForm = enterpriseForm || {
    init: function () {
        $('.ent-lead-form form').on('submit', function(e){
            e.preventDefault();
            var form = this;
            
            if($(this).find('.err').length == 0){
                $(this).parent().prepend('<div class="form-mask"></div>');
 
                $.ajax({
                    url: $(this).attr('action'),
                    type: formUrlParam.postType,
                    dataType: 'json',
                    contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                    data: $(this).serialize()
                    
                }).done(function(data) {
                    data=JSON.stringify(data);
                    var jsondata = $.parseJSON(data);
                    if(jsondata.status == "success"){
                        var userId = getUU();
                        dataLayer.push = [{
                          'transactionId': userId,
                          'transactionTotal': '10.00',
                          'transactionProducts': [{'id': userId,'sku': 'Enterprise','name': 'Lead','price': '10.00','quantity': '1'}]
                        }];
                        
                        // redirect to success or show thank you
                        if($(form).find('input[name=successurl]').length == 1){
                            window.location = $(form).find('input[name=successurl]').val();
                        } else {
                            $(form).parent().prepend('<div class="confirm-mask">Thank you for your submission.</div>');
                        }
                        
                        // cleanup
                        $('.form-mask').remove();
                    }
                    else{
                        $(form).parent().prepend('<div class="confirm-mask">There was an error processing your request.  Please reload the page and try again.</div>');
                        $('.form-mask').remove();
                    }    
               });
            }
        });
    }
};
