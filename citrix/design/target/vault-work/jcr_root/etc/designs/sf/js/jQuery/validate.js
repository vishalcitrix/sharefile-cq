var invalid = [];
var isValid = false;
function enableValidation() {
    jQuery(".next-valid").addClass("next-invalid")
    jQuery(".next-valid").removeClass("next-valid")
/* Validate form onsubmit*/
    jQuery('form.validate').bind('submit', function() {
        console.log('in bind');
        var theForm = this;
    /* Check validity of required fields */
        jQuery(this).find(".required").each( function() {
            placeholder(this,true)
            if( !isRequired(this) && $(this).is(":visible")) {
                placeholder(this,false)
                jQuery(this).addClass("invalid")
                invalid.push(this)
            } else
                jQuery(this).removeClass("invalid")
        });
    /* Check for email validity */
        jQuery(this).find(".email").each( function() {
            if( !isValidEmail(this) ) {
                jQuery(this).addClass("invalid")
                invalid.push(this)
            } else
                jQuery(this).removeClass("invalid")
        });
    /* Check for meeting/webinar/training ID validity */
        jQuery(this).find(".eventId").each( function() {
            if( !isValidEvent(this) ) {
                jQuery(this).addClass("invalid")
                invalid.push(this)
            } else
                jQuery(this).removeClass("invalid")
        });
    /* Check for phone validity */
        jQuery(this).find(".phone").each( function() {
            if( !isValidPhone(this) ) {
                jQuery(this).addClass("invalid")
                invalid.push(this)
            } else
                jQuery(this).removeClass("invalid")
        });
    /* Display error message and states */
        return displayError();
    });
}
/* Check Required fields */
function isRequired(theObj) {
    if( ((theObj.type == "text"|| theObj.type == "select-one") && theObj.value == "") || (theObj.type == "checkbox" && !theObj.checked) ) {
        return false;
    } else return true;
}
/* Validate Email Address */
function isValidEmail(theObj) {
    var emailRegEx = RegExp('^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$');
    return emailRegEx.test(theObj.value)
}
/* Validate Event */
function isValidEvent(theObj) {
    var eventRegEx = RegExp('([0-9]){9,}?');
    return eventRegEx.test(theObj.value);
}
/* Validate phone number. Return error if phone field is required */
function isValidPhone(theObj) {
    var phoneRegEx = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4,5})(([xX]|[eE][xX][tT])\d{1,5}){0,1}$/;
    var phone = theObj.value.replace(/\D/g, '');
    var phoneRequired = $(theObj).hasClass('required');
    var validPhone = false;
    if( phone.indexOf("1") == 0 )
        phone = phone.substr(1);
    if( jQuery('#Country').val() == "USA" || jQuery('#Country').val() == "Canada" ) {
        phoneRegEx = /^(\d{10,15})$/;
        if( phoneRegEx.test(phone) ) {
            validPhone = true;
            if( phone.length >= 11 ) {
                phone = phone.replace(/(\d{3})(\d{3})(\d{4})(\d)/, "+1 ($1) $2-$3 ext.$4");
            }
            else {
                phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "+1 ($1) $2-$3");
            }
            theObj.value = phone;
            validPhone = true;
        }
    }
    else if( phoneRegEx.test(phone) ) {
            validPhone = true;
    }
    if(phoneRequired || validPhone) {
        return validPhone;
    } else {
        var filter = /([\@|\!|\#|\$|\%|\^|\&|\*|\:|\;|\"|\\|\~|\`|\,|\<|\>|\{|\}|\[|\]])/g;
        var tempdata = theObj.value.replace(filter,"")
        if( tempdata.length >20 )
            theObj.value = tempdata.substring(0,19);
        else
            theObj.value = tempdata;
        return true;
    }
}
/* Display Error text and states. Return true if valid */
function displayError(theObj) {
    if(typeof(theObj) != "undefined" ) {
        var objParent = jQuery(theObj).parent();
        objParent.removeClass("error-field");
        jQuery(objParent).find("div.error").remove();
    } else {
        jQuery(".error-field").removeClass("error-field");
        jQuery(".error").remove();
    }
    if(jQuery(".invalid").length < 1) {
        jQuery(".error").hide();
        jQuery(".next-invalid").addClass("next-valid");
        jQuery(".next-invalid").removeClass("next-invalid");
    } else {
        jQuery(".next-valid").addClass("next-invalid")
        jQuery(".next-valid").removeClass("next-valid")
    }
    var flag = invalid.length;
    invalid = jQuery.unique(invalid);
    while( invalid.length>0 ) {
        var theObj = invalid.pop();
				var parentObj = jQuery(theObj).parent();
				if(parentObj.prop("tagName").toLowerCase() == "label" || (  parentObj.attr("class") != undefined && parentObj.attr("class").indexOf("icon-") > -1 ) ){
					parentObj = parentObj.parent();
				}
				parentObj.addClass("error-field");
				parentObj.prepend('<div class="hidden error icon-attention">'+theObj.title+"</div>")
    }
    if(flag>0) {
        jQuery('.error-label').fadeIn();
        isValid = false;
    } else {
        isValid = true
    }
    refillPlaceholder(); 
    return isValid;
}
/* Refill placeholder for IE9 and older */
function refillPlaceholder() {
    if (!Modernizr.input.placeholder) {
        $('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        });
    } 
}
/* Handle placeholder for older IE */
function placeholder(obj, flag) {
    if($.support.placeholder) {
        if(obj.value == "" && !flag && typeof($(obj).attr('placeholder')) != 'undefined') {
            obj.value = $(obj).attr('placeholder');
        }
        if(obj.value == $(obj).attr('placeholder') && flag) {
            obj.value = "";
        }
   }
}

/*remove Hyphens to given field of a form*/
function clearHyphens(form, field) {
	var f = form[field].value;
	    f = htmlEncode(f);//remove user entered js/html
	var nf = f.replace(/\-/g, '');
	form[field].value = nf;
}
/*Using jQuery : the script to escape HTML/JS characters*/
function htmlEncode(value) {
     if (value) {
         return $('<div/>').text(value).html();
     } else {
         return '';
     }
 }
