var trialForm = trialForm || {
	init: function () {
		$('.trialForm #password').on('keyup', function(){
			 var pwdRegex = /^(?=.{8,})(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).*$/,
			 password = $(this).val(),
			 passed = pwdRegex.test( password );

			if( password.length >= 8 && passed){
				$('.password-message').addClass('passed');
			} else {
				$('.password-message').removeClass('passed');
				return false;
			}
		});
		
		$( "#password").focus(function() {
			$('.password-message').slideDown(500, function(){
				$(this).css('overflow', 'visible');
			});
		});

		// IE legacy fallback
	    var elem = document.createElement( "input" );
		if(elem.placeholder == undefined) {
			$('input[type=text]').each(function(){
				var ph = $(this).attr('placeholder');
				if(this.value == "") {
					$(this).addClass('placeholder');
					this.value = ph;
				}
				$(this).focus(function() {
					if( this.value == $(this).attr('placeholder'))
						this.value = "";
					if( $(this).hasClass('placeholder')) {
						$(this).removeClass('placeholder');
					}
				});
				$(this).blur(function() {
					if( this.value == "") {
						this.value = $(this).attr('placeholder');
						$(this).addClass('placeholder');
					}
				});
			});
		}

	}
};