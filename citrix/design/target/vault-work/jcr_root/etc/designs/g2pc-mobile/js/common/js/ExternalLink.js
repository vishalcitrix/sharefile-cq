var ExternalLink = {
	selector: "a[rel=external]",
		
	init: function() {
		$(ExternalLink.selector).click(function() {
            window.open(this.href);
            return false;
        });
	}
};