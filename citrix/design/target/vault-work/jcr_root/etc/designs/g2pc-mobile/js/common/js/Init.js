var Common = Common || {
	init: function() {
	    Accordion.init();
	    ExternalLink.init();
	    MainNavigation.init();
	    FormBuilder.init();
	}
};

jQuery(function() {
	Common.init();
});