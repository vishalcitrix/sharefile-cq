var moPrice = 11.95;
var annPrice = 114.72;
jQuery(document).ready( function() {
	var corpLink = "pricing/corporate";
	if(location.href.indexOf("pricing/") >0 )
		corpLink = "corporate";
	jQuery('select').change(function(){
		if(jQuery(this).val() == "corp") {
			location.assign(corpLink);
		} else {
			var computers = parseInt(jQuery(this).val());
			var moCost = parseFloat(moPrice * computers).toFixed(2);
			var moDecimal = Math.round((moCost-Math.floor(moCost))*100);
			moDecimal > 9 ? "" : moDecimal="0"+moDecimal;
			
			var annCost = parseFloat(annPrice * computers).toFixed(2);
			var annDecimal = Math.round((annCost-Math.floor(annCost))*100);
			annDecimal > 9 ? "" : annDecimal="0"+annDecimal;
			
			var savings = parseFloat((moCost*12)-annCost).toFixed(2);
			jQuery('.monthly .pricing-value').html(Math.floor(moCost));
			jQuery('.monthly .decimals').html(moDecimal);
			
			jQuery('.annual .pricing-value').html(Math.floor(annCost));
			
			if (computers > 8) {
				jQuery('.annual .decimals').html('00');
			}else {
				jQuery('.annual .decimals').html(annDecimal);
			}
			
			jQuery('.annual-saving').html(savings);
		}
	});
});