var Accordion = {
    classNames :  {
        categories : {
            active:'minus',
            inactive:'plus'
        }
    },

    init : function() {
    	$(".accordian-container").each(function() {
    		if($(this).hasClass('active')) {
    			Accordion.hideAll();
    	        Accordion.clickPlus();
    	        Accordion.clickQuestion();
    	        jQuery('#hideAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.hideAll();
    	        }));
    	        jQuery('#showAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.showAll();
    	        }));
    		}
    	});
    },

    hideAll : function() {
        jQuery('.category').hide();
        jQuery('.accordionentry .' + Accordion.classNames.categories.active).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
    },

    showAll : function() {
        jQuery('.category').show();
        jQuery('.accordionentry .' + Accordion.classNames.categories.inactive).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
    },

    clickPlus : function() {
        jQuery('.accordionentry .text.plus').each(function() {
            jQuery(this).unbind('click').bind('click', ( function() {
                var indicator = jQuery(this)[0];
                if(indicator.className.indexOf(Accordion.classNames.categories.active) >= 0)
                    jQuery(this).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
                else
                    jQuery(this).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
                jQuery(this).next('div').slideToggle();
                return false;
            })
            );
        });
    },

    clickQuestion : function() {
        jQuery('.accordionentry .question').each(function() {
            jQuery(this).unbind('click').bind('click',( function() {
                jQuery(this).next('div').slideToggle();
                return false;
            })
            );
        });
    }
};