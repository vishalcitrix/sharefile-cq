// -----------------------------------
// Top Nav + Slidebar Menu
// vishal.gupta@citrix.com
// -----------------------------------

var t1,t2;
var menuSlideBar = menuSlideBar || {
    slideBarContent: "#slidebar-menu #slidebar-content",
    utilityNav: ".topNav",
    tipsContainer: ".tipsContainer",

    init: function() {
        var windowResizeEvent = function () {
            if (ssize == 'small') {
                $('li.tips > a:first-child').click(function (event) {
                    event.preventDefault();
                });

                $("li.tips").click(function(){
                    $(this).find(menuSlideBar.tipsContainer).slideDown();
                });
                if($(menuSlideBar.utilityNav).hasClass('topNavTransition')) {
                    $(menuSlideBar.utilityNav).addClass("navBar");
                    $(".linkSet2").hide();
                }
                $(".icon-rocket-1").css('display','block');

              	// iphone5  browser height fix for sidebar
		            if (mobile && navigator.userAgent.toLowerCase().indexOf('iphone') > 0 && !(navigator.userAgent.match('CriOS'))) {
		                $(".shareList.section.socialList").next('div').next('div').height('height', "50px");
		            }

                menuSlideBar.sectionDisplay();

            } else if (ssize == 'med') {
                $('li.tips > a:first-child').click(function (event) {
                    event.preventDefault();
                });

                $("li.tips").click(function(){
                    $(this).find(menuSlideBar.tipsContainer).slideDown();
                });

                if($(menuSlideBar.utilityNav).hasClass('topNavTransition')) {
                    $(menuSlideBar.utilityNav).removeClass("navBar");
                    $(".linkSet1").show();
                    $(".linkSet2").hide();
                }
                $(".icon-rocket-1").hide();

                menuSlideBar.sectionDisplay();

            } else {
                $("li.tips a").mouseenter(function(e){

                    clearTimeout(t1);
                    t1 = setTimeout(function(){
                        $(e.target).parents("li.tips:first").find(menuSlideBar.tipsContainer).slideDown(150);
                    },250);
                    e.stopPropagation();
                });

                $("li.tips").mouseleave(function(e){

                    clearTimeout(t2);
                    t2 = setTimeout(function(){
                        $(e.target).parents("li.tips:first").find(menuSlideBar.tipsContainer).slideUp(150);
                    },250);
                });

                if($(menuSlideBar.utilityNav).hasClass('topNavTransition')) {
                    $(menuSlideBar.utilityNav).removeClass("navBar");
                    $(".linkSet1").show();
                    $(".linkSet2").hide();
                }
                $(".icon-rocket-1").hide();
            }
        };

        //call window reszie event
        $(document).ready(function(){
            windowResizeEvent();
        });
        $(window).resize(windowResizeEvent);

		var scrollPosition = 0;
        //add navbar and handle active state while scrolling down
        $(window).scroll(function () {

            //add current to nav link based on current section of page
            menuSlideBar.sectionDisplay();

            var y = $(window).scrollTop();
            if (y > 65) {
                if (ssize == 'small') {
                    $(".launcher").removeClass('icon-rocket-1');
                    $(".linkSet2").hide;
                }else if($(menuSlideBar.utilityNav).hasClass('topNavTransition')){
                    $(menuSlideBar.utilityNav).addClass( "navBar" );
                    $(".linkSet2").show();
                    $(".linkSet1").hide();
                }
            } else {
                if (ssize == 'small'){
                    $(".launcher").addClass('icon-rocket-1');
                }else if($(menuSlideBar.utilityNav).hasClass('topNavTransition')){
                    $(menuSlideBar.utilityNav).removeClass("navBar");
                    $(".linkSet2").hide();
                    $(".linkSet1").show();
                }
            }
        });

        // Hide event for tips container
        $(".tipsContainer .icon-up").click(function(e){
            e.stopPropagation();
            $(this).parents(menuSlideBar.tipsContainer).slideUp();
        });
    },

    sectionDisplay: function() {
      if($(menuSlideBar.utilityNav).hasClass('topNavTransition')) {
        var y = $(window).scrollTop();
        var currentTopNavPosition = $(menuSlideBar.utilityNav).height() + y;
        $("section").each(function () {
            var secId = $(this).attr('id');
            var secName = $(this).attr('name');
            if(typeof(secName) == "undefined" || secName == ""){
                secName = "";
            }

            if(secId != ""){
                var secHeight = $(this).height();
                var currPos = $(this).position().top;
                var currEnd = secHeight + currPos;
                var diffT = currPos - (currentTopNavPosition);
                var diffB = currEnd - (currentTopNavPosition);
                if(diffT <= 0 && diffB >= 0){
                    $(".sec-name").text(secName);
                    if($("section").length > 1){
                        $('.linkSet2 a').each(function(){
                            $(this).removeClass('current');
                            var a_href = $(this).attr('href').split('#');
                            if(secId == a_href[1]){
                                $(this).addClass('current');
                            }
                        })
                    }
                }
            }
        });
      }
    }
};
