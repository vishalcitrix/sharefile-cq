var demandbase = (function(){
    var country = false,
        ipAddress = false;
    
    function getDemandBaseJSON(onSuccess){
        jQuery.getJSON(
            'https://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358',
            onSuccess
        );
    }
    
    function setCountryIP(jsonObj) {
        if (Storage) {
            sessionStorage.setItem('country_code',jsonObj.registry_country_code);
            sessionStorage.setItem('ip_address',jsonObj.ip);
        }
        country = jsonObj.registry_country_code;
        ipAddress = jsonObj.ip;
    }
    
    function getCountryIP(callback) {
        function cb(jsonObj){
            setCountryIP(jsonObj);
            callback();
        }
        
        if (Storage) {
            if (sessionStorage.getItem('country_code') === null || sessionStorage.getItem('ip_address') === null){
                getDemandBaseJSON(cb);
            }else {
                country = sessionStorage.getItem('country_code');
                ipAddress = sessionStorage.getItem('ip_address');
                callback();
            }
        }else {
            getDemandBaseJSON(cb);
        }
    }
    
    return {
        updateSegments: function(languageTextObj){
            getCountryIP(function(){
                var c = languageTextObj[country];
                if(c){
                    jQuery.each(c, function(k, confItem){
                        jQuery('#' + confItem.id).html(confItem.value);
                    });
                }
            });
        }
    };
}());