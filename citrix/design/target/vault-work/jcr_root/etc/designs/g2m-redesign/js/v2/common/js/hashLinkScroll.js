var hashLinkScroll = hashLinkScroll || {
    init: function() {
        $("a").click(function(event){
            // Prevent the default action from occurring
            //event.preventDefault(); 
            if(typeof($(this).attr('href')) != "undefined" && ($(this).attr('href') != "#")){
                if($(this).attr('href').indexOf('#') == 0) {
                    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
                    return false;
                }
            }
        });
    }
};