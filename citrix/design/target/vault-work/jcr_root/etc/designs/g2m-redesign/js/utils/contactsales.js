jQuery(document).ready(function() {
    setBuyerPerson();
    enableValidation();
    setDropdownColor();
});

function setBuyerPerson(){
    if(jQuery('input[name="Products"]:checked').attr('alt') != 'undefined' && jQuery('input[name="Products"]:checked').attr('alt') != null){
        jQuery('input[name="Buyer_Persona__c"]').val(jQuery('input[name="Products"]:checked').attr('alt'));
    }
    jQuery('input[name="Products"]').click(function(){
        if(jQuery('input[name="Products"]:checked').length == 1){
            jQuery('input[name="Buyer_Persona__c"]').val(jQuery('input[name="Products"]:checked').attr('alt'));
            jQuery('input[name="Product__c"]').val(jQuery('input[name="Products"]:checked').val());
        }else{
            jQuery('input[name="Buyer_Persona__c"]').val('G2MC - Collaboration');
            jQuery('input[name="Product__c"]').val('GoToMeeting');
        }
    });
}

function contact(theForm,leadGeoFlag) {
    var product = new Array();
    theNodeList = theForm.elements['Products'];
    if(theNodeList != undefined){
        var j=0;
        for( i=0;i<theNodeList.length;i++ )
            if(theNodeList[i].checked) {
                product[j] = theNodeList[i].value;
                j++;
            }
        switch(product.length) {
            case 1: theForm.Product__c.value = product[0];
                setLeadGeo(product[0],leadGeoFlag);
                break;
            case 2: for(i=0; i<2; i++) {
                if(product[i]=="GoToMeeting")
                    product[i] = "G2M";
                if(product[i]=="GoToWebinar")
                    product[i] = "G2W";
                if(product[i]=="GoToTraining")
                    product[i] = "G2T";
                if(product[i]=="HiDef Corporate")
                    product[i] = "G2M";
                }
                if(product[0] == "G2M" && product[1] == "G2M"){
                    setLeadGeo('GoToMeeting',leadGeoFlag);
                    theForm.Product__c.value = "GoToMeeting";
                } else {
                    setLeadGeo('Multiple',leadGeoFlag);
                    theForm.Product__c.value = "Both "+product[0]+" & "+product[1];
                }
                break;
            case 3: for(i=0; i<3; i++) {
                if(product[i]=="GoToMeeting")
                    product[i] = "G2M";
                if(product[i]=="GoToWebinar")
                    product[i] = "G2W";
                if(product[i]=="GoToTraining")
                    product[i] = "G2T";
                if(product[i]=="HiDef Corporate")
                    product[i] = "G2M";
                }
                leadGeo('Multiple',leadGeoFlag);
                if(product[0] == "G2M" && product[2] == "G2M"){
                    theForm.Product__c.value = "Both "+product[0]+" & "+product[1];
                } else {
                    theForm.Product__c.value = product[0]+" & "+product[1]+" & "+product[2];
                }
                break;
            case 5: theForm.Product__c.value = product[4];
                setLeadGeo('Multiple',leadGeoFlag);
                break;
            default: theForm.Product__c.value = 'GoToMeeting';
                setLeadGeo('GoToMeeting',leadGeoFlag);
        }
    }
    
    theForm.FirstName.value = htmlEncode(theForm.FirstName.value);//remove user entered  JS /Html, Since this field does not have data validation.
    theForm.LastName.value = htmlEncode(theForm.LastName.value);//remove user entered  JS /Html, Since this field does not have data validation.
    theForm.form_comments.value = htmlEncode(theForm.form_comments.value);//remove user entered  JS /Html, Since this field does not have data validation.
    
    if(typeof(Storage) !== "undefined"){
         sessionStorage.setItem("firstName",theForm.FirstName.value);
    }
}

function setLeadGeo (prod,leadGeoFlag) {
    if(leadGeoFlag){
        countryList = theForm.Country;
        leadGeo = countryList.options[countryList.selectedIndex].title;
        var  idList = { "NA" : [ {'GoToMeeting': '701000000005L4F','GoToWebinar':'701000000005L4H','GoToTraining':'701000000005L4P','HiDef Corporate':'701000000005BDf','Multiple':'701000000005LaQ'}],
            "EMEA" : [ {'GoToMeeting': '701000000005L4K','GoToWebinar':'701000000005L4Q','GoToTraining':'701000000005L4L','HiDef Corporate':'701000000005BDf','Multiple':'701000000005LaV'}],
            "APAC" : [ {'GoToMeeting': '701000000005L4G','GoToWebinar':'701000000005L4U','GoToTraining':'701000000005L4M','HiDef Corporate':'701000000005BDf','Multiple':'701000000005Laa'}] }
        if( theForm.sfdc_campaign_id.value == defaultID && leadGeo != "" ) {
            switch (leadGeo) {
                case "NA" :     defaultID = theForm.sfdc_campaign_id.value = idList.NA[0][prod];
                    break;
                case "EMEA" :   defaultID = theForm.sfdc_campaign_id.value = idList.EMEA[0][prod];
                    break;
                case "APAC" :   defaultID = theForm.sfdc_campaign_id.value = idList.APAC[0][prod];
                    break;
                default: break;
            }
        }
    }
}

function checkAll(theObject) {
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length;i++ )
        theNodeList[i].checked=theObject.checked
}

function unCheck(theObject) {
    var flag=true
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length-1;i++ )
        if(!theNodeList[i].checked) {
            flag=false;
            break;
        }
    theNodeList[theNodeList.length-1].checked = flag;
}

function getCookieSFID(mktCookie,website) {
    var sfid = '';
    var sfStart = -1;

    if(mktCookie.indexOf('LST_cmp') > -1) {
        sfStart = mktCookie.indexOf('LST_cmp')+10;
    } else if(mktCookie.indexOf('FIS_cmp') > -1) {
        sfStart = mktCookie.indexOf('FIS_cmp')+10;
    }
    if (sfStart > -1) {
        var temp = mktCookie.substring(sfStart);
        if(temp.indexOf('%26')>-1) {
            var sfEnd = temp.indexOf('%26');
            temp = temp.substring(0,sfEnd);
        }
        if(temp.indexOf('sf-') == 0) {
            sfid = temp.substring(3)
        }
    }

    return sfid;
}

function getmktCookie(name) {
    var mktCookie = '';
    if(document.cookie.indexOf(name) > -1){
        var start = document.cookie.indexOf(name)+name.length+1;
        mktCookie = document.cookie.substring(start);
        if(mktCookie.indexOf(';') >-1){
            var end = mktCookie.indexOf(';')
            mktCookie = mktCookie.substring(0,end);
        }
    }
    return mktCookie;
}

function getURLParam(name) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == name)
      {
          return sParameterName[1];
      }
    }
}

function setDropdownColor() {
    $(".contact-sales select").each(function () {
        $(this).find('option').css('color', '#424445');
    });
    
    $('.contact-sales select').on('change', function (e) {
        if(this.value != "") {
            $(this).css('color', '#424445');
        }else {
            $(this).css('color', '#babcbe');
        }
    });
}
