var ExternalLink = {
	selector: "a[rel=external]",
	init: function() {
		$(ExternalLink.selector).attr("target","_blank");
	}
};