var Link = Link || {
	init: function() {
		if(typeof CurrentPageReference != 'undefined') {
			CurrentPageReference.init();
		}
		if(typeof ExternalLink != 'undefined') {
			ExternalLink.init();
		}
		if(typeof Lightbox != 'undefined') {
			Lightbox.init();
		}
		if(typeof PopupLink != 'undefined') {
			PopupLink.init();
		}
	}
};