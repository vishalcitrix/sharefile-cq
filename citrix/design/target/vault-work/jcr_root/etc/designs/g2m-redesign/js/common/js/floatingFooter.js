// -----------------------------------
// FLoating Footer
// vishal.gupta@citrix.com
// -----------------------------------

jQuery(document).ready(function() {
    var t;
    var isMobileView = $('.floatingFooterContent').attr("data-mobile");
    if(isMobileView =='false' && ssize == 'small') {
        $(".floatingFooterContent").hide();
    }else {
        handleFloatingFooter();
        $(window).scroll(function () {
            clearTimeout(t);
            t = setTimeout(handleFloatingFooter, 100);
        });
    }
});

function handleFloatingFooter(){
    var y = $(window).scrollTop();
    var floatingFooter = $(".floatingFooterContent");
    var floatingHeight = floatingFooter.height();
    var floatingOffset = y + $(window).height();

    if($("#static-footer").length > 0)
        var footerOffset = $("#static-footer").offset().top;
    else
        var footerOffset = $(".footer").offset().top;

    if((ssize == 'small')|| ( ssize == 'med' && y > 65 ) || ( ssize == 'large' ) ) {
        if(floatingOffset >= footerOffset){
            floatingFooter.slideUp();
        } else {
            floatingFooter.slideDown();
        }
    } else {
        floatingFooter.slideUp();
    }
}