var secondaryNavSticky = {
    element: "#action-header",
    topNav: ".topNav",
    columnLink: ".stickyLink",
    mobileText: ".mobile-heading",
    iconText: ".icon-description",
    iconDown: "icon-down-open",
    iconRight: "icon-right-open",
    prevScroll: 0,
    init: function() {
        if($(secondaryNavSticky.element).hasClass('active')) {
            if ($(secondaryNavSticky.element).offset()) {
                var ePos = $(secondaryNavSticky.element).offset().top;
                    ePos = parseInt(ePos) -parseInt($(secondaryNavSticky.topNav).css('height'));
                var marginTop = $(".stickyPar").css('margin-top'); 
                var stickyLinks = $(secondaryNavSticky.columnLink);
                var linkArray = []; // create the empty aArray
                for (var i=0; i < stickyLinks.length; i++) {
                    var stickyLink = stickyLinks[i];
                    var ahref = $(stickyLink).attr('href');
                    linkArray.push("#"+ahref);
                }
                $(window).scroll(function() { //scroll functionality for fixing the container and selecting the proper icon depending on page position
                    var currScroll = $(window).scrollTop();
                    var position = ePos-currScroll;
                    if (position < 0) {
                        $(secondaryNavSticky.element).addClass('fixed');
                        if(ssize!='large' && ssize!='med'){
                            $('.stickyPar').css('margin-top',marginTop);
                        }

                        if(currScroll <= secondaryNavSticky.prevScroll){    // scrolling up
                            if(ssize=='large'|| ssize=='med'){
                                setTimeout(function(){
                                    secondaryNavSticky.adjustStyleOnScrollUp();
                                },600);
                            }
                        }else{// scrolling down
                            if(ssize=='large'|| ssize=='med'){
                                secondaryNavSticky.adjustStyleOnScrollDown();
                            }
                            $(secondaryNavSticky.element).css({"top":"0px"});
                            $('body .mainContent').first().css({"margin-top":"0px"});
                        }
                    } else {  
                        $(secondaryNavSticky.element).removeClass('fixed');
                        $(secondaryNavSticky.element).css({"top":"0px"});
                        $(".stickyPar").css('margin-top', marginTop);
                    }
                    var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
                    for (var i=0; i < linkArray.length; i++) {
                        var linkID = linkArray[i];
                        var divPos = parseInt($(linkID).offset().top)-parseInt($(secondaryNavSticky.element).outerHeight())-parseInt($(secondaryNavSticky.topNav).css('height')); // get the offset of the div from the top of page
                        var divHeight = $(linkID).parent().height(); // get the height of the section
                        if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                            linkID = linkID.substring(1);
                            var index = $("a[href='" + linkID + "']").attr("index");
                            $("a.stickyLink").each(function() {
                                if($(this).attr("index") == index){
                                    $(this).parent().addClass("selected");
                                }else {
                                    $(this).parent().removeClass("selected");
                                }
                            });
                        } else {
                            $("a[href='" + linkID + "']").removeClass("selected");
                        }
                    }
                    secondaryNavSticky.prevScroll = currScroll;
                });
            }
        }
        if(ssize=='large') {
            $(secondaryNavSticky.columnLink).hover(function() {
                $(this).find(secondaryNavSticky.iconText).addClass("hover");
                }, function() {
                $(this).find(secondaryNavSticky.iconText).removeClass("hover");
                }
            );
        }
        $(secondaryNavSticky.columnLink).click(function(event){
            event.preventDefault();
            var target = "#"+$(this).attr("href");
            var scrollPosBefore = $(window).scrollTop();
            if(ssize=='large'|| ssize=='med'){
                target = parseInt($(target).closest('.container').offset().top)-parseInt($(secondaryNavSticky.topNav).css('height'));
            }else {
                $(secondaryNavSticky.columnLink).toggleClass('show');
                $(secondaryNavSticky.mobileText).removeClass(secondaryNavSticky.iconDown).addClass(secondaryNavSticky.iconRight);
                target = parseInt($(target).closest('.container').offset().top)-parseInt($(secondaryNavSticky.element).height());
            }
            $('html:not(:animated),body:not(:animated)').animate({ scrollTop: target},400, function(){
                setTimeout(function(){
                    var scrollPosAfter = $(window).scrollTop();
                    if(ssize=='large'|| ssize=='med'){
                        if(scrollPosAfter > scrollPosBefore){   // scrolled down
                            secondaryNavSticky.adjustStyleOnScrollDown();
                        }else{                                  // scrolled up
                            secondaryNavSticky.adjustStyleOnScrollUp();
                        }
                    }
                },600);
            });
        });
        $(secondaryNavSticky.mobileText).click( function() { // mobile functionality
            $(this).toggleClass(secondaryNavSticky.iconRight);
            $(this).toggleClass(secondaryNavSticky.iconDown);
            $(secondaryNavSticky.columnLink).toggleClass('show');
        });
    },

    adjustStyleOnScrollUp: function(){
        var marginTop= parseInt($('.action-nav.active.fixed').css('height')) +parseInt($(secondaryNavSticky.topNav).css('height'));
        $(secondaryNavSticky.element).css({"top": $(secondaryNavSticky.topNav).height()+"px"});
        $('.stickyPar').css('margin-top',marginTop);
    },

    adjustStyleOnScrollDown: function(){
        var marginTop= parseInt($('.action-nav.active.fixed').css('height'));
        $(secondaryNavSticky.element).css({"top":"0px"});
        $('.stickyPar').css('margin-top',marginTop);
    }
};