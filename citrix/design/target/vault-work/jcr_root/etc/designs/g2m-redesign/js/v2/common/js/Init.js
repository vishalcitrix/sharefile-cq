var Common = Common || {
	init: function() {
		dotDotDot.init();
		Accordion.init();
		backgroundVideo.init();
		countrySelector.init();
		carousel.init();
		CurrentPageReference.init();
		dropDownLink.init();
		hashLinkScroll.init();
		Link.init();
		secondaryNavSticky.init();
		searchAutoSuggest.init();
		searchRefinements.init();
		socialNav.init();
		PrimaryCarousel.init();
		tryForm.init();
		planAndPricing.init();
		blockContainerFlip.init();
		footerNav.init();
		toaster.init();
		if(!useUnityNav){
			menuSlideBar.init();
			slideBar.init();
		}else{
			utilityNav.init();
		}
	},

	lightboxInit: function(){
		pricingChart.init();
		ToolTip.init();
		blockContainerFlip.init();
	}
};

jQuery(function() {
	setTimeout(Common.init, 250);
	$('#ieSupportMsg .container').wrap('<a href="http://support.citrixonline.com/en_US/meeting/knowledge_articles/000220680"></a>');
});