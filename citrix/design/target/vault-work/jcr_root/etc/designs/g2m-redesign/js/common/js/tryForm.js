var tryForm = tryForm || {
    init: function () {
        var tryFormTrigger = function () {
            $("div.trial-box").each(function () {
                var id = $(this).find('input.prefill.show-for-medium-only').attr('id');
                if (ssize == 'med') {
                    $(this).find('input.prefill.show-for-medium-only').attr('name', id);
                    $(this).find('input.prefill.show-for-large-up').removeAttr("name");
                    $(this).find('input.prefill.show-for-small-only').removeAttr("name");

                } else if (ssize == 'small') {
                    $(this).find('input.prefill.show-for-small-only').attr('name', id);
                    $(this).find('input.prefill.show-for-large-up').removeAttr("name");
                    $(this).find('input.prefill.show-for-medium-only').removeAttr("name");
                } else {
                    $(this).find('input.prefill.show-for-large-up').attr('name', id);
                    $(this).find('input.prefill.show-for-small-only').removeAttr("name");
                    $(this).find('input.prefill.show-for-medium-only').removeAttr("name");
                }
            });
        };
        tryFormTrigger();
        $(window).resize(tryFormTrigger);
    }
};