// -----------------------------------
// Country Selector
// -----------------------------------

var countrySelector = countrySelector || {
    init: function() {	
    	$(".footer-country-selector").click(function(){
			var leftPos = $(this).width() - 35;
			jQuery(".footer .arrow").css("left",leftPos);
			jQuery(".locale-selector").toggle();
			return false;
		});
    	
		$(".locale-selector .icon-close").click(function(){
			$(".locale-selector").hide();
		});
		
		$(document).ready(function(){
			$('.footer select').change(function() {
	            var $opt = $(this).find("option:selected");
	            var $span = $('<span>').addClass('tempOpt').text($opt.text());
	            $span.css({
	                'font-family': $opt.css('font-family'),
	                'font-size': $opt.css('font-size')
	            });
	            
	            $('body').append($span);
	            
	            // The 30px is for select open icon - it may vary a little per-browser
	            $(this).width($span.width() + 30);
	            $span.remove();
	        }).change();
		});
    }
};
