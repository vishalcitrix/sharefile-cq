var Common = Common || {
	init: function() {
		dotDotDot.init();
		Accordion.init();
		backgroundVideo.init();
		countrySelector.init();
		carousel.init();
		CurrentPageReference.init();
		dropDownLink.init();
		formPlaceHolder.init();
		hashLinkScroll.init();
		Link.init();
		parallax.init();
		socialNav.init();
		PrimaryCarousel.init();
		SecondaryCarousel.init();
		searchAutoSuggest.init();
		searchRefinements.init();
		tripleStack.init();
		Tab.init();
		videospotlight.init();
		tryForm.init();
		planAndPricing.init();
		ToolTip.init();
		footerNav.init();
		toaster.init();
		if(!useUnityNav){
			menuSlideBar.init();
			slideBar.init();
		}else{
			utilityNav.init();
		}
	},

	lightboxInit: function(){
		pricingChart.init();
		styleSwitcher.init();
		ToolTip.init();
	}
};

jQuery(function() {
	setTimeout(Common.init, 250);
	$('#ieSupportMsg .container').wrap('<a href="http://support.citrixonline.com/en_US/meeting/knowledge_articles/000220680"></a>');
});
