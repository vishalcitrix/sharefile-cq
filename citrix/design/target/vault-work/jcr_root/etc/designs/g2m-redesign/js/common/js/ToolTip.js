var ToolTip = ToolTip || {
	init: function() {
		var docObj = (jQuery.browser.webkit) ? 'body' : 'html';
		$(".show_tooltip").hover(function(e){
			var posY = window.scrollY;

			if($(this).parents("#lightbox-container").length){
				posY = jQuery("#lightbox-container").scrollTop();
			}
			$(this).parents("li:first").find("div[id^=tip]").css({
				"left": e.pageX + 10,
				"top": e.pageY + 10 + (posY - $(docObj).scrollTop())
			}).show();
		},function(){
			$(this).parents("li:first").find("div[id^=tip]").hide();
		});
	}
};