var ssize;
var mobile  = detectDevice();

var calcScreenSize = function () {
	if($(window).width() <= '1024' && $(window).width() > '640') {
	   ssize = 'med';
	}else if($(window).width() <= '640') {
	    ssize = 'small';
	}else {
	    ssize = 'large';
	}
};

function detectDevice() {
    var mobRegEx = RegExp(".*(android|iphone|ipad|ipod|iemobile|blackberry).*");
    return mobRegEx.test(navigator.userAgent.toLowerCase());
}

$(document).ready(calcScreenSize);
$(window).resize(calcScreenSize);
$(window).resize(detectDevice);