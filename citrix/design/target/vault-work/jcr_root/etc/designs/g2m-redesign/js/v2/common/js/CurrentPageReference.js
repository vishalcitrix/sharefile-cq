var CurrentPageReference = CurrentPageReference || {

    init : function() {
        var facebookLinks = $('.fb .icon-facebook');
        var twitterLinks = $('.tw .icon-monitor');
        var googlePlusLinks = $('.gplus .icon-gplus');
        var mailLinks = $('.mail .icon-mail');
        
        CurrentPageReference.replaceCurrentPage(facebookLinks);
        CurrentPageReference.replaceCurrentPage(twitterLinks);
        CurrentPageReference.replaceCurrentPage(googlePlusLinks);
        CurrentPageReference.replaceCurrentPage(mailLinks);
    },

	replaceCurrentPage: function(links) {
		if(links != null && links.length > 0) {
			links.each(function() {
				var link = $(this);
				var linkPath= link.attr("href");
				if(linkPath != null && linkPath.length > 0) {
					link.attr("href", linkPath.replace("((currentPage))", window.location.href));
				}
			});
		}
	}
};