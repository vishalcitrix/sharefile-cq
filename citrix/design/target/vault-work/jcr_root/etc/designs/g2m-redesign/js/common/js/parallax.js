// -----------------------------------
// Set Parallax Properties 
// -----------------------------------

var parallax = parallax || {
    
    init: function() {
    	var parallaxProps = function () {
    		$(".parallax").each(function () {
            	$(this).parallax("50%", 0.5);
    		});   
        };
        
        $(document).ready(parallaxProps);
    }
};