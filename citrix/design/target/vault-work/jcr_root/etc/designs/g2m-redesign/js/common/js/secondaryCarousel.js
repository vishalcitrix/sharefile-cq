// -----------------------------------
// Secondary Carousel
// -----------------------------------

var SecondaryCarousel = SecondaryCarousel || {
	secondaryCarousel: ".secondary-carousel",
	secondaryCarouselSelector: ".secondary-carousel .selector",
	secondaryCarouselCurrent: ".secondary-carousel .current",
	secondaryTextCarousel: ".secondary-carousel #textCarousel",
	secondaryCarouselFeedLink: ".secondary-carousel .feed-link",
	
    init: function() {
	    var txCarousel = $(SecondaryCarousel.secondaryTextCarousel);
	    var playStatus = true;
	    
	    //initiate carousel
	      txCarousel.owlCarousel({
	        singleItem : true,
	        slideSpeed : 1000,
	        pagination:false,
	        responsiveRefreshRate : 200,
	        autoPlay: true,
	        stopOnHover: true,
	        afterMove: afterMove,
	        responsiveBaseWidth: window
	    });

	    $('.secondary-carousel p').dotdotdot({watch: 'window'});
	    $('.secondary-carousel h3').dotdotdot({watch: 'window'});
	    
        //console.log('initial playstatus = ' + playStatus);
        $(SecondaryCarousel.secondaryCarouselSelector).hover(function() {
            $(SecondaryCarousel.secondaryCarouselCurrent).removeClass('current');
            $(this).addClass('current');    
            var clickPos = $(this).index();//index of clicked item    
            var carouselPos = txCarousel.data("owlCarousel").owl.visibleItems;//index of current carousel item
            txCarousel.trigger("owl.goTo", clickPos);
            txCarousel.data("owlCarousel").stop()
        }, function() {txCarousel.data("owlCarousel").play()});

        //stop and start carousel on hover 
        $(SecondaryCarousel.secondaryCarousel).hover(function() {
            txCarousel.data("owlCarousel").stop();},
            function() {txCarousel.data("owlCarousel").play();
        });
        
        $(SecondaryCarousel.secondaryCarouselSelector).click(function() {
            if(playStatus === true) {
                //console.log('just went through a click wwith true');
                $(SecondaryCarousel.secondaryCarouselCurrent).removeClass('current');
                $(this).addClass('current');    
                var clickPos = $(this).index();//index of clicked item    
                var carouselPos = txCarousel.data("owlCarousel").owl.visibleItems;//index of current carousel item
                txCarousel.trigger("owl.goTo", clickPos);
                txCarousel.data("owlCarousel").stop()
                playStatus = false;
                $(SecondaryCarousel.secondaryCarouselSelector).unbind('mouseenter mouseleave');
                $(SecondaryCarousel.secondaryCarousel).unbind('mouseenter mouseleave');
                //console.log('playStatus should be false = ' + playStatus);
            } else {
                //console.log('just went through a click wwith false'); 
                $(SecondaryCarousel.secondaryCarouselCurrent).removeClass('current');
                $(this).addClass('current');    
                var clickPos = $(this).index();//index of clicked item    
                var carouselPos = txCarousel.data("owlCarousel").owl.visibleItems;//index of current carousel item
                txCarousel.trigger("owl.goTo", clickPos);
                $(SecondaryCarousel.secondaryCarouselSelector).hover(function() {        
                    $(SecondaryCarousel.secondaryCarouselCurrent).removeClass('current');
                    $(this).addClass('current');    
                    var clickPos = $(this).index();//index of clicked item    
                    var carouselPos = txCarousel.data("owlCarousel").owl.visibleItems;//index of current carousel item
                    txCarousel.trigger("owl.goTo", clickPos);
                    txCarousel.data("owlCarousel").stop()
                }, function() {txCarousel.data("owlCarousel").play()});
        
                //stop and start carousel on hover 
                $(SecondaryCarousel.secondaryCarousel).hover(function() {
                    txCarousel.data("owlCarousel").stop();},
                    function() {txCarousel.data("owlCarousel").play();
                });
                playStatus = true;      
            }
            return playStatus;
        });

	    //synchronize thumbnails and carousel slide
	    function afterMove() {
	        var selector = $(SecondaryCarousel.secondaryCarouselSelector);
	        var clickPos = $(SecondaryCarousel.secondaryCarouselCurrent).index();
	        var carouselPos = txCarousel.data("owlCarousel").owl.visibleItems;
	        if(carouselPos != clickPos) {
	            $(SecondaryCarousel.secondaryCarouselCurrent).removeClass('current');
	            selector.eq(carouselPos).addClass('current');
	        }
	    }
    }
};