/********************************
*    Primary Carousel js
*
* Part of this code interacts with the tripleStack component
* to ensure equal height is distributed to all its sections
*
*********************************/
var tm1, tm2;
var PrimaryCarousel = PrimaryCarousel || {
    init: function() {
        $(".primary-carousel").each(function() {
            var primaryCarousel = $("#primaryCarousel").data('owlCarousel');
            
            $(this).owlCarousel({
                singleItem : true,
                //slideSpeed : 1000,
                //pagination:false,
                responsiveRefreshRate : 200,
                autoPlay: false,
                //autoHeight: true,
                rewindNav: true,
                afterUpdate: setHeight,
                pagination: true,
                navigation: true,
                navigationText: false,
                responsiveBaseWidth: window
            });  
        });    
        
        if($(".primary-carousel")[0]) {
            setHeight();
        }

        var textResize = function (){
            $('.primary-carousel .text.rte').dotdotdot({
                ellipsis: '...',
                watch: 'window'
            });      
        }
        
        $(document).ready(function(){
            clearTimeout(tm1);
            tm1 = setTimeout(function(){textResize()},2000);          
        });
        
        $(window).resize(function(){
            clearTimeout(tm2);
            tm2 = setTimeout(function(){textResize()},500);
        });  
        $('.primaryCarousel img, .primaryCarousel .owl-item').hover(
            function() {
                $('.primaryCarousel .owl-buttons').css('display', 'block');
                if($(window).width() > 1025) {
                    $('.primaryCarousel .owl-item-info h5').css('color', '#fc6a06');
                }
                $('.primaryCarousel .link-sub-container').hover(
                    function() {
                        $('.primaryCarousel .owl-item-info h5').css('color', '#434b58');
                    },
                    function() {
                        $('.primaryCarousel .link-sub-container').unbind('mouseenter mouseleave');
                    }
                );
            }, 
            function() {
                    $('.primaryCarousel .owl-buttons').css('display', 'none');
                    $('.primaryCarousel .owl-item-info h5').css('color', '#434b58');
            }
        );
        $('.primaryCarousel .owl-item-info, .primaryCarousel img').click(function() {
            if($(window).width() < 1025) {
                $(this).closest('.carouselItem').find('h5').css('color', '#fc6a06');
            }
        });
        $('.primaryCarousel .owl-buttons').hover(
            function() {
                $('.primaryCarousel .owl-buttons').css('display', 'block');
            },
            function() {
                $('.primaryCarousel .owl-buttons').css('display', 'block');
            }
        );       
    }
};  

//setHeight maintains the triple decker component at equal height for 
//each item depending on the total height of the responsive carousel image
function setHeight() {        
    //set default variables. get heights and process
    var singleStackHeight = $('.stacker').outerHeight();
    var stackAdjustment = 0;
    var division = 0;       
    var carouselHeight = $('.primary-carousel').height();
    division = Math.round((carouselHeight/3));
    singleStackHeight = division;
    var fullStackHeight = (division * 3);
    var carouselHeight = $('.owl-item').height();  
    
    //determine remainder number, set all stacks to same 
    //height, and add or subtract from last stack
    if(fullStackHeight > carouselHeight) {
        stackAdjustment = (fullStackHeight - carouselHeight);        
    } else if(fullStackHeight < carouselHeight) {
        stackAdjustment = (fullStackHeight - carouselHeight);        
    } else {
        stackAdjustment = 0;
    }
    $('.stacker').height(singleStackHeight);
    $('.singleStack-3').height(singleStackHeight - stackAdjustment);      
}    