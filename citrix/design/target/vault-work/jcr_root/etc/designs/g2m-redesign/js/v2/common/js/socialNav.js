// -----------------------------------------------------------
//  Floating Social Nav
//  - Scroll event to show up-down buttons on scrolling from top
//  - Hover event to show social icons
//  - 2 click events to scroll page onclick of up-down arrows
// -----------------------------------------------------------

var socialNav = socialNav || {
    currSegment:'', 
    segFromTop:[], 
    segments:[],
    docObj: (jQuery.browser.webkit) ? 'body' : 'html',
    scrollByArrow:false,

    captureCords: function() {
        if( jQuery("section").length > 1 ) {
            jQuery("section").each(function(i){
                if(this.id != '') {
                    socialNav.segFromTop[this.id] = parseInt(jQuery('#'+this.id).offset().top);
                    socialNav.segments[i] = this.id;
                }    
            });
            socialNav.currSegment =''+socialNav.segments[0];
        }
    },
    
    resetBg: function() {
    	$("#shareOptions").css({"background":"transparent"});
    },
    
    hideSocialIcons: function() {
        $("#shareOpt").stop(true).animate({left:'-256px'});
        $("#shareOptions").css({"background":"transparent"});
		$(".shareOpt .icons-box").css({'display':'none'});
    },
    
    adjustSocialNavColor: function() {
        var useAltColor = $(".left-nav").attr("data-social-nav-user-alt-color");
        var altColorSection = $(".left-nav").attr("data-social-nav-alt-color-section");
        var altColor = $(".left-nav").attr("data-social-nav-alt-color");
    
        if (useAltColor == "true") {
            console.log("enabled");
            var bottom = $(altColorSection).position().top+ $(altColorSection).height();
            if ( $(".left-nav").position().top > $(altColorSection).position().top && $(".left-nav").position().top < bottom ) {
                if ( altColor != "" )
                    $(".left-nav li a, .left-nav li span").css("color", altColor);
                else
                    $(".left-nav").addClass("alternative-color");
            }
            else {
                if ( altColor != "" )
                    $(".left-nav li a, .left-nav li span").css("color", "");
                else
                    $(".left-nav").removeClass("alternative-color");
            }
        }
    },
	
    init: function() {
        $(".navbuttons span").hide();
        t = setTimeout("socialNav.captureCords()", 1000);	
        if(ssize != 'large'){
            $("a#share").click(function(e){
				e.preventDefault();
                e.stopPropagation();
                $(".shareOpt, .icons-box").css({'width':'0px'});
                $(this).parents("#shareOptions").css({"background":"#000"});
				$(".shareOpt .icons-box").css({'display':'block'});
                $(".shareOpt, .icons-box").css({'width':'256px'});
                $("#shareOpt").stop(true).animate({left:'256px'});
                setTimeout(socialNav.hideSocialIcons,3000);
            });
        }else{
            $("#shareOptions").hover(function(){
                $("#shareOptions").css({"background":"#000"});
				$(".shareOpt .icons-box").css({'display':'block'});
                $(".shareOpt, .icons-box").css({'width':'256px'});
                $("#shareOpt").stop(true).animate({left:'256px'});
            },function(){
                $("#shareOpt").stop(true).animate({left:'-256px'});
                setTimeout(socialNav.resetBg,200);
                $(".shareOpt, .icons-box").css({'width':'0px'});
				$(".shareOpt .icons-box").css({'display':'none'});
            });
        }
		
        $(window).scroll(function () {
            //add current to nav link based on current section of page
            if($(window).scrollTop() < 1) socialNav.currSegment = socialNav.segments[0];
            var currentTopNavPosition = $(window).scrollTop();
            
            if($("section").length > 2 ) {
                $("section").each(function () {
					if(this.id != '') {
						var secId = $(this).attr('id');
						secId = (socialNav.scrollByArrow) ? socialNav.currSegment : secId;
						var secHeight = $('#'+secId).height();
						var currPos = $('#'+secId).position().top;
						var currEnd = secHeight + currPos;
						var diffT = currPos - (currentTopNavPosition);
						var diffB = currEnd - (currentTopNavPosition);
						if(diffT <= 0 && diffB >= 0){
							$(socialNav.segments).each(function(i){
								if(secId != "" && secId == socialNav.segments[i]) {
									socialNav.currSegment = socialNav.segments[i];
									if(i == socialNav.segments.length-1){
										$('span#followDown').hide();
									}else if(i>0){
										$(".navbuttons span").show();
									}else{
										$(".navbuttons span").hide();
									}
								}
								if($(window).scrollTop() + $(window).height() == $(document).height()) $('span#followDown').hide();
								if($(window).scrollTop() < 1) $(".navbuttons span").hide();
							});
						} 
					}
                });
            }
            
			// Handling social floating navigation for other pages without section element
			var hideTopArrow = function() {
				if(currentTopNavPosition >= 350 && socialNav.showFloatingNav != true){
					$('span#followUp').show();
					socialNav.showFloatingNav = true;
				}else if($(window).scrollTop() < 1 && socialNav.showFloatingNav != false){
					$('span#followUp').hide();
					socialNav.showFloatingNav = false;
				}
			};

            if($("section").length <= 2) {
                hideTopArrow();
            }else{
				var sectionWithId = false;
				jQuery("section").each(function(i) {
					if(this.id != '') {
						sectionWithId = true;
						return false;
					}
				});
				if(!sectionWithId) hideTopArrow();
			}
			
            if($("section").length < 1 && currentTopNavPosition >= 200 && socialNav.showFloatingNav != true){
                $('span#followUp').show();
                socialNav.showFloatingNav = true;
            }else if(currentTopNavPosition < 100){
                currentTopNavPosition = false;
            }
            socialNav.adjustSocialNavColor();
        });
        $(document).ready(function() {
            socialNav.adjustSocialNavColor();
        });
        
        
        // Down button click event
        $('.top-layer-content .icon-learn-more').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            socialNav.scrollByArrow = true;
            $(socialNav.docObj).animate({scrollTop: [socialNav.segFromTop[socialNav.segments[socialNav.segments.indexOf(socialNav.currSegment) + 1]]+20, 'swing']}, 1000);
            socialNav.currSegment = socialNav.segments[socialNav.segments.indexOf(socialNav.currSegment) + 1];
            $(".navbuttons span").show();
        });
        

        // Nav button click event
        $('.navbuttons span').click(function(e){
            socialNav.scrollByArrow = true;
            if(this.id == 'followDown'){
                $('html,body').animate({scrollTop: $('#'+socialNav.segments[socialNav.segments.indexOf(socialNav.currSegment) + 1]).offset().top}, 500);
                socialNav.currSegment = socialNav.segments[socialNav.segments.indexOf(socialNav.currSegment) + 1];
                if(socialNav.segments.indexOf(socialNav.currSegment) == (socialNav.segments.length-1)) {
                    $('span#followDown').hide();
                }
                
            }else{
                $(socialNav.docObj).animate({scrollTop: 0}, 1000);
                $(".navbuttons span").fadeOut();
                socialNav.currSegment = ''+socialNav.segments[0];
                socialNav.scrollByArrow = false;
            }
        });
    }
};  