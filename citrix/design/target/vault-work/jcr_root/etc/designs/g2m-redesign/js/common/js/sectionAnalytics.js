$(window).scroll(function () {
    var y = $(window).scrollTop();
    if (y > 0) {
        var currentTopNavPosition = y;

        $("section").each(function () {
            if( $(this).attr('data-analytics-enable-tracking') == "true" ) {
                var secId = $(this).attr('id');
                if(secId != "")
                    var secName = $(this).attr('name');
                var secHeight = $(this).height();
                var currPos = $(this).position().top;
                var currEnd = secHeight + currPos;
                var diffT = currPos - (currentTopNavPosition);
                var diffB = currEnd - (currentTopNavPosition);
                var diffM = currPos + $(this).height() / 2 - (currentTopNavPosition);
                if( (diffT <= 0 && diffT >= -100) ){
                    setAnalyticsValues($(this));
                }
                else if( $(this).attr("data-analytics-track-when-visible") == "true" &&
                         diffT > 0 &&
                         diffM < $(window).height() ){
                    $currSec = $(this);
                    setAnalyticsValues($(this));
                }
            }
        });
    }
});

function setAnalyticsValues(currSec) {
    if( (utag_data.template !== $(currSec).attr("data-analytics-template") ||
         utag_data.content_type !== $(currSec).attr("data-analytics-content-type")) &&
        $(currSec).attr("data-analytics-enable-tracking") == "true" ) {
        utag_data.template = $(currSec).attr("data-analytics-template");
        utag_data.content_type = $(currSec).attr("data-analytics-content-type");
        utag_data.sub_section = $(currSec).attr("data-analytics-sub-section");
        utag.view(utag_data);
        //console.log("Analytics Debug: Triggering [template=" + utag_data.template + "], [content_type=" + utag_data.content_type + "], [sub_section=" + utag_data.sub_section + "], in section ID " + $(currSec).attr('id'));
    }
}