var node;
function addUpdate(theForm) {
	var name = theForm.fontName.value;
	var family = $("#family").val();
	var size = $("#size").val();
	var lineHeight = $("#lineHeight").val();
	var color = $("#color").val();
	var icon = $("#icon").val();
	var others = $("#others").val();
	node = "";
	var fontName = "";
	if(family) {
		node = family;
		if(name === '')
			fontName = $("#family option:selected").text();
	}
	if(lineHeight) {
		if(node !== "")
			node+="-";
		node+=lineHeight;
		if(name === '')
			fontName += " "+$("#lineHeight option:selected").text();
	}
	if(size) {
		if(node !== "")
			node+="-";
		node+=size;
		if(name === '')
			fontName += " "+$("#size option:selected").text();
	}
	if(color) {
		if(node !== "")
			node+="-";
		node+=color;
		if(name === '')
			fontName += " "+$("#color option:selected").text();
	}
	if(icon) {
		if(node !== "")
			node+="-";
		node+=icon;
		if(name === '')
			fontName += " "+$("#icon option:selected").text();
	}
	if(others) {
		if(node !== "")
			node+="-";
		node+=others;
		if(name === '')
			fontName += " "+$("#others option:selected").text();
	}
	if(fontName != "")
		theForm.fontName.value=fontName
	if(node) {
		var action= path+node;
		jqxhr = $.post( action, $( "#style" ).serialize() ).fail(addFailed).done(addSuccess)
	}
	return false;
}
function addSuccess(){
	var name = $("#fontName").val();
	var family = $("#family").val()+' ';
	var size = $("#size").val()+' ';
	var lineHeight = $("#lineHeight").val()+' ';
	var color = $("#color").val()+' ';
	var icon = $("#icon").val()+' ';
	var others = $("#others").val();
	if(name === '')
		name= $("#family").text()+" "+$("#size").text()+" "+$("#lineHeight").text()+" "+$("#color").text()+" "+$("#icon").text()+" "+$("#others").text();
	var html = '<tr id="'+node+'"><td><a href="#" class="deleteStyle" title="'+name+'" data-name="'+node+'">&#10006;</a></td><td><div class="'+family+size+lineHeight+color+icon+others+'">'+name+'</div></td><tr>';
	if($('#'+node).length > 0) {
		$('#'+node).fadeOut(600,function(){$(this).replaceWith(html).fadeIn(600)})
	}
	else
		$('table').append(html);
	$(".message").removeClass("failed").html("&#10004; Request Processed.").slideDown(600);
	document.forms.style.reset();
	setTimeout(hideMessage,5000);
}
function addFailed(){
	$(".message").addClass("failed").html("&#10008; Error Processing Request").slideDown(600);
	setTimeout(hideMessage,5000);
}
function hideMessage() {
	$(".message").slideUp();
}