var Common = Common || {
	init: function() {
		hashLinkScroll.init();
		Link.init();
		Tab.init();
	},

	lightboxInit: function(){
		styleSwitcher.init();
	}
};

jQuery(Common.init);