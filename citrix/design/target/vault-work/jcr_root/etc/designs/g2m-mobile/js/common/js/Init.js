var Common = Common || {
	init: function() {
		ScrollToTop.init();
	    ExternalLink.init();
	    PopupLink.init();
	    Accordion.init();
	    MainNavigation.init();
	    FormBuilder.init();
	}
};

jQuery(function() {
	Common.init();
});