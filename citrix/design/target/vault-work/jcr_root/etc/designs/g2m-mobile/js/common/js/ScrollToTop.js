var ScrollToTop = {
	selector: ".gotop",
		
	init: function() {
		$(ScrollToTop.selector).click(function() {
		    $('html,body').animate({scrollTop: 0}, 1000);
		});
	}	
};