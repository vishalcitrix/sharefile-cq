var MainNavigation = {
	selector : "#mainNavigation",

	init : function() {
		$(MainNavigation.selector).unbind('click').click(function() {
			$("#navigationDropDown").slideToggle('slow');
			$("#currentCategory").slideToggle();
		});
	
        jQuery("#mainNavigation").click(function () {
            jQuery(this).toggleClass("down-state");
        });
	}
};