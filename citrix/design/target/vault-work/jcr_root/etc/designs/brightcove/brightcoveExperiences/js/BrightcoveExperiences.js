var BCLplayer;
var BCLexperienceModule;
var BCLvideoPlayer;
var BCLcurrentVideo;

//listener for player error
function onPlayerError(event) {
    /* */
}

//listener for when player is loaded
function onPlayerLoaded(id) {
  // newLog();
//  log("EVENT: onPlayerLoaded");
  BCLplayer = brightcove.getExperience(id);
  BCLexperienceModule = BCLplayer.getModule(APIModules.EXPERIENCE);
}

//listener for when player is ready
function onPlayerReady(event) {
 // log("EVENT: onPlayerReady");

  // get a reference to the video player module
  BCLvideoPlayer = BCLplayer.getModule(APIModules.VIDEO_PLAYER);
  // add a listener for media change events
  BCLvideoPlayer.addEventListener(BCMediaEvent.BEGIN, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.COMPLETE, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.CHANGE, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.ERROR, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.PLAY, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.STOP, onMediaBegin);
}  

if (customBC == undefined) {
    var customBC = {};
    customBC.createElement = function (el) {
        if (document.createElementNS) {
            return document.createElementNS('http://www.w3.org/1999/xhtml', el);
        } else {
            return document.createElement(el);
        }
    };
    customBC.createVideo = function (width,height,playerID,playerKey,videoPlayer,VideoRandomID) {
    	var innerhtml = '<object id="myExperience_'+VideoRandomID+'" class="BrightcoveExperience">';
		innerhtml += '<param name="bgcolor" value="#FFFFFF" />';
		innerhtml += '<param name="width" value="'+width+'" />';
		innerhtml += '<param name="height" value="'+height+'" />';
		innerhtml += '<param name="playerID" value="'+playerID+'" />';
		innerhtml += '<param name="playerKey" value="'+playerKey+'" />';
		innerhtml += '<param name="isVid" value="true" />';
		innerhtml += '<param name="isUI" value="true" />';
		innerhtml += '<param name="dynamicStreaming" value="true" />';
		innerhtml += '<param name="@videoPlayer" value="'+videoPlayer+'" />';
		if ( window.location.protocol == 'https:') innerhtml += '<param name="secureConnections" value="true" /> ';
		innerhtml += '<param name="templateLoadHandler" value="onPlayerLoaded" />';
		innerhtml += '<param name="templateReadyHandler" value="onPlayerReady" />';
		innerhtml += '<param name="templateErrorHandler" value="onPlayerError" />';
		innerhtml += '<param name="includeAPI" value="true" /> ';
		innerhtml += '<param name="wmode" value="transparent" />';
		innerhtml += '</object>';
		var objID = document.getElementById(VideoRandomID);
		objID.innerHTML = innerhtml;
		
		var apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "https://sadmin.brightcove.com/js/BrightcoveExperiences.js";
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "https://sadmin.brightcove.com/js/APIModules_all.js";
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "https://files.brightcove.com/bc-mapi.js";
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.text  = "window.onload = function() {brightcove.createExperiences();};";
		objID.parentNode.appendChild(apiInclude);
    };
}








var BCL = BCL || {}; //Namespace for brightcove functions and variables

/**
 * Load player based on the video id
 */
function onPlayerLoaded(id) {
	BCL.player = brightcove.getExperience(id);
	BCL.experienceModule = BCL.player.getModule(APIModules.EXPERIENCE);
}

/**
 * Set the listeners of the video for analytics and add the video to the collection
 */
function onPlayerReady(event) {
	BCL.videoPlayer = BCL.player.getModule(APIModules.VIDEO_PLAYER);
	BCL.videoPlayer.addEventListener(BCMediaEvent.BEGIN, onMediaEventHandler);
	BCL.videoPlayer.addEventListener(BCMediaEvent.COMPLETE, onMediaEventHandler);
	BCL.videoPlayer.addEventListener(BCMediaEvent.CHANGE, onMediaEventHandler);
	BCL.videoPlayer.addEventListener(BCMediaEvent.ERROR, onMediaEventHandler);
	BCL.videoPlayer.addEventListener(BCMediaEvent.PLAY, onMediaEventHandler);
	BCL.videoPlayer.addEventListener(BCMediaEvent.STOP, onMediaEventHandler);
	BCL.addVideoPlayer(BCL.videoPlayer);
}

/**
 * Error handling events for videos and players
 */
function onPlayerError(event) {
    console.log("Brightcove Error: ", event);
}

function onMediaEventHandler(event) {
    var currentVideoId = BCL.videoPlayer.getCurrentVideo().id;
    var currentVideoName = BCL.videoPlayer.getCurrentVideo().displayName;
    
    switch (event.type) {
        case "mediaBegin":
            var currentVideoLength = BCLvideoPlayer.getCurrentVideo().length;
            if (currentVideoLength > 0) {
            	currentVideoLength = currentVideoLength/1000;
            }
            _gaq.push(['_trackEvent', location.pathname, event.type + " - " + currentVideoLength, currentVideoName + " - " + currentVideoId]);
            break;
        case "mediaPlay":
            _gaq.push(['_trackEvent', location.pathname, event.type + " - " + event.position, currentVideoName + " - " + currentVideoId]);
            break;
        case "mediaStop":
            _gaq.push(['_trackEvent', location.pathname, event.type + " - " + event.position, currentVideoName + " - " + currentVideoId]);
            break;
        case "mediaChange":
            _gaq.push(['_trackEvent', location.pathname, event.type + " - " + event.position, currentVideoName + " - " + currentVideoId]);
            break;
        case "mediaComplete":
            _gaq.push(['_trackEvent', location.pathname, event.type + " - " + event.position, currentVideoName + " - " + currentVideoId]);
            break;
        default:
            _gaq.push(['_trackEvent', location.pathname, event.type, currentVideoName + " - " + currentVideoId]);
    }
}

// Pause the video by object id.
BCL.pausePlayerByObjectId = function(id) {
    for(var i = 0; i < BCL.listOfVideoPlayers.length; i++) {
        if(BCL.listOfVideoPlayers[i].callback.substring(0, id.length) === id) {
        	if(BCL.listOfVideoPlayers[i] != null) {
				BCL.listOfVideoPlayers[i].pause(true);
        	}
            break;
        }
    }
};

// Find video player by object id
BCL.findPlayerByObjectId = function(id) {
	for(var i = 0; i < BCL.listOfVideoPlayers.length; i++) {
        if(BCL.listOfVideoPlayers[i].callback.substring(0, id.length) === id) {
        	if(BCL.listOfVideoPlayers[i] != null) {
        		return BCL.listOfVideoPlayers[i];
        	}else {
        		return null;
        	}
        }
	}
	return null;
};

// Add the video player to a the player collection
BCL.addVideoPlayer = function(videoPlayer) {
    BCL.listOfVideoPlayers = BCL.listOfVideoPlayers || new Array();
    BCL.listOfVideoPlayers[BCL.listOfVideoPlayers .length] = videoPlayer;
};

BCL.createVideo = function (width, height, playerId, playerKey, videoPlayer, videoRandomId) {
	 var innerhtml = '<object id="myExperience_' + VideoRandomID + '" class="BrightcoveExperience">';
         innerhtml += '<param name="bgcolor" value="#FFFFFF" />';
         innerhtml += '<param name="width" value="' + width + '" />';
         innerhtml += '<param name="height" value="' + height + '" />';
         innerhtml += '<param name="playerID" value="' + playerId + '" />';
         innerhtml += '<param name="playerKey" value="' + playerKey + '" />';
         innerhtml += '<param name="isVid" value="true" />';
         innerhtml += '<param name="isUI" value="true" />';
         innerhtml += '<param name="dynamicStreaming" value="true" />';
         innerhtml += '<param name="@videoPlayer" value="'+videoPlayer+'" />';
         innerhtml += '<param name="autoStart" value="true" />';
         innerhtml += '<param name="templateLoadHandler" value="onPlayerLoaded" />';
         innerhtml += '<param name="templateReadyHandler" value="onPlayerReady" />';
         innerhtml += '<param name="templateErrorHandler" value="onPlayerError" />';
         innerhtml += '<param name="includeAPI" value="true" /> ';
         innerhtml += '<param name="wmode" value="transparent" />';
         if(window.location.protocol == 'https:') innerhtml += '<param name="secureConnections" value="true" />';
         innerhtml += '</object>';
     var objId = document.getElementById(videoRandomId);
     if(objId === null) {
    	 console.log("There is no containter for this video id: " + videoRandomId);
     }else {
    	 objID.innerHTML = innerhtml;
     }
}
