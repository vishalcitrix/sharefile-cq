var ssize,country,ip_address;
var mobile  = detectDevice();

var calcScreenSize = function () {
	if($(window).width() <= '1024' && $(window).width() > '640') {
	   ssize = 'med';
	}else if($(window).width() <= '640') {
	    ssize = 'small';
	}else {
	    ssize = 'large';
	}
};

function detectDevice() {
    var mobRegEx = RegExp(".*(android|iphone|ipad|ipod|iemobile|blackberry).*");
    return mobRegEx.test(navigator.userAgent.toLowerCase());
}

function setCountryIP(jsonObj) {
	if (typeof(Storage) !== "undefined") {
		sessionStorage.setItem('country_code',jsonObj.registry_country_code);
		sessionStorage.setItem('ip_address',jsonObj.ip);
		country = jsonObj.registry_country_code;
		ip_address = jsonObj.ip;
	}else {
		country = jsonObj.registry_country_code;
		ip_address = jsonObj.ip;
	}
}

function getDemandBaseJson() {
	$.ajax({
        type: "GET",
        async: false,
        url: "https://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358",
        contentType: "application/json",
        dataType: 'jsonp',
        success: function(dataJsonP) {
        	setCountryIP(dataJsonP);
        }
    });
}

function getCountryIP() {
	if (typeof(Storage) !== "undefined") {
		if (sessionStorage.getItem('country_code') == null || sessionStorage.getItem('ip_address') == null){
			getDemandBaseJson();
		}else {
			country = sessionStorage.getItem('country_code');
			ip_address = sessionStorage.getItem('ip_address');
		}
	}else {
		getDemandBaseJson();
	}	
}

$(document).ready(function() {
	calcScreenSize();
	getCountryIP();
});
$(window).resize(calcScreenSize);
$(window).resize(detectDevice);