jQuery( document ).ready( function(){
    jQuery("label[for='Industry'], #Industry, label[for='NumberofEmployees'], #NumberofEmployees, label[for='Country'], #Country, label[for='PostalCode'], #PostalCode ").hide();
});
var industryVal, countryVal, numberofEmployeesVal,marketingAliasVal;
var DemandGen = {};
DemandGen.DemandBase = {
    name: 'DemandGen DemandBase Integrator',
    parserData: null,
    ISP_STR: 'isp',
    industry: null,
    sub_industry: null,
    country: null,
    employee_range: 0,
    phone: null,
    zip: null,
    company_name: null,
    DB_CALLBACK_IP: 0x00000001,
    DB_CALLBACK_EMAIL: 0x00000002,
    DB_CALLBACK_COMPANY: 0x00000004,
    callback_flags : 0,
    processIPInfo: function(data){
        if(! data) data=this.parserData;
        this.parserData=data;
        if(! data) return '';
        this.callback_flags |= this.DB_CALLBACK_IP;
        if ( data[ this.ISP_STR ] )
        return;
        if ( data['country'] )
        this.country = data['country'];
        if ( data['industry'] )
        this.industry = data['industry'];
        if ( data['sub_industry'] )
        this.sub_industry = data['sub_industry'];
        if ( data['employee_range'] )
        this.employee_range = data['employee_range'];
        if ( data['phone'] )
        this.phone = data['phone'];
    // initialize
    },
    processInfo: function( data ){
    var info = null;
    // If the data received is from the email search, we will always override
    // all information
    if ( typeof ( data.person ) != 'undefined' )
        this.callback_flags |= this.DB_CALLBACK_EMAIL;
    if ( typeof ( data.pick ) != 'undefined' )
        this.callback_flags |= this.DB_CALLBACK_COMPANY;
    if ( data.person )
    {
        info = data.person;
        if ( info.industry )
            this.industry = info.industry;
        if ( info.sub_industry )
            this.sub_industry = info.sub_industry;
        if ( info.employee_range )
            this.employee_range = info.employee_range;
        if ( info.country )
            this.country = info.country;
        if ( info.phone )
            this.phone = info.phone;
        if ( info.zip )
            this.zip = info.zip;
        if ( info.company_name )
            this.company_name = info.company_name;
    }
    // If information is received from company pick, only override if we have
    // not retrieved information from IP search OR email address.
    else if ( data.pick )
    {
        info = data.pick;
        if ( info.industry && this.industry == null )
            this.industry = info.industry;
        if ( info.sub_industry && this.sub_industry == null )
            this.sub_industry = info.sub_industry;
        if ( info.employee_range && this.employee_range == 0 )
            this.employee_range = info.employee_range;
        if ( info.country && this.country == null )
            this.country = info.country;
        if ( info.phone && this.phone == null )
            this.phone = info.phone;
        if ( info.zip && this.zip == null )
            this.zip = info.zip;
        if ( info.company_name && this.company_name == null )
            this.company_name = info.company_name;
        if ( info.marketing_alias && this.marketing_alias == null )
            this.marketing_alias = info.marketing_alias;
    }
    return info;
}};
jQuery(function(){
    jQuery('#NumberofEmployees2').change( onEmployeeRangeChange);
    jQuery('#Company').blur(onCompanyBlur);

    // Pre-set demand base fields based on pre-popuation of form
    if( jQuery('#Email').val().length )
        DemandGen.DemandBase.callback_flags |= DemandGen.DemandBase.DB_CALLBACK_EMAIL;
    if( jQuery('#Company').val().length )
        DemandGen.DemandBase.callback_flags |= DemandGen.DemandBase.DB_CALLBACK_COMPANY;

    if( jQuery('#Email').val().length && jQuery('#Company').val().length && jQuery('#NumberofEmployees').val().length == 0 )
        jQuery("#trEmployees").show();
});
if(typeof Demandbase!='undefined'){
    Demandbase.CompanyAutocomplete.widget({
        company: "Company",
        email:   "Email ",
        key: '851275e3832f80a88e468a191a65984648201358',
        callback: function(data) {
            if( pick = data.pick) {
                if(pick.company_name) {
                    industryVal = pick.industry;
                    countryVal = CountryMap[pick.country];
                    marketingAliasVal = pick.marketing_alias;
                    zipVal = pick.zip;
                    numberofEmployeesVal = pick.employee_range;
                }
            }
            if ( DemandGen.DemandBase.processInfo(data) )
            {
                jQuery('#Company').removeClass("validation-failed").addClass("validation-passed");
                jQuery('#Company').next(".validation-advice").hide();
                if ( jQuery('#Industry').length ) {
                    jQuery('#Industry').val(industryVal);
                    jQuery('#Industry').val(industryVal).hide();
                    jQuery('#Industry').next(".validation-advice").hide();
                    jQuery('label[for="Industry"]').hide();
                }
                else
                    jQuery("form").append( jQuery('<input></input>')
                    .attr('name','Industry')
                    .attr('id','Industry')
                    .val( industryVal ) );
                var country = countryVal;
                if ( jQuery('#Country').length ) {
                    jQuery('#Country').val( country );
                    jQuery('#Country').val( country ).hide();
                    jQuery('#Country').next(".validation-advice").hide();
                    jQuery('label[for="Country"]').hide();
                }
                else
                    jQuery("form").append( jQuery('<input></input>')
                    .attr('name','Country')
                    .attr('id','Country')
                    .val( country ) );

                if ( jQuery('#NumberofEmployees').length ) {
                    jQuery('#NumberofEmployees').val(numberofEmployeesVal);
                    jQuery('#NumberofEmployees').val( numberofEmployeesVal ).hide();
                    jQuery('#NumberofEmployees').next(".validation-advice").hide();
                    jQuery('label[for="NumberofEmployees"]').hide();
                }
                else
                    jQuery("form").append( jQuery('<input type="hidden"></input>')
                    .attr('name','NumberofEmployees')
                    .attr('id','NumberofEmployees')
                    .val( numberofEmployeesVal ) );
                //if ( jQuery('#phone').length )
                    //jQuery('#phone').val( DemandGen.DemandBase.phone );
                //else
                    //jQuery("form").append( jQuery('<input></input>')
                    //.attr('type','hidden')
                    //.attr('name','phone')
                    //.attr('id','phone')
                    //.val( DemandGen.DemandBase.phone ) );
                if ( jQuery('#PostalCode').length ){
                    jQuery('#PostalCode').val( zipVal );
                    jQuery('#PostalCode').val( zipVal ).hide();
                    jQuery('#PostalCode').next(".validation-advice").hide();
                    jQuery('label[for="PostalCode"]').hide();}
                else{
                    jQuery('#PostalCode').val( zipVal );
                    jQuery("form").append( jQuery('<input type="hidden"></input>')
                    .attr('name','PostalCode')
                    .attr('id','PostalCode')
                    .val( zipVal ) );
                }
                // Extra demandbase fields
                jQuery(".demandbase").remove(); // clar any existing
            }
        }
    });
}

function onCompanyBlur()
{
    removeCompanyErrorMsg(this)//remove existing error messages form company and its related fields
    DemandGen.DemandBase.callback_flags |= DemandGen.DemandBase.DB_CALLBACK_COMPANY;
    checkEmployeeRange();
    if(marketingAliasVal != jQuery("#Company").val() && DemandGen.DemandBase.marketing_alias != ""){
        industryVal = "";
        countryVal = "";
        numberofEmployeesVal = "";
        zipVal = "";
    }
    if( industryVal == "" || industryVal == null || industryVal == "undefined" ) {
        if(jQuery('#Industry').is(":hidden")){
            jQuery('#Industry').closest('li').show('slow');
            jQuery('#Industry').val("").show('slow');
        }
        jQuery('label[for="Industry"]').show('slow');
    }
    if( countryVal == "" || countryVal == null || countryVal == "undefined") {
        jQuery('#Country').closest("li").show('slow');
        jQuery('#Country').val("").show('slow');
        jQuery('label[for="Country"]').show('slow');
    }
    if( numberofEmployeesVal == "" || numberofEmployeesVal == null || numberofEmployeesVal == "undefined" ) {
        jQuery('#NumberofEmployees').show('slow');
        jQuery('#NumberofEmployees').val("");
        jQuery('label[for="NumberofEmployees"]').show('slow');
    }
     if( zipVal == "" || zipVal == null || zipVal == "undefined" ) {
        jQuery('#PostalCode').closest("li").show('slow');
        jQuery('#PostalCode').val("").show('slow');
        jQuery('label[for="PostalCode"]').show('slow');
    }
}
function checkEmployeeRange()
{
    if (DemandGen.DemandBase.callback_flags == 7 )
    {
        if( jQuery('#NumberofEmployees').length == 0 ||
                jQuery('#NumberofEmployees').val().length == 0)
        {
            jQuery("#trEmployees").show();
        }
        else
            jQuery("#trEmployees").hide();
    }
}
function onEmployeeRangeChange()
{
    if ( jQuery('#NumberofEmployees').length )
        jQuery('#NumberofEmployees').val( jQuery('#NumberofEmployees2').val() );
    else
        jQuery("form").append( jQuery('<input type="hidden"></input>')
    .attr('name','NumberofEmployees')
    .attr('id','NumberofEmployees')
    .val( jQuery('#NumberofEmployees2').val() ) );
}
function removeCompanyErrorMsg(context) {
    if (jQuery(context).parents().is("li")){
        jQuery('label[for=Department__c]').prev('.error').addClass('department');
        jQuery(context).parents('li').nextAll('li.error-field').find('div.error').not('.department').remove();
    }else {
        jQuery(context).nextAll("ul").find("div.error").not('div[for=Department__c]').remove();
    }
}


var CountryMap = {
    "US":"USA",
    "CA":"Canada",
    "AL":"Albania",
    "DZ":"Algeria",
    "AS":"Samoa, American",
    "AD":"Andorra",
    "AO":"Angola",
    "AI":"Anguilla",
    "AQ":"Antarctica",
    "AG":"Antigua/Barbuda",
    "AR":"Argentina",
    "AM":"Armenia",
    "AW":"Aruba",
    "AU":"Australia",
    "AT":"Austria",
    "AZ":"Azerbaijan",
    "BS":"Bahamas",
    "BH":"Bahrain",
    "BD":"Bangladesh",
    "BB":"Barbados",
    "BY":"Belarus",
    "BE":"Belgium",
    "BZ":"Belize",
    "BJ":"Benin",
    "BM":"Bermuda",
    "BT":"Bhutan",
    "BO":"Bolivia",
    "BA":"Bosnia-Herz.",
    "BW":"Botswana",
    "BV":"Bouvet Island",
    "BR":"Brazil",
    "BN":"Brunei Dar-es-S",
    "BG":"Bulgaria",
    "BF":"Burkina-Faso",
    "BI":"Burundi",
    "KH":"Cambodia",
    "CM":"Cameroon",
    "CV":"Cape Verde",
    "KY":"Cayman Islands",
    "TD":"Chad",
    "CL":"Chile",
    "CN":"China",
    "CX":"Christmas Islnd",
    "CO":"Colombia",
    "KM":"Comoros",
    "CG":"Congo",
    "CK":"Cook Islands",
    "CR":"Costa Rica",
    "CI":"Cote d'Ivoire",
    "HR":"Croatia",
    "CY":"Cyprus",
    "CZ":"Czech Republic",
    "DK":"Denmark",
    "DJ":"Djibouti",
    "DM":"Dominica",
    "DO":"Dominican Rep.",
    "TL":"East Timor",
    "EC":"Ecuador",
    "EG":"Egypt",
    "SV":"El Salvador",
    "GQ":"Equatorial Gui.",
    "ER":"Eritrea",
    "EE":"Estonia",
    "ET":"Ethiopia",
    "FK":"Falkland Islnds",
    "FO":"Faroe Islands",
    "FJ":"Fiji",
    "FI":"Finland",
    "FR":"France",
    "GF":"French Guayana",
    "PF":"Frenc.Polynesia",
    "GA":"Gabon",
    "GM":"Gambia",
    "GE":"Georgia",
    "DE":"Germany",
    "GH":"Ghana",
    "GI":"Gibraltar",
    "GR":"Greece",
    "GL":"Greenland",
    "GD":"Grenada",
    "GP":"Guadeloupe",
    "GT":"Guatemala",
    "GN":"Guinea",
    "GW":"Guinea-Bissau",
    "GY":"Guyana",
    "HT":"Haiti",
    "HN":"Honduras",
    "HK":"Hong Kong",
    "HU":"Hungary",
    "IS":"Iceland",
    "IN":"India",
    "ID":"Indonesia",
    "IE":"Ireland",
    "IL":"Israel",
    "IT":"Italy",
    "JM":"Jamaica",
    "JP":"Japan",
    "JO":"Jordan",
    "KZ":"Kazakhstan",
    "KE":"Kenya",
    "KI":"Kiribati",
    "KR":"South Korea",
    "KW":"Kuwait",
    "KG":"Kyrgyzstan",
    "LA":"Laos",
    "LV":"Latvia",
    "LB":"Lebanon",
    "LS":"Lesotho",
    "LR":"Liberia",
    "LI":"Liechtenstein",
    "LT":"Lithuania",
    "LU":"Luxembourg",
    "MO":"Macau",
    "MK":"Macedonia",
    "MG":"Madagascar",
    "MW":"Malawi",
    "MY":"Malaysia",
    "MV":"Maldives",
    "ML":"Mali",
    "MT":"Malta",
    "MH":"Marshall Islnds",
    "MQ":"Martinique",
    "MR":"Mauretania",
    "MU":"Mauritius",
    "YT":"Mayotte",
    "MX":"Mexico",
    "FM":"Micronesia",
    "MD":"Moldavia",
    "MC":"Monaco",
    "MN":"Mongolia",
    "MS":"Montserrat",
    "MA":"Morocco",
    "MZ":"Mozambique",
    "MM":"Myanmar",
    "NA":"Namibia",
    "NR":"Nauru",
    "NP":"Nepal",
    "NL":"Netherlands",
    "AN":"Netherlands Antilles",
    "NC":"New Caledonia",
    "NZ":"New Zealand",
    "NI":"Nicaragua",
    "NE":"Niger",
    "NG":"Nigeria",
    "NU":"Niue Islands",
    "NF":"Norfolk Island",
    "NO":"Norway",
    "OM":"Oman",
    "PK":"Pakistan",
    "PW":"Palau",
    "PA":"Panama",
    "PG":"Papua Nw Guinea",
    "PY":"Paraguay",
    "PE":"Peru",
    "PH":"Philippines",
    "PN":"Pitcairn Islnds",
    "PL":"Poland",
    "PT":"Portugal",
    "QA":"Qatar",
    "RE":"Reunion",
    "RO":"Romania",
    "RU":"Russian Fed.",
    "RW":"Rwanda",
    "KN":"St Kitts&Nevis",
    "LC":"St. Lucia",
    "WS":"Western Samoa",
    "ST":"S.Tome,Principe",
    "SA":"Saudi Arabia",
    "SN":"Senegal",
    "SC":"Seychelles",
    "SL":"Sierra Leone",
    "SG":"Singapore",
    "SK":"Slovakia",
    "SI":"Slovenia",
    "SB":"Solomon Islands",
    "SO":"Somalia",
    "ZA":"South Africa",
    "ES":"Spain",
    "LK":"Sri Lanka",
    "SH":"St. Helena",
    "SR":"Suriname",
    "SZ":"Swaziland",
    "SE":"Sweden",
    "CH":"Switzerland",
    "TW":"Taiwan",
    "TJ":"Tajikstan",
    "TZ":"Tanzania",
    "TH":"Thailand",
    "TG":"Togo",
    "TK":"Tokelau Islands",
    "TO":"Tonga",
    "TT":"Trinidad,Tobago",
    "TN":"Tunisia",
    "TR":"Turkey",
    "TM":"Turkmenistan",
    "TV":"Tuvalu",
    "UG":"Uganda",
    "UA":"Ukraine",
    "AE":"Utd.Arab Emir.",
    "GB":"United Kingdom",
    "UY":"Uruguay",
    "UZ":"Uzbekistan",
    "VU":"Vanuatu",
    "VA":"Vatican City",
    "VE":"Venezuela",
    "VN":"Vietnam",
    "EH":"West Sahara",
    "YE":"Yemen",
    "ZM":"Zambia",
    "ZW":"Zimbabwe"
};