// -----------------------------------
// Sales Chat - handles the display of the Sales Chat section in pre-footer based on the activation of the 
//              sales chat linkn in the top nav.  Solution is intented to bw temporary
// rick.potsakis@citrix.com
// -----------------------------------

var salesChat = salesChat || {
    attempts: 0,
    attemptsMax: 20,
    sectionDefaultId: '#lpSection-default',
    sectionOnlineId: '#lpSection-online',
    sectionInner: '.salesChat-inner',
    targetId: '#lpButton-TopNav',
    mobileTarget: '.lpButton-MobileNav',
    toasterTarget: '.lpButton-Toaster',

    init: function() {
        salesChat.targetId = '#' + $(salesChat.sectionInner).attr('data-targetid');
        if( $(salesChat.targetId).length > 0 && $(salesChat.sectionOnlineId).length > 0 ){
            $(salesChat.sectionOnlineId).hide();
            salesChat.checkChat();
        }
    },

    checkChat: function(){

        setTimeout(function(){
            var $sectionDefault = $(salesChat.sectionDefaultId).first(),
                $sectionOnline = $(salesChat.sectionOnlineId).first(),
                $button = $(salesChat.targetId).first(),
                isShown = false;

            salesChat.attempts += 1;
            
            if( $button.length > 0 && $button.find('a').length > 0 ){
                // chat is active
                $sectionOnline.find('a').attr({
                    'onclick': $button.find('a').attr('onclick'),
                    'href': $button.find('a').attr('href')
                });

                // toggle content
                $sectionOnline.fadeIn();
                $sectionDefault.hide();

                // add mobile button
                var $smButton = $('<a>').attr({
                    'onclick': $button.find('a').attr('onclick'),
                    'href': $button.find('a').attr('href')
                });
                $(salesChat.mobileTarget).append($smButton);

                // add toaster button
                var $trButton = $('<a>').attr({
                    'onclick': $button.find('a').attr('onclick'),
                    'href': $button.find('a').attr('href')
                }).text( $button.find('a').text() );
                $(salesChat.toasterTarget).append($trButton);
                
                isShown = true;
            }

            if(salesChat.attempts == salesChat.attemptsMax || isShown){
                return true;
            } else {
                salesChat.checkChat();
            }
        }, 500);

    }

};