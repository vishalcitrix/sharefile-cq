// -----------------------------------
// Search
// -----------------------------------

var searchAutoSuggest = searchAutoSuggest || {
    index: 0, /* Keyboard Nav Index */
    searchID: "#q",
    iskybrdNav: 1,
    searchFormID: "#searchForm",
    suggestionClass: ".suggestionList",
    suggestionID: "#searchAutoSuggestionsList",
    maxResult: "",
    searchDir: "",
    searchPage: "",
    searchStr: "",
    dataSuggest: "",
    searchVal: "",
    init: function() {
        searchAutoSuggest.searchDir = $("#searchForm #q").attr('data-path');
        searchAutoSuggest.searchPage = $("#searchForm").attr('action');
        searchAutoSuggest.dataSuggest = $("#searchForm #q").attr('data-suggest');
        searchAutoSuggest.maxResult = $("#searchForm #q").attr('data-limit');

        if(searchAutoSuggest.dataSuggest == "true") {
            $(searchAutoSuggest.searchID).on({
                keyup: function(e) {
                    searchAutoSuggest.searchStr = this.value;
                    var iskybrd = searchAutoSuggest.iskybrdNav;

                    if(searchAutoSuggest.searchStr.length < 3) {
                        $(searchAutoSuggest.suggestionClass).hide(); /* Hide the suggestion box. */
                    } else {
                        iskybrd = searchAutoSuggest.keybrdNav(e); /* Handle Keyboard Navigation & Get Result */

                        if(iskybrd != 0) { /* Check we've done keyboard navigation */
                            /* Remove the active class & add to current one */
                            $(searchAutoSuggest.suggestionID +' ul li.active').removeClass('active');
                            $(searchAutoSuggest.suggestionID +' ul li').eq(searchAutoSuggest.index).addClass('active');
                        } else {
                            searchAutoSuggest.getSearchResult(searchAutoSuggest.searchStr, searchAutoSuggest.searchDir);
                        }
                    }
                },
                focus: function() {
                    $(searchAutoSuggest.searchID).val("");
                }
            });
        }

        $(".autosearch-icon").on('click', function(p) {
            p.preventDefault();
            console.log($(searchAutoSuggest.searchID).hasClass('searchInput'));
            if($(searchAutoSuggest.searchID).hasClass('searchInput')) {
                $(searchAutoSuggest.searchID).removeClass('searchInput');
            } else {
                $(this).parents('form').submit();
            }
        });

        $('body').on('click touchstart', function(e) { /* Hide Suggestion box on focus out */
            if(!$(e.target).closest(searchAutoSuggest.searchID +', '+ searchAutoSuggest.searchFormID +' input["type=submit"]').length && ssize!= "small") {
                $(searchAutoSuggest.suggestionClass).hide();
            };
            if(!$(e.target).is(".autosearch-icon") && !$(e.target).is("#q") && ssize == "small") {
                $(searchAutoSuggest.searchFormID).css({'left':'auto'});
                $(searchAutoSuggest.searchID).addClass('searchInput');
            }
        });

        searchAutoSuggest.showHideSearchForm();
        $(window).resize(searchAutoSuggest.showHideSearchForm);
    },
    getContextRoot: function() {
        return $('#searchForm input').attr('data-context');
    },
    keybrdNav: function(e) {
        var index = searchAutoSuggest.index;
        var iskybrd = searchAutoSuggest.iskybrdNav;
        if(e.which == 38) { /* Down arrow */
            index--;
            if(index < 0) { /* Check that we've not tried to select before the first item */
                index = 0;
            }
            iskybrd = 1; /* Set a variable to show that we've done some keyboard navigation */
        }else if(e.which == 40) { /* Up arrow */
            index++;
            if (index > $(searchAutoSuggest.suggestionID + ' ul li').length - 1) { /* Check that index is not beyond the last item */
                index = $(searchAutoSuggest.suggestionID + ' ul li').length - 1;
            }
            iskybrd = 1;
        }else if(e.which == 27) { /* Esc key */
            index = -1;
            $(searchAutoSuggest.suggestionClass).hide();
            iskybrd = 1;
        }else if(e.which == 13) { /* Enter key */
            if(index > -1) {
                $(searchAutoSuggest.suggestionID+' ul li.active a').trigger("click"); /* trigger click on Enter */
            }
            iskybrd = 1;
        }else {
            iskybrd = 0;
        }

        searchAutoSuggest.index = index;
        return iskybrd;
    },
    getSearchResult: function(inputString, path) {
        $.getJSON(searchAutoSuggest.getContextRoot() + "/bin/citrix/getSearchJSON?q="+inputString.toLowerCase()+"&dir="+path,
           function(data) {
            count = 0;
            html = '';
            if(data.success) {
                html = '<ul>';
                $.each(data.hits, function(k,v) {
                    if(count == searchAutoSuggest.maxResult) {
                        return false;
                    } /* break to show only limited hits */
                    html = html + '<li id="srch_item_' + k + '" class="srch_item"><a href="' + v.path + '?q=' + inputString + '&source=search"><span class="title">' + v.title + '</span><span class="excerpt">' + v.excerpt + '</span></a></li>';
                    count++;
                });
                html = html + '</ul>';
            }

            if(count != 0)
                recmd_text = "<span>Recommended links</span><a class='srch_all' href=''>Search All</a>";
            else
                recmd_text = "<span>No Recommended links</span>";

            html = '<div class="srch_recommended">' + recmd_text + '</div>' + html;

            $(searchAutoSuggest.suggestionClass).empty();
            $(searchAutoSuggest.suggestionID).append(html);
            $(searchAutoSuggest.suggestionClass).show();
            $(".srch_all").on('click', function(p) {
                p.preventDefault();
                $(this).parents('form').submit();
            });
        });
    },
    showHideSearchForm: function() {
        if(ssize == "small") {
            $(searchAutoSuggest.searchID).css("width",$(window).width() - 30);
            $(searchAutoSuggest.suggestionID).css("width",$(window).width() - 30);
        }else {
            $(searchAutoSuggest.searchFormID).css("right","0px");
            $(searchAutoSuggest.searchID).css("width","");
            $(searchAutoSuggest.suggestionID).css("width","");
        }
    }
};