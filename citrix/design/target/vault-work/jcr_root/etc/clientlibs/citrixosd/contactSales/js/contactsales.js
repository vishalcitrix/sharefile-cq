jQuery(document).ready(function() {
    setBuyerPerson();
    jQuery("form").submit(function() {
        jQuery("input[type='text']").each(function() {
            if( jQuery(this).val() == jQuery(this).attr("placeholder") )
            jQuery(this).val("");
        });
        setTimeout(function(){
            if (jQuery('.error:visible').first().length == 1) {
                var navOffset = (jQuery('.primary-nav').length == 1) ? jQuery('.primary-nav').height() : 0;
                jQuery('html, body').animate({
                    scrollTop: jQuery('.error:visible').first().offset().top - navOffset
                }, 500);
            }
        }, 250);
    });

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
    });

    jQuery("form").validate({
        rules: {
            Phone: {
                customPhonevalidation: true
            }
        },
        errorElement: "div",
        focusInvalid: false,
        errorPlacement: function(error, element) {
            
        	if (element.attr("name") == "privacy-policy"){
        		error.insertAfter(jQuery(".privacy-policy"));
        	} else if ($(element[0].form).hasClass('error-prepend')) {
                error.insertBefore(element);
            } else {
        		error.insertAfter(element);
        	}

        },
        submitHandler: function(form) {
            setBuyerPerson();
            var email_opt = jQuery('input[name="email_opt_in"]').is(':checked')  ? "true" : "false";
            jQuery('input[name="email_opt_in"]').val(email_opt);
            form.submit();
        }
    });
    jQuery.validator.addMethod("customPhonevalidation", function(value) {
        if( jQuery('#Country').val() == "USA" || jQuery('#Country').val() == "Canada" ) {
            phone = value.replace(/\D/g, '');
            if( phone.indexOf("1") == 0 ) {
                phone = phone.substr(1);
            }
            var regex = /^(\d{10,15})$/;
            if( !regex.test(phone) ) {
                return false;
            }
            jQuery("#Phone").val(function() {
                if( phone.length >= 11 ) {
                    return phone.replace(/(\d{3})(\d{3})(\d{4})(\d)/, "+1 ($1) $2-$3 ext.$4");
                }
                else {
                    return phone.replace(/(\d{3})(\d{3})(\d{4})/, "+1 ($1) $2-$3");
                }
            });
        }
        else {
            phone = value.replace(/\D/g, '');
            if( phone.indexOf("1") == 0 ) {
                phone = phone.substr(1);
            }
            var regex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4,5})(([xX]|[eE][xX][tT])\d{1,5}){0,1}$/;
            if( !regex.test(phone) ) {
                return false;
            }
        }
        return true;
    });
});
function setBuyerPerson(){
    if(jQuery('input[name="Products"]:checked').attr('alt') != 'undefined' && jQuery('input[name="Products"]:checked').attr('alt') != null){
        jQuery('input[name="Buyer_Persona__c"]').val(jQuery('input[name="Products"]:checked').attr('alt'));
    }
    jQuery('input[name="Products"]').click(function(){
        if(jQuery('input[name="Products"]:checked').length == 1){
            jQuery('input[name="Buyer_Persona__c"]').val(jQuery('input[name="Products"]:checked').attr('alt'));
            jQuery('input[name="Product__c"]').val(jQuery('input[name="Products"]:checked').val());
        }else{
            jQuery('input[name="Buyer_Persona__c"]').val('G2MC - Collaboration');
            jQuery('input[name="Product__c"]').val('GoToMeeting');
        }
    });
}

function contact(theForm,leadGeoFlag) {
    var product = new Array();
    theNodeList = theForm.elements['Products'];
    var j=0;
    for( i=0;i<theNodeList.length;i++ )
        if(theNodeList[i].checked) {
            product[j] = theNodeList[i].value;
            j++;
        }
    switch(product.length) {
        case 1: theForm.Product__c.value = product[0];
            setLeadGeo(product[0],leadGeoFlag);
            break;
        case 2: for(i=0; i<2; i++) {
            if(product[i]=="GoToMeeting")
                product[i] = "G2M";
            if(product[i]=="GoToWebinar")
                product[i] = "G2W";
            if(product[i]=="GoToTraining")
                product[i] = "G2T";
            if(product[i]=="HiDef Corporate")
                product[i] = "G2M";
            }
            if(product[0] == "G2M" && product[1] == "G2M"){
                setLeadGeo('GoToMeeting',leadGeoFlag);
                theForm.Product__c.value = "GoToMeeting";
            } else {
                setLeadGeo('Multiple',leadGeoFlag);
                theForm.Product__c.value = "Both "+product[0]+" & "+product[1];
            }
            break;
        case 3: for(i=0; i<3; i++) {
            if(product[i]=="GoToMeeting")
                product[i] = "G2M";
            if(product[i]=="GoToWebinar")
                product[i] = "G2W";
            if(product[i]=="GoToTraining")
                product[i] = "G2T";
            if(product[i]=="HiDef Corporate")
                product[i] = "G2M";
            }
            leadGeo('Multiple',leadGeoFlag);
            if(product[0] == "G2M" && product[2] == "G2M"){
                theForm.Product__c.value = "Both "+product[0]+" & "+product[1];
            } else {
                theForm.Product__c.value = product[0]+" & "+product[1]+" & "+product[2];
            }
            break;
        case 5: theForm.Product__c.value = product[4];
            setLeadGeo('Multiple',leadGeoFlag);
            break;
        default: theForm.Product__c.value = 'GoToMeeting';
            setLeadGeo('GoToMeeting',leadGeoFlag);
    }

}

function setLeadGeo (prod,leadGeoFlag) {
    if(leadGeoFlag){
        countryList = theForm.Country;
        leadGeo = countryList.options[countryList.selectedIndex].title;
        var  idList = { "NA" : [ {'GoToMeeting': '701000000005L4F','GoToWebinar':'701000000005L4H','GoToTraining':'701000000005L4P','HiDef Corporate':'701000000005BDf','Multiple':'701000000005LaQ'}],
            "EMEA" : [ {'GoToMeeting': '701000000005L4K','GoToWebinar':'701000000005L4Q','GoToTraining':'701000000005L4L','HiDef Corporate':'701000000005BDf','Multiple':'701000000005LaV'}],
            "APAC" : [ {'GoToMeeting': '701000000005L4G','GoToWebinar':'701000000005L4U','GoToTraining':'701000000005L4M','HiDef Corporate':'701000000005BDf','Multiple':'701000000005Laa'}] }
        if( theForm.sfdc_campaign_id.value == defaultID && leadGeo != "" ) {
            switch (leadGeo) {
                case "NA" :     defaultID = theForm.sfdc_campaign_id.value = idList.NA[0][prod];
                    break;
                case "EMEA" :   defaultID = theForm.sfdc_campaign_id.value = idList.EMEA[0][prod];
                    break;
                case "APAC" :   defaultID = theForm.sfdc_campaign_id.value = idList.APAC[0][prod];
                    break;
                default: break;
            }
        }
    }
}

function checkAll(theObject) {
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length;i++ )
        theNodeList[i].checked=theObject.checked
}
function unCheck(theObject) {
    var flag=true
    theNodeList = theObject.form.elements[theObject.name];
    for( i=0;i<theNodeList.length-1;i++ )
        if(!theNodeList[i].checked) {
            flag=false;
            break;
        }
    theNodeList[theNodeList.length-1].checked = flag;
}
function getCookieSFID(mktCookie,website) {
    var sfid = '';
    var sfStart = -1;
    if(website == "GoToMyPC") {
        if(mktCookie.indexOf("sf%252F") > -1) {
            var idIndex = mktCookie.indexOf("sf%252F") + 7;
            mktCookie= mktCookie.substring(idIndex);
            if (mktCookie.indexOf("%252F") > -1) {
                mktCookie= mktCookie.substring(0, mktCookie.indexOf("%252F"));
            }
            sfid = mktCookie;
        }
    } else {
        if(mktCookie.indexOf('LST_cmp') > -1) {
            sfStart = mktCookie.indexOf('LST_cmp')+10;
        } else if(mktCookie.indexOf('FIS_cmp') > -1) {
            sfStart = mktCookie.indexOf('FIS_cmp')+10;
        }
        if (sfStart > -1) {
            var temp = mktCookie.substring(sfStart);
            if(temp.indexOf('%26')>-1) {
                var sfEnd = temp.indexOf('%26');
                temp = temp.substring(0,sfEnd);
            }
            if(temp.indexOf('sf-') == 0) {
                sfid = temp.substring(3)
            }
        }
    }
    return sfid;
}
function getmktCookie(name) {
    var mktCookie = '';
    if(document.cookie.indexOf(name) > -1){
        var start = document.cookie.indexOf(name)+name.length+1;
        mktCookie = document.cookie.substring(start);
        if(mktCookie.indexOf(';') >-1){
            var end = mktCookie.indexOf(';')
            mktCookie = mktCookie.substring(0,end);
        }
    }
    return mktCookie;
}
function getURLParam(name) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == name)
      {
          return sParameterName[1];
      }
    }
}