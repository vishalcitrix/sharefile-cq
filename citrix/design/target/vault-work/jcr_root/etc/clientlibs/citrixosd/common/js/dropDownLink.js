// ---------------------------------------
// On Change go to link - Select Drop Down
// ---------------------------------------

var dropDownLink = dropDownLink || {
    dropDown: ".cLink",
    
    init: function() {
        // Binding event on change dropdown
        $(document).ready(function(){
            $(dropDownLink.dropDown).change(function() {
                 window.location = $(':selected',this).attr('value')
            });
        });
    }
};