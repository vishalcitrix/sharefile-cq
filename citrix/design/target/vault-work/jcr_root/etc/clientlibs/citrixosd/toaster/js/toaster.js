// -----------------------------------
// Toaster
// -----------------------------------

var toaster = toaster ||  {
    prevScroll: 0,
    rootNode: ".toaster",
    toasterBar: ".toasterBar",
    footerCTA: "#footer-cta",
    footerSection: "#footerSection",

    init : function() {
        $(document).ready(function() {
             var timer;

            if(toaster.hasToaster()) {
                toaster.handleToaster();

                $(window).scroll(function () {
                    clearTimeout(timer);
                    timer = setTimeout(toaster.handleToaster, 50);
                });
            }
        });         
    },
    
    hasToaster: function(){
        return ( ($(toaster.rootNode).length > 0) && $(toaster.toasterBar).hasClass("active") );
    },
    
    handleToaster: function(){
        var y = $(window).scrollTop();
        var floatingOffset = y + $(window).height();
        var currentScroll = y;
        var showOnScrollUp = $(toaster.toasterBar).attr("data-show-scroll-up");
        var footerOffset;

        // Handle toaster scroll interaction only if user is scrolling down
        // OR showOnScrollUp is marked true in component settings
        if((currentScroll > toaster.prevScroll) || (showOnScrollUp == "true")){
            if($(toaster.footerCTA).length > 0)
                footerOffset = $(toaster.footerCTA).next().offset().top;
            else if($(toaster.footerSection).length > 0)
                footerOffset = $(toaster.footerSection).offset().top;
        
            if(y <= 60){
                $(toaster.toasterBar).slideUp("slow");
            }else if(footerOffset!= undefined && floatingOffset >= footerOffset){
                $(toaster.toasterBar).slideUp("slow");
            }else{
                $(toaster.toasterBar).slideDown("slow");
            }
        }else{
            $(toaster.toasterBar).slideUp("slow");
        }
        toaster.prevScroll = currentScroll;
    }
}; 