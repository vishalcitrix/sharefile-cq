// -----------------------------------
// Belt Navigation- handles the display of the belt nav in mobile viewport
//
// rick.potsakis@citrix.com
// -----------------------------------

var beltNavigation = beltNavigation || {
    subSel: '.belt-subnav',

    currentHighlight: function() {
        var urlPath = window.location.pathname;
        $('.belt-subnav ul li a').each(function() {
            if ($(this)[0].pathname == urlPath) $(this).addClass('current');
        });
    },

    init: function() {

        beltNavigation.currentHighlight();

        $('.linkset-anchor').click(function(e) {
            var $anchor = $(this);
            var linksetId = $anchor.data('linkset');
            var linksetSel = '.linkSet' + linksetId;
            var $beltNav = $(beltNavigation.subSel);
            var $linkSet = $beltNav.find(linksetSel);
            e.preventDefault();

            if( $beltNav.is(":visible") ){
                // hide
                $beltNav.removeClass('active');
                $beltNav.slideUp({
                    complete: function(){
                    }
                });
            } else {
                // show
                $beltNav.find('.linkList').hide();
                $linkSet.show();
                $beltNav.addClass('active');
                $beltNav.slideDown({
                    complete: function(){

                    }
                });
            }

        });

        window.onresize = beltNavigation.onResize;
        beltNavigation.onResize();
    },

    onResize: function(){
        if(window.ssize && window.ssize == 'small'){
            $(beltNavigation.subSel).slideUp({
                complete: function(){
                }
            });
        } else {
            $(beltNavigation.subSel).show();
        }
    }


};