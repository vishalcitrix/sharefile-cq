var StickyHeader = {
	init: function() {
		var element = $('#action-header');
		if(element.hasClass('active')) {
			var next = $('.content-body.container');
			if (element.offset()) {
				var ePos = element.offset().top;
				var eHeight = element.css('height');
				if (eHeight == 'auto')
					eHeight = '60px';
				eHeight = parseInt(eHeight) + parseInt(element.css('padding-top'))
						+ parseInt(element.css('padding-bottom'));
				var marginTop = next.css('margin-top');
				$(window).scroll(function() {
					var position = ePos - $(window).scrollTop();
					if (position < 0) {
						next.css('margin-top', eHeight + 'px');
						element.addClass('fixed-nav');
					} else {
						element.removeClass('fixed-nav');
						next.css('margin-top', marginTop);
					}
				});
			}
		}
	}
}