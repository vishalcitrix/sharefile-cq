var Hero = Hero || {
    selector: ".hero",
    inlineVideoSelector: "a[rel='inline-video']",
    
    init: function() {
        $(Hero.selector).each(function() {
            //Check if the hero has already been initialized
            if($(this).attr("javascript") == null && $(this).attr("javascript") != "enabled") {
                Hero.inlineVideo($(this));
            }
            //Add a attribute to ensure the hero already been initialized so it doesn't get reinitialized
            $(this).attr("javascript", "enabled");
        });
    },
    
    inlineVideo: function(hero) {
        $(hero).find(Hero.inlineVideoSelector).each(function() {
            $(this).click(function() {
                //Get the inline video id
                var inlineVideoId = $(this).attr("href").split("#")[1];
                if(inlineVideoId != null && inlineVideoId != '' && ($(hero).has("#" + inlineVideoId).length ? true : false)) {
                    //Stop the carousel from rotating
                    var carousel = $(hero).closest('.carousel');
                    if(carousel != null) {
                        if(typeof Carousel != 'undefined') {
                        	Carousel.stopRotation();
                        }
                    }
                    
                    //Get the hero DOM hooks
                    var heroContent = $(hero).find(".hero-content-container");
                    var heroContainer = heroContent.parent();
                    var heroContainerHeight = heroContainer.height();
                    //Create a close button markup
                    var videoCloseButton = "<a href='#' class='brightcove-close'>close</a>";
                    //Get the video player
                    var videoContainer = $(hero).find("#" + inlineVideoId);
                    //Get the video, add the container class so the video is centered, add the style position to relative so the close button can be top right, and add the close button.
                    var videoContainerClone = videoContainer.clone().addClass('container').addClass('hero-brightcove-video').css('position','relative').append(videoCloseButton).appendTo(heroContainer);

                    //Hide the content
                    heroContent.hide();
                    
                    //Show the video
                    videoContainerClone.slideDown(800, function() {
                        //Slided down callback
                    });
                    
                    //Set the close button click handler
                    videoContainerClone.find("a.brightcove-close").unbind('click').click(function () {
                        //Hide the video and show the content
                        heroContainer.children('.hero-brightcove-video').animate({'height': heroContainerHeight}, 400, function() {
                            $(this).remove();
                            heroContent.show();
                            
                            //Restart the rotation
                            if(typeof Carousel != 'undefined') {
                            	Carousel.startRotation();
                            }
                        });
                        return false;
                    });
                }
                return false;
            });
        });
    }  
};