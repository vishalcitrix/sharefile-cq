var Carousel = Carousel || {};

Carousel.initialized = false;

Carousel.rotater = null;

Carousel.init = function() {
	if($("#carousel") != null && Carousel.initialized == false) {
		Carousel.bannerNav();
		Carousel.startRotation();
		Carousel.initialized = true;
	}else {
		//Do nothing
	}
};

Carousel.startRotation = function() {
	Carousel.rotater = setInterval(function() {Carousel.bannerRotate(false);}, 8000);
};

Carousel.stopRotation = function() {
	clearInterval(Carousel.rotater);
};

Carousel.bannerNav = function(flag) {
	if(flag) {
		$('div.banner-home-dots').html('');
		$('div.banner-home-dots').css('margin-bottom', '0');
	}else {
		$('div.banner-home-dots').html('<span class="banner-dots-left"><\/span><ul class="banner-dots"><li class="previous"><\/li><\/ul><span class="banner-dots-right"><\/span>');
		$('div.banner-home-dots').css('margin-top', '-17px');
		$('ul.banner-home li').each(function() {
			bannerClass = $(this).attr('class');
			$('ul.banner-dots').each(function() {
				$(this).append('<li class="' + bannerClass + '-dot"><\/li>');
			});
		});
		$('ul.banner-dots').append('<li class="next"><\/li>');
		$('div.banner-home-dots').append('<div class="clearBoth"><\/div>');
		$('ul.banner-dots li').click(function() {
			Carousel.stopRotation();
			var thisClass = $(this).attr('class');
			if(thisClass == 'previous') {
				if($('.banner-home li.active').prev().length == 0) {
					var eNext = $('.banner-home li').last();
				}else {
					var eNext = $('.banner-home li.active')
							.prev();
				}
				if(typeof ntptEventTag == 'function') {
					ntptEventTag('ev=banner-click&evdetail=previous');
				}
				Carousel.bannerRotate(true, eNext);
			}else if(thisClass == 'next') {
				if(typeof ntptEventTag == 'function') {
					ntptEventTag('ev=banner-click&evdetail=next');
				}
				Carousel.bannerRotate(false);
			}else {
				thisClass = thisClass.split('-dot');
				if($('.banner-home li.active').hasClass(
						thisClass[0]) != true) { // disregard if banner clicked is active
					if(typeof ntptEventTag == 'function') {
						ntptEventTag('ev=banner-click&evdetail='
								+ thisClass[0]);
					}
					Carousel.bannerRotate(true, $('.' + thisClass[0]));
				}
			}
		});
	}
};

Carousel.bannerRotate = function(flag, eNext) {
	if(typeof Hero != 'undefined') {
		if($(Hero.selector) != null) {
			$(Hero.selector).each(function () {
				var heroContent = $(this).find(".hero-content-container");
                var heroContainer = heroContent.parent();
                var heroContainerHeight = heroContainer.height();
                
                heroContainer.children('.hero-brightcove-video').animate({'height': heroContainerHeight}, 400, function() {
                    $(this).remove();
                    heroContent.show();
                    
                    //Restart the rotation
                    if(Carousel != null) {
                    	Carousel.startRotation();
                    }
                });
			});
		}
	}

	if(!flag) {
		if($('.banner-home li.active').next().length == 0) {
			var eNext = $('.banner-home li').first();
		}else {
			var eNext = $('.banner-home li.active').next();
		}
	}

	var eCurrent = $('.banner-home li.active');
	$('.banner-dots li.active').removeClass('active');
	var ePrev = false;

	eCurrent.prevAll().each(function() {
		if ($(this).attr('class') == eNext.attr('class'))
			ePrev = true;
	});
	(ePrev) ? eCurrent.css('margin-top', '-340px') : eNext.css('margin-top', '-340px');
	eNext.fadeIn(800);
	dotClass = '.' + eNext.attr('class') + '-dot';
	$(dotClass).addClass('active');
	eCurrent.fadeOut(500, function() {
		$(this).removeClass('active');
		$(this).hide();
		$(this).css('margin-top', '0')
		eNext.css('margin-top', '0')
		eNext.addClass('active');
	});
};