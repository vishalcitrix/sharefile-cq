var BCL = BCL || {}; //Namespace for brightcove functions and variables

BCL.player;
BCL.APIModules;
BCL.adEvent;
BCL.captionsEvent;
BCL.contentEvent;
BCL.cuePointEvent;
BCL.mediaEvent;
BCL.listOfVideoPlayers = new Array();

BCL.onPlayerLoaded = function(id) {
    BCL.player = brightcove.api.getExperience(id);
    BCL.APIModules = brightcove.api.modules.APIModules;
    BCL.adEvent = brightcove.api.events.AdEvent;
    BCL.captionsEvent = brightcove.api.events.CaptionsEvent;
    BCL.contentEvent = brightcove.api.events.ContentEvent;
    BCL.cuePointEvent = brightcove.api.events.CuePointEvent;
    BCL.mediaEvent = brightcove.api.events.MediaEvent;
};

BCL.onPlayerReady = function(event) {
    var videoPlayer = BCL.player.getModule(BCL.APIModules.VIDEO_PLAYER);
    BCL.addVideoPlayer(videoPlayer);
};

BCL.onPlayerError = function(event) {

};

BCL.onMediaEventHandler = function(event) {

};

// Pause the video by object id.
BCL.pausePlayerByObjectId = function(id) {
    if(typeof BCL.listOfVideoPlayers != 'undefined' && typeof id != 'undefined') {
        for(var i = 0; i < BCL.listOfVideoPlayers.length; i++) {
            if(BCL.listOfVideoPlayers[i].experience.id === id) {
                if(BCL.listOfVideoPlayers[i] != null) {
                    BCL.listOfVideoPlayers[i].pause(true);
                }
                break;
            }
        }
    }
};

// Find video player by object id
BCL.findPlayerByObjectId = function(id) {
    if(typeof BCL.listOfVideoPlayers != 'undefined' && typeof id != 'undefined') {
        for(var i = 0; i < BCL.listOfVideoPlayers.length; i++) {
            if(BCL.listOfVideoPlayers[i].experience.id === id) {
                if(BCL.listOfVideoPlayers[i] != null) {
                    return BCL.listOfVideoPlayers[i];
                }else {
                    return null;
                }
            }
        }
        return null;
    }else {
        return null;
    }
};

// Add the video player to a the player collection
BCL.addVideoPlayer = function(videoPlayer) {
    BCL.listOfVideoPlayers[BCL.listOfVideoPlayers .length] = videoPlayer;
};

// Create the video object and insert it in DOM with container ID
BCL.createVideo = function (width, height, playerId, playerKey, videoPlayer, containerId) {
    var innerhtml = '<object id="myExperience_' + containerId + '" class="BrightcoveExperience">';
    innerhtml += '<param name="width" value="' + width + '" />';
    innerhtml += '<param name="height" value="' + height + '" />';
    innerhtml += '<param name="playerID" value="' + playerId + '" />';
    innerhtml += '<param name="playerKey" value="' + playerKey + '" />';
    innerhtml += '<param name="isVid" value="true" />';
    innerhtml += '<param name="isUI" value="true" />';
    innerhtml += '<param name="dynamicStreaming" value="true" />';
    innerhtml += '<param name="@videoPlayer" value="'+videoPlayer+'" />';
    innerhtml += '<param name="autoStart" value="true" />';
    innerhtml += '<param name="includeAPI" value="true" /> ';
    innerhtml += '<param name="htmlFallback" value="true" /> ';
    innerhtml += '<param name="templateLoadHandler" value="BCL.onPlayerLoaded" />';
    innerhtml += '<param name="templateReadyHandler" value="BCL.onPlayerReady" />';
    innerhtml += '<param name="templateErrorHandler" value="BCL.onPlayerError" />';
    innerhtml += '<param name="wmode" value="transparent" />';
    if(window.location.protocol == 'https:') {
        innerhtml += '<param name="secureConnections" value="true" />';
        innerhtml += '<param name="secureHTMLConnections" value="true" />';
    }
    innerhtml += '</object>';
    innerhtml += '<script type="text/javascript">brightcove.createExperiences();</script>';
    var objId = document.getElementById(containerId);
    if(objId === null) {
        console.log("There is no containter for this video id: " + containerId);
    }else {
        objId.innerHTML = innerhtml;
    }
};

BCL.createVideoWithNoAutoPlay = function (width, height, playerId, playerKey, videoPlayer, containerId) {
    var innerhtml = '<object id="myExperience_' + containerId + '" class="BrightcoveExperience">';
    innerhtml += '<param name="width" value="' + width + '" />';
    innerhtml += '<param name="height" value="' + height + '" />';
    innerhtml += '<param name="playerID" value="' + playerId + '" />';
    innerhtml += '<param name="playerKey" value="' + playerKey + '" />';
    innerhtml += '<param name="isVid" value="true" />';
    innerhtml += '<param name="isUI" value="true" />';
    innerhtml += '<param name="dynamicStreaming" value="true" />';
    innerhtml += '<param name="@videoPlayer" value="'+videoPlayer+'" />';
    innerhtml += '<param name="autoStart" value="false" />';
    innerhtml += '<param name="includeAPI" value="true" /> ';
    innerhtml += '<param name="htmlFallback" value="true" /> ';
    innerhtml += '<param name="templateLoadHandler" value="BCL.onPlayerLoaded" />';
    innerhtml += '<param name="templateReadyHandler" value="BCL.onPlayerReady" />';
    innerhtml += '<param name="templateErrorHandler" value="BCL.onPlayerError" />';
    innerhtml += '<param name="wmode" value="transparent" />';
    if(window.location.protocol == 'https:') {
        innerhtml += '<param name="secureConnections" value="true" />';
        innerhtml += '<param name="secureHTMLConnections" value="true" />';
    }
    innerhtml += '</object>';
    innerhtml += '<script type="text/javascript">brightcove.createExperiences();</script>';
    var objId = document.getElementById(containerId);
    if(objId === null) {
        console.log("There is no containter for this video id: " + containerId);
    }else {
        objId.innerHTML = innerhtml;
    }
};

BCL.createLightboxContainers = function () {
    $('.BrightcoveLightboxSettings').each(function(){
        var attr_string = '';
        $.each(this.attributes, function() {
            if(this.name.substr(0, 5) == 'data-'){
                attr_string += ' ' + this.name.substr(5) + '="' + this.value +'"';
            }
        });
        $('#lightbox-container div#lightbox-border').append('<div'+ attr_string +'></div>');
    });
}

BCL.brightcoveContainer = "#lightbox-container .brightcove-container";

BCL.playInLightbox = function(){
    $(BCL.brightcoveContainer).each(function(){
        if($(this).attr('position') == 'show'){
            BCL.createVideo($(this).attr('width'),
                $(this).attr('height'),
                $(this).attr('playerid'),
                $(this).attr('key'),
                $(this).attr('player'),
                $(this).attr('containerid'));
            brightcove.createExperiences();
        }
    });
};

$(document).ready(function() {
    BCL.createLightboxContainers();
});