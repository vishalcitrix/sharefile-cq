var CurrentPageReference = CurrentPageReference || {
	replaceCurrentPage: function(links) {
		if(links != null && links.length > 0) {
			links.each(function() {
				var link = $(this);
				var linkPath= link.attr("href");
				if(linkPath != null && linkPath.length > 0) {
					link.attr("href", linkPath.replace("((currentPage))", window.location.href));
				}
			});
		}
	}
};