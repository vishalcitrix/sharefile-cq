var MultiBlockquote = {
	imagePropertyKey: "data-multiblockquote-image",
	quotePropertyKey: "data-multiblockquote-quote",
	
	init: function() {
	    $('['+ MultiBlockquote.imagePropertyKey +']').each(function() {
	    	var nodeName = $(this).attr(MultiBlockquote.imagePropertyKey);
	        MultiBlockquote.setupBottomMargin(nodeName);
  	        MultiBlockquote.setupHover(nodeName);
	    });
	},
	
	setupHover: function(nodeName) {
	    $('[' + MultiBlockquote.quotePropertyKey + '="' + nodeName + '"] li').each(function() {
	        if($(this).index() == 0) {
	            $(this).show();
	        }else {
	            $(this).hide();
	        }
	    });
	    
	    $('[' + MultiBlockquote.imagePropertyKey + '="' + nodeName + '"] li').hover(function() {
	        var selected = $(this).index();
	        $('[' + MultiBlockquote.quotePropertyKey + '="' + nodeName + '"] li').each(function() {
	            if($(this).index() == selected) {
	                $(this).show();
	            }else {
	                $(this).hide();
	            }
	        });
	    });
	},
	
	setupBottomMargin: function(nodeName) {
	    Array.max = function (array) {
	        return Math.max.apply(Math, array);
	     };

	    var listOfHeights = new Array();
	    $('[' + MultiBlockquote.quotePropertyKey + '="' + nodeName + '"] li').each(function (index) {
	        listOfHeights.push($(this).height());
	    }); 
	    
	    $('[' + MultiBlockquote.quotePropertyKey + '="' + nodeName + '"] li').each(function(index){
	        if((Array.max(listOfHeights) - $(this).height()) > 0){
	            $(this).css("margin-bottom", (Array.max(listOfHeights) - $(this).height()));
	        }
	    });
	}
};   