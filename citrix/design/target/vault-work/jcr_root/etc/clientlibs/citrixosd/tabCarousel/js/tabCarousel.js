//----------------------------------------------------------
//Carousel Components trigger script - used for both tab carousel and normal carousel.
//----------------------------------------------------------

var carousel = carousel || {
    init : function () {
        //cancel in edit mode
        var isEditMode = $('.tab-carousel').first().attr('data-editmode') == 'EDIT';
        if(isEditMode) {
           return false;
        }
        var carouselTrigger = function () {
            $(".carousel-primary").each(function () {
                var itemCount = $(this).attr("data-itemCount");
                var showSingleItemOnSmallViewport = $(this).attr("data-showsingleitemonsmallviewport");
                var desktopItems = $(this).attr("data-itemslarge");
                var tabletItems = $(this).attr("data-itemsmedium");
                var mobileItems = $(this).attr("data-itemssmall");
                var navigation = $(this).attr("data-navigation");
                var disableNavInSmallViewport = $(this).attr("data-disablenavinsmallviewport");
                var disableNavInMediumViewport = $(this).attr("data-disablenavinmediumviewport");
                var autoPlay = $(this).attr("data-autoplay");
                var rewindNavigation = $(this).attr("data-rewindnav");
                var theme = $(this).attr("data-theme");
                var noSpaceoption = $(this).attr("data-nospace");
                var disablePaginationOverlap = $(this).attr("data-disableoverlap");
                var paginationIconsColor = $(this).attr("data-paginationDotsColor");
                var disableNavigationOverlap = $(this).attr("data-disablenavoverlap");
                if (typeof $(this).attr("data-tabcarousel") !== typeof undefined) {
                    var tabCarousel = $(this).attr("data-tabcarousel");
                }
                if (typeof $(this).attr("data-autoloopspeed") !== typeof undefined) {
              	    var autoloopSpeed = $(this).attr("data-autoloopspeed");
                }

                var id = "#" + $(this).attr("id");
                var play = false;
                var rewindC = false;
                var nav= false;
                
                if(navigation == 'true') {
                    nav= true;
                } 
                if(ssize == 'small' && disableNavInSmallViewport == 'true') {
                	nav= false;
                }
                if(ssize == 'med' && disableNavInMediumViewport == 'true') {
                	nav= false;
                }
                if(autoPlay == 'true') {
                    play  = true;
                }
                if((typeof autoloopSpeed !== typeof undefined) && autoloopSpeed !== '') {
                	play = autoloopSpeed;
                }

                if(rewindNavigation == 'true') {
                    rewindC = true;
                }
                
                if (disableNavigationOverlap) {
                    theme=theme+'  owl-theme-features';
                }
                
                if (ssize == 'small' && showSingleItemOnSmallViewport == 'true') //Initialize Owl Carousel Plugin for Small viewport while first item needs to be displayed
                    {
						var owl =  $(id).data('owlCarousel');
						if(owl) {
							owl.destroy(); // destroy if any existing Owl plugin config data is present, Since in window resize configuration could be changed for Viewport   
						}
                        if(tabCarousel === 'true') {
                            $(id).owlCarousel({
                             items : 1,
                             singleItem : true,
                             mouseDrag : false,
                             touchDrag : true,
                             theme:theme,
                             slideSpeed : 300,
                             paginationSpeed : 1000,
                             navigation : true,
                             pagination : true,
                             navigationText : false,
                             stopOnHover : true,
                             autoPlay : play,
                             rewindNav : true,
                             afterMove : function(){
                                                var tabC = $(id).closest('.tab-carousel');
                                                var index = this.currentItem + 1;
                                                tabC.find('.item-'+this.currentitem+' .lazy').attr('src','');
                                                tabC.find('.current').removeClass('current').addClass('hide-for-small-only');
                                                 var dataSrc = tabC.find('.item-'+index+' .lazy').attr('data-src');
                                                tabC.find('.item-'+index+' .lazy').attr('src',dataSrc);
                                                tabC.find('.tab-'+index+'').removeClass('hide-for-small-only').addClass('current');
                                            }
                            });
                        }
                        else {
                            $(id).owlCarousel({
                                items : 1,
                                singleItem : true,
                                mouseDrag : false,
                                touchDrag : false,
                                theme : theme,
                                navigation : true,
                                pagination : true,
                                navigationText : false,
                                autoPlay : false,
                                rewindNav : false,
    						});
                        }


                    }
                else //Initialize Owl Carousel Plugin 
                	{
						var owl =  $(id).data('owlCarousel');
						if(owl) {
							owl.destroy();// destroy if any existing Owl plugin config data is present, Since in window resize configuration could be changed for Viewport
						}
                        if(tabCarousel === 'true') {
                            $(id).owlCarousel({
                                 items : desktopItems,
                                 itemsDesktop : [1921, desktopItems],
                                 itemsTablet : [1024, tabletItems],
                                 itemsMobile : [640, mobileItems],
                                 theme : theme,
                                 navigation : false,
                                 pagination : true,
                                 slideSpeed : 300,
                                 paginationSpeed : 1000,
                                 autoPlay : play,
                                 rewindNav : rewindC,
                                 responsive : true,
                                 navigationText : false,
                                 stopOnHover : true,
                                 afterMove : function(){
                                                var tabC = $(id).closest('.tab-carousel');
                                                var index = this.currentItem + 1;
                                                tabC.find('.current').removeClass('current').addClass('hide-for-small-only');
                                                 var dataSrc = tabC.find('.item-'+index+' .lazy').attr('data-src');
                                                 var timeStamp = new Date().getTime();
                                                 tabC.find('.item-'+index+' .lazy').attr('src',dataSrc+"?t="+timeStamp);
                                                 tabC.find('.tab-'+index+'').removeClass('hide-for-small-only').addClass('current');
                                                }
                            });
                        }
                        else {
                            $(id).owlCarousel({
                            	 items : desktopItems,
                                 itemsDesktop : [1921, desktopItems],
                                 itemsTablet : [1024, tabletItems],
                                 itemsMobile : [640, mobileItems],
                                 theme : theme,
                                 navigation : nav,
                                 slideSpeed : 300,
                                 paginationSpeed : 400,
                                 autoPlay : play,
                                 rewindNav : rewindC,
                                 responsive : true,
                                 navigationText : false,
                                 stopOnHover : true 
                            });
                        }
                   }

                if (disablePaginationOverlap) {
                    $(id).find('div.owl-pagination').css("bottom", "0");
                }
                if (paginationIconsColor) {
                	$(id).find("div.owl-page span").css( "background", paginationIconsColor );
                }

                if (noSpaceoption) {
                    var windowWidth = $(window).width();
                    var itemWidth = windowWidth / itemCount;
                    if (ssize != 'small') {
                        $(id).find(".owl-item").each(function () {
                            $(this).css("width", itemWidth + 'px');
                        });
                    }
                }
            });

        };
        carouselTrigger();
        $(window).resize(carouselTrigger);
        $(".tab-carousel div[class^='tab-']").on('click', function(){
               var classes = $(this).attr('class').split(' ');
               var index = (classes[0].split('-'))[1];
               var dataSrc = $(this).closest('.tab-carousel').find('.item-'+index-1+' .lazy').attr('data-src');
               $(this).closest('.tab-carousel').find('.item-'+index-1+' .lazy').attr('src',dataSrc);
               $(this).closest('.tab-carousel').find('.carousel-primary').trigger('owl.goTo',(index-1));
        });
    }
};
