var ip_address,uuid="";
getIP();
getUUID();
$(document).ready(function(){
	formInit();
});

/* Form Submit*/

function deviceFP(uuid,theForm) {
	var imgLink = commServer+"/commerce/fingerPrint/image.tmpl?org_id="+orgId+'&session_id='+merchantId+uuid;
	var flashLink = commServer+"/commerce/fingerPrint/flash.tmpl?org_id"+orgId+'&session_id='+merchantId+uuid;
	var ele = "<p style=\"background:url('"+imgLink+"&m=1');margin:0;\"></p>";
	ele += "<img src=\""+imgLink+"&m=2\" alt=\"\" width=\"1\" height=\"1\">";
	ele += '<object type="application/x-shockwave-flash" data="'+flashLink+'" width="1" height="1" id="thm_fp"><param name="movie" value="'+flashLink+'" /><div></div></object>';
	$(theForm).append(ele);
}

function formInit() {
	if(typeof(document.forms.tryForm)!= "undefined") {
		document.forms.tryForm.ipAddress.value = ip_address;
		document.forms.tryForm.uuid.value = uuid;
		deviceFP(uuid,document.forms.tryForm);
		channelTrackingInit();
		isNoCC();
		if((typeof(disableJSValidation) == "undefined" || !disableJSValidation)&&typeof(enableValidation)=='function') {
			enableValidation(document.forms.tryForm);
		}
		$(document.forms.tryForm).bind('submit', function() {
			$('.gray-loader').removeClass('hiddenContent');
			$('.button').attr('disabled','disabled').addClass('disabled');
			$('.throbber-loader').css('display','inline-block');
			if(!isValid)
				$('.gray-loader').addClass('hiddenContent');
				// focus on first error
				if(jQuery('.error-label:visible').length > 0)
					jQuery( window ).scrollTop( jQuery('.error-label:visible').first().offset().top );
					$('.button').removeAttr('disabled').removeClass('disabled');
					$('.throbber-loader').css('display','none');
			if(this.isNocc.value == "true") {
				return updateForm(this);
			} else
				return true;
		});
	}
}

function getmktCookie(name) {
	var mktCookie = '';
	if(document.cookie.indexOf(name) > -1){
		var start = document.cookie.indexOf(name)+name.length+1;
		mktCookie = document.cookie.substring(start);
		if(mktCookie.indexOf(';') >-1){
			var end = mktCookie.indexOf(';')
			mktCookie = mktCookie.substring(0,end);
		}
	}
	return mktCookie;
}

function getCookieChannel(mktCookie,param) {
	var pVal = '';
	var pLenght = param.length+7;
	var paramStart = -1;
	if(mktCookie.indexOf('LST_'+param) > -1) {
		paramStart = mktCookie.indexOf('LST_'+param)+pLenght;
	} else if(mktCookie.indexOf('FIS_'+param) > -1) {
		paramStart = mktCookie.indexOf('FIS_'+param)+pLenght;
	}
	if (paramStart > -1) {
		var temp = mktCookie.substring(paramStart);
		if(temp.indexOf('%26')>-1) {
				var paramEnd = temp.indexOf('%26');
				temp = temp.substring(0,paramEnd);
		}
		pVal=temp;
	}
	return pVal;
}

function channelTrackingInit(){
	var promo = "";
	var promoParam = getURLParam('c_pals');
	if(promoParam === null)
		promoParam = getURLParam('promotion');
	if(promoParam === null) {
		var promoCookie = getCookieChannel(getmktCookie('__col_mkt_GoToMeeting'),'pals');
		if(promoCookie=="")
			promoCookie = getCookieChannel(getmktCookie('__col_mkt_GoToManageRemoteSupport'),'pals');
		if(promoCookie != "")
			promo = promoCookie;
	} else
		promo = promoParam;
	if( promo != '' && typeof(document.forms.tryForm)!= "undefined")
		document.forms.tryForm.promotion.value=promo;
}

function setIP(jsonObj) {
	if (typeof(Storage) !== "undefined") {
		sessionStorage.setItem('ip_address',jsonObj.ip);
		ip_address = jsonObj.ip;
	}else {
		ip_address = jsonObj.ip;
	}

	if(typeof(document.forms.tryForm)!= "undefined") {
	  if(document.forms.tryForm.ipAddress.value =="undefined"){
	   document.forms.tryForm.ipAddress.value = ip_address;
	  }
 	}
}

function getDemandBaseJson() {
	$.ajax({
		type: "GET",
		async: false,
		url: "https://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358",
		contentType: "application/json",
		dataType: 'jsonp',
		success: function(dataJsonP) {
			setIP(dataJsonP);
		}
	});
}

function getIP() {
	if (typeof(Storage) !== "undefined") {
		if (sessionStorage.getItem('ip_address') == null){
			getDemandBaseJson();
		}else {
			ip_address = sessionStorage.getItem('ip_address');
		}
	}else {
		getDemandBaseJson();
	}
}

/* Create unique identifier */
function getUUID() {
	var rArray = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
	if(document.cookie.indexOf('__col_visit') > -1) {
		var start = document.cookie.indexOf('__col_visit') + 12;
		var tmp = document.cookie.substring(start);
		var end = tmp.indexOf(';');
		uuid = tmp.substring(0,end);
	}else if(getURLParam('__col_mkt_cookies') != '') {
		var colParam = getURLParam('__col_mkt_cookies');
		if(colParam.indexOf('__col_visit') > -1) {
			var start = colParam('__col_visit')+12;
			uuid = colParam.substring(start);
			if(uuid.indexOf(';') > -1) {
				var endIndex = uuid.indexOf(';');
				uuid = uuid.substring(0,endIndex);
			}
		}
	}
	var currentDate = new Date();
	uuid = uuid.replace(/-/g,'');
	var randomNum = Math.floor(Math.random() * 1000000000000000) + currentDate.getTime();
	$.each([uuid.length,Math.floor(Math.random() * 41) + 33],function(index,value) {
		var index = Math.floor(Math.random() * 62);
		uuid = uuid + rArray[index];
	});
	uuid = uuid + randomNum;
}

function isNoCC() {
	var theForm = document.forms['tryForm'];
	if( typeof(theForm.planKey) != 'undefined')
		var planKey = theForm.planKey.value;
	else if( typeof(theForm.planKeys) != 'undefined')
		var planKey = theForm.planKeys[0].value;
	if(typeof(CommerceRestAPI) == "function" && typeof(theForm) != "undefined" && theForm.promotion.value !="") {
		commerceRestApi = new CommerceRestAPI();
		var promoURL = commerceRestApi.buildURL('','',{'regions':theForm.region.value});
		commerceRestApi.isValidPromotion(function(data) {
			theForm.isNocc.value = data.noCreditCardTrial;
			theForm.validpromo.value = true;
			if(!data.noCreditCardTrial && typeof(document.getElementById('formPassword')) != 'undefined') {
				document.getElementById('formPassword').innerHTML = "";
			}
		}, function(data) {
			theForm.isNocc.value = '';
		}, planKey, theForm.promotion.value, theForm.region.value);
	}
}

function updateForm(theForm) {
	var firstName = theForm.firstName.value;
	var lastName = theForm.lastName.value;
	var email = theForm.emailAddress.value;
	var password = (typeof(theForm.password) != 'undefined') ? theForm.password.value : '';
	var locale = theForm.locale.value;
	var catalog = theForm.catalog.value;
	var quantities = theForm.quantities.value;
	if( typeof(theForm.planKey) != 'undefined')
		var planKeys = theForm.planKey.value;
	else if( typeof(theForm.planKeys) != 'undefined') {
		var planKeys = [];
		var j=0;
		for(i=0; i<theForm.planKeys.length; i++) {
			if(theForm.planKeys[i].checked) {
				planKeys[j] = theForm.planKeys[i].value;
				j++;
			}
		}
	}
	var promotions = theForm.promotion.value;
	var region =  theForm.region.value;
	var ipAddress =  theForm.ipAddress.value;
	var fingerPrintSessionId =  theForm.uuid.value;
	var billingFirstName = theForm.firstName.value;
	var billingLastName = theForm.lastName.value;
	var billingPhone = "";
	var billingAddress1 = "";
	var billingAddress2 = "";
	var billingcity = "";
	var billingState = "";
	var billingZip = "";
	var billingCountry = "";
	if(typeof(theForm.country)!= "undefined" && theForm.country.value != "")
		billingCountry = theForm.country.value;
	var creditCardNumber = "";
	var creditCardExpiresMonth = "";
	var creditCardExpiresYear = "";
	var creditCardCVV = "";
	var directDebitAccountNumber = "";
	var directDebitBankNumber = false;
	var mailingListOptIn = "";
	var daytimePhone = '';
	if(document.getElementById('daytimePhone') && !jQuery('#daytimePhone').hasClass('hiddenContent') && jQuery('#daytimePhone').attr('placeholder') != jQuery('#daytimePhone').val())
		daytimePhone = theForm.daytimePhone.value;
	if(typeof(theForm.mailingListOptIn) != 'undefined') {
		if(theForm.mailingListOptIn.type === "checkbox")
			mailingListOptIn = theForm.mailingListOptIn.checked;
		else
			mailingListOptIn = theForm.mailingListOptIn.value;
	}
	var uc_plan_1_product = "";
	var uc_plan_1_duration = "";
	var uc_plan_1_name = "";

	if(isValid && typeof(theForm.password) != 'undefined') {
		// submit via API
		$(theForm).find('input[type=submit]').attr('disabled', 'disabled');
		if($.browser.msie && $.browser.version < 8 )
			$(theForm).find('input[type=submit]').attr('disabled', true);
		$(theForm).find('input[type=submit]').addClass('disabled');
		var pKey = "";
		if(planKeys instanceof Array) {
			pKey = planKeys[0];
		}
		else {
			pKey = planKeys;
		}
		$(theForm).find('.loading-mask').show();
		commerceRestApi = new CommerceRestAPI();
		commerceRestApi.getPromoData(pKey,region,theForm.catalog.value,promotions,true)
		commerceRestApi.createNewAccount(
			function(data){
				theForm.action = theForm.successUrl.value+"?locale="+locale+"&uc_isTrial=true&uc_isNoCCTrial=true&uc_firstName="+firstName+"&uc_lastName="+lastName+"&uc_email="+email+"&uc_language="+locale+"&uc_promotion="+promotions+"&uc_userKey="+data.userKey+"&uc_accountKey="+data.accountKey+"&uc_casTicket="+data.casTicket+"&uc_plan_1_quantity="+quantities+'&uc_plan_1_keys='+planKeys;
				setTimeout(formSubmit,500);
			},
			function(data){
				var errorFields = '';
				if(typeof(data.error) == 'string'){
					if( data.error.indexOf('User already exist') != -1 && $('#errorUserExistsText').length == 1 ){
						var errorUserExistsText = $('#errorUserExistsText').val();
						var errorUserExistsUrl = ($('#errorUserExistsUrl').length == 1) ? $('#errorUserExistsUrl').val() : '';
						var protomatch = /^(https?|http):\/\//;
						errorFields = '  '+ errorUserExistsText.replace('{url}', '<a target="_blank" href="'+ errorUserExistsUrl +'">'+ errorUserExistsUrl.replace(protomatch, '') +'</a>');
					} else {
						// add API response, remove content inside of parenthesis
						errorFields = 'Please correct the following:<br>'+ data.error.replace(/\(.*\)/gi, '');
					}
				}

				// re-enable submit
				$(theForm).find('input[type=submit]').removeAttr('disabled');
				$(theForm).find('input[type=submit]').removeClass('disabled');

				$(theForm).find('#tryform-error-target').html('<div id="tryform-error-msg" class="error-label icon-attention" style="display:none;"><span>There was an error your submission. '+ errorFields +'</span></div>');
				$(theForm).find('#tryform-error-msg').fadeIn();
				$(theForm).find('.loading-mask').hide();

				if(jQuery('#tryform-error-msg:visible').length > 0)
					jQuery( window ).scrollTop( jQuery('#tryform-error-msg:visible').first().offset().top );
			}, firstName, lastName, email, password, locale, catalog, planKeys, quantities, mailingListOptIn, promotions, region, billingFirstName, billingLastName, billingPhone, billingAddress1, billingAddress2, billingcity, billingState, billingZip, billingCountry, creditCardNumber, creditCardExpiresMonth, creditCardExpiresYear, creditCardCVV, fingerPrintSessionId, true, daytimePhone, ipAddress
		);
		if( $('#tryForm').attr('data-useFallback') == 'true' ){
			setTimeout(formSubmit,500);
		}
	} else if(isValid){
		// fallback POST
		setTimeout(formSubmit,500);
	}
	return false;
}
var cc = 0;

function formSubmit() {
	cc++;
	var theForm = document.forms['tryForm'];
	if(theForm.action.indexOf(theForm.successUrl.value) > -1) {
		if(planObj.product !="") {
			uc_plan_1_product = planObj.product;
			uc_plan_1_duration = planObj.duration+'_'+planObj.period;
			uc_plan_1_name = planObj.name;
			theForm.action += "&uc_plan_1_product="+uc_plan_1_product+"&uc_plan_1_duration="+uc_plan_1_duration+"&uc_plan_1_name="+uc_plan_1_name;
			if(typeof(theForm.__col_mkt_cookies) != "undefined"){
				var col_mkt_cookies = encodeURIComponent(theForm.__col_mkt_cookies.value)
				theForm.action += "&__col_mkt_cookies="+col_mkt_cookies;
			}
			window.location.assign(theForm.action)
		} else {
			setTimeout(formSubmit,500)
		}
	} else if(cc < 20){
		setTimeout(formSubmit,500)
	} else {
		if(typeof(theForm.password) != "undefined")
			theForm.password.value = "";
		theForm.submit();
	}
}

function getURLParam(name) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++){
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == name)
			return sParameterName[1];
	}
	return "";
}