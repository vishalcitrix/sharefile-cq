// -----------------------------------
// FLoating Footer
// vishal.gupta@citrix.com
// -----------------------------------

var floatingFooter = floatingFooter ||  {
    init : function() {
        $(document).ready(function() {
            var t;
            if(floatingFooter.hasFloatingFooter()) {
                floatingFooter.handleFloatingFooter();
                $(window).scroll(function () {
                    clearTimeout(t);
                    t = setTimeout(floatingFooter.handleFloatingFooter, 0);
                });
            }
        });         
    },
    
    hasFloatingFooter: function(){
        return ( $(".floatingFooter").length > 0 );
    },
    
    handleFloatingFooter: function(){
        var y = $(window).scrollTop();
        var floatingFooter = $(".floatingFooterContent");
        var floatingHeight = floatingFooter.height();
        var floatingOffset = y + $(window).height();
    
        if($(".staticFooter").length > 0)
            var footerOffset = $(".staticFooter").offset().top;
        else
            var footerOffset = $("footer").offset().top;
    
		if (!($("#content-body").hasClass( "resourcePage" ))) {
			if(floatingOffset >= footerOffset || y <= 60)
			 floatingFooter.slideUp(500);
			else
			floatingFooter.slideDown(500);
		}
		else{
			if(floatingOffset >= footerOffset)
			floatingFooter.slideUp(500);
			 else
			floatingFooter.slideDown(500);
		}
    }
}; 