var blockContainerFlip = {
	selector: ".block-flip",
	trigger: "hover",
	axis: 'y',
	init: function() {
		$(blockContainerFlip.selector).flip({
			trigger: blockContainerFlip.trigger,
			axis: blockContainerFlip.axis,
		});
	}
}