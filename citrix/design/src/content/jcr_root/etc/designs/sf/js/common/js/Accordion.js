var Accordion = {
    classNames :  {
        categories : {
            active:'minus',
            inactive:'plus'
        },
        active:'active'
    },

    init : function() {
    	$(".accordian-container").each(function() {
    		if($(this).hasClass('active')) {
    			Accordion.hideAll();
    	        Accordion.clickPlus();
    	        Accordion.clickQuestion();
    	        $('#hideAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.hideAll();
                    $(this).addClass(Accordion.classNames.active);
                    $('#showAll').removeClass(Accordion.classNames.active);
    	        }));
    	        $('#showAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.showAll();
                    $(this).addClass(Accordion.classNames.active);
                    $('#hideAll').removeClass(Accordion.classNames.active);
    	        }));
    		}
    	});
    },

    hideAll : function() {
        $('.category').hide();
        $('.accordionentry .' + Accordion.classNames.categories.active).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
    },

    showAll : function() {
        $('.category').show();
        $('.accordionentry .' + Accordion.classNames.categories.inactive).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
    },

    clickPlus : function() {
        $('.accordionentry .text.plus').each(function() {
            $(this).unbind('click').bind('click', ( function() {
                var indicator = $(this)[0];
                if(indicator.className.indexOf(Accordion.classNames.categories.active) >= 0)
                    $(this).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
                else
                    $(this).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
                $(this).next('div').slideToggle();
                return false;
            })
            );
        });
    },

    clickQuestion : function() {
        $('.accordionentry .question').each(function() {
            $(this).unbind('click').bind('click',( function() {
                $(this).next('div').slideToggle();
                return false;
            })
            );
        });
    }
};