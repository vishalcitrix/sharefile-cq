var events = events || {
    init: function() {
        $('.event-filter').change(function() {
            var product = $('#event-product').val() == undefined ? '' : $('#event-product').val();
            var industry = $('#event-industry').val() == undefined ? '' : $('#event-industry').val();
            var topic = $('#event-topic').val() == undefined ? '' : $('#event-topic').val();
            
            var regex = new RegExp(product +'(.+)'+ topic +'(.+)'+ industry);
            
            $(".resourcesFeature-container .events").each(function() {
                if(regex.test($(this).attr('class'))) {
                    $(this).removeClass("hide");
                }else {
                    $(this).addClass("hide");
                }   
            });
            $(".more-list").hide();
        });
        $(".more-list .show").click(function() {
            $(this).parent().parent().children(".events.hide").removeClass("hide").addClass("show");
            $(this).hide();
            $(this).parent().children(".hide").show();
        });
        $(".more-list .hide").click(function() {
            $(this).parent().parent().children(".events.show").removeClass("show").addClass("hide");
            $(this).hide();
            $(this).parent().children(".show").show();
        });
    }
};