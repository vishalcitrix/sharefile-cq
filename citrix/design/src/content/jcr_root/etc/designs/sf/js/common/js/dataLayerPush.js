var dataLayerPush = dataLayerPush || {
    init: function () {
          
        if(formUrlParam.referrer.indexOf("virtual-data-room") >= 0 && formUrlParam.currentPage.indexOf("virtual-data-room/thank-you") >= 0 ){
         var accountId = sessionStorage.vdrAccountId;   
            dataLayer.push({
                'transactionId': accountId,
                'transactionTotal': '70.00',
                'transactionProducts': [{'id': accountId,'sku': 'VDR','name': 'Lead','price': '70.00','quantity': '1'}]
            });
        }
        
        if(formUrlParam.referrer.indexOf("enterprise") >= 0 && formUrlParam.currentPage.indexOf("enterprise/thank-you") >= 0 ){
          var userId = getUU();
            dataLayer.push ({
                'transactionId': userId,
                'transactionTotal': '10.00',
                'transactionProducts': [{'id': userId,'sku': 'Enterprise','name': 'Lead','price': '10.00','quantity': '1'}]
            });
        }
    }
};