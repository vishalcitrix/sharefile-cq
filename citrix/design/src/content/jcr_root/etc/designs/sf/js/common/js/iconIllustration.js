//----------------------------------------------------------
// Icon Illustration, animate illustration based on view port
//----------------------------------------------------------
var iconIllustration = iconIllustration || {
	timer : null,
	init : function () {
		var animate = function () {
			var winHeight = $(window).height();
			var topOfWindow = $(window).scrollTop();
			var botOfWindow = ($(window).scrollTop() + winHeight > $(document).height() - 100);

			if(iconIllustration.timer) {
				window.clearTimeout(iconIllustration.timer);
			}
			
			iconIllustration.timer = window.setTimeout(function() {
				$('.icon-illustration').each(function(i, elem){

					var divPos = $(elem).offset().top;
					var firstDiv = $(elem).find('div').first();

					if ( divPos < topOfWindow+(winHeight*.73) || botOfWindow ) {
						if( !$(firstDiv).hasClass('animate') ){
							$(elem).removeClass("fade-out");
							$(elem).addClass("fade-in");
							$(firstDiv).addClass("animate");
							$(firstDiv).find('div').each(function(j, animLayer){
								$(animLayer).removeClass("animate");
								animLayer.offsetWidth = animLayer.offsetWidth;
								$(animLayer).addClass("animate");
							});
						}
					} else {
						$(firstDiv).removeClass('animate');
						$(firstDiv).find('div').each(function(j, animLayer){
							$(animLayer).removeClass("animate");
							animLayer.offsetWidth = animLayer.offsetWidth;
						});
					}
				});
			}, 100);
		};

		animate();
		$(window).scroll(animate);
	}
};

