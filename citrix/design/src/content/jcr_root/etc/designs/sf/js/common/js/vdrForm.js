var vdrForm = vdrForm || {
    init: function() {
        var theForm;
        $("#first-name").focus();
        $("#vdr-button").attr("href", sessionStorage.activationlink);
        $('.vdr form').on('submit', function(event) {
            $('#IndId').val($("#industry").val());
            $('#IndName').val($("#industry option:selected").text());
            //prevent multiple form submission- disable submit button 
            if($('.vdr-form form').find('.error').length == 0){
                $(this).find(':submit').attr('disabled', 'disabled').addClass('disabled');
                //submit form using AJAX post
                vdrForm.submitForm(this);
                event.preventDefault();
            }else{
                 event.preventDefault();
            }       
        });
        $( window ).on("orientationchange",function(event){
           $('#industry').blur();
        });
    },
    showLightbox: function() {
        Common.lightboxInit();
        $("#lightbox-dynamic").show();
        $("#lightbox-border").addClass($("#lightbox-content").attr("class"));
        vdrForm.bindHideLightbox();
        $("#lightbox-container").fadeIn(100);
    },
    addLightboxContent: function(content) {
        $("#lightbox-dynamic").empty();
        $("#lightbox-dynamic").append('<div>' + content + '</div>');
    },
    bindHideLightbox: function() {
        $("div#lightbox-close").unbind("click").click(function() {
            $("#lightbox-container").fadeOut(100, function() {
                $("#lightbox-content").remove();
                $("#lightbox-dynamic").hide();
                $("#lightbox-border").removeClass().removeAttr("style");
            });
        });
        $("div#lightbox-container").unbind("click").click(function() {
            $("#lightbox-container").fadeOut(100, function() {
                $("#lightbox-content").remove();
                $("#lightbox-dynamic").hide(); 
                $("#lightbox-border").removeClass().removeAttr("style");
            });
        });
    },
    submitForm: function(form) {
        var $form = $(form);
        theForm = $form;
        var action = $form.attr("action");
        var firstName = $form.find('#first-name').val();
        var lastName = $form.find('#last-name').val();
        var email = $form.find('#email').val();
        var companyName = $form.find('#company-name').val();
        var phone = $form.find('#phone-number').val();
        var indName = $form.find('#IndName').val();
        var indId = $form.find('#IndId').val();
        var useCQConfirmationPage = $form.find('#useCQConfirmationPage').val();
        var UserId = $form.find("[name='UserId']").val();
        useCQConfirmationPage = useCQConfirmationPage != null && useCQConfirmationPage != '' ? useCQConfirmationPage : 'true';
             
        $.ajax({
            type: formUrlParam.postType,
            url: action,
            data: {
                'CompanyName': companyName,
                'Phone': phone,
                'IndName': indName,
                'IndId': indId,
                'useCQConfirmationPage': useCQConfirmationPage,
                'Email': email,
                'FirstName': firstName,
                'LastName': lastName,
                'UserId' : UserId
            },
            crossDomain: 'false',
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            jsonpCallback: 'loaded',
            success: function(data, textStatus) {
                if (typeof data.activationLink !== 'undefined') {
                    var activationLink = data.activationLink;
                    sessionStorage.activationlink = activationLink;
                    sessionStorage.vdrAccountId = data.accountId;
                    window.location = $('input#successUrl').val();
                } else {
                    vdrForm.addLightboxContent('Your request could not be processed at this time. Please try again later.');
                    vdrForm.showLightbox();
                    $(theForm).find(':submit').removeClass('disabled');
                    $(theForm).find(':submit').removeAttr('disabled');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                vdrForm.addLightboxContent('Your request could not be processed at this time. Please try again later.');
                vdrForm.showLightbox();
                $(theForm).find(':submit').removeClass('disabled');
                $(theForm).find(':submit').removeAttr('disabled');

            }
        });
    }
};