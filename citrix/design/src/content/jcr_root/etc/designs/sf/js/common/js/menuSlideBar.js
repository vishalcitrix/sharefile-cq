// -----------------------------------
// Top Nav + Slidebar Menu 
// vishal.gupta@citrix.com
// -----------------------------------

var t1,t2;
var menuSlideBar = menuSlideBar || {
    slideBarContent: "#slidebar-menu #slidebar-content",
    utilityNav: ".topNav",
    
    init: function() {
    	if($(".utilityNavigation").length) {
    		var windowResizeEvent = function () {
                //if (ssize == 'med' || ssize == 'small') {
                if (ssize == 'small') {
                    $(menuSlideBar.utilityNav).addClass("navBar");
                    $(".secondaryLinkSet").hide();
                    $(".mainLinkSet").hide();
                    menuSlideBar.sectionDisplay();
                } else if (ssize == 'med') {
                    $(menuSlideBar.utilityNav).removeClass("navBar");
                    $(".mainLinkSet").show();
                    $(".secondaryLinkSet").hide();
                    menuSlideBar.sectionDisplay();
                } else {
                    $(menuSlideBar.utilityNav).removeClass("navBar");
                    $(".mainLinkSet").show();
                    if($(".secondaryLinkSet").length > 0) {
                    	$(".secondaryLinkSet").hide();
                    }
                }
            };
            
            //call window resize event
            $(document).ready(function(){
            	if ($('div.topNav').length) {
                    if($('.topNav').css('position') != "fixed") {
                        $('.mainContent').css('margin-top','0');
                    }
                }
                windowResizeEvent();
            });
            $(window).resize(windowResizeEvent);
    		
    		var scrollPosition = 0;
            //add navbar and handle active state while scrolling down
            $(window).scroll(function () {
                menuSlideBar.sectionDisplay();
                var y = $(window).scrollTop();
                if (y > 0) {
                    if (ssize == 'small') {
                        $(".secondaryLinkSet").hide;
                    }else {
                        $(menuSlideBar.utilityNav).addClass( "navBar" );
                        if($(".secondaryLinkSet").length > 0) {
    	                    $(".secondaryLinkSet").show();
    	                    $(".mainLinkSet").hide();
                        }
                    }
                } else {
                    if (ssize != 'small'){
                    	$(menuSlideBar.utilityNav).removeClass("navBar");
                        if($(".secondaryLinkSet").length > 0) {
    	                    $(".secondaryLinkSet").hide();
    	                    $(".mainLinkSet").show();
                        }
                    }
                }
            });
    	}
    },
    
    sectionDisplay: function() {
        var y = $(window).scrollTop();
        var currentTopNavPosition = $(menuSlideBar.utilityNav).height() + y;
        $("section").each(function () {
            var secId = $(this).attr('id');
            var secName = $(this).attr('name');
            if(typeof(secName) == "undefined" || secName == ""){
                secName = "";
            }
            
            if(secId != ""){
                var secHeight = $(this).height();
                var currPos = $(this).position().top;
                var currEnd = secHeight + currPos;
                var diffT = currPos - (currentTopNavPosition);
                var diffB = currEnd - (currentTopNavPosition);
				var firstSection = $("section").eq(0).position().top;
                var lastSection = $("section").eq($("section").length - 1).position().top + $("section").eq($("section").length - 1).height();
				
                if(diffT <= 0 && diffB >= 0){
                    $(".sec-name").text(secName);
                    if($("section").length > 1){
                        $('.secondaryLinkSet a').each(function(){
                            $(this).removeClass('current');
                            var a_href = $(this).attr('href').split('#');
                            if(secId == a_href[1]){
                                $(this).addClass('current');
                            }
                        })
                    }
                }
				
				if(currentTopNavPosition < firstSection || currentTopNavPosition > lastSection) {
                    $('.secondaryLinkSet a').removeClass('current');
                }				
            }
        });
    }
};
