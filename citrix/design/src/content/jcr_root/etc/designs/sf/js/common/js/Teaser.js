var Teaser = {
    init: function() {
        //Nothing to initialize
    },
    afterTeaserLoad: function() {
        BCL.createLightboxContainers();
        styleSwitcher.init();
        hashLinkScroll.init();
        Link.init();
        formBuilder.init();
        formPlaceHolder.init();
        if((typeof dnt !== 'undefined' && typeof dnt.dntTrue === 'function') ? !dnt.dntTrue() : true) {
            formUrlParam.init();
        }
        trialForm.init();
    }
};