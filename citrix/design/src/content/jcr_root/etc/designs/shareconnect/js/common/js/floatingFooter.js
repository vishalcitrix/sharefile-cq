// -----------------------------------
// Floating Footer
// vishal.gupta@citrix.com
// rick.potsakis@citrix.com
// -----------------------------------

var floatingFooter = floatingFooter || {
    timer : null,
    init : function () {
        if( floatingFooter.hasFloatingFooter() ) {
            floatingFooter.handleFloatingFooter();
            $(window).scroll(function () {
                clearTimeout(floatingFooter.timer);
                floatingFooter.timer = setTimeout(floatingFooter.handleFloatingFooter, 100);
            });
        }
    },
    handleFloatingFooter: function (){
        var y = $(window).scrollTop();
        var footer = $(".floatingFooter");
        var floatingOffset = y + $(window).height();
        var footerOffset = $("#static-footer").offset().top;
        var clientOffset = 0;
        var iosTest = /(iPad|iPhone)/.test(navigator.userAgent);
        
        if( iosTest ){
            clientOffset = 58;
        }
        
            var footerSitemapH = $(".footerSiteMap").outerHeight();
            var paddingOffset = ( footerSitemapH + ($(window).scrollTop() + $(window).height() + clientOffset - $(document).height()) );
            if( paddingOffset > 0){
                $(".floatingFooter").animate({'padding-bottom': paddingOffset}, 500);
            } else {
                $(".floatingFooter").animate({'padding-bottom': 0}, 500);
            }
        
        if(window.location.pathname != '/' && window.location.pathname.indexOf('en_US.html') == -1){
            // hide 'fill-out form' link on sub-pages
            $('.show-for-small-only').find('.form-alt-text').text('to');
        }

        if( floatingOffset <= footerOffset || y <= 60 ){
            footer.slideUp(500);
        } else {
            footer.slideDown(500, function(){
                $(this).css({ 'overflow' : 'visible' });
            });
        }
    },
    hasFloatingFooter: function (){
        return ( $("#static-footer").length > 0 && !$('body').hasClass('cq-wcm-edit') );
    }
};