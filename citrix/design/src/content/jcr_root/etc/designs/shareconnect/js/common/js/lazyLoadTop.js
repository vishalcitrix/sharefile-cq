// -----------------------------------
// Lazy Load Initialization
// -----------------------------------

$(document).ready(function() {
	$(this).scrollTop(0);
	$("img.lazy").unveil(500);
});