var redirect = redirect || {
	init: function() {          
		$.ajax({
			type: 'POST',
			url: '/bin/citrix/getCookieValue',
			data: {
				'cookieName': 'scremme'
			},
			success: function(msg) {
				if(msg != "noCookieSet") {
					window.location = "https://app.shareconnect.com/members/login.tmpl";
				}          
			}
		});
	}  
};