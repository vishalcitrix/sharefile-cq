// -----------------------------------
// Search Refinements toggle
// -----------------------------------

var searchRefinements = searchRefinements || {
    init: function() {
        $(".secondaryNavigation .resource-filter").on("click",function(e){
            $(".searchRefinements-content").slideToggle(500);
        });
        $(document).on("click",function(e){
            if(!$(e.target).parents('li').hasClass('filter')) {
            	searchRefinements.hideFilterDropDown();
            }
        });
        searchRefinements.removeFilter();
    },
    removeFeatured : function() {
    	if($(".featuredResourceList").length) {
    		$(".featuredResourceList").detach();	
    	}
    },
    removeFilter : function() {
    	if($(".searchRefinements-content .drop-down-container").length == 0) {
    		$(".resource-filter").detach();	
    	}
    },
    hideFilterDropDown : function() {
    	$(".drop-down-li").hide();
    }
};

$(window).bind('resize', searchRefinements.hideFilterDropDown);