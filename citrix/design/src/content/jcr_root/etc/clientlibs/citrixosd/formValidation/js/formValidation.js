var invalid = [];
var isValid = false;
var xssRegEx = XRegExp('^((?:(?!(&#[xX]?\\d*|</?\\w*>)))[\\p{L}\\p{M}*\\p{N}\\p{Z}\\s\\p{Cf}\\p{S}\\p{P}])*$');
function enableValidation(formSelector) {
	$(".next-valid").addClass("next-invalid")
	$(".next-valid").removeClass("next-valid")
	/* Validate form onsubmit */
	$(formSelector).bind('submit', runValidation);
}
function runValidation(){
		var theForm = this;
	/* Check validity of required fields */
		$(this).find(".required").each( function() {
			placeholder(this,true)
			if( !isRequired(this) && !isDisabled(this) ) {
				placeholder(this,false)
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
	/* Check validity of password fields */
		var pwd = $(this).find(".password")[0];
		var reType = $(this).find(".re-type")[0];
		if(typeof(pwd) != 'undefined' && !isDisabled(pwd) ) {
			if( !isValidPassword(pwd) ) {
				$(pwd).addClass("invalid")
				invalid.push(pwd);
			} else
				$(pwd).removeClass("invalid")
			if( typeof(reType) != 'undefined') {
				if(pwd.value != reType.value ) {
					$(reType).addClass("invalid")
					invalid.push(reType);
				} else
					$(reType).removeClass("invalid")
			}
		}
	/* Check for email validity */
		$(this).find(".email").each( function() {
			if( !isValidEmail(this) && !isDisabled(this) ) {
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
	/* Check for meeting/webinar/training ID validity */
		$(this).find(".eventId").each( function() {
			if( !isValidEvent(this) && !isDisabled(this) ) {
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
	/* Check for promotion validity */
		$(this).find(".promotion").each( function() {
			if( !isValidPromo(this,theForm) && !isDisabled(this) ) {
				if(theForm.validpromo.value == 'false') {
					$(this).addClass("invalid")
					invalid.push(this)
				} else if(theForm.validpromo.value == 'true') {
					$(this).removeClass("invalid")
					if( theForm.PromoCode.value != "" && theForm.PromoCode.value != $('#PromoCode').attr('placeholder') )
						theForm.promotion.value = theForm.PromoCode.value;
				}
			} else {
				$(this).removeClass("invalid")
				if(theForm.PromoCode.value != "" && theForm.PromoCode.value != $('#PromoCode').attr('placeholder') )
					theForm.promotion.value = theForm.PromoCode.value;
			}
		});
	/* Check for phone validity */
		$(this).find(".phone").each( function() {
			if( !isValidPhone(this) && !isDisabled(this) ) {
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
	/* Check for CC validity */
		$(this).find(".creditcard").each( function() {
			if( !isValidCC(this) && !isDisabled(this) ) {
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
		$(this).find(".creditcard-date").each( function() {
			var month = $(this).find('select.month').val();
			var year = $(this).find('select.year').val();

			if( !isValidCC_date(month, year) && !isDisabled(this) ) {
				$(this).find('.month').addClass("invalid")
				$(this).find('.year').addClass("invalid")
				invalid.push($(this).find('.year')[0])
			} else {
				$(this).removeClass("invalid")
				$(this).find('.month').removeClass("invalid")
				$(this).find('.year').removeClass("invalid")
			}
		});
		$(this).find(".creditcard-cvv").each( function() {
			if( !isValidCC_cvv(this) && !isDisabled(this) ) {
				$(this).addClass("invalid")
				invalid.push(this)
			} else
				$(this).removeClass("invalid")
		});
		$('[aria-label]').not('#Company').blur();
		if($('#Company').val() == ""){ $('#Company').blur(); }
	/* Display error message and states */
		if($(this).hasClass("error-prepend"))
			return displayError(true);
		else
			return displayError(false);

}
/* Check Required fields */
function isRequired(theObj) {
	if( ((theObj.type == "text"|| theObj.type == "select-one") && theObj.value == "") || (theObj.type == "checkbox" && !theObj.checked) ) {
		return false;
	} else return true;
}
/* Validate password */
function isValidPassword(theObj, theObj1) {
	var pwdRegEx = RegExp('.*[0-9].*[a-zA-Z].*|.*[a-zA-Z].*[0-9].*');
	var pwdExclude = /[<>]|(\\[0-9])/;
	var isXSS = false;
	isXSS = !xssRegEx.test(theObj.value)
	if( pwdExclude.test(theObj.value) || !pwdRegEx.test(theObj.value) || theObj.value.length < 8 || theObj.value.length>32 || isXSS ) {
		return false;
	} else
		return true;
}
/* Validate Email Address */
function isValidEmail(theObj) {
	var emailRegEx = RegExp('^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,15}$');
	return emailRegEx.test(theObj.value)
}
/* Validate Event */
function isValidEvent(theObj) {
	var eventRegEx = RegExp('([0-9]){9,}?');
	return eventRegEx.test(theObj.value);
}
/* Validate Promotion */
function isValidPromo(theObj,theForm) {
	var planKey = "";
	if( typeof(theForm.planKey) != 'undefined')
		planKey = theForm.planKey.value;
	else if( typeof(theForm.planKeys) != 'undefined')
		planKey = theForm.planKeys[0].value;
	if( typeof(CommerceRestAPI) == "function" && theForm.PromoCode.value != "" && theForm.PromoCode.value != $('#PromoCode').attr('placeholder') ) {
		var commerceRestApi = new CommerceRestAPI();

		commerceRestApi.isValidPromotion(function(data) {
			theForm.validpromo.value = true;
			if( typeof(theForm.isNocc) != "undefined" )
				theForm.isNocc.value = data.noCreditCardTrial;
		}, function(data) {
			theForm.validpromo.value = false;
			if( typeof(theForm.isNocc) != "undefined" )
				theForm.isNocc.value = "";
		}, planKey, theForm.PromoCode.value, theForm.region.value);

	} else if(theForm.PromoCode.value == "" || theForm.PromoCode.value == $('#PromoCode').attr('placeholder')) {
		theForm.validpromo.value = true;
	}
}
/* Validate phone number. Return error if phone field is required */
function isValidPhone(theObj) {
	var phoneRegEx = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4,5})(([xX]|[eE][xX][tT])\d{1,5}){0,1}$/;
	var specialRegEx = /(\^)+|(&|!|@)+|(\$)+|(\*)+/gi;
	var phone = theObj.value.replace(/\D/g, '');
	var phoneRequired = $(theObj).hasClass('required');
	var validPhone = false;
	if( phone.indexOf("1") == 0 )
		phone = phone.substr(1);
	if( $('#Country').val() == "USA" || $('#Country').val() == "Canada" ) {
		phoneRegEx = /^(\d{10,15})$/;
		if( phoneRegEx.test(phone) ) {
			validPhone = true;
			if( phone.length >= 11 ) {
				phone = phone.replace(/(\d{3})(\d{3})(\d{4})(\d)/, "+1 ($1) $2-$3 ext.$4");
			}
			else {
				phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "+1 ($1) $2-$3");
			}
			theObj.value = phone;
			validPhone = true;
		}
	}
	else if( phoneRegEx.test(phone) && !specialRegEx.test(theObj.value) ) {
			validPhone = true;
	}
	if(phoneRequired || validPhone) {
		return validPhone;
	} else {
		var filter = /([\@|\!|\#|\$|\%|\^|\&|\*|\:|\;|\"|\\|\~|\`|\,|\<|\>|\{|\}|\[|\]])/g;
		var tempdata = theObj.value.replace(filter,"")
		if( tempdata.length >20 )
			theObj.value = tempdata.substring(0,19);
		else
			theObj.value = tempdata;
		return true;
	}
}
/* Validate creditcard */
function isValidCC(theObj) {
	var ccRegEx = RegExp('^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|6(?:011|5[0-9]{2})[0-9]{12})$');
	var cc = theObj.value.replace(/\D/g, '');
	if( ccRegEx.test(theObj.value) ) {
		return true;
	} else
		return false;
}
function isValidCC_date(month, year) {
	var dateSubmitted = new Date(year,parseInt(month)-1, 1);
	var dateToday = new Date();

	if( dateSubmitted > dateToday ) {
		return true;
	} else
		return false;
}
function isValidCC_cvv(theObj){
	var ccNum = $('.creditcard').val();
	var amexRegEx = RegExp('^(3[47][0-9]{13})$');
	if( (amexRegEx.test(ccNum) && theObj.value.length == 4) ||
			(!amexRegEx.test(ccNum) && theObj.value.length == 3) )
	{
		return true;
	} else {
		return false;
	}
}
/* Is Disabled check */
function isDisabled(theObj){
	return $(theObj).attr('disabled');
}
/* Display Error text and states. Return true if valid */
function displayError(prepend,theObj) {
	if(typeof(theObj) != "undefined" ) {
		var objParent = $(theObj).parent().parent();
		objParent.removeClass("error-field");
		$(objParent).find("div.error-label").remove();
	} else {
		$(".error-field").removeClass("error-field");
		$(".error-label").remove();
	}
	if($(".invalid").length < 1) {
		$(".error").hide();
		$(".next-invalid").addClass("next-valid");
		$(".next-invalid").removeClass("next-invalid");
	} else {
		$(".next-valid").addClass("next-invalid")
		$(".next-valid").removeClass("next-valid")
	}
	var flag = invalid.length;
	invalid = $.unique(invalid);
	while( invalid.length>0 ) {
		var theObj = invalid.pop();
		$(theObj).parent().parent().addClass("error-field");
		if(theObj.title != '') {
			if(prepend)
				$(theObj).parent().parent().prepend('<div class="error-label icon-attention"><span>'+theObj.title+"</span></div>")
			else
				$(theObj).parent().append('<div class="error-label icon-cancel"><span>'+theObj.title+"</span></div>")
		}
	}
	if(flag>0) {
		$('.error-label').fadeIn();
		isValid = false;
	} else {
		isValid = true
	}
	return isValid;
}
/* Validation functions used for onChange events */
function validateRequired(theObj) {
	var prepend = $(theObj).closest('form').hasClass("error-prepend");
	if( !isRequired(theObj) && !isDisabled(theObj) ) {
		$(theObj).addClass("invalid")
		invalid.push(theObj)
	} else
		$(theObj).removeClass("invalid")
	displayError(prepend,theObj);
}
function validateEmail(theObj) {
	var prepend = $(theObj).closest('form').hasClass("error-prepend");
	if( !isValidEmail(theObj) && !isDisabled(theObj) ) {
		$(theObj).addClass("invalid")
		invalid.push(theObj)
	} else
		$(theObj).removeClass("invalid")
	displayError(prepend,theObj);
}
function validatePassword(theObj) {
	var prepend = $(theObj).closest('form').hasClass("error-prepend");
	if( !isValidPassword(theObj) && !isDisabled(theObj) ) {
		$(theObj).addClass("invalid")
		invalid.push(theObj)
	} else
		$(theObj).removeClass("invalid")
	displayError(prepend,theObj);
}
function validateReType(theObj, compareObj) {
	var prepend = $(theObj).closest('form').hasClass("error-prepend");
	if( theObj.value != compareObj.value ) {
		$(theObj).addClass("invalid")
		invalid.push(theObj)
	} else
		$(theObj).removeClass("invalid")
	displayError(prepend,theObj);
}
$.support.placeholder = false;
var ptest = document.createElement('input');
if('placeholder' in ptest) $.support.placeholder = true;
function placeholder(obj, flag) {
	if(!$.support.placeholder) {
		if(obj.value == "" && !flag && typeof($(obj).attr('placeholder')) != 'undefined') {
			obj.value = $(obj).attr('placeholder');
		}
		if(obj.value == $(obj).attr('placeholder') && flag) {
			obj.value = "";
		}
	}
}
/*remove Hyphens to given field of a form*/
function clearHyphens(form, field) {
	var f = form[field].value;
	    f = htmlEncode(f);//remove user entered js/html
	var nf = f.replace(/\-/g, '');
	form[field].value = nf;
}
/*Using jQuery : the script to escape HTML/JS characters*/
function htmlEncode(value) {
     if (value) {
         return $('<div/>').text(value).html();
     } else {
         return '';
     }
 }