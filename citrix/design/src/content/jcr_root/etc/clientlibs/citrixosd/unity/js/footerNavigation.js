var footerNav = footerNav || {
	iconOpen:"icon-up-open",
	iconClose: "icon-down-open",
	init: function() {
		$(".footerNavigation .navList h5").click(function(){
			if($(this).css("cursor") === "pointer") {
				$(this).next().slideToggle();
				$(this).toggleClass(footerNav.iconOpen).toggleClass(footerNav.iconClose);
			}
		});
	}
};