//---------------------------------------------------------
//Get URL Parameters and add those to form as hidden fields
//---------------------------------------------------------

var wcmmode = getURLParameter("wcmmode");
var _debug = getURLParameter("debug");
var paramRegEx = /(^|\&)src=/i;

var formUrlParam = formUrlParam || {
    domainName : '',
    referrer : '',
    referrerHostName : '',
    urlParam : '',
    allSources : '',
    allSourcesInput : '',
    inputStr : '',
    paramString : '',
    src : 'direct',
    CookieDate : '',
    OSName : '',
    BrowserType : '',
    BrowserVersion : '',
    postType : 'POST',
    sourcesCaptured: false,
    currentPage:'',

    setSrc: function() {
        if(this.referrer && this.referrer.indexOf(this.domainName) < 0)
            this.src = getDomainName(this.referrerHostName);
        if(getURLParameter("src"))
            this.src = getURLParameter("src");
    },
    getAllURLParam: function() { //Converting URL Parameters to hidden fields
        if(this.paramString) {
            this.urlParam = this.paramString;
        }

        if(!this.urlParam) {
            this.urlParam = "src=" + this.src;
        }else if(!paramRegEx.test(this.urlParam)) {
            this.urlParam = "src=" + this.src + "&" + this.urlParam;
        }

        this.inputStr = '';
        var params = this.urlParam;

        var paramArray = params.split("&");
        for(var i = 0; i < paramArray.length; i++) {
            var paramVal = paramArray[i].split("=");
            if(paramVal[1]) {
                this.inputStr += "<input type='hidden' value='" + paramVal[1] + "' name='" + paramVal[0] + "'>";
            }
        }
    },
    getAllSources: function() {
        this.allSources = getCookie('allSources') != null ? getCookie('allSources') : '';
        var seperator = '';
        var isLstSrc = false;
        if(this.allSources) {
            seperator = ',';
            if(this.src == this.allSources.substring(this.allSources.lastIndexOf(',') + 1))
                isLstSrc = true;
        }

        if(this.src && !this.sourcesCaptured && this.referrer.indexOf(this.domainName) < 0 && !isLstSrc) {
            this.allSources += (seperator + this.src);
            document.cookie = "allSources=" + this.allSources + ";expires=" + this.CookieDate + ";path=/";
        }

        if(this.allSources) {
            this.allSourcesInput = "<input type='hidden' value='" + this.allSources + "' name='AllSources'>";
        }
    },
    getBrowserOSInfo: function() {
        var nAgt = navigator.userAgent;
        var nameOffset,verOffset,ix;
        var fullVersion  = ''+parseFloat(navigator.appVersion);
        this.BrowserVersion = parseInt(navigator.appVersion,10);

        // In Opera, the true version is after "Opera" or after "Version"
        if((verOffset=nAgt.indexOf("Opera")) != -1) {
           this.BrowserType = "Opera";
           fullVersion = nAgt.substring(verOffset + 6);
           if ((verOffset = nAgt.indexOf("Version")) != -1)
               fullVersion = nAgt.substring(verOffset + 8);
        }
        // In MSIE, the true version is after "MSIE" in userAgent
        else if((verOffset=nAgt.indexOf("MSIE")) != -1) {
            this.BrowserType = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset + 5);
        }
        // In Chrome, the true version is after "Chrome"
        else if((verOffset=nAgt.indexOf("Chrome")) != -1) {
            this.BrowserType = "Chrome";
            fullVersion = nAgt.substring(verOffset + 7);
        }
        // In Safari, the true version is after "Safari" or after "Version"
        else if((verOffset=nAgt.indexOf("Safari")) != -1) {
            this.BrowserType = "Safari";
            fullVersion = nAgt.substring(verOffset + 7);
           if ((verOffset=nAgt.indexOf("Version")) != -1)
               fullVersion = nAgt.substring(verOffset + 8);
        }
        // In Firefox, the true version is after "Firefox"
        else if((verOffset=nAgt.indexOf("Firefox")) != -1) {
            this.BrowserType = "Firefox";
            fullVersion = nAgt.substring(verOffset + 8);
        }
        // In most other browsers, "name/version" is at the end of userAgent
        else if((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            this.BrowserType = nAgt.substring(nameOffset,verOffset);
            fullVersion = nAgt.substring(verOffset + 1);
            if(this.BrowserType.toLowerCase() == this.BrowserType.toUpperCase()) {
                this.BrowserType = navigator.appName;
            }
        }

        // trim the fullVersion string at semicolon/space if present
        if ((ix = fullVersion.indexOf(";")) != -1)
            fullVersion = fullVersion.substring(0,ix);
        if ((ix = fullVersion.indexOf(" ")) != -1)
            fullVersion = fullVersion.substring(0,ix);

        this.BrowserVersion = parseInt(''+fullVersion,10);
        if(isNaN(this.BrowserVersion)) {
            this.BrowserVersion  = ''+parseFloat(navigator.appVersion);
        }

        //get OS Name
        if (navigator.appVersion.indexOf("Win") != -1) this.OSName = "Windows";
        else if (navigator.appVersion.indexOf("Mac") != -1) this.OSName = "MacOS";
        else if (navigator.appVersion.indexOf("X11") != -1) this.OSName = "UNIX";
        else if (navigator.appVersion.indexOf("Linux") != -1) this.OSName = "Linux";

        //IE Version < 10
        if(this.BrowserType == "Microsoft Internet Explorer" && this.BrowserVersion < 10)
            this.postType = "GET";

    },
    setData: function(){
        this.referrer = getUrlNoQS(document.referrer);
        this.referrerHostName = getHostName(this.referrer);
        this.domainName = document.domain;
        this.paramString = location.search.substring(1);
        this.CookieDate = new Date;
        this.CookieDate.setFullYear(this.CookieDate.getFullYear() + 1);
        this.setSrc();
        this.getBrowserOSInfo();
        this.currentPage = $(location).attr('href');
        this.getAllURLParam();
    },
    init: function() {
        if((!CQ.WCM && !wcmmode) || (!CQ.WCM && wcmmode && _debug)) {
            this.getAllSources();
            this.sourcesCaptured = true;
            $(document).ready(function() {
                if(!$('form').parent().hasClass('ignore-params')) {
                    $('form').append("<input type='hidden' value='" + getUU() + "' name='UserId'>");
                    $('form').append("<input type='hidden' value='" + getSessionId() + "' name='SessionId'>");
                    $('form').append(formUrlParam.inputStr + formUrlParam.allSourcesInput);
                }
                appendPramsToLinks(formUrlParam.urlParam);
            });
        }
    }
};
//Set formUrlParam Data before init()
formUrlParam.setData();

function getCookie(c_name) {
    var c_value = " " + document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

//Getting domain name from URL
function getDomainName(hostName) {
	return hostName.replace(/\./g,'');
}

//Getting complete domain name from URL
function getFullDomainName(url) {
    var start = 7;
    var fullDomainName = '';
    if(url.startsWith('https://'))
        start++;
    url=url.substring(start);
    var end = url.indexOf('?');
    if (url.indexOf('/') > -1)
        end = url.indexOf('/');
    if(end > 0)
        url=url.substring(0,end);
    return url;
}

// get URL without query string
function getUrlNoQS(url) {
    var end = url.indexOf('?');
    if(end > 0)
        url = url.substring(0,end);
    return url;
}

//Getting particular URL parameter
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
}

//Getting User Id
function getUU() {
    if(getCookie("uuid") == null) {
        var uu;
        if(getCookie("__utma")) {
            //process classic ga cookie
            if(_debug) {
                console.log("Found __utma cookie: " + JSON.stringify(getCookie("__utma")));
            }
            uu = getCookie("__utma").split('.')[1] + getCookie("__utma").split('.')[2];
        }else if(getCookie("_ga")) {
            //process universal ga cookie
            if(_debug) {
                console.log("Found _ga cookie: " + JSON.stringify(getCookie("_ga")));
            }
            uu = getCookie("_ga").split('.')[2] + getCookie("_ga").split('.')[3];
        }
        if(typeof (uu) == "undefined") {
            uu = generateUUID();
        }

        document.cookie = "uuid=" + uu + ";expires=" + formUrlParam.CookieDate + ";path=/";
    }

    return getCookie("uuid");
}

//Create unique identifier
function generateUUID() {
    var rArray = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    var currentDate = new Date();
    var uuid = (Math.floor(Math.random() * 10000000000) + currentDate.getTime()) + '';
    for(var i = 0; i < 7; i++) {
        var index = Math.floor(Math.random() * 62);
        uuid = uuid + rArray[index];
    }
    return uuid;
}

function getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        return match[2];
    }
    else {
        return '';
    }
}

function getDomain(url) {
    var hostName = getHostName(url);
    var domain = hostName;

    if(hostName != null) {
        var parts = hostName.split('.').reverse();

        if(parts != null && parts.length > 1) {
            domain = parts[1] + '.' + parts[0];

            if(hostName.toLowerCase().indexOf('.co.uk') != -1 && parts.length > 2) {
              domain = parts[2] + '.' + domain;
            }
        }
    }
    return domain;
}

function appendPramsToLinks(){
    $('a').each(function() {
        var href = $(this).attr('href');
        var searchString = '';
        var urlRegEx = /redirects\/(try|buy)/i;
        var paramStart = '?';
        var param = formUrlParam.urlParam ? formUrlParam.urlParam + '&' : '';
        var AllSources = getCookie('allSources') ? "&AllSources=" + getCookie('allSources') : '';
        var SessionId = getCookie('sessionId') ? "&SessionId=" + getSessionId() : '';

        if(typeof(href) != 'undefined' && href.indexOf('?') > 0) {
            paramStart = '&';
            searchString = href.substring(href.indexOf('?') + 1);
        }

        param += "UserId=" + getUU() + AllSources + SessionId;
        if(urlRegEx.test(href) && !paramRegEx.test(searchString)) {
            $(this).attr("href", href + paramStart + param);
        }
    });
}

function getSessionId() {
    if(getCookie("sessionId")) {
        sessionId = getCookie("sessionId");
    }else {
        sessionId = generateSessionId();
        document.cookie = "sessionId=" + sessionId + "; path=/";
    }
    return sessionId;
}

function generateSessionId() {
    var guidHolder = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
    var hex = '0123456789abcdef';
    var r = 0;
    var guidResponse = "";
    for(var i = 0; i < 36; i++) {
        if(guidHolder[i] === 'x') {
            r = Math.random() * 16 | 0;
            guidResponse += hex[r];
        } else if(guidHolder[i] === 'y') {
            r = Math.random() * 16 | 0;
            // clock-seq-and-reserved first hex is filtered and remaining hex values are random
            r &= 0x3; // bit and with 0011 to set pos 2 to zero
            r |= 0x8; // set pos 3 to 1 as 1
            guidResponse += hex[r];
        } else {
            guidResponse += guidHolder[i];
        }
    }
    return guidResponse;
}