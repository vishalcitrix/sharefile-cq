var dotDotDot = dotDotDot || {
    init: function(){
        $(document).ready(function(){
            dotDotText();
        });
    }
};

function dotDotText() {
    $('.content-wrapper').height(215);
    $('.content-wrapper').dotdotdot();
}

$(window).resize(dotDotText);