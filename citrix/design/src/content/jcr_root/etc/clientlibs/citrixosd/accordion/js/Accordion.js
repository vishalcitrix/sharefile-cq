var Accordion = {
    classNames :  {
        categories : {
            active:'minus',
            inactive:'plus'
        }
    },

    init : function() {
    	$(".accordian-container").each(function() {
    		if($(this).hasClass('active')) {
    			Accordion.hideAll();
    	        Accordion.clickPlus();
    	        Accordion.clickQuestion();
    	        $('#hideAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.hideAll();
    	        }));
    	        $('#showAll').unbind('click').bind('click', (function(event) {
    	            event.preventDefault();
    	            Accordion.showAll();
    	        }));
    		}
    	});
    },

    hideAll : function() {
        $('.category').hide();
        var parentAccordEntry = $('.accordionentry .' + Accordion.classNames.categories.active);
        parentAccordEntry.removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
        parentAccordEntry.find('div:first-child').removeClass(parentAccordEntry.attr('data-expand-icon')).addClass(parentAccordEntry.attr('data-collapse-icon'));
    },

    showAll : function() {
        $('.category').show();
        var parentAccordEntry = $('.accordionentry .' + Accordion.classNames.categories.inactive);
        parentAccordEntry.removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
        parentAccordEntry.find('div:first-child').removeClass(parentAccordEntry.attr('data-collapse-icon')).addClass(parentAccordEntry.attr('data-expand-icon'));
    },

    clickPlus : function() {
        $('.accordionentry .text.plus').each(function() {
            $(this).unbind('click').bind('click', ( function() {
                var indicator = $(this)[0];
                var collapseIcon = $(this).attr('data-collapse-icon');
                var expandIcon = $(this).attr('data-expand-icon');

                if(indicator.className.indexOf(Accordion.classNames.categories.active) >= 0) {
                    $(this).removeClass(Accordion.classNames.categories.active).addClass(Accordion.classNames.categories.inactive);
                    $(this).find('div:first-child').removeClass(expandIcon).addClass(collapseIcon);
                }
                else{
                    $(this).removeClass(Accordion.classNames.categories.inactive).addClass(Accordion.classNames.categories.active);
                    $(this).find('div:first-child').removeClass(collapseIcon).addClass(expandIcon);
                }
                $(this).next('div').slideToggle();
                return false;
            })
            );
        });
    },

    clickQuestion : function() {
        $('.accordionentry .question').each(function() {
            $(this).unbind('click').bind('click',( function() {
                $(this).next('div').slideToggle();
                return false;
            })
            );
        });
    }
};