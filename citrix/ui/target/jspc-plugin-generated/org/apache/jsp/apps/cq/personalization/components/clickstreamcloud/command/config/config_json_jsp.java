package org.apache.jsp.apps.cq.personalization.components.clickstreamcloud.command.config;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.day.cq.collab.commons.CollabUtil;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.JSONWriterUtil;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.TidyJsonItemWriter;
import com.day.cq.security.profile.Profile;
import com.day.cq.security.profile.ProfileManager;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.core.stats.PageViewStatistics;
import com.day.cq.xss.ProtectionContext;
import com.day.cq.xss.XSSProtectionService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.io.StringWriter;
import java.net.URI;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class config_json_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {




    void setProfileInitialData(JSONWriter writer, ProfileManager pMgr,
                               SlingHttpServletRequest slingRequest,
                               String absoluteDefaultAvatar,
                               XSSProtectionService xss) throws Exception {
       
        writer.key("profile").object();
        final Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
        Profile profile = pMgr.getProfile(session.getUserID(), session);
        if (profile != null) {
            String avatar = CollabUtil.getAvatar(profile, profile.getPrimaryMail(),absoluteDefaultAvatar);
            //increate avatar size
            avatar = avatar == null ? "" : avatar.replaceAll("\\.32\\.",".80.");
            writer.key("avatar").value(avatar);
            writer.key("path").value(profile.getPath());

            Boolean isLoggedIn = profile.getAuthorizable().getID() != null && profile.getAuthorizable().getID() != "anonymous";
            writer.key("isLoggedIn").value(isLoggedIn);
            writer.key("isLoggedIn" + JSONWriterUtil.KEY_SUFFIX_XSS)
            		.value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,isLoggedIn.toString()));
            
            writer.key("authorizableId")
                    .value(profile.getAuthorizable().getID());
            writer.key("authorizableId" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getAuthorizable().getID()));

            writer.key("formattedName")
                    .value(profile.getFormattedName());
            writer.key("formattedName" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getFormattedName()));

            for (String key : profile.keySet()) {
                if (!key.startsWith("jcr:") && !key.startsWith("sling:") && !key.startsWith("cq:last")) {
                    String s = profile.get(key, String.class);
                    s = s != null ? s : "";
                    writer.key(key)
                            .value(s);
                    writer.key(key + JSONWriterUtil.KEY_SUFFIX_XSS)
                            .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,s));
                }
            }

            Date created = profile.get("memberSince", Date.class);
            if( created == null) {
                created = profile.get("jcr:created", Date.class);
            }
            if( created != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy h:mm a", slingRequest.getLocale());
                writer.key("memberSince")
                    .value(df.format(created));
            }

            Date birthday = profile.get("birthday", Date.class);
            if( birthday != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy", slingRequest.getLocale());
                writer.key("birthday")
                    .value(df.format(birthday));
            }
        }
        writer.endObject();
    }

    void setPageInitialData(JSONWriter writer,
                            SlingScriptHelper sling,
                            Page currentPage) throws Exception {
        writer.key("pagedata").object();
        if (currentPage != null) {
            long monthlyHits = 0;
            try {
                final PageViewStatistics pwSvc = sling.getService(PageViewStatistics.class);
                Object[] hits = pwSvc.report(currentPage);
                if (hits != null && hits.length > 2) {
                    monthlyHits = (Long) hits[2];
                }
            } catch (WCMException ex) {
                monthlyHits = -1;
            }
            writer.key("hits").value(monthlyHits);
            writer.key("title").value(currentPage.getTitle());
            writer.key("path").value(currentPage.getPath());

            String navTitle = currentPage.getNavigationTitle();
            if(navTitle == null) {
            	navTitle = currentPage.getTitle();
            }
            if(navTitle == null) {
            	navTitle = currentPage.getName();
            }
            writer.key("navTitle").value(navTitle);
            
            if(currentPage.getTemplate() != null) {
            	writer.key("template").value(currentPage.getTemplate().getPath());
                writer.key("thumbnail").value(currentPage.getTemplate().getThumbnailPath());
            }

            Tag[] tags = currentPage.getTags();
            String tagsStr = "";
            for(Tag tag: tags) {
                tagsStr += tag.getTitle() + " ";
            }
            writer.key("tags").value(tagsStr);
            String descr = currentPage.getDescription();
            writer.key("description").value(descr != null ? descr : "");

        }


        Random rand = new Random(new Date().getTime());
        DecimalFormat df = new DecimalFormat("0.00");
        double r = rand.nextDouble();
        writer.key("random").value(df.format(r));
        writer.endObject();
    }

    void setTagCloudInitialData(JSONWriter writer, Page currentPage) throws Exception {
        writer.key("tagcloud").object();
        if (currentPage != null) {
            writer.key("tags").array();
            for (Tag tag : currentPage.getTags()) {
                writer.value(tag.getTagID());
            }
            writer.endArray();
        }
        writer.endObject();
    }

    void setSurferInfoInitialData(JSONWriter writer,
                                  SlingHttpServletRequest request) throws Exception {
        writer.key("surferinfo").object();
        String ip = request.getRemoteAddr();
        String keywords = null;
        String referer = request.getHeader("Referer");
        if (referer != null) {
            URI uri = new URI(referer);
            String query = uri.getQuery();
            if (query != null) {
                int qindex = query.indexOf("q=");
                if(qindex >-1) {
                    int andindex = query.indexOf("&",qindex);
                    keywords = query.substring(qindex+2,andindex > -1 ? andindex : query.length());
                    keywords = keywords.replaceAll("\\+", " ");
                }
            }
        }
        keywords = (keywords != null ? keywords : (request.getParameter("q") != null ? request.getParameter("q") : ""));
        keywords = keywords.replaceAll("<","&lt;");
        keywords = keywords.replaceAll(">","&gt;");
        writer.key("IP").value(ip);
        writer.key("keywords").value(keywords);
        writer.endObject();
    }


    void setClickstreamCloudUI(JSONWriter writer, boolean isEditMode) throws Exception {
        writer.key("ui").object();
        writer.key("target").value("clickstreamcloud-gui");
        writer.key("version").value("light");
        if (isEditMode) {
            writer.key("hideEditLink").value(false);
            writer.key("hideLoadLink").value(false);
            writer.key("disableKeyShortcut").value(false);
        }
        writer.endObject();
    }

    void dumpConfigs(Node rootNode, JSONWriter writer, boolean isEditMode) throws JSONException, RepositoryException {
        if (rootNode != null) {
            writer.key("configs").object();
            NodeIterator ni = rootNode.getNodes();
            TidyJsonItemWriter tjiw = new TidyJsonItemWriter(Collections.EMPTY_SET);
            while (ni.hasNext()) {
                Node child = ni.nextNode();
                writer.key(child.getName()).object();
                if (child.hasNode("ui")) {
                    writer.key("ui");
                    tjiw.dump(child.getNode("ui"), writer, 10);
                }

                if (child.hasNode("store")) {
                    writer.key("store");
                    tjiw.dump(child.getNode("store"), writer, 10);
                }

                if (isEditMode) {
                    if (child.hasNode("edit")) {
                        writer.key("edit");
                        tjiw.dump(child.getNode("edit"), writer, 10);
                    }
                }
                writer.endObject();
            }
        }
        writer.endObject();
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here



    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");
    
    Externalizer externalizer = sling.getService(Externalizer.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);

    String absoluteDefaultAvatar = "";
    if(externalizer != null){
        absoluteDefaultAvatar = externalizer.absoluteLink(slingRequest, slingRequest.getScheme(), CollabUtil.DEFAULT_AVATAR);
    }

    boolean isEditMode = (WCMMode.fromRequest(request) != WCMMode.DISABLED);
    ProfileManager pMgr = sling.getService(ProfileManager.class);
    StringWriter buf = new StringWriter();
    try {
        Page cPage = null;
        if (request.getParameter("path") != null) {
            Resource r = resourceResolver.getResource(request.getParameter("path"));
            cPage = (r != null ? r.adaptTo(Page.class) : null);
        } else {
            cPage = currentPage;
        }

        TidyJSONWriter writer = new TidyJSONWriter(buf);
        writer.setTidy(true);

        writer.object();
        writer.key("data").object();
        //TODO find a more generic place for these configs
//        setProfileInitialData(writer, pMgr, slingRequest, absoluteDefaultAvatar, xss);
//        setPageInitialData(writer, sling, cPage);
//        setTagCloudInitialData(writer, cPage);
//        setSurferInfoInitialData(writer,slingRequest);
        writer.endObject();

        //write clickstreamcloud UI config
        setClickstreamCloudUI(writer, isEditMode);

        //dump configs found under current node
        dumpConfigs(currentNode, writer, isEditMode);

        writer.endObject();
    } catch (Exception e) {
        log.error("Error while generating JSON clickstreamcloud config", e);
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.toString());
    }
    // send string buffer
    response.getWriter().print(buf.getBuffer().toString());


    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
