package org.apache.jsp.apps.cq.personalization.components.contextstores.surferinfo;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.day.cq.wcm.msm.api.LiveRelationshipManager;
import java.util.Collection;
import com.day.cq.wcm.msm.api.LiveRelationship;
import com.day.cq.wcm.msm.api.RolloutConfig;
import java.util.List;
import com.day.text.Text;
import com.day.cq.wcm.api.WCMException;
import java.io.PrintWriter;
import com.day.cq.wcm.emulator.EmulatorGroup;
import org.apache.sling.commons.json.JSONObject;
import com.day.cq.wcm.emulator.Emulator;
import com.day.cq.wcm.emulator.EmulatorService;
import static com.day.cq.wcm.mobile.api.MobileConstants.HTML_ID_CONTENT_CSS;
import org.apache.sling.commons.json.JSONException;
import com.day.cq.wcm.mobile.api.device.DeviceGroup;
import com.day.cq.wcm.mobile.api.device.DeviceGroupList;
import org.apache.commons.lang3.StringEscapeUtils;
import java.io.StringWriter;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class internal_005fepilog_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {




    


    Page getMobilePage(LiveRelationshipManager relationMgr, Page currentPage) throws com.day.cq.wcm.api.WCMException {
        if (relationMgr != null && currentPage != null) {
            PageManager pageManager = currentPage.getPageManager();
            Collection<LiveRelationship> relations  = relationMgr.getLiveRelationships(currentPage, null, null, false);
            for(LiveRelationship lr: relations) {
                List<RolloutConfig> configs = lr.getRolloutConfigs();
                for(RolloutConfig rc: configs) {
                    if( rc.getPath().toLowerCase().indexOf("mobile") != -1) {
                        Page mobilePage = pageManager.getContainingPage(lr.getTargetPath());
                        if( mobilePage != null) {
                            return mobilePage;
                        }
                    }
                }
            }
        }
        return null;
    }

    List<EmulatorGroup> getEmulatorGroups(EmulatorService emulatorService, Page mobilePage) {
        return emulatorService.getEmulatorGroups(mobilePage.adaptTo(Resource.class));
    }

    List<Emulator> getEmulators(EmulatorService emulatorService, Page mobilePage) {
        return emulatorService.getEmulators(mobilePage.adaptTo(Resource.class));
    }

    StringWriter getEmulatorsConfig(DeviceGroupList deviceGroups, String contextPath) {
        StringWriter res = new StringWriter();
        String delim = "";

        res.write("{");
        for (final DeviceGroup group : deviceGroups) {
            final List<Emulator> emulators = group.getEmulators();
            for (final Emulator emulator : emulators) {
                res.write(delim);
                res.write(StringEscapeUtils.escapeEcmaScript(emulator.getName()) + ": {");
                res.write("plugins: {");

                String pluginDelim = "";
                if (emulator.canRotate()) {
                    res.write("rotation: {");
                    res.write("ptype: CQ.wcm.emulator.plugins.RotationPlugin.NAME,");
                    res.write("config: {");
                    res.write("defaultDeviceOrientation: \"vertical\"");
                    res.write("}");
                    res.write("}");

                    pluginDelim = ",";
                }

                if (emulator.hasTouchScrolling()) {
                    res.write(pluginDelim + "touchscrolling: {");
                    res.write("ptype: CQ.wcm.emulator.plugins.TouchScrollingPlugin.NAME,");
                    res.write("config: {}");
                    res.write("}");
                }

                res.write("},");
                res.write("group: \"" + StringEscapeUtils.escapeEcmaScript(group.getName()) + "\",");
                res.write("title: \"" + StringEscapeUtils.escapeEcmaScript(emulator.getTitle()) + "\",");
                res.write("description: \"" + StringEscapeUtils.escapeEcmaScript(emulator.getDescription()) + "\",");
                res.write("contentCssPath: \"" +
                        (null != emulator.getContentCssPath() ?
                                contextPath + emulator.getContentCssPath():
                                "null") + "\"");


                res.write("}");
                delim = ",";
            }
        }
        res.write("}");
        return res;

    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here




    Page cPage = null;
    if (request.getParameter("path") != null) {
        Resource r = resourceResolver.getResource(request.getParameter("path"));
        cPage = (r != null ? r.adaptTo(Page.class) : null);
    } else {
        cPage = currentPage;
    }

    if( cPage != null ) {
        //found corresponding mobile page
        LiveRelationshipManager relationMgr = sling.getService(LiveRelationshipManager.class);
        Page mobilePage = getMobilePage(relationMgr, cPage);
        if( mobilePage == null ) {
            //try by injecting "_mobile" in the path
            String path = cPage.getPath();
            String computedPath = Text.getAbsoluteParent(path, 1);
            computedPath = path.replaceAll(computedPath, computedPath + "_mobile");
            Resource computed = resourceResolver.resolve(computedPath);
            if( computed != null ) {
                mobilePage = computed.adaptTo(Page.class);
            }
        }

        if( mobilePage != null) {
            EmulatorService emulatorService = sling.getService(EmulatorService.class);
            List<Emulator> allEmulators = getEmulators(emulatorService, mobilePage);
            final DeviceGroupList deviceGroups = mobilePage.adaptTo(DeviceGroupList.class);

            if(deviceGroups != null && !allEmulators.isEmpty()) {


      out.write("<script type=\"text/javascript\">\n");
      out.write("    $CQ(function() {\n");
      out.write("        CQ_Analytics.ClientContextUI.onLoad(function() {\n");
      out.write("\n");
      out.write("            var path = CQ_Analytics.PageDataMgr.getProperty(\"path\");\n");
      out.write("\n");
      out.write("            if( !path ) return;\n");
      out.write("            var app = path.substring(9);\n");
      out.write("            app = app.substring(0, app.indexOf(\"/\"));\n");
      out.write("            //surferinfo slider\n");
      out.write("            $CQ(function() {\n");
      out.write("                var createSliderSI = function(event, show, vertical) {\n");
      out.write("                    CQ_Analytics.SurferInfoMgr.slider = new CQ_Analytics.Slider({\n");
      out.write("                        \"vertical\": vertical,\n");
      out.write("                        \"wrap\": \"circular\",\n");
      out.write("                        \"animation\": \"slow\",\n");
      out.write("                        \"start\": start,\n");
      out.write("                        \"clazz\": \"cq-cc-slider-surferinfo\",\n");
      out.write("                        \"parent\": $CQ(\".cq-cc-surferinfo-thumbnail\").parent()\n");
      out.write("                    });\n");
      out.write("                    CQ_Analytics.SurferInfoMgr.slider.init();\n");
      out.write("\n");
      out.write("                    var devicesObj = {};\n");
      out.write("\n");
      out.write("                    var surferInfoStore = ClientContext.get(\"surferinfo\");\n");
      out.write("                    var valuesStore = surferInfoStore.initProperty;\n");
      out.write("                    var deviceList = [{\n");
      out.write("                        \"id\": \"Desktop\",\n");
      out.write("                        \"picturePath\": ClientContext.get(\"surferinfo/thumbnail\", true),\n");
      out.write("                        \"name\": ClientContext.get(\"surferinfo/browserFamily\", true)\n");
      out.write("                    }];\n");
      out.write("\n");
      out.write("                    ");

                        for (final DeviceGroup group : deviceGroups) {
                            final List<Emulator> emulators = group.getEmulators();
                            for (final Emulator emulator : emulators) {
                                final JSONObject e = new JSONObject();
                                try {
                                    e.put("id",emulator.getName());
                                    e.put("name",emulator.getName());
                                    e.put("path",emulator.getPath());
                                    e.put("picturePath",emulator.getPath() + "/thumbnail.png");
                                    e.put("css",emulator.getPath() + "/css/source/emulator.css");
                                    e.put("contextCss",emulator.getContentCssPath());
                                    e.put("group", group.getName());
                                    
      out.write("deviceList.push(");
      out.print(e.toString());
      out.write(')');
      out.write(';');

                                    
      out.write("devicesObj[\"");
      out.print(emulator.getName());
      out.write("\"] = ");
      out.print(e.toString());
      out.write(';');

                                } catch (JSONException e1) {}
                            }
                        }
                    
      out.write("\n");
      out.write("\n");
      out.write("                    var getItemHTML = function(id, imagePath, title) {\n");
      out.write("                        var item = $CQ(\"<li>\");\n");
      out.write("                        var div = $CQ(\"<div>\")\n");
      out.write("                                .attr(\"title\", title)\n");
      out.write("                                .attr(\"data-id\",id)\n");
      out.write("                                .css(\"width\", \"80px\")\n");
      out.write("                                .css(\"height\", \"80px\")\n");
      out.write("                                .css(\"background-image\", \"url(\"+_g.shared.HTTP.externalize(imagePath)+\")\")\n");
      out.write("                                .css(\"background-repeat\", \"no-repeat\")\n");
      out.write("                                .css(\"background-position\", \"center center\")\n");
      out.write("                                .css(\"padding\", \"4px\")\n");
      out.write("                                .bind(\"click\",function() {\n");
      out.write("                                    CQ_Analytics.SurferInfoMgr.slider.select($CQ(this).parent().attr(\"jcarouselindex\"));\n");
      out.write("                                })\n");
      out.write("                                .appendTo(item);\n");
      out.write("                        return item;\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("                    var start = 1;\n");
      out.write("                    var totalItems = 0;\n");
      out.write("                    var current = CQ_Analytics.SurferInfoMgr.getProperty(\"device\") || \"\" ;\n");
      out.write("                    for(var i = 0; i < deviceList.length; i++) {\n");
      out.write("                        var p = deviceList[i];\n");
      out.write("                        totalItems ++;\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.slider.carousel.append(getItemHTML(p[\"id\"], p[\"picturePath\"], p[\"name\"]));\n");
      out.write("                        if( p[\"id\"] == current ) {\n");
      out.write("                            start = totalItems;\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("\n");
      out.write("                    CQ_Analytics.SurferInfoMgr.slider.onSelect = function(toLoadId) {\n");
      out.write("                        var mobilePath = \"");
      out.print(mobilePage.getPath());
      out.write("\";\n");
      out.write("\n");
      out.write("                        var speed = \"slow\";\n");
      out.write("\n");
      out.write("                        var DESKTOP_MAIN_ID = CQ_Analytics.MobileSliderUtils.getConfig(app, \"DESKTOP_MAIN_ID\");\n");
      out.write("                        var MOBILE_MAIN_ID = CQ_Analytics.MobileSliderUtils.getConfig(app, \"MOBILE_MAIN_ID\");\n");
      out.write("\n");
      out.write("                        var startEmulator = function(device) {\n");
      out.write("                            $CQ(document.body).css(\"display\",\"block\");\n");
      out.write("                            CQ_Analytics.MobileSliderUtils.injectCss(\"/libs/wcm/emulator/widgets.css\");\n");
      out.write("                            $CQ(document.body).css(\"display\",\"block\");\n");
      out.write("\n");
      out.write("                            {\n");
      out.write("                                var emulator = devicesObj[device];\n");
      out.write("                                CQ_Analytics.MobileSliderUtils.injectCss(emulator[\"contentCss\"]);\n");
      out.write("                                for(var name in devicesObj) {\n");
      out.write("                                    CQ_Analytics.MobileSliderUtils.injectCss(devicesObj[name][\"css\"]);\n");
      out.write("                                }\n");
      out.write("                            }\n");
      out.write("\n");
      out.write("                            if (!CQ_Analytics.SurferInfoMgr.slider.emulatorMgr) {\n");
      out.write("                                $CQ.getScript(_g.shared.HTTP.externalize(\"/libs/wcm/emulator/widgets.js\"), function() {\n");
      out.write("                                    try {\n");
      out.write("                                        var config = {\n");
      out.write("                                            defaultEmulator: device,\n");
      out.write("                                            contentCssId: \"");
      out.print(HTML_ID_CONTENT_CSS);
      out.write("\",\n");
      out.write("                                            showCarousel: true,\n");
      out.write("                                            emulatorConfigs: ");
      out.print(getEmulatorsConfig(deviceGroups, request.getContextPath()));
      out.write("\n");
      out.write("                                        };\n");
      out.write("\n");
      out.write("                                        if( !CQ_Analytics.SurferInfoMgr.slider.emulatorMgr ) {\n");
      out.write("                                            CQ.wcm.emulator.EmulatorManager.SKIP_GROUP_TEST = true;\n");
      out.write("\n");
      out.write("                                            var emulatorMgr = CQ.WCM.getEmulatorManager();\n");
      out.write("                                            emulatorMgr.launch(config);\n");
      out.write("                                            CQ_Analytics.SurferInfoMgr.slider.emulatorMgr = emulatorMgr;\n");
      out.write("                                        }\n");
      out.write("                                        CQ_Analytics.SurferInfoMgr.slider.emulatorMgr.switchEmulator(device);\n");
      out.write("\n");
      out.write("                                        CQ_Analytics.SurferInfoMgr.currentDevice = device;\n");
      out.write("                                        $CQ(\"#\" + MOBILE_MAIN_ID).fadeIn(speed, function() {\n");
      out.write("                                            window.setTimeout(function() {\n");
      out.write("                                                CQ.WCM.toggleEditables(true, mobilePath);\n");
      out.write("                                            }, 500);\n");
      out.write("                                        });\n");
      out.write("                                    } catch(error) {\n");
      out.write("                                        console.log(\"error while loading emulator.js\",error);\n");
      out.write("                                    }\n");
      out.write("                                });\n");
      out.write("                            } else {\n");
      out.write("                                CQ.wcm.emulator.EmulatorManager.SPECIAL_WRAPPING_ID = MOBILE_MAIN_ID;\n");
      out.write("                                CQ.wcm.emulator.EmulatorManager.SKIP_GROUP_TEST = true;\n");
      out.write("                                CQ_Analytics.SurferInfoMgr.slider.emulatorMgr.switchEmulator(device);\n");
      out.write("\n");
      out.write("                                CQ_Analytics.SurferInfoMgr.currentDevice = device;\n");
      out.write("                                $CQ(\"#\" + MOBILE_MAIN_ID).fadeIn(speed, function() {\n");
      out.write("                                    window.setTimeout(function() {\n");
      out.write("                                        CQ.WCM.toggleEditables(true, mobilePath);\n");
      out.write("                                    }, 500);\n");
      out.write("                                });\n");
      out.write("                            }\n");
      out.write("                        };\n");
      out.write("\n");
      out.write("                        var stopEmulator = function() {\n");
      out.write("                            if( CQ_Analytics.SurferInfoMgr.slider.emulatorMgr) {\n");
      out.write("                                $CQ(\"#cq-emulator-toolbar\").slideUp(speed, function() {\n");
      out.write("                                    var device = CQ_Analytics.SurferInfoMgr.currentDevice;\n");
      out.write("\n");
      out.write("                                    CQ_Analytics.SurferInfoMgr.slider.emulatorMgr.stopEmulator();\n");
      out.write("\n");
      out.write("                                    CQ_Analytics.MobileSliderUtils.removeCss(\"/libs/wcm/emulator/widgets.css\");\n");
      out.write("\n");
      out.write("                                    var emulator = devicesObj[device];\n");
      out.write("                                    CQ_Analytics.MobileSliderUtils.removeCss(emulator[\"contentCss\"]);\n");
      out.write("                                    for(var name in devicesObj) {\n");
      out.write("                                        CQ_Analytics.MobileSliderUtils.removeCss(devicesObj[name][\"css\"]);\n");
      out.write("                                    }\n");
      out.write("\n");
      out.write("                                    CQ_Analytics.SurferInfoMgr.currentDevice = \"Desktop\";\n");
      out.write("                                });\n");
      out.write("                            }\n");
      out.write("\n");
      out.write("                        };\n");
      out.write("\n");
      out.write("                        var restore = function() {\n");
      out.write("                            window.CQURLInfo.requestPath = \"");
      out.print(currentPage.getPath());
      out.write("\";\n");
      out.write("                            window.CQURLInfo.selectors = window.CQURLInfo.selectors_ori;\n");
      out.write("                            CQ.WCM.toggleEditables();\n");
      out.write("                            stopEmulator();\n");
      out.write("                            $CQ(\"#\" + MOBILE_MAIN_ID).fadeOut(speed, function() {\n");
      out.write("                                CQ_Analytics.MobileSliderUtils.switchToDesktop(app);\n");
      out.write("\n");
      out.write("                                $CQ(\"#\" + MOBILE_MAIN_ID).attr(\"id\",MOBILE_MAIN_ID + \"_mobile\");\n");
      out.write("                                $CQ(\"#\"+DESKTOP_MAIN_ID+\"_excluded\").attr(\"id\",DESKTOP_MAIN_ID);\n");
      out.write("\n");
      out.write("                                $CQ(\"#\"+MOBILE_MAIN_ID+\"_mobile\").appendTo(document.body);\n");
      out.write("                                $CQ(\"#\" + DESKTOP_MAIN_ID).fadeIn(speed, function() {\n");
      out.write("                                    CQ.WCM.getSidekick().loadContent(path);\n");
      out.write("                                    window.setTimeout(function() {\n");
      out.write("                                        CQ.WCM.toggleEditables(true, path);\n");
      out.write("                                    }, 500);\n");
      out.write("                                });\n");
      out.write("                            });\n");
      out.write("                        };\n");
      out.write("\n");
      out.write("                        var inject = function(device) {\n");
      out.write("                            var emulator = devicesObj[device];\n");
      out.write("\n");
      out.write("                            if( !window.CQURLInfo ) {\n");
      out.write("                                window.CQURLInfo = {\n");
      out.write("                                    requestPath: \"");
      out.print(currentPage.getPath());
      out.write("\",\n");
      out.write("                                    selectors: [emulator[\"group\"]]\n");
      out.write("                                };\n");
      out.write("                            }\n");
      out.write("                            window.CQURLInfo.requestPath = \"");
      out.print(mobilePage.getPath());
      out.write("\";\n");
      out.write("                            window.CQURLInfo.selectors_ori = window.CQURLInfo.selectors;\n");
      out.write("                            window.CQURLInfo.selectors = [emulator[\"group\"]];\n");
      out.write("\n");
      out.write("                            $CQ(document.body).children().addClass(\"excluded\");\n");
      out.write("\n");
      out.write("                            if( !CQ_Analytics.SurferInfoMgr.slider.emulatorMgr) {\n");
      out.write("                                var req = CQ.shared.HTTP.get(mobilePath + \".\" + emulator[\"group\"] + \".html\");\n");
      out.write("                                var t = req.responseText;\n");
      out.write("                                t = t.substring(t.indexOf(\"<div id=\\\"\"+MOBILE_MAIN_ID+\"\\\"\"), t.indexOf(\"</body>\"));\n");
      out.write("                                t = t.replace(\"id=\\\"\"+MOBILE_MAIN_ID+\"\\\"\",\"id=\\\"\"+MOBILE_MAIN_ID+\"_mobile\\\" style=\\\"display: none\\\"\");\n");
      out.write("                                //var toInject = $CQ(t);\n");
      out.write("\n");
      out.write("                                //toInject.hide();\n");
      out.write("                                var n = document.createElement(\"div\");\n");
      out.write("                                n.innerHTML = t;\n");
      out.write("                                var toInject = $CQ($CQ(\"body\")[0].insertBefore(n,$CQ(\"body\")[0].firstChild));\n");
      out.write("                                CQ.DOM.executeScripts(CQ.Ext.get(n));\n");
      out.write("                                var parent = toInject.parent();\n");
      out.write("                                var toRemove = toInject;\n");
      out.write("                                toInject = toInject.children().prependTo(parent);\n");
      out.write("                                toInject.addClass(\"injected\");\n");
      out.write("                                toRemove.remove();\n");
      out.write("\n");
      out.write("                            }\n");
      out.write("\n");
      out.write("                            CQ.WCM.toggleEditables();\n");
      out.write("                            $CQ(\"#\"+DESKTOP_MAIN_ID).fadeOut(speed, function() {\n");
      out.write("                                $CQ(\"#\"+DESKTOP_MAIN_ID).attr(\"id\",DESKTOP_MAIN_ID + \"_excluded\");\n");
      out.write("                                $CQ(\"#\"+MOBILE_MAIN_ID+\"_mobile\").attr(\"id\",MOBILE_MAIN_ID);\n");
      out.write("\n");
      out.write("                                window.setTimeout(function() {\n");
      out.write("                                    CQ_Analytics.MobileSliderUtils.switchToMobile(app);\n");
      out.write("\n");
      out.write("                                    CQ.WCM.getSidekick().loadContent(mobilePath);\n");
      out.write("                                    startEmulator(device);\n");
      out.write("                                }, 200);\n");
      out.write("                            });\n");
      out.write("                        };\n");
      out.write("\n");
      out.write("                        if( toLoadId == \"Desktop\") {\n");
      out.write("                            if( CQ_Analytics.SurferInfoMgr.currentDevice != \"Desktop\") {\n");
      out.write("                                CQ_Analytics.SurferInfoMgr.reset();\n");
      out.write("                                if( CQ_Analytics.SurferInfoMgr.slider.emulatorMgr ) {\n");
      out.write("                                    restore();\n");
      out.write("                                }\n");
      out.write("                            }\n");
      out.write("                        } else {\n");
      out.write("                            //TODO improve with more emulator data\n");
      out.write("                            CQ_Analytics.SurferInfoMgr.setProperty(\"device\", toLoadId);\n");
      out.write("                            CQ_Analytics.SurferInfoMgr.setProperty(\"browserFamily\", \"\");\n");
      out.write("                            CQ_Analytics.SurferInfoMgr.setProperty(\"browserVersion\", \"\");\n");
      out.write("                            CQ_Analytics.SurferInfoMgr.setProperty(\"OS\", \"\");\n");
      out.write("                            var emulator = devicesObj[toLoadId];\n");
      out.write("                            CQ_Analytics.SurferInfoMgr.setProperty(\"thumbnail\", emulator[\"picturePath\"]);\n");
      out.write("                            if( !CQ_Analytics.SurferInfoMgr.slider.emulatorMgr ) {\n");
      out.write("                                inject(toLoadId);\n");
      out.write("                            } else {\n");
      out.write("                                startEmulator(toLoadId);\n");
      out.write("                            }\n");
      out.write("\n");
      out.write("                        }\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("                    CQ_Analytics.SurferInfoMgr.slider.getCurrentValue = function() {\n");
      out.write("                        return CQ_Analytics.SurferInfoMgr.getProperty(\"device\");\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("                    CQ_Analytics.SurferInfoMgr.slider.show();\n");
      out.write("\n");
      out.write("                };\n");
      out.write("\n");
      out.write("                var handleSliderSI = function(event) {\n");
      out.write("                    if( !CQ_Analytics.SurferInfoMgr.slider) {\n");
      out.write("                        createSliderSI.call(this, event, true);\n");
      out.write("                    } else {\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.slider.show();\n");
      out.write("                    }\n");
      out.write("                    if( CQ_Analytics.ProfileDataMgr.slider ) CQ_Analytics.ProfileDataMgr.slider.hide();\n");
      out.write("                    event.stopPropagation();\n");
      out.write("                };\n");
      out.write("\n");
      out.write("                if( CQ_Analytics.MobileSliderUtils.CONFIG[app]) {\n");
      out.write("                    $CQ(\".cq-cc-surferinfo-thumbnail\").parent().parent().bind(\"click\", handleSliderSI);\n");
      out.write("                }\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            });\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("</script>");
 }
    }

    //case of a mobile page, SurerInfo should be updated when emulator is changed

      out.write("<script type=\"text/javascript\">\n");
      out.write("    $CQ(function() {\n");
      out.write("        CQ_Analytics.ClientContextUI.onLoad(function() {\n");
      out.write("            if( window.CQ && window.CQ.WCM) {\n");
      out.write("                var emulMgr = CQ.WCM.getEmulatorManager();\n");
      out.write("                if( emulMgr ) {\n");
      out.write("                    emulMgr.on(\"start\", function(emulator) {\n");
      out.write("                        //TODO improve with more emulator data\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.setProperty(\"device\", emulator[\"name\"]);\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.setProperty(\"browserFamily\", \"\");\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.setProperty(\"browserVersion\", \"\");\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.setProperty(\"OS\", \"\");\n");
      out.write("                        CQ_Analytics.SurferInfoMgr.setProperty(\"thumbnail\",\n");
      out.write("                                CQ.shared.HTTP.externalize(\n");
      out.write("                                        \"/libs/wcm/mobile/components/emulators/\" + emulator[\"name\"] + \"/thumbnail.png\"));\n");
      out.write("                    });\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("</script>\n");

}
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
