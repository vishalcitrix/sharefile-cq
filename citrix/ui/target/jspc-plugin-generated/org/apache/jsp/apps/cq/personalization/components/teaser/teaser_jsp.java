package org.apache.jsp.apps.cq.personalization.components.teaser;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.StringWriter;
import java.util.Iterator;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import com.day.cq.commons.JSONItem;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.core.stats.PageViewStatistics;
import com.day.text.Text;
import java.util.ResourceBundle;
import com.day.cq.i18n.I18n;
import com.day.cq.personalization.ClientContextUtil;
import com.day.cq.personalization.TeaserUtils;
import com.siteworx.rewrite.transformer.ContextRootTransformer;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class teaser_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

 
	private static final String HEIGHT_PROPERTY = "height"; 


    static com.day.cq.commons.Filter<Page> TEASER_FILTER = new com.day.cq.commons.Filter<Page>() {
        public boolean includes(Page page) {
            if (page == null) {
                return false;
            } else {
                Resource r = page.getContentResource();
                return true;
            }
        }
    };

    static com.day.cq.commons.Filter<Page> CAMPAIGN_FILTER = new com.day.cq.commons.Filter<Page>() {
        public boolean includes(Page page) {
            if (page == null) {
                return false;
            } else {
                Resource r = page.getContentResource();
                return r != null && ResourceUtil.isA(r, "cq/personalization/components/campaignpage");
            }
        }
    };

    class Teaser implements JSONItem {
        public String path;
        public String rewrittenPath;
        public String name;
        public String title;
        public String[] segments;
        public Tag[] tags;
        public String campainName;
        public String thumbnail;

        Teaser(String path, String rewrittenPath, String name, String title, String[] segments, Tag[] tags, String campainName, String thumbnail) {
            this.name = name;
            this.path = path;
            this.rewrittenPath = rewrittenPath;
            this.title = title;
            this.segments = segments;
            this.tags = tags;
            this.campainName = campainName;
            this.thumbnail = thumbnail;

            if(this.segments == null) {
                this.segments = new String[]{};
            }

            if(this.tags == null) {
                this.tags = new Tag[]{};
            }

        }

        public String getId() {
            return campainName + "_" + name;
        }


        public void write(JSONWriter out) throws JSONException {
            out.object();
            out.key("path").value(rewrittenPath);
            out.key("name").value(name);
            out.key("title").value(title);
            out.key("campainName").value(campainName);
            out.key("thumbnail").value(thumbnail);
            out.key("id").value(getId());
            out.key("segments");
            out.array();
            for(String s: segments) {
                out.value(s);
            }
            out.endArray();
            out.key("tags");
            out.array();
            for(Tag t: tags) {
                out.object();
                out.key("name").value(t.getName());
                out.key("title").value(t.getTitle());
                out.key("titlePath").value(t.getTitlePath());
                out.key("path").value(t.getPath());
                out.key("tagID").value(t.getTagID());
                out.endObject();
            }
            out.endArray();
            out.endObject();
        }
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_005ftest;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fif_005ftest = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fif_005ftest.release();
    _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_005fset_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fset_005f0.setParent(null);
      // /apps/cq/personalization/components/teaser/teaser.jsp(43,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setVar("height");
      // /apps/cq/personalization/components/teaser/teaser.jsp(43,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setValue( properties.get(HEIGHT_PROPERTY, 0) );
      int _jspx_eval_c_005fset_005f0 = _jspx_th_c_005fset_005f0.doStartTag();
      if (_jspx_th_c_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
      out.write('\n');
      out.write('\n');

    ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    final ResourceBundle resourceBundle = slingRequest.getResourceBundle(null);
    final I18n i18n = new I18n(resourceBundle);  
    
    final String basePath = properties.get("campaignpath", "/content/campaigns");
    final Page campaignPage = pageManager.getPage(basePath);
    boolean validBasePath = CAMPAIGN_FILTER.includes(campaignPage);
    if (validBasePath) {
        final String strategyPath = properties.get("strategyPath", "");
        String strategy = "";
        if ( strategyPath.length() > 0 ) {
            strategy = Text.getName(strategyPath);
            strategy = strategy.replaceAll(".js","");
        }
        
	    final String targetDivId = ClientContextUtil.getId(resource.getPath());
	
	    final PageViewStatistics pwSvc = sling.getService(PageViewStatistics.class);
	    String trackingURLStr = null;
	    if (pwSvc!=null && pwSvc.getTrackingURI() != null) {
	        trackingURLStr = pwSvc.getTrackingURI().toString();
	    }
	
	    final String TEASER_SUFFIX = "/_jcr_content/par.html";
	    Teaser defaultTeaser = null;

      out.write('\n');
      out.write('\n');

	StringBuffer allTeasers = new  StringBuffer();
	Iterator<Page> teasers = campaignPage.listChildren(TEASER_FILTER);
	boolean first = true;
	while (teasers.hasNext()) {
	    Page teaser = teasers.next();
	    if(teaser.isValid()) {
	        ValueMap teaserProperties = teaser.getProperties();
	        String imagePath = TeaserUtils.getImage(teaser);
	
	           String rewrittenPath = teaser.getPath();
	           String transformed = null;
	           
	           transformed = transformer.transform(rewrittenPath);
	           if (transformed != null)
	               rewrittenPath = transformed;
	
	        Teaser t = new Teaser(
	                teaser.getPath(),
	                   rewrittenPath,
	                teaser.getName(),
	                teaser.getTitle(),
	                teaserProperties.get("cq:segments",String[].class),
	                teaser.getTags(),
	                campaignPage.getName(),
	                imagePath);
	        StringWriter sw = new StringWriter();
	        JSONWriter json = new JSONWriter(sw);
	        t.write(json);
	        if ( ! first ) {
	            allTeasers.append(",");
	        }
	        allTeasers.append(sw.toString());
	        first = false;
	
	        //last teaser with no segment and no tag is the default teaser.
	        if( t.segments.length == 0 && t.tags.length == 0) {
	            defaultTeaser = t;
	        }
	    }
	}

      out.write("\n");
      out.write("\t\t<script type=\"text/javascript\">    \n");
      out.write("            if(!document.attachEvent || typeof document.attachEvent === \"undefined\"){\n");
      out.write("                document.addEventListener(\"DOMContentLoaded\", function(event) {\n");
      out.write("    \t\t\t\tinitializeTeaserLoader([");
      out.print(allTeasers);
      out.write("], \"");
      out.print(strategy);
      out.write("\", \"");
      out.print(targetDivId);
      out.write("\", \"");
      out.print((WCMMode.fromRequest(request) == WCMMode.EDIT));
      out.write("\", \"");
      out.print(trackingURLStr);
      out.write("\", \"");
      out.print(resource.getPath());
      out.write("\");\n");
      out.write("    \t        });\n");
      out.write("            } else {\n");
      out.write("                // IE8\n");
      out.write("                document.attachEvent(\"onreadystatechange\", function() {\n");
      out.write("                   if(document.readyState === \"complete\") {\n");
      out.write("                      initializeTeaserLoader([");
      out.print(allTeasers);
      out.write("], \"");
      out.print(strategy);
      out.write("\", \"");
      out.print(targetDivId);
      out.write("\", \"");
      out.print((WCMMode.fromRequest(request) == WCMMode.EDIT));
      out.write("\", \"");
      out.print(trackingURLStr);
      out.write("\", \"");
      out.print(resource.getPath());
      out.write("\");\n");
      out.write("                   }\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("    \t</script>\n");
      out.write("    \t\n");
      out.write("    \t<div id=\"");
      out.print(targetDivId);
      out.write("\" class=\"campaign campaign-");
      out.print(campaignPage.getName());
      out.write("\" style=\"");
      if (_jspx_meth_c_005fif_005f0(_jspx_page_context))
        return;
      out.write('"');
      out.write('>');
      out.write('\n');

			if( defaultTeaser != null) {
                //include a default teaser into a noscript tag in case of no JS (SEO...)
                StringWriter defaultHtml = new StringWriter();
                pageContext.pushBody(defaultHtml);

      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t");
      //  sling:include
      org.apache.sling.scripting.jsp.taglib.IncludeTagHandler _jspx_th_sling_005finclude_005f0 = (org.apache.sling.scripting.jsp.taglib.IncludeTagHandler) _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody.get(org.apache.sling.scripting.jsp.taglib.IncludeTagHandler.class);
      _jspx_th_sling_005finclude_005f0.setPageContext(_jspx_page_context);
      _jspx_th_sling_005finclude_005f0.setParent(null);
      // /apps/cq/personalization/components/teaser/teaser.jsp(137,4) name = replaceSelectors type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_sling_005finclude_005f0.setReplaceSelectors("noscript");
      // /apps/cq/personalization/components/teaser/teaser.jsp(137,4) name = path type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_sling_005finclude_005f0.setPath( defaultTeaser.path+TEASER_SUFFIX );
      int _jspx_eval_sling_005finclude_005f0 = _jspx_th_sling_005finclude_005f0.doStartTag();
      if (_jspx_th_sling_005finclude_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody.reuse(_jspx_th_sling_005finclude_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fsling_005finclude_005freplaceSelectors_005fpath_005fnobody.reuse(_jspx_th_sling_005finclude_005f0);
      out.write('\n');

                pageContext.popBody();

      out.write("\t\t\t\t<noscript>");
      out.print(defaultHtml);
      out.write("</noscript>\n");
      out.write("\n");

            }

      out.write("\n");
      out.write("\t\t</div>\n");

	    } else if (WCMMode.fromRequest(request) == WCMMode.EDIT) {

      out.write("\t\t\t<style type=\"text/css\">\n");
      out.write("\t            .cq-teaser-placeholder-off {\n");
      out.write("\t                display: none;\n");
      out.write("\t            }\n");
      out.write("\t        </style>\n");
      out.write("        <h3 class=\"cq-texthint-placeholder\">");
      out.print(i18n.get("Campaign path does not reference a campaign") );
      out.write("</h3>\n");
      out.write("        <img src=\"/libs/cq/ui/resources/0.gif\" class=\"cq-teaser-placeholder\" alt=\"\">\n");

    }

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f0.setParent(null);
    // /apps/cq/personalization/components/teaser/teaser.jsp(130,93) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f0.setTest(((java.lang.Boolean) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${not empty height && height > 0}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
    if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("min-height: ");
        out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${height}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write('p');
        out.write('x');
        out.write(';');
        int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
    return false;
  }
}
