package org.apache.jsp.apps.brightcove.components.tools.brightcoveadmin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.wcm.api.components.EditConfig;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.replication.Replicator;
import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentConfig;
import com.day.cq.widget.HtmlLibraryManager;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.api.components.Toolbar;
import com.day.cq.replication.ReplicationQueue;
import com.day.cq.replication.AgentManager;
import java.util.Iterator;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class brightcoveadmin_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;













      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here



    AgentManager agentMgr = sling.getService(AgentManager.class);


      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; utf-8\" />\n");
      out.write("    ");
      if (_jspx_meth_cq_005fincludeClientLib_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    ");
      if (_jspx_meth_cq_005fincludeClientLib_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_cq_005fincludeClientLib_005f2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    ");
      if (_jspx_meth_cq_005fincludeClientLib_005f3(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_cq_005fincludeClientLib_005f4(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    <title>VideoManager - Media API Sample Application</title>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("<div id=\"header\"><a href=\"/welcome\" class=\"home\"></a></div>\n");
      out.write("<div id=\"brightcove\">\n");
      out.write("    <div class=\"navbar\">\n");
      out.write("      <div class=\"navbar-inner\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("          <ul class=\"nav\">\n");
      out.write("              <li class=\"active\">\n");
      out.write("                <a id=\"allVideos\" onclick='$(this).parent(\"li\").parent(\"ul\").children(\"li\").attr(\"class\",\"\");$(this).parent(\"li\").attr(\"class\",\"active\");Load(getAllVideosURL());'>All Videos</a>\n");
      out.write("              </li>\n");
      out.write("              <li><a id=\"allPlaylists\" onclick='$(this).parent(\"li\").parent(\"ul\").children(\"li\").attr(\"class\",\"\");$(this).parent(\"li\").attr(\"class\",\"active\");Load(getAllPlaylistsURL())'>All Playlists</a></li>              \n");
      out.write("            </ul>\n");
      out.write("            <div class=\"pull-right\" ><img src=\"/etc/designs/brightcove/images/logo_brightcove.png\" alt=\"Brightcove Console\"></div>\n");
      out.write("            \n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div id=\"divConsole\">\n");
      out.write("<table width=\"100%\" class=\"tblConsole\" cellspacing=\"0\" callpadding=\"0\" border=\"0\">\n");
      out.write("<tr>\n");
      out.write("\n");
      out.write("<!-- Start center column -->\n");
      out.write("<td width=\"75%\" valign=\"top\" >\n");
      out.write("<table id=\"listTable\" width=\"100%\" cellspacing=\"0\" callpadding=\"0\">\n");
      out.write("<tr class=\"trCenterHeader\"><td id=\"tdOne\">\n");
      out.write("    <div id=\"headTitle\" style=\"font-weight:bold\">All Videos</div><p>\n");
      out.write("    <div id=\"divVideoCount\" style=\"float:left\"></div>\n");
      out.write("    <div id=\"searchDiv\" style=\"float:right;padding:5px\">\n");
      out.write("        <input id=\"search\" type=\"text\" value=\"Search Video\" onClick=\"this.value=''\" >\n");
      out.write("        <!--Store the search query in searchBut.value so we can use it as the title of the page once the results are returned.  See searchVideoCallBack -->\n");
      out.write("        <button id=\"searchBut\" onClick=\"searchVal=document.getElementById('search').value;Load(searchVideoURL())\">Search</button>\n");
      out.write("    </div>\n");
      out.write("</td></tr>\n");
      out.write("    <tr><td id=\"tdTwo\">\n");
      out.write("    <div name=\"butDiv\" class=\"butDiv\">\n");
      out.write("        <span name=\"buttonRow\"><!--The buttons in buttonRow are hidden in playlist view -->\n");
      out.write("            <button id=\"delButton\" class=\"delButton\" onClick=\"openBox('delConfPop')\">Delete Checked</button> \n");
      out.write("            <button id=\"uplButton\" class=\"uplButton\" onClick=\"openBox('uploadDiv')\">Upload Video</button> \n");
      out.write("            <button id=\"newplstButton\" onClick=\"createPlaylistBox()\">Create Playlist</button> \n");
      out.write("        </span>\n");
      out.write("        <button class=\"btn\" name=\"delFromPlstButton\" onClick=\"openBox('modPlstPop')\"> Remove From Playlist</button>\n");
      out.write("    </div>\n");
      out.write("    <div name=\"pageDiv\" class=\"pageDiv\">\n");
      out.write("        Page Number: <select name=\"selPageN\" onchange=\"changePage(this.selectedIndex)\">\n");
      out.write("    </div>\n");
      out.write("    </td>\n");
      out.write("    </tr>\n");
      out.write("    <tr><td>\n");
      out.write("    \n");
      out.write("        <!-- Start Main list -->\n");
      out.write("        <!-- buildMainVideoList, buildPlaylistList and showPlaylist populate this section, -->\n");
      out.write("        <table id=\"tblMainList\" cellpadding=\"3\" cellspacing=\"0\">\n");
      out.write("        <thead>\n");
      out.write("            <tr id=\"trHeader\"> \n");
      out.write("                <th id=\"checkCol\" class=\"tdMainTableHead\" style=\"border-right:0px;color:#e5e5e5;font-size:1px\">\n");
      out.write("                    <input type=\"checkbox\" onclick=\"toggleSelect(this)\" id=\"checkToggle\"/>?\n");
      out.write("                </th>\n");
      out.write("                <th id=\"nameCol\" class=\"tdMainTableHead\" style=\"border-left:0px\">\n");
      out.write("                    Video Name\n");
      out.write("                </th>\n");
      out.write("                <th id=\"lastUpdated\" class=\"tdMainTableHead\">\n");
      out.write("                    Last Updated\n");
      out.write("                </th>\n");
      out.write("                <th class=\"tdMainTableHead\">\n");
      out.write("                    ID</th>\n");
      out.write("                <th class=\"tdMainTableHead\">\n");
      out.write("                    Reference Id\n");
      out.write("                </th>\n");
      out.write("            </tr>\n");
      out.write("        </thead>\n");
      out.write("        <tbody id=\"tbData\">\n");
      out.write("        </tbody>\n");
      out.write("        </table>\n");
      out.write("        </td></tr>\n");
      out.write("    <tr><td>\n");
      out.write("    <div name=\"butDiv\" class=\"butDiv\">\n");
      out.write("        <span name=\"buttonRow\"><!--The buttons in buttonRow are hidden in playlist view -->\n");
      out.write("            <button id=\"delButton\" class=\"delButton\" onClick=\"openBox('delConfPop')\">Delete Checked</button> \n");
      out.write("            <button id=\"uplButton\" class=\"uplButton\" onClick=\"openBox('uploadDiv')\">Upload Video</button>\n");
      out.write("            <button id=\"newplstButton\" onClick=\"createPlaylistBox()\">Create Playlist</button>\n");
      out.write("        </span>\n");
      out.write("        <button class=\"btn\" name=\"delFromPlstButton\" onClick=\"openBox('modPlstPop')\"> Remove From Playlist</button>\n");
      out.write("    </div>\n");
      out.write("    <div name=\"pageDiv\" class=\"pageDiv\">\n");
      out.write("        Page Number: <select name=\"selPageN\" onchange=\"changePage(this.selectedIndex)\">\n");
      out.write("    </div>\n");
      out.write("    </td></tr>\n");
      out.write("    </table>\n");
      out.write("</td>\n");
      out.write("\n");
      out.write("<!-- start right metadata column, fields filled in by showMetaData() -->\n");
      out.write("<!--Element Id's have the meta. prefix followed by the actual name of the Video object field.  This allows the fields to be easily compared.  See  metaSubmit()  -->\n");
      out.write("<td id=\"tdMeta\" valign=\"top\" class=\"tdMetadata\">\n");
      out.write("<br/>\n");
      out.write("<div id=\"divMeta.name\" style=\"font-weight:bold\"></div><br/>\n");
      out.write("Last Updated: <div id=\"divMeta.lastModifiedDate\"></div>\n");
      out.write("<hr/>\n");
      out.write("<div id=\"divMeta.previewDiv\" >\n");
      out.write("<ul class=\"thumbnails\">\n");
      out.write("        <li class=\"span3\">\n");
      out.write("          <div class=\"thumbnail\">\n");
      out.write("            <img id=\"divMeta.videoStillURL\" alt=\" No Image \" style=\"width:260px\" />\n");
      out.write("             <a onClick=\"changeVideoImage(document.getElementById('divMeta.id').innerHTML)\" href=\"#\" class=\"btn\">Change Video Still Image</a>\n");
      out.write("          </div>\n");
      out.write("        </li>\n");
      out.write("        <li class=\"span2\">\n");
      out.write("          <div class=\"thumbnail\">\n");
      out.write("            <img id=\"divMeta.thumbnailURL\" alt=\" No Thumbnail \" style=\"width:160px\" />\n");
      out.write("            <a onClick=\"changeImage(document.getElementById('divMeta.id').innerHTML)\" href=\"#\" class=\"btn\">Change Thumbnail</a>\n");
      out.write("          </div>\n");
      out.write("        </li>\n");
      out.write("        \n");
      out.write("      </ul>\n");
      out.write("<p><a href=\"#\"  onClick=\"doPreview(document.getElementById('divMeta.id').innerHTML)\" class=\"btn btn-primary\">Video Preview</a></p>\n");
      out.write("</div><br/>\n");
      out.write("Duration: <div id=\"divMeta.length\"></div><br/>\n");
      out.write("Video ID: <div id=\"divMeta.id\"></div><br/>\n");
      out.write("<hr>\n");
      out.write("Short Description: <div id=\"divMeta.shortDescription\"></div><br/>\n");
      out.write("Tags: <div id=\"divMeta.tags\"></div><br/>\n");
      out.write("Related Link: <br/><a id=\"divMeta.linkURL\" ></a><br/><br/>\n");
      out.write("<div style=\"display:none\" id=\"divMeta.linkText\"></div>\n");
      out.write("Economics: <div id=\"divMeta.economics\"></div><br/>\n");
      out.write("Date Published: <div id=\"divMeta.publishedDate\"></div><br/>\n");
      out.write("Reference ID: <div id=\"divMeta.referenceId\"></div><br/>\n");
      out.write("<center><button id=\"bmetaEdit\" onClick=\"metaEdit()\" style=\"display:block\">Edit</button></center>\n");
      out.write("</td>\n");
      out.write("</tr>\n");
      out.write("</table>\n");
      out.write("<div id=\"loading\" class=\"alert\">\n");
      out.write("  <strong>Loading!</strong> Please wait while your page loads.\n");
      out.write("</div>\n");
      out.write("<!-- Dimmed \"Screen\" over content behind overlays -->\n");
      out.write("<div id=\"screen\" style=\"display:none\"></div>\n");
      out.write("\n");
      out.write("<!-- Edit Metadata Overlay -->\n");
      out.write("<div id=\"metaEditPop\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("<form id=\"metaEditForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("<br/><center><span class=\"title\">Edit Metadata</span><br/><br/>\n");
      out.write("<table>\n");
      out.write("<!--the div below creates a horizontal line.  it is repeated throughout this page. -->\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<tr><td>Title:</td><td><input type=\"text\" name=\"meta.name\" id=\"meta.name\"/></td></tr>\n");
      out.write("<tr><td>Last Updated:</td><td id=\"meta.lastModifiedDate\"></td></tr>\n");
      out.write("<tr><td>Preview</td><td style=\"cursor:pointer\" id=\"meta.preview\" onClick=\"doPreview(this.value)\">Click Here to Preview</td></tr>\n");
      out.write("<tr><td>Duration:</td><td id=\"meta.length\"></td></tr>\n");
      out.write("<tr><td>Video ID:</td><td name=\"tdmeta.id\" id=\"tdmeta.id\"></td></tr>\n");
      out.write("<tr><td>Short Description:</td><td><input type=\"text\" name=\"meta.shortDescription\" id=\"meta.shortDescription\" maxLength=\"256\"/></td></tr>\n");
      out.write("<tr><td>Tags:</td><td><input type=\"text\" name=\"meta.tags\" id=\"meta.tags\"/></td></tr>\n");
      out.write("<tr><td>Related Link URL:</td><td><input type=\"text\" name=\"meta.linkURL\" id=\"meta.linkURL\"/></td></tr>\n");
      out.write("<tr><td>Related Link Text:</td><td><input type=\"text\" name=\"meta.linkText\" id=\"meta.linkText\"/></td></tr>\n");
      out.write("<tr><td>Economics:</td><td><select name=\"meta.economics\" id=\"meta.economics\" style=\"width:100%\">\n");
      out.write("<option value=\"AD_SUPPORTED\">Ad Enabled</option><option value=\"FREE\">No Ads</option></select></td></tr>\n");
      out.write("<tr><td>Date Published:</td><td id=\"meta.publishedDate\"></td></tr>\n");
      out.write("<tr><td>Reference ID:</td><td><input type=\"text\" name=\"meta.referenceId\" id=\"meta.referenceId\"/></td></tr>\n");
      out.write("</table>\n");
      out.write("<div class=\"hLine\"></div><br/>\n");
      out.write("<center> \n");
      out.write("    <button type=\"button\" id=\"bmetaSubmit\" onClick=\"metaSubmit()\" >Submit</button> \n");
      out.write("    <button type=\"button\" id=\"bmetaCancel\" onClick=\"closeBox('metaEditPop', this.form)\">Cancel</button>\n");
      out.write("</center>\n");
      out.write("<!-- This section holds data processed by proxy.jsp  -->\n");
      out.write("<input type=\"hidden\" name=\"meta.id\" id=\"meta.id\">\n");
      out.write("<input type=\"hidden\" name=\"command\" value=\"update_video\">\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("<!--Upload Form-->\n");
      out.write("<div id=\"uploadDiv\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("<form id=\"uploadForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("<br/><center><span class=\"title\">Upload A New Video:</span><br/><br/>\n");
      out.write("<span class=\"subTitle\">Title and Short Description are Required</span></center>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<table>\n");
      out.write("<tr class=\"requiredFields\"><td>Title:</td><td  style=\"width:100%\"><input type=\"text\" name=\"name\" id=\"name\"/></td></tr>\n");
      out.write("<tr class=\"requiredFields\"><td>Short Description:</td><td><input type=\"text\" name=\"shortDescription\" id=\"shortDescription\" maxLength=\"256\"/></td></tr>\n");
      out.write("<tr><td>Link to Related Item:</td><td><input type=\"text\" name=\"linkURL\" id=\"linkURL\"/></td></tr>\n");
      out.write("<tr><td>Text for Related Item:</td><td><input type=\"text\" name=\"linkText\" id=\"linkText\"/></td></tr>\n");
      out.write("<tr><td>Tags:</td><td><input type=\"text\" name=\"tags\" id=\"tags\"/></td></tr>\n");
      out.write("<tr><td>Reference ID:</td><td><input type=\"text\" name=\"referenceId\"/></td></tr>\n");
      out.write("<tr><td>Long Description:</td><td style=\"width:100%\"><textarea name=\"longDescription\"  id=\"longDescription\" style=\"width:100%\"></textarea></td></tr>\n");
      out.write("<tr class=\"requiredFields\"><td>File:</td><td><input type=\"file\" name=\"filePath\" id=\"filePath\" style=\"width:100%\" />\n");
      out.write("<input type=\"hidden\" name=\"video\"/>\n");
      out.write("<input type=\"hidden\" name=\"command\"  id=\"command\" value=\"create_video\"/></td></tr>\n");
      out.write("</table>\n");
      out.write("<div class=\"hLine\"></div><br/>\n");
      out.write("<center>\n");
      out.write("    <div class=\"subTitle\">\n");
      out.write("        Delays up to 20 minutes may occur before changes are reflected\n");
      out.write("    </div><br/>\n");
      out.write("    <button type=\"button\" id=\"startUploadButton\" onClick=\"startUpload()\">Start Upload</button>   \n");
      out.write("    <button type=\"button\" id=\"cancelUploadButton\" onClick=\"closeBox('uploadDiv',this.form)\">Cancel</button>\n");
      out.write("</center>\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("<!--Upload Image-->\n");
      out.write("<div id=\"uploadImageDiv\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("<form id=\"uploadImageForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("<br/><center><span class=\"title\">Upload A New Image:</span><br/><br/>\n");
      out.write("<span class=\"subTitle\">Title and Reference ID are Required</span></center>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<table>\n");
      out.write("<tr class=\"requiredFields\"><td>Title:</td><td  style=\"width:100%\"><input type=\"text\" name=\"name\" id=\"name\"/></td></tr>\n");
      out.write("<tr><td>Reference ID:</td><td><input type=\"text\" name=\"referenceId\"/></td></tr>\n");
      out.write("<tr class=\"requiredFields\"><td>File:</td><td><input type=\"file\" name=\"filePath\" id=\"filePath\" style=\"width:100%\" />\n");
      out.write("<input type=\"hidden\" name=\"image\"/>\n");
      out.write("<input type=\"hidden\" id=\"videoidthumb\" name=\"videoidthumb\"/>\n");
      out.write("<input type=\"hidden\" name=\"command\"  id=\"command\" value=\"add_image\"/></td></tr>\n");
      out.write("</table>\n");
      out.write("<div class=\"hLine\"></div><br/>\n");
      out.write("<center>\n");
      out.write("    <div class=\"subTitle\">\n");
      out.write("        Delays up to 20 minutes may occur before changes are reflected\n");
      out.write("    </div><br/>\n");
      out.write("    <button type=\"button\" id=\"startUploadButton\" onClick=\"startImageUpload()\">Start Upload</button>   \n");
      out.write("    <button type=\"button\" id=\"cancelUploadButton\" onClick=\"closeBox('uploadImageDiv',this.form)\">Cancel</button>\n");
      out.write("</center>\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!--Upload Video Image-->\n");
      out.write("<div id=\"uploadVideoImageDiv\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("<form id=\"uploadVideoImageForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("<br/><center><span class=\"title\">Upload A New Image:</span><br/><br/>\n");
      out.write("<span class=\"subTitle\">Title and Reference ID are Required</span></center>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<table>\n");
      out.write("<tr class=\"requiredFields\"><td>Title:</td><td  style=\"width:100%\"><input type=\"text\" name=\"name\" id=\"name\"/></td></tr>\n");
      out.write("<tr><td>Reference ID:</td><td><input type=\"text\" name=\"referenceId\"/></td></tr>\n");
      out.write("<tr class=\"requiredFields\"><td>File:</td><td><input type=\"file\" name=\"filePath\" id=\"filePath\" style=\"width:100%\" />\n");
      out.write("<input type=\"hidden\" name=\"image\"/>\n");
      out.write("<input type=\"hidden\" id=\"videoidthumb\" name=\"videoidthumb\"/>\n");
      out.write("<input type=\"hidden\" name=\"command\"  id=\"command\" value=\"add_video_image\"/></td></tr>\n");
      out.write("</table>\n");
      out.write("<div class=\"hLine\"></div><br/>\n");
      out.write("<center>\n");
      out.write("    <div class=\"subTitle\">\n");
      out.write("        Delays up to 20 minutes may occur before changes are reflected\n");
      out.write("    </div><br/>\n");
      out.write("    <button type=\"button\" id=\"startUploadButton\" onClick=\"startVideoImageUpload()\">Start Upload</button>   \n");
      out.write("    <button type=\"button\" id=\"cancelUploadButton\" onClick=\"closeBox('uploadVideoImageDiv',this.form)\">Cancel</button>\n");
      out.write("</center>\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("<!--Create Playlist -->\n");
      out.write("<div id=\"createPlaylistDiv\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("    <form id=\"createPlaylistForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("        <br/><center><span class=\"title\">Create Playlist:</span><br/><br/>\n");
      out.write("        <span class=\"subTitle\">Title is Required</span></center>\n");
      out.write("        <table>\n");
      out.write("        <div class=\"hLine\"></div>\n");
      out.write("        <tr class=\"requiredFields\"><td>Title:</td><td  style=\"width:100%\"><input type=\"text\" name=\"plst.name\" id=\"plst.name\"/></tr></td>\n");
      out.write("        <tr><td>Short Description:</td><td><input type=\"text\" name=\"plst.shortDescription\" id=\"plst.shortDescription\"/></td></tr>\n");
      out.write("        <tr><td>Reference ID:</td><td><input type=\"text\" name=\"plst.referenceId\" id=\"plst.referenceId\"/></td></tr>\n");
      out.write("        <tr><td>Thumbnail URL:</td><td><input type=\"text\" name=\"plst.thumbnailURL\" id=\"plst.thumbnailURL\"/></td></tr>\n");
      out.write("        </table>\n");
      out.write("        <fieldset><legend>Videos</legend>\n");
      out.write("            <table id=\"createPlstVideoTable\">\n");
      out.write("                <tr style=\"background-color:#e5e5e5\">\n");
      out.write("                    <td class=\"tdMainTableHead\" style=\"width:100%\">Video Name</td>\n");
      out.write("                    <td class=\"tdMainTableHead\" style=\"width:40%\">ID</td>\n");
      out.write("                </tr>\n");
      out.write("            </table> \n");
      out.write("            <input type=\"hidden\" name=\"command\" value=\"create_playlist\"/>\n");
      out.write("            <input type=\"hidden\" name=\"playlist\" id=\"playlist\"/>\n");
      out.write("        </fieldset>\n");
      out.write("        <div class=\"hLine\"></div>\n");
      out.write("        <br><center>\n");
      out.write("        <button type=\"button\" id=\"createPlstSubmit\" onClick=\"createPlaylistSubmit()\">Create Playlist</button>\n");
      out.write("        <button type=\"button\" id=\"createPlstCancel\" onClick=\"$('#createPlstVideoTable').empty();closeBox('createPlaylistDiv',this.form)\">Cancel</button>\n");
      out.write("        </center>\n");
      out.write("    </form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!--Get Upload status-->\n");
      out.write("<div id='getUplStatus' style=\"display:none\" class=\"overlay\">\n");
      out.write("<center>Get Upload Status By VideoId<br><br>\n");
      out.write("VideoId: <input type=\"text\" id=\"uplStatusId\">\n");
      out.write("<br><br>\n");
      out.write("<button onClick=\"Load(getUploadStatusById())\">Submit</button> \n");
      out.write("<button type=\"button\" onClick=\"closeBox('getUplStatus')\">Cancel</button>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!--Upload Status Bar-->\n");
      out.write("<div id=\"uploadStatus\" style=\"display:none\" class=\"overlay\">\n");
      out.write("<center>Uploading...</center>\n");
      out.write("<div style=\"background-color:black;overflow:hidden;width:100%;position:relative\">\n");
      out.write("    <div id=\"progress\"></div>\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- Delete confirmation dialog -->\n");
      out.write("<div id=\"delConfPop\" style=\"display:none\" class=\"delConfPop overlay\">\n");
      out.write("<center><strong style=\"font-size:14px;\">DELETE</strong><br/>Are you sure you want to proceed?\n");
      out.write("<br><span class=\"subTitle\">Delays up to 20 minutes may occur before changes are reflected</span>\n");
      out.write("<br/><br/>\n");
      out.write("<button name=\"deleteConfBut\" onClick=\"delConfYes()\" class=\"btn\">Yes</button>\n");
      out.write("<button type=\"button\" class=\"btn\" name=\"deleteConfBut\" onClick=\"closeBox('delConfPop')\">No</button>\n");
      out.write("</center>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- Modify Playlist confirmation dialog -->\n");
      out.write("<div id=\"modPlstPop\" style=\"display:none\" class=\"delConfPop overlay\">\n");
      out.write("<center>Remove Selected From Playlist?<br/>\n");
      out.write("<span class=\"subTitle\">Are you sure you want to proceed?</span>\n");
      out.write("<br/><br/>\n");
      out.write("<form id=\"modPlstForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\">\n");
      out.write("<button onClick=\"modPlstSubmit()\">Yes</button>\n");
      out.write("<button type=\"button\" class=\"btn\" onClick=\"closeBox('modPlstPop')\">No</button></center>\n");
      out.write("<input type=\"hidden\" name=\"command\" value=\"update_playlist\"/>\n");
      out.write("<input type=\"hidden\" name=\"playlist\" id=\"playlist\"/>\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!--Player Preview  -->\n");
      out.write("<!-- This window is populated by doPreview -->\n");
      out.write("<div id=\"playerDiv\" style=\"display:none\" class=\"overlay\">\n");
      out.write("<div id=\"playerdrag\" class=\"hLine\"></div>\n");
      out.write("<div id=\"playerTitle\" style=\"text-transform:uppercase; font-weight:bold; font-size:14px;\"></div>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!--Share Video -->\n");
      out.write("<!--this functionality has been disabled (commented out), and has not been fully maintained -->\n");
      out.write("<div id=\"shareVideoDiv\" style=\"display:none\" class=\"overlay tbInput\">\n");
      out.write("<form id=\"shareVideoForm\" method=\"POST\" enctype=\"multipart/form-data\" target=\"postFrame\" >\n");
      out.write("<br/><center><span class=\"title\">Share Video</span><br/><br/>\n");
      out.write("<span class=\"subTitle\">Sharee Account Id's Should be Comma Separated </span></center>\n");
      out.write("<table>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<tr class=\"requiredFields\"><td>Sharee Account Id's:</td><td  style=\"width:100%\"><input type=\"text\" name=\"sharees\" id=\"sharees\"  style=\"width:100%\"/></tr></td>\n");
      out.write("</table>\n");
      out.write("<fieldset><legend>Video</legend>\n");
      out.write("<table id=\"shareVideoTable\">\n");
      out.write("<tr style=\"background-color:#e5e5e5\"><td class=\"tdMainTableHead\" style=\"width:100%\">Video Name</td><td class=\"tdMainTableHead\" style=\"width:40%\">ID</td></tr>\n");
      out.write("</table> \n");
      out.write("</fieldset>\n");
      out.write("<div class=\"hLine\"></div>\n");
      out.write("<br><center>\n");
      out.write("<button id=\"shareSubmit\" onClick=\"shareVidSubmit()\">Share Video</button>\n");
      out.write("<button type=\"button\" id=\"shareCancel\" onClick=\"$('shareVideoTable').empty();closeBox('shareVideoDiv',this.form)\">Cancel</button>\n");
      out.write("</center>\n");
      out.write("<input type=\"hidden\" name=\"command\" value=\"share_video\"/>\n");
      out.write("<input type=\"hidden\" name=\"data\"/>\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("<iframe id=\"postFrame\" name=\"postFrame\" style=\"width:100%;border:none;display:none;\"></iframe>\n");
      out.write("</div></div>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_cq_005fincludeClientLib_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:includeClientLib
    com.day.cq.wcm.tags.IncludeClientLibraryTag _jspx_th_cq_005fincludeClientLib_005f0 = (com.day.cq.wcm.tags.IncludeClientLibraryTag) _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody.get(com.day.cq.wcm.tags.IncludeClientLibraryTag.class);
    _jspx_th_cq_005fincludeClientLib_005f0.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fincludeClientLib_005f0.setParent(null);
    // /apps/brightcove/components/tools/brightcoveadmin/brightcoveadmin.jsp(39,4) name = categories type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005fincludeClientLib_005f0.setCategories("cq.wcm.edit");
    int _jspx_eval_cq_005fincludeClientLib_005f0 = _jspx_th_cq_005fincludeClientLib_005f0.doStartTag();
    if (_jspx_th_cq_005fincludeClientLib_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcategories_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f0);
    return false;
  }

  private boolean _jspx_meth_cq_005fincludeClientLib_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:includeClientLib
    com.day.cq.wcm.tags.IncludeClientLibraryTag _jspx_th_cq_005fincludeClientLib_005f1 = (com.day.cq.wcm.tags.IncludeClientLibraryTag) _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.get(com.day.cq.wcm.tags.IncludeClientLibraryTag.class);
    _jspx_th_cq_005fincludeClientLib_005f1.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fincludeClientLib_005f1.setParent(null);
    // /apps/brightcove/components/tools/brightcoveadmin/brightcoveadmin.jsp(41,4) name = css type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005fincludeClientLib_005f1.setCss("brc.bootstrap");
    int _jspx_eval_cq_005fincludeClientLib_005f1 = _jspx_th_cq_005fincludeClientLib_005f1.doStartTag();
    if (_jspx_th_cq_005fincludeClientLib_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f1);
    return false;
  }

  private boolean _jspx_meth_cq_005fincludeClientLib_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:includeClientLib
    com.day.cq.wcm.tags.IncludeClientLibraryTag _jspx_th_cq_005fincludeClientLib_005f2 = (com.day.cq.wcm.tags.IncludeClientLibraryTag) _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.get(com.day.cq.wcm.tags.IncludeClientLibraryTag.class);
    _jspx_th_cq_005fincludeClientLib_005f2.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fincludeClientLib_005f2.setParent(null);
    // /apps/brightcove/components/tools/brightcoveadmin/brightcoveadmin.jsp(42,4) name = css type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005fincludeClientLib_005f2.setCss("brc.brightcove-api");
    int _jspx_eval_cq_005fincludeClientLib_005f2 = _jspx_th_cq_005fincludeClientLib_005f2.doStartTag();
    if (_jspx_th_cq_005fincludeClientLib_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fcss_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f2);
    return false;
  }

  private boolean _jspx_meth_cq_005fincludeClientLib_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:includeClientLib
    com.day.cq.wcm.tags.IncludeClientLibraryTag _jspx_th_cq_005fincludeClientLib_005f3 = (com.day.cq.wcm.tags.IncludeClientLibraryTag) _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.get(com.day.cq.wcm.tags.IncludeClientLibraryTag.class);
    _jspx_th_cq_005fincludeClientLib_005f3.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fincludeClientLib_005f3.setParent(null);
    // /apps/brightcove/components/tools/brightcoveadmin/brightcoveadmin.jsp(44,4) name = js type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005fincludeClientLib_005f3.setJs("brc.bootstrap");
    int _jspx_eval_cq_005fincludeClientLib_005f3 = _jspx_th_cq_005fincludeClientLib_005f3.doStartTag();
    if (_jspx_th_cq_005fincludeClientLib_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f3);
    return false;
  }

  private boolean _jspx_meth_cq_005fincludeClientLib_005f4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:includeClientLib
    com.day.cq.wcm.tags.IncludeClientLibraryTag _jspx_th_cq_005fincludeClientLib_005f4 = (com.day.cq.wcm.tags.IncludeClientLibraryTag) _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.get(com.day.cq.wcm.tags.IncludeClientLibraryTag.class);
    _jspx_th_cq_005fincludeClientLib_005f4.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fincludeClientLib_005f4.setParent(null);
    // /apps/brightcove/components/tools/brightcoveadmin/brightcoveadmin.jsp(45,0) name = js type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005fincludeClientLib_005f4.setJs("brc.brightcove-api");
    int _jspx_eval_cq_005fincludeClientLib_005f4 = _jspx_th_cq_005fincludeClientLib_005f4.doStartTag();
    if (_jspx_th_cq_005fincludeClientLib_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fincludeClientLib_005fjs_005fnobody.reuse(_jspx_th_cq_005fincludeClientLib_005f4);
    return false;
  }
}
