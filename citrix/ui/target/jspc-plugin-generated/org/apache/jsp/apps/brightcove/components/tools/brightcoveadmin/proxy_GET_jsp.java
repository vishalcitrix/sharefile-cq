package org.apache.jsp.apps.brightcove.components.tools.brightcoveadmin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import com.siteworx.brightcove.BrightcoveConfigurationService;
import org.apache.commons.httpclient.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.util.Streams;
import java.io.InputStream;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Enumeration;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.sling.api.request.RequestParameter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.Arrays;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.*;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.*;
import com.brightcove.proserve.mediaapi.wrapper.utils.*;
import com.brightcove.proserve.mediaapi.wrapper.*;
import org.apache.sling.api.request.RequestParameter;

public final class proxy_GET_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/libs/foundation/global.jsp");
    _jspx_dependants.add("/apps/brightcove/global/configuration.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_005ftest;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fimport_005furl_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fif_005ftest = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fimport_005furl_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fif_005ftest.release();
    _005fjspx_005ftagPool_005fc_005fimport_005furl_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f1 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f1.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f1.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f1 = _jspx_th_cq_005fdefineObjects_005f1.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");

	BrightcoveConfigurationService bcService = sling.getService(BrightcoveConfigurationService.class);
	//final static String READ_TOKEN = "zwZ-MtUrfWOEx-Du4YHdTqSGsLHbjaQqyIg1-20gYJxwgwfvGvtIBA..";
	final String READ_TOKEN = bcService.getReadToken();
	//final static String WRITE_TOKEN = "zwZ-MtUrfWOEx-Du4YHdTqSGsLHbjaQqViA2SEluL1DnmisO2YYQKw..";
	final String WRITE_TOKEN = bcService.getWriteToken();


response.reset();
response.setContentType("application/json");

/****************************************************************
*  proxy.jsp -- Media API Proxy
* Takes  requests sent by client and forwards it to external API server
* to avoid cross-domain scripting issues.
*   Also forwards tokens to keep them hidden.
*****************************************************************/

/*****************************************************************
*Media API Strings go Here
*   -Fill in with your info
*       *Make sure you include the '.' at the end of the token*
******************************************************************/
final String apiReadLoc =       "http://api.brightcove.com:80/services/library";
//final String apiReadLoc =         "http://localhost:8080/services/library";
final String apiWriteLoc =      "http://api.brightcove.com:80/services/post";
//final String apiWriteLoc =        "http://localhost:8080/services/post";
final String apiReadToken =     READ_TOKEN;
//final String apiReadToken =   "riBfgveLvpQ5OS4_D9jZxXushEmUmH9WoT4dBuEskrU.";//localhost
final String apiWriteToken =    WRITE_TOKEN;
//final String apiWriteToken =  "riBfgveLvpRb-rHAkx3mBISAQXs-Q8NmphGxt0z04kE.";//localhost
 /*************************************************************
*Don't do any error checking for paramters here, just forward them along
* since the api server will check them anyway.
**************************************************************/
 /*******************************************************************************
*This list contains the names of all the write methods. It's used to check to see if a command
* sent as a GET request should be forwarded as a multipart/post request.
********************************************************************************/
 final List <String> write_methods = Arrays.asList( new String[] {"update_video", "delete_video", "get_upload_status", "create_playlist", "update_playlist", "share_video"});

 Logger logger = LoggerFactory.getLogger("Brightcove");
/**************************************************************************
* (*) bar is the all purpose utility string.  For write requests it's used to construct and
* hold the formatted JSON request. For read requests it's used to construct and 
* hold the request URL. 
*
*(*) useGet is to determine whether the request should be sent as a GET or POST request,
* since some requests that should be sent as a POST arrive as a GET.
***************************************************************************/
String bar = null;
boolean useGet = false;
String[] ids = null;
try{
    
    String command = slingRequest.getRequestParameter("command").getString();
    logger.info("Command: '" + command +"' ");
    if (write_methods.contains(command) && request.getMethod().equals("GET")){
        WriteApi wapi = new WriteApi(logger);
        switch (write_methods.indexOf(command)) {
           case 1:
               useGet = false;
               ids = slingRequest.getRequestParameter("ids").getString().split(",");
               logger.info("Deleting videos");
               Boolean cascade        = true; // Deletes even if it is in use by playlists/players
               Boolean deleteShares   = true; // Deletes if shared to child accounts
               for(String idStr : ids){
                   Long id = Long.parseLong(idStr);       
                   String  deleteResponse = wapi.DeleteVideo(apiWriteToken, id, null, cascade, deleteShares).toString();
                   logger.info(idStr+" Response from server for delete (no message is perfectly OK): '" + deleteResponse + "'.");
                   
               }
               
               break;
           
           default:
               useGet = false;
                String temp;
                //The method can't be part of the params section, so we write that out first, then the token and then loop through the rest of the parameters.
                bar = "{\"method\": \"" + command + "\", \"params\": {\"token\": \"" + apiWriteToken +"\"";
                for(Enumeration e = request.getParameterNames(); e.hasMoreElements();){
                    temp = (String) e.nextElement();
                    //don't want to include command twice
                    if(!temp.equals("command")){
                       bar += ",\""+temp+"\": \"" +request.getParameter(temp)+"\"";
                    }
                }
                bar += "}}";

                //out.print(bar);
                
                Part[] parts = {new StringPart("data", bar)};
                HttpClient client = new HttpClient();
                PostMethod postreq = new PostMethod(apiWriteLoc);
                postreq.setRequestEntity( new MultipartRequestEntity(parts, postreq.getParams()) );
                client.executeMethod(postreq);
                if(postreq.getStatusCode() == HttpStatus.SC_OK){
                    out.print(postreq.getResponseBodyAsString());
                    postreq.releaseConnection();
                }else{
                    out.print( "Post Failed, error: " + postreq.getStatusLine());
                    postreq.releaseConnection();
                }
             
        }
        
    
    /******************************************************************************
    * The last case is an incoming GET request that should be forwarded as a GET request.
    * We set useGet to true and concatenate the read token at the end of the parameter string.
    * The JSTL  below sends the entire request and sends the response from the API server
    * back to the client.
    *******************************************************************************/
    } else {
        useGet = true;
        bar = apiReadLoc + '?' + request.getQueryString() + "&token=" + apiReadToken;
        /************************************************************************************
        *If you don't want to support JSTL, the block below duplicates the functionality of the JSTL block 
        *at the bottom of this document. It might be faster to use JSTL but this hasn't been verified.
        *************************************************************************************/
        /*HttpClient client = new HttpClient();
        HttpMethod getreq = new GetMethod(bar);
        client.executeMethod(getreq);
        if(getreq.getStatusCode() == HttpStatus.SC_OK){
        out.print(getreq.getResponseBodyAsString());
        getreq.releaseConnection();
        }else{
        out.print( "Get Failed, error: " + getreq.getStatusLine());
        getreq.releaseConnection();
        }*/
    }
} catch(Exception e){
    out.write("{\"error\": \"Proxy Error, please check your tomcat logs.\", \"result\":null, \"id\": null}");
}


      //  c:if
      org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
      _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fif_005f0.setParent(null);
      // /apps/brightcove/components/tools/brightcoveadmin/proxy.GET.jsp(162,0) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fif_005f0.setTest(useGet);
      int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
      if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          //  c:import
          org.apache.taglibs.standard.tag.rt.core.ImportTag _jspx_th_c_005fimport_005f0 = (org.apache.taglibs.standard.tag.rt.core.ImportTag) _005fjspx_005ftagPool_005fc_005fimport_005furl_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.ImportTag.class);
          _jspx_th_c_005fimport_005f0.setPageContext(_jspx_page_context);
          _jspx_th_c_005fimport_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fif_005f0);
          // /apps/brightcove/components/tools/brightcoveadmin/proxy.GET.jsp(163,0) name = url type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
          _jspx_th_c_005fimport_005f0.setUrl(bar);
          int[] _jspx_push_body_count_c_005fimport_005f0 = new int[] { 0 };
          try {
            int _jspx_eval_c_005fimport_005f0 = _jspx_th_c_005fimport_005f0.doStartTag();
            if (_jspx_th_c_005fimport_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
              return;
            }
          } catch (Throwable _jspx_exception) {
            while (_jspx_push_body_count_c_005fimport_005f0[0]-- > 0)
              out = _jspx_page_context.popBody();
            _jspx_th_c_005fimport_005f0.doCatch(_jspx_exception);
          } finally {
            _jspx_th_c_005fimport_005f0.doFinally();
            _005fjspx_005ftagPool_005fc_005fimport_005furl_005fnobody.reuse(_jspx_th_c_005fimport_005f0);
          }
          int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
