package org.apache.jsp.apps.sling.servlet.errorhandler;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jackrabbit.util.Text;
import com.day.cq.wcm.api.WCMMode;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import com.siteworx.rewrite.transformer.ContextRootTransformer;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.api.scripting.SlingBindings;

public final class publisher_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!--\n");
      out.write("    Conditions:\n");
      out.write("        All     ==> Display site specific 404 page. If we can't, display a generic 404 page.\n");
      out.write("\n");
      out.write(" -->\n");
      out.write("\n");
      out.write("\n");

    Pattern sitePattern = Pattern.compile("^\\/content/.*\\/.*");
    String currentPathInfo = request.getPathInfo();
    final Matcher siteMatcher = sitePattern.matcher(currentPathInfo);
    boolean isSitePage = siteMatcher.matches() && !currentPathInfo.contains("dam");

      out.write("\n");
      out.write("\n");
      out.write("<c:set var=\"isSitePage\" value=\"");
      out.print(isSitePage);
      out.write("\"/>\n");
      out.write("<c:choose>\n");
      out.write("    <c:when test=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${isSitePage}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\n");
      out.write("        ");
      out.write("\n");
      out.write("        ");

            SlingBindings slingBindings = (SlingBindings) request.getAttribute("org.apache.sling.api.scripting.SlingBindings");
            SlingScriptHelper slingScripter = slingBindings.getSling();
            String sitePath = Text.getAbsoluteParent(currentPathInfo, 2);
            String errorPage = sitePath + "/404";
            response.setStatus(404);
            request.getRequestDispatcher(errorPage).forward(request, response);
        
      out.write("\n");
      out.write("    </c:when>\n");
      out.write("    <c:otherwise>\n");
      out.write("        ");

            response.setStatus(404);
        
      out.write("\n");
      out.write("        404: Page Not Found.\n");
      out.write("    </c:otherwise>\n");
      out.write("</c:choose>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
