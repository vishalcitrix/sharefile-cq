package org.apache.jsp.apps.brightcove.components.tools.brightcoveadmin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.Arrays;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.*;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.*;
import com.brightcove.proserve.mediaapi.wrapper.utils.*;
import com.brightcove.proserve.mediaapi.wrapper.*;
import org.apache.sling.api.request.RequestParameter;
import java.io.*;
import java.util.UUID;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import com.siteworx.brightcove.BrightcoveConfigurationService;

public final class proxy_POST_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/libs/foundation/global.jsp");
    _jspx_dependants.add("/apps/brightcove/global/configuration.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f1 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f1.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f1.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f1 = _jspx_th_cq_005fdefineObjects_005f1.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");

	BrightcoveConfigurationService bcService = sling.getService(BrightcoveConfigurationService.class);
	//final static String READ_TOKEN = "zwZ-MtUrfWOEx-Du4YHdTqSGsLHbjaQqyIg1-20gYJxwgwfvGvtIBA..";
	final String READ_TOKEN = bcService.getReadToken();
	//final static String WRITE_TOKEN = "zwZ-MtUrfWOEx-Du4YHdTqSGsLHbjaQqViA2SEluL1DnmisO2YYQKw..";
	final String WRITE_TOKEN = bcService.getWriteToken();


response.reset();
response.setContentType("application/json");
UUID uuid = new UUID(64L,64L);
String RandomID = new String(uuid.randomUUID().toString().replaceAll("-",""));

final List <String> write_methods = Arrays.asList( new String[] {"create_video", "update_video", "get_upload_status", "create_playlist", "update_playlist", "share_video","add_image","add_video_image"});
   
final String apiReadToken = READ_TOKEN;
final String apiWriteToken = WRITE_TOKEN;
String apiToken = apiReadToken;
String[] ids = null;
Logger logger = LoggerFactory.getLogger("Brightcove");

WriteApi wapi = new WriteApi(logger);
ReadApi rapi = new ReadApi(logger);
//try{
    
    if(slingRequest.getMethod().equals("POST")){
        String command = slingRequest.getRequestParameter("command").getString();
        logger.info(command + "   "+ String.valueOf(write_methods.indexOf(command)));
        if (write_methods.contains(command)) {
            apiToken = apiWriteToken;
            Long VideoId = null;
            File tempImageFile = null;
            InputStream fileImageStream;
            RequestParameter thumbnailFile = null;
            String thumbnailFilename = null;
            FileOutputStream outImageStream =null;
            byte [] imagebuf = null;
            
            
            switch (write_methods.indexOf(command)) {
                case 0:
                    File tempFile = null;
                    InputStream fileStream;
                    Video video = new Video();
                    RequestParameter videoFile = slingRequest.getRequestParameter("filePath");
                    String videoFilename = "/tmp/"+RandomID+"_"+videoFile.getFileName();
                    fileStream = videoFile.getInputStream();
                    tempFile = new File(videoFilename);
                    FileOutputStream outStream = new FileOutputStream(tempFile);
                    byte [] buf = new byte [1024];
                    for(int byLen = 0; (byLen = fileStream.read(buf, 0, 1024)) > 0;){
                        outStream.write(buf, 0, byLen);
                        //if(tempFile.length()/1000 > 2){}//maximum file size is 2gigs
                    }
                    outStream.close();
                    // Required fields
                    video.setName(request.getParameter("name"));
                    video.setShortDescription(request.getParameter("shortDescription"));
                    
                    
                    // Optional fields
                    //video.setAccountId(accountId);
                    //video.setEconomics(EconomicsEnum.FREE);
                    video.setItemState(ItemStateEnum.ACTIVE);
                    video.setLinkText(request.getParameter("linkText"));
                    video.setLinkUrl(request.getParameter("linkURL"));
                    video.setLongDescription(request.getParameter("longDescription"));
                    video.setReferenceId(request.getParameter("referenceId"));
                    //video.setStartDate(new Date((new Date()).getTime() - 30*1000*60 )); // 30 minutes ago
                      
                    // Complex fields (all optional)
                    //Date endDate = new Date();
                    //endDate.setTime(endDate.getTime() + (30*1000*60)); // 30 minutes from now
                    //video.setEndDate(endDate);
                      
                    //video.setGeoFiltered(true);
                    //List<GeoFilterCodeEnum> geoFilteredCountries = new ArrayList<GeoFilterCodeEnum>();
                    //geoFilteredCountries.add(GeoFilterCodeEnum.lookupByName("UNITED STATES"));
                    //geoFilteredCountries.add(GeoFilterCodeEnum.CA);
                    //video.setGeoFilteredCountries(geoFilteredCountries);
                    //video.setGeoFilteredExclude(false);
                    
                    if (request.getParameter("tags") != null && request.getParameter("tags").trim().length()>0) {
                            
                        List<String> tags = Arrays.asList(request.getParameter("tags").split(","));
                        video.setTags(tags);
                    
                    }
                    // Some miscellaneous fields for the Media API (not the video objects)
                    Boolean createMultipleRenditions = false;
                    Boolean preserveSourceRendition  = false;
                    Boolean h264NoProcessing         = false;
                      
                    // Image meta data
                    /*Image thumbnail  = new Image();
                    Image videoStill = new Image();
                      
                    thumbnail.setReferenceId("this is the thumbnail refid");
                    videoStill.setReferenceId("this is the video still refid");
                      
                    thumbnail.setDisplayName("this is the thumbnail");
                    videoStill.setDisplayName("this is the video still");
                      
                    thumbnail.setType(ImageTypeEnum.THUMBNAIL);
                    videoStill.setType(ImageTypeEnum.VIDEO_STILL);
                    */
                    try{
                       // Write the video
                       logger.info("Writing video to Media API");
                       Long newVideoId = wapi.CreateVideo(apiToken, video, videoFilename, TranscodeEncodeToEnum.FLV, createMultipleRenditions, preserveSourceRendition, h264NoProcessing);
                       logger.info("New video id: '" + newVideoId + "'.");
                       tempFile.delete();
                       
                       /*
                       // Delete the video
                       log.info("Deleting created video");
                       Boolean cascade        = true; // Deletes even if it is in use by playlists/players
                       Boolean deleteShares   = true; // Deletes if shared to child accounts
                       String  deleteResponse = wapi.DeleteVideo(writeToken, newVideoId, null, cascade, deleteShares);
                       log.info("Response from server for delete (no message is perfectly OK): '" + deleteResponse + "'.");
                       */
                        
                   }
                   catch(Exception e){
                       logger.error("Exception caught: '" + e + "'.");
                       
                   }
                   break;
                case 1:
                    EnumSet<VideoFieldEnum> videoFields = VideoFieldEnum.CreateEmptyEnumSet();
                    videoFields.add(VideoFieldEnum.ID);
                    videoFields.add(VideoFieldEnum.NAME);
                    videoFields.add(VideoFieldEnum.SHORTDESCRIPTION);
                    videoFields.add(VideoFieldEnum.LINKTEXT);
                    videoFields.add(VideoFieldEnum.LINKURL);
                    videoFields.add(VideoFieldEnum.ECONOMICS);
                    videoFields.add(VideoFieldEnum.REFERENCEID);
                    videoFields.add(VideoFieldEnum.TAGS);
                    
                    Set<String> customFields = CollectionUtils.CreateEmptyStringSet();
                    Long videoId = Long.parseLong(slingRequest.getRequestParameter("meta.id").getString());
                    
                    video = rapi.FindVideoById(apiReadToken,videoId,videoFields,customFields);
                    // Required fields
                    video.setName(request.getParameter("meta.name"));
                    video.setShortDescription(request.getParameter("meta.shortDescription"));
                    
                    
                    // Optional fields
                    video.setLinkText(request.getParameter("meta.linkText"));
                    video.setLinkUrl(request.getParameter("meta.linkURL"));
                    video.setEconomics(EconomicsEnum.valueOf(request.getParameter("meta.economics")));
                    video.setReferenceId(request.getParameter("meta.referenceId"));
                    
                    //video.setGeoFiltered(null);
                    //video.setGeoFilteredCountries(null);
                    //video.setGeoFilteredExclude(null);
                    
                    if (request.getParameter("meta.tags") != null && request.getParameter("meta.tags").trim().length()>0) {
                            
                        List<String> tags = Arrays.asList(request.getParameter("meta.tags").split(","));
                        video.setTags(tags);
                    
                    }
                    
                    try{
                       // Write the video
                       logger.info("Updating video to Media API");
                       Video responseUpdate = wapi.UpdateVideo(apiToken, video);
                       logger.info("Updated video: '" + responseUpdate.getId() + "'.");
                       
                        
                   }
                   catch(Exception e){
                       logger.error("Exception caught: '" + e + "'.");
                       
                   }
                   break;
                case 3:
                       ids = slingRequest.getRequestParameter("playlist").getString().split(",");
                       
                       logger.info("Creating a Playlist");
                       Playlist playlist = new Playlist();
                       // Required fields
                       playlist.setName(request.getParameter("plst.name"));
                       playlist.setShortDescription(request.getParameter("plst.shortDescription"));
                       playlist.setPlaylistType(PlaylistTypeEnum.EXPLICIT);
                       // Optional Fields
                       if (request.getParameter("plst.referenceId") != null && request.getParameter("plst.referenceId").trim().length()>0) playlist.setReferenceId(request.getParameter("plst.referenceId"));
                       if (request.getParameter("plst.thumbnailURL") != null && request.getParameter("plst.thumbnailURL").trim().length()>0)playlist.setThumbnailUrl(request.getParameter("plst.thumbnailURL"));
                       
                       List<Long> videoIDs = new ArrayList<Long>();
                       for(String idStr : ids){
                           Long id = Long.parseLong(idStr);       
                           logger.info("Video ID: "+idStr);
                           videoIDs.add(id);
                       }
                       logger.info("Writing Playlist to Media API");
                      
                       playlist.setVideoIds(videoIDs);
                       Long newPlaylistId = wapi.CreatePlaylist(apiToken,playlist);
                       logger.info("New Playlist id: '" + newPlaylistId + "'.");
                       
                       break;
                case 6:
                	VideoId = Long.valueOf(request.getParameter("videoidthumb"));
                	thumbnailFile = slingRequest.getRequestParameter("filePath");
                    thumbnailFilename = "/tmp/"+RandomID+"_"+thumbnailFile.getFileName();
                    fileImageStream = thumbnailFile.getInputStream();
                    tempImageFile = new File(thumbnailFilename);
                    outImageStream = new FileOutputStream(tempImageFile);
                    imagebuf = new byte [1024];
                    for(int byLen = 0; (byLen = fileImageStream.read(imagebuf, 0, 1024)) > 0;){
                    	outImageStream.write(imagebuf, 0, byLen);
                        //if(tempFile.length()/1000 > 2){}//maximum file size is 2gigs
                    }
                    outImageStream.close();
                    // Required fields
                    // Image meta data
                    Image thumbnail  = new Image();
                    //Image videoStill = new Image();
                      
                    thumbnail.setReferenceId(request.getParameter("referenceId"));
                    //videoStill.setReferenceId(request.getParameter("referenceId"));
                      
                    thumbnail.setDisplayName(request.getParameter("name"));
                    //videoStill.setDisplayName(request.getParameter("name"));
                      
                    thumbnail.setType(ImageTypeEnum.THUMBNAIL);
                    //videoStill.setType(ImageTypeEnum.VIDEO_STILL);
                    
                    try{
                       // Write the image
                       Boolean resizeImage = false;
       
                       Image thumbReturn = wapi.AddImage(apiWriteToken, thumbnail, thumbnailFilename, VideoId, null, resizeImage);
				       logger.info("Thumbnail image: " + thumbReturn + ".");
				       //Image stillReturn = wapi.AddImage(apiWriteToken, videoStill, thumbnailFilename, VideoId, null, resizeImage);
				       //logger.info("Video still image: " + stillReturn + ".");
				       
				       tempImageFile.delete();
                       
                       /*
                       // Delete the video
                       log.info("Deleting created video");
                       Boolean cascade        = true; // Deletes even if it is in use by playlists/players
                       Boolean deleteShares   = true; // Deletes if shared to child accounts
                       String  deleteResponse = wapi.DeleteVideo(writeToken, newVideoId, null, cascade, deleteShares);
                       log.info("Response from server for delete (no message is perfectly OK): '" + deleteResponse + "'.");
                       */
                        
                   }
                   catch(Exception e){
                       logger.error("Exception caught: '" + e + "'.");
                       
                   }
                   break;
                case 7:
                    VideoId = Long.valueOf(request.getParameter("videoidthumb"));
                    thumbnailFile = slingRequest.getRequestParameter("filePath");
                    thumbnailFilename = "/tmp/"+RandomID+"_"+thumbnailFile.getFileName();
                    fileImageStream = thumbnailFile.getInputStream();
                    tempImageFile = new File(thumbnailFilename);
                    outImageStream = new FileOutputStream(tempImageFile);
                    imagebuf = new byte [1024];
                    for(int byLen = 0; (byLen = fileImageStream.read(imagebuf, 0, 1024)) > 0;){
                        outImageStream.write(imagebuf, 0, byLen);
                        //if(tempFile.length()/1000 > 2){}//maximum file size is 2gigs
                    }
                    outImageStream.close();
                    // Required fields
                    // Image meta data
                    Image videoStill = new Image();
                      
                    videoStill.setReferenceId(request.getParameter("referenceId"));
                      
                    videoStill.setDisplayName(request.getParameter("name"));
                      
                    videoStill.setType(ImageTypeEnum.VIDEO_STILL);
                    
                    try{
                       // Write the image
                       Boolean resizeImage = false;
       
                       Image stillReturn = wapi.AddImage(apiWriteToken, videoStill, thumbnailFilename, VideoId, null, resizeImage);
                       logger.info("Video still image: " + stillReturn + ".");
                       
                       tempImageFile.delete();
                       
                       /*
                       // Delete the video
                       log.info("Deleting created video");
                       Boolean cascade        = true; // Deletes even if it is in use by playlists/players
                       Boolean deleteShares   = true; // Deletes if shared to child accounts
                       String  deleteResponse = wapi.DeleteVideo(writeToken, newVideoId, null, cascade, deleteShares);
                       log.info("Response from server for delete (no message is perfectly OK): '" + deleteResponse + "'.");
                       */
                        
                   }
                   catch(Exception e){
                       logger.error("Exception caught: '" + e + "'.");
                       
                   }
                   break;
            }
        }
        
    } 
//} catch(Exception e){
//  out.write("{\"error\": \"Proxy Error, please check your tomcat logs.\", \"result\":null, \"id\": null}"),
   
//}


    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
