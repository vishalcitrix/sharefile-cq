package org.apache.jsp.apps.cq.personalization.components.contextstores.profiledata;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class internal_005fepilog_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<script type=\"text/javascript\">\n");
      out.write("    $CQ(function() {\n");
      out.write("        CQ_Analytics.ClientContextUI.onLoad(function() {\n");
      out.write("            //profiledata slider\n");
      out.write("            var createSliderProf = function(event, show, vertical) {\n");
      out.write("                var url = CQ.shared.HTTP.addParameter(\"/bin/security/authorizables.json\", \"limit\", 25);\n");
      out.write("                url = CQ.shared.HTTP.addParameter(url, \"hideGroups\", true);\n");
      out.write("                url = CQ.shared.HTTP.noCaching(url);\n");
      out.write("                CQ.shared.HTTP.get(url, function(options, success, response) {\n");
      out.write("                    if( !success ) return;\n");
      out.write("                    var profiles = CQ.shared.HTTP.eval(response);\n");
      out.write("                    if( !profiles || !profiles.authorizables) return;\n");
      out.write("\n");
      out.write("                    var slider = new CQ_Analytics.Slider({\n");
      out.write("                        \"vertical\": vertical,\n");
      out.write("                        \"wrap\": \"circular\",\n");
      out.write("                        \"animation\": \"slow\",\n");
      out.write("                        \"start\": start,\n");
      out.write("                        \"clazz\": \"cq-cc-slider-profiledata\",\n");
      out.write("                        \"parent\": $CQ(\".cq-cc-thumbnail-profiledata\")\n");
      out.write("                    });\n");
      out.write("                    CQ_Analytics.ProfileDataMgr.slider = slider;\n");
      out.write("\n");
      out.write("                    slider.init();\n");
      out.write("\n");
      out.write("                    var getItemHTML = function(id, imagePath, title) {\n");
      out.write("                        var url = \"/etc/designs/default/images/collab/avatar.png\";\n");
      out.write("                        if( imagePath ){\n");
      out.write("                            url = imagePath + \"/image.prof.thumbnail.80.png\";\n");
      out.write("                        }\n");
      out.write("\n");
      out.write("                        var item = $CQ(\"<li>\");\n");
      out.write("                        var img = $CQ(\"<img>\")\n");
      out.write("                                .attr(\"src\",_g.shared.HTTP.externalize(\"/etc/clientcontext/shared/thumbnail/content.png?path=\" + url))\n");
      out.write("                                .attr(\"width\", \"80\")\n");
      out.write("                                .attr(\"height\", \"80\")\n");
      out.write("                                .attr(\"alt\", title)\n");
      out.write("                                .attr(\"title\", title)\n");
      out.write("                                .attr(\"data-id\",id)\n");
      out.write("                                .bind(\"click\",function() {\n");
      out.write("                                    CQ_Analytics.ProfileDataMgr.slider.select($CQ(this).parent().attr(\"jcarouselindex\"));\n");
      out.write("                                })\n");
      out.write("                                .appendTo(item);\n");
      out.write("                        return item;\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("                    var start = 1;\n");
      out.write("                    var totalItems = 0;\n");
      out.write("                    var current = CQ_Analytics.ProfileDataMgr.getProperty(\"authorizableId\") || \"\" ;\n");
      out.write("                    for(var i = 0; i < profiles.authorizables.length; i++) {\n");
      out.write("                        var p = profiles.authorizables[i];\n");
      out.write("                        if(p[\"picturePath\"]) {\n");
      out.write("                            totalItems ++;\n");
      out.write("                            CQ_Analytics.ProfileDataMgr.slider.carousel.append(getItemHTML(p[\"rep:userId\"], p[\"picturePath\"], p[\"name\"]));\n");
      out.write("                            if( p[\"rep:userId\"] == current ) {\n");
      out.write("                                start = totalItems;\n");
      out.write("                            }\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("\n");
      out.write("                    slider.onSelect = function(toLoadId) {\n");
      out.write("                        var generatedThumbnail = CQ_Analytics.ProfileDataMgr.getProperty(\"generatedThumbnail\");\n");
      out.write("\n");
      out.write("                        CQ_Analytics.ProfileDataMgr.loadProfile(toLoadId);\n");
      out.write("\n");
      out.write("                        //set the generatedThumbnail has it was before\n");
      out.write("                        if( generatedThumbnail ) {\n");
      out.write("                            CQ_Analytics.ProfileDataMgr.addInitProperty(\"generatedThumbnail\",generatedThumbnail);\n");
      out.write("                            CQ_Analytics.ProfileDataMgr.setProperty(\"generatedThumbnail\",generatedThumbnail);\n");
      out.write("                        }\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("                    slider.getCurrentValue = function() {\n");
      out.write("                        return CQ_Analytics.ProfileDataMgr.getProperty(\"authorizableId\");\n");
      out.write("                    };\n");
      out.write("\n");
      out.write("\n");
      out.write("                    slider.show();\n");
      out.write("                });\n");
      out.write("            };\n");
      out.write("\n");
      out.write("            var handleSliderProf = function(event) {\n");
      out.write("                if( !CQ_Analytics.ProfileDataMgr.slider) {\n");
      out.write("                    createSliderProf.call(this, event, true);\n");
      out.write("                } else {\n");
      out.write("                    CQ_Analytics.ProfileDataMgr.slider.show();\n");
      out.write("                }\n");
      out.write("                event.stopPropagation();\n");
      out.write("            };\n");
      out.write("\n");
      out.write("            $CQ(\".cq-cc-thumbnail-profiledata\").bind(\"click\", handleSliderProf);\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("</script>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
