package org.apache.jsp.apps.cq.personalization.components.contextstores.profiledata;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.day.cq.collab.commons.CollabUtil;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.JSONWriterUtil;
import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.date.DateUtil;
import com.day.cq.security.profile.Profile;
import com.day.cq.security.profile.ProfileManager;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.xss.ProtectionContext;
import com.day.cq.xss.XSSProtectionService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.io.JSONWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Date;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class init_js_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {





    void setProfileInitialData(JSONWriter writer, ProfileManager pMgr,
                               SlingHttpServletRequest slingRequest,
                               String absoluteDefaultAvatar,
                               XSSProtectionService xss) throws Exception {

        writer.object();
        final Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
        Profile profile = pMgr.getProfile(session.getUserID(), session);
        if (profile != null) {
            String avatar = CollabUtil.getAvatar(profile, profile.getPrimaryMail(),absoluteDefaultAvatar);
            //increate avatar size
            avatar = avatar == null ? "" : avatar.replaceAll("\\.32\\.",".80.");
            writer.key("avatar").value(avatar);
            writer.key("path").value(profile.getPath());

            Boolean isLoggedIn = profile.getAuthorizable().getID() != null && !profile.getAuthorizable().getID().equals("anonymous");
            writer.key("isLoggedIn").value(isLoggedIn);
            writer.key("isLoggedIn" + JSONWriterUtil.KEY_SUFFIX_XSS)
            		.value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,isLoggedIn.toString()));

            writer.key("authorizableId")
                    .value(profile.getAuthorizable().getID());
            writer.key("authorizableId" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getAuthorizable().getID()));

            writer.key("formattedName")
                    .value(profile.getFormattedName());
            writer.key("formattedName" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getFormattedName()));

            for (String key : profile.keySet()) {
                if (!key.startsWith("jcr:") && !key.startsWith("sling:") && !key.startsWith("cq:last")) {
                    String s = profile.get(key, String.class);
                    s = s != null ? s : "";
                    writer.key(key)
                            .value(s);
                    writer.key(key + JSONWriterUtil.KEY_SUFFIX_XSS)
                            .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,s));
                }
            }

            Date created = profile.get("memberSince", Date.class);
            if( created == null) {
                created = profile.get("jcr:created", Date.class);
            }
            if( created != null ) {
                DateFormat df = DateUtil.getDateFormat("d MMM yyyy h:mm a", slingRequest.getLocale());
                writer.key("memberSince")
                    .value(df.format(created));
            }

            Date birthday = profile.get("birthday", Date.class);
            if( birthday != null ) {
                DateFormat df = DateUtil.getDateFormat("d MMM yyyy", slingRequest.getLocale());
                writer.key("birthday")
                    .value(df.format(birthday));
            }
        }
        writer.endObject();
    }


  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/javascript");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here





    Externalizer externalizer = sling.getService(Externalizer.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);
    boolean isDisabled = WCMMode.DISABLED.equals(WCMMode.fromRequest(slingRequest));

    String absoluteDefaultAvatar = "";
    if(externalizer != null){
        absoluteDefaultAvatar = externalizer.relativeLink(slingRequest, CollabUtil.DEFAULT_AVATAR);
    }

    ProfileManager pMgr = sling.getService(ProfileManager.class);
    StringWriter buf = new StringWriter();

    TidyJSONWriter writer = new TidyJSONWriter(buf);
    writer.setTidy(true);
    try {
        setProfileInitialData(writer, pMgr, slingRequest, absoluteDefaultAvatar, xss);
    } catch (Exception e) {
        log.error("Error while generating JSON profile initial data", e);
    }


      out.write("if (CQ_Analytics && CQ_Analytics.ProfileDataMgr) {\n");
      out.write("    CQ_Analytics.ProfileDataMgr.addListener(\"update\", function(event, property) {\n");
      out.write("        var authorizableId = this.getProperty(\"authorizableId\");\n");
      out.write("        if (!authorizableId || authorizableId == \"anonymous\") {\n");
      out.write("            jQuery(\".cq-cc-profile-not-anonymous\").hide();\n");
      out.write("            jQuery(\".cq-cc-profile-anonymous\").show();\n");
      out.write("        } else {\n");
      out.write("            jQuery(\".cq-cc-profile-not-anonymous\").show();\n");
      out.write("            jQuery(\".cq-cc-profile-anonymous\").hide();\n");
      out.write("        }\n");
      out.write("    });\n");
      out.write("\n");
      out.write("    ");
if (!isDisabled) { 
      out.write("\n");
      out.write("        CQ_Analytics.ProfileDataMgr.loadInitProperties({\n");
      out.write("            \"authorizableId\": \"anonymous\",\n");
      out.write("            \"formattedName\": \"Anonymous Surfer\",\n");
      out.write("            \"path\": \"/home/users/a/anonymous\",\n");
      out.write("            \"avatar\": \"");
      out.print(absoluteDefaultAvatar);
      out.write("\"\n");
      out.write("        });\n");
      out.write("    ");
} else {
      out.write("\n");
      out.write("        CQ_Analytics.ProfileDataMgr.loadInitProperties(");
      out.print(buf);
      out.write(");\n");
      out.write("    ");
}
      out.write("\n");
      out.write("\n");
      out.write("    CQ_Analytics.ProfileDataMgr.init();\n");
      out.write("}\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
