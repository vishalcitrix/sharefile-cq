package org.apache.jsp.apps.citrixosd.components.page.rte;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import java.util.*;

public final class rte_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {


	public void getStyles(Node currentNode, Map<String,String> styleMap, String style) throws RepositoryException {
		if (currentNode.hasNode(style)) {
			final Node styleNode = currentNode.getNode(style);
			NodeIterator iter = styleNode.getNodes();
			while (iter.hasNext()) {
				Node styleItemNode = iter.nextNode();
				if (styleItemNode.hasProperty("name") && styleItemNode.hasProperty("css")) {
					styleMap.put(styleItemNode.getProperty("css").getString(),styleItemNode.getProperty("name").getString());
				}
			}
		}
	}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_005ftest;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fif_005ftest = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fif_005ftest.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');
      out.write('\n');
      if (_jspx_meth_cq_005finclude_005f0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');

    String styleCSS = properties.get("rteDesign","");
	Map <String, String> family = new HashMap<String, String>();
	Map <String, String> size = new HashMap<String, String>();
	Map <String, String> lineHeight = new HashMap<String, String>();
	Map <String, String> color = new HashMap<String, String>();
	Map <String, String> icon = new HashMap<String, String>();
	Map <String, String> others = new HashMap<String, String>();
	getStyles(currentNode, family,"fontFamily");
	getStyles(currentNode, size,"fontSize");
	getStyles(currentNode, lineHeight,"lineHeight");
	getStyles(currentNode, color,"color");
	getStyles(currentNode, icon,"icon");
	getStyles(currentNode, others,"others");
	Iterator<Map.Entry<String, String>> familyEntries = family.entrySet().iterator();
	Iterator<Map.Entry<String, String>> sizeEntries = size.entrySet().iterator();
	Iterator<Map.Entry<String, String>> lineHeightEntries = lineHeight.entrySet().iterator();
	Iterator<Map.Entry<String, String>> colorEntries = color.entrySet().iterator();
	Iterator<Map.Entry<String, String>> iconEntries = icon.entrySet().iterator();
	Iterator<Map.Entry<String, String>> othersEntries = others.entrySet().iterator();

      out.write("\n");
      out.write("\t<head>\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"");
      out.print( currentDesign.getPath() );
      out.write("/css/rte.css\"></link>\n");
      out.write("\t\t");
      if (_jspx_meth_c_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("\t\t<form action=\"");
      out.print( currentPage.getPath());
      out.write("/jcr%3Acontent/style\" method=\"POST\" name=\"style\" id=\"style\" onsubmit=\"return addUpdate(this)\">\n");
      out.write("\t\t\t<h1>Add/Update styles for RTE</h1>\n");
      out.write("\t\t\t<div class=\"message\"></div>\n");
      out.write("\t\t\t<label for=\"fontName\">Font Name (This will appear in RTE dropdown)</label>\n");
      out.write("\t\t\t<input type=\"text\" name=\"fontName\" id=\"fontName\">\n");
      out.write("\t\t\t<label for=\"family\">Font Family</label>\n");
      out.write("\t\t\t<select name=\"family\" id=\"family\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (familyEntries.hasNext()) {
		Map.Entry<String, String> entry = familyEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<label for=\"size\">Font Size</label>\n");
      out.write("\t\t\t<select name=\"size\" id=\"size\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (sizeEntries.hasNext()) {
		Map.Entry<String, String> entry = sizeEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<label for=\"lineHeight\">Font Line Height</label>\n");
      out.write("\t\t\t<select name=\"lineHeight\" id=\"lineHeight\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (lineHeightEntries.hasNext()) {
		Map.Entry<String, String> entry = lineHeightEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<label for=\"color\">Font Color</label>\n");
      out.write("\t\t\t<select name=\"color\" id=\"color\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (colorEntries.hasNext()) {
		Map.Entry<String, String> entry = colorEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<label for=\"icon\">Icons</label>\n");
      out.write("\t\t\t<select name=\"icon\" id=\"icon\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (iconEntries.hasNext()) {
		Map.Entry<String, String> entry = iconEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<label for=\"others\">Other Classes</label>\n");
      out.write("\t\t\t<select name=\"others\" id=\"others\">\n");
      out.write("\t\t\t\t<option value=\"\">Select One</option>\n");

	while (othersEntries.hasNext()) {
		Map.Entry<String, String> entry = othersEntries.next(); 
      out.write("\n");
      out.write("\t\t\t\t<option value=\"");
      out.print( entry.getKey() );
      out.write('"');
      out.write('>');
      out.print( entry.getValue() );
      out.write("</option>");

	}

      out.write("\n");
      out.write("\t\t\t</select>\n");
      out.write("\t\t\t<input type=\"submit\" value=\"Add/Update\">\n");
      out.write("\t\t</form>\n");
      out.write("\t\t<div class=\"print\">\n");
      out.write("\t\t\t<table>");

	if (currentNode.hasNode("style")) {
		final Node styleNode = currentNode.getNode("style");
		NodeIterator iter = styleNode.getNodes();
		while (iter.hasNext()) {
			String fontFamily = "";
			String fontSize = "";
			String fontLineHeight = "";
			String fontColor = "";
			String fontClass = "";
			String fontName = "";
			Node styleItemNode = iter.nextNode();
			if(styleItemNode.hasProperty("family"))
				fontFamily = styleItemNode.getProperty("family").getString();
			if(styleItemNode.hasProperty("size"))
				fontSize = styleItemNode.getProperty("size").getString();
			if(styleItemNode.hasProperty("lineHeight"))
				fontLineHeight = styleItemNode.getProperty("lineHeight").getString();
			if(styleItemNode.hasProperty("color"))
				fontColor = styleItemNode.getProperty("color").getString();
			if(styleItemNode.hasProperty("class"))
				fontClass = styleItemNode.getProperty("class").getString();
			if(styleItemNode.hasProperty("fontName"))
				fontName = styleItemNode.getProperty("fontName").getString();
			else
				fontName = "Lorem ipsum";

      out.write("\n");
      out.write("\t\t\t\t<tr id=\"");
      out.print( styleItemNode.getName() );
      out.write("\"><td><a href=\"#\" class=\"deleteStyle\" title=\"");
      out.print( fontName );
      out.write("\" data-name=\"");
      out.print( styleItemNode.getName() );
      out.write("\">&#10006;</a></td><td><div class=\"");
      out.print( fontFamily );
      out.write(' ');
      out.print( fontSize );
      out.write(' ');
      out.print( fontLineHeight );
      out.write(' ');
      out.print( fontColor );
      out.write(' ');
      out.print( fontClass );
      out.write('"');
      out.write('>');
      out.print( fontName );
      out.write("</div></td><tr>");

		}
	}

      out.write("\t\t\t</table>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<script src=\"");
      out.print( currentDesign.getPath() );
      out.write("/js/common.js\" type=\"text/javascript\"></script>\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\tvar path=\"");
      out.print( currentPage.getPath());
      out.write("/jcr%3Acontent/style/\";\n");
      out.write("\t\t</script>\n");
      out.write("\t</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_cq_005finclude_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:include
    com.day.cq.wcm.tags.IncludeTag _jspx_th_cq_005finclude_005f0 = (com.day.cq.wcm.tags.IncludeTag) _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.get(com.day.cq.wcm.tags.IncludeTag.class);
    _jspx_th_cq_005finclude_005f0.setPageContext(_jspx_page_context);
    _jspx_th_cq_005finclude_005f0.setParent(null);
    // /apps/citrixosd/components/page/rte/rte.jsp(5,0) name = script type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005finclude_005f0.setScript("/libs/wcm/core/components/init/init.jsp");
    int _jspx_eval_cq_005finclude_005f0 = _jspx_th_cq_005finclude_005f0.doStartTag();
    if (_jspx_th_cq_005finclude_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f0.setParent(null);
    // /apps/citrixosd/components/page/rte/rte.jsp(43,2) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f0.setTest(false);
    int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
    if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<link rel=\"stylesheet\" href=\"");
        out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${styleCSS}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("\"></link>");
        int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
    return false;
  }
}
