package org.apache.jsp.apps.brightcove.components.content.brightcoveplaylist;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import com.day.cq.wcm.api.components.DropTarget;

public final class brightcoveplaylist_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;


/*    
    Adobe CQ5 Brightcove Connector  
    
    Copyright (C) 2011 Coresecure Inc.
        
        Authors:    Alessandro Bonfatti
                    Yan Kisen
        
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

      out.write('\n');










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');
      out.write('\n');
      out.write(' ');

 
 String containerClass = DropTarget.CSS_CLASS_PREFIX + "brightcoveplaylist";


String position= properties.get("align","center");
String margLeft = "auto";
String margRight = "auto";


if (position.equals("left")) {
    margLeft ="0";
} else if (position.equals("right")) {
    margRight = "0";
}

      out.write("\n");
      out.write("<div style=\"margin-bottom: 0;margin-left: ");
      out.print(margLeft);
      out.write(";margin-right: ");
      out.print(margRight);
      out.write(";margin-top: 0;overflow-x: hidden;overflow-y: hidden;text-align: center;width: ");
      out.print(properties.get("width","480"));
      out.write("px;text-align:");
      out.print(properties.get("align","center"));
      out.write(";\">\n");
      out.write("    <script language=\"JavaScript\" type=\"text/javascript\" src=\"https://sadmin.brightcove.com/js/BrightcoveExperiences.js\"></script>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\" src=\"https://sadmin.brightcove.com/js/APIModules_all.js\"> </script>\n");
      out.write("\n");
      out.write("    <script type=\"text/javascript\" src=\"https://files.brightcove.com/bc-mapi.js\"></script>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("    var BCLplayer;\n");
      out.write("    var BCLexperienceModule;\n");
      out.write("    var BCLvideoPlayer;\n");
      out.write("    var BCLcurrentVideo;\n");
      out.write("\n");
      out.write("//listener for player error\n");
      out.write("    function onPlayerError(event) {\n");
      out.write("        /* */\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("//listener for when player is loaded\n");
      out.write("    function onPlayerLoaded(id) {\n");
      out.write("      // newLog();\n");
      out.write("    //  log(\"EVENT: onPlayerLoaded\");\n");
      out.write("      BCLplayer = brightcove.getExperience(id);\n");
      out.write("      BCLexperienceModule = BCLplayer.getModule(APIModules.EXPERIENCE);\n");
      out.write("    }\n");
      out.write("\n");
      out.write("//listener for when player is ready\n");
      out.write("    function onPlayerReady(event) {\n");
      out.write("     // log(\"EVENT: onPlayerReady\");\n");
      out.write("\n");
      out.write("      // get a reference to the video player module\n");
      out.write("      BCLvideoPlayer = BCLplayer.getModule(APIModules.VIDEO_PLAYER);\n");
      out.write("      // add a listener for media change events\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.BEGIN, onMediaBegin);\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.COMPLETE, onMediaBegin);\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.CHANGE, onMediaBegin);\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.ERROR, onMediaBegin);\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.PLAY, onMediaBegin);\n");
      out.write("      BCLvideoPlayer.addEventListener(BCMediaEvent.STOP, onMediaBegin);\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("// listener for media change events\n");
      out.write("    function onMediaBegin(event) {\n");
      out.write("        var BCLcurrentVideoID;\n");
      out.write("        var BCLcurrentVideoNAME;\n");
      out.write("        BCLcurrentVideoID = BCLvideoPlayer.getCurrentVideo().id;\n");
      out.write("        BCLcurrentVideoNAME = BCLvideoPlayer.getCurrentVideo().displayName;\n");
      out.write("        switch (event.type) {\n");
      out.write("            case \"mediaBegin\":\n");
      out.write("                var currentVideoLength =\"0\";\n");
      out.write("                currentVideoLength = BCLvideoPlayer.getCurrentVideo().length;\n");
      out.write("                if (currentVideoLength != \"0\") currentVideoLength = currentVideoLength/1000;\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type+\" - \"+currentVideoLength, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("                break;\n");
      out.write("            case \"mediaPlay\":\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type+\" - \"+event.position, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("                break;\n");
      out.write("            case \"mediaStop\":\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type+\" - \"+event.position, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("                break;\n");
      out.write("            case \"mediaChange\":\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type+\" - \"+event.position, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("                break;\n");
      out.write("            case \"mediaComplete\":\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type+\" - \"+event.position, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("                break;\n");
      out.write("            default:\n");
      out.write("                _gaq.push(['_trackEvent', location.pathname, event.type, BCLcurrentVideoNAME+\" - \"+BCLcurrentVideoID]);\n");
      out.write("        }\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("    \n");
      out.write("\n");
      out.write("<div style=\"display:none\">\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("<object id=\"myExperience\" class=\"BrightcoveExperience\">\n");
      out.write("  <param name=\"bgcolor\" value=\"#FFFFFF\" />\n");
      out.write("  <param name=\"width\" value=\"");
      out.print(properties.get("width","480"));
      out.write("\" />\n");
      out.write("  <param name=\"height\" value=\"");
      out.print(properties.get("height","270"));
      out.write("\" />\n");
      out.write("  <param name=\"playerID\" value=\"");
      out.print(properties.get("playerID",""));
      out.write("\" />\n");
      out.write("  <param name=\"playerKey\" value=\"");
      out.print(properties.get("playerKey",""));
      out.write("\" />\n");
      out.write("  <param name=\"isVid\" value=\"true\" />\n");
      out.write("  <param name=\"isUI\" value=\"true\" />\n");
      out.write("  <param name=\"dynamicStreaming\" value=\"true\" />\n");
      out.write("  <param name=\"@playlistTabs\"  value=\"");
      out.print(properties.get("videoPlayerPL",""));
      out.write("\" />\n");
      out.write("  <param name=\"templateLoadHandler\" value=\"onPlayerLoaded\" />\n");
      out.write("  <param name=\"templateReadyHandler\" value=\"onPlayerReady\" />\n");
      out.write("  <param name=\"templateErrorHandler\" value=\"onPlayerError\" />\n");
      out.write("  <param name=\"includeAPI\" value=\"true\" /> \n");
      out.write("  <param name=\"wmode\" value=\"transparent\" />\n");
      out.write("</object>\n");
      out.write("   \n");
      out.write("    \n");
      out.write("<script type=\"text/javascript\">brightcove.createExperiences();</script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("</div>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
