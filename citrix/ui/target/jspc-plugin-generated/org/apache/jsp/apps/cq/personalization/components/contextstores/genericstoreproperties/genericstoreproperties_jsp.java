package org.apache.jsp.apps.cq.personalization.components.contextstores.genericstoreproperties;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class genericstoreproperties_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {







    boolean isImage(String value) {
        return value != null && (value.toLowerCase().contains(".png")
                                        || value.toLowerCase().contains(".jpg")
                                        || value.toLowerCase().contains(".gif"));
    }

    boolean isURL(String value) {
        return value != null && value.indexOf("://") != -1;
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.release();
    _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write("<div class=\"cq-cc-store\">\n");
      out.write("    ");
      if (_jspx_meth_cq_005finclude_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_cq_005finclude_005f1(_jspx_page_context))
        return;

    String store = properties.get("store", currentStyle.get("store",String.class));
    String[] props = properties.get("properties", currentStyle.get("properties",new String[0]));
    String thumbnail = properties.get("thumbnail",String.class);
    boolean hasThumbnail = thumbnail != null;
    String thumbnailDynamicValue = null;
    String thumbnailProperty = thumbnail;
    if( hasThumbnail ) {
        
      out.write("<div class=\"cq-cc-thumbnail\">");

            if( isImage(thumbnail) || isURL(thumbnail)) {
                thumbnailProperty = "generatedThumbnail";
                if( isImage(thumbnail) ) {
                    thumbnailDynamicValue = request.getContextPath() + thumbnail;
                } else {
                    thumbnailDynamicValue = thumbnail;
                }

                //register extra generated property to the store
                
      out.write("<script type=\"text/javascript\">\n");
      out.write("                    $CQ(function() {\n");
      out.write("                       var init = function(store,a,b,c) {\n");
      out.write("                            store.addInitProperty(\"");
      out.print(thumbnailProperty);
      out.write('"');
      out.write(',');
      out.write('"');
      out.print(thumbnailDynamicValue);
      out.write("\");\n");
      out.write("                            store.setProperty(\"");
      out.print(thumbnailProperty);
      out.write('"');
      out.write(',');
      out.write('"');
      out.print(thumbnailDynamicValue);
      out.write("\");\n");
      out.write("                        };\n");
      out.write("                        CQ_Analytics.ClientContextUtils.onStoreRegistered(\"");
      out.print(store);
      out.write("\", init);\n");
      out.write("                    });\n");
      out.write("                </script>");

            }
            
      out.write("<div class=\"cq-cc-store-property\">");
      //  personalization:storePropertyTag
      com.day.cq.personalization.tags.StorePropertyTag _jspx_th_personalization_005fstorePropertyTag_005f0 = (com.day.cq.personalization.tags.StorePropertyTag) _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.get(com.day.cq.personalization.tags.StorePropertyTag.class);
      _jspx_th_personalization_005fstorePropertyTag_005f0.setPageContext(_jspx_page_context);
      _jspx_th_personalization_005fstorePropertyTag_005f0.setParent(null);
      // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(50,48) name = propertyName type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_personalization_005fstorePropertyTag_005f0.setPropertyName(thumbnailProperty);
      // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(50,48) name = store type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_personalization_005fstorePropertyTag_005f0.setStore(store);
      int _jspx_eval_personalization_005fstorePropertyTag_005f0 = _jspx_th_personalization_005fstorePropertyTag_005f0.doStartTag();
      if (_jspx_th_personalization_005fstorePropertyTag_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.reuse(_jspx_th_personalization_005fstorePropertyTag_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.reuse(_jspx_th_personalization_005fstorePropertyTag_005f0);
      out.write("</div>");

        
      out.write("</div>");

    }

    
      out.write("<div class=\"cq-cc-content\">");

    if(store != null && props != null ) {
        int i = 0;
        for(String property: props) {
            if( !hasThumbnail || !thumbnail.equals(property)) {
                
      out.write("<div class=\"cq-cc-store-property cq-cc-store-property-level");
      out.print((i++));
      out.write('"');
      out.write('>');
      //  personalization:storePropertyTag
      com.day.cq.personalization.tags.StorePropertyTag _jspx_th_personalization_005fstorePropertyTag_005f1 = (com.day.cq.personalization.tags.StorePropertyTag) _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.get(com.day.cq.personalization.tags.StorePropertyTag.class);
      _jspx_th_personalization_005fstorePropertyTag_005f1.setPageContext(_jspx_page_context);
      _jspx_th_personalization_005fstorePropertyTag_005f1.setParent(null);
      // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(59,89) name = propertyName type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_personalization_005fstorePropertyTag_005f1.setPropertyName(property);
      // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(59,89) name = store type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_personalization_005fstorePropertyTag_005f1.setStore(store);
      int _jspx_eval_personalization_005fstorePropertyTag_005f1 = _jspx_th_personalization_005fstorePropertyTag_005f1.doStartTag();
      if (_jspx_th_personalization_005fstorePropertyTag_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.reuse(_jspx_th_personalization_005fstorePropertyTag_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fpersonalization_005fstorePropertyTag_005fstore_005fpropertyName_005fnobody.reuse(_jspx_th_personalization_005fstorePropertyTag_005f1);
      out.write("</div>");

            }
        }
    }
    
      out.write("</div>\n");
      out.write("    ");
      if (_jspx_meth_cq_005finclude_005f2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_cq_005finclude_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    <div class=\"cq-cc-clear\"></div>\n");
      out.write("</div>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_cq_005finclude_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:include
    com.day.cq.wcm.tags.IncludeTag _jspx_th_cq_005finclude_005f0 = (com.day.cq.wcm.tags.IncludeTag) _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.get(com.day.cq.wcm.tags.IncludeTag.class);
    _jspx_th_cq_005finclude_005f0.setPageContext(_jspx_page_context);
    _jspx_th_cq_005finclude_005f0.setParent(null);
    // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(21,4) name = script type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005finclude_005f0.setScript("internal_prolog.jsp");
    int _jspx_eval_cq_005finclude_005f0 = _jspx_th_cq_005finclude_005f0.doStartTag();
    if (_jspx_th_cq_005finclude_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f0);
    return false;
  }

  private boolean _jspx_meth_cq_005finclude_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:include
    com.day.cq.wcm.tags.IncludeTag _jspx_th_cq_005finclude_005f1 = (com.day.cq.wcm.tags.IncludeTag) _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.get(com.day.cq.wcm.tags.IncludeTag.class);
    _jspx_th_cq_005finclude_005f1.setPageContext(_jspx_page_context);
    _jspx_th_cq_005finclude_005f1.setParent(null);
    // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(22,4) name = script type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005finclude_005f1.setScript("prolog.jsp");
    int _jspx_eval_cq_005finclude_005f1 = _jspx_th_cq_005finclude_005f1.doStartTag();
    if (_jspx_th_cq_005finclude_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f1);
    return false;
  }

  private boolean _jspx_meth_cq_005finclude_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:include
    com.day.cq.wcm.tags.IncludeTag _jspx_th_cq_005finclude_005f2 = (com.day.cq.wcm.tags.IncludeTag) _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.get(com.day.cq.wcm.tags.IncludeTag.class);
    _jspx_th_cq_005finclude_005f2.setPageContext(_jspx_page_context);
    _jspx_th_cq_005finclude_005f2.setParent(null);
    // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(64,4) name = script type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005finclude_005f2.setScript("internal_epilog.jsp");
    int _jspx_eval_cq_005finclude_005f2 = _jspx_th_cq_005finclude_005f2.doStartTag();
    if (_jspx_th_cq_005finclude_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f2);
    return false;
  }

  private boolean _jspx_meth_cq_005finclude_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:include
    com.day.cq.wcm.tags.IncludeTag _jspx_th_cq_005finclude_005f3 = (com.day.cq.wcm.tags.IncludeTag) _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.get(com.day.cq.wcm.tags.IncludeTag.class);
    _jspx_th_cq_005finclude_005f3.setPageContext(_jspx_page_context);
    _jspx_th_cq_005finclude_005f3.setParent(null);
    // /apps/cq/personalization/components/contextstores/genericstoreproperties/genericstoreproperties.jsp(65,4) name = script type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_cq_005finclude_005f3.setScript("epilog.jsp");
    int _jspx_eval_cq_005finclude_005f3 = _jspx_th_cq_005finclude_005f3.doStartTag();
    if (_jspx_th_cq_005finclude_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005finclude_005fscript_005fnobody.reuse(_jspx_th_cq_005finclude_005f3);
    return false;
  }
}
