package org.apache.jsp.apps.sling.servlet.errorhandler;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.net.URLEncoder;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.engine.auth.Authenticator;
import org.apache.sling.engine.auth.NoAuthenticationHandlerException;
import com.day.cq.wcm.api.WCMMode;
import org.apache.jackrabbit.util.Text;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.jcr.Session;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

public final class author_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {



    private boolean isAnonymousUser(HttpServletRequest request) {
        return request.getAuthType() == null
            || request.getRemoteUser() == null;
    }
    

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!-- \n");
      out.write("\tConditions:\n");
      out.write("\t\tIf 'forbidden', aka user isn't logged in \t==> send them to login page.\\\n");
      out.write("\t\tIf 'forbidden', aka user has no permissions ==> let them know that.\n");
      out.write("\t\tIf '404' within a site, page not found   \t==> send them to site's 404 page, non-editable.\n");
      out.write("\n");
      out.write(" -->\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');

	SlingBindings slingBindings = (SlingBindings) request.getAttribute("org.apache.sling.api.scripting.SlingBindings");
	SlingScriptHelper slingScripter = slingBindings.getSling();
	SlingHttpServletRequest slingRequest2 = slingScripter.getRequest();
	ResourceResolver slingResourceResolver = slingRequest2.getResourceResolver();
	javax.jcr.Session jcrSession = slingResourceResolver.adaptTo(javax.jcr.Session.class);
	Pattern sitePattern = Pattern.compile("^\\/content/.*\\/.*");
	boolean sendToLogin = isAnonymousUser(request);
	boolean resourceNotFound = false;
	boolean hasPermission = false;
	
	//user is logged in, check if they have permissions on path
	Resource r = slingRequest2.getResource();
	if (r == null)
		resourceNotFound = true;
	else {
		if (jcrSession.hasPermission(r.getPath(), "read"))
			hasPermission = true;
	}

	String currentPathInfo = request.getPathInfo();
	final Matcher siteMatcher = sitePattern.matcher(currentPathInfo);
	boolean isSitePage = siteMatcher.matches();

      out.write("\n");
      out.write("<c:set var=\"sendToLogin\" value=\"");
      out.print(sendToLogin);
      out.write("\"/>\n");
      out.write("<c:set var=\"isSitePage\" value=\"");
      out.print(isSitePage);
      out.write("\"/>\n");
      out.write("<c:set var=\"hasPermission\" value=\"");
      out.print(hasPermission );
      out.write("\"/>\n");
      out.write("<c:choose>\n");
      out.write("\t<c:when test=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${sendToLogin}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\n");
      out.write("\t\t");
      out.write('\n');
      out.write('	');
      out.write('	');

	        Authenticator auth = slingScripter.getService(Authenticator.class);
	        if (auth != null) {
	            try {
	                auth.login(request, response);
	                return;
	            } catch (NoAuthenticationHandlerException nahe) {
	                slingBindings.getLog().warn("Cannot login: No Authentication Handler is willing to authenticate");
	            }
	        } else {
	           slingBindings.getLog().warn("Cannot login: Missing Authenticator service");
	        }
		
      out.write("\n");
      out.write("\t</c:when>\n");
      out.write("\t<c:when test=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${hasPermission}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\n");
      out.write("\t\t");

			String sitePath = Text.getAbsoluteParent(currentPathInfo, 2);
		   	String errorPagePath = sitePath + "/404";
		   	Resource errorPageResource = slingResourceResolver.getResource(errorPagePath);
		
      out.write("\n");
      out.write("\t\t<c:set var=\"errorPage\" value=\"");
      out.print(errorPageResource );
      out.write("\"/>\n");
      out.write("\t\t<c:choose>\n");
      out.write("\t\t\t<c:when test=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${isSitePage && not empty errorPage}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\n");
      out.write("\t\t\t\t");
      out.write("\n");
      out.write("\t\t\t\t<swx:setWCMMode mode=\"READ_ONLY\">\n");
      out.write("\t\t    \t\t<sling:include resource=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${errorPage}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\"/>\n");
      out.write("\t\t\t\t</swx:setWCMMode>\n");
      out.write("\t\t\t</c:when>\n");
      out.write("\t\t\t<c:otherwise>\n");
      out.write("\t\t\t\t");

					response.setStatus(404);
				
      out.write("\n");
      out.write("\t\t\t\t404: Page Not Found.\n");
      out.write("\t\t\t</c:otherwise>\n");
      out.write("\t\t</c:choose>\n");
      out.write("\t</c:when>\n");
      out.write("\t<c:otherwise>\n");
      out.write("\t\t");

			response.setStatus(404);
		
      out.write("\n");
      out.write("\t\tYou have no permissions to view this page, please contact your administrator.\n");
      out.write("\t</c:otherwise>\n");
      out.write("</c:choose>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
