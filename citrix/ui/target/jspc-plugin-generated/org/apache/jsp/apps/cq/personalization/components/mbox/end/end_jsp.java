package org.apache.jsp.apps.cq.personalization.components.mbox.end;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import com.day.cq.analytics.AnalyticsConfiguration;
import com.day.cq.analytics.testandtarget.util.MboxHelper;
import com.day.cq.wcm.webservicesupport.ConfigurationManager;
import com.day.cq.wcm.webservicesupport.Configuration;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import com.day.cq.wcm.api.WCMMode;

public final class end_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {


/**
 * Returns a {@link Property} value and handles multi-value properties by concatenating
 * the values separated by comma.
 *
 * @param property The property to get the value from
 * @return A string representation of the value of this property
 */
protected String getPropertyValue(Property property) throws RepositoryException {
    if(!property.isMultiple()) {
        return property.getString();
    }else{
        String v = "";
        Value[] values = property.getValues();
        for(int i=0; i<values.length; i++) {
            if(i>0) {
                v += ",";
            }
            v += values[i].getString();
        }
        return v;
    }
}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;











      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here




      out.write('\n');

    final WCMMode wcmMode = WCMMode.fromRequest(request);

    ConfigurationManager cfgMgr = sling.getService(ConfigurationManager.class);
    Configuration configuration = null;
    String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});
    if(cfgMgr != null) {
        configuration = cfgMgr.getConfiguration("testandtarget", services);
    }
    final AnalyticsConfiguration analyticsConfig = resource.adaptTo(AnalyticsConfiguration.class);
    final Boolean isValidConfig = ( (analyticsConfig != null && analyticsConfig.get("cq:ttclientcode") != null) || 
            (configuration != null && configuration.getInherited("clientcode", null) != null) );
  
    final Resource startMbox = MboxHelper.searchStartElement(resource);
    final ValueMap resourceConfig = startMbox.adaptTo(ValueMap.class); 
    final String mboxId = MboxHelper.getMboxId(resource);
    final String mboxName = MboxHelper.getMboxName(resource);
    final String clientCode;
    if (analyticsConfig != null && analyticsConfig.get("cq:ttclientcode") != null) {
        clientCode = analyticsConfig.get("cq:ttclientcode", null);
    } else if (configuration != null) {
        clientCode = configuration.getInherited("clientcode", null);
    } else {
    	clientCode = null;
   	}

    // draw the edit bar
    if (editContext != null) {
        editContext.includeEpilog(slingRequest, slingResponse, wcmMode);
    }
    
    // turn of decoration and close the decorating DIV
    componentContext.setDecorate(false);
    
      out.write(" \n");
      out.write("    </div>  \n");
      out.write("</div>\n");

if(isValidConfig && (wcmMode != WCMMode.EDIT)) {
  String[] mappings = resourceConfig.get("cq:mappings", new String[0]);  
  String[] isProfile = resourceConfig.get("cq:isprofile", new String[0]);      

      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("CQ_Analytics.TestTarget.init('");
      out.print( clientCode );
      out.write("');\n");
 if (mappings.length > 0 ) { 


      out.write("mboxDefine(\"");
      out.print( mboxId );
      out.write("\", \"");
      out.print( mboxName );
      out.write('"');


 } else { 


      out.write("mboxCreate(\"");
      out.print( mboxName );
      out.write('"');


 } 

    //page parameters
    for(String key: resourceConfig.keySet() ) {
        if(key.indexOf(":") == -1) {
            
      out.write(',');

            
      out.write('"');
      out.print( key );
      out.write(' ');
      out.write('=');
      out.print(resourceConfig.get(key, "") );
      out.write('"');

        }
    }
    //static parameters
    String[] staticparams = resourceConfig.get("cq:staticparams", new String[0]);
    Node resNode = currentPage.adaptTo(Node.class);
    for(String arr : staticparams) {
        JSONArray jsonElem = new JSONArray(arr);
        String key = jsonElem.getString(0);
        String value = jsonElem.getString(1);
        if(value.startsWith("./") && resNode != null) {
            try {
                Property prop = resNode.getProperty(value);
                value = getPropertyValue(prop);
            }catch(PathNotFoundException e) {}
        }
        
      out.write(',');

        
      out.write('"');
      out.print( xssAPI.encodeForJSString(key + "=" + value));
      out.write('"');

    }

      out.write(')');

	if (mappings.length > 0) {

      out.write("    \n");
      out.write("if (!CQ_Analytics.mboxes) { CQ_Analytics.mboxes = new Array(); }\n");
      out.write("CQ_Analytics.mboxes.push({id: \"");
      out.print( mboxId );
      out.write("\", name: \"");
      out.print( mboxName );
      out.write("\",\n");
      out.write("    mappings: [");

        for(int i=0; i < mappings.length; i++) {
            if(i>0) {
                
      out.write(',');

            }
            
      out.write('\'');
      out.print( xssAPI.encodeForJSString(mappings[i]) );
      out.write('\'');

        }

      out.write("],\n");
      out.write("    isProfile: [");

        for(int i=0; i < isProfile.length; i++) {
            if(i>0) {
                
      out.write(',');

            }
            
      out.write('\'');
      out.print( xssAPI.encodeForJSString(isProfile[i]) );
      out.write('\'');

        }

      out.write("]\n");
      out.write("});");

	}

      out.write("\n");
      out.write("</script>");



} else {
// WCM.EDITMODE



      out.write("<script type=\"text/javascript\">\n");
      out.write("//force sidekick to reload in preview mode\n");
      out.write("CQ.Ext.onReady(function(){\n");
      out.write("    CQ.WCM.getSidekick().previewReload = true;\n");
      out.write("});\n");
      out.write("</script>");



}

      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
