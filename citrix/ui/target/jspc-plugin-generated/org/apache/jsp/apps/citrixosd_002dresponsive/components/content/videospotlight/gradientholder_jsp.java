package org.apache.jsp.apps.citrixosd_002dresponsive.components.content.videospotlight;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class gradientholder_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("<div class=\"hovercontent\" style=\"background: ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("; /* Old browsers */\n");
      out.write("    background: -moz-linear-gradient(top, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 100%); /* FF3.6+ */\n");
      out.write("    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("), color-stop(100%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(")); /* Chrome,Safari4+ */\n");
      out.write("    background: -webkit-linear-gradient(top, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 100%); /* Chrome10+,Safari5.1+ */\n");
      out.write("    background: -o-linear-gradient(top, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 100%); /* Opera 11.10+ */\n");
      out.write("    background: -ms-linear-gradient(top, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 100%); /* IE10+ */\n");
      out.write("    background: linear-gradient(to bottom,");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 0%, ");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" 100%); /* W3C */\n");
      out.write("    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorFirst}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("', endColorstr='");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.gradientcolorSecond}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("', GradientType=0 ); /* IE6-9 */ \">\n");
      out.write("\t<p class=\"title\">");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.titleGardient}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</p>\n");
      out.write("\t<p class=\"description\">");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${properties.subtitle}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</p>\n");
      out.write("\t<p class=\"playvideo icon-play\"></p>\n");
      out.write("</div>\n");
      out.write(" \n");
      out.write(" \n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
