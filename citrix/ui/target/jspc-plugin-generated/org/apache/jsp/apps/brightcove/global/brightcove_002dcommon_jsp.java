package org.apache.jsp.apps.brightcove.global;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.sling.commons.json.JSONArray;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import org.apache.sling.commons.json.io.JSONWriter;
import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import java.util.*;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.*;
import com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.*;
import com.brightcove.proserve.mediaapi.wrapper.utils.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public final class brightcove_002dcommon_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {


static Logger loggerBR = LoggerFactory.getLogger("Brightcove");

static List sortByValue(final Map m) {
    List keys = new ArrayList();
    keys.addAll(m.keySet());
    Collections.sort(keys, new Comparator() {
        public int compare(Object o1, Object o2) {
        try{
            JSONObject v1 = (JSONObject)m.get(o1);
            String s1 = (String)v1.get("name");
            JSONObject v2 = (JSONObject)m.get(o2);
            String s2 = (String)v2.get("name");

            if (s1 == null) {
                return (s2 == null) ? 0 : 1;
            }
            else if (s1 instanceof Comparable) {
                return ((Comparable) s1).compareTo(s2);
            }
            else {
                return 0;
            }
           
           } catch (JSONException e) {
                   return 0;
            }
        }
    });
    return keys;
}

static String getLength(String videoId, String tokenID) {
   String result = "";
   
   
       HttpURLConnection connection = null;
       OutputStreamWriter wr = null;
       BufferedReader rd  = null;
       StringBuilder sb = null;
       String line = null;
       URL serverAddress = null;
       
       try {
           
           serverAddress = new URL("http://api.brightcove.com/services/library?command=find_video_by_id&video_id="+videoId+"&video_fields=name,length&token="+tokenID);
           //set up out communications stuff
           connection = null;
          
           //Set up the initial connection
           connection = (HttpURLConnection)serverAddress.openConnection();
           connection.setRequestMethod("GET");
           connection.setDoOutput(true);
           connection.setReadTimeout(10000);
                     
           connection.connect();
          
           rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
           sb = new StringBuilder();
          
           while ((line = rd.readLine()) != null)
           {
               sb.append(line + '\n');
           }
           
           JSONObject js = new JSONObject(sb.toString());
            
           String temp_result = js.getString("length");
           long millis = Long.parseLong(temp_result);
           result = String.format("%02d:%02d", 
                   TimeUnit.MILLISECONDS.toMinutes(millis),
                   TimeUnit.MILLISECONDS.toSeconds(millis) - 
                   TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
               );
           
       } catch (JSONException e) {
           e.printStackTrace();
       } catch (MalformedURLException e) {
           e.printStackTrace();
       } catch (ProtocolException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       finally
       {
           //close the connection, set all objects to null
           if (connection!=null)connection.disconnect();
           rd = null;
           sb = null;
           wr = null;
           connection = null;
       }
    return result;
}
static String getName(String videoId, String tokenID) {
       String result = "";
       
       
           HttpURLConnection connection = null;
           OutputStreamWriter wr = null;
           BufferedReader rd  = null;
           StringBuilder sb = null;
           String line = null;
           URL serverAddress = null;
           
           try {
               
               serverAddress = new URL("http://api.brightcove.com/services/library?command=find_video_by_id&video_id="+videoId+"&video_fields=name,length&token="+tokenID);
               //set up out communications stuff
               connection = null;
              
               //Set up the initial connection
               connection = (HttpURLConnection)serverAddress.openConnection();
               connection.setRequestMethod("GET");
               connection.setDoOutput(true);
               connection.setReadTimeout(10000);
                         
               connection.connect();
              
               rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               sb = new StringBuilder();
              
               while ((line = rd.readLine()) != null)
               {
                   sb.append(line + '\n');
               }
               
               JSONObject js = new JSONObject(sb.toString());
                
               result = js.getString("name");
              
               
           } catch (JSONException e) {
               e.printStackTrace();
           } catch (MalformedURLException e) {
               e.printStackTrace();
           } catch (ProtocolException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           }
           finally
           {
               //close the connection, set all objects to null
               connection.disconnect();
               rd = null;
               sb = null;
               wr = null;
               connection = null;
           }
        return result;
    }
static String getList(String token, String params, Boolean exportCSV) {
  
    String result = "";

    HttpURLConnection connection = null;
    OutputStreamWriter wr = null;
    BufferedReader rd  = null;
    StringBuilder sb = null;
    String line = null;
    URL serverAddress = null;
    JSONObject js =null;
    try {            
      
        java.util.Map<String, JSONObject> sortedjson = new HashMap();
        
        int pageNumber = 0;
        double totalPages = 0;
        
        while(pageNumber <= totalPages){
        
            serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields="+params+"&get_item_count=true&page_number="+pageNumber+"&token="+token);
            //set up out communications stuff
            connection = null;
           
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
                        
            js = new JSONObject(sb.toString());
            
            totalPages = Math.floor(js.getInt("total_count")/100);
            
            JSONArray jsa = new JSONArray(js.get("items").toString());
            for (int i = 0; i < jsa.length(); i++) {
                JSONObject row = jsa.getJSONObject(i);
                sortedjson.put(row.getString("id"), row);
            }
        
            pageNumber ++;
        }
        
        if(exportCSV){
             JSONObject tempJSON;
             String csvString = "\"Video Name\",\"Video ID\"\r\n";    
     
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                tempJSON =  sortedjson.get(key);
                csvString += "\""+tempJSON.getString("name") + "\",\""+tempJSON.getString("id") + "\"\r\n";
            }
            result = csvString;
        }
        else{
            JSONObject jsTotal = new JSONObject();
            
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                jsTotal.accumulate("items", sortedjson.get(key));
            }
            jsTotal.put("totals", js.getInt("total_count"));
            result = jsTotal.toString();
        }
       
    } catch (JSONException e) {
        e.printStackTrace();
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    finally
    {
        //close the connection, set all objects to null
        connection.disconnect();
        rd = null;
        sb = null;
        wr = null;
        connection = null;
    }
     return result;
 }
static String getList(String token, String params, Boolean exportCSV, String start, String limit, String query) {
	  
	String result = "";
    
    HttpURLConnection connection = null;
    OutputStreamWriter wr = null;
    BufferedReader rd  = null;
    StringBuilder sb = null;
    String line = null;
    URL serverAddress = null;
    JSONObject jsTotal = new JSONObject();
    try {
        
       java.util.Map<String, JSONObject> sortedjson = new HashMap();
        
        int pageNumber = 0;
        int firstElement=0;
        if (start!= null && !start.trim().isEmpty() && Integer.parseInt(start)>0) {
        	firstElement = Integer.parseInt(start);
        	if (limit!= null && !limit.trim().isEmpty()) pageNumber = (firstElement+Integer.parseInt(limit))/20;
            
        }
        int totalPages = 0;
        
        	if (query != null && !query.trim().isEmpty()) {
        		serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&all=display_name:="+query.trim()+"&video_fields=" + params + "&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
                	
        	}else{
        	    serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields=" + params + "&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
        	}
        	//set up out communications stuff
            connection = null;
           
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
            
             
            JSONObject js = new JSONObject(sb.toString());
            totalPages = js.getInt("total_count");
            
            if (firstElement<totalPages) {
            	jsTotal.put("items",js.get("items"));
            	jsTotal.put("results", js.getInt("total_count"));
            }else{
                jsTotal = new JSONObject("{\"items\":[],\"results\":0}");
            }

        
        
        
        result = jsTotal.toString();
        
        
    } catch (JSONException e) {
        e.printStackTrace();
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    finally
    {
        //close the connection, set all objects to null
        connection.disconnect();
        rd = null;
        sb = null;
        wr = null;
        connection = null;
    }
 return result;
 }
 
static String getListSideMenu(String token, String limit) {
    String result = "";
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        JSONObject jsTotal = new JSONObject();
        try {
            
           java.util.Map<String, JSONObject> sortedjson = new HashMap();
            
            int pageNumber = 0;
            int firstElement=0;
            if (limit!= null && !limit.trim().isEmpty() && limit.split("\\.\\.")[0] != null) {
            	pageNumber = Integer.parseInt(limit.split("\\.\\.")[0])/20;
            	firstElement = Integer.parseInt(limit.split("\\.\\.")[0]);
            }
            int totalPages = 0;
            
                serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields=name,id,thumbnailURL&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
                //set up out communications stuff
                connection = null;
               
                //Set up the initial connection
                connection = (HttpURLConnection)serverAddress.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setReadTimeout(10000);
                          
                connection.connect();
               
                rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sb = new StringBuilder();
               
                while ((line = rd.readLine()) != null)
                {
                    sb.append(line + '\n');
                }
                
                 
                JSONObject js = new JSONObject(sb.toString());
                totalPages = js.getInt("total_count");
                if (firstElement<totalPages) {
	                JSONArray jsa = new JSONArray(js.get("items").toString());
	                for (int i = 0; i < jsa.length(); i++) {
	                    JSONObject row = jsa.getJSONObject(i);
	                    sortedjson.put(row.getString("id"), row);
	                }
	                
	               
	                for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
		                String key = (String) i.next();
		                jsTotal.accumulate("items", sortedjson.get(key));
		            } 
	                jsTotal.put("results", js.getInt("total_count"));
                }else{
                	jsTotal = new JSONObject("{\"items\":[],\"results\":0}");
                }
                
            
            
            
            result = jsTotal.toString().replace("\"id\"", "\"videoid\"").replaceAll("\"videoid\":([0-9]*)", "\"path\":\"$1\"");
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return result;
 }

//Returns JSON of the video information based on a comma separated string of their ids.
static JSONArray getVideosJsonByIds(String videoIds, String videoProperties, String tokenID) {
    JSONArray jsa = new JSONArray();
    
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        
        try {
            
            serverAddress = new URL("http://api.brightcove.com/services/library?command=find_videos_by_ids&video_ids="+videoIds+"&video_fields="+videoProperties+"&token="+tokenID);
            
            //set up connection
            connection = null;
            
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
            
           
            JSONObject js = new JSONObject(sb.toString());
            jsa = new JSONArray(js.get("items").toString());
            
            
            
             
           
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return jsa;
 }
 
 
 //FindAllPlaylists(String readToken, Integer pageSize, Integer pageNumber, SortByTypeEnum sortBy, SortOrderTypeEnum sortOrderType, EnumSet<VideoFieldEnum> videoFields, Set<String> customFields, EnumSet<PlaylistFieldEnum> playlistFields)
static String getListPlaylistsSideMenu(String token) {
    String result = "";
    
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        
        try {
            
           java.util.Map<String, JSONObject> sortedjson = new HashMap();
            
            int pageNumber = 0;
            double totalPages = 0;
            
            while(pageNumber <= totalPages){
                serverAddress = new URL("http://api.brightcove.com/services/library?command=find_all_playlists&playlist_fields=name,id,thumbnailURL&get_item_count=true&page_number="+pageNumber+"&token="+token);
                //set up out communications stuff
                connection = null;
               
                //Set up the initial connection
                connection = (HttpURLConnection)serverAddress.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setReadTimeout(10000);
                          
                connection.connect();
               
                rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sb = new StringBuilder();
               
                while ((line = rd.readLine()) != null)
                {
                    sb.append(line + '\n');
                }
                
                 
                JSONObject js = new JSONObject(sb.toString());
                totalPages = Math.floor(js.getInt("total_count")/100);
                JSONArray jsa = new JSONArray(js.get("items").toString());
                for (int i = 0; i < jsa.length(); i++) {
                    JSONObject row = jsa.getJSONObject(i);
                    sortedjson.put(row.getString("id"), row);
                }
                pageNumber ++;
                
            }
            
            JSONObject jsTotal = new JSONObject();
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            jsTotal.accumulate("items", sortedjson.get(key));
        } 
            
            result = jsTotal.toString().replaceAll("\"id\":([0-9]*)", "\"path\":\"$1\"").replaceAll("\"thumbnailURL\":null", "\"thumbnailURL\":\"#\"");
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return result;
}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;


/*    
    Adobe CQ5 Brightcove Connector  
    
    Copyright (C) 2011 Coresecure Inc.
        
        Authors:    Alessandro Bonfatti
                    Yan Kisen
        
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

      out.write('\n');
      out.write('\n');
      out.write('\n');










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
