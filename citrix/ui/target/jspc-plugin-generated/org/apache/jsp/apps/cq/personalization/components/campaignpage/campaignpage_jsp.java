package org.apache.jsp.apps.cq.personalization.components.campaignpage;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.day.cq.i18n.I18n;
import com.day.cq.personalization.TeaserUtils;
import com.day.cq.wcm.core.stats.PageViewStatistics;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.tagging.Tag;
import com.day.text.Text;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import org.apache.sling.api.resource.ResourceUtil;
import com.day.cq.commons.Filter;
import com.day.cq.wcm.api.Page;
import org.apache.commons.lang3.StringEscapeUtils;
import com.day.cq.widget.HtmlLibraryManager;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class campaignpage_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public static String formatDate(Calendar date) {
        if (date == null) {
            return "not defined";
        } else {
            return dateFormat.format(date.getTime());
        }
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/libs/foundation/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');
	final String campaignTitle = StringEscapeUtils.escapeHtml4(currentPage.getTitle());
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("    <title>CQ5 Campaign | ");
      out.print( campaignTitle );
      out.write("</title>\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; utf-8\"/>\n");
      out.write("    ");

        final HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
        if (htmlMgr != null) {
            htmlMgr.writeIncludes(slingRequest, out, "cq.wcm.edit", "cq.security", "cq.personalization");
        }
    
      out.write("\n");
      out.write("    <script src=\"/libs/cq/ui/resources/cq-ui.js\" type=\"text/javascript\"></script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\t<h1>");
      out.print( campaignTitle );
      out.write("</h1>\n");

    final Iterator<Page> iterator = currentPage.listChildren();
    while (iterator.hasNext()) {
        final Page child = iterator.next();
        final I18n i18n = new I18n(slingRequest);
        final String imagePath = TeaserUtils.getImage(child);

        final String name = child.getName();
        final String title = StringEscapeUtils.escapeHtml4(child.getTitle());

        ValueMap content = child.getProperties();
        if (content == null) {
            content = ValueMap.EMPTY;
        }
        final String description = StringEscapeUtils.escapeHtml4(content.get("jcr:description", ""));

        String iconClass = "cq-teaser-header-on";
        if (!child.isValid()) {
        	iconClass = "cq-teaser-header-off";
        }

        long monthlyHits = 0;
        try {
            final PageViewStatistics statistics = sling.getService(PageViewStatistics.class);
            final Object[] hits = statistics.report(child);
            if (hits != null && hits.length > 2) {
                monthlyHits = (Long) hits[2];
            }
        } catch (WCMException ex) {
            monthlyHits = -1;
        }

    
      out.write("\n");
      out.write("    \n");
      out.write("    <h2 class=\"");
      out.print( iconClass );
      out.write("\"><a class=\"cq-teaser-header\" href=\"");
      out.print( child.getPath() );
      out.write(".html\">");
      out.print( title );
      out.write("</a></h2>\n");
      out.write("  \t<p>");
      out.print( description );
      out.write("</p>\n");
      out.write("    \n");
      out.write("    <img class=\"cq-teaser-img\" src=\"");
      out.print( imagePath );
      out.write("\" alt=\"");
      out.print( title );
      out.write("\" border=\"0\">\n");
      out.write("    <ul class=\"cq-teaser-data\">\n");
      out.write("        <li>\n");
      out.write("            <div class=\"li-bullet cq-teaser-status-");
      out.print( (child.isValid() ? "active" : "inactive") );
      out.write("\">\n");
      out.write("                ");
      out.print( child.isValid() ? i18n.get("Teaser is <strong>active</strong>:") : i18n.get("Teaser is <strong>inactive</strong>:") );
      out.write("\n");
      out.write("                ");
      out.print( i18n.get("on/off times are") );
      out.write("<strong>");
      out.print( formatDate(child.getOnTime()) );
      out.write(' ');
      out.write('/');
      out.write(' ');
      out.print( formatDate(child.getOffTime()) );
      out.write("</strong>\n");
      out.write("            </div>\n");
      out.write("        </li>\n");
      out.write("        <li>");
      out.print( i18n.get("Page tags are: ") );
      out.write("\n");
      out.write("            ");

              	final Tag[] tags = child.getTags();
                for (Tag tag : tags) {
            
      out.write("\n");
      out.write("            \t<strong title=\"");
      out.print( tag.getTitlePath() );
      out.write('"');
      out.write('>');
      out.print( title );
      out.write("</strong>&nbsp;\n");
      out.write("            ");

                }
            
      out.write("\n");
      out.write("        </li>\n");
      out.write("        <li>");
      out.print( i18n.get("Segments are:") );
      out.write("&nbsp;\n");
      out.write("            ");

                String[] segments = new String[]{};
                if (content.containsKey("cq:segments")) {
                    segments = content.get("cq:segments", segments);
                }
                for (String segment : segments) {
            
      out.write("\n");
      out.write("            \t\t<strong><a href=\"");
      out.print( segment );
      out.write(".html\">");
      out.print( Text.getName(segment) );
      out.write("</a></strong>&nbsp;\n");
      out.write("            ");

                }
            
      out.write("\n");
      out.write("        </li>\n");
      out.write("        <li>");
      out.print( i18n.get("Teaser has been viewed <strong>{0}</strong> times last month", null, monthlyHits) );
      out.write("</li>\n");
      out.write("\n");
      out.write("    </ul>\n");
      out.write("    <br>\n");
      out.write("    \n");

    }

      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
