package org.apache.jsp.apps.g2m_002dredesign.components.content.contactSales;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.util.Map;
import com.citrixosd.utils.Utilities;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class countryList_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {

 
    public class Country {
        private String value;
        private String name;
        private String title;
        
        public Country(String value,String name,String title){
            this.value = value;
            this.name = name;
            this.title = title;
        }
    
        public String getValue() {
            return this.value;
        }
        public String getName() {
            return this.name;
        }
        public String getTitle() {
            return this.title;
        }
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/libs/foundation/global.jsp");
    _jspx_dependants.add("/apps/citrixosd/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fforEach_005fvar_005fitems;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fforEach_005fvar_005fitems = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fforEach_005fvar_005fitems.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f1 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f1.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f1.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f1 = _jspx_th_cq_005fdefineObjects_005f1.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      if (_jspx_meth_swx_005fdefineObjects_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_citrixosd_005fdefineObjects_005f0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');
      if (_jspx_meth_cq_005fsetContentBundle_005f0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');
      out.write('\n');
      if (_jspx_meth_cq_005fsetContentBundle_005f1(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
 
    //using Utilities to parse structured multi field
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("selectCountry")) {
        final Node baseNode = currentNode.getNode("selectCountry");
        values = Utilities.parseStructuredMultifield(baseNode);
    }

    ArrayList<Country> countries = new ArrayList<Country>();
    countries.add(new Country("Albania","form.country.al","EMEA"));
    countries.add(new Country("Algeria","form.country.dz","EMEA"));
    countries.add(new Country("American Samoa","form.country.as",""));
    countries.add(new Country("Andorra","form.country.ad",""));
    countries.add(new Country("Angola","form.country.ao","EMEA"));
    countries.add(new Country("Anguilla","form.country.ai",""));
    countries.add(new Country("Antarctica","form.country.aq",""));
    countries.add(new Country("Antigua/Barbuda","form.country.ag",""));
    countries.add(new Country("Argentina","form.country.ar",""));
    countries.add(new Country("Armenia","form.country.am",""));
    countries.add(new Country("Aruba","form.country.aw",""));
    countries.add(new Country("Australia","form.country.au","APAC"));
    countries.add(new Country("Austria","form.country.at","EMEA"));
    countries.add(new Country("Azerbaijan","form.country.az","EMEA"));
    countries.add(new Country("Bahamas","form.country.bs",""));
    countries.add(new Country("Bahrain","form.country.bh","EMEA"));
    countries.add(new Country("Bangladesh","form.country.bd","APAC"));
    countries.add(new Country("Barbados","form.country.bb",""));
    countries.add(new Country("Belarus","form.country.by",""));
    countries.add(new Country("Belgium","form.country.be","EMEA"));
    countries.add(new Country("Belize","form.country.bz",""));
    countries.add(new Country("Benin","form.country.bj",""));
    countries.add(new Country("Bermuda","form.country.bm",""));
    countries.add(new Country("Bhutan","form.country.bt","APAC"));
    countries.add(new Country("Bolivia","form.country.bo",""));
    countries.add(new Country("Bosnia-Herz.","form.country.ba",""));
    countries.add(new Country("Botswana","form.country.bw",""));
    countries.add(new Country("Bouvet Island","form.country.bv",""));
    countries.add(new Country("Brazil","form.country.br",""));
    countries.add(new Country("Brunei Darussalam","form.country.bn","APAC"));
    countries.add(new Country("Bulgaria","form.country.bg",""));
    countries.add(new Country("Burkina-Faso","form.country.bf",""));
    countries.add(new Country("Brunei Dar-es-S","form.country.bi",""));
    countries.add(new Country("Cambodia","form.country.kh","APAC"));
    countries.add(new Country("Cameroon","form.country.cm",""));
    countries.add(new Country("Canada","form.country.ca","NA"));
    countries.add(new Country("Cape Verde","form.country.cv",""));
    countries.add(new Country("Cayman Islands","form.country.ky",""));
    countries.add(new Country("Chad","form.country.td",""));
    countries.add(new Country("Chile","form.country.cl",""));
    countries.add(new Country("China","form.country.cn","APAC"));
    countries.add(new Country("Christmas Island","form.country.cx",""));
    countries.add(new Country("Colombia","form.country.co",""));
    countries.add(new Country("Comoros","form.country.km",""));
    countries.add(new Country("Congo","form.country.cg",""));
    countries.add(new Country("Cook Islands","form.country.ck",""));
    countries.add(new Country("Costa Rica","form.country.cr",""));
    countries.add(new Country("Cote d'Ivoire","form.country.ci",""));
    countries.add(new Country("Croatia","form.country.hr","EMEA"));
    countries.add(new Country("Cyprus","form.country.cy",""));
    countries.add(new Country("Czech Republic","form.country.cz","EMEA"));
    countries.add(new Country("Denmark","form.country.dk","EMEA"));
    countries.add(new Country("Djibouti","form.country.dj",""));
    countries.add(new Country("Dominica","form.country.dm",""));
    countries.add(new Country("Dominican Rep.","form.country.do",""));
    countries.add(new Country("East Timor","form.country.tp","APAC"));
    countries.add(new Country("Ecuador","form.country.ec",""));
    countries.add(new Country("Egypt","form.country.eg","EMEA"));
    countries.add(new Country("El Salvador","form.country.sv",""));
    countries.add(new Country("Equatorial Gui.","form.country.gq",""));
    countries.add(new Country("Eritrea","form.country.er",""));
    countries.add(new Country("Estonia","form.country.ee",""));
    countries.add(new Country("Ethiopia","form.country.et","EMEA"));
    countries.add(new Country("Falkland Islands","form.country.fk",""));
    countries.add(new Country("Faroe Islands","form.country.fo",""));
    countries.add(new Country("Fiji","form.country.fj","APAC"));
    countries.add(new Country("Finland","form.country.fi","EMEA"));
    countries.add(new Country("France","form.country.fr","EMEA"));
    countries.add(new Country("French Guayana","form.country.gf",""));
    countries.add(new Country("Frenc.Polynesia","form.country.pf",""));
    countries.add(new Country("Gabon","form.country.ga",""));
    countries.add(new Country("Gambia","form.country.gm",""));
    countries.add(new Country("Georgia","form.country.ge",""));
    countries.add(new Country("Germany","form.country.de","EMEA"));
    countries.add(new Country("Ghana","form.country.gh","EMEA"));
    countries.add(new Country("Gibraltar","form.country.gi",""));
    countries.add(new Country("Greece","form.country.gr","EMEA"));
    countries.add(new Country("Greenland","form.country.gl",""));
    countries.add(new Country("Grenada","form.country.gd",""));
    countries.add(new Country("Guadeloupe","form.country.gp",""));
    countries.add(new Country("Guatemala","form.country.gt",""));
    countries.add(new Country("Guinea","form.country.gn",""));
    countries.add(new Country("Guinea-Bissau","form.country.gw",""));
    countries.add(new Country("Guyana","form.country.gy",""));
    countries.add(new Country("Haiti","form.country.ht",""));
    countries.add(new Country("Honduras","form.country.hn",""));
    countries.add(new Country("Hong Kong","form.country.hk","APAC"));
    countries.add(new Country("Hungary","form.country.hu","EMEA"));
    countries.add(new Country("Iceland","form.country.is","EMEA"));
    countries.add(new Country("India","form.country.in","APAC"));
    countries.add(new Country("Indonesia","form.country.id","APAC"));
    countries.add(new Country("Ireland","form.country.ie","EMEA"));
    countries.add(new Country("Israel","form.country.il","EMEA"));
    countries.add(new Country("Italy","form.country.it","EMEA"));
    countries.add(new Country("Jamaica","form.country.jm",""));
    countries.add(new Country("Japan","form.country.jp","APAC"));
    countries.add(new Country("Jordan","form.country.jo","EMEA"));
    countries.add(new Country("Kazakhstan","form.country.kz",""));
    countries.add(new Country("Kenya","form.country.ke","EMEA"));
    countries.add(new Country("Kiribati","form.country.ki","APAC"));
    countries.add(new Country("South Korea","form.country.kp","APAC"));
    countries.add(new Country("Kuwait","form.country.kw","EMEA"));
    countries.add(new Country("Kyrgyzstan","form.country.kg",""));
    countries.add(new Country("Laos","form.country.la","APAC"));
    countries.add(new Country("Latvia","form.country.lv",""));
    countries.add(new Country("Lebanon","form.country.lb",""));
    countries.add(new Country("Lesotho","form.country.ls",""));
    countries.add(new Country("Liberia","form.country.lr",""));
    countries.add(new Country("Liechtenstein","form.country.li",""));
    countries.add(new Country("Lithuania","form.country.lt",""));
    countries.add(new Country("Luxembourg","form.country.lu","EMEA"));
    countries.add(new Country("Macau","form.country.mo","APAC"));
    countries.add(new Country("Macedonia","form.country.mk",""));
    countries.add(new Country("Madagascar","form.country.mg",""));
    countries.add(new Country("Malawi","form.country.mw",""));
    countries.add(new Country("Malaysia","form.country.my","APAC"));
    countries.add(new Country("Maldives","form.country.mv","APAC"));
    countries.add(new Country("Mali","form.country.ml",""));
    countries.add(new Country("Malta","form.country.mt","EMEA"));
    countries.add(new Country("Marshall Islands","form.country.mh","APAC"));
    countries.add(new Country("Martinique","form.country.mq",""));
    countries.add(new Country("Mauritania","form.country.mr",""));
    countries.add(new Country("Mauritius","form.country.mu",""));
    countries.add(new Country("Mayotte","form.country.yt",""));
    countries.add(new Country("Mexico","form.country.mx",""));
    countries.add(new Country("Micronesia","form.country.fm","APAC"));
    countries.add(new Country("Moldavia","form.country.md",""));
    countries.add(new Country("Monaco","form.country.mc",""));
    countries.add(new Country("Mongolia","form.country.mn","APAC"));
    countries.add(new Country("Montserrat","form.country.ms",""));
    countries.add(new Country("Morocco","form.country.ma","EMEA"));
    countries.add(new Country("Mozambique","form.country.mz",""));
    countries.add(new Country("Myanmar","form.country.mm","APAC"));
    countries.add(new Country("Namibia","form.country.na",""));
    countries.add(new Country("Nauru","form.country.nr","APAC"));
    countries.add(new Country("Nepal","form.country.np","APAC"));
    countries.add(new Country("Netherlands","form.country.nl","EMEA"));
    countries.add(new Country("Netherlands Antilles","form.country.an",""));
    countries.add(new Country("New Caledonia","form.country.nc",""));
    countries.add(new Country("New Zealand","form.country.nz","APAC"));
    countries.add(new Country("Nicaragua","form.country.ni",""));
    countries.add(new Country("Niger","form.country.ne",""));
    countries.add(new Country("Nigeria","form.country.ng","EMEA"));
    countries.add(new Country("Niue","form.country.nu",""));
    countries.add(new Country("Norfolk Island","form.country.nf",""));
    countries.add(new Country("Norway","form.country.no","EMEA"));
    countries.add(new Country("Oman","form.country.om","EMEA"));
    countries.add(new Country("Pakistan","form.country.pk","APAC"));
    countries.add(new Country("Palau","form.country.pw","APAC"));
    countries.add(new Country("Panama","form.country.pa",""));
    countries.add(new Country("Papua Nw Guinea","form.country.pg","APAC"));
    countries.add(new Country("Paraguay","form.country.py",""));
    countries.add(new Country("Peru","form.country.pe",""));
    countries.add(new Country("Philippines","form.country.ph","APAC"));
    countries.add(new Country("Pitcairn","form.country.pn",""));
    countries.add(new Country("Poland","form.country.pl","EMEA"));
    countries.add(new Country("Portugal","form.country.pt","EMEA"));
    countries.add(new Country("Qatar","form.country.qa",""));
    countries.add(new Country("Reunion","form.country.re",""));
    countries.add(new Country("Romania","form.country.ro","EMEA"));
    countries.add(new Country("Russian Fed.","form.country.ru","EMEA"));
    countries.add(new Country("Rwanda","form.country.rw",""));
    countries.add(new Country("St Kitts&Nevis","form.country.kn",""));
    countries.add(new Country("Saint Lucia","form.country.lc",""));
    countries.add(new Country("Western Samoa","form.country.ws","APAC"));
    countries.add(new Country("S.Tome,Principe","form.country.st",""));
    countries.add(new Country("Saudi Arabia","form.country.sa","EMEA"));
    countries.add(new Country("Senegal","form.country.sn",""));
    countries.add(new Country("Seychelles","form.country.sc",""));
    countries.add(new Country("Sierra Leone","form.country.sl",""));
    countries.add(new Country("Singapore","form.country.sg","APAC"));
    countries.add(new Country("Slovakia","form.country.sk","EMEA"));
    countries.add(new Country("Slovenia","form.country.si","EMEA"));
    countries.add(new Country("Solomon Islands","form.country.sb","APAC"));
    countries.add(new Country("Somalia","form.country.so",""));
    countries.add(new Country("South Africa","form.country.za","EMEA"));
    countries.add(new Country("Spain","form.country.es","EMEA"));
    countries.add(new Country("Sri Lanka","form.country.lk","APAC"));
    countries.add(new Country("St. Helena","form.country.sh",""));
    countries.add(new Country("Suriname","form.country.sr",""));
    countries.add(new Country("Swaziland","form.country.sz",""));
    countries.add(new Country("Sweden","form.country.se","EMEA"));
    countries.add(new Country("Switzerland","form.country.ch","EMEA"));
    countries.add(new Country("Taiwan","form.country.tw","APAC"));
    countries.add(new Country("Tajikistan","form.country.tj",""));
    countries.add(new Country("Tanzania","form.country.tz","EMEA"));
    countries.add(new Country("Thailand","form.country.th","APAC"));
    countries.add(new Country("Togo","form.country.tg",""));
    countries.add(new Country("Tokelau","form.country.tk",""));
    countries.add(new Country("Tonga","form.country.to","APAC"));
    countries.add(new Country("Trinidad,Tobago","form.country.tt",""));
    countries.add(new Country("Tunisia","form.country.tn",""));
    countries.add(new Country("Turkey","form.country.tr","EMEA"));
    countries.add(new Country("Turkmenistan","form.country.tm",""));
    countries.add(new Country("Tuvalu","form.country.tv","APAC"));
    countries.add(new Country("Uganda","form.country.ug","EMEA"));
    countries.add(new Country("Ukraine","form.country.ua",""));
    countries.add(new Country("Utd.Arab Emir.","form.country.ae","EMEA"));
    countries.add(new Country("United Kingdom","form.country.gb","EMEA"));
    countries.add(new Country("USA","form.country.us","NA"));
    countries.add(new Country("Uruguay","form.country.uy",""));
    countries.add(new Country("Uzbekistan","form.country.uz",""));
    countries.add(new Country("Vanuatu","form.country.vu","APAC"));
    countries.add(new Country("Vatican City","form.country.va",""));
    countries.add(new Country("Venezuela","form.country.ve",""));
    countries.add(new Country("Vietnam","form.country.vn","APAC"));
    countries.add(new Country("Western Sahara","form.country.eh",""));
    countries.add(new Country("Yemen","form.country.ye",""));
    countries.add(new Country("Zambia","form.country.zm",""));
    countries.add(new Country("Zimbabwe","form.country.zw",""));
    countries.add(new Country("Other","Other",""));
    
    System.out.println(countries.size());
    
    for(int i = values.size(); i >= 1 ; i--) {
        final String countryName = values.get(i-1).get("countryName").getString();
        for(int j = 0; j < countries.size(); j++) {
            //System.out.println(countries.get(j).getValue());
            if(countries.get(j).getValue().equals(countryName)) {   
                countries.add(0, countries.get(j));
                countries.remove(j);
            }
        }
    }
    

      out.write("\n");
      out.write("\n");
      out.write("    ");
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_005fset_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fset_005f0.setParent(null);
      // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(274,4) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setVar("countries");
      // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(274,4) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setValue( countries );
      int _jspx_eval_c_005fset_005f0 = _jspx_th_c_005fset_005f0.doStartTag();
      if (_jspx_th_c_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
      out.write("\n");
      out.write("        \n");
      out.write("    <option value=''>");
      if (_jspx_meth_fmt_005fmessage_005f0(_jspx_page_context))
        return;
      out.write("</option>\n");
      out.write(" \n");
      out.write("    ");
      if (_jspx_meth_c_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("   ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_swx_005fdefineObjects_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  swx:defineObjects
    com.siteworx.tags.DefineObjectsTag _jspx_th_swx_005fdefineObjects_005f0 = (com.siteworx.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.get(com.siteworx.tags.DefineObjectsTag.class);
    _jspx_th_swx_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
    _jspx_th_swx_005fdefineObjects_005f0.setParent(null);
    int _jspx_eval_swx_005fdefineObjects_005f0 = _jspx_th_swx_005fdefineObjects_005f0.doStartTag();
    if (_jspx_th_swx_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.reuse(_jspx_th_swx_005fdefineObjects_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.reuse(_jspx_th_swx_005fdefineObjects_005f0);
    return false;
  }

  private boolean _jspx_meth_citrixosd_005fdefineObjects_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  citrixosd:defineObjects
    com.citrixosd.tags.DefineObjectsTag _jspx_th_citrixosd_005fdefineObjects_005f0 = (com.citrixosd.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.get(com.citrixosd.tags.DefineObjectsTag.class);
    _jspx_th_citrixosd_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
    _jspx_th_citrixosd_005fdefineObjects_005f0.setParent(null);
    int _jspx_eval_citrixosd_005fdefineObjects_005f0 = _jspx_th_citrixosd_005fdefineObjects_005f0.doStartTag();
    if (_jspx_th_citrixosd_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.reuse(_jspx_th_citrixosd_005fdefineObjects_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.reuse(_jspx_th_citrixosd_005fdefineObjects_005f0);
    return false;
  }

  private boolean _jspx_meth_cq_005fsetContentBundle_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:setContentBundle
    com.day.cq.wcm.tags.SetContentBundleTag _jspx_th_cq_005fsetContentBundle_005f0 = (com.day.cq.wcm.tags.SetContentBundleTag) _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.get(com.day.cq.wcm.tags.SetContentBundleTag.class);
    _jspx_th_cq_005fsetContentBundle_005f0.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fsetContentBundle_005f0.setParent(null);
    int _jspx_eval_cq_005fsetContentBundle_005f0 = _jspx_th_cq_005fsetContentBundle_005f0.doStartTag();
    if (_jspx_th_cq_005fsetContentBundle_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f0);
    return false;
  }

  private boolean _jspx_meth_cq_005fsetContentBundle_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:setContentBundle
    com.day.cq.wcm.tags.SetContentBundleTag _jspx_th_cq_005fsetContentBundle_005f1 = (com.day.cq.wcm.tags.SetContentBundleTag) _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.get(com.day.cq.wcm.tags.SetContentBundleTag.class);
    _jspx_th_cq_005fsetContentBundle_005f1.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fsetContentBundle_005f1.setParent(null);
    int _jspx_eval_cq_005fsetContentBundle_005f1 = _jspx_th_cq_005fsetContentBundle_005f1.doStartTag();
    if (_jspx_th_cq_005fsetContentBundle_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f1);
    return false;
  }

  private boolean _jspx_meth_fmt_005fmessage_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_005fmessage_005f0 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_005fmessage_005f0.setPageContext(_jspx_page_context);
    _jspx_th_fmt_005fmessage_005f0.setParent(null);
    // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(276,21) name = key type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_fmt_005fmessage_005f0.setKey("form.option.country");
    int _jspx_eval_fmt_005fmessage_005f0 = _jspx_th_fmt_005fmessage_005f0.doStartTag();
    if (_jspx_th_fmt_005fmessage_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fforEach_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fc_005fforEach_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_005fforEach_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fforEach_005f0.setParent(null);
    // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(278,4) name = items type = java.lang.Object reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f0.setItems((java.lang.Object) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${countries}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(278,4) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f0.setVar("country");
    int[] _jspx_push_body_count_c_005fforEach_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_c_005fforEach_005f0 = _jspx_th_c_005fforEach_005f0.doStartTag();
      if (_jspx_eval_c_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("        <option title=\"");
          out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${country.title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write("\" value=\"");
          out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${country.value}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
          out.write('"');
          out.write('>');
          if (_jspx_meth_fmt_005fmessage_005f1(_jspx_th_c_005fforEach_005f0, _jspx_page_context, _jspx_push_body_count_c_005fforEach_005f0))
            return true;
          out.write("</option>\n");
          out.write("    ");
          int evalDoAfterBody = _jspx_th_c_005fforEach_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_005fforEach_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_005fforEach_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_005fforEach_005f0.doFinally();
      _005fjspx_005ftagPool_005fc_005fforEach_005fvar_005fitems.reuse(_jspx_th_c_005fforEach_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_fmt_005fmessage_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_005fforEach_005f0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_005fforEach_005f0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_005fmessage_005f1 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_005fmessage_005f1.setPageContext(_jspx_page_context);
    _jspx_th_fmt_005fmessage_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fforEach_005f0);
    // /apps/g2m-redesign/components/content/contactSales/countryList.jsp(279,66) name = key type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_fmt_005fmessage_005f1.setKey((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${country.name}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
    int _jspx_eval_fmt_005fmessage_005f1 = _jspx_th_fmt_005fmessage_005f1.doStartTag();
    if (_jspx_th_fmt_005fmessage_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005ffmt_005fmessage_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f1);
    return false;
  }
}
