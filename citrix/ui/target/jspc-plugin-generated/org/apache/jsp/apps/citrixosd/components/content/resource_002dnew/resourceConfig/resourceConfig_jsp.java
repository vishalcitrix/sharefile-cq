package org.apache.jsp.apps.citrixosd.components.content.resource_002dnew.resourceConfig;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.sling.api.SlingHttpServletRequest;
import com.siteworx.rewrite.transformer.ContextRootTransformer;
import com.day.cq.personalization.ClientContextUtil;
import com.citrixosd.utils.Utilities;
import com.citrixosd.resources.ResourceUtils;
import com.citrixosd.resources.models.ResourceCategoryeModel;
import com.citrixosd.enums.ResourceType;
import java.util.Properties;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.util.TimeZone;
import javax.jcr.Value;
import java.util.ArrayList;
import java.util.Iterator;
import com.day.cq.tagging.TagManager;
import com.day.cq.tagging.Tag;
import java.util.ResourceBundle;
import com.citrixosd.utils.TranslationUtil;
import java.util.Locale;
import java.util.Map;
import java.util.HashMap;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;
import javax.jcr.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.commons.WCMUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.EditContext;

public final class resourceConfig_jsp extends org.apache.sling.scripting.jsp.jasper.runtime.HttpJspBase
    implements org.apache.sling.scripting.jsp.jasper.runtime.JspSourceDependent {


	public static String resourceDocument(Iterator<Node> nodeIter, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
		
		String finalHtml = "";
		
		//pagination
		final int pagination = limit > 0 ? (int) Math.ceil((double)size/limit) : 1;
		limit = limit > 0 ? limit : size;
		
		for(int page = 1; page <= pagination; page++){
			String pageShow = page == 1 ? "" : "style='display:none'";
			finalHtml = finalHtml.concat("<ul class='document-container' data-resource='#document-container-" + page + "' " + pageShow + ">");
			for(int limitSet = 0; limitSet < limit; limitSet++){
				if(nodeIter.hasNext()) {
					String credit = null;
					String creditPath = null;
					String nodeLink = null;
					String nodeTitle = "";
					boolean isGated = false;
					
					Node currNode = nodeIter.next();
				
					isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
					nodeLink = isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath();
			
					if(currNode.hasNode("resourceContainer")){
						final Node resContainerNode = currNode.getNode("resourceContainer");
						nodeTitle = resContainerNode.hasProperty("documentTitle") ? resContainerNode.getProperty("documentTitle").getString() : null;
						credit = resContainerNode.hasProperty("credit") ? resContainerNode.getProperty("credit").getString() : null;
						creditPath = resContainerNode.hasProperty("creditPath") ? resContainerNode.getProperty("creditPath").getString() : null;
					}
			
					//creating output
					if(limitSet % 2 == 0){
						finalHtml = finalHtml.concat("<li class='even'>");
					}else {
						finalHtml = finalHtml.concat("<li class='odd'>");
					}
			
					finalHtml = finalHtml.concat("<div class='title'>");
						if(isGated){
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>" + nodeTitle + "</a>");
						}else {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>" + nodeTitle + "</a>");
						}
					finalHtml = finalHtml.concat("</div>");
		
					finalHtml = finalHtml.concat("<div class='credit'>");
						if(credit != null) {
							if(creditPath != null){
								finalHtml = finalHtml.concat("<a href='" + creditPath + "' target='_blank'>" + credit + "</a>");
							}else {
								finalHtml = finalHtml.concat(credit);
							}
						}
					finalHtml = finalHtml.concat("</div>");
		
					if(isGated){
						finalHtml = finalHtml.concat("<div class='gated'><span class='lock'></span></div>");
					}
		
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
					finalHtml = finalHtml.concat("</li>");
				}
			}
			finalHtml = finalHtml.concat("</ul>");	
		}
		
		if(limit != size){
			finalHtml = finalHtml.concat("<div class='pagination' style='display:block;'>");
				finalHtml = finalHtml.concat("<div class='pagination-border'>");
					finalHtml = finalHtml.concat("<ul class='pagination-container'>");
						for(int page = 1; page <= pagination; page++){
							finalHtml = finalHtml.concat("<li><a href='#document-container-" + page + "'>" + page + "</a></li>");
						}
					finalHtml = finalHtml.concat("</ul>");
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</div>");
		}
		
		return finalHtml;
	}
	
	public static String resourceWebinar(Iterator<Node> nodeIter, boolean webinarUpcomingOnly, TagManager tagManager, Locale locale, String timeZone, SlingHttpServletRequest slingRequest, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
	
		String finalHtml = "";
		
		final DateFormat longDateFormatter = DateFormat.getDateInstance(DateFormat.LONG, locale);
		final DateFormat shortTimeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		
		//Set the time zone for the dates
		if(timeZone != null && timeZone.trim().length() > 0) {
			longDateFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
			shortTimeFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
		}
		
		//pagination
		final int pagination = limit > 0 ? (int) Math.ceil((double)size/limit) : 1;
		limit = limit > 0 ? limit : size;

		for(int page = 1; page <= pagination; page++) {
			String pageShow = page == 1 ? "":"style='display:none'";
			finalHtml = finalHtml.concat("<ul class='webinar-container' data-resource='#webinar-container-" + page + "' " + pageShow + ">");
			for(int limitSet = 0; limitSet < limit; limitSet++){
				if(nodeIter.hasNext()) {
					String nodeLink = null;
					String nodeTitle = "";
					String fileReference = "";
					String authorTitle = null;
					String authorDescription = null;
					String upcoming = null;
					String upcomingTime = null;
					boolean isGated = false;
					
					Node currNode = nodeIter.next();
					isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
					
					if(currNode.hasNode("resourceContainer")){
						final Node resContainerNode = currNode.getNode("resourceContainer");
						
						nodeTitle = resContainerNode.hasProperty("videoTitleOverride") ? resContainerNode.getProperty("videoTitleOverride").getString() : resContainerNode.hasProperty("videoTitle") ? resContainerNode.getProperty("videoTitle").getString() : null;
						fileReference = resContainerNode.hasProperty("fileReference") ? resContainerNode.hasProperty("rendition") ? resContainerNode.getProperty("rendition").getString() : resContainerNode.getProperty("fileReference").getString() : resContainerNode.hasProperty("videoThumbnailUrl") ? resContainerNode.getProperty("videoThumbnailUrl").getString(): null; 
						authorTitle = resContainerNode.hasProperty("authorTitle") ? resContainerNode.getProperty("authorTitle").getString() : null;
						authorDescription = resContainerNode.hasProperty("authorDescription") ? resContainerNode.getProperty("authorDescription").getString() : null;
						
						if(resContainerNode.hasProperty("webinarStartDate")) {
				    		final Date currentDate = new Date();
				    		final Date webinarDate = resContainerNode.getProperty("webinarStartDate").getDate().getTime();
				    		if(currentDate.before(webinarDate) || currentDate.equals(webinarDate)) {
				    			nodeLink = resContainerNode.hasProperty("registrationPath") ? resContainerNode.getProperty("registrationPath").getString() : null;
				    		}
						}
						
						if(webinarUpcomingOnly) {
		                    Date upcomingDate = resContainerNode.getProperty("webinarStartDate").getDate().getTime();
		                    upcoming = resContainerNode.hasProperty("webinarStartDate") ? longDateFormatter.format(upcomingDate) : null;
		                    upcomingTime = resContainerNode.hasProperty("webinarStartDate") ? shortTimeFormatter.format(upcomingDate) + " (" + shortTimeFormatter.getTimeZone().getDisplayName(shortTimeFormatter.getTimeZone().inDaylightTime(upcomingDate), TimeZone.SHORT) + ")" : null;
				    	}
					}
					
					nodeLink = nodeLink != null ? nodeLink : isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath();
					
					//get Categories
					final ResourceBundle resourceBundle = slingRequest.getResourceBundle(locale);
					final String[] categories = TranslationUtil.translateArray(Utilities.getStringArrayFromArrayProperty(currNode, "resourceCategories"), resourceBundle);
				
					//creating output
					if(limitSet % 2 == 0){
						finalHtml = finalHtml.concat("<li class='even'>");
					}else {
						finalHtml = finalHtml.concat("<li class='odd'>");
					}
						finalHtml = finalHtml.concat("<div class='author-container'>");
							finalHtml = finalHtml.concat("<div class='author-thumbnail-container'>");
								finalHtml = finalHtml.concat("<img src='" + fileReference + "'/>");
							finalHtml = finalHtml.concat("</div>");
								
							finalHtml = finalHtml.concat("<div class='author-description-container'>");
								finalHtml = finalHtml.concat("<div class='title'>");
									if(isGated){
										finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>" + nodeTitle + "</a>");
									}else {
										finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>" + nodeTitle + "</a>");
									}
									
									if(authorTitle != null) {
										finalHtml = finalHtml.concat("<div class='author-title'><strong>" + authorTitle + "</strong></div>");	
									}
									
									if(authorDescription != null) {
										finalHtml = finalHtml.concat("<div class='author-description ellipsis'>" + authorDescription + "</div>");	
									}
								finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
						finalHtml = finalHtml.concat("</div>");
						
						finalHtml = finalHtml.concat("<div class='categories-container'><div class='categories-container-margin'>");
							if(upcoming != null){
								finalHtml = finalHtml.concat("<div>");
								finalHtml = finalHtml.concat("<strong>" + upcoming + "</strong>");
								if(upcomingTime != null) {
									finalHtml = finalHtml.concat("<br/>");
									finalHtml = finalHtml.concat("<strong>" + upcomingTime + "</strong>");
								}
								finalHtml = finalHtml.concat("</div>");
							}
							
							if(categories != null && categories.length > 0) {
								for(int j = 0; j < categories.length; j++) {
									finalHtml = finalHtml.concat("<div>" + categories[j] + "</div>");
								}
							}
							finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
					finalHtml = finalHtml.concat("</li>");
				}
			}
			finalHtml = finalHtml.concat("</ul>");
		}
		
		if(limit != size){
			finalHtml = finalHtml.concat("<div class='pagination' style='display:block;'>");
				finalHtml = finalHtml.concat("<div class='pagination-border'>");
					finalHtml = finalHtml.concat("<ul class='pagination-container'>");
						for(int page = 1; page <= pagination; page++){
							finalHtml = finalHtml.concat("<li><a href='#webinar-container-" + page + "'>" + page + "</a></li>");
						}
					finalHtml = finalHtml.concat("</ul>");
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</div>");
		}
		
		return finalHtml;
	}
	
	public static String resourceVideo(Iterator<Node> nodeIter, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
		
		int i = 0;
		String finalHtml = "<ul class='video-container'>";
		
		while(nodeIter.hasNext()) {
			if(i %3 == 0){
				if(i > 0){
					finalHtml = finalHtml.concat("</ul></li>");
				}
				finalHtml = finalHtml.concat("<li><ul class='video-row'>");
			}
			
			Node currNode = nodeIter.next();
			String nodeLink = "";
			String nodeTitle = "";
			String thumbnailPath = "";
			String videoLength = "";
			boolean isGated = false;
			
			isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
			nodeLink = isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath(); 
					
			if(currNode.hasNode("resourceContainer")){
				final Node resContainerNode = currNode.getNode("resourceContainer");
				
				nodeTitle = resContainerNode.hasProperty("videoTitleOverride") ? resContainerNode.getProperty("videoTitleOverride").getString() : resContainerNode.hasProperty("videoTitle") ? resContainerNode.getProperty("videoTitle").getString() : null;
				thumbnailPath = resContainerNode.hasProperty("fileReference") ? resContainerNode.hasProperty("rendition") ? resContainerNode.getProperty("rendition").getString() : resContainerNode.getProperty("fileReference").getString() : resContainerNode.hasProperty("videoThumbnailUrl") ? resContainerNode.getProperty("videoThumbnailUrl").getString(): null;
				videoLength = resContainerNode.hasProperty("videoHumanLength") ? resContainerNode.getProperty("videoHumanLength").getString() : null;
			}
			
			finalHtml = finalHtml.concat("<li>");
				if(isGated){
					finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>");
				}else {
					finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>");
				}
				
						finalHtml = finalHtml.concat("<div class='thumbnail-container'>");
							finalHtml = finalHtml.concat("<img src='" + thumbnailPath + "'/>");
							finalHtml = finalHtml.concat("<div class='video-play-sprite'></div>");
							finalHtml = finalHtml.concat("<div class='video-duration'>" + videoLength + "</div>");
						finalHtml = finalHtml.concat("</div>");
					finalHtml = finalHtml.concat("</a>");
				
					finalHtml = finalHtml.concat("<div class='title-container'>");
					finalHtml = finalHtml.concat("<span class='small-right-arrow-orange'></span>");
						if(isGated) {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' class='video-title ellipsis' target='_blank'>" + nodeTitle + "</a>");
						}else {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' class='video-title ellipsis'>" + nodeTitle + "</a>");
						}
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</li>");
			i++;
		}
	
		finalHtml = finalHtml.concat("</ul>");
		finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
		return finalHtml;
	}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/libs/foundation/global.jsp");
    _jspx_dependants.add("/apps/citrixosd/global.jsp");
  }

  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody;
  private org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_005ftest;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fc_005fif_005ftest = org.apache.sling.scripting.jsp.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.sling.scripting.jsp.jasper.runtime.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.release();
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fc_005fif_005ftest.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f0 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f0.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f0 = _jspx_th_cq_005fdefineObjects_005f0.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f0);
      org.apache.sling.api.SlingHttpServletRequest slingRequest = null;
      org.apache.sling.api.SlingHttpServletResponse slingResponse = null;
      org.apache.sling.api.resource.Resource resource = null;
      javax.jcr.Node currentNode = null;
      org.apache.sling.api.resource.ResourceResolver resourceResolver = null;
      org.apache.sling.api.scripting.SlingScriptHelper sling = null;
      org.slf4j.Logger log = null;
      org.apache.sling.api.scripting.SlingBindings bindings = null;
      com.day.cq.wcm.api.components.ComponentContext componentContext = null;
      com.day.cq.wcm.api.components.EditContext editContext = null;
      org.apache.sling.api.resource.ValueMap properties = null;
      com.day.cq.wcm.api.PageManager pageManager = null;
      com.day.cq.wcm.api.Page currentPage = null;
      com.day.cq.wcm.api.Page resourcePage = null;
      com.day.cq.commons.inherit.InheritanceValueMap pageProperties = null;
      com.day.cq.wcm.api.components.Component component = null;
      com.day.cq.wcm.api.designer.Designer designer = null;
      com.day.cq.wcm.api.designer.Design currentDesign = null;
      com.day.cq.wcm.api.designer.Design resourceDesign = null;
      com.day.cq.wcm.api.designer.Style currentStyle = null;
      com.adobe.granite.xss.XSSAPI xssAPI = null;
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write('\n');










      //  cq:defineObjects
      com.day.cq.wcm.tags.DefineObjectsTag _jspx_th_cq_005fdefineObjects_005f1 = (com.day.cq.wcm.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.get(com.day.cq.wcm.tags.DefineObjectsTag.class);
      _jspx_th_cq_005fdefineObjects_005f1.setPageContext(_jspx_page_context);
      _jspx_th_cq_005fdefineObjects_005f1.setParent(null);
      int _jspx_eval_cq_005fdefineObjects_005f1 = _jspx_th_cq_005fdefineObjects_005f1.doStartTag();
      if (_jspx_th_cq_005fdefineObjects_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fcq_005fdefineObjects_005fnobody.reuse(_jspx_th_cq_005fdefineObjects_005f1);
      slingRequest = (org.apache.sling.api.SlingHttpServletRequest) _jspx_page_context.findAttribute("slingRequest");
      slingResponse = (org.apache.sling.api.SlingHttpServletResponse) _jspx_page_context.findAttribute("slingResponse");
      resource = (org.apache.sling.api.resource.Resource) _jspx_page_context.findAttribute("resource");
      currentNode = (javax.jcr.Node) _jspx_page_context.findAttribute("currentNode");
      resourceResolver = (org.apache.sling.api.resource.ResourceResolver) _jspx_page_context.findAttribute("resourceResolver");
      sling = (org.apache.sling.api.scripting.SlingScriptHelper) _jspx_page_context.findAttribute("sling");
      log = (org.slf4j.Logger) _jspx_page_context.findAttribute("log");
      bindings = (org.apache.sling.api.scripting.SlingBindings) _jspx_page_context.findAttribute("bindings");
      componentContext = (com.day.cq.wcm.api.components.ComponentContext) _jspx_page_context.findAttribute("componentContext");
      editContext = (com.day.cq.wcm.api.components.EditContext) _jspx_page_context.findAttribute("editContext");
      properties = (org.apache.sling.api.resource.ValueMap) _jspx_page_context.findAttribute("properties");
      pageManager = (com.day.cq.wcm.api.PageManager) _jspx_page_context.findAttribute("pageManager");
      currentPage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("currentPage");
      resourcePage = (com.day.cq.wcm.api.Page) _jspx_page_context.findAttribute("resourcePage");
      pageProperties = (com.day.cq.commons.inherit.InheritanceValueMap) _jspx_page_context.findAttribute("pageProperties");
      component = (com.day.cq.wcm.api.components.Component) _jspx_page_context.findAttribute("component");
      designer = (com.day.cq.wcm.api.designer.Designer) _jspx_page_context.findAttribute("designer");
      currentDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("currentDesign");
      resourceDesign = (com.day.cq.wcm.api.designer.Design) _jspx_page_context.findAttribute("resourceDesign");
      currentStyle = (com.day.cq.wcm.api.designer.Style) _jspx_page_context.findAttribute("currentStyle");
      xssAPI = (com.adobe.granite.xss.XSSAPI) _jspx_page_context.findAttribute("xssAPI");


    // add more initialization code here


      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      if (_jspx_meth_swx_005fdefineObjects_005f0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_citrixosd_005fdefineObjects_005f0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');
      if (_jspx_meth_cq_005fsetContentBundle_005f0(_jspx_page_context))
        return;
      out.write('\n');
      out.write('\n');

	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	final String title = properties.get("title", "");
	final String subtitle = properties.get("subtitle", "");
	
	//Resource type
	final String[] resourceTypes = properties.get("resourceType", String[].class);
	final Tag resourceTypeTag = Utilities.getFirstTag(resourceTypes, tagManager);
	
	//Categories
	final String[] categories = properties.get("categories", String[].class);
	final List<Tag> categoryTags = Utilities.getTags(categories, tagManager);
	final StringBuilder builder = new StringBuilder();
	for(int i = 0; i < categoryTags.size(); i++) {
		final Tag categoryTag = categoryTags.get(i);
		builder.append(categoryTag.getTagID());
		builder.append(i == categoryTags.size() - 1 ? "" : ",");
	}
	final String categoriesStr = builder.toString();
	
	final String[] defaultLanguage = properties.get("defaultLanguage", String[].class);
	final Tag defaultLanguageTag = Utilities.getFirstTag(defaultLanguage, tagManager);
	
	final String timeZone = properties.get("timeZone", "PST");
	final int limit = properties.get("limit", 0);
	final boolean webinarUpcomingOnly = properties.get("upcomingOnly", false);
	final Locale locale = Utilities.getLocale(currentPage,request);

      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_005fset_005f0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_005fset_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fset_005f0.setParent(null);
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(344,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setVar("title");
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(344,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f0.setValue( title );
      int _jspx_eval_c_005fset_005f0 = _jspx_th_c_005fset_005f0.doStartTag();
      if (_jspx_th_c_005fset_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f0);
      out.write('\n');
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_005fset_005f1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_005fset_005f1.setPageContext(_jspx_page_context);
      _jspx_th_c_005fset_005f1.setParent(null);
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(345,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f1.setVar("subtitle");
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(345,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f1.setValue( subtitle );
      int _jspx_eval_c_005fset_005f1 = _jspx_th_c_005fset_005f1.doStartTag();
      if (_jspx_th_c_005fset_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f1);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f1);
      out.write('\n');
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_005fset_005f2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_005fset_005f2.setPageContext(_jspx_page_context);
      _jspx_th_c_005fset_005f2.setParent(null);
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(346,0) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f2.setVar("resourceTypeTag");
      // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(346,0) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fset_005f2.setValue( resourceTypeTag );
      int _jspx_eval_c_005fset_005f2 = _jspx_th_c_005fset_005f2.doStartTag();
      if (_jspx_th_c_005fset_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f2);
        return;
      }
      _005fjspx_005ftagPool_005fc_005fset_005fvar_005fvalue_005fnobody.reuse(_jspx_th_c_005fset_005f2);
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"resourceCategories\">\n");
      out.write("\t<div class=\"");
      out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${resourceTypeTag.name}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\">\n");
      out.write("\t\t");
      if (_jspx_meth_c_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t\n");
      out.write("\t\t<!-- Displaying Resources -->\n");
      out.write("\t\t");

			if(resourceTypeTag != null && defaultLanguageTag != null) {	
				//ResourceTypeModel is a model in core/resources
				ResourceCategoryeModel resourcesSet = new ResourceCategoryeModel();
		
				resourcesSet.setResourceType(resourceTypeTag.getName());
				resourcesSet.setCategories(categoriesStr);
				resourcesSet.setDefaultLanguage(defaultLanguageTag.getTagID());
				resourcesSet.setUpComingOnly(webinarUpcomingOnly);
				
			    //call Resources Utilities
			    List<Node> nodeList = ResourceUtils.getAllResources(resourcesSet, slingRequest);
			   	
			    ResourceType rt = ResourceType.getEnum(resourceTypeTag.getName());
			    
			    if(rt != null && nodeList.size() > 0) {
			    	Iterator<Node> nodeIter = nodeList.iterator();
			    	String finalHtml = "";
					switch(rt) {
						case ResourceDocument:
							finalHtml = resourceDocument(nodeIter, nodeList.size(), limit);
							break;
						case ResourceWebinar:
							finalHtml = resourceWebinar(nodeIter,webinarUpcomingOnly, tagManager, locale, timeZone, slingRequest, nodeList.size(), limit);
							break;
						case ResourceVideo:
							finalHtml = resourceVideo(nodeIter, nodeList.size(), limit);
							break;
						default:
							System.out.println("Resource is not defined");
							break;
					}
					out.println(finalHtml);
				}else {
					out.println("No Resources Found");
				}		    
			}
		
      out.write("\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_swx_005fdefineObjects_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  swx:defineObjects
    com.siteworx.tags.DefineObjectsTag _jspx_th_swx_005fdefineObjects_005f0 = (com.siteworx.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.get(com.siteworx.tags.DefineObjectsTag.class);
    _jspx_th_swx_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
    _jspx_th_swx_005fdefineObjects_005f0.setParent(null);
    int _jspx_eval_swx_005fdefineObjects_005f0 = _jspx_th_swx_005fdefineObjects_005f0.doStartTag();
    if (_jspx_th_swx_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.reuse(_jspx_th_swx_005fdefineObjects_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fswx_005fdefineObjects_005fnobody.reuse(_jspx_th_swx_005fdefineObjects_005f0);
    return false;
  }

  private boolean _jspx_meth_citrixosd_005fdefineObjects_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  citrixosd:defineObjects
    com.citrixosd.tags.DefineObjectsTag _jspx_th_citrixosd_005fdefineObjects_005f0 = (com.citrixosd.tags.DefineObjectsTag) _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.get(com.citrixosd.tags.DefineObjectsTag.class);
    _jspx_th_citrixosd_005fdefineObjects_005f0.setPageContext(_jspx_page_context);
    _jspx_th_citrixosd_005fdefineObjects_005f0.setParent(null);
    int _jspx_eval_citrixosd_005fdefineObjects_005f0 = _jspx_th_citrixosd_005fdefineObjects_005f0.doStartTag();
    if (_jspx_th_citrixosd_005fdefineObjects_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.reuse(_jspx_th_citrixosd_005fdefineObjects_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcitrixosd_005fdefineObjects_005fnobody.reuse(_jspx_th_citrixosd_005fdefineObjects_005f0);
    return false;
  }

  private boolean _jspx_meth_cq_005fsetContentBundle_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  cq:setContentBundle
    com.day.cq.wcm.tags.SetContentBundleTag _jspx_th_cq_005fsetContentBundle_005f0 = (com.day.cq.wcm.tags.SetContentBundleTag) _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.get(com.day.cq.wcm.tags.SetContentBundleTag.class);
    _jspx_th_cq_005fsetContentBundle_005f0.setPageContext(_jspx_page_context);
    _jspx_th_cq_005fsetContentBundle_005f0.setParent(null);
    int _jspx_eval_cq_005fsetContentBundle_005f0 = _jspx_th_cq_005fsetContentBundle_005f0.doStartTag();
    if (_jspx_th_cq_005fsetContentBundle_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fcq_005fsetContentBundle_005fnobody.reuse(_jspx_th_cq_005fsetContentBundle_005f0);
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fif_005f0.setParent(null);
    // /apps/citrixosd/components/content/resource-new/resourceConfig/resourceConfig.jsp(350,2) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fif_005f0.setTest(((java.lang.Boolean) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${not empty title}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null, false)).booleanValue());
    int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
    if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("\t\t\t<div class=\"resource-categories-header\">\n");
        out.write("\t\t\t\t<div class=\"resource-categories-title\">\n");
        out.write("   \t\t\t\t\t");
        out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${title}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("\n");
        out.write("\t\t\t\t</div>\n");
        out.write("\t\t\t\t<div class=\"resource-categories-subtitle\">\n");
        out.write("\t\t\t\t\t");
        out.write((java.lang.String) org.apache.sling.scripting.jsp.jasper.runtime.PageContextImpl.proprietaryEvaluate("${subtitle}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
        out.write("\n");
        out.write("\t\t\t\t</div>\n");
        out.write("\t\t\t\t<div class=\"clearBoth\"></div>\n");
        out.write("\t\t\t\t\n");
        out.write("\t\t\t\t<div class=\"resource-category-title-arrow-container\">\n");
        out.write("\t\t\t\t\t<span class=\"resource-category-title-arrow\"></span>\n");
        out.write("\t\t\t\t</div>\n");
        out.write("\t\t\t\t\n");
        out.write("\t\t\t\t<div class=\"resource-category-subtitle-arrow-container\">\n");
        out.write("\t\t\t\t\t<span class=\"resource-category-title-arrow\"></span>\n");
        out.write("\t\t\t\t</div>\n");
        out.write("\t\t\t</div>\n");
        out.write("\t\t");
        int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fc_005fif_005ftest.reuse(_jspx_th_c_005fif_005f0);
    return false;
  }
}
