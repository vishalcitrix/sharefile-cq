<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    Note: Depends on logo and linkSet component.
    
    vishal.gupta@citrix.com
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:choose>
    <c:when test="${not (isEditMode || isReadOnlyMode)}">     
        <div class="topNav topNavTransition">
    </c:when>
    <c:otherwise>
        <div class="topNav" style="position:inherit !important;">
    </c:otherwise>
</c:choose>

<header>
	<div class="logo">
		<a href="${properties.path}" class="show-for-small-only" <c:if test="${not empty properties.linkOption}">rel="${properties.linkOption}"</c:if>><span></span></a>
	   	<a href="${properties.path}" class="show-for-medium-up" <c:if test="${not empty properties.linkOption}">rel="${properties.linkOption}"</c:if>><span></span></a>
	</div>
    <div class="link-group">
    	<swx:setWCMMode mode="READ_ONLY">
    		<cq:include script="mainLinkSet.jsp"/>
    	</swx:setWCMMode>
    </div>
    <div class="clearBoth"></div>
</header>