<%--
  Try Form component
  - Form with action, field text options.
--%>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:set var="formMethod" value="<%= properties.get("formMethod","POST") %>" />
<c:set var="errorMessages" value="<%= properties.get("errorMessages","Please correct the fields below highlighted in red.") %>" />
<c:set var="formClass" value="<%= properties.get("formClass","trial-form") %>" />

<div class="custom-form ${formClass} section">
    <form  class="trial-form" action="${properties.formAction}" method="${formMethod}">
         <div class="trial-form-container">
	         <div class="row">
	                <div class="trial-form-content">
	                    <div class="error-message"><span class="icon-Warning"></span>${errorMessages}</div>
	                    <div class="columns large-2 medium-4" >
	                        <input type="text" id="FirstName" name="FirstName" placeholder="${properties.firstNamePlaceHolder}" class="prefill required">
	                    </div>
	                    <div class="columns large-2 medium-4" >
	                        <input type="text" id="LastName" name="LastName" placeholder="${properties.lastNamePlaceHolder}" class="prefill required">
	                    </div>
	                    <div class="columns large-2 medium-4" >
	                        <input type="text" id="Email" name="Email" placeholder="${properties.emailPlaceHolder}" class="prefill required" constraint="email">
	                    </div>
	                    <div class="columns large-2 medium-4 medium-view">
	                        <div class="password-message">Password must contain at least 8 characters. At least 1 number, 1 upper case, and 1 lower case letter.<div class="notch"></div></div>
	                        <input type="password" id="RegPassword" placeholder="${properties.passwordPlaceHolder}" trackerror="true"  class="prefill required pwd" constraint="password" >
	                    </div>
	                      <c:if test="${not empty properties.phonePlaceHolder}">  
	                        <div class="columns large-12 medium-12 small-12" >
	                            <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="${properties.phonePlaceHolder}" class="prefill" constraint="phoneNumber">
	                        </div>    
	                    </c:if>
	                    <div class="columns large-2 medium-4 trial-form-button-container " >
	                        <input type="submit" class="" id="RegFormSubmit" value="${properties.buttonLabel}">
	                    </div>
	                     <!--  adding hidden fields -->
	                    <input type="hidden" name="UserID" />
	                    <input type="hidden" name="src" />
	                    <input type="hidden" name="cat" />
	                    <!--  add extra text in form -->
	            	</div>
	         </div>
	         <div class="row">
				<div class="columns large-12 medium-12">
				    <cq:include path="extraTextfirst" resourceType="/apps/shareConnect/components/content/text" />
				</div>
			</div>
    	</div>
    </form>
</div>
<div class="clearBoth"></div>