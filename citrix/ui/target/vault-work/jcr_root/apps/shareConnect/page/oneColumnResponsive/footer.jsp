<%@include file="/apps/citrixosd/global.jsp"%>

<cq:include script="geoSegmentation.jsp"/>
<cq:include script="lightbox.jsp"/>

<div id="footerSection">
	<cq:include path="floatingFooter" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="footerSiteMap" resourceType="swx/component-library/components/content/single-ipar"/>
</div>

<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
