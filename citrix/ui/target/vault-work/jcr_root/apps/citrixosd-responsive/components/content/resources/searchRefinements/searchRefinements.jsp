<%--
    Filters - vishal.gupta@citrix.com
--%>
<%@ page import="com.citrixosdRedesign.constants.ResourceConstants,
				com.day.cq.tagging.TagManager,
				com.citrixosdRedesign.utils.ResourceUtils,
				com.citrixosd.utils.Utilities,
				com.day.cq.personalization.ClientContextUtil,
				com.citrixosd.utils.ContextRootTransformUtil" %>
				
<%@include file="/apps/citrixosd/global.jsp"%>
<cq:setContentBundle/>

<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

    String[] products = properties.get(ResourceConstants.PRODUCTS, String[].class);
    String[] categories = properties.get(ResourceConstants.CATEGORIES, String[].class);
    String[] topics = properties.get(ResourceConstants.TOPICS, String[].class);
    String[] industries = properties.get(ResourceConstants.INDUSTRIES, String[].class);
    String[] defaultProduct = new String[0];

    String resourceListPath = null;
    String resourceListId = null;
    if(currentNode != null) {
        final Node pageJcrNode = currentNode.getParent();
        if(pageJcrNode.hasNode("resourceList")) {
            final Node resourceNode = pageJcrNode.getNode("resourceList");
            resourceListPath = resourceNode.getPath();
            resourceListId = ClientContextUtil.getId(resourceListPath);

            final ValueMap resourceProperties = resourceResolver.getResource(resourceListPath).adaptTo(ValueMap.class);

            if(products == null || products.length == 0) {
                products = resourceProperties.get(ResourceConstants.PRODUCTS, String[].class);
            }
            if(categories == null || categories.length == 0) {
                categories = resourceProperties.get(ResourceConstants.CATEGORIES, String[].class);
            }
            if(topics == null || topics.length == 0) {
                topics = resourceProperties.get(ResourceConstants.TOPICS, String[].class);
            }
            if(industries == null || industries.length == 0) {
                industries = resourceProperties.get(ResourceConstants.INDUSTRIES, String[].class);
            }

            //get default product from resourceList node
            defaultProduct = resourceProperties.get(ResourceConstants.DEFAULT_PRODUCT, String[].class);
        }
    }
    resourceListPath = ContextRootTransformUtil.transformedPath(resourceListPath,request);
%>

<c:set var="resourceListPath" value="<%= resourceListPath %>"/>
<c:set var="resourceListId" value="<%= resourceListId %>"/>
<c:set var="products" value="<%= ResourceUtils.getTags(products, tagManager)%>"/>
<c:set var="categories" value="<%= ResourceUtils.getTags(categories, tagManager)%>"/>
<c:set var="topics" value="<%= ResourceUtils.getTags(topics, tagManager)%>"/>
<c:set var="industries" value="<%= ResourceUtils.getTags(industries, tagManager)%>"/>
<c:set var="defaultProduct" value="<%= Utilities.getFirstTag(defaultProduct, tagManager)%>"/>

<style>
    .drop-down-li li.active {
        display: none;
    }
</style>

<div class="search-refinements-container<c:if test='not empty ${properties.colorScheme}'> ${properties.colorScheme}</c:if>" data-resource-list-path="${resourceListPath}" data-resource-list-id="${resourceListId}" data-products="${defaultProduct.tagID}" data-categories="" data-topics="" data-industries="">
    <%-- Products --%>
    <div class="searchRefinements-content columns">
        <div>
            <c:if test="${not empty products && fn:length(products) > 1}">
                <ul class="drop-down-container">
                    <li class="filter" data-filter="products">
                        <c:choose>
                            <c:when test="${not empty defaultProduct.tagID}">
                               <a class="drop-down-header" href="javascript:void(0)"><span class="drop-down-header-text">${defaultProduct.title}</span><span class="icon-DownArrow2"></span></a>
                            </c:when>
                            <c:otherwise>
                                <a class="drop-down-header" href="javascript:void(0)"><span class="drop-down-header-text"><fmt:message key="select.products"/></span><span class="icon-DownArrow2"></span></a>
                            </c:otherwise>
                        </c:choose>

                        <ul class="drop-down-li">
                            <c:choose>
                                <c:when test="${not empty defaultProduct.tagID}">
                                   <li><a href="all"><fmt:message key="select.products"/></a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="active"><a href="all"><fmt:message key="select.products"/></a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach items="${products}" var= "product">
                                <c:choose>
                                    <c:when test="${defaultProduct.tagID eq product.tagID}">
                                       <li class="active"><a href="${product.tagID}">${product.title}</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="${product.tagID}">${product.title}</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </ul>
                    </li>
                </ul>
            </c:if>

            <%-- Categories --%>
            <c:if test="${not empty categories && fn:length(categories) > 1}">
                <ul class="drop-down-container">
                    <li class="filter" data-filter="categories">
                        <a class="drop-down-header" href="javascript:void(0)"><span class="drop-down-header-text"><fmt:message key="select.categories"/></span><span class="icon-DownArrow2"></span></a>
                        <ul class="drop-down-li">
                            <li class="active"><a href=""><fmt:message key="select.categories"/></a></li>
                            <c:forEach items="${categories}" var= "category">
                                <li><a href="${category.tagID}">${category.title}</a></li>
                            </c:forEach>
                        </ul>
                    </li>
                </ul>
            </c:if>

            <%-- Topics --%>
            <c:if test="${not empty topics && fn:length(topics) > 1}">
                <ul class="drop-down-container">
                    <li class="filter" data-filter="topics">
                        <a class="drop-down-header" href="javascript:void(0)"><span class="drop-down-header-text"><fmt:message key="select.topics"/></span><span class="icon-DownArrow2"></span></a>
                        <ul class="drop-down-li">
                            <li class="active"><a href=""><fmt:message key="select.topics"/></a></li>
                            <c:forEach items="${topics}" var= "topic">
                                <li><a href="${topic.tagID}">${topic.title}</a></li>
                            </c:forEach>
                        </ul>
                    </li>
                </ul>
            </c:if>

            <%-- Industries --%>
            <c:if test="${not empty industries && fn:length(industries) > 1}">
                <ul class="drop-down-container">
                    <li class="filter" data-filter="industries">
                        <a class="drop-down-header" href="javascript:void(0)"><span class="drop-down-header-text"><fmt:message key="select.industries"/></span><span class="icon-DownArrow2"></span></a>
                        <ul class="drop-down-li">
                            <li class="active"><a href=""><fmt:message key="select.industries"/></a></li>
                            <c:forEach items="${industries}" var= "industry">
                                <li><a href="${industry.tagID}">${industry.title}</a></li>
                            </c:forEach>
                        </ul>
                    </li>
                </ul>
            </c:if>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script>
    $(".search-refinements-container").each(function() {
        var searchRefinementsContainer = $(this);
        var filters = searchRefinementsContainer.find(".filter");
        filters.each(function(){
            var filter = $(this);
            var filterProperty = filter.attr("data-filter");
            filter.click(function(){
                var listContainer = $(this);
                var listHeader = listContainer.children(".drop-down-header");
                var listHeaderText = listHeader.children(".drop-down-header-text");
                var listDropDown = listContainer.children(".drop-down-li");
                listDropDown.find("a").each(function() {
                    var anchor = $(this);
                    anchor.unbind("click").click(function() {
                    	searchRefinements.removeFeatured();
                        listDropDown.children("li").removeClass("active");
                        anchor.parent().addClass("active");
                        listHeaderText.html(anchor.html());
                        searchRefinementsContainer.attr("data-" + filterProperty, anchor.attr("href"));
                        resourceSearchResultQuery(searchRefinementsContainer);
                        listDropDown.hide();
                        return false;
                    });	
                });
                searchRefinementsContainer.find('.drop-down-li').not(listDropDown).hide();
                listDropDown.toggle();
            });
        });
    });

    function resourceSearchResultQuery(container) {
        var searchRefinementsContainer = $(container);
        var resourceListPath = searchRefinementsContainer.attr("data-resource-list-path");
        var resourceListId = searchRefinementsContainer.attr("data-resource-list-id");
        var products = searchRefinementsContainer.attr("data-products");
        var categories = searchRefinementsContainer.attr("data-categories");
        var topics = searchRefinementsContainer.attr("data-topics");
        var industries = searchRefinementsContainer.attr("data-industries");

        var path = resourceListPath;
        path += ".pagination.0.startQuery";
        path += ".products";
        path += "." + products;
        path += ".categories";
        path += "." + categories;
        path += ".topics";
        path += "." + topics;
        path += ".industries"
        path += "." + industries;
        path += ".endQuery"
        path += ".html";
        
        $("#" + resourceListId).load(path, function() {
            dotDotDot.init();
        });
    }
</script>