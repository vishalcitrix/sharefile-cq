<%@include file="/apps/citrixosd/global.jsp"%>

<%@page trimDirectiveWhitespaces="true"%>

<%!
    public static final String EYEBROW_PROPERTY = "eyebrow";
    public static final String HEADING_PROPERTY = "heading";
    public static final String SUB_HEADING_PROPERTY = "subHeading";
%>
<c:set var="eyebrow" value="<%= properties.get(EYEBROW_PROPERTY, "")%>"/>
<c:set var="heading" value="<%=properties.get(HEADING_PROPERTY, "")%>"/>
<c:set var="subHeading" value="<%=properties.get(SUB_HEADING_PROPERTY, "")%>"/>

<div class="text">
	<c:if test="${not empty properties.eyebrow}"><h3 class="size14px-heavy-primaryColor">${properties.eyebrow}</h3></c:if>
	<c:if test="${not empty properties.heading}"><h1 class="size38px-normal-grey3-lineheight46px">${properties.heading}</h1></c:if>
	<c:if test="${not empty properties.subHeading}"><h2 class="size22px-normal-grey3-lineheight30px">${properties.subHeading}</h2></c:if>
<div>