<%--
    SecondaryNavSticky Container.
    
    Javascript will change position css attribute to top when the user
    scrolls below that component. 

    Requires secondaryNavSticky.scss from global-sass
    Requires secondaryNavSticky.js from collaboration

    Note: Javascript is disabled in edit mode because it could be in the way for the author.
--%>

<%@ page import="com.day.cq.wcm.api.WCMMode,
                com.day.cq.personalization.ClientContextUtil" %>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String SECONDARYNAV_STICKY_NODE = "columns";
 %>
 <%
    final ArrayList<Map<String, Property>> stickyColumns = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode(SECONDARYNAV_STICKY_NODE)) {
        stickyColumns.addAll(Utilities.parseStructuredMultifield(currentNode.getNode(SECONDARYNAV_STICKY_NODE)));
    }
%>
<c:set var="stickyColumns" value="<%= stickyColumns%>"/>
<c:set var="columnsConfig" value="<%= properties.get("columnsConfig")%>"/>
<c:set var="mobileText" value="<%= properties.get("mobileText")%>"/>

<div id="action-header" class="action-nav ${(isPreviewMode || isDisabledMode || isPublishInstance)? 'active' : 'inactive'}">
    <div class="container">
        <div class="row">
            <div class="mobile-heading show-for-small-only icon-right-open">${mobileText}</div>
            <c:forEach var="column" items="${stickyColumns}" varStatus="i">
                <c:set var="className" value="" />
                <c:if test="${i.index == 0}" >
                    <c:set var="className" value="selected" />
                </c:if>
                <div class="${columnsConfig}">
                    <div class="${className}">
                        <a class="stickyLink" href="${column.linkPath.string}" index="${i.index}">
                            <div id="column-${i.index}" class="${column.icon.string}">
                                <p class="icon-description">${column.descText.string}</p>
                            </div>
                        </a>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<div class="stickyPar">
    <c:forEach var="column" items="${stickyColumns}" varStatus="i">
        <cq:include path= "sticky-${i.index}" resourceType="foundation/components/parsys"/>
    </c:forEach>
</div>