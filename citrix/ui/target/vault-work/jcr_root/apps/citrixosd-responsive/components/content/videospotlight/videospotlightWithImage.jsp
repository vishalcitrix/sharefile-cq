<%--
	When Background Color option is not selected
--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
	boolean hasImageRef = currentNode != null ? currentNode.hasNode("imageRenditions") ? currentNode.getNode("imageRenditions").hasProperty("fileReference") : false : false;
%>

<div style="float:auto">
    <a href="${properties.videocontainerId}" rel="lightbox">
        <swx:setWCMMode mode="READ_ONLY">
            <div class="videos-spotlight-container ${(isEditMode || isReadOnlyMode) ? 'shrinkImage' : ''}" data-enablejs="${not (isEditMode || isReadOnlyMode)}">
                <cq:include path="imageRenditions" resourceType="/apps/citrixosd-responsive/components/content/imageRenditions"/>
                <cq:include script="gradientholder.jsp" />
            </div>   
        </swx:setWCMMode>
    </a>
</div>            
<div class="clearBoth"></div>  
 