<%--
    Options Provider
        - Processes a json file of styles and generates a new json array of options for the component dialog

    Dependencies:
        - styles.json from icomoon (https://icomoon.io/#docs)
    
    rick.potsakis@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.*" %>
<%@page import="org.apache.sling.commons.json.JSONObject" %>
<%@page import="org.apache.sling.commons.json.JSONArray" %>
<%@page import="org.apache.sling.commons.json.JSONException" %>

<%@page trimDirectiveWhitespaces="true" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%!
    public static final String PATH_PROPERTY = "path";
    public static final String LINK_OPTION_PROPERTY = "linkOption";
%>
<%
    response.setContentType("application/json");
    response.setHeader("Content-Disposition", "inline");

    String jsonstr = "";
   
    // Get the styles json as string
    String designPath= currentDesign.getPath()+"/css/static/iconFonts";
    Resource res = resourceResolver.getResource(designPath);
    Node node = res.adaptTo(Node.class);
    Node datanode = node.getNode("styles.json");
    jsonstr = datanode.getNode("jcr:content").getProperty("jcr:data").getString();

    // Start new array for condensed json
    ArrayList<JSONObject> list = new ArrayList<JSONObject>();
    
    try {
        JSONObject jsonObject = new JSONObject(jsonstr);
        JSONArray icons = (JSONArray) jsonObject.get("icons");

        // take each value from the json array separately
        for (int i=0; i<icons.length(); i++) {
            JSONObject innerObj = (JSONObject) icons.get(i);
            JSONObject props = (JSONObject) innerObj.get("properties");

            JSONObject newObj = new JSONObject();
            newObj.put("jcr:primaryType", "nt:unstructured");
            newObj.put("text", props.get("name").toString().toLowerCase());
            newObj.put("value", props.get("name").toString().toLowerCase());
            list.add(newObj);
        }

    } catch (Exception e) {
       e.printStackTrace();
    }

    // Sort json array by the option name
    Collections.sort(list, new Comparator<JSONObject>(){
        public int compare(JSONObject o1, JSONObject o2) {
            String s1 = "";
            String s2 = "";
            
            try {
                s1 = o1.get("text").toString().toLowerCase();
                s2 = o2.get("text").toString().toLowerCase();
            } 
            catch (JSONException e) {
                e.printStackTrace();
            }

            return s1.compareTo(s2);
        }
    });

    // output new json array
%>
<%= list.toString() %>