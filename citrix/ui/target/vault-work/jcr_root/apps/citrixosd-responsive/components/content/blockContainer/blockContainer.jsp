<%--
    Block Container

    This component is to enclose content with a fixed with and height and border at top. It also has option to add card behavior with flip

    SCSS required: /citrixosd-sass/global-sass/components/global/_blockContainer.scss
    Clientlib required: jquery.flip
    JS required: /g2m-redesign/js/v2/common/js/blockContainerFlip.js - Add to your design and make changes if needed
                Add blockContainerFlip.js to js.txt
                Add blockContainerFlip.init() to Init.js in site design
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="flipSelector" value=""/>
<c:if test="${not empty properties.flipContent && properties.flipContent eq 'true'}">
	<c:set var="flipSelector" value="block-flip"/>
</c:if>
<div class="${flipSelector} ${' '} ${properties.containerType}">
	<c:if test="${not empty properties.linkPath}">
		<a rel="${properties.linkBehavior}" href="${properties.linkPath}">
	</c:if>

	<c:if test="${not empty flipSelector || empty flipSelector}"><div class="front ${properties.borderTop}"></c:if>
		<cq:include path="blockContainer_parsys" resourceType="foundation/components/parsys"/></div>
	<c:if test="${not empty flipSelector}">
		<cq:include path="back" resourceType="foundation/components/parsys"/>
	</c:if>
	<c:if test="${not empty properties.linkPath}">
		</a>
	</c:if>
</div>
<% if(WCMMode.fromRequest(slingRequest).toString().equals("EDIT")) { %>
	<cq:include path="endofsection" resourceType="citrixosd/components/content/endComponent"/>
<% } %>