<%--
  Carousel component.
  This is a carousel component developed based on owl carousel, with various options.
  maruti.prasad@citrix.com
  vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode,
                com.day.cq.personalization.ClientContextUtil"%>

<%
   String currentMode = WCMMode.fromRequest(slingRequest).toString();
   final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
   pageContext.setAttribute("nodeId",containerId);
   pageContext.setAttribute("pageMode", currentMode);
%>

<c:choose>
    <c:when test="${pageMode eq 'EDIT'}">
        <ol style="height:auto; overflow: scroll;">
            <c:forEach begin="1" end="${properties.itemscount}"
                varStatus="status">
                <li>
                	<cq:include path="item-${status.count}" resourceType="foundation/components/parsys" />
                </li>
            </c:forEach>
       </ol>
    </c:when>
    <c:otherwise>
        <div id="${nodeId}" class="owl-carousel carousel-primary" data-itemCount="${properties.itemscount}" data-itemslarge="${properties.desktopitemscount}" data-disablenavinmediumviewport="${properties.disableNavInMediumViewport}" data-itemsmedium="${properties.tabletitemscount}"  data-showsingleitemonsmallviewport="${properties.smallViewPortSingleItemDisplay}" data-itemssmall="${properties.mobileitemscount}" data-navigation="${properties.navigation}" data-disablenavinsmallviewport="${properties.disableNavInSmallViewport}"  data-autoplay="${properties.autoplay}" data-rewindnav="${properties.rewind}"  data-theme="${properties.theme}" data-ospace="${properties.nospaceoption}" data-disableoverlap="${properties.disablePaginationOverlap}" data-disablenavoverlap="${properties.disableNavigationOverlap}" data-paginationDotsColor="${properties.paginationIconsColor}" >
            <c:forEach begin="1" end="${properties.itemscount}" varStatus="status">
                <div class="item">
                    <cq:include path="item-${status.count}"
                        resourceType="foundation/components/parsys" />
                </div>
            </c:forEach>
        </div>
    </c:otherwise>
</c:choose>

<%--logic to display action arrow --%>
<c:if test="${properties.isActionArrowReq eq 'true'}">
	<div class="notch">
		<c:choose>
			<c:when test="${not empty properties.imgPath}">
				<c:choose>
					<c:when test="${not empty properties.imgLink}">
						<a href="${properties.imgLink}"><img src="${properties.imgPath}" /></a>
					</c:when>
					<c:when test="${empty properties.imgLink && not empty properties.videocontainerId}">
						<a class="icon-play" href="${properties.videocontainerId}" rel="lightbox">       
				        	<img src="${properties.imgPath}" />
				    	</a>
					</c:when>
					<c:otherwise>
						<img src="${properties.imgPath}" />
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<div class="banner-notch">
				    <a class="icon-play" href="${properties.videocontainerId}" rel="lightbox">       
				          <p> ${properties.actionArrowLabel}</p>
				    </a>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</c:if>
