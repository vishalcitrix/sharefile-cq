<%--
    Resource List- vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils,
				com.day.cq.wcm.api.WCMMode,
				org.apache.jackrabbit.commons.JcrUtils,
				com.day.cq.commons.jcr.JcrConstants" %>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
    //Create itself if it is not already created because this is required for filtering
    if(currentNode == null && WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
        final Session session = resourceResolver.adaptTo(Session.class);
        final Node newNode = JcrUtils.getOrCreateByPath(resource.getPath(), JcrConstants.NT_UNSTRUCTURED, session);
        newNode.setProperty("sling:resourceType", component.getResourceType());
        session.save();
        response.sendRedirect(currentPage.getPath());
    }
%>

<c:set var="resourceList" value="<%= ResourceUtils.getResourceList(resource, currentPage, request, null, currentDesign) %>" scope="request"/>
<c:set var="colorScheme" value="<%= ResourceUtils.ResourceCenterProductTheme(resource,currentPage) %>"/>

<div id="${resourceList.id}"<c:if test="${colorScheme ne null}"> class="${colorScheme}"</c:if>>
    <cq:include script="content.jsp"/>
</div>