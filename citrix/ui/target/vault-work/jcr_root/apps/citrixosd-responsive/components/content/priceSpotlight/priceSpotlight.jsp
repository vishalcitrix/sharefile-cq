<%--
  Price Spotlight component.

--%>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%@ page trimDirectiveWhitespaces="true"%>
<%
	//using Utilities to parse structured multi field
	ArrayList<Map<String,Property>> values = new ArrayList<Map<String,Property>>();
	if(currentNode != null && currentNode.hasNode("links")) {
		final Node baseNode = currentNode.getNode("links");
		values = Utilities.parseStructuredMultifield(baseNode);
	}
%>
<c:set var="values" value="<%= values %>"/>
<div class="spotlightOuter ${properties.theme} ${properties.spotlight eq 'true' ? 'spotlight' : ''}">
	<c:if test="${not empty properties.mostPopular && properties.spotlight eq 'true'}">
		<div class="mostPopular">
			${properties.mostPopular}
			<div class="arrow"></div>
		</div>
	</c:if>
	<h3 class="theme ${properties.spotlight eq 'true'? 'spotLight' :'noSpotlight'}">${properties.title}</h3>
	<div class="spotlightInner ${properties.spotlight eq 'true'? 'spotlight' :''}">
		<p class="attendees">${properties.priceHeaderFirst}<br>${properties.priceHeaderSecond}</p>
		<div class="pricestyle clearfix">
			<span class="price ${empty properties.priceCurrency ? 'free' :''}"> <sup>${properties.priceCurrency}</sup>${properties.price}</span>
			<c:if test="${not empty properties.pricePostfix}">
				<span class="division">/</span>
				<span class="message">${properties.pricePostfix}</span>
			</c:if>
		</div>
		<p class="annualprice text">${properties.priceFooter}</p>
		<c:if test="${not empty properties.buttonLabel}">
			<a class="buynow" href="${properties.buttonURL}">${properties.buttonLabel}</a>
		</c:if>
		<div class="linkContainer text">
			<c:choose>
				<c:when test="${not empty properties.linkURL}">
					<a class="trialfree" href="${properties.linkURL}">
						${properties.link}
						<c:if test="${not empty properties.linkicon}">
							<span class="${properties.linkicon}"></span>
						</c:if>
					</a>
				</c:when>
				<c:otherwise>
					<div class="offer">${properties.link}</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<c:if test="${not empty values}">
		<div class="features">
			<c:forEach var="item" items="${values}">
				<p class="featuresList">${item.text.string}</p>
			</c:forEach>
		</div>
	</c:if>
</div>
<div class="clearBoth"></div>