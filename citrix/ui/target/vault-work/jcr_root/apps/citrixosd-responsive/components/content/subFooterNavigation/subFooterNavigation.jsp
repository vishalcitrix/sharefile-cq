<%--
	Sub Footer Navigation

	Creates sub footer navigation that contains locale selector (opional), list of links, copyright text and company logo
	SCSS required: /citrixosd-sass/global-sass/components/unity/_subFooterNavigation.scss
	JS required: Locale selector JS needed
--%>
<%@ page trimDirectiveWhitespaces="true" %>

<%@page import="com.citrixosd.utils.Utilities" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@ include file="/apps/citrixosd/global.jsp" %>

<jsp:useBean id="date" class="java.util.Date" />
<%
	//using Utilities to parse structured multi field
	Node baseNode = null;
	ArrayList<Map<String,Property>> list = new ArrayList<Map<String,Property>>();
	if(currentNode != null && currentNode.hasNode("linkSet")) {
		if(currentNode.getNode("linkSet").hasNode("links")) {
			baseNode = currentNode.getNode("linkSet").getNode("links");
			list = Utilities.parseStructuredMultifield(baseNode);
		}
	}
%>
<c:set var="linkList" value="<%= list %>"/>
<div class="row">
<%-- Locale Selector --%>
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="countrySelector" resourceType="/apps/citrixosd-responsive/components/content/countrySelector"/>
	</swx:setWCMMode>
<%-- List of links --%>
	<c:if test="${not empty linkList}">
		<ul class="linkList">
			<c:forEach var="item" items="${linkList}">
				<c:set var="fnItemPath">${item.path.string}</c:set>
				<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
			</c:forEach>
		</ul>
	</c:if>
<%-- Copyright/Disclaimer text --%>
	<c:if test="<%= currentNode.hasNode("linkSet") %>">
		<c:set var="disableCopy" value="false" />
		<c:if test="<%= currentNode.getNode("linkSet").hasProperty("disableCopyright") %>">
			<c:set var="disableCopy" value="<%= currentNode.getNode("linkSet").getProperty("disableCopyright").getString() %>" />
		</c:if>
		<c:if test="<%= currentNode.getNode("linkSet").hasProperty("startYear") %>">
			<c:set var="startYear" value="<%= currentNode.getNode("linkSet").getProperty("startYear").getString() %>" />
		</c:if>
		<c:set var="copyText" value="<%= currentNode.getNode("linkSet").getProperty("copyright").getString() %>" />
		<c:set var="disclaimerText" value="<%= currentNode.getNode("linkSet").getProperty("disclaimer").getString() %>" />

		<c:if test="${not disableCopy}">
			<div class="copyright">
				&copy;<c:if test="${not empty startYear}"><fmt:message key="${startYear}"/></c:if><fmt:formatDate value="${date}" pattern="yyyy" />&nbsp;<fmt:message key="${copyText}"/>&nbsp;<fmt:message key="${disclaimerText}"/>
			</div>
		</c:if>
	</c:if>
<%-- Company logo --%>
	<c:if test="<%= currentNode.hasNode("logo") %>">
		<c:if test="<%= currentNode.getNode("logo").hasProperty("path") %>">
			<c:set var="logoPath" value="<%= ContextRootTransformUtil.transformedPath(currentNode.getNode("logo").getProperty("path").getString(),request) %>" />
		</c:if>
		<c:if test="<%= currentNode.getNode("logo").hasProperty("linkOption") %>">
			<c:set var="logoOption" value="<%= currentNode.getNode("logo").getProperty("linkOption").getString() %>" />
	    </c:if>
	    <div class="logoContainer">
			<c:choose>
				<c:when test="${not empty logoPath}">
					<a href="${logoPath}" rel="${logoOption}" class="logo"></a>
				</c:when>
				<c:otherwise>
					<span class="logo"></span>
				</c:otherwise>
			</c:choose>
		</div>
	</c:if>
	<div class="clearBoth"></div>
</div>