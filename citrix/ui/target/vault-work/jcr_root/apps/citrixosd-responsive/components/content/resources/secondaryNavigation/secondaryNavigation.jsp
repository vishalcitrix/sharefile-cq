<%--
    Secondary Navigation
    vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosd.utils.Utilities,
				java.util.Map,java.util.ArrayList,
				javax.jcr.Property,
				com.citrixosd.utils.ContextRootTransformUtil" %>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final ArrayList<Map<String, Property>> navigationLinks = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode("navigationLinks")) {
        navigationLinks.addAll(Utilities.parseStructuredMultifield(currentNode.getNode("navigationLinks")));
        for (int i = 0; i < navigationLinks.size(); i++) { 
            Map<String, Property> hm = navigationLinks.get(i);
            Property path = (Property) hm.get("path");
            String transformedPath = ContextRootTransformUtil.transformedPath(path.getString(),request);
            path.setValue(transformedPath);
            hm.put("path",path);
        }
    }
%>

<c:set var="navigationLinks" value="<%= navigationLinks %>"/>
<c:set var="currentPage" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getPath(),request) %>"/>
<c:set var="parentPath" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getParent().getPath(),request) %>"/>
<c:set var="parentPathDepth" value="<%= currentPage.getParent().getDepth() %>"/>

<div class="rowC columns ${properties.theme}">
	<div class="first">
		<c:if test="${properties.showTwoLevels}">
			<c:if test="${isEditMode || isDesignMode}">
				<div class="temp">
			</c:if> 
			<span class="title"><c:if test="${not empty properties.title}">${properties.title}</c:if></span>
			<cq:include path="secondaryNav_parsys" resourceType="foundation/components/parsys"/>
			<div class="clearBoth"></div>
			<c:if test="${isEditMode || isDesignMode}">
				</div>
			</c:if>
		</c:if>
	</div>
    <nav class="columns large-9 medium-9 small-9">
        <ul class="show-for-medium-up">
        	<c:if test="${not properties.showTwoLevels}">
            	<li class="title"><c:if test="${not empty properties.title}">${properties.title}</c:if></li>
            </c:if>
            <c:forEach items="${navigationLinks}" var="navigationLink">
                <c:choose>
                    <c:when test="${currentPage eq navigationLink.path.string}">
                        <li class="selected"><a href="#">${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:when>
                    <c:when test="${parentPath eq navigationLink.path.string && parentPathDepth > 4}">
                        <li class="selected"><a href="${navigationLink.path.string}" ${navigationLink.externalLink.string eq true ? "rel='external'" : ""}>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${navigationLink.path.string}" ${navigationLink.externalLink.string eq true ? "rel='external'" : ""}>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
        <div class="show-for-small-only">
        	<c:if test="${not properties.showTwoLevels}">
            	<div class="title"><c:if test="${not empty properties.title}">${properties.title}</c:if></div>
            </c:if>
            <select class="cLink">
                <c:forEach items="${navigationLinks}" var="navigationLink">
                    <c:choose>
                        <c:when test="${currentPage eq navigationLink.path.string || (parentPath eq navigationLink.path.string && parentPathDepth > 4)}">
                            <option value="${navigationLink.path.string}" selected>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${navigationLink.path.string}">${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>
    </nav>
    <div class="columns small-1">
        <span class="icon-Filter resource-filter filter show-for-small-only"></span>
    </div>
    <c:if test="${not empty properties.showSearch}">
        <div class="columns large-3 medium-3 small-2">
            <cq:include path="search" resourceType="citrixosd/components/content/search"/>
        </div>
    </c:if>
</div>