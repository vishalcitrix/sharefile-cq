<%--
  Sales Chat
  
  This component will provide to content area that will automatically toggle content based on Sales Chat availability.
  Dependencies:
  	salesChat.js
  	global-sass/salesChat.scss
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="targetid" value="<%= properties.get("targetid", "lpButton-TopNav") %>"/>

<div class="salesChat-inner" data-targetid="${targetid}">
	<div id="lpSection-default">
		<cq:include path="lpSectionDefault" resourceType="foundation/components/parsys" />
	</div>
	<div id="lpSection-online">
		<cq:include path="lpSectionOnline" resourceType="foundation/components/parsys" />
	</div>
</div>