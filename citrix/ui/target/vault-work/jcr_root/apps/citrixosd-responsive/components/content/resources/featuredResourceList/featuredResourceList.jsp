<%--
  Featured Resource List - vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%  
	Node baseNode = null;
    if(currentNode != null && currentNode.hasNode("featuredResource")) {
        baseNode = currentNode.getNode("featuredResource");
    }
%>

<c:set var="featuredResourceList" value="<%= ResourceUtils.getFeaturedResourcesFromLinks(resource,baseNode,currentDesign,request) %>" scope="request"/>
<c:set var="colorScheme" value="<%= ResourceUtils.ResourceCenterProductTheme(resource,currentPage) %>"/>

<div<c:if test="${colorScheme ne null}"> class="${colorScheme}"</c:if>>
    <cq:include script="content.jsp"/>
</div>