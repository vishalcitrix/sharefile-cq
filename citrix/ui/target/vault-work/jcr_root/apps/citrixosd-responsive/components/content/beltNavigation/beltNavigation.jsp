<%--
	Belt Navigation

	Component for hero section nav
	Note: Depends on linkSet component.
	
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page trimDirectiveWhitespaces="true" %>

<%!
	public static final String SIDE_NAV_LABEL = "sideNavLabel";
	public static final String DISPLAY_HOMEPAGE_LINKS = "displayHomepageLinks";
%>
<%
    Node linksNode1 = null;
    Boolean hasLinks1 = false;
    
    if( currentNode != null ){
    	if( currentNode.hasNode("linkSet1") ){
    		linksNode1 = currentNode.getNode("linkSet1");
    	}
    	if( linksNode1 != null ){
            hasLinks1 = linksNode1.hasNodes();
        }
    }
%>



<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>
<c:set var="linkSet1Title" value="<%= properties.get("linkSet1Title", "") %>"/>
<c:set var="hasLinks1" value="<%= hasLinks1 %>"/>

<c:choose>
	<c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed)}">     
		<div class="belt-nav">
	</c:when>
	<c:otherwise>
		<div class="belt-nav" style="position:inherit !important;">
	</c:otherwise>
</c:choose>

	<div class="link-group belt-linkmain">
		<swx:setWCMMode mode="READ_ONLY">
		<ul>
			<c:if test="${not empty linkSet1Title}">
				<li class="show-for-small-only">
					<a href="javascript:void 0;" class="linkset-anchor" data-linkset="1"><span class="icon-down-arrow-circle"></span>${linkSet1Title}</a>
				</li>
			</c:if>
		</ul>
		</swx:setWCMMode>
	</div>

	<div class="link-group belt-subnav">
		<swx:setWCMMode mode="READ_ONLY">
			<c:if test="${hasLinks1}">
				<cq:include script="linkset1.jsp"/>
			</c:if>
		</swx:setWCMMode>
		<div class="clearBoth"></div>
	</div>

</div>







