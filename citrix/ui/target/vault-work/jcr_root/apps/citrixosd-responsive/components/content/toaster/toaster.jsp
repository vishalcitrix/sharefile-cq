<%--
    Toaster component.
    This component has 2 broad sections - one on the left and one on the right.
    Left section includes a parsys component where user can add any required content.
    Right section contains buttons that float from right to left. User can add as many buttons as he/she wants.
    Buttons are followed up with support links (from right to left).
    All the eleents in left and right sections are configurable to be visible / hidden in desktop and tablet
    viewports.

    In mobile view, toaster displays a separate message alongwith support links like Phone and Sales Chat

    Toaster position is fixed at the bottom of the page and appears when user scrolls down. Disappears
    when scroll reaches the CTA component at the bottom of the page as well as when user scrolls up.
--%>

<%@ page trimDirectiveWhitespaces="true" %>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
    List<List> buttons = new ArrayList<List>(); 
    List<Map> buttonGroup = new ArrayList<Map>();
    Map<String,String> buttonProperties;

    if (currentNode != null && currentNode.hasNode("buttons")) {
        final Node baseNode = currentNode.getNode("buttons");
        final NodeIterator nodeIter = baseNode.getNodes();
        String visibilityClass;
        int grpCount = 0;
        
        while(nodeIter.hasNext()){
            try{
                final Node currButton = nodeIter.nextNode();
                buttonProperties = new HashMap<String,String>();
                boolean showForLarge = false;
                boolean showForMedium = false;
                boolean showForSmall = false;
                visibilityClass = "show-for-medium-up";

                if((grpCount%2)== 0){
                    buttonGroup = new ArrayList<Map>();
                    buttons.add(buttonGroup);
                }
                    
                if(currButton.hasProperty("showForLarge"))
                    showForLarge = true;
                if(currButton.hasProperty("showForMedium"))
                    showForMedium = true;
                if(showForLarge || showForMedium)
                    visibilityClass = Utilities.getVisibilityClass(showForLarge,showForMedium,showForSmall);
                
                buttonProperties.put("buttonPath",currButton.getPath());
                buttonProperties.put("visibilityClass",visibilityClass);
                buttonGroup.add(buttonProperties);
                grpCount++;
            }catch(RepositoryException re){
                re.printStackTrace();
            }
        }
    }

    boolean leftParsysDesktop = properties.get("leftParsysDesktop", false);
    boolean leftParsysTablet = properties.get("leftParsysTablet", false);
    String leftParsysVisibility = Utilities.getVisibilityClass(leftParsysDesktop,leftParsysTablet,false);
%>

<c:set var="buttonList" value="<%= buttons %>"/>
<c:set var="leftParsysVisibility" value="<%= leftParsysVisibility %>"/>

    <div class="toasterBar ${(isPreviewMode || isDisabledMode || isPublishInstance)? 'active' : 'inactive'}" data-show-scroll-up="${not empty properties.showOnScrollUp ? properties.showOnScrollUp : false}">
            <div class="leftParsys ${leftParsysVisibility}">
                <cq:include path="toasterLeft" resourceType="swx/component-library/components/content/single-par"/>
            </div>
                
            <div class="rightGroup show-for-medium-up">
                <c:if test="${fn:length(buttonList) > 0}">
                    <c:forEach items="${buttonList}" begin="0" var="buttonGrp" varStatus="i">
                        <ul class="${i.first ? 'first' : ''}">
                            <c:forEach items="${buttonGrp}" begin="0" var="button">
                                <li class="${button['visibilityClass']} right">
                                    <cq:include path="${button['buttonPath']}" resourceType="/apps/citrixosd-responsive/components/content/button"/>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </c:if>
            </div>

            <cq:include script="supportOptions.jsp"/>
                
             <c:if test="${not empty properties.phoneMessage}">
                <div class="message show-for-small-only">
                        <a href="${not empty properties.msgPath ? properties.msgPath : ''}" ${empty properties.msgPath ? 'onclick="return false;"' : ''} rel="${properties.mainMsgLinkOption}">
                            <div class="msgText">
                                ${properties.phoneMessage}
                            </div>
                            <div class="arrow"><span class="icon-right-open"></span></div>
                        </a>
                </div>
            </c:if>

            <div class="clearBoth"></div>
    </div>