<%--
  When Background Color option is selected
--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<div style="float:auto">
    <a href="${properties.videocontainerId}" rel="lightbox">
        <swx:setWCMMode mode="READ_ONLY">
            <div class="videos-spotlight-containerWithBackground">
                <div class="hovercontent" style="background: ${properties.bgcolor};">
					<p class="title" >${properties.titleGardient}</p>
					<p class="description">${properties.subtitle}</p>
					<p class="playvideo icon-play"></p>
				</div>
            </div>   
        </swx:setWCMMode>
    </a>
</div> 
<div class="clearBoth"></div>  
 