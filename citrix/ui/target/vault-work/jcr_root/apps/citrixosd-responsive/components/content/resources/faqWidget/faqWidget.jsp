<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String TITLE_PROPERTY = "title";
    public static final String FAQ_DESC_PROPERTY = "faqDescription";
    public static final String BOTTOM_TITLE_PROPERTY = "bottomTitle";
    public static final String BOTTOM_LINK_PROPERTY = "bottomTitleLink";
%>

<c:set var="title" value="<%= properties.get(TITLE_PROPERTY, "") %>"/>
<c:set var="faqDescription" value="<%=properties.get(FAQ_DESC_PROPERTY, "")%>"/>
<c:set var="bottomTitle" value="<%=properties.get(BOTTOM_TITLE_PROPERTY, "")%>"/>
<c:set var="bottomTitleLink" value="<%=properties.get(BOTTOM_LINK_PROPERTY, "")%>"/>
        <div class="faqWidget" >
            <div class="faq-header">
                <div class="faqImage"> <cq:include path="faqImage" resourceType="citrixosd-responsive/components/content/imageRenditions"/></div>
                <div class="faqTitle">${title} </div>
                <div class="faqDesc"> ${faqDescription}</div>
           </div>
            <div class="faq-container">
                <cq:include path="faq" resourceType="foundation/components/parsys"/>
            </div>
            <div class="faq-sub-container">
                <span class="icon-arrow" > <a class="faqFooter" href="${bottomTitleLink}">${bottomTitle}</a> </span>
            </div>
        </div>
