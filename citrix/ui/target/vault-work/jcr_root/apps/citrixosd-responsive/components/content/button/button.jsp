<%--
  Button
  
  This component will consist of custom css buttons which will contain a 
  label and a link. The button also allows an optional sub text displayed 
  under the button. Default button style is set to none.
  
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	public static final String CSS_PROPERTY = "css";
	public static final String LABEL_PROPERTY = "label";
	public static final String PATH_PROPERTY = "path";
	public static final String LINK_OPTION_PROPERTY = "linkOption";
	public static final String SUB_TEXT_PROPERTY = "subText";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, "none") %>"/>
<c:set var="label" value="<%= properties.get(LABEL_PROPERTY, "Button") %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, "") %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>
<c:set var="subText" value="<%= properties.get(SUB_TEXT_PROPERTY, "") %>"/>

<div class="button-container">
<c:choose>
	<c:when test="${not empty linkOption}">
		<a class="button ${css}" href="${path}" rel="${linkOption}">
		<c:if test="${not empty label and css ne 'playIconWhite'}">
			<fmt:message key="${label}" />
		</c:if>
		</a>
		<c:if test="${not empty subText}">
		<div class="subText">
			<a href="${path}" rel="${linkOption}">
			<c:if test="${not empty subText}">
				<fmt:message key="${subText}"/>
			</c:if>
			</a>
		</div>
		</c:if>
	</c:when>
	<c:otherwise>
		<a class="button ${css}" href="${path}">
		<c:if test="${not empty label and css ne 'playIconWhite'}">
			<fmt:message key="${label}" />
		</c:if>
		</a>
		<c:if test="${not empty subText}">
		<div class="subText">
			<a href="${path}">
			<c:if test="${not empty subText}">
				<fmt:message key="${subText}"/>
			</c:if>
			</a>
		</div>
		</c:if>
	</c:otherwise>
</c:choose>
</div>