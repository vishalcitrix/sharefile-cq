<%@page import="com.citrixosd.utils.ContextRootTransformUtil,com.citrixosdRedesign.constants.ResourceConstants"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="mimeTypePDF" value="<%= ResourceConstants.Document.PDF_MIMETYPE %>"/>
<c:set var="mimeTypePPT" value="<%= ResourceConstants.Document.PPT_MIMETYPE %>"/>

<section class="resourcesFeature-container">
	<ul id="resourcesFeatureList" class="resourcesFeature-list">
		<c:if test="${fn:length(featuredResourceList) > 0}">
		    <c:forEach items="${featuredResourceList}" var="resourceItem">
		    	<c:set var="detailPagePath" value="${((resourceItem.resourceType eq 'Awards') || (resourceItem.resourceType eq 'Webinar' && resourceItem.isGated) || (resourceItem.isGated)) && (not isEditMode) ? (resourceItem.resourceType eq 'Webinar' ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : (not empty resourceItem.externalLink ? resourceItem.externalLink : '#')) : resourceItem.path}"/>
		        <li>
		        	<a href="${detailPagePath}">
		             <div class="resourcesFeature">
		             	<span class="icon-Favorite-a"></span>
		              	<span class="icon-Favorite-b"></span>
		              	<aside class="featured-image">
							<div style="background-image:url('${resourceItem.hasImage ? resourceItem.smallImagePath : resourceItem.hasVideo && not empty resourceItem.thumbnailPath ? resourceItem.thumbnailPath : resourceItem.smallImagePath}'); background-position: center center; background-size: cover; height: 200px;"></div>
							<c:if test="${resourceItem.hasVideo}">
								<span class="notch"></span>
								<span class="icon-Play"></span>
							</c:if>
							<c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePDF && !resourceItem.hasVideo}">
								<span class="notch"></span>
								<span class="icon-PDF-a"></span>
								<span class="icon-PDF-b"></span>
								<span class="icon-PDF-c"></span>
							</c:if>
							<c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePPT && !resourceItem.hasVideo}">
								<span class="notch"></span>
							    <span class="icon-PDF-a ppt"></span>
							    <span class="icon-PDF-b"></span>
							    <span class="icon-PPT-c"></span>
							</c:if>
		                 </aside>
		                 <section class="content-description">
				<div class="content-wrapper">
					<h2>
						${resourceItem.resourceType}
						<c:if test="${not empty resourceItem.date}">
							<span class="resource-date"><fmt:formatDate value="${resourceItem.date}" type="DATE" pattern="dd MMMM, yyyy"/></span>
						</c:if>
					</h2>
					<h3>${resourceItem.title}</h3>
					<p class="resources-feature-description">${resourceItem.description}</p>
				</div>
				<div class="read-article">
					<fmt:message key="sf.read.full.article"/>
				</div>
		                 </section>
		             </div>
		            </a>
		        </li>
		    </c:forEach>
		</c:if>
    </ul>
</section>