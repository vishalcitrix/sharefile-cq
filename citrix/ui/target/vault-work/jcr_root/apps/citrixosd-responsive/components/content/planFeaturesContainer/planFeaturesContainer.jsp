<%--
  Plan Features container component.
  This component can be used to display plan features details of configured plans.
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>

<%!
    private static final String PLAN_DATA_NODE = "features";
    public static final String NUMBER_OF_PLANS = "numberOfPlans";
 %>
<%
    final ArrayList<Map<String, Property>> planFetures = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode(PLAN_DATA_NODE)) {
        planFetures.addAll(Utilities.parseStructuredMultifield(currentNode.getNode(PLAN_DATA_NODE)));
    }
%>
<c:set var="planFetures" value="<%= planFetures %>"/>
<c:set var="numberOfPlans" value="<%= properties.get(NUMBER_OF_PLANS, "") %>"/>
<c:set var="disableFeatureLabelBorderClass" value=""/>

<div class="planAndPricing">
    <div class="row">
        <div class="medium-2 large-2 columns feature-index">
            <cq:include path="product-features" resourceType="foundation/components/parsys" />
        </div>
        <div class="medium-10 large-10 small-12 columns">
            <div class="row">
                <div class="medium-3 large-3 small-12 columns">
                    <cq:include path="pricing-plan-1" resourceType="foundation/components/parsys" />
                </div>
                <div class="medium-3 large-3 small-12 columns">
                    <cq:include path="pricing-plan-2" resourceType="foundation/components/parsys" />
                </div>
                <div class="medium-3 large-3 small-12 columns">
                    <cq:include path="pricing-plan-3" resourceType="foundation/components/parsys" />
                </div>
                <div class="medium-3 large-3 small-12 columns">
                    <cq:include path="pricing-plan-4" resourceType="foundation/components/parsys" />
                </div>
            </div>
        </div>
    </div>
    <c:forEach items="${planFetures}" var="feature">
        <div class="row planFeatures">
            <div class="medium-2 large-2 columns feature-index show-for-medium-up">
                <c:choose>
                    <c:when test="${feature.disableFeatureLabelBorder.string eq 'true'}">
                        <c:set var="disableFeatureLabelBorderClass" value=" no-border"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="disableFeatureLabelBorderClass" value=""/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${empty feature.featureLabel.string}">
                        <div class="left-row${disableFeatureLabelBorderClass}">
                        </div>
                    </c:when>
                    <c:when test="${empty feature.featureDesc.string}">
                        <div class="left-row${disableFeatureLabelBorderClass}">
                            <span>${feature.featureLabel.string}</span>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="left-row plus${disableFeatureLabelBorderClass}">
                            <span class="icon-plus">${feature.featureLabel.string}</span>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="medium-10 large-10 columns show-for-medium-up">
                <div class="row">
                    <div class="medium-3 large-3 columns basic">
                        <c:choose>
                            <c:when test="${not empty feature.basicTextValue.string and feature.showTextValue.string eq 'true'}">
                                <p>${feature.basicTextValue.string}</p>
                            </c:when>
                            <c:when test="${empty feature.basicTextValue.string and feature.showTextValue.string eq 'true' and empty feature.enableFeatureForBasic.string}">
                                <p class="empty"></p>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${feature.enableFeatureForBasic.string eq 'true'}">
                                        <p class="check"><span class="icon-check"></span></p>
                                    </c:when>
                                    <c:when test="${empty feature.enableFeatureForBasic.string}">
                                        <p class="check"><span class="icon-cancel"></span></p>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="small-12 medium-3 large-3 columns professional">
                        <c:choose>
                            <c:when test="${not empty feature.professionalTextValue.string and feature.showTextValue.string eq 'true'}">
                                <p>${feature.professionalTextValue.string}</p>
                            </c:when>
                            <c:when test="${empty feature.professionalTextValue.string and feature.showTextValue.string eq 'true' and empty feature.enableFeatureForProfessional.string}">
                                <p class="empty"></p>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${feature.enableFeatureForProfessional.string eq 'true'}">
                                        <p class="check"><span class="icon-check"></span></p>
                                    </c:when>
                                    <c:when test="${empty feature.enableFeatureForProfessional.string}">
                                        <p class="check"><span class="icon-cancel"></span></p>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="medium-3 large-3 columns <c:if test="${numberOfPlans eq '2'}">hide</c:if> corprate">
                        <c:choose>
                            <c:when test="${not empty feature.corporateTextValue.string and feature.showTextValue.string eq 'true'}">
                                <p>${feature.corporateTextValue.string}</p>
                            </c:when>
                            <c:when test="${empty feature.corporateTextValue.string and feature.showTextValue.string eq 'true' and empty feature.enableFeatureForCorporate.string}">
                                <p class="empty"></p>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${feature.enableFeatureForCorporate.string eq 'true'}">
                                        <p class="check"><span class="icon-check"></span></p>
                                    </c:when>
                                    <c:when test="${empty feature.enableFeatureForCorporate.string}">
                                        <p class="check"><span class="icon-cancel"></span></p>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="medium-3 large-3 columns <c:if test="${numberOfPlans eq '2' || numberOfPlans eq '3'}">hide</c:if> VirtualDataRoom">
                        <c:choose>
                            <c:when test="${not empty feature.virtualDataRoomTextValue.string and feature.showTextValue.string eq 'true'}">
                                <p>${feature.virtualDataRoomTextValue.string}</p>
                            </c:when>
                            <c:when test="${empty feature.virtualDataRoomTextValue.string and feature.showTextValue.string eq 'true' and empty feature.enableFeatureForVirtualDataRoom.string}">
                                <p class="empty"></p>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${feature.enableFeatureForVirtualDataRoom.string eq 'true'}">
                                        <p class="check"><span class="icon-check"></span></p>
                                    </c:when>
                                    <c:when test="${empty feature.enableFeatureForVirtualDataRoom.string}">
                                        <p class="check"><span class="icon-cancel"></span></p>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="medium-12 large-12 columns features" >
                        <p>${feature.featureDesc.string}</p>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<div class="clearBoth"></div>