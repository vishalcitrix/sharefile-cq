<%--
    Link List
    
    A collection of links which can have customizable properties. The link list 
    will also have a css class for the first and last item so it would be easier 
    to apply css classes to them. Link text is i18n.
    
    Also option to check if link is same as current page and add 'current' class to li
    
    vishal.gupta@citrix.com 
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String LINKS_PROPERTY = "links";
    private static final String STYLE_SPLITTER_PROPERTY = "splitter";

    public class Link {
        private String css;
        private String text;
        private String path;
        private String linkOption;
        private String visibilityClass;
        private boolean lastLargeLink;
        private boolean lastMediumLink;
        private boolean lastSmallLink;
        
        public Link() {
            this.lastLargeLink = false;
            this.lastMediumLink = false;
            this.lastSmallLink = false;
        }
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
        public void setVisibilityClass(String visibilityClass) { 
            this.visibilityClass = visibilityClass;
        }
        public String getVisibilityClass() {
            return this.visibilityClass;
        }
        public void setLastLargeLink(boolean lastLargeLink) {
            this.lastLargeLink = lastLargeLink;
        }
        public boolean getLastLargeLink() {
            return this.lastLargeLink;
        }
        public void setLastMediumLink(boolean lastMediumLink) {
            this.lastMediumLink = lastMediumLink;
        }
        public boolean getLastMediumLink() {
            return this.lastMediumLink;
        }
        public void setLastSmallLink(boolean lastSmallLink) {
            this.lastSmallLink = lastSmallLink;
        }
        public boolean getLastSmallLink() {
            return this.lastSmallLink;
        }
    }
    
    public List<Link> getLinks(NodeIterator nodeIter) {
        final List<Link> shares = new ArrayList<Link>();
        boolean showForLarge = false;
        boolean showForMedium = false;
        boolean showForSmall = false;
        Map<String,Boolean> showHideMap = new HashMap<String,Boolean>();
        Map<String,Link> lastLinksMap = new HashMap<String,Link>();
        
        showHideMap.put("show",true);
        showHideMap.put("hide",false);
        
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final Link link = new Link();
                    link.setCss(currLink.hasProperty("css") ? currLink.getProperty("css").getString() : "");
                    link.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : "");
                    link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : "");
                    link.setLinkOption(currLink.hasProperty("linkOption") ? currLink.getProperty("linkOption").getString() : "");
                    
                    String desktopProperty = (currLink.hasProperty("showForLarge") ? currLink.getProperty("showForLarge").getString() : "show");
                    String tabletProperty = (currLink.hasProperty("showForMedium") ? currLink.getProperty("showForMedium").getString() : "show");
                    String phoneProperty = (currLink.hasProperty("showForSmall") ? currLink.getProperty("showForSmall").getString() : "hide");
                    
                    showForLarge = (showHideMap.containsKey(desktopProperty) ? showHideMap.get(desktopProperty) : true);
                    showForMedium = (showHideMap.containsKey(tabletProperty) ? showHideMap.get(tabletProperty) : true);
                    showForSmall = (showHideMap.containsKey(phoneProperty) ? showHideMap.get(phoneProperty) : false);
                    
                    String visibilityClass = Utilities.getVisibilityClass(showForLarge,showForMedium,showForSmall);
                    link.setVisibilityClass(visibilityClass);
                    
                    if(showForLarge){
                        if(lastLinksMap.containsKey("lastLink_desktop")){
                            lastLinksMap.get("lastLink_desktop").setLastLargeLink(false);
                        }
                        link.setLastLargeLink(true);
                        lastLinksMap.put("lastLink_desktop",link);    
                    }
                    if(showForMedium){
                        if(lastLinksMap.containsKey("lastLink_tablet")){
                            lastLinksMap.get("lastLink_tablet").setLastMediumLink(false);
                        }
                        link.setLastMediumLink(true);
                        lastLinksMap.put("lastLink_tablet",link);    
                    }
                    if(showForSmall){
                        if(lastLinksMap.containsKey("lastLink_phone")){
                            lastLinksMap.get("lastLink_phone").setLastSmallLink(false);
                        }
                        link.setLastSmallLink(true);
                        lastLinksMap.put("lastLink_phone",link);    
                    }
                    
                    if(link.getCss() != null || link.getText() != null || link.getPath() != null) {
                        shares.add(link);
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }
%>

<%
    List<Link> links = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
    }
    
    final Map<String, Object> linkList = new HashMap<String, Object>();
    linkList.put(LINKS_PROPERTY, links);
    linkList.put(STYLE_SPLITTER_PROPERTY, currentStyle.get(STYLE_SPLITTER_PROPERTY, String.class));
%>

<c:set var="linkList" value="<%= linkList %>"/>
<c:set var="currentPageCheck" value="<%= properties.get("currentPage", false) %>"/>
<c:set var="currentPagePath" value="<%= currentPage.getPath() %>"/>
<c:set var="linkStyle" value="<%= properties.get("linkStyle", null) %>"/>
<c:set var="seperatorText" value="<%= properties.get("linkSeperator", "|") %>"/>

<c:choose>
    <c:when test="${fn:length(linkList.links) > 0}">
        <ul>
            <c:forEach items="${linkList.links}" begin="0" var="link" varStatus="i">
                <c:choose>
                    <c:when test="${link.linkOption eq '_blank'}">
                        <li class="<c:if test='${not empty linkStyle}'>${linkStyle}</c:if> ${link.visibilityClass} ${i.first ? 'first' : ''}${i.last ? 'last' : ''}${currentPageCheck ? (link.path eq currentPagePath ? ' current' : '') : ''}${link.css eq 'customer' ? ' customer' : '' }">
                            <c:if test="${link.css eq 'icon-chat'}">${link.text}</c:if><c:if test="${link.css eq 'icon-phone-traditional'}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption}" class="${link.css}"><fmt:message key="${link.text}"/></a></c:if><c:if test="${not (link.css eq 'icon-chat' || link.css eq 'icon-phone-traditional')}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} target="${link.linkOption}"><span class="${link.css}"></span><fmt:message key="${link.text}"/></a></c:if>
                            <c:if test="${(linkStyle eq 'hyperLink') && (not empty seperatorText) && (not i.last)}"><span class="linkSeperator ${link.lastLargeLink ? 'lastSeperatorLarge' : ''} ${link.lastMediumLink ? 'lastSeperatorMedium' : ''} ${link.lastSmallLink ? 'lastSeperatorSmall' : ''}">${seperatorText}</span></c:if>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="<c:if test='${not empty linkStyle}'>${linkStyle}</c:if> ${link.visibilityClass} ${i.first ? 'first' : ''}${i.last ? 'last' : ''}${currentPageCheck ? (link.path eq currentPagePath ? ' current' : '') : ''}${link.css eq ' customer' ? 'customer' : '' }">
                            <c:if test="${link.css eq 'icon-chat'}">${link.text}</c:if><c:if test="${link.css eq 'icon-phone-traditional'}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption}" class="${link.css}"><fmt:message key="${link.text}"/></a></c:if><c:if test="${not (link.css eq 'icon-chat' || link.css eq 'icon-phone-traditional')}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption}"><span class="${link.css}"></span><fmt:message key="${link.text}"/></a></c:if>
                            <c:if test="${(linkStyle eq 'hyperLink') && (not empty seperatorText) && (not i.last)}"><span class="linkSeperator ${link.lastLargeLink ? 'lastSeperatorLarge' : ''} ${link.lastMediumLink ? 'lastSeperatorMedium' : ''} ${link.lastSmallLink ? 'lastSeperatorSmall' : ''}">${seperatorText}</span></c:if>
                        </li>
                    </c:otherwise>
                </c:choose>
                <c:if test="${not empty linkList.splitter}">
                    <c:choose>
                        <c:when test="${i.last}">
                            <li style="display:inline;"><%--Do nothing --%></li>
                        </c:when>
                        <c:otherwise>
                            <li class="splitter" style="display:inline;"><span>${linkList.splitter}</span></li>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>