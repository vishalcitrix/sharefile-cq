<%--
    Background Color Component
    - Container component which holds the css class for the background color and can be dimensions of each
      color can be altered in CSS.

    vishal.gupta@citrix.com - Added option to specify different color
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String COLOR_POINT = "userColorPoint";
    public static final String GRADIENT_POINT = "userColorGradientPoint";
%>

<c:set var="colorPoint" value="<%= properties.get(COLOR_POINT, "50%") %>"/>
<c:set var="gradientPoint" value="<%= properties.get(GRADIENT_POINT, "100%") %>"/>
<c:choose>
    <c:when test="${not empty properties['userColor']}">
        <c:choose>
            <c:when test="${not empty properties['userColorGradient']}">
                <div style = "background-color: ${properties['userColor']};
                              background: -o-linear-gradient(top, ${properties['userColor']} ${colorPoint}, ${properties['userColorGradient']} ${gradientPoint});
                              background: -webkit-gradient(linear, left top, left bottom, color-stop(${colorPoint}, ${properties['userColorGradient']}));
                              background: -webkit-linear-gradient(top, ${properties['userColor']} ${colorPoint}, ${properties['userColorGradient']} ${gradientPoint});
                              background: -moz-linear-gradient(${colorPoint} ${gradientPoint} 90deg, ${properties['userColorGradient']}, ${properties['userColor']});
                              background: -ms-linear-gradient(top, ${properties['userColor']} ${colorPoint}, ${properties['userColorGradient']} ${gradientPoint});
                              background: linear-gradient(to bottom, ${properties['userColor']} ${colorPoint}, ${properties['userColorGradient']} ${gradientPoint});
                              filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='${properties['userColor']}',
                              endColorstr='${properties['userColorGradient']}', GradientType=0);"
                >
            </c:when>
            <c:otherwise>
                <div style = "background-color: ${properties['userColor']};">
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <div class = ${properties['color']}>
    </c:otherwise>
</c:choose>

    <cq:include script="content.jsp"/>
</div>
