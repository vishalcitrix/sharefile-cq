<%--
	Footer Navigation

	Creates layout with list for footer navigation. Column is 7,5 for desktop and tablet. For mobile, component changes to accordian.
	Dependency: Foundation Grid
	SCSS required: /citrixosd-sass/global-sass/components/global/_footerNavigation.scss
	JS required: citrixosd.unity (/design/src/content/jcr_root/etc/clientlibs/citrixosd/unity)
--%>
<%@page trimDirectiveWhitespaces="true" %>
<%@page import="com.citrixosd.utils.Utilities" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%
	//using Utilities to parse structured multi field
	ArrayList<Map<String,Property>> list1 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> list2 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> list3 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> list4 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> list5 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> list6 = new ArrayList<Map<String,Property>>();
	ArrayList<Map<String,Property>> socialList = new ArrayList<Map<String,Property>>();
	Node baseNode = null;
	
	if(currentNode != null && currentNode.hasNode("linkSet1")) {
		baseNode = currentNode.getNode("linkSet1").getNode("links");
		list1 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("linkSet2")) {
		baseNode = currentNode.getNode("linkSet2").getNode("links");
		list2 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("linkSet3")) {
		baseNode = currentNode.getNode("linkSet3").getNode("links");
		list3 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("linkSet4")) {
		baseNode = currentNode.getNode("linkSet4").getNode("links");
		list4 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("linkSet5")) {
		baseNode = currentNode.getNode("linkSet5").getNode("links");
		list5 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("linkSet6")) {
		baseNode = currentNode.getNode("linkSet6").getNode("links");
		list6 = Utilities.parseStructuredMultifield(baseNode);
	}
	if(currentNode != null && currentNode.hasNode("socialList")) {
		baseNode = currentNode.getNode("socialList").getNode("social");
		socialList = Utilities.parseStructuredMultifield(baseNode);
	}
%>
<c:set var="list1" value="<%= list1 %>"/>
<c:set var="list2" value="<%= list2 %>"/>
<c:set var="list3" value="<%= list3 %>"/>
<c:set var="list4" value="<%= list4 %>"/>
<c:set var="list5" value="<%= list5 %>"/>
<c:set var="list6" value="<%= list6 %>"/>
<c:set var="socialList" value="<%= socialList %>"/>
<div class="navList row collapse">
	<div class="columns large-7">
		<div class="row collapse">
			<div class="columns large-4">
				<h5 class="icon-down-open"><fmt:message key="${properties.listHead1}"/></h5>
<c:if test="${not empty list1}">
				<ul>
<c:forEach var="item" items="${list1}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
			<div class="columns large-4">
				<h5 class="icon-down-open"><fmt:message key="${properties.listHead2}"/></h5>
<c:if test="${not empty list2}">
				<ul>
<c:forEach var="item" items="${list2}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
			<div class="columns large-4">
				<h5 class="icon-down-open"><fmt:message key="${properties.listHead3}"/></h5>
<c:if test="${not empty list3}">
				<ul>
<c:forEach var="item" items="${list3}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
		</div>
	</div>
	<div class="columns large-5">
		<h5 class="icon-down-open"><fmt:message key="${properties.listHead4}"/></h5>
		<div class="row collapse">
			<div class="columns large-4 medium-4">
				<h6><fmt:message key="${properties.listSubHead1}"/></h6>
<c:if test="${not empty list4}">
				<ul>
<c:forEach var="item" items="${list4}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
			<div class="columns large-4 medium-4">
				<h6><fmt:message key="${properties.listSubHead2}"/></h6>
<c:if test="${not empty list5}">
				<ul>
<c:forEach var="item" items="${list5}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
			<div class="columns large-4 medium-4">
				<h6><fmt:message key="${properties.listSubHead3}"/></h6>
<c:if test="${not empty list6}">
				<ul>
<c:forEach var="item" items="${list6}">
<c:set var="fnItemPath">${item.path.string}</c:set>
					<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="${item.linkOption.string}"><fmt:message key="${item.text.string}"/></a></li>
</c:forEach>
				</ul>
</c:if>
			</div>
		</div>
	</div>
</div>
<c:if test="${not empty socialList}">
<div class="connect row collapse">
	<div class="columns large-12 medium-12 small-12">
		<h5 class="icon-down-open"><fmt:message key="${properties.listConnect}"/></h5>
		<ul>
<c:forEach var="item" items="${socialList}">
<c:set var="fnItemPath">${item.path.string}</c:set>
			<li><a href="<%= ContextRootTransformUtil.transformedPath((String)pageContext.getAttribute("fnItemPath"),request) %>" rel="popup" class="${item.icon.string}" title="<fmt:message key="${item.text.string}"/>"></a></li>
</c:forEach>
		</ul>
	</div>
</div>
</c:if>