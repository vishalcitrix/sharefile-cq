<%--
  Try Form component
  - Form with action, field text and theme options.
--%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@include file="/libs/foundation/global.jsp"%>
<c:set var="formLocale" value="<%= properties.get("locale","en_US") %>"/>
<c:set var="buttonURL" value="<%= ContextRootTransformUtil.transformedPath(properties.get("buttonURL",""),request) %>"/>

<form id="freeTrial" class="email-box" action="${buttonURL}" method="get">
	<div class="trial-box">
		<div>
			<input type="hidden" name="locale" value="${formLocale}" >
<%
			if (currentNode.hasNode("hiddenFields")) {
				final Node fieldNode = currentNode.getNode("hiddenFields");
				NodeIterator iter = fieldNode.getNodes();
				while (iter.hasNext()) {
					Node fieldItemNode = iter.nextNode();
					if (fieldItemNode.hasProperty("name") && fieldItemNode.hasProperty("value")) {
						%><input type="hidden" name="<%= fieldItemNode.getProperty("name").getString() %>" value="<%= fieldItemNode.getProperty("value").getString() %>"><%
					}
				}
			}
%>
		</div>
		<cq:include path="primaryText" resourceType="foundation/components/parsys" />
		<div class="row">
			<div class="column small-centered small-12">
				<div class="small-7 columns icon-envelope">
					<input type="text" id="emailAddress" Name="emailAddress" placeholder="${properties.emailPlaceHolder}" class="prefill">
				</div>
				<div class="small-5 columns">
					<input type="submit" class="show-for-medium-up" value="${properties.buttonLabel}">
					<input type="submit" class="show-for-small-only" value="${properties.buttonLabelSmall}">
				</div>
			</div>
		</div>
		
		<div class="row nocc">
			<div class="column small-centered small-10">
				${properties.textContent}
			</div>
		</div>
		
	</div>
</form>
<div class="clearBoth"></div>
