<%--
  Creates a country list with translations
--%>

<%@include file="/libs/foundation/global.jsp"%>
<cq:setContentBundle />

        <option value=''><fmt:message key="form.label.country"/></option>
        <option value="USA" title="NA"><fmt:message key="form.country.us"/></option>
        <option value="Canada" title="NA"><fmt:message key="form.country.ca"/></option>
        <option value="Albania" title="EMEA"><fmt:message key="form.country.al"/></option>
        <option value="Algeria" title="EMEA"><fmt:message key="form.country.dz"/></option>
        <option value="American Samoa" title=""><fmt:message key="form.country.as"/></option>
        <option value="Andorra" title=""><fmt:message key="form.country.ad"/></option>
        <option value="Angola" title="EMEA"><fmt:message key="form.country.ao"/></option>
        <option value="Anguilla" title=""><fmt:message key="form.country.ai"/></option>
        <option value="Antarctica" title=""><fmt:message key="form.country.aq"/></option>
        <option value="Antigua/Barbuda" title=""><fmt:message key="form.country.ag"/></option>
        <option value="Argentina" title=""><fmt:message key="form.country.ar"/></option>
        <option value="Armenia" title=""><fmt:message key="form.country.am"/></option>
        <option value="Aruba" title=""><fmt:message key="form.country.aw"/></option>
        <option value="Australia" title="APAC"><fmt:message key="form.country.au"/></option>
        <option value="Austria" title="EMEA"><fmt:message key="form.country.at"/></option>
        <option value="Azerbaijan" title="EMEA"><fmt:message key="form.country.az"/></option>
        <option value="Bahamas" title=""><fmt:message key="form.country.bs"/></option>
        <option value="Bahrain" title="EMEA"><fmt:message key="form.country.bh"/></option>
        <option value="Bangladesh" title="APAC"><fmt:message key="form.country.bd"/></option>
        <option value="Barbados" title=""><fmt:message key="form.country.bb"/></option>
        <option value="Belarus" title=""><fmt:message key="form.country.by"/></option>
        <option value="Belgium" title="EMEA"><fmt:message key="form.country.be"/></option>
        <option value="Belize" title=""><fmt:message key="form.country.bz"/></option>
        <option value="Benin" title=""><fmt:message key="form.country.bj"/></option>
        <option value="Bermuda" title=""><fmt:message key="form.country.bm"/></option>
        <option value="Bhutan" title="APAC"><fmt:message key="form.country.bt"/></option>
        <option value="Bolivia" title=""><fmt:message key="form.country.bo"/></option>
        <option value="Bosnia-Herz." title=""><fmt:message key="form.country.ba"/></option>
        <option value="Botswana" title=""><fmt:message key="form.country.bw"/></option>
        <option value="Bouvet Island" title=""><fmt:message key="form.country.bv"/></option>
        <option value="Brazil" title=""><fmt:message key="form.country.br"/></option>
        <option value="Brunei Darussalam" title="APAC"><fmt:message key="form.country.bn"/></option>
        <option value="Bulgaria" title=""><fmt:message key="form.country.bg"/></option>
        <option value="Burkina-Faso" title=""><fmt:message key="form.country.bf"/></option>
        <option value="Brunei Dar-es-S" title=""><fmt:message key="form.country.bi"/></option>
        <option value="Cambodia" title="APAC"><fmt:message key="form.country.kh"/></option>
        <option value="Cameroon" title=""><fmt:message key="form.country.cm"/></option>
        <option value="Cape Verde" title=""><fmt:message key="form.country.cv"/></option>
        <option value="Cayman Islands" title=""><fmt:message key="form.country.ky"/></option>
        <option value="Chad" title=""><fmt:message key="form.country.td"/></option>
        <option value="Chile" title=""><fmt:message key="form.country.cl"/></option>
        <option value="China" title="APAC"><fmt:message key="form.country.cn"/></option>
        <option value="Christmas Island" title=""><fmt:message key="form.country.cx"/></option>
        <option value="Colombia" title=""><fmt:message key="form.country.co"/></option>
        <option value="Comoros" title=""><fmt:message key="form.country.km"/></option>
        <option value="Congo" title=""><fmt:message key="form.country.cg"/></option>
        <option value="Cook Islands" title=""><fmt:message key="form.country.ck"/></option>
        <option value="Costa Rica" title=""><fmt:message key="form.country.cr"/></option>
        <option value="Cote d'Ivoire" title=""><fmt:message key="form.country.ci"/></option>
        <option value="Croatia" title="EMEA"><fmt:message key="form.country.hr"/></option>
        <option value="Cyprus" title=""><fmt:message key="form.country.cy"/></option>
        <option value="Czech Republic" title="EMEA"><fmt:message key="form.country.cz"/></option>
        <option value="Denmark" title="EMEA"><fmt:message key="form.country.dk"/></option>
        <option value="Djibouti" title=""><fmt:message key="form.country.dj"/></option>
        <option value="Dominica" title=""}><fmt:message key="form.country.dm"/>,
        <option value="Dominican Rep." title=""><fmt:message key="form.country.do"/></option>
        <option value="East Timor" title="APAC"><fmt:message key="form.country.tp"/></option>
        <option value="Ecuador" title=""><fmt:message key="form.country.ec"/></option>
        <option value="Egypt" title="EMEA"><fmt:message key="form.country.eg"/></option>
        <option value="El Salvador" title=""><fmt:message key="form.country.sv"/></option>
        <option value="Equatorial Gui." title=""><fmt:message key="form.country.gq"/></option>
        <option value="Eritrea" title=""><fmt:message key="form.country.er"/></option>
        <option value="Estonia" title=""><fmt:message key="form.country.ee"/></option>
        <option value="Ethiopia" title="EMEA"><fmt:message key="form.country.et"/></option>
        <option value="Falkland Islands" title=""><fmt:message key="form.country.fk"/></option>
        <option value="Faroe Islands" title=""><fmt:message key="form.country.fo"/></option>
        <option value="Fiji" title="APAC"><fmt:message key="form.country.fj"/></option>
        <option value="Finland" title="EMEA"><fmt:message key="form.country.fi"/></option>
        <option value="France" title="EMEA"><fmt:message key="form.country.fr"/></option>
        <option value="French Guayana" title=""><fmt:message key="form.country.gf"/></option>
        <option value="Frenc.Polynesia" title=""><fmt:message key="form.country.pf"/></option>
        <option value="Gabon" title=""><fmt:message key="form.country.ga"/></option>
        <option value="Gambia" title=""><fmt:message key="form.country.gm"/></option>
        <option value="Georgia" title=""><fmt:message key="form.country.ge"/></option>
        <option value="Germany" title="EMEA"><fmt:message key="form.country.de"/></option>
        <option value="Ghana" title="EMEA"><fmt:message key="form.country.gh"/></option>
        <option value="Gibraltar" title=""><fmt:message key="form.country.gi"/></option>
        <option value="Greece" title="EMEA"><fmt:message key="form.country.gr"/></option>
        <option value="Greenland" title=""><fmt:message key="form.country.gl"/></option>
        <option value="Grenada" title=""><fmt:message key="form.country.gd"/></option>
        <option value="Guadeloupe" title=""><fmt:message key="form.country.gp"/></option>
        <option value="Guatemala" title=""><fmt:message key="form.country.gt"/></option>
        <option value="Guinea" title=""><fmt:message key="form.country.gn"/></option>
        <option value="Guinea-Bissau" title=""><fmt:message key="form.country.gw"/></option>
        <option value="Guyana" title=""><fmt:message key="form.country.gy"/></option>
        <option value="Haiti" title=""><fmt:message key="form.country.ht"/></option>
        <option value="Honduras" title=""><fmt:message key="form.country.hn"/></option>
        <option value="Hong Kong" title="APAC"><fmt:message key="form.country.hk"/></option>
        <option value="Hungary" title="EMEA"><fmt:message key="form.country.hu"/></option>
        <option value="Iceland" title="EMEA"><fmt:message key="form.country.is"/></option>
        <option value="India" title="APAC"><fmt:message key="form.country.in"/></option>
        <option value="Indonesia" title="APAC"><fmt:message key="form.country.id"/></option>
        <option value="Ireland" title="EMEA"><fmt:message key="form.country.ie"/></option>
        <option value="Israel" title="EMEA"><fmt:message key="form.country.il"/></option>
        <option value="Italy" title="EMEA"><fmt:message key="form.country.it"/></option>
        <option value="Jamaica" title=""><fmt:message key="form.country.jm"/></option>
        <option value="Japan" title="APAC"><fmt:message key="form.country.jp"/></option>
        <option value="Jordan" title="EMEA"><fmt:message key="form.country.jo"/></option>
        <option value="Kazakhstan" title=""><fmt:message key="form.country.kz"/></option>
        <option value="Kenya" title="EMEA"><fmt:message key="form.country.ke"/></option>
        <option value="Kiribati" title="APAC"><fmt:message key="form.country.ki"/></option>
        <option value="South Korea" title="APAC"><fmt:message key="form.country.kp"/></option>
        <option value="Kuwait" title="EMEA"><fmt:message key="form.country.kw"/></option>
        <option value="Kyrgyzstan" title=""><fmt:message key="form.country.kg"/></option>
        <option value="Laos" title="APAC"><fmt:message key="form.country.la"/></option>
        <option value="Latvia" title=""><fmt:message key="form.country.lv"/></option>
        <option value="Lebanon" title=""><fmt:message key="form.country.lb"/></option>
        <option value="Lesotho" title=""><fmt:message key="form.country.ls"/></option>
        <option value="Liberia" title=""><fmt:message key="form.country.lr"/></option>
        <option value="Liechtenstein" title=""><fmt:message key="form.country.li"/></option>
        <option value="Lithuania" title=""><fmt:message key="form.country.lt"/></option>
        <option value="Luxembourg" title="EMEA"><fmt:message key="form.country.lu"/></option>
        <option value="Macau" title="APAC"><fmt:message key="form.country.mo"/></option>
        <option value="Macedonia" title=""><fmt:message key="form.country.mk"/></option>
        <option value="Madagascar" title=""><fmt:message key="form.country.mg"/></option>
        <option value="Malawi" title=""><fmt:message key="form.country.mw"/></option>
        <option value="Malaysia" title="APAC"><fmt:message key="form.country.my"/></option>
        <option value="Maldives" title="APAC"><fmt:message key="form.country.mv"/></option>
        <option value="Mali" title=""><fmt:message key="form.country.ml"/></option>
        <option value="Malta" title="EMEA"><fmt:message key="form.country.mt"/></option>
        <option value="Marshall Islands" title="APAC"><fmt:message key="form.country.mh"/></option>
        <option value="Martinique" title=""><fmt:message key="form.country.mq"/></option>
        <option value="Mauritania" title=""><fmt:message key="form.country.mr"/></option>
        <option value="Mauritius" title=""><fmt:message key="form.country.mu"/></option>
        <option value="Mayotte" title=""><fmt:message key="form.country.yt"/></option>
        <option value="Mexico" title=""><fmt:message key="form.country.mx"/></option>
        <option value="Micronesia" title="APAC"><fmt:message key="form.country.fm"/></option>
        <option value="Moldavia" title=""><fmt:message key="form.country.md"/></option>
        <option value="Monaco" title=""><fmt:message key="form.country.mc"/></option>
        <option value="Mongolia" title="APAC"><fmt:message key="form.country.mn"/></option>
        <option value="Montserrat" title=""><fmt:message key="form.country.ms"/></option>
        <option value="Morocco" title="EMEA"><fmt:message key="form.country.ma"/></option>
        <option value="Mozambique" title=""><fmt:message key="form.country.mz"/></option>
        <option value="Myanmar" title="APAC"><fmt:message key="form.country.mm"/></option>
        <option value="Namibia" title=""><fmt:message key="form.country.na"/></option>
        <option value="Nauru" title="APAC"><fmt:message key="form.country.nr"/></option>
        <option value="Nepal" title="APAC"><fmt:message key="form.country.np"/></option>
        <option value="Netherlands" title="EMEA"><fmt:message key="form.country.nl"/></option>
        <option value="Netherlands Antilles" title=""><fmt:message key="form.country.an"/></option>
        <option value="New Caledonia" title=""><fmt:message key="form.country.nc"/></option>
        <option value="New Zealand" title="APAC"><fmt:message key="form.country.nz"/></option>
        <option value="Nicaragua" title=""><fmt:message key="form.country.ni"/></option>
        <option value="Niger" title=""><fmt:message key="form.country.ne"/></option>
        <option value="Nigeria" title="EMEA"><fmt:message key="form.country.ng"/></option>
        <option value="Niue" title=""><fmt:message key="form.country.nu"/></option>
        <option value="Norfolk Island" title=""><fmt:message key="form.country.nf"/></option>
        <option value="Norway" title="EMEA"><fmt:message key="form.country.no"/></option>
        <option value="Oman" title="EMEA"><fmt:message key="form.country.om"/></option>
        <option value="Pakistan" title="APAC"><fmt:message key="form.country.pk"/></option>
        <option value="Palau" title="APAC"><fmt:message key="form.country.pw"/></option>
        <option value="Panama" title=""><fmt:message key="form.country.pa"/></option>
        <option value="Papua Nw Guinea" title="APAC"><fmt:message key="form.country.pg"/></option>
        <option value="Paraguay" title=""><fmt:message key="form.country.py"/></option>
        <option value="Peru" title=""><fmt:message key="form.country.pe"/></option>
        <option value="Philippines" title="APAC"><fmt:message key="form.country.ph"/></option>
        <option value="Pitcairn" title=""><fmt:message key="form.country.pn"/></option>
        <option value="Poland" title="EMEA"><fmt:message key="form.country.pl"/></option>
        <option value="Portugal" title="EMEA"><fmt:message key="form.country.pt"/></option>
        <option value="Qatar" title=""><fmt:message key="form.country.qa"/></option>
        <option value="Reunion" title=""><fmt:message key="form.country.re"/></option>
        <option value="Romania" title="EMEA"><fmt:message key="form.country.ro"/></option>
        <option value="Russian Fed." title="EMEA"><fmt:message key="form.country.ru"/></option>
        <option value="Rwanda" title=""><fmt:message key="form.country.rw"/></option>
        <option value="St Kitts&Nevis" title=""><fmt:message key="form.country.kn"/></option>
        <option value="Saint Lucia" title=""><fmt:message key="form.country.lc"/></option>
        <option value="Western Samoa" title="APAC"><fmt:message key="form.country.ws"/></option>
        <option value="S.Tome,Principe" title=""><fmt:message key="form.country.st"/></option>
        <option value="Saudi Arabia" title="EMEA"><fmt:message key="form.country.sa"/></option>
        <option value="Senegal" title=""><fmt:message key="form.country.sn"/></option>
        <option value="Seychelles" title=""><fmt:message key="form.country.sc"/></option>
        <option value="Sierra Leone" title=""><fmt:message key="form.country.sl"/></option>
        <option value="Singapore" title="APAC"><fmt:message key="form.country.sg"/></option>
        <option value="Slovakia" title="EMEA"><fmt:message key="form.country.sk"/></option>
        <option value="Slovenia" title="EMEA"><fmt:message key="form.country.si"/></option>
        <option value="Solomon Islands" title="APAC"><fmt:message key="form.country.sb"/></option>
        <option value="Somalia" title=""><fmt:message key="form.country.so"/></option>
        <option value="South Africa" title="EMEA"><fmt:message key="form.country.za"/></option>
        <option value="Spain" title="EMEA"><fmt:message key="form.country.es"/></option>
        <option value="Sri Lanka" title="APAC"><fmt:message key="form.country.lk"/></option>
        <option value="St. Helena" title=""><fmt:message key="form.country.sh"/></option>
        <option value="Suriname" title=""><fmt:message key="form.country.sr"/></option>
        <option value="Swaziland" title=""><fmt:message key="form.country.sz"/></option>
        <option value="Sweden" title="EMEA"><fmt:message key="form.country.se"/></option>
        <option value="Switzerland" title="EMEA"><fmt:message key="form.country.ch"/></option>
        <option value="Taiwan" title="APAC"><fmt:message key="form.country.tw"/></option>
        <option value="Tajikistan" title=""><fmt:message key="form.country.tj"/></option>
        <option value="Tanzania" title="EMEA"><fmt:message key="form.country.tz"/></option>
        <option value="Thailand" title="APAC"><fmt:message key="form.country.th"/></option>
        <option value="Togo" title=""><fmt:message key="form.country.tg"/></option>
        <option value="Tokelau" title=""><fmt:message key="form.country.tk"/></option>
        <option value="Tonga" title="APAC"><fmt:message key="form.country.to"/></option>
        <option value="Trinidad,Tobago" title=""><fmt:message key="form.country.tt"/></option>
        <option value="Tunisia" title=""><fmt:message key="form.country.tn"/></option>
        <option value="Turkey" title="EMEA"><fmt:message key="form.country.tr"/></option>
        <option value="Turkmenistan" title=""><fmt:message key="form.country.tm"/></option>
        <option value="Tuvalu" title="APAC"><fmt:message key="form.country.tv"/></option>
        <option value="Uganda" title="EMEA"><fmt:message key="form.country.ug"/></option>
        <option value="Ukraine" title=""><fmt:message key="form.country.ua"/></option>
        <option value="Utd.Arab Emir." title="EMEA"><fmt:message key="form.country.ae"/></option>
        <option value="United Kingdom" title="EMEA"><fmt:message key="form.country.gb"/></option>
        <option value="Uruguay" title=""><fmt:message key="form.country.uy"/></option>
        <option value="Uzbekistan" title=""><fmt:message key="form.country.uz"/></option>
        <option value="Vanuatu" title="APAC"><fmt:message key="form.country.vu"/></option>
        <option value="Vatican City" title=""><fmt:message key="form.country.va"/></option>
        <option value="Venezuela" title=""><fmt:message key="form.country.ve"/></option>
        <option value="Vietnam" title="APAC"><fmt:message key="form.country.vn"/></option>
        <option value="Western Sahara" title=""><fmt:message key="form.country.eh"/></option>
        <option value="Yemen" title=""><fmt:message key="form.country.ye"/></option>
        <option value="Zambia" title=""><fmt:message key="form.country.zm"/></option>
        <option value="Zimbabwe" title=""><fmt:message key="form.country.zw"/></option>
        <option value="Other" title="">Other</option>

