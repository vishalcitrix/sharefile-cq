<%--
	Gradient Color Holder
--%>

<div class="hovercontent" style="background: ${properties.gradientcolorFirst}; /* Old browsers */
    background: -moz-linear-gradient(top, ${properties.gradientcolorFirst} 0%, ${properties.gradientcolorSecond} 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, ${properties.gradientcolorFirst}), color-stop(100%, ${properties.gradientcolorSecond})); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, ${properties.gradientcolorFirst} 0%, ${properties.gradientcolorSecond} 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, ${properties.gradientcolorFirst} 0%, ${properties.gradientcolorSecond} 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, ${properties.gradientcolorFirst} 0%, ${properties.gradientcolorSecond} 100%); /* IE10+ */
    background: linear-gradient(to bottom,${properties.gradientcolorFirst} 0%, ${properties.gradientcolorSecond} 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='${properties.gradientcolorFirst}', endColorstr='${properties.gradientcolorSecond}', GradientType=0 ); /* IE6-9 */ ">
	<p class="title">${properties.titleGardient}</p>
	<p class="description">${properties.subtitle}</p>
	<p class="playvideo icon-play"></p>
</div>
 
 
