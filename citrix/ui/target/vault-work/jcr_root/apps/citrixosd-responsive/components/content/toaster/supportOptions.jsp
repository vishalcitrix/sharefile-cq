<%@ page trimDirectiveWhitespaces="true" %>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
    List<Map<String, Property>> links = new ArrayList<Map<String, Property>>();
    Map<String,Boolean> showHideMap = new HashMap<String,Boolean>();
    String[] linkVisibility = null;

    showHideMap.put("show",true);
    showHideMap.put("hide",false);

    if (currentNode != null && currentNode.hasNode("links")) {
        final Node baseNode = currentNode.getNode("links");
        links = Utilities.parseStructuredMultifield(baseNode);
        linkVisibility = new String[links.size()];
        int linkIndex = 0;

        for(Map<String, Property> link : links){
            String desktopProperty = (link.containsKey("showForLarge") ? link.get("showForLarge").getString() : "hide");
            String tabletProperty = (link.containsKey("showForMedium") ? link.get("showForMedium").getString() : "hide");
            String phoneProperty = (link.containsKey("showForSmall") ? link.get("showForSmall").getString() : "hide");
            
            boolean showForLarge = (showHideMap.containsKey(desktopProperty) ? showHideMap.get(desktopProperty) : false);
            boolean showForMedium = (showHideMap.containsKey(tabletProperty) ? showHideMap.get(tabletProperty) : false);
            boolean showForSmall = (showHideMap.containsKey(phoneProperty) ? showHideMap.get(phoneProperty) : false);

            String visibilityClass = Utilities.getVisibilityClass(showForLarge,showForMedium,showForSmall);
            linkVisibility[linkIndex] = visibilityClass;
            linkIndex++;
        }
    }
%>

<c:set var="linkList" value="<%= links %>"/>
<c:set var="currentPagePath" value="<%= currentPage.getPath() %>"/>
<c:set var="linkVisibility" value="<%= linkVisibility %>"/>

<c:choose>
    <c:when test="${fn:length(linkList) > 0}">
        <div class="supportOptions">
            <ul>
                <c:forEach items="${linkList}" begin="0" var="link" varStatus="i">
                    <c:choose>
                        <c:when test="${link.linkOption.string eq '_blank'}">
                            <li class="${linkVisibility[i.index]} ${i.first ? ' first' : ''}${i.last ? ' last' : ''}${link.css.string eq 'customer' ? ' customer' : '' }">
                                <c:if test="${link.css.string eq 'icon-chat'}">${link.text.string}</c:if><c:if test="${link.css.string eq 'icon-phone-traditional'}"><a href="${not empty link.path ? link.path.string : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption..string}" class="${link.css.string}"><span class="show-for-medium-up"><fmt:message key="${link.text.string}"/></span><span class="show-for-small-only">Call</span></a></c:if><c:if test="${not (link.css.string eq 'icon-chat' || link.css.string eq 'icon-phone-traditional')}"><a href="${not empty link.path ? link.path.string : ''}" ${empty link.path ? 'onclick="return false;"' : ''} target="${link.linkOption.string}"><span class="${link.css.string}"></span><fmt:message key="${link.text.string}"/></a></c:if>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="${linkVisibility[i.index]} ${i.first ? ' first' : ''}${i.last ? ' last' : ''}${link.css.string eq ' customer' ? 'customer' : '' }">
                                <c:if test="${link.css.string eq 'icon-chat'}">${link.text.string}</c:if><c:if test="${link.css.string eq 'icon-phone-traditional'}"><a href="${not empty link.path ? link.path.string : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption.string}" class="${link.css.string}"><span class="show-for-medium-up"><fmt:message key="${link.text.string}"/></span><span class="show-for-small-only">Call</span></a></a></c:if><c:if test="${not (link.css.string eq 'icon-chat' || link.css.string eq 'icon-phone-traditional')}"><a href="${not empty link.path ? link.path.string : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption.string}"><span class="${link.css.string}"></span><fmt:message key="${link.text.string}"/></a></c:if>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
    </c:when>
</c:choose>