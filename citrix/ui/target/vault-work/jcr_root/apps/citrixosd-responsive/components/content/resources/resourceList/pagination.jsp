<%@ page import="com.day.cq.personalization.ClientContextUtil,
				com.citrixosdRedesign.constants.ResourceConstants,
				com.citrixosd.utils.ContextRootTransformUtil" %>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final String uniqueRequestParameter = ClientContextUtil.getId(resource.getPath());
    final String[] selectors = sling.getRequest().getRequestPathInfo().getSelectors();
    if(selectors != null) {
        for(int i = 0; i < selectors.length; i++) {
            if(selectors[i].equals("pagination")) {
                request.setAttribute(uniqueRequestParameter + "-page-index", selectors[++i]);
            }
            if(selectors[i].equals("products")) {
                request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.PRODUCTS, selectors[++i]);
            }
            if(selectors[i].equals("categories")) {
                request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.CATEGORIES, selectors[++i]);
            }
            if(selectors[i].equals("topics")) {
                request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.TOPICS, selectors[++i]);
            }
            if(selectors[i].equals("industries")) {
                request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.INDUSTRIES, selectors[++i]);
            }
        }
    }else {
        request.setAttribute(uniqueRequestParameter, "0");
    }
%>

<%-- Avoid SEO from indexing paginated pages. --%>
<head>
    <meta name="robots" content="noindex">
    <%-- Redirect the end user if they hit the paginated page --%>
    <script>
        if(typeof $ === "undefined") {
        	window.location = "<%= ContextRootTransformUtil.transformedPath(currentPage.getPath(),request) %>";
        }
    </script>
</head>

<cq:include script="resourceList.jsp"/>
