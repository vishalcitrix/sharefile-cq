<%--
    Standard Detail Page (takes care of All Documents types, Customer Story, Video and Case Studies)- vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils,
				com.citrixosd.SiteUtils,
				com.citrixosd.utils.ContextRootTransformUtil,
				com.citrixosdRedesign.models.resource.StandardDetail,
				com.citrixosdRedesign.constants.ResourceConstants,
				com.day.cq.dam.api.Asset,
				com.day.cq.commons.jcr.JcrConstants,
				com.day.cq.dam.api.DamConstants,
				com.day.cq.wcm.api.WCMMode,
				org.apache.sling.api.SlingConstants,
				com.day.cq.tagging.Tag,
				com.day.cq.tagging.TagManager,
				java.util.Arrays,
				java.text.DateFormat,
				java.text.SimpleDateFormat" %>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String ENABLE_PDF_TO_HTML_PROPERTY = "enablePdfToHtml";
    private static final String PDF_TO_HTML = "pdfToHtml";
    private static final String CONTENT_TYPE = "contentType";
%>
<%
    final String RTE_COMPONENT = properties.get("product","sf") + "/components/content/resources/resourceText";
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    final boolean enablePdfToHtml = properties.get(ENABLE_PDF_TO_HTML_PROPERTY, false);
    final String[] contentType = properties.get(CONTENT_TYPE, String[].class);
    final boolean documentType = contentType == null ? false : Arrays.asList(contentType).contains("document");
    final boolean videoType = contentType == null ? false : Arrays.asList(contentType).contains("video");
    DateFormat iso8601DateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HHmmZ");
    String iso8601Date = "";
       
    if(currentNode != null){
        final String damAsset = properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) != null ? properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) : null;
        final Resource damResource = damAsset != null ? resourceResolver.getResource(damAsset) : null;
        final Asset asset = damResource != null ? damResource.adaptTo(Asset.class) : null;
        final String assetType = asset!= null ? asset.getMimeType() : null;
        
        if(WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
            if(enablePdfToHtml && !currentNode.hasNode(PDF_TO_HTML) && documentType) {
                if(asset != null && asset.getMimeType().equals(ResourceConstants.Document.PDF_MIMETYPE)) {
                    final Node assetNode = damResource.adaptTo(Node.class);
                    final Node assetNodeJcr = assetNode.getNode(JcrConstants.JCR_CONTENT);
                    final Node metadata = assetNodeJcr.hasNode(DamConstants.METADATA_FOLDER) ? assetNodeJcr.getNode(DamConstants.METADATA_FOLDER) : null;
                    if(metadata != null) {
                        final String html = metadata.getProperty("html").getString();
                        if(html != null && html.trim().length() > 0) {
                            final Session session = resourceResolver.adaptTo(Session.class);
                            final Node pdfToHtmlNode = currentNode.addNode(PDF_TO_HTML);
                            pdfToHtmlNode.setProperty("text", html);
                            pdfToHtmlNode.setProperty(SlingConstants.PROPERTY_RESOURCE_TYPE, RTE_COMPONENT);
                            session.save();
                        }
                    }
                }
            }
        }
    }
    
    //Standard Detail Object
    StandardDetail details = new StandardDetail();
    details = ResourceUtils.getStandardDetails(resource, currentPage, currentDesign, request);
    try{
        iso8601Date = iso8601DateFormat.format(details.getDate());
    } catch(Exception e){}
%>

<c:set var="details" value="<%= details %>"/>
<c:set var="iso8601Date" value="<%= iso8601Date %>"/>
<c:set var="documentType" value="<%= documentType %>"/>
<c:set var="videoType" value="<%= videoType %>"/>
<c:set var="mimeTypePDF" value="<%= ResourceConstants.Document.PDF_MIMETYPE %>"/>
<c:set var="mimeTypePPT" value="<%= ResourceConstants.Document.PPT_MIMETYPE %>"/>
<c:set var="baseURL" value="<%= request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath()) %>"/>
<c:set var="parentPagePath" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getParent().getPath(),request) %>"/>
<c:set var="parentPageTitle" value="<%= currentPage.getParent().getTitle() %>"/>
<c:set var="landingPagePath" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getParent().getParent().getPath(),request) %>"/>
<c:set var="landingPageTitle" value="<%= currentPage.getParent().getParent().getTitle() %>"/>

<article class="resourceDetail">
    <section class="mainFeaturedContent" >
        <%-- Type--%>
        <div class="feature-type">
            <a href="${landingPagePath}">${landingPageTitle}</a> / <a href="${parentPagePath}">${parentPageTitle}</a> / <span class="category">${details.resourceType}</span>
        </div>
        
        <%-- Title--%>
        <header>
            <h1 class="feature-title">${details.title}</h1>
        </header>
        
        <%-- Author --%>
        <c:if test="${not empty details.authorName}">
            <div class="author">
                <fmt:message key="author.by"></fmt:message> ${details.authorName}
                <c:if test="${not empty details.authorTitle}">
                 , ${details.authorTitle}
                </c:if>
            </div>
        </c:if>
         
        <%-- Download link--%>
        <c:if test="${not empty details.downloadType}">
            <%-- PDF Type --%>
            <c:if test="${details.downloadType eq 'pdf' && details.damAssetType eq mimeTypePDF && documentType}">
                <div class='document featured-link'><span class='icon-PDF-a'></span><span class='icon-PDF-b'></span><span class='icon-PDF-c'></span><a rel='external' href='${details.damAssetPath}'><fmt:message key="download.this.doc"></fmt:message></a></div>
            </c:if>
            
            <%-- PPT Type --%>
            <c:if test="${details.downloadType eq 'ppt' && details.damAssetType eq mimeTypePPT && documentType}">
                <div class='ppt featured-link'><span class='icon-PDF-a ppt'></span><span class='icon-PDF-b'></span><span class='icon-PPT-c'></span><a href='${details.damAssetPath}'><fmt:message key="download.this.doc"></fmt:message></a></div>
            </c:if>
            
            <%-- External Link --%>
            <c:if test="${details.downloadType eq 'external' && not empty details.externalLink && not empty details.externalLinkTitle}">
                <div class='featured-link'><a href='${details.externalLink}'>${details.externalLinkTitle}</a></div>
            </c:if>
        </c:if>
        
        <c:choose>
            <c:when test="${not empty details.videoPlayer && videoType}">
                <%-- Video --%>
                <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
                    <meta itemprop="thumbnailURL" content="${details.thumbnailPath}" />
                    <meta itemprop="name" content="${details.title}" />
                    <meta itemprop="description" content="${details.description}" />
                    <meta itemprop="contentURL" content="http://c.brightcove.com/services/viewer/federated_f9?playerID=${details.playerId}&playerKey=${details.playerKey}&isVid=1&isUI=1&videoID=${details.videoPlayer}" />
                    <meta itemprop="duration" content="${details.videoLength}" />
                    <c:if test="${not empty iso8601Date}">
                    <meta itemprop="uploadDate" content="${iso8601Date}" />
                    </c:if>
                    <div id="${details.id}" class="main-image brightcove-container" 
                         data-video-player="${details.videoPlayer}" 
                         data-player-id="${details.playerId}" 
                         data-player-key="${details.playerKey}">
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <%-- Image--%>
                <c:if test="${empty properties.hideBanner && not empty details.imagePath}">
                    <img src="${details.imagePath}" class="main-image" />
                </c:if>
            </c:otherwise>
        </c:choose>
        
        <c:if test="${properties.enablePdfToHtml && documentType && details.damAssetType eq mimeTypePDF}">
            <cq:include path="<%= PDF_TO_HTML %>" resourceType="<%= RTE_COMPONENT %>"/>
        </c:if>
        <cq:include path="detailContent" resourceType="foundation/components/parsys"/>
        
        <%-- Download link--%>
        <c:if test="${not empty details.downloadType}">
            <%-- PDF Type --%>
            <c:if test="${details.downloadType eq 'pdf' && details.damAssetType eq mimeTypePDF && documentType}">
                <div class='document featured-link'><span class='icon-PDF-a'></span><span class='icon-PDF-b'></span><span class='icon-PDF-c'></span><a rel='external' href='${details.damAssetPath}'><fmt:message key="download.this.doc"></fmt:message></a></div>
            </c:if>
            
            <%-- PPT Type --%>
            <c:if test="${details.downloadType eq 'ppt' && details.damAssetType eq mimeTypePPT && documentType}">
                <div class='ppt featured-link'><span class='icon-PDF-a ppt'></span><span class='icon-PDF-b'></span><span class='icon-PPT-c'></span><a href='${details.damAssetPath}'><fmt:message key="download.this.doc"></fmt:message></a></div>
            </c:if>
            
            <%-- External Link --%>
            <c:if test="${details.downloadType eq 'external' && not empty details.externalLink && not empty details.externalLinkTitle}">
                <div class='featured-link'><a href='${details.externalLink}'>${details.externalLinkTitle}</a></div>
            </c:if>
        </c:if>
        
        <div class="addthis_toolbox addthis_default_style" addthis:url="${baseURL}${details.path}" addthis:title="${details.title}" addthis:description="${details.description}">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_button_linkedin_counter"></a>
            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
            <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
    </section>
</article>

<c:if test="${not empty details.videoPlayer && videoType}">
    <script type="text/javascript">
        $(document).ready(function() {
            function createVideo() {
                BCL.createVideoWithNoAutoPlay(
                    "100%",
                    "100%",
                    "${details.playerId}",
                    "${details.playerKey}",
                    "${details.videoPlayer}",
                    "${details.id}");
                brightcove.createExperiences();
            }
            
            window.addEventListener('orientationchange', createVideo);  
            createVideo(); 
        });
    </script>
</c:if>