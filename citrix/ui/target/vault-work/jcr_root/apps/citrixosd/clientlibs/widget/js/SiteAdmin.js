//Overrides for SiteAdmin to kick off workflows rather than activate directly.

CQ.wcm.SiteAdmin.internalActivatePage = function(paths, callback) {
    if (callback == undefined) {
        // assume scope is admin and reload grid
        var admin = this;
        callback = function(options, success, response) {
            if (success) admin.reloadPages();
            else admin.unmask();
        };
    }
    /*
    CQ.HTTP.post(
        CQ.shared.HTTP.externalize("/bin/replicate.json"),
        callback,
        { "_charset_":"utf-8", "path":paths, "cmd":"Activate" }
    );*/
    var params = {
        "_charset_":"UTF-8",
        "model":"/etc/workflow/models/request_for_activation/jcr:content/model",
        //"payload":CQ.wcm.SiteAdmin.getTargetFromList(),
        "payload":paths,
        "payloadType":"JCR_PATH"
    };

    CQ.HTTP.post("/etc/workflow/instances",
        function(options, success, response) {
            if (!success) {
                CQ.Ext.Msg.alert(
                    CQ.I18n.getMessage("Error"),
                    CQ.I18n.getMessage("Could not schedule page for deactivation."));
            } else {
                admin.reloadPages();
            }
        },
        params
   );

};


CQ.wcm.SiteAdmin.internalDeactivatePage = function() {
    var admin = this;
    this.mask();
    var paths = [];
    var selections = this.getSelectedPages();
    for (var i=0; i<selections.length; i++) {
        paths.push(selections[i].id);
    }
    /*CQ.HTTP.post(
        CQ.shared.HTTP.externalize("/bin/replicate.json"),
        function(options, success, response) {
            if (success) admin.reloadPages();
            else admin.unmask();
        },
        { "_charset_":"utf-8", "path":paths, "cmd":"Deactivate" }
    );*/
    var params = {
        "_charset_":"UTF-8",
        "model":"/etc/workflow/models/request_for_deactivation/jcr:content/model",
        //"payload":CQ.wcm.SiteAdmin.getTargetFromList(),
        "payload":paths,
        "payloadType":"JCR_PATH"
    };

    CQ.HTTP.post("/etc/workflow/instances",
        function(options, success, response) {
            if (!success) {
                CQ.Ext.Msg.alert(
                    CQ.I18n.getMessage("Error"),
                    CQ.I18n.getMessage("Could not schedule page for deactivation."));
            } else {
                admin.reloadPages();
            }
        },
        params
   );
};