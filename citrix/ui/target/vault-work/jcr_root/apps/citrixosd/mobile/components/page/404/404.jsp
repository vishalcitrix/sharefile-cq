<%--
	Page: includes head, body, and foot
--%>

<%@page import="java.util.Locale"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%
    Locale pageLocale = currentPage.getLanguage(false);
    response.setStatus(404);
%>
<!DOCTYPE html>
<html lang="<%= pageLocale %>">

	<cq:include script="head.jsp"/>
	
	<cq:include script="body.jsp"/>
	
	<cq:include script="foot.jsp"/>

</html>