<%--

  Background Color Component
  
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="colorContainer ${empty properties.style ? 'lightGrey' : properties.style }">
		<cq:include path="par" resourceType="foundation/components/parsys" />

	<c:if test="${properties.addArrow}"> 
		<div class="bottom-arrow"></div>
	</c:if>
</div>
