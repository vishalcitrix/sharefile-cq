<%--
    Main Navigation
    
    Automatically generate the component based on site structure. Categories pages are 
    auto-populated based on the pages in the depth of 3 and is hideInNav disabled. The navigation 
    toggle is on the left side which contains all the other category pages. The current category 
    page is also automatically generated. The page bullet is displayed base on the page property.
    
    Dependencies: Page property, MainNavigation.js
    
    achew@siteworx.com
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    private static final int DEPTH = 2;
    public final String HIDE_BUTTON = "hideButton";
%>

<%
    final Page rootPage = currentPage.getAbsoluteParent(DEPTH);
    final List<Page> filteredCategoriesPage = new ArrayList<Page>(); 
    final Iterator<Page> categoriesPage = rootPage.listChildren();
    while(categoriesPage.hasNext()) {
        final Page categoryPage = categoriesPage.next();
        if(!categoryPage.isHideInNav()) {
            filteredCategoriesPage.add(categoryPage);
        }
    }
    
    Page currentCategoryPage = null;
    if(currentPage != null) {
        final Page categoryPage = currentPage.getAbsoluteParent(DEPTH + 1);
        if(categoryPage != null) {
            currentCategoryPage = categoryPage;
        }
    }
%>

<c:set var="rootPage" value="<%= rootPage %>"/>
<c:set var="currentPage" value="<%= currentPage %>"/>
<c:set var="currentCategoryPage" value="<%= currentCategoryPage %>"/>
<c:set var="filteredCategoriesPage" value="<%= filteredCategoriesPage %>"/>
<c:set var="hideButton" value="<%= properties.get(HIDE_BUTTON, "") %>"/>
    
    <div class="navigation-logo">
    <a id="mainNavigation" href="#"></a>
            
        <c:if test="${hideButton == ''}">
	        <swx:setWCMMode mode="READ_ONLY">
	            <cq:include path="button" resourceType="citrixosd/mobile/components/content/button"/>
	        </swx:setWCMMode>
        </c:if>
            
    <swx:setWCMMode mode="READ_ONLY">
        <cq:include script="logo.jsp"/>
    </swx:setWCMMode>
    
    <div class="clearBoth"></div>
</div>

<ul id="navigationDropDown">
    <li>
        <a href="${rootPage.path}">
            <span class="navigation-option page-sprite ${rootPage.properties['bulletIcon']}"></span>
            <span class="navigation-title">${not empty rootPage.navigationTitle ? rootPage.navigationTitle : not empty rootPage.title ? rootPage.title : rootPage.name}</span>
        </a>
    </li>
    <c:forEach items="${filteredCategoriesPage}" var="categoryPage">
        <li>
            <a href="${categoryPage.path}">
                <span class="navigation-option page-sprite ${categoryPage.properties['bulletIcon']}"></span>
                <span class="navigation-title">${not empty categoryPage.navigationTitle ? categoryPage.navigationTitle : not empty categoryPage.title ? categoryPage.title : categoryPage.name}</span>
            </a>
        </li>
    </c:forEach>
</ul>

<c:if test="${not empty currentCategoryPage}">
    <div id="currentCategory" class="current-category-container">
        <p>
            <span class="page-sprite ${currentCategoryPage.properties['bulletIcon']}"></span>
            <span class="navigation-title">${not empty currentCategoryPage.navigationTitle ? currentCategoryPage.navigationTitle : not empty currentCategoryPage.title ? currentCategoryPage.title : currentCategoryPage.name}</span>
        </p>
    </div>
</c:if>