<%--
	Page: includes head, body, and foot
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.foundation.ELEvaluator"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%
	//Redirects
	String redirectTarget = properties.get("redirectTarget", null);
	if(redirectTarget != null && redirectTarget.length() > 0) {
		redirectTarget = ELEvaluator.evaluate(redirectTarget, slingRequest, pageContext);
	    if(WCMMode.fromRequest(request) != WCMMode.EDIT) {
	        //Check for recursion
	        if(!redirectTarget.equals(currentPage.getPath())) {
	            final String redirectTo = slingRequest.getResourceResolver().map(request, redirectTarget);
	            response.sendRedirect(redirectTo);
	        } else {
	            response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        }
	    }
	}
%>
<!DOCTYPE html>
<html lang="<%= currentPage.getLanguage(false) %>">

	<cq:include script="head.jsp"/>
	
	<cq:include script="body.jsp"/>
	
	<cq:include script="foot.jsp"/>

</html>