<%--
  Try form component.
  - vishal.gupta@citrix.com
  - deepak.gupta@citrix.com
--%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="rightImg" value="<%= ContextRootTransformUtil.transformedPath(properties.get("rightFileReference",""),request) %>" />
<c:set var="leftImg" value="<%= ContextRootTransformUtil.transformedPath(properties.get("leftFileReference",""),request) %>" />
<c:set var="formCss" value="<%= properties.get("cssClass","") %>"/>
<c:set var="jsCode" value="<%= properties.get("jsCode","") %>"/>
<c:set var="region" value="<%= properties.get("currency","US") %>"/>
<c:set var="catalog" value="<%= properties.get("catalog","") %>"/>
<c:set var="planKey" value="<%= properties.get("planKey","") %>"/>
<c:set var="promoCode" value="<%= properties.get("defaultPromo","") %>"/>
<c:set var="language" value="<%= properties.get("flowLanguage","en") %>"/>
<c:set var="country" value="<%= properties.get("country","US") %>"/>
<c:set var="locale" value="<%= Utilities.getLocale(currentPage,request).toString() %>"/>
<c:set var="mailingOptIn" value="<%= properties.get("mailingOptin","setFalse") %>"/>
<c:set var="showPhoneField" value="<%= properties.get("showPhoneField",false) %>"/>
<c:set var="showIndustry" value="<%= properties.get("showIndustry",false) %>"/>
<c:set var="longForm" value="<%= properties.get("longForm",false) %>"/>
<c:set var="phoneFieldRequired" value="<%= properties.get("phoneFieldRequired",false) %>"/>
<c:set var="showPassword" value="<%= properties.get("showPassword",false) %>"/>
<c:set var="successURL" value="<%= properties.get("successURL","") %>"/>
<c:set var="submitBg" value="<%= properties.get("submitBg","") %>"/>
<c:set var="submitRadius" value="<%= properties.get("submitRadius","") %>"/>
<c:set var="submitWidth" value="<%= properties.get("submitWidth","") %>"/>
<c:set var="submitHeight" value="<%= properties.get("submitHeight","") %>"/>
<c:set var="submitTextStyle" value="<%= properties.get("submitTextStyle","") %>"/>
<c:set var="submitClass" value="<%= properties.get("submitStyle","") %>"/>
<c:set var="submitText" value="<%= properties.get("submitText","Submit") %>"/>
<c:set var="submitSubText" value="<%= properties.get("submitSubText","") %>"/>
<c:set var="submitAlign" value="<%= properties.get("submitAlign","") %>"/>
<c:set var="containerWidth" value="<%= properties.get("containerWidth","") %>"/>
<c:set var="errorDisplay" value="<%= properties.get("errorDisplay","prepend") %>"/>

<%-- Set variables for labels--%>
<c:set var="fname" value="<%= properties.get("firstName","form.placeholder.firstName") %>"/>
<c:set var="lname" value="<%= properties.get("lastName","form.placeholder.lastName") %>"/>
<c:set var="email" value="<%= properties.get("email","form.placeholder.email") %>"/>
<c:set var="emailLabel" value="<%= properties.get("emailLabel","") %>"/>
<c:set var="password" value="<%= properties.get("password","form.placeholder.password") %>"/>
<c:set var="passwordLabel" value="<%= properties.get("passwordLabel","") %>"/>
<c:set var="phone" value="<%= properties.get("phone","form.placeholder.phone") %>"/>
<c:set var="mailingListOptin" value="<%= properties.get("mailingListOptin","form.placeholder.mailingListOptin") %>"/>

<%-- Set variables for errors--%>
<c:set var="fnameError" value="<%= properties.get("firstNameError","form.validate.firstName") %>"/>
<c:set var="lnameError" value="<%= properties.get("lastNameError","form.validate.lastName") %>"/>
<c:set var="emailError" value="<%= properties.get("emailError","form.validate.email") %>"/>
<c:set var="passwordError" value="<%= properties.get("passwordError","form.validate.password") %>"/>
<c:set var="phoneError" value="<%= properties.get("phoneError","form.validate.phone") %>"/>

<c:set var="showOptIn" value="false"/>
<c:set var="optInValue" value="false"/>
<c:if test="${mailingOptIn == 'setTrue'}">
	<c:set var="optInValue" value="true"/>
</c:if>
<c:if test="${mailingOptIn == 'showUnchecked'}">
	<c:set var="showOptIn" value="true"/>
	<c:set var="optInValue" value="false"/>
</c:if>
<c:if test="${mailingOptIn == 'showChecked'}">
	<c:set var="showOptIn" value="true"/>
	<c:set var="optInValue" value="true"/>
</c:if>

<%-- Set server variable --%>
<c:choose>
	<c:when test="${isDev || isQA}">
			<c:set var="server" value="secureed1"/>
	</c:when>
	<c:when test="${isStage}">
		<c:set var="server" value="stage.secure"/>
	</c:when>
	<c:when test="${isProd}">
		<c:set var="server" value="secure"/>
	</c:when>
	<c:otherwise>
		<c:set var="server" value="secureed1"/>
	</c:otherwise>
</c:choose>

<c:if test="${not empty leftImg}">
	<div <c:if test="${not empty properties.leftImageMargin}">style="margin:${properties.leftImageMargin};"</c:if> class="leftImageContainer"><img src="${leftImg}"></div>
</c:if>
<c:if test="${not empty rightImg}">
	<div <c:if test="${not empty properties.rightImageMargin}">style="margin:${properties.rightImageMargin};"</c:if> class="rightImageContainer"><img src="${rightImg}"></div>
</c:if>

<div class="form-container  <c:if test="${longForm}">longForm</c:if> ${formCss}">
	<form id="tryForm" name="tryForm" action="https://${server}.citrixonline.com/commerce/buy" method="POST" <c:if test="${errorDisplay == 'prepend'}">class="error-prepend"</c:if> <c:if test="${errorDisplay == 'right'}">class="error-right"</c:if>>
		<div>
			<input type="hidden" name="region" value="${region}">
			<input type="hidden" name="catalog" value="${catalog}">
			<input type="hidden" name="planKey" value="${planKey}">
			<input type="hidden" name="promotion" value="${promoCode}">
			<input type="hidden" name="language" value="${language}">
			<input type="hidden" name="locale" value="${locale}">
			<input type="hidden" name="country" value="${country}">
			<c:if test="${not showOptIn}"><input type="hidden" name="mailingListOptIn" value="${optInValue}"></c:if>
			<input type="hidden" name="validpromo" value="">
			<input type="hidden" name="isNocc" value="">
			<input type="hidden" name="uuid" value="">
			<input type="hidden" name="ipAddress" value="">
			<input type="hidden" name="successUrl" value="${successURL}">
			<input type="hidden" name="submit-text" value="${submitText}">
		</div>
		<div class="form-head">
			<cq:include path="tryHead" resourceType="foundation/components/parsys" />
		</div>
		<c:choose>
			<c:when test="${not empty properties.maxSeatValue}">
				<select id="quantities" name="quantities">
					<option value="1">1 ${properties.singularSeat}</option>
					<c:forEach begin="2" end="${properties.maxSeatValue}" var="loop">
					    <option value="${loop}">${loop} ${properties.pluralSeat}</option>
					</c:forEach>
				</select>
				<cq:include path="trySubHead" resourceType="foundation/components/parsys" />
				<div class="clearBoth"></div>
			</c:when>
			<c:otherwise>
				<input type="hidden" name="quantities" value="${properties.defaultSeatValue}">
			</c:otherwise>
		</c:choose>
		<ul>
			<li>
				<div class="icon-person">
					<input id="firstName" name="firstName" type="text" class="required" title="<fmt:message key="${fnameError}"/>" placeholder="<fmt:message key="${fname}"/>">
				</div>
			</li>
			<li>
				<div class="icon-person">
					<input id="lastName" name="lastName" type="text" class="required" title="<fmt:message key="${lnameError}"/>" placeholder="<fmt:message key="${lname}"/>">
				</div>
			</li>
			<li>
				<c:if test="${errorDisplay == 'right'}"><label for="password"><fmt:message key="${emailLabel}"/></label></c:if>
				<div class="icon-mail">
					<input id="emailAddress" name="emailAddress" type="text" class="required email" title="<fmt:message key="${emailError}"/>" placeholder="<fmt:message key="${email}"/>">
				</div>
			</li>
			<c:if test="${showPassword}">
				<li id="formPassword">
					<c:if test="${errorDisplay == 'right'}"><label for="password"><fmt:message key="${passwordLabel}"/></label></c:if>
					<div class="icon-fontawesome-webfont-3">
						<input id="password" name="password" type="password" class="required password" title="<fmt:message key="${passwordError}"/>" placeholder="<fmt:message key="${password}"/>">
					</div>
				</li>
			</c:if>
			<c:if test="${showPhoneField}">
				<li>
					<div class="icon-phone-1">
						<input id="daytimePhone" name="daytimePhone" type="text" class="phone <c:if test="${phoneFieldRequired}">required</c:if>" title="<fmt:message key="${phoneError}"/>" placeholder="<fmt:message key="${phone}"/>">
					</div>
				</li>
			</c:if>
			<c:if test="${showIndustry}">
			  <div class="selectbox">
				<li>
 					   <%@include file="/apps/citrixosd/components/content/customForms/selectbox/selectbox.jsp"%>
				</li>
			</div>
			</c:if>
		</ul>
		<c:if test="${showOptIn}">
		<div class="checkbox">
			<input type="checkbox" name="mailingListOptIn" value="true" <c:if test="${optInValue}">checked</c:if>><span><fmt:message key="${mailingListOptin}"/></span>
		</div>
		</c:if>
		<div class="form-submit ${submitAlign}" <c:if test="${not empty containerWidth}">style="width: ${containerWidth}"</c:if>>
			<input type="submit" id="formSubmit" value="${submitText}" class="button ${submitBg} ${submitRadius} ${submitWidth} ${submitHeight} ${submitTextStyle} ${submitClass}">
			<c:if test="${not empty submitSubText}"><label for="formSubmit" class="button-text ${submitHeight}"><fmt:message key="${submitSubText}"/></label></c:if>
		</div>
		<div class="throbber-loader"></div>
	</form>
</div>
<div class="clearBoth"></div>
<script type="text/javascript" src="https://${server}.citrixonline.com/secure/scripts/commerce/restClientAPI.js"></script>
<script type="text/javascript">
	var commServer = "https://${server}.citrixonline.com";
<c:choose>
	<c:when test="${isProd}">
	var merchantId = "g2aexpress";
	var orgId = "k8vif92e";
	</c:when>
	<c:otherwise>
	var merchantId = "citrixonline_test";
	var orgId = "1snn5n9w";
	</c:otherwise>
</c:choose>
	${jsCode}
</script>