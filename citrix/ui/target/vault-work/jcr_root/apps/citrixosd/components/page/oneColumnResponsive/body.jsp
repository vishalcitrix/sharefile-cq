<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="com.citrixosd.motionpoint.MotionpointUtils"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>
<%!
    public static final String MOTIONPOINT_EASYLINK_PROPERTY = "easyLinkSiteEnabled";
%>
<%-- Add optional product theme --%>
<c:set var="productTheme" value="<%= SiteUtils.getProductTheme(currentPage) %>"/>
<c:if test="${not empty productTheme}">
	<c:set var="productTheme" value=' class="${productTheme}"'/>
</c:if>
<body${productTheme}>
	<c:set var="clientContext" value="<%= SiteUtils.getPropertyFromSiteRoot("clientContext",currentPage) %>"/>
	<c:if test="${empty clientContext}">
		<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
	</c:if>
	<div id="content-body">
		<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
		<cq:include script="noscript.jsp"/>
		<cq:include script="ieBrowserSupport.jsp"/>
		<cq:include path="mainContent" resourceType="foundation/components/parsys"/>
	</div>
	<cq:include script="footer.jsp" />
	<c:set var="easyLinkEnabled" value="<%= MotionpointUtils.isEasyLinkEnabled(currentPage) %>" />
    <c:if test="${easyLinkEnabled}">
       <cq:include script="motionpointBody.jsp"/>
    </c:if>
</body>
