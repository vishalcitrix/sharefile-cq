<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>

<cq:include script="noscript.jsp"/>

<cq:include path="navigation" resourceType="swx/component-library/components/content/single-ipar"/>

<cq:include path="breadcrumbs" resourceType="swx/component-library/components/content/single-ipar"/>