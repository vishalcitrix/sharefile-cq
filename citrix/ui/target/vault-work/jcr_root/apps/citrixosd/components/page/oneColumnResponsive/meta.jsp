<%@page import="java.util.ArrayList,
				java.util.ListIterator,
				javax.jcr.Node,javax.jcr.NodeIterator,
				com.day.cq.commons.inherit.InheritanceValueMap" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%!
	public ArrayList<Node> getParentMeta(Page page) {
	    ArrayList<Node> list = new ArrayList();
	    try {
	        if(page == null) {
	            return null;
	        } else {
	            final Resource contentResource = page.getContentResource();
	            if (contentResource != null) {
	                final Node contentNode = page.getContentResource().adaptTo(Node.class);
	                if (contentNode.hasNode("meta")) {
	                    Node metaNode = contentNode.getNode("meta");
	                    NodeIterator iter = metaNode.getNodes();
	                    while(iter.hasNext()){
	                        Node tmpMetaNode = iter.nextNode();
	                        if(tmpMetaNode.hasProperty("showMetaOnSubpages") &&
	                           tmpMetaNode.getProperty("showMetaOnSubpages").getString().equals("true")
	                          ) {
	                            list.add(tmpMetaNode);
	                        }
	                    }
	                    return list;
	                }
	            }
	            return getParentMeta(page.getParent());
	        }
	    } catch (RepositoryException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
%>
<%
	String language = "";
	final Page localePage = currentPage.getAbsoluteParent(2);
	if(localePage != null) {
		language = localePage.getName();
	}
%>
<meta http-equiv="content-language" content="<%=language %>">
<%
	if(currentNode.hasNode("metaHttp")) {
		final Node metaHttpNode = currentNode.getNode("metaHttp");
		NodeIterator iter = metaHttpNode.getNodes();
		while(iter.hasNext()) {
			Node metaItemNode = iter.nextNode();
				if (metaItemNode.hasProperty("http") && metaItemNode.hasProperty("content"))
				%><meta http-equiv="<%=metaItemNode.getProperty("http").getString() %>" content="<%=metaItemNode.getProperty("content").getString() %>">
<%
		}
	}
	if (currentNode.hasNode("meta")) {
		final Node metaNode = currentNode.getNode("meta");
		NodeIterator iter1 = metaNode.getNodes();
		while (iter1.hasNext()) {
			Node metaItemNode = iter1.nextNode();
				if (metaItemNode.hasProperty("metaName") && metaItemNode.hasProperty("metaContent"))
				%><meta name="<%=metaItemNode.getProperty("metaName").getString() %>" content="<%=metaItemNode.getProperty("metaContent").getString() %>">
<%
		}
	}
    
    ArrayList<Node> siteMeta = getParentMeta(currentPage.getParent());
    if(siteMeta != null) {
        ListIterator<Node> iter3 = siteMeta.listIterator();
        while(iter3.hasNext()) {
            Node metaItemNode = iter3.next();
            if(metaItemNode.hasProperty("metaName") && metaItemNode.hasProperty("metaContent")) {
                %><meta name="<%=metaItemNode.getProperty("metaName").getString() %>" content="<%=metaItemNode.getProperty("metaContent").getString() %>"><%
            }
        }
    }
    
	if(currentNode.hasNode("link")) {
		final Node linkNode = currentNode.getNode("link");
		NodeIterator iter2 = linkNode.getNodes();
		while (iter2.hasNext()) {
			Node linkItemNode = iter2.nextNode();
			if(linkItemNode.hasProperty("linkRel") && linkItemNode.hasProperty("linkHref"))
			%>	<link rel="<%=linkItemNode.getProperty("linkRel").getString()%>" href="<%=linkItemNode.getProperty("linkHref").getString()%>"<c:if test="<%= linkItemNode.hasProperty("linkType") %>"> type="<%=linkItemNode.getProperty("linkType").getString()%>"</c:if><c:if test="<%= linkItemNode.hasProperty("linkHrefLang") %>"> hreflang="<%=linkItemNode.getProperty("linkHrefLang").getString()%>"</c:if><c:if test="<%= linkItemNode.hasProperty("linkMedia") %>"> media="<%=linkItemNode.getProperty("linkMedia").getString()%>"</c:if> ><%
		}
	}
%>