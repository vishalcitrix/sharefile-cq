<%--

  Textfield component.
  Textfield for Custom Form
  
  vishal.gupta.82@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>

<%
    // TODO add you code here
    final String fieldId = properties.get("fieldId","");   
    final String fieldLabel = properties.get("fieldLabel","");   
    final String fieldName = properties.get("fieldName","");
    final boolean isRequired = properties.get("isRequired",false);
    final String constraint = properties.get("constraintType","None");
    final String fieldType = properties.get("tfType","text");
    final String placeHolder = properties.get("placeHolder","");

    if (editContext != null && editContext.getEditConfig() != null) {
        if(fieldName == ""){
            editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Textbox Field - (Constraint:" + constraint + ") - (Please select field name)"));
        }else{
            editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Textbox Field - (Constraint:" + constraint + ") - (name: " + fieldName + ")"));
        }

        editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
    }

%>
    <c:set var="constraint" value="<%= constraint %>" />
    <c:set var="fieldLabel" value="<%= fieldLabel %>" />
    <c:set var="isRequired" value="<%= isRequired %>" />
    <c:set var="placeHolder" value="<%= placeHolder %>" />
    <c:set var="fieldId" value="<%= fieldId %>" />
    <c:set var="fieldName" value="<%= fieldName %>" />
    
    <c:if test="${fieldLabel ne ''}">
        <label for="${fieldName}">${fieldLabel}</label>
    </c:if>
    
	<c:choose>
       <c:when test="${placeHolder == ''}">
       	<c:choose>
	        <c:when test="${fieldId ne ''}">
	        	<c:choose>
			        <c:when test="${isRequired}">
			            <input type="<%= fieldType %>" class="required" id="<%= fieldId %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" <c:if test="${not empty param[fieldName]}">value="${param[fieldName]}"</c:if>/>
			        </c:when>
			        <c:otherwise>
			           	<input type="<%= fieldType %>" id="<%= fieldId %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" <c:if test="${not empty param[fieldName]}">value="${param[fieldName]}"</c:if>/>
			        </c:otherwise>  
			    </c:choose>
	        </c:when>
	        <c:otherwise>
	        	<c:choose>
			        <c:when test="${isRequired}">
			            <input type="<%= fieldType %>" class="required" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" <c:if test="${not empty param[fieldName]}">value="${param[fieldName]}"</c:if>/>
			        </c:when>
			        <c:otherwise>
			           	<input type="<%= fieldType %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" <c:if test="${not empty param[fieldName]}">value="${param[fieldName]}"</c:if>/>
			        </c:otherwise>  
			    </c:choose>
	        </c:otherwise>  
	    </c:choose>
       </c:when>
       <c:otherwise>
       	<c:choose>
	        <c:when test="${fieldId ne ''}">
	        	<c:choose>
			        <c:when test="${isRequired}">
			            <input type="<%= fieldType %>" class="required" id="<%= fieldId %>" placeholder="<%= placeHolder %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" />
			        </c:when>
			        <c:otherwise>
			           	<input type="<%= fieldType %>" id="<%= fieldId %>" placeholder="<%= placeHolder %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" />
			        </c:otherwise>  
			    </c:choose>
	        </c:when>
	        <c:otherwise>
	        	<c:choose>
			        <c:when test="${required}">
			        	<input type="<%= fieldType %>" class="required prefill" placeholder="<%= placeHolder %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" />
			        </c:when>
			        <c:otherwise>
			        	<input type="<%= fieldType %>" class="prefill" placeholder="<%= placeHolder %>" name="<%= fieldName %>" constraint="<%= constraint %>" trackerror="true" />
			        </c:otherwise>  
			    </c:choose>
	           	
	        </c:otherwise>  
	    </c:choose>
       </c:otherwise>  
   </c:choose>
