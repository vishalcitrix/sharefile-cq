<%--

  Commerce component.
  This component used to get the commerce system data for a user.
  Once initialized, will get the email id request parameter.
  This parameter will be used by the javascript for making an ajax call to 
  the service.
  upon getting the data, all the user trailts will be initialized.
  USAGE : By default this component is added to exit pop up message template (in G2M Project)
  for exit pop up message project.

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.citrix.marketing.cq.commerce.models.Plan,
                com.citrix.marketing.cq.commerce.service.CommerceService,
                com.citrix.marketing.cq.commerce.utils.CommerceAPIUtils,
                javax.xml.bind.DatatypeConverter,
                java.util.Date,java.util.Calendar "
%>

<%
    String userkey = request.getParameter("key");
    String productKey = request.getParameter("prd");
    if(userkey != null){
        CommerceService commerceService = sling.getService(CommerceService.class);
        Plan plan = CommerceAPIUtils.getUserPlan(commerceService , userkey, productKey);
        if(plan != null){
        Date expDate = DatatypeConverter.parseDateTime(plan.getEndDate()).getTime();
        Calendar now = Calendar.getInstance();
        long currentTime = now.getTime().getTime();
        long endTime =   expDate.getTime();
        long diffTime = endTime - currentTime ;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);
        pageContext.setAttribute("daysleft", diffDays);
        pageContext.setAttribute("plan", plan);
        }
    }
%>

<c:if test="${not empty plan}">
    <script>
		var isTrial = "${plan.trial}";
        var nocc = "${plan.nocc}";
        var autoRenew = "${plan.autoRenew}";
        var active= "${plan.active}";
        var billingPeriod= "${plan.billingPeriod}";
        var product= "${plan.product}";
        var seats= "${plan.seats}";
        var name= "${plan.name}";
        var daysleft  = "${daysleft}";
    </script>
</c:if>

<c:if test="${empty plan}">
    <script>
        var isGuest = "true";
    </script>
</c:if>

<ul class="share-list">
    <c:if test="${properties.fbenable eq 'true'}">
        <li>
            <a href="${properties.fburl}&amp;p[title]=${properties.fbtitle}&amp;p[url]=${properties.fbshareurl}&amp;p[summary]=${properties.fbsummary}" rel="popup" class="facebook"></a>
        </li>
    </c:if>
    <c:if test="${properties.twitterenable eq 'true'}">
        <li>
            <a href="${properties.twitterurl}?&amp;text=${properties.tweet}" rel="popup" class="twitter"></a>
        </li>
    </c:if>
    <c:if test="${properties.gpenable eq 'true'}">
        <li>
            <a href="${properties.googleurl}?${properties.googlesharelink}" rel="popup" class="googlePlus"></a>
        </li>
    </c:if>
</ul>
<ul class="bottom-links">
   <cq:include path="links" resourceType="citrixosd/components/content/linkSet" />
</ul>
