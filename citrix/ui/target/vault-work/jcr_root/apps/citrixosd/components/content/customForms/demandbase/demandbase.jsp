<%--

  Demandbase component.
  Demandbase for Custom Form
  
  vishal.gupta.82@citrix.com

--%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<head>
    <script type="text/javascript" src="http://autocomplete.demandbase.com/autocomplete/v1.7/widget.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/demandbase.js"></script>
    <script type="text/javascript" src="http://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358&amp;callback=DemandGen.DemandBase.processIPInfo"></script>
</head>

<%
	if (editContext != null && editContext.getEditConfig() != null) {
		editContext.getEditConfig().getToolbar().add(0, new Toolbar.Label("Demandbase"));  
		editContext.getEditConfig().getToolbar().add(1, new Toolbar.Separator());
	}
%>
    <label for="Company"><fmt:message key="form.label.company"/></label>
        <input id="Company" name="Company" type="text" class="required long-text">
        
        <ul>
            <li class="left-col">
                <label for="Industry"><fmt:message key="form.label.industry"/></label>
                <select id="Industry" name="Industry" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="Accounting"><fmt:message key="form.option.industry.accounting"/></option>
                    <option value="Advertising/Marketing/PR"><fmt:message key="form.option.industry.advertisingMarketingPR"/></option>
                    <option value="Aerospace &amp; Defense"><fmt:message key="form.option.industry.aerospaceAndDefense"/></option>
                    <option value="Call Center Outsourcing"><fmt:message key="form.option.industry.callCenterOutsourcing"/></option>
                    <option value="Consulting"><fmt:message key="form.option.industry.consulting"/></option>
                    <option value="Education"><fmt:message key="form.option.industry.education"/></option>
                    <option value="Energy, Chemical, Utilities"><fmt:message key="form.option.industry.energyChemicalUtilities"/></option>
                    <option value="Financial Services"><fmt:message key="form.option.industry.financialService"/></option>
                    <option value="Government - Federal"><fmt:message key="form.option.industry.governmentFederal"/></option>
                    <option value="Government - State &amp; Local"><fmt:message key="form.option.industry.governmentStateAndLocal"/></option>
                    <option value="Healthcare"><fmt:message key="form.option.industry.healthcare"/></option>
                    <option value="High Tech - Hardware"><fmt:message key="form.option.industry.highTechHardware"/></option>
                    <option value="High Tech - ISP"><fmt:message key="form.option.industry.highTechISP"/></option>
                    <option value="High Tech - Software"><fmt:message key="form.option.industry.highTechSoftware"/></option>
                    <option value="High Tech - Other"><fmt:message key="form.option.industry.highTechOther"/></option>
                    <option value="Hospitality/Travel/Tourism"><fmt:message key="form.option.industry.hospitalityTravelTourism"/></option>
                    <option value="Insurance"><fmt:message key="form.option.industry.insurance"/></option>
                    <option value="Legal"><fmt:message key="form.option.industry.legal"/></option>
                    <option value="Manufacturing"><fmt:message key="form.option.industry.manufacturing"/></option>
                    <option value="Pharmaceuticals &amp; Biotechnology"><fmt:message key="form.option.industry.pharmaceuticalsAndBiotechnology"/></option>
                    <option value="Real Estate"><fmt:message key="form.option.industry.realEstate"/></option>
                    <option value="Retail"><fmt:message key="form.option.industry.retail"/></option>
                    <option value="Support Outsourcing"><fmt:message key="form.option.industry.supportOutsourcing"/></option>
                    <option value="Telecommunications"><fmt:message key="form.option.industry.telecommunications"/></option>
                    <option value="Transportation"><fmt:message key="form.option.industry.transportation"/></option>
                    <option value="VAR/Systems Integrator"><fmt:message key="form.option.industry.VARSystemIntegrator"/></option>
                    <option value="Other"><fmt:message key="form.option.industry.other"/></option>
                </select>
            </li>
            <li class="right-col">
                <label for="NumberofEmployees"><fmt:message key="form.label.numberOfEmployees"/></label>
                <select name="NumberofEmployees" id="NumberofEmployees" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="1-10"><fmt:message key="form.option.numberOfEmployees.10"/></option>
                    <option value="11-20"><fmt:message key="form.option.numberOfEmployees.20"/></option>
                    <option value="21-50"><fmt:message key="form.option.numberOfEmployees.50"/></option>
                    <option value="51-100"><fmt:message key="form.option.numberOfEmployees.100"/></option>
                    <option value="101-250"><fmt:message key="form.option.numberOfEmployees.250"/></option>
                    <option value="251-500"><fmt:message key="form.option.numberOfEmployees.500"/></option>
                    <option value="501-1,000"><fmt:message key="form.option.numberOfEmployees.1000"/></option>
                    <option value="1,001-2,500"><fmt:message key="form.option.numberOfEmployees.2500"/></option>
                    <option value="2,501-5,000"><fmt:message key="form.option.numberOfEmployees.5000"/></option>
                    <option value="5,001-7,500"><fmt:message key="form.option.numberOfEmployees.7500"/></option>
                    <option value="7,501-10,000"><fmt:message key="form.option.numberOfEmployees.10000"/></option>
                    <option value="10,001+"><fmt:message key="form.option.numberOfEmployees.10001"/></option>
                </select>
            </li>
            <li class="left-col">
                <label for="Department"><fmt:message key="form.label.department"/></label>
                <select name="Department" id="Department" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="Consulting"><fmt:message key="form.option.department.consulting"/></option>
                    <option value="Customer Service"><fmt:message key="form.option.department.customerService"/></option>
                    <option value="Engineering"><fmt:message key="form.option.department.engineering"/></option>
                    <option value="Finance/Legal/Administration"><fmt:message key="form.option.department.financialLegalAdministration"/></option>
                    <option value="Human Resources"><fmt:message key="form.option.department.humanResource"/></option>
                    <option value="IT"><fmt:message key="form.option.department.it"/></option>
                    <option value="Marketing/PR"><fmt:message key="form.option.department.marketingPR"/></option>
                    <option value="Operations"><fmt:message key="form.option.department.operations"/></option>
                    <option value="Professional Services"><fmt:message key="form.option.department.professionalService"/></option>
                    <option value="Sales/Business Development"><fmt:message key="form.option.department.salesBusinessDevelopment"/></option>
                    <option value="Training"><fmt:message key="form.option.department.training"/></option>
                    <option value="Other"><fmt:message key="form.option.department.other"/></option>
                </select>
            </li>
            <li class="right-col">
                <label for="Country"><fmt:message key="form.label.country"/></label>
                <select id="Country" name="Country" class="required">
                    <cq:include script="countryList.jsp"/>
                </select>
            </li>
        </ul>
        <div class="clearBoth"></div>