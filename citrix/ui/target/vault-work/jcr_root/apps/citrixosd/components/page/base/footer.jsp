<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%@page import="java.util.Locale"%>

<%! public static final String HIDE_SITEMAP_PROPERTY = "hideSitemap"; %>
<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>

<c:if test="<%= !properties.get(HIDE_SITEMAP_PROPERTY, false) %>">
    <cq:include path="sitemap" resourceType="swx/component-library/components/content/single-ipar"/>
</c:if>

<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>

<cq:include script="lightbox.jsp"/>

<c:if test="${not empty stickyChatButtonId}"><div id="${stickyChatButtonId}"></div></c:if>

<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>

    <%-- DNT --%>
    <%
        Locale locale = null;
        final Page localePage = currentPage.getAbsoluteParent(2);
        if (localePage != null) {
            try {
                locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
            } catch (Exception e) {
                locale = request.getLocale();
            }
        }
        else
            locale = request.getLocale();
    %>
    <script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
    <script type="text/javascript">
        dnt.setMsgClass('footer-dnt');
        dnt.setMsgLocId('footer-dnt');
        dnt.setLocale('<%=locale.toString()%>');
        dnt.dntInit();
    </script>

    <cq:include script="channelTracking.jsp"/>

<c:if test="${jquerytoolsOption == 'enableFooter'}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.tools.min.js"></script></c:if>
<c:if test="${jqstationarytipOption == 'enableFooter'}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.stationaryTip.js"></script></c:if>
<c:if test="${jqdotdotdotOption == 'enableFooter'}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.dotdotdot-1.5.9.min.js"></script></c:if>
<c:if test="${cqpersonalizationjsOption == 'enableFooter'}"><cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/></c:if>

<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>