<%@include file="/libs/foundation/global.jsp" %>

<body>
    <div class="content-area">
        <cq:include path="contentBody" resourceType="foundation/components/parsys"/>
        <cq:include path="epfooter" resourceType="citrixosd/components/content/commerce-integration/exit-popup"/>
    </div>
    <cq:include script="footer.jsp" />
</body>
