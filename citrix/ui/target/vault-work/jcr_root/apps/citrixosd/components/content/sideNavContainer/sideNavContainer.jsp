<%--
	Side Nav Container
	
	This is a container used to for a collection of side navigation and has the 
	ability to override or hold information which can be used for the side navigation 
	component.
	
	Manual Selection: Selected path will be used in the side navigation component to 
	manually select that specific path.

	achew@siteworx.com
--%>

<%!
	public static final String MANUAL_SELECTION_PROPERTY = "manualSelection";
	public static final String NUMBER_OF_SIDE_NAVIGATION_PROPERTY = "numberOfSideNav";
%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
	request.setAttribute("sideNavContainerManualSelection", properties.get(MANUAL_SELECTION_PROPERTY, null));
%>

<c:forEach begin="1" end="${properties.numberOfSideNav}" varStatus="i">
	<cq:include path="sideNav_${i.index}" resourceType="citrixosd/components/content/sideNav"/>
</c:forEach>