<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="opt-in">
    <div class="checkbox"><input type="checkbox" name="email_opt_in" value="true" checked></div>
    <cq:include path="optInMsg" resourceType="citrixosd/components/content/text" />
</div>