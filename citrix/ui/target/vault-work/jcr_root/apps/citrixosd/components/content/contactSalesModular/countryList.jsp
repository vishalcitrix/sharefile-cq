<%--
  Creates a country list with translations
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:setContentBundle />

<%! 
    public class Country {
        private String value;
        private String name;
        private String title;
        
        public Country(String value,String name,String title){
            this.value = value;
            this.name = name;
            this.title = title;
        }
    
        public String getValue() {
            return this.value;
        }
        public String getName() {
            return this.name;
        }
        public String getTitle() {
            return this.title;
        }
    }
%>

<% 
    //using Utilities to parse structured multi field
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("selectCountry")) {
        final Node baseNode = currentNode.getNode("selectCountry");
        values = Utilities.parseStructuredMultifield(baseNode);
    }

    ArrayList<Country> countries = new ArrayList<Country>();
    countries.add(new Country("Albania","form.country.al","EMEA"));
    countries.add(new Country("Algeria","form.country.dz","EMEA"));
    countries.add(new Country("American Samoa","form.country.as",""));
    countries.add(new Country("Andorra","form.country.ad",""));
    countries.add(new Country("Angola","form.country.ao","EMEA"));
    countries.add(new Country("Anguilla","form.country.ai",""));
    countries.add(new Country("Antarctica","form.country.aq",""));
    countries.add(new Country("Antigua/Barbuda","form.country.ag",""));
    countries.add(new Country("Argentina","form.country.ar",""));
    countries.add(new Country("Armenia","form.country.am",""));
    countries.add(new Country("Aruba","form.country.aw",""));
    countries.add(new Country("Australia","form.country.au","APAC"));
    countries.add(new Country("Austria","form.country.at","EMEA"));
    countries.add(new Country("Azerbaijan","form.country.az","EMEA"));
    countries.add(new Country("Bahamas","form.country.bs",""));
    countries.add(new Country("Bahrain","form.country.bh","EMEA"));
    countries.add(new Country("Bangladesh","form.country.bd","APAC"));
    countries.add(new Country("Barbados","form.country.bb",""));
    countries.add(new Country("Belarus","form.country.by",""));
    countries.add(new Country("Belgium","form.country.be","EMEA"));
    countries.add(new Country("Belize","form.country.bz",""));
    countries.add(new Country("Benin","form.country.bj",""));
    countries.add(new Country("Bermuda","form.country.bm",""));
    countries.add(new Country("Bhutan","form.country.bt","APAC"));
    countries.add(new Country("Bolivia","form.country.bo",""));
    countries.add(new Country("Bosnia-Herz.","form.country.ba",""));
    countries.add(new Country("Botswana","form.country.bw",""));
    countries.add(new Country("Bouvet Island","form.country.bv",""));
    countries.add(new Country("Brazil","form.country.br",""));
    countries.add(new Country("Brunei Darussalam","form.country.bn","APAC"));
    countries.add(new Country("Bulgaria","form.country.bg",""));
    countries.add(new Country("Burkina-Faso","form.country.bf",""));
    countries.add(new Country("Brunei Dar-es-S","form.country.bi",""));
    countries.add(new Country("Cambodia","form.country.kh","APAC"));
    countries.add(new Country("Cameroon","form.country.cm",""));
    countries.add(new Country("Canada","form.country.ca","NA"));
    countries.add(new Country("Cape Verde","form.country.cv",""));
    countries.add(new Country("Cayman Islands","form.country.ky",""));
    countries.add(new Country("Chad","form.country.td",""));
    countries.add(new Country("Chile","form.country.cl",""));
    countries.add(new Country("China","form.country.cn","APAC"));
    countries.add(new Country("Christmas Island","form.country.cx",""));
    countries.add(new Country("Colombia","form.country.co",""));
    countries.add(new Country("Comoros","form.country.km",""));
    countries.add(new Country("Congo","form.country.cg",""));
    countries.add(new Country("Cook Islands","form.country.ck",""));
    countries.add(new Country("Costa Rica","form.country.cr",""));
    countries.add(new Country("Cote d'Ivoire","form.country.ci",""));
    countries.add(new Country("Croatia","form.country.hr","EMEA"));
    countries.add(new Country("Cyprus","form.country.cy",""));
    countries.add(new Country("Czech Republic","form.country.cz","EMEA"));
    countries.add(new Country("Denmark","form.country.dk","EMEA"));
    countries.add(new Country("Djibouti","form.country.dj",""));
    countries.add(new Country("Dominica","form.country.dm",""));
    countries.add(new Country("Dominican Rep.","form.country.do",""));
    countries.add(new Country("East Timor","form.country.tp","APAC"));
    countries.add(new Country("Ecuador","form.country.ec",""));
    countries.add(new Country("Egypt","form.country.eg","EMEA"));
    countries.add(new Country("El Salvador","form.country.sv",""));
    countries.add(new Country("Equatorial Gui.","form.country.gq",""));
    countries.add(new Country("Eritrea","form.country.er",""));
    countries.add(new Country("Estonia","form.country.ee",""));
    countries.add(new Country("Ethiopia","form.country.et","EMEA"));
    countries.add(new Country("Falkland Islands","form.country.fk",""));
    countries.add(new Country("Faroe Islands","form.country.fo",""));
    countries.add(new Country("Fiji","form.country.fj","APAC"));
    countries.add(new Country("Finland","form.country.fi","EMEA"));
    countries.add(new Country("France","form.country.fr","EMEA"));
    countries.add(new Country("French Guayana","form.country.gf",""));
    countries.add(new Country("Frenc.Polynesia","form.country.pf",""));
    countries.add(new Country("Gabon","form.country.ga",""));
    countries.add(new Country("Gambia","form.country.gm",""));
    countries.add(new Country("Georgia","form.country.ge",""));
    countries.add(new Country("Germany","form.country.de","EMEA"));
    countries.add(new Country("Ghana","form.country.gh","EMEA"));
    countries.add(new Country("Gibraltar","form.country.gi",""));
    countries.add(new Country("Greece","form.country.gr","EMEA"));
    countries.add(new Country("Greenland","form.country.gl",""));
    countries.add(new Country("Grenada","form.country.gd",""));
    countries.add(new Country("Guadeloupe","form.country.gp",""));
    countries.add(new Country("Guatemala","form.country.gt",""));
    countries.add(new Country("Guinea","form.country.gn",""));
    countries.add(new Country("Guinea-Bissau","form.country.gw",""));
    countries.add(new Country("Guyana","form.country.gy",""));
    countries.add(new Country("Haiti","form.country.ht",""));
    countries.add(new Country("Honduras","form.country.hn",""));
    countries.add(new Country("Hong Kong","form.country.hk","APAC"));
    countries.add(new Country("Hungary","form.country.hu","EMEA"));
    countries.add(new Country("Iceland","form.country.is","EMEA"));
    countries.add(new Country("India","form.country.in","APAC"));
    countries.add(new Country("Indonesia","form.country.id","APAC"));
    countries.add(new Country("Ireland","form.country.ie","EMEA"));
    countries.add(new Country("Israel","form.country.il","EMEA"));
    countries.add(new Country("Italy","form.country.it","EMEA"));
    countries.add(new Country("Jamaica","form.country.jm",""));
    countries.add(new Country("Japan","form.country.jp","APAC"));
    countries.add(new Country("Jordan","form.country.jo","EMEA"));
    countries.add(new Country("Kazakhstan","form.country.kz",""));
    countries.add(new Country("Kenya","form.country.ke","EMEA"));
    countries.add(new Country("Kiribati","form.country.ki","APAC"));
    countries.add(new Country("South Korea","form.country.kp","APAC"));
    countries.add(new Country("Kuwait","form.country.kw","EMEA"));
    countries.add(new Country("Kyrgyzstan","form.country.kg",""));
    countries.add(new Country("Laos","form.country.la","APAC"));
    countries.add(new Country("Latvia","form.country.lv",""));
    countries.add(new Country("Lebanon","form.country.lb",""));
    countries.add(new Country("Lesotho","form.country.ls",""));
    countries.add(new Country("Liberia","form.country.lr",""));
    countries.add(new Country("Liechtenstein","form.country.li",""));
    countries.add(new Country("Lithuania","form.country.lt",""));
    countries.add(new Country("Luxembourg","form.country.lu","EMEA"));
    countries.add(new Country("Macau","form.country.mo","APAC"));
    countries.add(new Country("Macedonia","form.country.mk",""));
    countries.add(new Country("Madagascar","form.country.mg",""));
    countries.add(new Country("Malawi","form.country.mw",""));
    countries.add(new Country("Malaysia","form.country.my","APAC"));
    countries.add(new Country("Maldives","form.country.mv","APAC"));
    countries.add(new Country("Mali","form.country.ml",""));
    countries.add(new Country("Malta","form.country.mt","EMEA"));
    countries.add(new Country("Marshall Islands","form.country.mh","APAC"));
    countries.add(new Country("Martinique","form.country.mq",""));
    countries.add(new Country("Mauritania","form.country.mr",""));
    countries.add(new Country("Mauritius","form.country.mu",""));
    countries.add(new Country("Mayotte","form.country.yt",""));
    countries.add(new Country("Mexico","form.country.mx",""));
    countries.add(new Country("Micronesia","form.country.fm","APAC"));
    countries.add(new Country("Moldavia","form.country.md",""));
    countries.add(new Country("Monaco","form.country.mc",""));
    countries.add(new Country("Mongolia","form.country.mn","APAC"));
    countries.add(new Country("Montserrat","form.country.ms",""));
    countries.add(new Country("Morocco","form.country.ma","EMEA"));
    countries.add(new Country("Mozambique","form.country.mz",""));
    countries.add(new Country("Myanmar","form.country.mm","APAC"));
    countries.add(new Country("Namibia","form.country.na",""));
    countries.add(new Country("Nauru","form.country.nr","APAC"));
    countries.add(new Country("Nepal","form.country.np","APAC"));
    countries.add(new Country("Netherlands","form.country.nl","EMEA"));
    countries.add(new Country("Netherlands Antilles","form.country.an",""));
    countries.add(new Country("New Caledonia","form.country.nc",""));
    countries.add(new Country("New Zealand","form.country.nz","APAC"));
    countries.add(new Country("Nicaragua","form.country.ni",""));
    countries.add(new Country("Niger","form.country.ne",""));
    countries.add(new Country("Nigeria","form.country.ng","EMEA"));
    countries.add(new Country("Niue","form.country.nu",""));
    countries.add(new Country("Norfolk Island","form.country.nf",""));
    countries.add(new Country("Norway","form.country.no","EMEA"));
    countries.add(new Country("Oman","form.country.om","EMEA"));
    countries.add(new Country("Pakistan","form.country.pk","APAC"));
    countries.add(new Country("Palau","form.country.pw","APAC"));
    countries.add(new Country("Panama","form.country.pa",""));
    countries.add(new Country("Papua Nw Guinea","form.country.pg","APAC"));
    countries.add(new Country("Paraguay","form.country.py",""));
    countries.add(new Country("Peru","form.country.pe",""));
    countries.add(new Country("Philippines","form.country.ph","APAC"));
    countries.add(new Country("Pitcairn","form.country.pn",""));
    countries.add(new Country("Poland","form.country.pl","EMEA"));
    countries.add(new Country("Portugal","form.country.pt","EMEA"));
    countries.add(new Country("Qatar","form.country.qa",""));
    countries.add(new Country("Reunion","form.country.re",""));
    countries.add(new Country("Romania","form.country.ro","EMEA"));
    countries.add(new Country("Russian Fed.","form.country.ru","EMEA"));
    countries.add(new Country("Rwanda","form.country.rw",""));
    countries.add(new Country("St Kitts&Nevis","form.country.kn",""));
    countries.add(new Country("Saint Lucia","form.country.lc",""));
    countries.add(new Country("Western Samoa","form.country.ws","APAC"));
    countries.add(new Country("S.Tome,Principe","form.country.st",""));
    countries.add(new Country("Saudi Arabia","form.country.sa","EMEA"));
    countries.add(new Country("Senegal","form.country.sn",""));
    countries.add(new Country("Seychelles","form.country.sc",""));
    countries.add(new Country("Sierra Leone","form.country.sl",""));
    countries.add(new Country("Singapore","form.country.sg","APAC"));
    countries.add(new Country("Slovakia","form.country.sk","EMEA"));
    countries.add(new Country("Slovenia","form.country.si","EMEA"));
    countries.add(new Country("Solomon Islands","form.country.sb","APAC"));
    countries.add(new Country("Somalia","form.country.so",""));
    countries.add(new Country("South Africa","form.country.za","EMEA"));
    countries.add(new Country("Spain","form.country.es","EMEA"));
    countries.add(new Country("Sri Lanka","form.country.lk","APAC"));
    countries.add(new Country("St. Helena","form.country.sh",""));
    countries.add(new Country("Suriname","form.country.sr",""));
    countries.add(new Country("Swaziland","form.country.sz",""));
    countries.add(new Country("Sweden","form.country.se","EMEA"));
    countries.add(new Country("Switzerland","form.country.ch","EMEA"));
    countries.add(new Country("Taiwan","form.country.tw","APAC"));
    countries.add(new Country("Tajikistan","form.country.tj",""));
    countries.add(new Country("Tanzania","form.country.tz","EMEA"));
    countries.add(new Country("Thailand","form.country.th","APAC"));
    countries.add(new Country("Togo","form.country.tg",""));
    countries.add(new Country("Tokelau","form.country.tk",""));
    countries.add(new Country("Tonga","form.country.to","APAC"));
    countries.add(new Country("Trinidad,Tobago","form.country.tt",""));
    countries.add(new Country("Tunisia","form.country.tn",""));
    countries.add(new Country("Turkey","form.country.tr","EMEA"));
    countries.add(new Country("Turkmenistan","form.country.tm",""));
    countries.add(new Country("Tuvalu","form.country.tv","APAC"));
    countries.add(new Country("Uganda","form.country.ug","EMEA"));
    countries.add(new Country("Ukraine","form.country.ua",""));
    countries.add(new Country("Utd.Arab Emir.","form.country.ae","EMEA"));
    countries.add(new Country("United Kingdom","form.country.gb","EMEA"));
    countries.add(new Country("USA","form.country.us","NA"));
    countries.add(new Country("Uruguay","form.country.uy",""));
    countries.add(new Country("Uzbekistan","form.country.uz",""));
    countries.add(new Country("Vanuatu","form.country.vu","APAC"));
    countries.add(new Country("Vatican City","form.country.va",""));
    countries.add(new Country("Venezuela","form.country.ve",""));
    countries.add(new Country("Vietnam","form.country.vn","APAC"));
    countries.add(new Country("Western Sahara","form.country.eh",""));
    countries.add(new Country("Yemen","form.country.ye",""));
    countries.add(new Country("Zambia","form.country.zm",""));
    countries.add(new Country("Zimbabwe","form.country.zw",""));
    countries.add(new Country("Other","form.country.other",""));
    
    System.out.println(countries.size());
    
    for(int i = values.size(); i >= 1 ; i--) {
        final String countryName = values.get(i-1).get("countryName").getString();
        for(int j = 0; j < countries.size(); j++) {
            //System.out.println(countries.get(j).getValue());
            if(countries.get(j).getValue().equals(countryName)) {   
                countries.add(0, countries.get(j));
                countries.remove(j);
            }
        }
    }
    
%>

    <c:set var="countries" value="<%= countries %>"/>
        
    <option value=''><fmt:message key='form.option.default'/></option>
 
    <c:forEach items="${countries}" var="country">
        <option title="${country.title}" value="${country.value}"><fmt:message key="${country.name}"/></option>
    </c:forEach>
   