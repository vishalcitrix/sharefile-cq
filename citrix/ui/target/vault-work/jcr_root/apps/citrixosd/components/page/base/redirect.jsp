<%--

    Redirect logic.

    Allows for internal redirects via the RequestDispatcher.forward() method,
    used only for internal links. Sends a 404 response on attempts to call this
    on external links.

    Allows for 301/302 redirects to any domain or context path.

    Will send the redirect in cases of WCMMode.DISABLED. Otherwise, relies on
    redirectMessage.jsp for sending messages to front end about status of properties.

--%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
				 java.util.ArrayList,
				 java.util.Map,
				 com.day.cq.wcm.foundation.ELEvaluator,
				 com.citrixosd.utils.ContextRootTransformUtil,
				 com.citrixosd.SiteUtils,
				 com.citrixosd.utils.Utilities,
				 java.net.URLEncoder" 
%>
<%@include file="/libs/foundation/global.jsp" %>
<%!
    // returns a string (without any ? or & prefix) with the __col_ cookies as request parameters
    String getCookieQueryString (Cookie[] cookies) {
        final String COOKIE_PREFIX = "__col_";
        //request parameter is "__col_mkt_cookies"
        boolean populated = false;
        StringBuilder queryString = new StringBuilder("__col_mkt_cookies=");
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().startsWith(COOKIE_PREFIX)) {
                    //this is a cookie we want to append as a request parameter
                    try {
                        queryString.append(URLEncoder.encode(cookie.getName() + "=" + cookie.getValue() + "; ", "UTF-8"));
                        populated = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (populated)
            return queryString.toString().replaceAll("\\+", "%20");
        else return "";
    }
%>
<%
    // read the redirect target from the 'page properties'
    String location = properties.get("redirectTarget", "");
    String redirectType = properties.get("redirectOptions", "internal");
    String queryString = request.getQueryString();
    String redirectErrorMessage = "";
    // resolve variables in location
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
	
    //parse redirects based on parameters
    ArrayList<Map<String, Property>> paramRedirects = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("redirectCat")) {
        final Node baseNode = currentNode.getNode("redirectCat");
        paramRedirects = Utilities.parseStructuredMultifield(baseNode);
        if(paramRedirects != null) {
        	for (Map<String, Property> temp : paramRedirects) {
        		String tempRedirectParam = temp.get("redirectParam").getString();
        		String tempRedirectParamVal = temp.get("redirectParamVal").getString();
        		String tempRedirectTarget = temp.get("redirectTarget").getString();
        		String tempRedirectOptions = temp.get("redirectOptions").getString();
 				if(request.getParameter(tempRedirectParam) != null && request.getParameter(tempRedirectParam).equals(tempRedirectParamVal)) {
 					location = tempRedirectTarget;
 					redirectType = tempRedirectOptions;
 				}
    		}
        }
    }
    
    /* 
        if in 'publish' or mode, handle the redirect
        if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
        check for recursion
    */
    if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
        // check for absolute path
        final int protocolIndex = location.indexOf(":/");
        final int queryIndex = location.indexOf('?');
        final String redirectPath;
        if(protocolIndex > -1 && (queryIndex == -1 || queryIndex > protocolIndex)) {
            redirectPath = location;
        }else {
            redirectPath = request.getContextPath() + location;
        }
        request.setAttribute("redirectLocation", redirectPath);
        if (redirectType.equals("internal")) {
            try {
                if (protocolIndex != -1 || queryIndex != -1) {
                    // no calling forward on external url or with query params.. return 404
                    redirectErrorMessage = "Can't use an external URL or query parameters with internal redirect!";
                    String errorPage = currentPage.getAbsoluteParent(2).getPath() + "/404";
                    errorPage = ContextRootTransformUtil.transformedPath(errorPage,request);
                    if (WCMMode.fromRequest(request) == WCMMode.DISABLED)
                        response.sendRedirect(errorPage);
                } else {
                    // internal redirect.. browser will not see new path
                    request.setAttribute("redirectLocation", "");
                    if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
                        request.getRequestDispatcher(redirectPath).forward(request, response);
                        request.setAttribute("isForwarded", Boolean.TRUE);
                        return;
                    } else {
                        request.setAttribute("redirectLocation", redirectPath);
                    }
                }
            } catch (Exception e) {
                // Resource not found error
                if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
                    request.setAttribute("isForwarded", Boolean.TRUE);
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } else {
            // normal, 301/302 redirect
            // need to handle the case where the redirect path has a query string already
            final int redirectQueryIndex = redirectPath.indexOf("?");
            String redirectLocation = "";
            String[] trackingDomains = SiteUtils.getTrackingDomainsAsArray(currentPage);
            boolean toTrack = false;
            if (trackingDomains != null && trackingDomains.length > 0) {
                for (String domain : trackingDomains) {
                    if (redirectPath.contains(domain))
                        toTrack = true;
                }
            }
            if (toTrack) {
                //calculate the query string from request cookies
                Cookie[] cookies = request.getCookies();
                String cookieQueryString = getCookieQueryString(cookies);
                if (queryString != null && queryString.length() > 0) {
                    queryString += "&" + cookieQueryString;
                } else {
                    queryString = cookieQueryString;
                }
            }
            if (redirectQueryIndex == -1 && queryString != null) {
                redirectLocation = redirectPath + "?" + queryString;
            } else if (queryString != null) {
                // note no checking for duplicate request vars
                redirectLocation = redirectPath + "&" + queryString;
            } else {
                redirectLocation = redirectPath;
            }
            if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
                redirectLocation = ContextRootTransformUtil.transformedPath(redirectLocation,request);
                if(redirectType.equals("301")){
                    response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
                    response.setHeader("Location", redirectLocation);
                } else {
                    response.sendRedirect(redirectLocation);
                }
            }
            else
                request.setAttribute("redirectLocation", redirectLocation);
        }
    } else {
        if (WCMMode.fromRequest(request) == WCMMode.DISABLED && location.length() > 0)
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
    request.setAttribute("redirectErrorMessage", redirectErrorMessage);
%>
