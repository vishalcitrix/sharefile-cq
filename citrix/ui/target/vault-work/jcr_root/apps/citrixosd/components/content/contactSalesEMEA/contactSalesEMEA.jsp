<%--
  Contact Sales component for EMEA.
  vishal.gupta.82@citrix.com
--%>

<%@page import="java.util.Locale"%>
<%@page import="com.adobe.granite.xss.XSSAPI"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final String css = properties.get("css", "");
	final String jscode = properties.get("jscode","");
	final String saleTaskSubject = properties.get("saleTaskSubject","");
	final String buttonColor = properties.get("buttonColor", "blue");
%>   
    
    <c:set var="leadGeo" value="<%= properties.get("leadGeo", false) %>"/>
    <c:set var="collabration" value="<%= properties.get("collabration", false) %>"/> 
    <c:set var="cssCode" value="<%= properties.get("csscode", "") %>"/>
    <c:set var="jscode" value="<%= jscode %>"/> 
	<c:set var="saleTaskSubject" value="<%= saleTaskSubject %>"/>
	
	<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${currentDesign.path}/js/form.js"></script>
    <c:if test="${not empty jscode}">
        <script type="text/javascript">
            ${jscode}
        </script>
    </c:if>
    <c:if test="${not empty cssCode}">
        <style type="text/css">
            ${cssCode}
        </style>
    </c:if>
    
	<!--  adding cookie evaluator -->
 	<cq:include script="cookieEval.jsp"/>
    
<div class="contactSales <%= css %>">
        <c:choose>
            <c:when test="${collabration}">
                <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" onSubmit="contact(this,${leadGeo});" name="contactSales">
            </c:when>
            <c:otherwise>
                <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" name="contactSales">
            </c:otherwise>
        </c:choose>
        <div class="form-box">
        
            <!--  adding hidden fields (inheriting)-->
            <cq:include script="hiddenFields.jsp"/>
            <c:if test="${not empty saleTaskSubject}">
		    	<input type="hidden" name="sfdc_task_subject" value="${saleTaskSubject}">
		    </c:if>
            <!-- adding collaboration checkboxes (inheriting)-->
            <c:if test="${collabration}">
    			<label><fmt:message key="contactSales.needs.label"/></label>
    		</c:if>
            <cq:include script="isCollaboration.jsp"/>
        
            <ul>
                <li class="left-col">
                    <label for="FirstName"><fmt:message key="form.label.firstName"/><span class="asterix">*</span></label>
                    <input id="FirstName" name="FirstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>" placeholder="<fmt:message key="form.placeholder.firstName"/>">
                </li>
                <li class="right-col">
                    <label for="LastName"><fmt:message key="form.label.lastName"/><span class="asterix">*</span></label>
                    <input id="LastName"  name="LastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>" placeholder="<fmt:message key="form.placeholder.lastName"/>">
                </li>
                <li class="left-col">
                    <label for="Company"><fmt:message key="form.label.company"/><span class="asterix">*</span></label>
                    <input id="Company" name="Company" type="text" class="required">
                </li>
                <li class="right-col">
                    <label for="Address"><fmt:message key="form.label.address"/><span class="asterix">*</span></label>
                    <input id="Address" name="Address" type="text" class="required">
                </li>
                <li class="left-col">
                	<div id="zipCity">
	                    <div style="display:inline-block;">
	                        <label for="PostalCode"><fmt:message key="form.label.zipcode"/><span class="asterix">*</span></label>
	                        <input id="PostalCode" name="PostalCode" type="text" class="required" style="width: 65px !important;">
	                    </div>
	                    <div style="display:inline-block;">
	                        <label for="City"><fmt:message key="form.label.city"/><span class="asterix">*</span></label>
	                        <input id="City" name="City" type="text" class="required" style="width: 110px !important;">
	                    </div>
	             	</div>
                </li>
                <li class="right-col">
                    <label for="Country"><fmt:message key="form.label.country"/><span class="asterix">*</span></label>
                    <select id="Country" name="Country" class="required">
                        <!-- adding country list -->
                        <cq:include script="countryList.jsp"/>
                    </select>
                </li>
                <li class="left-col">
                    <label for="Email"><fmt:message key="form.label.email"/><span class="asterix">*</span></label>
                    <input id="Email" name="Email" type="text" class="required email" title="<fmt:message key="form.validate.email"/>">
                </li>
                <li class="right-col">
                    <label for="Phone"><fmt:message key="form.label.phone"/><span class="asterix">*</span></label>
                    <input id="Phone" name="Phone" type="text" class="required phone" title="<fmt:message key="form.validate.phone"/>">
                </li>
                <li class="left-col">
                    <label for="Department__c"><fmt:message key="form.label.department"/><span class="asterix">*</span></label>
                    <!-- adding department list (inheriting)-->
                    <cq:include script="departmentList.jsp"/>
                </li>
                <li class="right-col">
                    <label for="Industry"><fmt:message key="form.label.industry"/><span class="asterix">*</span></label>
                    <!-- adding industry list (inheriting)-->
                    <cq:include script="industryList.jsp"/>
                </li>
            </ul>
            <div class="clearBoth"></div>
            
            <br><br>
            <!--  add more info -->
            <label class="moreInfo"><fmt:message key="form.company.moreinfo"/></label>
            
            <div class="lab-sel-div">
                <div>
                    <!-- adding invest in solution -->
                    <label for="Timeframe__c"><fmt:message key="form.label.investsolution"/><span class="asterix">*</span></label>
                    <cq:include script="investInSolution.jsp"/>
                </div>
                
                <div>
                    <!-- adding decision making section -->
                    <label for="Purchasing_Role__c"><fmt:message key="form.label.role"/><span class="asterix">*</span></label>
                    <cq:include script="roleInProcess.jsp"/>
                </div>
                
                <div>
                    <!-- adding no. of employee list (inheriting) -->
                    <label for="NumberofEmployees"><fmt:message key="form.label.numberOfEmployees2"/><span class="asterix">*</span></label>
                    <cq:include script="employeeList.jsp"/>
                </div>
                
                <div>
                    <!-- adding current solution section -->
                    <label for="Current_Solution_Y_N__c"><fmt:message key="form.label.currentsolution"/><span class="asterix">*</span></label>
                    <cq:include script="currentSolution.jsp"/>
                </div>
            </div>
            
            <div class="clearBoth"></div>
            <!--  adding form comments (inheriting)-->
            <cq:include script="formComments.jsp"/>
            
            <div class="privacy-policy">
                <span class="checkbox left"><input type="checkbox" id = "privacy-policy" name="privacy-policy" class="required" ></span>
                <cq:include path="privacypolicy" resourceType="citrixosd/components/content/text" />
            </div>
                        
            <input type="submit" value="<fmt:message key="form.label.submit"/>" class="button <%= buttonColor %>" title="<fmt:message key="form.label.submit"/>">
            
            <!--  add extra text in form -->
            <cq:include path="extraText" resourceType="citrixosd/components/content/text" />
        </div>
    </form>
</div>