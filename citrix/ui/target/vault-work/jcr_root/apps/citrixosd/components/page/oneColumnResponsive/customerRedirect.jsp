<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%@page import="java.util.Locale, com.day.cq.wcm.api.WCMMode"%>

<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String domainName = request.getServerName();
    String customerRedirectUrl = properties.get("customerRedirectUrl","");
    String transformed = transformer.transform(customerRedirectUrl);
    if (transformed != null) {
        customerRedirectUrl = transformed;
    }    
%>


<c:set var="customerRedirectUrl" value="<%= customerRedirectUrl %>"/>
<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>
<c:set var="referrer" value="<%= request.getHeader("referer") %>"/>
<c:set var="pagePath" value="<%= request.getServerName() %>"/>

<c:if test="${(pageProperties.customerRedirect eq 'true') && (not empty customerRedirectUrl) && (empty pageProperties.redirectTarget) && (not empty cookie['G2MProminentLogin'].value) && (pageDepth eq 3) && (not fn:contains(referrer,pagePath))}">
    <c:if test="${cookie['G2MProminentLogin'].value eq 'G2MExistingUser%3Dtrue'}">
        <%
            if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
        %>
            <c:redirect url="${customerRedirectUrl}" />
        <%
            }else {
        %>
            <div>
                <p>Customer Cookie Set, this page redirects to <a href="${customerRedirectUrl}">${customerRedirectUrl}</a>.</p>
            </div>
        <%        
            }
        %>
    </c:if>
</c:if>
