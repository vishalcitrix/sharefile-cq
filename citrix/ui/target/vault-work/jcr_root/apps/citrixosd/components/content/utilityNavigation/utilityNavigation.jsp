<%--
	Utility Navigation

  	Header contains logo component on the right and link set component on the right.
  	
  	Note: Depends on logo, countrySelector, listSet component.
  	
  	achew@siteworx.com

--%>

<%@page import="com.siteworx.component.scheduler.ScheduledComponent"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.siteworx.component.scheduler.config.SchedulerTimeZoneService"%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
	boolean isEditMode = WCMMode.fromRequest(request).equals(WCMMode.EDIT);

	final SchedulerTimeZoneService timeZoneService = sling.getService(SchedulerTimeZoneService.class);
	final TimeZone timeZone = timeZoneService.getTimeZone();
	final ScheduledComponent scheduledComponent = new ScheduledComponent(currentNode, isEditMode, timeZone);
	scheduledComponent.initialize();

	if (isEditMode) {
	    editContext.getEditConfig().setEmptyText(currentStyle.get("cq:emptyText", editContext.getEditConfig().getEmptyText()));
	    editContext.getEditConfig().getToolbar().add(new Toolbar.Separator());
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label("Status:"));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label(scheduledComponent.getStatusMessage()));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Separator());
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label("Current Time: "));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label(scheduledComponent.getCurrentTime()));
	}

	final String asyncPath = resource.getPath() + ".scheduler.html";
%>

<c:set var="asyncPath" value="<%= asyncPath %>"/>
<div class="container">
	<div style="float:left;">
 		<swx:setWCMMode mode="READ_ONLY">
 			<cq:include script="logo.jsp"/>
 		</swx:setWCMMode>
	</div>	
	
	<div style="float:right;">
		<a class="utility-navigation-sales-chat-async hide" href="${asyncPath}" editmode="${isEditMode}"></a>
		<div class="links ${properties['navStyle']}">
		    <ul>
   		    	<c:if test="${empty properties['hideCountrySelector'] || properties['hideCountrySelector'] == false}">
			        <li>
			        	<swx:setWCMMode mode="READ_ONLY">
			        		<cq:include path="countrySelector" resourceType="citrixosd/components/content/countrySelector"/>
			        	</swx:setWCMMode>
			        </li>
		        </c:if>
		        <c:if test="${empty properties['hideCountrySelectorSlash'] || properties['hideCountrySelectorSlash'] == false}">
		        	<li><span>|</span></li>
		        </c:if>
		        <li style="padding: 0; ${isDesignMode ? 'min-width: 200px;' : ''}">
		        	<swx:setWCMMode mode="READ_ONLY">
        				<%-- Link Set component --%>
		        		<cq:include script="linkSet.jsp"/>
		        	</swx:setWCMMode>
		        </li>
			</ul>
		</div>
	</div>
	
	<div class="clearBoth"></div>
</div>
