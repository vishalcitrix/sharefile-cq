<%--
	Side Note
  	
  	This should be placed on the left rail because there is a fixed image background
  	associated with the component. This will have a round border with a shadow background 
  	image. Margin top is also added to adjust how to display this container.
  	
	achew@siteworx.com
	
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String MARGIN_TOP_PROPERTY = "marginTop";
%>

<div class="content-pod" style="margin-top:<%= properties.get(MARGIN_TOP_PROPERTY, "0")%>px;">
	<div class="pod">
		<cq:include path="sideNotePar" resourceType="foundation/components/parsys"/>
	</div>
</div>