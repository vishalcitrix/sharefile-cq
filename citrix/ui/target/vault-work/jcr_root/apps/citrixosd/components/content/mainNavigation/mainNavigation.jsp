<%--
	Main Navigation
  
  	This component will take all the child pages below the locale page and display it
  	on the navigation. This will also display the current parent by using a highlighted
  	css class. Authors can also add additional links.
  
  	Dev: The root page would be the website's 2nd level: 'content/website/language'. The 
  	right text will be a i18n tag for phone number. The global navigation will 
  	recursively check up to the parent until it hits the navigation root depth. The depth
  	will determine where to stop and is defined by page structure. 
  	
 	0 = content
  	1 = website
 	2 = language (default)
 	  
 	 achew@siteworx.com
 	 vishal.gupta@citrix.com
--%>

<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.wcm.api.PageFilter"%>
<%@page import="com.day.cq.wcm.api.components.Component"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final int ROOT_PAGE_DEPTH = 2; // 'content/website/language'
  	private static final String ADDITIONAL_LINK_TEXT_PROPERTY = "additionalLinkText";
  	private static final String ADDITIONAL_LINK_PATH_PROPERTY = "additionalLinkPath";
  	private static final String RIGHT_TEXT_I18N_PROPERTY = "rightTextI18n";
	
	private List<Page> getNavigationPages(Page currentPage, ServletRequest request, int navigationRootDepth) {
		List<Page> navigationPages = null;
	
		// Get all the pages that are only in the 2nd level of the web site eg. g2m/en
		Page navigationRootPage = currentPage.getAbsoluteParent(navigationRootDepth);
		if (navigationRootPage == null && currentPage != null) {
			// Use is in root page we can't determine which language the user is in, throw an error.
		}else if (navigationRootPage != null) {
			final Iterator<Page> children = navigationRootPage.listChildren(new PageFilter(request));
			navigationPages = new ArrayList<Page>();
			while (children.hasNext()) {
				Page child = children.next();
				if (!child.isHideInNav()) {
					navigationPages.add(child);
				} else {
					continue; //Skip this page does not have a navigation title
				}
			}
		}
		return navigationPages;
	}

%>

<%
	boolean isNavigationRoot = false;
	final Page rootPage = currentPage.getAbsoluteParent(ROOT_PAGE_DEPTH);
	if (rootPage != null && rootPage.equals(currentPage))
		isNavigationRoot = true;
	final Page currentNavigationPage = currentPage.getAbsoluteParent(ROOT_PAGE_DEPTH + 1);
	final List<Page> navigationPages = getNavigationPages(currentPage, request, ROOT_PAGE_DEPTH);
	final String rightTextI18n = properties.get(RIGHT_TEXT_I18N_PROPERTY, "");
	final String additionalLinkText = properties.get(ADDITIONAL_LINK_TEXT_PROPERTY, "");
	final String additionalLinkPath = properties.get(ADDITIONAL_LINK_PATH_PROPERTY, null);

	final Map<String, Object> globalNavigation = new HashMap<String, Object>();
	globalNavigation.put("rootPage", rootPage);
	globalNavigation.put("currentNavigationPage", currentNavigationPage);
	globalNavigation.put("navigationPages", navigationPages);
	globalNavigation.put("isNavigationRoot", isNavigationRoot);
	globalNavigation.put(ADDITIONAL_LINK_TEXT_PROPERTY, additionalLinkText);
	globalNavigation.put(ADDITIONAL_LINK_PATH_PROPERTY, additionalLinkPath);
	globalNavigation.put(RIGHT_TEXT_I18N_PROPERTY, rightTextI18n);
	globalNavigation.put("hasAdditionalLinks", currentNode != null ? currentNode.hasNode("additionalLinks") ? currentNode.getNode("additionalLinks").hasNode("links") : false : false);
%>

<c:set var="globalNavigation" value="<%= globalNavigation %>"/>
<c:set var="defaultNavigationStyle" value="<%= properties.get("defaultNavigationStyle", false) %>"/>

<c:choose>
    <c:when test="${defaultNavigationStyle}">
        <div class="nav navN">
    </c:when>
    <c:otherwise>
        <div class="nav">
    </c:otherwise>
</c:choose>
	<ul class="container">
		<li><a href="${globalNavigation.rootPage.path}" class="${empty globalNavigation.currentNavigationPage ? 'current' : ''}" id="home"><span>${globalNavigation.rootPage.navigationTitle}</span></a></li>
		<c:choose>
			<c:when test="${empty globalNavigation.navigationPages && not globalNavigation.isNavigationRoot}">
				<%--User is at root page, this component can not determine which links to display because locale is not set --%>
				<li><a>Root page can not have a main navigation</a></li>
			</c:when>
			<c:otherwise>
				<c:forEach items="${globalNavigation.navigationPages}" begin="0" var="navigationPage" varStatus="i">
					<li>
						<a href="${navigationPage.path}" class="${i.last ? 'last' : ''} ${empty globalNavigation.currentNavigationPage ? '' : globalNavigation.currentNavigationPage.path eq navigationPage.path ? 'current' : ''}">
							${empty navigationPage.navigationTitle ? navigationPage.title : navigationPage.navigationTitle}
						</a>
					</li>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		
		<li>
			<swx:setWCMMode mode="READ_ONLY">
				<cq:include path="additionalLinks" resourceType="citrixosd/components/content/linkSet"/>
			</swx:setWCMMode>
		</li>
		
		<%--Graceful degradation: Link set will replace this functionality --%>
		<li class="additional-links">
	        <ul>
	        	<c:choose>
	        		<c:when test="${not empty globalNavigation.additionalLinkText || not empty globalNavigation.additionalLinkPath}">
	        			<li><a href="${globalNavigation.additionalLinkPath}" class="${not empty globalNavigation.rightTextI18n || globalNavigation.hasAdditionalLinks ? '' : 'additional-last-link'}">
			        			<c:if test="${not empty globalNavigation.additionalLinkText}">
			        				<fmt:message key="${globalNavigation.additionalLinkText}"/>
			        			</c:if>
	        				</a>
	        			</li>
	        			<c:if test="${not empty globalNavigation.rightTextI18n}">
	        				<li><span>|</span></li>
	        			</c:if>
	        		</c:when>
	        		<c:otherwise>
	        			<%--Do not display the additional link --%>
	        		</c:otherwise>
	        	</c:choose>
	        	
	        	<c:if test="${not empty globalNavigation.rightTextI18n}">
	        		<li><span class="salesPhone ${globalNavigation.hasAdditionalLinks ? '' : 'additional-last-link'}"><fmt:message key="${globalNavigation.rightTextI18n}"/></span></li>
	        	</c:if>
	        	
	        	<c:if test="${globalNavigation.hasAdditionalLinks && (not empty globalNavigation.additionalLinkText || not empty globalNavigation.rightTextI18n)}">
	        		<li><span>|</span></li>
	        	</c:if>
	        </ul>
	    </li>
	    <%-- //Graceful degradation --%>
	    
	</ul>
</div>