<%--
    Sticky Header.
    
    Javascript will change position css attribute to absolute top when the user
    scrolls below that component. The component will also have an option to display the or in 
    i18n format. Authors can input the description in the Description text field which will 
    display the text in the correct css.
    
    Dev: There is a jQuery script which checks if the user/client scrolls below this 
    component container's initial height. The 'or' (i18n) splitting the two iparsys component 
    is controlled by a checkbox in the dialog of this component. If it is checked, the 
    'or' will display, otherwise it will hide. 
    
    Note: Javascript is disabled in edit mode because it could be in the way for the author.
    
    achew@siteworx.com
    
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.api.Page"%>
<%@page import="com.day.cq.wcm.api.components.Component"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String DESCRIPTION_PROPERTY = "description";
    public static final String HIDE_SPRITE_LIST_PROPERTY = "hideSpriteList";
%>

<c:set var="stickyHeader" value="<%= properties %>"/>

<div id="action-header" class="action-nav ${(isPreviewMode || isDisabledMode || isPublishInstance) ? 'active' : 'inactive'}">
    <div class="container">
        <div style="width: 540px; float:left">
            <h2>${stickyHeader.description}</h2>
            <br/>
            <c:if test="${not stickyHeader.hideSpriteList}">
				<swx:setWCMMode mode="READ_ONLY">
                	<cq:include script="spriteList.jsp"/>
                </swx:setWCMMode>
            </c:if>
        </div>
        
        <ul class="action">
            <li>            
				<swx:setWCMMode mode="READ_ONLY">
					<cq:include path="buttonLeft" resourceType="/apps/citrixosd/components/content/button"/>
				</swx:setWCMMode>
				<c:choose>
					<c:when test="${stickyHeader.separator eq 'divider'}">
                        <li class="split"><span class="divider"></span><span class="split-text"> </span></li>
                    </c:when>
                    
                    <c:when test="${stickyHeader.separator eq 'textAndLine'}">
                        <li class="split"><span class="divider"></span><span class="split-text"><fmt:message key="or" /></span></li>
                    </c:when>
					
					<c:otherwise>
						<%--Do not add divider --%>
					</c:otherwise>
					
				</c:choose>
            <li>
               <swx:setWCMMode mode="READ_ONLY">
               		<cq:include path="buttonRight" resourceType="/apps/citrixosd/components/content/button"/>
               </swx:setWCMMode>
            </li>
        </ul>
    </div>
</div>
