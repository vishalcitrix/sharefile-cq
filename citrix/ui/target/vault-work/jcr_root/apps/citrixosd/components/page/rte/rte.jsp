<!DOCTYPE html>
<html>
<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.*"%>
<cq:include script="/libs/wcm/core/components/init/init.jsp"/>
<%!
	public void getStyles(Node currentNode, Map<String,String> styleMap, String style) throws RepositoryException {
		if (currentNode.hasNode(style)) {
			final Node styleNode = currentNode.getNode(style);
			NodeIterator iter = styleNode.getNodes();
			while (iter.hasNext()) {
				Node styleItemNode = iter.nextNode();
				if (styleItemNode.hasProperty("name") && styleItemNode.hasProperty("css")) {
					styleMap.put(styleItemNode.getProperty("css").getString(),styleItemNode.getProperty("name").getString());
				}
			}
		}
	}
%>
<%
    String styleCSS = properties.get("rteDesign","");
	Map <String, String> family = new HashMap<String, String>();
	Map <String, String> size = new HashMap<String, String>();
	Map <String, String> lineHeight = new HashMap<String, String>();
	Map <String, String> color = new HashMap<String, String>();
	Map <String, String> icon = new HashMap<String, String>();
	Map <String, String> others = new HashMap<String, String>();
	getStyles(currentNode, family,"fontFamily");
	getStyles(currentNode, size,"fontSize");
	getStyles(currentNode, lineHeight,"lineHeight");
	getStyles(currentNode, color,"color");
	getStyles(currentNode, icon,"icon");
	getStyles(currentNode, others,"others");
	Iterator<Map.Entry<String, String>> familyEntries = family.entrySet().iterator();
	Iterator<Map.Entry<String, String>> sizeEntries = size.entrySet().iterator();
	Iterator<Map.Entry<String, String>> lineHeightEntries = lineHeight.entrySet().iterator();
	Iterator<Map.Entry<String, String>> colorEntries = color.entrySet().iterator();
	Iterator<Map.Entry<String, String>> iconEntries = icon.entrySet().iterator();
	Iterator<Map.Entry<String, String>> othersEntries = others.entrySet().iterator();
%>
	<head>
		<link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/rte.css"></link>
		<c:if test="not empty styleCSS"><link rel="stylesheet" href="${styleCSS}"></link></c:if>
	</head>
	<body>
		<form action="<%= currentPage.getPath()%>/jcr%3Acontent/style" method="POST" name="style" id="style" onsubmit="return addUpdate(this)">
			<h1>Add/Update styles for RTE</h1>
			<div class="message"></div>
			<label for="fontName">Font Name (This will appear in RTE dropdown)</label>
			<input type="text" name="fontName" id="fontName">
			<label for="family">Font Family</label>
			<select name="family" id="family">
				<option value="">Select One</option>
<%
	while (familyEntries.hasNext()) {
		Map.Entry<String, String> entry = familyEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<label for="size">Font Size</label>
			<select name="size" id="size">
				<option value="">Select One</option>
<%
	while (sizeEntries.hasNext()) {
		Map.Entry<String, String> entry = sizeEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<label for="lineHeight">Font Line Height</label>
			<select name="lineHeight" id="lineHeight">
				<option value="">Select One</option>
<%
	while (lineHeightEntries.hasNext()) {
		Map.Entry<String, String> entry = lineHeightEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<label for="color">Font Color</label>
			<select name="color" id="color">
				<option value="">Select One</option>
<%
	while (colorEntries.hasNext()) {
		Map.Entry<String, String> entry = colorEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<label for="icon">Icons</label>
			<select name="icon" id="icon">
				<option value="">Select One</option>
<%
	while (iconEntries.hasNext()) {
		Map.Entry<String, String> entry = iconEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<label for="others">Other Classes</label>
			<select name="others" id="others">
				<option value="">Select One</option>
<%
	while (othersEntries.hasNext()) {
		Map.Entry<String, String> entry = othersEntries.next(); %>
				<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option><%
	}
%>
			</select>
			<input type="submit" value="Add/Update">
		</form>
		<div class="print">
			<table><%
	if (currentNode.hasNode("style")) {
		final Node styleNode = currentNode.getNode("style");
		NodeIterator iter = styleNode.getNodes();
		while (iter.hasNext()) {
			String fontFamily = "";
			String fontSize = "";
			String fontLineHeight = "";
			String fontColor = "";
			String fontClass = "";
			String fontName = "";
			Node styleItemNode = iter.nextNode();
			if(styleItemNode.hasProperty("family"))
				fontFamily = styleItemNode.getProperty("family").getString();
			if(styleItemNode.hasProperty("size"))
				fontSize = styleItemNode.getProperty("size").getString();
			if(styleItemNode.hasProperty("lineHeight"))
				fontLineHeight = styleItemNode.getProperty("lineHeight").getString();
			if(styleItemNode.hasProperty("color"))
				fontColor = styleItemNode.getProperty("color").getString();
			if(styleItemNode.hasProperty("class"))
				fontClass = styleItemNode.getProperty("class").getString();
			if(styleItemNode.hasProperty("fontName"))
				fontName = styleItemNode.getProperty("fontName").getString();
			else
				fontName = "Lorem ipsum";
%>
				<tr id="<%= styleItemNode.getName() %>"><td><a href="#" class="deleteStyle" title="<%= fontName %>" data-name="<%= styleItemNode.getName() %>">&#10006;</a></td><td><div class="<%= fontFamily %> <%= fontSize %> <%= fontLineHeight %> <%= fontColor %> <%= fontClass %>"><%= fontName %></div></td><tr><%
		}
	}
%>			</table>
		</div>
		<script src="<%= currentDesign.getPath() %>/js/common.js" type="text/javascript"></script>
		<script type="text/javascript">
			var path="<%= currentPage.getPath()%>/jcr%3Acontent/style/";
		</script>
	</body>
</html>