<%--

  Chekbox component.
  Chekbox  for Custom Form
  
  vishal.gupta.82@citrix.com

--%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>
<%
    // TODO add you code here
    final String fieldLabel = properties.get("fieldLabel","");   
    final String fieldName = properties.get("fieldName","");        
    final String fieldId = properties.get("fieldId","");
    final String fieldValue = properties.get("fieldValue",""); 
    final boolean isRequired = properties.get("isRequired",false);
    
    if (editContext != null && editContext.getEditConfig() != null) {
    	if(fieldName == ""){
            editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Checkbox Field - (Please select field name)"));  
        }else{
            editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Checkbox Field - (name: " + fieldName + ")"));        
        }
        
        editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());	
    }
    
%>    
   <c:set var="fieldId" value="<%= fieldId %>" />
   <c:set var="fieldName" value="<%= fieldName %>" />
   <c:set var="fieldValue" value="<%= fieldValue %>" />
   <c:set var="isRequired" value="<%= isRequired %>" />
   
   	<div class="check-box">
   		 <c:choose>
	        <c:when test="${fieldId ne ''}">
	        	<c:choose>
			        <c:when test="${isRequired}">
			             <input type="checkbox" id="<%= fieldId %>" name="<%= fieldName %>" value="<%= fieldValue %>" class="required" <c:if test="${not empty param[fieldName] ? param[fieldName] eq fieldValue : false}">checked</c:if>>
			        </c:when>
			        <c:otherwise>
			             <input type="checkbox" id="<%= fieldId %>" name="<%= fieldName %>" value="<%= fieldValue %>" <c:if test="${not empty param[fieldName] ? param[fieldName] eq fieldValue : false}">checked</c:if>>
			        </c:otherwise>  
			    </c:choose>   
	        </c:when>
	        <c:otherwise>
	        	<c:choose>
			        <c:when test="${isRequired}">
			             <input type="checkbox" name="<%= fieldName %>" value="<%= fieldValue %>" class="required" <c:if test="${not empty param[fieldName] ? param[fieldName] eq fieldValue : false}">checked</c:if>>
			        </c:when>
			        <c:otherwise>
			             <input type="checkbox" name="<%= fieldName %>" value="<%= fieldValue %>" <c:if test="${not empty param[fieldName] ? param[fieldName] eq fieldValue : false}">checked</c:if>>
			        </c:otherwise>  
			    </c:choose>  
	        </c:otherwise>  
	    </c:choose>   
        <div><%= fieldLabel %></div>
   	</div>
	<div class="clearBoth"></div>
    