<%--
  Trail Days Left component.
  This component displays no.of days left for the trail. To be used with Commerce Integration.
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%
    String daysLeft = properties.get("daystxt", String.class);
    WCMMode mode = WCMMode.fromRequest(request);
    pageContext.setAttribute("mode", mode);
%>

<div class="left-content">
    <div class="days-left">
        <div id="daysleft" class="days">
            <c:if test="${mode eq 'EDIT'}">
                <p style="align:center">DAYS LEFT</p>
            </c:if>
        </div>
        <span id="message">
            ${properties.message}
        </span>
    </div>
</div>
<script>
    if(daysleft >= 0)
        {
            var dayscontent = daysleft + "<span><%= daysLeft %></span>";
            jQuery("#daysleft").append(dayscontent);
            jQuery(".headline").addClass("daysLeft");
            var content = jQuery("#message").html();
            content = content.replace("#DAYS#", daysleft);
            jQuery("#message").html(content);
          
        }
</script>