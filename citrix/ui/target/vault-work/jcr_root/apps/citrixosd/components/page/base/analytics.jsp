<%@page import="java.util.Locale,
		com.day.cq.tagging.Tag,
		com.day.cq.tagging.TagManager,
		com.citrixosd.SiteUtils,
		java.util.TimeZone,
		java.util.Date,
		javax.servlet.http.HttpServletRequest"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%-- Include  analytics tag if not already set --%>
<c:set var="anaLyticsTagSet" value="<%= request.getAttribute("analyticsTagSet") %>"/>
<c:if test="${not analyticsTagSet}">
	<cq:include script="analytics_tag.jsp" />
</c:if>
<%-- Set environment variable --%>
<c:choose>
	<c:when test="${isDev || isQA}">
		<c:set var="analyticsEnv" value="dev"/>
	</c:when>
	<c:when test="${isStage}">
		<c:set var="analyticsEnv" value="qa"/>
	</c:when>
	<c:when test="${isProd}">
		<c:set var="analyticsEnv" value="prod"/>
	</c:when>
	<c:otherwise>
		<c:set var="analyticsEnv" value="dev"/>
	</c:otherwise>
</c:choose>
<c:set var="analyticsSrcUrl" value="<%= SiteUtils.getAnalyticsURL(currentPage, pageContext.getAttribute("analyticsEnv").toString()) %>"/>
<c:if test="${not empty analyticsSrcUrl}">
	<script type="text/javascript" src="${analyticsSrcUrl}"></script>
</c:if>
<div class="mktgContainer">
	<img src="<%= currentDesign.getPath() %>/css/static/images/1x1.gif" id="mktgPixelImg" width="0" height="0" alt="">
</div>