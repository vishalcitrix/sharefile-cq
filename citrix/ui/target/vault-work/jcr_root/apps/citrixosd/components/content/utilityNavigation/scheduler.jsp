<%@page import="org.apache.sling.commons.json.JSONObject"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.siteworx.component.scheduler.ScheduledComponent"%>
<%@page import="com.siteworx.component.scheduler.config.SchedulerTimeZoneService"%>

<%@include file="/libs/foundation/global.jsp"%>

<% 
   final boolean isEditMode = WCMMode.fromRequest(request).equals(WCMMode.EDIT);
   final ScheduledComponent scheduledComponent = new ScheduledComponent(currentNode, isEditMode);
   JSONObject json = new JSONObject();
   json.put("scheduled", scheduledComponent != null ? scheduledComponent.isShowing() : false);
  
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.setContentType("application/json");
   response.getWriter().write(json.toString());
%>