<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
    public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics";
%>

<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
%>
<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="favIcon" value="<%= SiteUtils.getFavIcon(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getTitle() == null ? StringEscapeUtils.escapeHtml4(currentPage.getName()) : StringEscapeUtils.escapeHtml4(currentPage.getTitle()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="redirectLink" value="<%= properties.get("redirectTarget","") %>"></c:set>

<head>
    <%--Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <c:choose>
        <c:when test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) %>">

        </c:when>
        <c:otherwise>
            <c:if test="${not empty redirectLink}">
                <meta http-equiv="Refresh" content="20;URL=${redirectLink}">
            </c:if>
        </c:otherwise>
    </c:choose>

    <meta name="robots" content="noarchive">
    <meta http-equiv="expires" content="0">
    <cq:include script="meta.jsp"/>

    <%--CSS --%>
    <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/static.css" type="text/css">

    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/css/static/images/${not empty favIcon ? favIcon : 'favicon'}.ico">

    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    <meta name="keywords" content="${not empty siteName ? siteName : ''}">

     <%--Javascripts --%>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>

</head>