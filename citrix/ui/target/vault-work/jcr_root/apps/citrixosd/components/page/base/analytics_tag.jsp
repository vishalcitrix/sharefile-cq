<%@page import="java.util.Locale,
		com.day.cq.tagging.Tag,
		com.day.cq.tagging.TagManager,
		com.citrixosd.SiteUtils,
		java.util.TimeZone,
		java.util.Date,
		javax.servlet.http.HttpServletRequest"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%
	request.setAttribute("analyticsTagSet", true );
%>
<%!
	//Retrieve the tag name
	public String getTag(String[] tags, TagManager tagManager) {
		final StringBuilder builder = new StringBuilder();
		for(int i = 0; i < tags.length; i++) {
			final Tag tag = tagManager.resolve(tags[i]);
			if (tag != null) {
				if(i == 0) {
					builder.append(tag.getName());
				}else {
					builder.append(",").append(tag.getName());
				}
			}
		}
		return builder.toString();
	}
%>

<%
	String[] analyticsPagesetTags = null;
	String analyticsTemplate = currentPage.getName();
	String[] analyticsProductTags = null;
	String[] analyticsSubSectionTags = null;
	String[] analyticsCountryTags = null;
	String[] analyticsLanguageTags = null;
	String[] analyticsContentTypeTags = null;
	String[] analyticsPageSectionTags = null;
	String analyticsAbTest = null;
	String analyticsAbBranch = null;

	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	if (currentNode.hasNode("analytics-node")) {
		final Resource analyticsResource = resource.getChild("analytics-node");
		final ValueMap analyticsData = analyticsResource.adaptTo(ValueMap.class);

		analyticsPagesetTags = analyticsData.get("page-set", String[].class);
		analyticsTemplate = analyticsData.get("template", currentPage.getName());
		analyticsProductTags = analyticsData.get("product", String[].class);
		analyticsSubSectionTags = analyticsData.get("sub-section", String[].class);
		analyticsCountryTags = analyticsData.get("country", String[].class);
		analyticsLanguageTags = analyticsData.get("language", String[].class);
		analyticsContentTypeTags = analyticsData.get("content-type", String[].class);
		analyticsPageSectionTags = analyticsData.get("section", String[].class);
		analyticsAbTest = analyticsData.get("abTest", "");
		analyticsAbBranch = analyticsData.get("abBranch", "");
	}
	Locale locale = null;
	final Page localePage = currentPage.getAbsoluteParent(2);
	if (localePage != null) {
		try {
			locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
		} catch (Exception e) {
			locale = request.getLocale();
		}
	}
	else
		locale = request.getLocale();

	String dfString = "E, dd MMM yyyy HH:mm:ss z";
	java.text.DateFormat dfPST = com.day.cq.commons.date.DateUtil.getDateFormat(dfString, Locale.US);
	dfPST.setTimeZone(TimeZone.getTimeZone("PST"));
	String analyticsSystemDatePSTValue = dfPST.format(new Date());
	java.text.DateFormat dfGMT = com.day.cq.commons.date.DateUtil.getDateFormat(dfString, Locale.UK);
	dfGMT.setTimeZone(TimeZone.getTimeZone("GMT"));
	String analyticsSystemDateGMTValue = dfGMT.format(new Date());

	String defaultProduct = "";
	final Page defaultProductPage = currentPage.getAbsoluteParent(1);
	if (defaultProductPage != null)
		defaultProduct = defaultProductPage.getName();

	final String analyticsPageSetValue = (analyticsPagesetTags != null) ? getTag(analyticsPagesetTags, tagManager) : "";
	final String analyticsProductValue = (analyticsProductTags != null) ? getTag(analyticsProductTags, tagManager) : defaultProduct;
	final String analyticsSubSectionValue = (analyticsSubSectionTags != null) ? getTag(analyticsSubSectionTags, tagManager) : "";
	String analyticsCountryValue = (analyticsCountryTags != null && analyticsCountryTags.length > 0) ? getTag(analyticsCountryTags, tagManager) : locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
	if ("ee".equalsIgnoreCase(analyticsCountryValue)) {
		analyticsCountryValue = "gb_eur";
	}
	final String analyticsLanguageValue = (analyticsLanguageTags != null) ? getTag(analyticsLanguageTags, tagManager) : locale.getLanguage();
	final String analyticsContentTypeValue = (analyticsContentTypeTags != null) ? getTag(analyticsContentTypeTags, tagManager) : "";
	final String analyticsPageSectionValue = (analyticsPageSectionTags!= null) ? getTag(analyticsPageSectionTags, tagManager) : "web";
%>
<script type="text/javascript">
	var utag_data = {
		<c:if test="<%=analyticsAbTest != null && !"".equals(analyticsAbTest) %>">ab_test:"<%=analyticsAbTest%>",</c:if>
		<c:if test="<%=analyticsAbBranch != null && !"".equals(analyticsAbBranch) %>">ab_branch:"<%=analyticsAbBranch%>",</c:if>
		pageset:"<%= analyticsPageSetValue %>",
		template:"<%= analyticsTemplate %>",
		product:"<%= analyticsProductValue %>",
		sub_section:"<%= analyticsSubSectionValue %>",
		website_country:"<%= analyticsCountryValue %>",
		language:"<%= analyticsLanguageValue %>",
		content_type:"<%= analyticsContentTypeValue %>",
		section:"<%= analyticsPageSectionValue %>",
		system_date_pst:"<%= analyticsSystemDatePSTValue %>",
		system_date_gmt:"<%= analyticsSystemDateGMTValue %>"
	};
</script>