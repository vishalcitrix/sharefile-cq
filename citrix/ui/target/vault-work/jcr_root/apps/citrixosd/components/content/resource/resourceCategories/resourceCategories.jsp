<%--
	Resource Categories
	
	Categories holds the list of resource pages. Authors will select one resource type and 
	this will render in the Resource javascript. The categories will also hold the categories 
	and limit of resources shown on the page. The limit function determines how pagination will 
	work if there is resources more than the limit value. Authors can set the categories as many 
	as they prefer, the results will be based on any resource matching any of those categories. 
	If there is no categories set, then this will retrieve all the resources matching the resource 
	type.
	
	Dependencies: Resource Container (Component), Resource (Javascript)

	achew@siteworx.com
--%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="java.util.List"%>
<%@page import="com.day.cq.tagging.TagManager"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
	private static final String TITLE_PROPERTY = "title";
	private static final String RESOURCE_TYPE_PROPERTY = "resourceType";
	private static final String CATEGORIES_PROPERTY = "categories";
	private static final String LIMIT_PROPERTY = "limit";
%>

<%
	final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	
	//Resource type
	final String[] resourceTypes = properties.get(RESOURCE_TYPE_PROPERTY, String[].class);
	final Tag resourceTypeTag = Utilities.getFirstTag(resourceTypes, tagManager);
	
	//Categories
	final String[] categories = properties.get(CATEGORIES_PROPERTY, String[].class);
	final List<Tag> categoryTags = Utilities.getTags(categories, tagManager);
	final StringBuilder builder = new StringBuilder();
	for(int i = 0; i < categoryTags.size(); i++) {
		final Tag categoryTag = categoryTags.get(i);
		builder.append(categoryTag.getTagID());
		builder.append(i == categoryTags.size() - 1 ? "" : ",");
	}
	final String categoriesStr = builder.toString();
	
%>

<c:set var="containerId" value="<%= containerId %>"/>
<c:set var="resourceTypeTag" value="<%= resourceTypeTag %>"/>
<c:set var="categoriesStr" value="<%= categoriesStr %>"/>

<div class="${resourceTypeTag.name}">
	<c:if test="${not empty properties['title']}">
		<div class="resource-categories-header">
			<div class="resource-categories-title">
				${properties['title']}
			</div>
			
			<div class="resource-categories-subtitle">
				${properties['subtitle']}
			</div>
			
			<div class="clearBoth"></div>
			
			<div class="resource-category-title-arrow-container">
				<span class="resource-category-title-arrow"></span>
			</div>
			
			<div class="resource-category-subtitle-arrow-container">
				<span class="resource-category-title-arrow"></span>
			</div>
			
		</div>
	</c:if>
	
	<div id="${containerId}" class="resource-type-container-${resourceTypeTag.name}" resource-categories-container="${containerId}" resource-type="${resourceTypeTag.name}" resource-categories="${categoriesStr}" resource-limit="${not empty properties['limit'] ? properties['limit'] : '1000'}" pagination-index="0" order-by="${properties['orderBy']}" webinar-upcoming-only="${properties['upcomingOnly']}">
		<%-- AJAX will populate this component --%>
	</div>
	
	<div id="${containerId}_pagination" class="pagination">
	
	</div>
</div>