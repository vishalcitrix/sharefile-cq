<%--
    Hero
    
    The component will consist of a background image, adjustable height, predefined widths and parsys.
    This should be used in a campaign manager template, such as a banner template because there is a 
    fixed container width defined in the css. Also keep in mine that the image should be the correct 
    height or else it will not cover the entire div in IE 7 & 8.
    
    Dependencies: Image Rendition, Parsys
    
    achew@siteworx.com
    
--%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private final static String FILE_REFERENCE_PROPERTY = "fileReference";
    private final static String RENDITION_PROPERTY = "rendition";
    private final static String CONTAINER_HEIGHT_PROPERTY = "containerHeight";
    private final static String STYLE_PROPERTY = "style";
    private final static String DISABLE_COVER_PROPERTY ="disableCover";
%>
<% 
    ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String url = properties.get(RENDITION_PROPERTY, properties.get(FILE_REFERENCE_PROPERTY, ""));
    String transformed = null;
    transformed = transformer.transform(url);
    if (transformed != null)
        url = transformed;
%>
<c:set var="filePath" value="<%= url%>"/>

<div class="hero-content-container">
    <div class="${properties.style eq 'pageWidth' ? 'container' : ''}" style="
				<c:if test="${not empty properties.bgcolor}">background-color:${properties.bgcolor};</c:if>
    			background-image:url('${filePath}');
    			<c:choose>
    				<c:when test="${properties.disableCover}">
    				
    				</c:when>
    				<c:otherwise>
    					background-size: cover;
    				</c:otherwise>
    			</c:choose>
                background-repeat:no-repeat;
				<c:choose>
					<c:when test="${not empty properties.imagePositionX && not empty properties.imagePositionY}">
						background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'};
					</c:when>
					<c:when test="${not empty properties.imagePositionX && empty properties.imagePositionY}">
						background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} 0;
					</c:when>
					<c:when test="${empty properties.imagePositionX && not empty properties.imagePositionY}">
						background-position: 0 ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'};
					</c:when>
					<c:otherwise>
						<%--Do not add background position --%>
					</c:otherwise>
				</c:choose>
                overflow:visible;
                margin: 0 auto;
                <c:if test="${!isEditMode && not empty properties.containerHeight}">height: ${properties.containerHeight}${fn:contains(properties.containerHeight, '%') ? '' : 'px'};</c:if>
                <c:if test="${empty properties.style || properties.style eq 'browserWidth'}">width: 100%;</c:if>
    ">
        <div class="container">
            <div class="hero-content">
                <cq:include path="par" resourceType="foundation/components/parsys"/>
            </div>
        </div>
    </div>
</div>