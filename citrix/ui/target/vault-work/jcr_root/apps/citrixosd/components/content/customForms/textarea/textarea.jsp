<%--

  Textfield component.
  Textfield for Custom Form
  
  vishal.gupta.82@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>

<%
    // TODO add you code here
    final String fieldId = properties.get("fieldId","");   
    final String fieldLabel = properties.get("fieldLabel","");   
    final String fieldName = properties.get("fieldName","");
    final boolean isRequired = properties.get("isRequired",false);
    final int rows = properties.get("rows",2);   
    final int cols = properties.get("columns",40);
    
    if (editContext != null && editContext.getEditConfig() != null) {
	    if(fieldName == ""){
			editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("TextArea Field - (Please select field name)"));
		}else{
			editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("TextArea Field - (name: " + fieldName + ")"));
		}
		  
		editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());	
    }
%>
    <c:set var="fieldLabel" value="<%= fieldLabel %>" />
    <c:set var="isRequired" value="<%= isRequired %>" />
    <c:set var="fieldId" value="<%= fieldId %>" />
    <c:set var="fieldName" value="<%= fieldName %>" />
    
    <c:if test="${fieldLabel ne ''}">
        <label for="${fieldName}">${fieldLabel}</label>
    </c:if>
	<c:choose>
        <c:when test="${fieldId ne ''}">
        	<c:choose>
		        <c:when test="${isRequired}">
		            <textarea class="required" rows="<%= rows %>" cols="<%= cols %>" id="<%= fieldId %>" name="<%= fieldName %>"><c:if test="${not empty param[fieldName]}">${param[fieldName]}</c:if></textarea> 
		        </c:when>
		        <c:otherwise>
		           	<textarea id="<%= fieldId %>" rows="<%= rows %>" cols="<%= cols %>" name="<%= fieldName %>"><c:if test="${not empty param[fieldName]}">${param[fieldName]}</c:if></textarea> 
		        </c:otherwise>  
		    </c:choose>
        </c:when>
        <c:otherwise>
        	<c:choose>
		        <c:when test="${isRequired}">
		            <textarea name="<%= fieldName %>" class="required" rows="<%= rows %>" cols="<%= cols %>"><c:if test="${not empty param[fieldName]}">${param[fieldName]}</c:if></textarea> 
		        </c:when>
		        <c:otherwise>
		           	<textarea name="<%= fieldName %>" rows="<%= rows %>" cols="<%= cols %>"><c:if test="${not empty param[fieldName]}">${param[fieldName]}</c:if></textarea> 
		        </c:otherwise>  
		    </c:choose>
        </c:otherwise>  
    </c:choose>