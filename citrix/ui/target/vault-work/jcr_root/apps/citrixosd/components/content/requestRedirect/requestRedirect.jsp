<%--
	Request Redirect
	
	Back end functionality to redirect the user based on the request parameter. The request 
	parameter is specified by the author and the value is a list of redirects based on the 
	redirect options. The value is checked by order of the list. The first value which matches 
	will redirect the page to the specific location specified by authors. The value can be a 
	regex to match the request parameter. Example. en(.*) will match en_US, en_GB, en_UK, etc.
	There is a default redirect if there is no matching results, this is optional and used to ensure 
	that no end-user will be able to see the content on that page. There will also be an option to 
	redirect the page if there is no request parameter.
	
	achew@siteworx.com
--%>

<%@page import="com.citrixosd.utils.LinkUtil"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String REQUEST_PARAMETER_PROPERTY = "requestParameter";
	private static final String REDIRECTS_PROPERTY = "redirects";
	private static final String REDIRECTS_VALUE_PROPERTY = "value";
	private static final String REDIRECTS_REDIRECT_PATH_PROPERTY = "redirectPath";
	private static final String DEFAULT_REDIRECT_PATH = "defaultRedirectPath";
%>

<%
	String redirectTo = null;

	final String requestParameter = properties.get(REQUEST_PARAMETER_PROPERTY, null);
	if(requestParameter != null) {
		ArrayList<Map<String, Property>> redirects = new ArrayList<Map<String, Property>>();
		if(currentNode != null && currentNode.hasNode(REDIRECTS_PROPERTY)) {
		    final Node baseNode = currentNode.getNode(REDIRECTS_PROPERTY);
		    redirects = Utilities.parseStructuredMultifield(baseNode);
		}
		
		//Check the request parameter to check we we can find a match to redirect
		final WCMMode mode = WCMMode.fromRequest(request);
		if(mode != null && (mode.equals(WCMMode.EDIT) || mode.equals(WCMMode.READ_ONLY))) {
			//Do nothing, author mode
			final String value = request.getParameter(requestParameter);
			if(value != null) {
				for(Map<String, Property> redirect : redirects) {
					if(value.matches(redirect.get(REDIRECTS_VALUE_PROPERTY).getString())) {
						final String redirectPath = redirect.get(REDIRECTS_REDIRECT_PATH_PROPERTY).getString();
						if(redirectPath != null && redirectPath.trim().length() > 0) {
							redirectTo = LinkUtil.getTransformedUrl(redirectPath, sling);
							break;
						}
					}
				}
			}
		}else {
			final String value = request.getParameter(requestParameter);
			if(value != null) {
				for(Map<String, Property> redirect : redirects) {
					if(value.matches(redirect.get(REDIRECTS_VALUE_PROPERTY).getString())) {
						final String redirectPath = redirect.get(REDIRECTS_REDIRECT_PATH_PROPERTY).getString();
						if(redirectPath != null && redirectPath.trim().length() > 0) {
							final String transformedURL = LinkUtil.getTransformedUrl(redirectPath, sling);
							response.sendRedirect(transformedURL);
							break;
						}
					}
				}	
			}
		}
	}
	
	//Default redirect if there is no request parameter
	final WCMMode mode = WCMMode.fromRequest(request);
	if(mode != null && (mode.equals(WCMMode.EDIT) || mode.equals(WCMMode.READ_ONLY))) {
		//Do nothing, author mode
		if(redirectTo == null) {
			redirectTo = properties.get(DEFAULT_REDIRECT_PATH, null);
		}
	}else {
		final String defaultRedirectPath = properties.get(DEFAULT_REDIRECT_PATH, null);
		if(defaultRedirectPath != null && defaultRedirectPath.trim().length() > 0) {
			final String transformedURL = LinkUtil.getTransformedUrl(defaultRedirectPath, sling);
			response.sendRedirect(transformedURL);
		}	
	}
%>

<c:set var="redirectTo" value="<%= redirectTo %>"/>

<c:if test="${isEditMode}">
	<div class="warning">Request Redirect: Parameter: <strong>${properties['requestParameter']}</strong> Value: <strong>${param[properties['requestParameter']]}</strong> Redirect: <strong>${redirectTo}</strong></div>
</c:if>