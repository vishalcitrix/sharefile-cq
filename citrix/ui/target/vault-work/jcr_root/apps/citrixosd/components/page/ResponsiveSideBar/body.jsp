<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%
    String pageMode = WCMMode.fromRequest(slingRequest).toString();
%>

<c:set var="pageMode" value="<%= pageMode %>" />


<body>    
    <div class="icon-close"></div>
    <div class="clearBoth"></div>
    <div id="slidebar-content">
        <cq:include path="sideBarContent" resourceType="foundation/components/parsys"/>
    </div>
</body>