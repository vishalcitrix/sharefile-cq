<%--
  Contact Sales component.
  
  Lets users create contact sales form
  
  vishal.gupta.82@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%!
    private final static String ELQ_FORM_NAME_PROPERTY = "elqFormName";
    private final static String OFFER_PROPERTY = "offer";
    private final static String LEAD_CHANNEL_PROPERTY = "leadChannel";
    private final static String LEAD_SOURCE_PROPERTY = "leadSource";
    private final static String LEAD_DETAILS_PROPERTY = "leadDetails";
    private final static String SALE_FORCE_ID_PROPERTY = "saleForceId";
    private final static String PRODUCT_PROPERTY = "product";
    private final static String BUYER_PERSONA_PROPERTY = "buyerPersona";
    private final static String THANK_YOU_PAGE_PROPERTY = "thankYouPage";
%>

<%
    final String eloquaFormName = properties.get(ELQ_FORM_NAME_PROPERTY, "");
    final String offer = properties.get(OFFER_PROPERTY,"");   
    final String leadChannel = properties.get(LEAD_CHANNEL_PROPERTY,"");
    final String leadSource = properties.get(LEAD_SOURCE_PROPERTY,"");
    final String leadDetails = properties.get(LEAD_DETAILS_PROPERTY,"");
    final String product = properties.get(PRODUCT_PROPERTY, "");
    final String buyerPersona = properties.get(BUYER_PERSONA_PROPERTY,"");
    final String thankYouPage = properties.get(THANK_YOU_PAGE_PROPERTY,"");
    String salesForceId = properties.get(SALE_FORCE_ID_PROPERTY,"");
    
    //Attempt to retrieve cache sales force id
    final String saleForceIdParam = request.getParameter("sfid");
    
    String visitorCookie = null;
    boolean visitorCookieSet = false;
    
    final Cookie[] cookies = request.getCookies();
    if(cookies != null){
      for(int i = 0; i < cookies.length; i++) {
          if(cookies[i].getName().equals("ercVisitor")) {
             final Cookie cookie = cookies[i]; 
             visitorCookie = cookie.getValue();
          }
		}
    }
    
    if(visitorCookie != null && !visitorCookie.trim().isEmpty()) {
        if(visitorCookie.indexOf("sf%252F") >= 0) {
            final int idIndex = visitorCookie.indexOf("sf%252F") + 7;
            visitorCookie = visitorCookie.substring(idIndex);
            visitorCookieSet = true;
            if (visitorCookie.indexOf("%252F") >= 0) {
            	visitorCookie = visitorCookie.substring(0, visitorCookie.indexOf("%252F"));
            } 
        }
    }
    
    if(visitorCookieSet) {
        salesForceId = visitorCookie;
    }else if(saleForceIdParam != null) {
        salesForceId = saleForceIdParam;
    }else {
    	//return default
    }
%>

<head>
	<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/contactSales.js"></script>	
    <script type="text/javascript" src="http://autocomplete.demandbase.com/autocomplete/v1.7/widget.js"></script>
	<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/demandbase.js"></script>
    <script type="text/javascript" src="http://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358&amp;callback=DemandGen.DemandBase.processIPInfo"></script>    
</head>


<form action="http://now.eloqua.com/e/f2.aspx" method="POST" async="false">
    <div class="form-box">
        <input type="hidden" name="elqFormName" value="<%= eloquaFormName %>">
        <input type="hidden" name="elqSiteID" value="607">
        <input type="hidden" name="elqCustomerGUID" value="">
        <input type="hidden" name="Offer" value="<%= offer %>">
        <input type="hidden" name="LeadChannel" value="<%= leadChannel %>">
        <input type="hidden" name="LeadSource" value="<%= leadSource %>">
        <input type="hidden" name="LeadDetails" value="<%= leadDetails %>">
        <input type="hidden" name="LeadAssignment" value="All">
        <input type="hidden" name="RL_EloquaTrigger" value="True">
        <input type="hidden" name="ID" value="<%= salesForceId %>">
        <input type="hidden" name="Product" value="<%= product %>">
        <input type="hidden" name="BuyerPersona" value="<%= buyerPersona %>">
        <input type="hidden" name="email_notification" value="True">
        <input type="hidden" name="member_status" value="responded">
        <input type="hidden" name="MarketingLP" value="<%= currentPage.getPath() %>">
        <input type="hidden" name="EmailOptOut" value="false">
        <input type="hidden" name="ThankYouPage" value="<%= thankYouPage %>">
        
        <h2 class="first"><fmt:message key="form.title.question1"/></h2>
        <label for="Howcanwehelp"><fmt:message key="form.label.question"/></label>
        <textarea id="Howcanwehelp" name="Howcanwehelp" class="validate" cols="42" rows="3"></textarea>
        
        
        <h2><fmt:message key="form.title.question2"/></h2>
        <ul>
            <li class="left-col">
                <label for="FirstName"><fmt:message key="form.label.firstName"/></label>
                <input id="FirstName" name="FirstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>" placeholder="<fmt:message key="form.placeholder.firstName"/>">
            </li>
            <li class="right-col">
                <label for="LastName"><fmt:message key="form.label.lastName"/></label>
                <input id="LastName"  name="LastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>" placeholder="<fmt:message key="form.placeholder.lastName"/>">
            </li>
            <li class="left-col">
                <label for="Email"><fmt:message key="form.label.email"/></label>
                <input id="Email" name="Email" type="text" class="required email" title="<fmt:message key="form.validate.email"/>">
            </li>
            <li class="right-col">
                <label for="Phone"><fmt:message key="form.label.phone"/></label>
                <input id="Phone" name="Phone" type="text" class="phone" title="<fmt:message key="form.validate.phone"/>">
            </li>
        </ul>
        
        
        <h2><fmt:message key="form.title.question3"/></h2>
        <label for="Company"><fmt:message key="form.label.company"/></label>
        <input id="Company" name="Company" type="text" class="required long-text">
        
        <ul>
            <li class="left-col">
                <label for="Industry"><fmt:message key="form.label.industry"/></label>
                <select id="Industry" name="Industry" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="Accounting"><fmt:message key="form.option.industry.accounting"/></option>
                    <option value="Advertising/Marketing/PR"><fmt:message key="form.option.industry.advertisingMarketingPR"/></option>
                    <option value="Aerospace &amp; Defense"><fmt:message key="form.option.industry.aerospaceAndDefense"/></option>
                    <option value="Call Center Outsourcing"><fmt:message key="form.option.industry.callCenterOutsourcing"/></option>
                    <option value="Consulting"><fmt:message key="form.option.industry.consulting"/></option>
                    <option value="Education"><fmt:message key="form.option.industry.education"/></option>
                    <option value="Energy, Chemical, Utilities"><fmt:message key="form.option.industry.energyChemicalUtilities"/></option>
                    <option value="Financial Services"><fmt:message key="form.option.industry.financialService"/></option>
                    <option value="Government - Federal"><fmt:message key="form.option.industry.governmentFederal"/></option>
                    <option value="Government - State &amp; Local"><fmt:message key="form.option.industry.governmentStateAndLocal"/></option>
                    <option value="Healthcare"><fmt:message key="form.option.industry.healthcare"/></option>
                    <option value="High Tech - Hardware"><fmt:message key="form.option.industry.highTechHardware"/></option>
                    <option value="High Tech - ISP"><fmt:message key="form.option.industry.highTechISP"/></option>
                    <option value="High Tech - Software"><fmt:message key="form.option.industry.highTechSoftware"/></option>
                    <option value="High Tech - Other"><fmt:message key="form.option.industry.highTechOther"/></option>
                    <option value="Hospitality/Travel/Tourism"><fmt:message key="form.option.industry.hospitalityTravelTourism"/></option>
                    <option value="Insurance"><fmt:message key="form.option.industry.insurance"/></option>
                    <option value="Legal"><fmt:message key="form.option.industry.legal"/></option>
                    <option value="Manufacturing"><fmt:message key="form.option.industry.manufacturing"/></option>
                    <option value="Pharmaceuticals &amp; Biotechnology"><fmt:message key="form.option.industry.pharmaceuticalsAndBiotechnology"/></option>
                    <option value="Real Estate"><fmt:message key="form.option.industry.realEstate"/></option>
                    <option value="Retail"><fmt:message key="form.option.industry.retail"/></option>
                    <option value="Support Outsourcing"><fmt:message key="form.option.industry.supportOutsourcing"/></option>
                    <option value="Telecommunications"><fmt:message key="form.option.industry.telecommunications"/></option>
                    <option value="Transportation"><fmt:message key="form.option.industry.transportation"/></option>
                    <option value="VAR/Systems Integrator"><fmt:message key="form.option.industry.VARSystemIntegrator"/></option>
                    <option value="Other"><fmt:message key="form.option.industry.other"/></option>
                </select>
            </li>
            <li class="right-col">
                <label for="NumberofEmployees"><fmt:message key="form.label.numberOfEmployees"/></label>
                <select name="NumberofEmployees" id="NumberofEmployees" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="1-10"><fmt:message key="form.option.numberOfEmployees.10"/></option>
                    <option value="11-20"><fmt:message key="form.option.numberOfEmployees.20"/></option>
                    <option value="21-50"><fmt:message key="form.option.numberOfEmployees.50"/></option>
                    <option value="51-100"><fmt:message key="form.option.numberOfEmployees.100"/></option>
                    <option value="101-250"><fmt:message key="form.option.numberOfEmployees.250"/></option>
                    <option value="251-500"><fmt:message key="form.option.numberOfEmployees.500"/></option>
                    <option value="501-1,000"><fmt:message key="form.option.numberOfEmployees.1000"/></option>
                    <option value="1,001-2,500"><fmt:message key="form.option.numberOfEmployees.2500"/></option>
                    <option value="2,501-5,000"><fmt:message key="form.option.numberOfEmployees.5000"/></option>
                    <option value="5,001-7,500"><fmt:message key="form.option.numberOfEmployees.7500"/></option>
                    <option value="7,501-10,000"><fmt:message key="form.option.numberOfEmployees.10000"/></option>
                    <option value="10,001+"><fmt:message key="form.option.numberOfEmployees.10001"/></option>
                </select>
            </li>
            <li class="left-col">
                <label for="Department"><fmt:message key="form.label.department"/></label>
                <select name="Department" id="Department" class="required">
                    <option value=""><fmt:message key="form.option.default"/></option>
                    <option value="Consulting"><fmt:message key="form.option.department.consulting"/></option>
                    <option value="Customer Service"><fmt:message key="form.option.department.customerService"/></option>
                    <option value="Engineering"><fmt:message key="form.option.department.engineering"/></option>
                    <option value="Finance/Legal/Administration"><fmt:message key="form.option.department.financialLegalAdministration"/></option>
                    <option value="Human Resources"><fmt:message key="form.option.department.humanResource"/></option>
                    <option value="IT"><fmt:message key="form.option.department.it"/></option>
                    <option value="Marketing/PR"><fmt:message key="form.option.department.marketingPR"/></option>
                    <option value="Operations"><fmt:message key="form.option.department.operations"/></option>
                    <option value="Professional Services"><fmt:message key="form.option.department.professionalService"/></option>
                    <option value="Sales/Business Development"><fmt:message key="form.option.department.salesBusinessDevelopment"/></option>
                    <option value="Training"><fmt:message key="form.option.department.training"/></option>
                    <option value="Other"><fmt:message key="form.option.department.other"/></option>
                </select>
            </li>
            <li class="right-col">
                <label for="Country"><fmt:message key="form.label.country"/></label>
                <select id="Country" name="Country" class="required">
                    <cq:include script="countryList.jsp"/>
                </select>
            </li>
        </ul>
        <div class="opt-in">
            <div class="checkbox"><input type="checkbox" name="MailingListOptIn" value="true" checked></div>
            <div><fmt:message key="form.label.subscribe"/></div>
        </div>
        <input type="submit" value="<fmt:message key="form.label.submit"/>" class="button green" title="<fmt:message key="form.label.submit"/>">
    </div>
</form>
