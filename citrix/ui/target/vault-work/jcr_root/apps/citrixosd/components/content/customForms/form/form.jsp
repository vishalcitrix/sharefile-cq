<%--

    Form component.
    
    This component allows authors to create custom forms as per their requirements.
    Authors can define input fields, landing page, submit button text etc.
    
    vishal.gupta.82@citrix.com

--%>

<%@ page import="com.day.cq.wcm.api.WCMMode"%>
<%@ page import="com.adobe.granite.xss.XSSAPI"%>
<%@include file="/libs/foundation/global.jsp"%>

<%
    final String formClass = properties.get("formClass","");
    final String formMethod = properties.get("formMethod","POST");
    final String formId = properties.get("formID","");
    final String formName = properties.get("formName","");
    final String actionPath = properties.get("formAction","");
    final String formButton = properties.get("formButton","Submit");
    final String jscode = properties.get("jscode","");
    final String csscode = properties.get("csscode","");
    final String buttonColor =  properties.get("buttonColor","orange");
    final String formErrorMsg =  properties.get("formErrorMsg","");
%>
<c:set var="formClass" value="<%= formClass %>"/> 
<c:set var="jscode" value="<%= jscode %>"/> 
<c:set var="cssCode" value="<%= csscode %>"/>
<c:set var="formErrorMsg" value="<%= formErrorMsg %>"/>

    <c:if test="${not empty jscode}">
        <script type="text/javascript">
            ${jscode}
        </script>
    </c:if>
    <c:if test="${not empty cssCode}">
        <style type="text/css">
             ${cssCode}
        </style>
    </c:if>

<c:choose>
    <c:when test="${not empty formClass}">
        <div class="custom-form ${formClass}">
    </c:when>
    <c:otherwise>
        <div class="custom-form">
    </c:otherwise>  
</c:choose>
    <c:if test="${not empty formErrorMsg}">
        <div id="formErrorMsg" class="form-error-msg"><span class="icon-Warning"></span>${formErrorMsg}</div>
    </c:if>
    <form action="<%= actionPath %>" name="<%=  formName %>" id="<%=  formId %>" method="<%= formMethod %>">
            <div class="form-box">
                <div class="form-row">
                    <cq:include path="formfield" resourceType="foundation/components/parsys" />
                </div>
                
                <div class="custom-form-button-container">
                    <input class="button <%= buttonColor %>" type="submit" value="<%= formButton %>">
                    
                    <%
                        WCMMode mode = WCMMode.fromRequest(request);
                            if (mode.equals(WCMMode.EDIT)) {
                    %>
                                <div class="warning"></div>
                    <%
                            }
                    %>
                    
                    <c:if test="${properties.showResetButton == true}">
                        <input class="reset" type="reset" value="${not empty properties.resetButton ? properties.resetButton : 'Reset'}"/>
                    </c:if>
                    <div class="clearBoth"></div>
                </div>
                <cq:include path="extraformfield" resourceType="foundation/components/parsys" />
            </div>
    </form>
</div>