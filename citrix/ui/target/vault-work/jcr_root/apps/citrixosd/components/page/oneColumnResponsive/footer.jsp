<%@page import="java.util.Locale"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
%>
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="useUnityNav" value="<%= SiteUtils.getPropertyFromSiteRoot("useUnityNav",currentPage) %>" />

<c:if test="${empty useUnityNav}">
    <cq:include script="slidebar.jsp"/>
</c:if>

<cq:include script="lightbox.jsp"/>

<div id="footerSection">
	<cq:include path="footerSiteMap" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="footerLinks" resourceType="swx/component-library/components/content/single-ipar"/>
</div>
<c:set var="altStatic" value=""/>
<c:if test="${not empty properties.altStatic}"><c:set var="altStatic" value="/${properties.altStatic}"/></c:if>
<%-- Javascripts - Place after including jQuery Lib --%>
<c:if test="${empty properties.altStatic}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script></c:if>
<%-- DNT --%>
<%
    Locale locale = null;
    final Page localePage = currentPage.getAbsoluteParent(2);
    if (localePage != null) {
        try {
            locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
        } catch (Exception e) {
            locale = request.getLocale();
        }
    }
    else
        locale = request.getLocale();
%>
<script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
<script type="text/javascript">
	dnt.setMsgClass('footer-dnt');
	dnt.setMsgLocId('footer-dnt');
	dnt.setLocale('<%=locale.toString()%>');
	dnt.dntInit();
</script>
<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>

<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
   <script async="" type="text/javascript" src="${designPath}/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>

<%-- Include Parallax JS (To Do: add in properties section)--%>
<c:if test="${not empty properties.showParallax}">
    <script async="" type="text/javascript" src="<%= currentDesign.getPath() %>/js/parallax.js"></script>
</c:if>
<c:if test="${not (empty properties.altStatic || isEditMode || isReadOnlyMode)}">
    <c:if test="${empty useUnityNav}">
        <script type="text/javascript">
            function domInit() {
                if(typeof menuScrollBar === "object") {
                    menuScrollBar.init();
                }
            }
            domInit();
        </script>
    </c:if>
</c:if>
<c:set var="globalJS" value="<%= SiteUtils.getPropertyFromSiteRoot("globaljs",currentPage) %>" />
<c:if test="${not empty globalJS and empty properties.disableGlobalJs}">
    <script src="${globalJS}" type="text/javascript"></script>
</c:if>
<c:if test="${not empty properties.customPageJs}">
    <script src="${properties.customPageJs}" type="text/javascript"></script>
</c:if>
<cq:include script="geoSegmentation.jsp"/>