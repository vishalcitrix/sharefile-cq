<%--

  Footer Button component.

  This is the Footer Button in GotomyPC

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String CSS_PROPERTY = "css";
    public static final String LABEL_PROPERTY = "label";
    public static final String PATH_PROPERTY = "path";
	public static final String LINK_OPTION_PROPERTY = "linkOption";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, "cross-sell") %>"/>
<c:set var="label" value="<%= properties.get(LABEL_PROPERTY, "") %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, "") %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>

<c:choose>
	<c:when test="${css eq 'none'}">

	</c:when>
	
	<c:otherwise>
		<c:choose>
			<c:when test="${not empty linkOption}">
			    <a class="${css}" href="${path}" rel="${linkOption}">
			    <c:if test="${not empty label}">
			    	<fmt:message key="${label}" />
			    </c:if>&nbsp;<span></span></a>
			</c:when>
			<c:otherwise>
				<a class="${css}" href="${path}" rel="${linkOption}">
			    <c:if test="${not empty label}">
			    	<fmt:message key="${label}" />
			    </c:if>&nbsp;<span></span></a>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>