<%--
  Case Study Container
  
  The 'Case Study Container' is a container designated for encasing and grouping 'Case Study' 
  components by tags and is meant to be utilized within the 'Case Study Filter' component. It 
  contains a Tag Input Field used to add a tag to this component. The Tags for this component is limited 
  to the 'Case Studies' Namespace. Also, no more than one tag can be placed on this component. 
  
  kguamanquispe@siteworx.com
  
--%>

<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<% 
	TagManager tagManager = resource.getResourceResolver().adaptTo(TagManager.class);
	Property tag = currentNode.hasProperty("tag") ? currentNode.getProperty("tag"): null;
	Tag  caseTag = null;
	boolean isAuthorInstance = false;
    Object authorInstanceObj = pageContext.getAttribute("isAuthorInstance");
 
    if (authorInstanceObj != null)
        isAuthorInstance = (Boolean)authorInstanceObj;

	
	if(tag != null){
		 caseTag = tagManager.resolve((tag.getValues().length > 0) ? tag.getValues()[0].getString() : null);
 		 
		 if(caseTag != null && isAuthorInstance){
		 	tagManager.setTags(resource,(new Tag[]{ caseTag }));
 		 }
 	}	
%>
<c:set var="tag" value="<%=caseTag%>"/>

<div id="${tag.name}">
	<h3> <%=(caseTag != null ? caseTag.getTitle(currentPage.getLanguage(false)) : "Enter Case Study" ) %> </h3>
	<cq:include path="parsys" resourceType="foundation/components/parsys"/>
	<span class="return-top gotop"><span class="caret">^ </span> <a href="#"><fmt:message key="return.to.top"/></a></span>
</div>
