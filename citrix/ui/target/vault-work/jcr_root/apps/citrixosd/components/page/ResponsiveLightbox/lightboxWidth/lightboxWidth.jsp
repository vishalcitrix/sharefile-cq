<%@ page contentType="text/html; charset=utf-8"%> 
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String CSS_WIDTH_CLASS = "lightboxWidth";
    public static final String CSS_HEIGHT_CLASS = "lightboxHeight";
%>

<c:set var="lightboxWidth" value="<%= properties.get(CSS_WIDTH_CLASS, "") %>"/>
<c:set var="lightboxHeight" value="<%= properties.get(CSS_HEIGHT_CLASS, "") %>"/>

    <div id="lightbox-container" style="display: block;">
        <div id="lightbox-border" class="${lightboxWidth}">
            <div id="lightbox-close"></div>
            <div id="lightbox-content" class="${lightboxWidth} ${lightboxHeight}">       
                <cq:include path="lightboxContent" resourceType="foundation/components/parsys"/>                   
            </div>
        </div>
    </div>

