<%--
    Background Color Component
    
    Container component which holds the css class for the background color and can be dimensions of each 
    color can be altered in CSS.
    
    achew@siteworx.com
    vishal.gupta@citrix.com - Added option to specify different color
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:choose>
    <c:when test="${not empty properties['userColor']}">
        <div style = "background-color: ${properties['userColor']};">
    </c:when>
    <c:otherwise>
        <div class = ${properties['color']}>
    </c:otherwise>
</c:choose>

    <cq:include script="content.jsp"/>
</div>
