<%--
	Feature Spotlight
	
    This spotlight consist of title, subtitle, and content text on the left side. On the right 
    side is the background image component. This component will also allow one component to be 
    place under the content. The height property will take override the content height but will not 
    use height if height was set less than 0 pixel.
    
    Note: To overflow the image top or right, height and width is required.
    
    Dependencies: Background Image, Image Rendition(dialog)
    
    achew@siteworx.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String TITLE_PROPERTY = "title";
	private static final String SUBTITLE_PROPERTY = "subtitle";
	private static final String CONTENT_PROPERTY = "content";
	private static final String HEIGHT_PROPERTY = "height";
%>

<c:set var="title" value="<%= properties.get(TITLE_PROPERTY, "title") %>"/>
<c:set var="subtitle" value="<%= properties.get(SUBTITLE_PROPERTY, null) %>"/>
<c:set var="content" value="<%= properties.get(CONTENT_PROPERTY, null) %>"/>
<c:set var="height" value="<%= properties.get(HEIGHT_PROPERTY, 0) %>"/>

<div style="<c:if test="${not empty height ? (height > 0 ? true : false) : false}">height: ${height}px</c:if>">
	<div style="float:right;" class="image-width">
		<swx:setWCMMode mode="READ_ONLY">
			<cq:include path="rightImage" resourceType="swx/component-library/components/content/backgroundImage"/>
		</swx:setWCMMode>
	</div>
	
	<h3>${title}</h3>
	<c:if test="${not empty subtitle}"><h4>${subtitle}</h4></c:if>
	<c:if test="${not empty content}"><p>${content}</p></c:if>
	
	<div class="additionalContent">
		<cq:include path="par" resourceType="swx/component-library/components/content/single-par"/>
	</div>
	
	<div class="clearBoth"></div>
</div>