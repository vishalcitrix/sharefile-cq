<%--
    Share List
  
    List of shares where authors can select from to display the functionality of a 
    service. If there is any value blank, it will not display. If text is not provided
    then there will not be any text but the icon/share will still have the functionality. 
    The shares will display in a horizontal position with the order if shares respectively.
  
    Dev: The share object holds all the data that gets transfered from the JSON object. The 
    object is then used for JSTL and EL to display the logic.
    
    - (optional)Share CSS
    - (optional)Share Text
    - (optional)Link Path
    - (optional)Pop-up
    
    achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Arrays"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private final static String SHARE_PROPERTY = "shares";
	private final static String TITLE_PROPERTY = "title";
    
    public class Share {
        private String css;
        private String text;
        private String path;
        private String linkOption;
    
        public Share() {
        }
        
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) {
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<Share> getShares(NodeIterator nodeIter) {
        final List<Share> shares = new ArrayList<Share>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currShare = nodeIter.nextNode();
                    final Share share = new Share();
                    share.setCss(currShare.hasProperty("css") ? currShare.getProperty("css").getString() : null);
                    share.setText(currShare.hasProperty("text") ? currShare.getProperty("text").getString() : null);
                    share.setPath(currShare.hasProperty("path") ? currShare.getProperty("path").getString() : null);
                    share.setLinkOption(currShare.hasProperty("linkOption") ? currShare.getProperty("linkOption").getString() : null);
                    if(share.getCss() != null || share.getText() != null || share.getPath() != null || share.getLinkOption() != null) {
                    	shares.add(share);
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }
%>

<%
	List<Share> shares = null;
	if (currentNode != null && currentNode.hasNode(SHARE_PROPERTY)) {
	    final Node baseNode = currentNode.getNode(SHARE_PROPERTY);
	    final NodeIterator nodeIter = baseNode.getNodes();
	    shares = getShares(nodeIter);
	}

	final Map<String, Object> shareList = new HashMap<String, Object>();
    shareList.put(SHARE_PROPERTY, shares);
    shareList.put(TITLE_PROPERTY, properties.get(TITLE_PROPERTY, null));
%>

<c:set var="shareList" value="<%= shareList %>"/>

<c:choose>
    <c:when test="${fn:length(shareList.shares) > 0 || not empty shareList.title}">
    	<c:if test="${not empty shareList.title}">
			<span class="title"><fmt:message key="${shareList.title}"/></span>
		</c:if>
    
        <ul class="sharelist">
            <c:forEach items="${shareList.shares}" begin="0" var="share">
                <li>
                    <c:choose>
                        <c:when test="${empty share.path}">
                        	<c:if test="${not empty share.css}">
                        		<span class="share ${share.css}"></span>
                        	</c:if>
                        </c:when>
                        <c:otherwise>
                        	<c:choose>
                        		<c:when test="${not empty share.linkOption && not empty share.text}">
                        			<a rel="${share.linkOption}" href="${share.path}" title="${share.text}">							
	                        			<c:if test="${not empty share.css}">
				                			<span class="share ${share.css}"></span>
				                		</c:if>
			                		</a>	
                        		</c:when>
                        		<c:when test="${not empty share.linkOption && empty share.text}">
                        			<a rel="${share.linkOption}" href="${share.path}">
	                        			<c:if test="${not empty share.css}">
				                			<span class="share ${share.css}"></span>
				                		</c:if>
			                		</a>                        			
                        		</c:when>
                        		<c:when test="${empty share.linkOption && not empty share.text}">
                        			<a href="${share.path}" title="${share.text}">
	                        			<c:if test="${not empty share.css}">
				                			<span class="share ${share.css}"></span>
				                		</c:if>
			                		</a>                        			
                        		</c:when>   
                        		<c:otherwise>                    			
                        			<a href="${share.path}">
	                        			<c:if test="${not empty share.css}">
				                			<span class="share ${share.css}"></span>
				                		</c:if>
			                		</a>                        											
								</c:otherwise>
							</c:choose>						
                        </c:otherwise>
                    </c:choose>
                </li>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
		<c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Social List: Please enter a sprite</div></c:if>
    </c:otherwise>
</c:choose>
