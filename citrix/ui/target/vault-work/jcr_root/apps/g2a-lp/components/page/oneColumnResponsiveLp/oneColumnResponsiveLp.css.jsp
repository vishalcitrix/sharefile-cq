<%--
	CSS: scans current node an children for css
--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page trimDirectiveWhitespaces="true"%>
<%   
   String allStyles = getAllNodeStyles(currentNode);
%>
<c:set var="allStyles" value="<%= allStyles %>"/>
${allStyles}
<%!
    public String getAllNodeStyles(Node someNode) throws RepositoryException {
      NodeIterator iter = someNode.getNodes();
      String styles = "";

      while (iter.hasNext()) {
        Node itemNode = iter.nextNode();
        /* debug
        if (itemNode.hasProperty("jcr:primaryType")) {
          styles = styles + itemNode.getName() + "<br>";
        } */
        
        // devonly
        if (itemNode.hasProperty("csscode")) {
          styles = styles + itemNode.getProperty("csscode").getString();
        }

        if(itemNode.hasNodes()){
          styles = styles + getAllNodeStyles(itemNode);
        }
      }

      return styles;
    }
%>