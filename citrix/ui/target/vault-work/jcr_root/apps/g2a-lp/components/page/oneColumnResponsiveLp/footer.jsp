<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>


<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
    String jsUrl = (String)request.getAttribute("currentPageJsUrl");
%>
<c:set var="jsUrl" value="<%= jsUrl %>" />
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/lp/common.js"></script>
<cq:include script="lightbox.jsp"/>
<div class="footer">	       
    <div class="row">
	<div class="small-2 medium-2 large-2 columns">
	    <div class="logo leftLogo leftLogoNew">
	       <span class="gotoassistLogo" rel=""></span>	
	    </div>
       </div>
        <div class="small-10 medium-8 large-8 columns ">
	     <cq:include path="footer-g2a-lp" resourceType="foundation/components/parsys"/>
        </div>        
        <div class="small-12 medium-2 large-2 columns ">
	    <div class="logo rtSideLogo">
	      <a rel="external" href="http://www.citrix.com"><span class="citrix"></span></a>
	    </div>
        </div>
    </div>
</div>
<c:if test="${not empty jsUrl}">
    <script type="text/javascript" src="<%= jsUrl %>"></script>
</c:if>

<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>
<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
<script type="text/javascript" src="https://sadmin.brightcove.com/js/APIModules_all.js"></script>
<script type="text/javascript" src="https://files.brightcove.com/bc-mapi.js"></script>

<script type="text/javascript">
	function sideBar() {
		if(typeof slideBar != "undefined" && typeof slideBar.defaultHide == "function")
			slideBar.defaultHide();
		else
			setTimeout(sideBar,200);
	}
	sideBar();
</script>