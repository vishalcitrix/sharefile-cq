<%--
  Try form component.

  vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode,
	com.citrixosd.utils.ContextRootTransformUtil,
	com.citrixosdRedesign.constants.ResourceConstants,
	com.citrixosd.utils.Utilities"
%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:set var="rightImg" value="<%= ContextRootTransformUtil.transformedPath(properties.get("rightFileReference",""),request) %>" />
<c:set var="leftImg" value="<%= ContextRootTransformUtil.transformedPath(properties.get("leftFileReference",""),request) %>" />
<c:set var="formCss" value="<%= properties.get("cssClass","") %>"/>
<c:set var="jsCode" value="<%= properties.get("jsCode","") %>"/>
<c:set var="region" value="<%= properties.get("currency","US") %>"/>
<c:set var="catalog" value="<%= properties.get("catalog","") %>"/>
<c:set var="planKey" value="<%= properties.get("planKey","") %>"/>
<c:set var="promoCode" value="<%= properties.get("defaultPromo","") %>"/>
<c:set var="language" value="<%= properties.get("flowLanguage","en") %>"/>
<c:set var="country" value="<%= properties.get("country","US") %>"/>
<c:set var="locale" value="<%= Utilities.getLocale(currentPage,request).toString() %>"/>
<c:set var="mailingOptIn" value="<%= properties.get("mailingOptin","setFalse") %>"/>
<c:set var="showPhoneField" value="<%= properties.get("showPhoneField",false) %>"/>
<c:set var="phoneFieldRequired" value="<%= properties.get("phoneFieldRequired",false) %>"/>
<c:set var="showPassword" value="<%= properties.get("showPassword",false) %>"/>
<c:set var="successURL" value="<%= properties.get("successURL","") %>"/>
<c:set var="submitText" value="<%= properties.get("submitText","") %>"/>
<c:set var="errorDisplay" value="<%= properties.get("errorDisplay","prepend") %>"/>
<c:set var="useFallback" value="<%= properties.get("useFallback",false) %>"/>
<c:set var="submitSubText" value="<%= properties.get("submitSubText","") %>"/>
<c:set var="setNocc" value="<%= properties.get("setNocc",false) %>"/>
<c:set var="errorUserExistsText" value="<%= properties.get("errorUserExistsText","") %>"/>
<c:set var="errorUserExistsUrl" value="<%= properties.get("errorUserExistsUrl","") %>"/>
<%-- --%>
<c:set var="showOptIn" value="false"/>
<c:set var="optInValue" value="false"/>
<c:if test="${mailingOptIn == 'setTrue'}">
	<c:set var="optInValue" value="true"/>
</c:if>
<c:if test="${mailingOptIn == 'showUnchecked'}">
	<c:set var="showOptIn" value="true"/>
	<c:set var="optInValue" value="false"/>
</c:if>
<c:if test="${mailingOptIn == 'showChecked'}">
	<c:set var="showOptIn" value="true"/>
	<c:set var="optInValue" value="true"/>
</c:if>
<c:choose>
	<c:when test="${isDev || isQA}">
			<c:set var="server" value="secureed1"/>
	</c:when>
	<c:when test="${isStage}">
		<c:set var="server" value="stage.secure"/>
	</c:when>
	<c:when test="${isProd}">
		<c:set var="server" value="secure"/>
	</c:when>
	<c:otherwise>
		<c:set var="server" value="secureed1"/>
	</c:otherwise>
</c:choose>

<c:if test="${not empty leftImg}">
	<div <c:if test="${not empty properties.leftImageMargin}">style="margin:${properties.leftImageMargin};"</c:if> class="leftImageContainer"><img src="${leftImg}"></div>
</c:if>
<c:if test="${not empty rightImg}">
	<div <c:if test="${not empty properties.rightImageMargin}">style="margin:${properties.rightImageMargin};"</c:if> class="rightImageContainer"><img src="${rightImg}"></div>
</c:if>

<div class="registerFormContainer form-container ${formCss}">
	
	<form id="tryForm" name="tryForm" action="https://${server}.citrixonline.com/commerce/buy" method="POST" <c:if test="${useFallback}">data-useFallback="true"</c:if> <c:if test="${errorDisplay == 'prepend'}">class="error-prepend"</c:if> <c:if test="${errorDisplay == 'right'}">class="error-right"</c:if>>
		<div>
			<input type="hidden" name="region" value="${region}">
			<input type="hidden" name="catalog" value="${catalog}">
			<input type="hidden" name="planKey" value="${planKey}">
			<input type="hidden" name="planKeys" value="${planKey}">
			<input type="hidden" name="promotion" value="${promoCode}">
			<input type="hidden" name="language" value="${language}">
			<input type="hidden" name="locale" value="${locale}">
			<input type="hidden" name="country" value="${country}">
			<c:if test="${not showOptIn}"><input type="hidden" name="mailingListOptIn" value="${optInValue}"></c:if>
			<input type="hidden" name="validpromo" value="">
			<input type="hidden" name="isNocc" value="${setNocc}">
			<input type="hidden" name="uuid" value="">
			<input type="hidden" name="ipAddress" value="">
			<input type="hidden" name="successUrl" value="${successURL}">
			<input type="hidden" name="submit-text" value="${submitText}">
			<c:if test="${not empty errorUserExistsText}"><input type="hidden" name="errorUserExistsText" id="errorUserExistsText" value="${errorUserExistsText}"></c:if>
			<c:if test="${not empty errorUserExistsUrl}"><input type="hidden" name="errorUserExistsUrl" id="errorUserExistsUrl" value="${errorUserExistsUrl}"></c:if>
		</div>
		<div class="form-head">
			<cq:include path="tryHead" resourceType="foundation/components/parsys" />
		</div>
		<div id="tryform-error-target"></div>
		<c:choose>
			<c:when test="${not empty properties.maxSeatValue}">
				<select id="quantities" name="quantities">
					<option value="1">1 ${properties.singularSeat}</option>
					<c:forEach begin="2" end="${properties.maxSeatValue}" var="loop">
					    <option value="${loop}">${loop} ${properties.pluralSeat}</option>
					</c:forEach>
				</select>
				<cq:include path="trySubHead" resourceType="foundation/components/parsys" />
			</c:when>
			<c:otherwise>
				<input type="hidden" name="quantities" value="${properties.defaultSeatValue}">
			</c:otherwise>
		</c:choose>
		<ul>
			<li>
				<div class="icon-person">
					<input id="FirstName" name="firstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>" placeholder="<fmt:message key="form.placeholder.firstName"/>" maxlength="60">
				</div>
			</li>
			<li>
				<div class="icon-person">
					<input id="LastName" name="lastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>" placeholder="<fmt:message key="form.placeholder.lastName"/>" maxlength="60">
				</div>
			</li>
			<li>
				<div class="icon-mail">
					<input id="Email" name="emailAddress" type="text" class="required email" title="<fmt:message key="form.validate.email"/>" placeholder="<fmt:message key="form.placeholder.email"/>">
				</div>
			</li>
			<c:if test="${showPhoneField}">
				<li>
					<div class="icon-phone-1">
						<input id="daytimePhone" name="daytimePhone" type="text" class="phone <c:if test="${phoneFieldRequired}">required</c:if>" title="<fmt:message key="form.validate.phone"/>" placeholder="<fmt:message key="form.placeholder.phone"/>" maxlength="20">
					</div>
				</li>
			</c:if>
			<c:if test="${showPassword}">
				<li id="formPassword">
					<div class="icon-fontawesome-webfont-3">
						<input id="password" name="password" type="password" class="required password" title="<fmt:message key="form.validate.password"/>" placeholder="<fmt:message key="form.placeholder.password"/>">
					</div>
				</li>
			</c:if>
		</ul>
		<c:if test="${showOptIn}">
		<div class="checkbox">
			<input type="checkbox" name="mailingListOptIn" value="true" <c:if test="${optInValue}">checked</c:if>><span><fmt:message key="${mailingListOptin}"/></span>
		</div>
		</c:if>
		<!--  adding button -->
		<div class="form-submit button-container">
			<div class="loading-mask"></div>
			<input type="submit" value="${submitText}" class="button largeorangebuttonborder" title="${submitText}">
			<c:if test="${not empty submitSubText}"><label for="formSubmit" class="button-text ${submitHeight}"><fmt:message key="${submitSubText}"/></label></c:if>
		</div>

		<!--  add buy link -->
		<c:if test="${not empty properties.buyLinkLabel}">
			<div class="buyLinkContainer">
				<a href="${properties.buyLinkURL}">
					${properties.buyLinkLabel}
				</a>
		    </div>
		</c:if>

		
		<div class="bottomSubText">
			<cq:include path="bottomSubText" resourceType="foundation/components/parsys" />
		</div>
		
	</form>
</div>
<div class="clearBoth"></div>

<script type="text/javascript" src="https://${server}.citrixonline.com/secure/scripts/commerce/restClientAPI-v2.js"></script>
<script type="text/javascript">
	var commServer = "https://${server}.citrixonline.com";
<c:choose>
	<c:when test="${isProd}">
	var merchantId = "g2aexpress";
	var orgId = "k8vif92e";
	</c:when>
	<c:otherwise>
	var merchantId = "citrixonline_test";
	var orgId = "1snn5n9w";
	</c:otherwise>
</c:choose>
	${jsCode}
</script>