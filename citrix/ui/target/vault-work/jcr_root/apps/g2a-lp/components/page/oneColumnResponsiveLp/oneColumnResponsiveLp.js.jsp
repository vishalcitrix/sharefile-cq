<%--
	JS: scans current node an children for JS code
--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page trimDirectiveWhitespaces="true"%>
<%   
   String jsCode = getAllNodeJsCode(currentNode);
%>
<c:set var="jsCode" value="<%= jsCode %>"/>
${jsCode}
<%!
    public String getAllNodeJsCode(Node someNode) throws RepositoryException {
      NodeIterator iter = someNode.getNodes();
      String code = "";

      while (iter.hasNext()) {
        Node itemNode = iter.nextNode();
        /* debug
        if (itemNode.hasProperty("jcr:primaryType")) {
          code = code + itemNode.getName() + "<br>";
        } */
        
        // devonly
        if (itemNode.hasProperty("jscode")) {
          code = code + itemNode.getProperty("jscode").getString();
        }

        if(itemNode.hasNodes()){
          code = code + getAllNodeJsCode(itemNode);
        }
      }

      return code;
    }
%>