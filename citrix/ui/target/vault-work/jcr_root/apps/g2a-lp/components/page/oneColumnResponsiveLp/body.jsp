<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
    <div id="content-body">
		<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<cq:include path="banner" resourceType="swx/component-library/components/content/single-par"/>
        <cq:include path="stickyHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include path="mainContent" resourceType="foundation/components/parsys"/> 
	</div>  
    <cq:include script="footer.jsp" />
</body>
