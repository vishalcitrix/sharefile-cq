<%@ page import="com.day.cq.personalization.ClientContextUtil" %>
<%@ page import="com.citrixosdRedesign.constants.ResourceConstants" %>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final String uniqueRequestParameter = ClientContextUtil.getId(resource.getPath());
    final String[] selectors = sling.getRequest().getRequestPathInfo().getSelectors();
    if(selectors != null) {
        for(int i = 0; i < selectors.length; i++) {
            switch(i) {
                case 0:
                    //Do nothing this is the selector for pagination
                    break;
                case 1:
                    request.setAttribute(uniqueRequestParameter + "-page-index", selectors[i]);
                    break;
                case 2:
                    //Do nothing this is the selector for query
                    break;
                case 3:
                    //Do nothing this is the query for products
                    break;
                case 4:
                    request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.PRODUCTS, selectors[i]);
                    break;
                case 5:
                    //Do nothing this is the query for categories
                    break;
                case 6:
                    request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.CATEGORIES, selectors[i]);
                    break;
                case 7:
                    //Do nothing this is the query for featured
                    break;
                case 8:
                    request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.FEATURED, selectors[i]);
                    break;
                case 9:
                    //Do nothing this is the query for topics
                    break;
                case 10:
                    request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.TOPICS, selectors[i]);
                    break;
                case 11:
                    //Do nothing this is the query for industries
                    break;
                case 12:
                    request.setAttribute(uniqueRequestParameter + "-" + ResourceConstants.INDUSTRIES, selectors[i]);
                    break;
                default:
                    //Do nothing it's not implemented
                    break;
            }
        }
    }else {
        request.setAttribute(uniqueRequestParameter, "0");
    }
%>

<%-- Avoid SEO from indexing paginated pages. --%>
<head>
    <meta name="robots" content="noindex">
    <%-- Redirect the end user if they hit the paginated page --%>
    <script>
        if(typeof $ === "undefined") {
            window.location = "${currentPage.path}";
        }
    </script>
</head>

<cq:include script="searchResults.jsp"/>
