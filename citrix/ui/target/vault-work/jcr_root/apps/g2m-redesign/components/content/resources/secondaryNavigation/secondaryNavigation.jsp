<%--
    Secondary Navigation
    vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final ArrayList<Map<String, Property>> navigationLinks = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode("navigationLinks")) {
        navigationLinks.addAll(Utilities.parseStructuredMultifield(currentNode.getNode("navigationLinks")));
        for (int i = 0; i < navigationLinks.size(); i++) { 
            Map<String, Property> hm = navigationLinks.get(i);
            Property path = (Property) hm.get("path");
            String transformedPath = ContextRootTransformUtil.transformedPath(path.getString(),request);
            path.setValue(transformedPath);
            hm.put("path",path);
        }
    }
%>

<c:set var="navigationLinks" value="<%= navigationLinks %>"/>
<c:set var="currentPage" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getPath(),request) %>"/>
<c:set var="parentPath" value="<%= ContextRootTransformUtil.transformedPath(currentPage.getParent().getPath(),request) %>"/>
<c:set var="parentPathDepth" value="<%= currentPage.getParent().getDepth() %>"/>

<div class="rowC columns ${properties.theme}">
<div>
    <div class="left">
        <span class="title">${not empty properties.title ? properties.title : currentPage.title}</span>
    </div>
    <div class="clearfix"></div>
</div>
    <nav class="columns large-9 medium-9 small-9">
        <ul class="show-for-large-up">
            <c:forEach items="${navigationLinks}" var="navigationLink">
                <c:choose>
                    <c:when test="${currentPage eq navigationLink.path.string}">
                        <li class="selected"><a href="#">${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:when>
                    <c:when test="${parentPath eq navigationLink.path.string && parentPathDepth > 4}">
                        <li><a href="${navigationLink.path.string}" ${navigationLink.externalLink.string eq true ? "rel='external'" : ""}>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${navigationLink.path.string}" ${navigationLink.externalLink.string eq true ? "rel='external'" : ""}>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
        <div class="hide-for-large-up">
            <select class="cLink">
                <c:forEach items="${navigationLinks}" var="navigationLink">
                    <c:choose>
                        <c:when test="${currentPage eq navigationLink.path.string || (parentPath eq navigationLink.path.string && parentPathDepth > 5)}">
                                <option value="${navigationLink.path.string}" selected>${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${navigationLink.path.string}">${not empty navigationLink.text.string ? navigationLink.text.string : 'Empty Text'}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>
    </nav>
    <c:if test="${not empty properties.showSearch}">
        <div class="columns large-3 medium-3 small-2">
            <cq:include path="search" resourceType="citrixosd/components/content/search"/>
        </div>
    </c:if>
</div>