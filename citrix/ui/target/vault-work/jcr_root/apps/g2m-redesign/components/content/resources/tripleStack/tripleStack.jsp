<%--
     Triple Stack component.

    
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode,
                com.day.cq.personalization.ClientContextUtil"%>

<%!
    public static final String COLOR_SCHEME_PROPERTY = "colorScheme";
%>
<%
    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>
<c:set var="colorScheme" value="<%=properties.get(COLOR_SCHEME_PROPERTY, "g2m")%>"/>

<c:if test="${pageMode eq 'EDIT'}">
    <style>
    .stacker {
        height: 183px !important;
    }
    </style>
</c:if>
<div class="triple-container ${colorScheme}">
    <ul class="triple-wrap">

            <c:forEach var="idx" begin="1" end="3">
            <li class="stacker singleStack-${idx}">
                <cq:include path="panel${idx}" resourceType="g2m-redesign/components/content/resources/tripleStack/singleStack"/>
            </li>
            </c:forEach>

    </ul>
</div>
<hr/>    
 <div class="clearBoth"></div>