<%--
  Try Form component
  - Form with action, field text and theme options.
--%>

<%@include file="/libs/foundation/global.jsp"%>
<c:set var="formLocale" value="<%= properties.get("locale","en_US") %>"/>
<form id="freeTrial" class="email-box" action="${properties.buttonURL}" method="get">
	<div class="trial-box ${properties.borderLayout ? 'border-container' :''}"<c:if test="${not empty properties.borderColor}"> style="background: ${properties.borderColor}"</c:if>>
		<div>
			<input type="hidden" name="locale" value="${formLocale}" >
<%
			if (currentNode.hasNode("hiddenFields")) {
				final Node fieldNode = currentNode.getNode("hiddenFields");
				NodeIterator iter = fieldNode.getNodes();
				while (iter.hasNext()) {
					Node fieldItemNode = iter.nextNode();
					if (fieldItemNode.hasProperty("name") && fieldItemNode.hasProperty("value")) {
						%><input type="hidden" name="<%= fieldItemNode.getProperty("name").getString() %>" value="<%= fieldItemNode.getProperty("value").getString() %>"><%
					}
				}
			}
%>
		</div>
		<cq:include path="primaryText" resourceType="/apps/g2m-redesign/components/content/text" />
		<div class="row">
			<div class="column small-centered ${properties.borderLayout ? 'small-12' :'small-10'}">
				<div class="${properties.borderLayout ? 'small-12 medium-7 large-7' :'small-7'} columns icon-mail">
					<input type="text" id="emailAddress" Name="emailAddress" placeholder="${properties.emailPlaceHolder}" class="prefill show-for-large-up">
					<input type="text" id="emailAddress" Name="emailAddress" placeholder="${properties.emailPlaceHolderSmall}" class="prefill show-for-small-only">
					<input type="text" id="emailAddress" Name="emailAddress"  placeholder="${properties.emailPlaceHolderSmall}" class="prefill show-for-medium-only">
					<!--  add extra text in form -->
					<c:if test="${(properties.borderLayout)}">
					 	<cq:include path="extraTextfirst" resourceType="/apps/g2m-redesign/components/content/text" />
					</c:if>
				</div>
				<div class="${properties.borderLayout ? 'small-12 medium-5 large-5' :'small-5'} columns">
					<input type="submit" class="${properties.theme} show-for-large-up" value="${properties.buttonLabel}">
					<input type="submit" class="${properties.theme} show-for-small-only" value="${properties.buttonLabelSmall}">
					<input type="submit" class="${properties.theme} show-for-medium-only" value="${properties.buttonLabelSmall}">
					<!--  add extra text in form -->
					<c:if test="${(properties.borderLayout)}">
						<cq:include path="extraTextSecond" resourceType="/apps/g2m-redesign/components/content/text" />
					</c:if>
				</div>
			</div>
		</div>
		<c:if test="${(not properties.borderLayout)}">
			<div class="row nocc">
				<div class="column small-centered small-10">
					${properties.textContent}
				</div>
			</div>
		</c:if>
	</div>
</form>
<div class="clearBoth"></div>