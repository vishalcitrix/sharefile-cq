<%--
  	Social Nav component.
	- This is a Social Side bar Nav component, which contains menu for editing the Social share details like summary and URL of the sharing link
	
	vishal.gupta@citrix.com
	ingrid.tseng@citrix.com
--%>

<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:choose>
    <c:when test="${not (isEditMode || isDesignMode)}"> 
    	<c:if test="${empty properties.disableSocialNav}">    
    		<ul class="left-nav <c:if test='${not empty properties.disableSocialNavMobile}'>hide-for-small-only</c:if>"
    			data-social-nav-user-alt-color="${properties.useAlternativeColor}"
    			data-social-nav-alt-color-section="${properties.altColorSection}"
    			data-social-nav-alt-color="${properties.alternativeColor}">
	            <li class="navbuttons"><span class="icon-top" id="followUp"></span></li>
	            <li id="shareOptions">
	                <a href="#" class="icon-share" id="share"></a>
	                <ul class="shareOpt">
	                    <li class="icons-box" style="position:relative; overflow:hidden;">
	                        <ul id="shareOpt">
	                            <li class="fb"><a href="${properties.facebook}" class="icon-facebook" rel="popup"></a></li>
	                            <li class="tw"><a href="${properties.twitter}" class="icon-monitor" rel="popup"></a></li>
	                            <li class="gplus"><a href="${properties.gplus}" class="icon-gplus" rel="popup"></a></li>
	                            <li class="mail"><a href="${properties.mailto}" class="icon-mail"></a></li>
	                        </ul>
	                    </li>
	                </ul>
	            </li>
	            <div id="lpButton-LeftBubble"></div>
	            <li class="navbuttons"><span class="icon-down" id="followDown"></span></li>
	        </ul>
    	</c:if> 
    </c:when>
    <c:otherwise>
        <div class="warning" style="position:inherit !important;">
            Social Nav
        </div>
    </c:otherwise>
</c:choose>