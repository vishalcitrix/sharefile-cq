<%--
    Search Results

    achew@siteworx.com
--%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%@ page import="org.apache.jackrabbit.commons.JcrUtils" %>
<%@ page import="com.day.cq.commons.jcr.JcrConstants" %>

<%@include file="/apps/citrixosd/global.jsp" %>
<cq:setContentBundle/>

<%
    //Create itself if it is not already created because this is required for filtering
    if(currentNode == null && WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
        final Session session = resourceResolver.adaptTo(Session.class);
        final Node newNode = JcrUtils.getOrCreateByPath(resource.getPath(), JcrConstants.NT_UNSTRUCTURED, session);
        newNode.setProperty("sling:resourceType", component.getResourceType());
        session.save();
    }
%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="resourceList" value="<%= ResourceUtils.getResourceList(resource, currentPage, request, request.getParameter("q"), currentDesign) %>" scope="request"/>

<div id="${resourceList.id}">
    <cq:include script="content.jsp"/>
</div>