<%--
  Webinar Teaser component.
  @author Michael Jostmeyer
--%><%
%><%@page import="org.apache.sling.api.SlingHttpServletRequest"%><%
%><%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.citrixosd.resources.ResourceUtils"%>
<%@page import="com.citrixosd.resources.models.ResourceCategoryeModel"%>
<%@page import="com.citrixosd.enums.ResourceType"%>
<%@page import="java.util.Properties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="javax.jcr.Value"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.citrixosd.utils.TranslationUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@include file="/apps/citrixosd/global.jsp" %><%

final String headline = properties.get("headline", "");
final String footer = properties.get("footer", "");
final String staticWebinarPath = properties.get("staticWebinar", "");
String path = currentPage.getAbsoluteParent(2).getPath();
Node webinar = ResourceUtils.getUpcomingWebinar(
    path,
    slingRequest,
    properties.get("useLatest","").equals("true")
);

if(staticWebinarPath != null && !staticWebinarPath.isEmpty()){
    webinar = ResourceUtils.getDefaultWebinar(staticWebinarPath, slingRequest);
}


String webinarTitle = "";
String webinarDate = "";
String webinarDescription = "";
String webinarRegisterPath = "";
String webinarImagePath = "";
if(webinar == null){
    String defaultWebinarParentPath = properties.get("defaultWebinar", "");
    webinar = ResourceUtils.getDefaultWebinar(defaultWebinarParentPath, slingRequest);
}
if(webinar.hasProperty("title")) {
    webinarTitle = webinar.getProperty("title").getString();
}
if(webinar.hasProperty("description")) {
    webinarDescription = webinar.getProperty("description").getString();
}
if(webinar.hasProperty("registerPath")) {
    webinarRegisterPath = webinar.getProperty("registerPath").getString();
}
if(webinar.hasProperty("fileReference")) {
    webinarImagePath = webinar.getProperty("fileReference").getString();
}
if(webinar.hasProperty("date")) {
    Locale locale = Utilities.getLocale(currentPage,request);
    DateFormat longDateFormatter = DateFormat.getDateInstance(DateFormat.LONG, locale);
    Date upcomingDate = webinar.getProperty("date").getDate().getTime();
    webinarDate = webinar.hasProperty("date") ? longDateFormatter.format(upcomingDate) : null;
}
%>
<c:set var="headline" value="<%= headline %>"/>
<c:set var="footer" value="<%= footer %>"/>
<c:set var="webinarTitle" value="<%= webinarTitle %>"/>
<c:set var="webinarDate" value="<%= webinarDate %>"/>
<c:set var="webinarDescription" value="<%= webinarDescription %>"/>
<c:set var="webinarRegisterPath" value="<%= webinarRegisterPath %>"/>
<c:set var="webinarImagePath" value="<%= webinarImagePath %>"/>

<div class="webinar-teaser-responsive">
  <div class="text parbase section">
      <h4>${headline}</h4>
  </div>
  <div class="col-control section">
      <div class="row ">
          <div class="large-4 medium-6 columns">
              <div class="parsys colPar-0 colparsys">
                  <div class="text parbase section">
                      <p><a href="${webinarRegisterPath}" rel="external" target="_blank"><img src="${webinarImagePath}" alt=""></a></p>
                  </div>
              </div>
          </div>
          <div class="large-8 medium-6 columns">
              <div class="parsys colPar-1 colparsys">
                  <div class="text parbase section">
                      <p class="webinar-teaser-date">${webinarDate}</p>
                      <p class="webinar-teaser-title">${webinarTitle}</p>
                      <p class="webinar-teaser-description">${webinarDescription}</p>
                  </div>
                  <div class="button section">
                      <div class="button-container">
                          <a class="button border-only-expandable matching-text-color" href="${webinarRegisterPath}" rel="external" target="_blank"><fmt:message key="webinar.label.register" /></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="webinar-teaser-footer">
          <p>${footer}</p>
      </div>
  </div>
  <div class="clearBoth"></div>
</div>
