<%--
  Contact Sales component - G2M.
  vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Locale"%>
<%@page import="com.adobe.granite.xss.XSSAPI"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

    
    <c:set var="leadGeo" value="<%= properties.get("leadGeo", false) %>"/>
    <c:set var="cssCode" value="<%= properties.get("csscode", "") %>"/>
    <c:set var="jscode" value="<%= properties.get("jscode","") %>"/> 
    <c:set var="buttonColor" value="<%= properties.get("buttonColor","orange") %>"/> 
    
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/validate.js"></script>
    <script type="text/javascript" src="${currentDesign.path}/js/utils/contactsales.js"></script>
    <c:if test="${not isEditMode}">
        <script type="text/javascript" src="//autocomplete.demandbase.com/autocomplete/widget.js"></script>
    </c:if>
    <script type="text/javascript" src="${currentDesign.path}/js/demandBase.js"></script>
    <script type="text/javascript" src="//api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358&amp;callback=DemandGen.DemandBase.processIPInfo"></script>
        
    <c:if test="${not empty jscode}">
        <script type="text/javascript">${jscode}</script>
    </c:if>
    <c:if test="${not empty cssCode}">
        <style type="text/css">${cssCode}</style>
    </c:if>
    
    <!--  adding cookie evaluator -->
    <cq:include script="cookieEval.jsp"/>
    
<div class="contact-sales">
	<div class="form-header">
	    <cq:include path="content" resourceType="swx/component-library/components/content/single-ipar" />
	</div>
            
    <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" onSubmit="contact(this,${leadGeo});" name="contactSales" class="validate">
	    <!--  adding hidden fields (inheriting)-->
	    <cq:include script="hiddenFields.jsp"/>
	    <c:if test="${not empty saleTaskSubject}">
	        <input type="hidden" name="sfdc_task_subject" value="${saleTaskSubject}">
	    </c:if>
	
	    <h3><fmt:message key="contactSales.needs.header"/></h3>
	
	    <ul>
	        <li>
	            <div class="icon-person">
	                <input id="FirstName" name="FirstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>" placeholder="<fmt:message key="form.placeholder.firstName"/>">
	            </div>
	        </li>
	        <li>
	            <div class="icon-person">
	                <input id="LastName"  name="LastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>" placeholder="<fmt:message key="form.placeholder.lastName"/>">
	            </div>
	        </li>
	        <li>
	            <div class="icon-mail">
	                <input id="Email" name="Email" type="text" class="required email" title="<fmt:message key="form.validate.email"/>" placeholder="<fmt:message key="form.placeholder.email"/>">
	            </div>
	        </li>
	         <li>
	            <div class="icon-phone-1">
	                <input id="Phone" name="Phone" type="text" class="required phone" title="<fmt:message key="form.validate.phone"/>" placeholder="<fmt:message key="form.placeholder.phone"/>">
	            </div>
	        </li>
	        <li>
	            <div class="icon-city">
	                <input id="Company" name="Company" type="text" class="required" title="<fmt:message key="form.placeholder.company"/>" placeholder="<fmt:message key="form.placeholder.company"/>">
	            </div>
	        </li>
	         <li class="selectBox">
	            <!-- adding department list -->
	            <label for="Department__c">
	            <cq:include script="departmentList.jsp"/></label>
	        </li>
	        <li class="selectBox">
	            <!-- adding industry list -->
	            <label for="Industry">
	            <cq:include script="industryList.jsp"/></label>
	        </li>
	        <li class="selectBox">
	            <!-- adding no. of employee list -->
	            <label for="NumberofEmployees">
	            <cq:include script="employeeList.jsp"/></label>
	        </li>
	        <li class="selectBox">
	        <label for="Country">
	            <select id="Country" name="Country" class="required" title="<fmt:message key="form.validate.required"/>">
	                <!-- adding country list -->
	                <cq:include script="countryList.jsp"/>
	            </select>
	            </label>
	        </li>
	    </ul>
	                
	    <!-- adding collaboration checkboxes (inheriting)-->
	    <cq:include script="isCollaboration.jsp"/>
	  
	    <!--  adding form comments (inheriting)-->
	    <cq:include script="formComments.jsp"/>
	    
	    <div class="opt-in checkboxes">
	        <span class="checkbox left"><label><input type="checkbox" id = "privacy-policy" name="privacy-policy"><span></span></label></span>
	        <cq:include path="privacypolicy" resourceType="/apps/g2m-redesign/components/content/text" />
	    </div>
	                
	    <input type="submit" value="<fmt:message key="form.label.submit"/>" class="button ${buttonColor}" title="<fmt:message key="form.label.submit"/>">
	    
	    <!--  add extra text in form -->
	    <cq:include path="extraText" resourceType="/apps/g2m-redesign/components/content/text" />
    </form>
</div>