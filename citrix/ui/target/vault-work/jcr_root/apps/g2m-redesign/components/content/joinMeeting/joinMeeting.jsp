<%-- Join Meeting --%>

<%@include file="/libs/foundation/global.jsp" %>

<%!
  public static final String JOIN_MEETING="join-meeting" ;
  public static final String HOST_MEETING="host-meeting" ;
  public static final String SCHEDULE_MEETING="schedule-meeting" ;
  public static final String WEBINAR="join-form-g2w" ;
  public static final String TRAINING="join-form-g2t" ;
  public static final String MEETING="join-form-g2m" ;
  public static final String ALIGNMENT_RIGHT="right" ;
%>
<c:set var="join_meeting" value="<%=JOIN_MEETING%>" />
<c:set var="host_meeting" value="<%=HOST_MEETING%>" />
<c:set var="schedule_meeting" value="<%=SCHEDULE_MEETING%>" />
<c:set var="right" value="<%=ALIGNMENT_RIGHT%>" />
<c:set var="webinar" value="<%=WEBINAR%>" />
<c:set var="training" value="<%=TRAINING%>" />
<c:set var="meeting" value="<%=MEETING%>" />

       <c:if test="${properties.meetingType eq join_meeting}">
         <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/validate.js"></script>
         <script type="text/javascript" src="${currentDesign.path}/js/utils/joinMeeting.js"></script>

             <div class="join-form join-meeting-padding ${properties.theme} ${properties.colorScheme}">
               <form class="validate" data-action="${properties.joinURL}" action="${properties.joinURL}" onsubmit="${properties.theme eq meeting ? 'GlobalFlow' : 'clearHyphens'}(this, '${properties.theme eq webinar ? 'publicId' : properties.theme eq training ? 'publicId' :'meetingId'}' );" method="post">
                   <div class="medium-10 large-12 ${properties.ContentAlignment eq right ? 'align-right' : ''}">

                       <c:if test="${(properties.theme eq meeting)}">
                              <div class="small-4 medium-4 columns large-text-center">
                                  <label class="icon-login" for="meetingId">${properties.inputTextLabel}</label>
                              </div>

                              <div class="small-5 medium-6 columns large-text-center">
                                  <input type="text" id="meetingId" name="meetingId" class="required eventId" title="${properties.errorMsg}" placeholder="${properties.idPlaceHolder}">
                              </div>

                              <div class="small-2 medium-2 columns join-button text-center">
                                  <label class="btn-go" for="joinContinue">
                                      <input id="joinContinue" type="submit">
                                  </label>
                              </div>
                        </c:if>
                       <c:if test="${(properties.theme eq webinar) or (properties.theme eq training) }">

                        <div class="small-12 medium-12 large-12 columns large-text-center">
                            <label class="icon-login" for="publicId">${properties.inputTextLabel}</label>
                        </div>
                        <div class="small-12 medium-12 large-12 columns large-text-center">
                            <div class="small-10 medium-5 large-4 columns" >
                                <input type="text" id="publicId" name="publicId" class="required eventId" title="${properties.errorMsg}" placeholder="${properties.idPlaceHolder}">
                            </div>
                            <div class="small-10 medium-5 large-6 columns emailAddress" >
                                <input type="text" id="email" name="email" class="required  email" title="${properties.emailErrorMsg}" placeholder="${properties.emailPlaceHolder}">
                            </div>
                            <div class="small-2 medium-2 large-2  columns join-button text-center">
                              <label class="btn-go" for="joinContinue">
                                <input id="joinContinue" type="submit">
                              </label>
                            </div>
                        </div>
                    </c:if>
                   </div>
               </form>
               <div class="clearBoth"></div>
           </div>
       </c:if>
       <c:if test="${properties.meetingType eq host_meeting || properties.meetingType eq schedule_meeting}">
           <div class="join-form join-meeting-padding ${properties.theme} ${properties.colorScheme} host-meeting">
               <div class="medium-10 large-12">
                   <div class="small-8 medium-10 large-9 columns large-text-center">
                       <label class="<c:if test="${properties.meetingType eq host_meeting}">icon-users</c:if><c:if test="${properties.meetingType eq schedule_meeting}">icon-clock</c:if>" for="meetingID">${properties.inputTextLabel}</label>
                   </div>
                   <div class="small-2 large-3 columns join-button text-center">
                       <a class="btn-go" href="${properties.joinURL}"></a>
                   </div>
               </div>
           </div>
       </c:if>