
<%--
    Tab

    This will hold either reference to another page on the site, or it will handle javascript
    show and hide feature to display the content. Default will send the users to another page.
    This will also contain a parsys so authors can put content in the tab. If javascript is selected
    the content will be split into as many links as the author specified.

    There is a checkbox to make any tab active.

    suresh.gupta@citrix.com
    vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String ACTION_PROPERTY = "action";
    public static final String TABS_PROPERTY = "tabs";
%>

<%
    ArrayList<Map<String, Property>> tabs = null;
    if(currentNode != null) {
        if(currentNode.hasNode(TABS_PROPERTY)) {
            tabs = Utilities.parseStructuredMultifield(currentNode.getNode(TABS_PROPERTY));
        }
    }

    final String generatedContainerId = ClientContextUtil.getId(resource.getPath()).replaceAll("-", "");
%>

<c:set var="tabs" value="<%= tabs %>"/>
<c:set var="generatedContainerId" value="<%= generatedContainerId %>"/>
<c:set var="tabStyle" value="<%= properties.get("tabStyle","fullWidth") %>"/>

<div id="${generatedContainerId}" class="tab-container">
    <c:choose>
        <c:when test="${properties['action'] eq 'renderInJavascript'}">
            <ul class="tab-link-container${tabStyle eq 'fixedWidth' ? ' tab-fixed-width' : ''}">
             <c:set var="found" value="false" />
                <c:forEach items="${tabs}" var="tab" varStatus="i">
                    <c:set var="first" value="" />
                    <c:if test="${i.first}" >
                        <c:set var="first" value="first" />
                    </c:if>
                    <c:set var="className" value="" />
                    <c:if test="${tab.isActive.boolean}" >
                        <c:set var="className" value="active" />
                    </c:if>
                    <li class="${first} ${className}">
                        <c:if test="${tabStyle eq 'fixedWidth'}">
                            <a href="#${not empty tab.anchor ? tab.anchor : ''}" class="tab-link" rel="tab" tab-container-id-link="${generatedContainerId}" index="${i.count}"><span>${tab.text.string}</span></a>
                        </c:if>
                        <c:if test="${tabStyle ne 'fixedWidth'}">
                            <a href="#${not empty tab.anchor ? tab.anchor : ''}" class="show-for-medium-up ${tab.sprite.string}" rel="tab" tab-container-id-link="${generatedContainerId}" index="${i.count}"><span>${tab.text.string}</span></a>
                            <a href="#${not empty tab.anchor ? tab.anchor : ''}" class="show-for-small-only ${tab.sprite.string}" rel="tab" tab-container-id-link="${generatedContainerId}" index="${i.count}"><span>${tab.text.string}</span></a>
                        </c:if>
                    </li>
                </c:forEach>
            </ul>
            <div class="clearBoth"></div>

            <c:forEach items="${tabs}" var="tab" varStatus="i">
                <c:if test="${isEditMode}"><div class="warning">Tab: Starting tab [${i.count}] container</div></c:if>
                    <c:set var="className" value="hide" />

                    <c:if test="${tab.isActive.boolean}" >
                        <c:set var="className" value="active" />
                    </c:if>

                    <div class="tab-content-container ${className}"  rel="tab" tab-container-id-content="${generatedContainerId}" index="${i.count}">
                        <cq:include path="tab_${i.count}-content${tabStyle eq 'fixedWidth' ? '' : ' tab-content'}" resourceType="foundation/components/parsys"/>
                    </div>
                <c:if test="${isEditMode}"><div class="warning">Tab: End tab [${i.count}] container</div></c:if>
            </c:forEach>
        </c:when>

        <c:otherwise>
            <ul class="tab-link-container">
                <c:forEach items="${tabs}" var="tab" varStatus="i">
                    <li class="${classes}">
                    <c:if test="${tabStyle eq 'fixedWidth'}">
                        <a href="${tab.path.string}" class="tab-link" rel="tab"><span>${tab.text.string}</span></a>
                    </c:if>
                    <c:if test="${tabStyle ne 'fixedWidth'}">
                        <a href="${tab.path.string}" class=" show-for-medium-up ${tab.sprite.string}"  rel="tab"><span>${tab.text.string}</span></a>
                        <a href="${tab.path.string}" class=" show-for-small-only ${tab.sprite.string}"  rel="tab"><span>${tab.text.string}</span></a>
                    </c:if>
                    </li>
                </c:forEach>
            </ul>
            <div class="clearBoth"></div>
            <c:if test="${isEditMode}"><div class="warning">Tab: Starting tab container</div></c:if>
                <div>
                    <cq:include path="tab-content" resourceType="foundation/components/parsys"/>
                </div>
            <c:if test="${isEditMode}"><div class="warning">Tab: End tab container</div></c:if>
        </c:otherwise>
    </c:choose>
</div>