<%--
    Sliding Menu - Link List
    
    A collection of links for sliding menu which can have customizable properties.
    Depends on slideBarMenu.js
    
    For pages that should be shown in parent pages, select parent as parent page.
    
    vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String LINKS_PROPERTY = "links";

    public class Link {
        private String css;
        private String text;
        private String path;
        private String parent;
        private String toolTip;
        private String linkOption;
        
        public Link() {
            
        }
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setParent(String parent) { 
            this.parent = parent;
        }
        public String getParent() {
            return this.parent;
        }
        public void setToolTip(String toolTip) { 
            this.toolTip = toolTip;
        }
        public String getToolTip() {
            return this.toolTip;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<Link> getLinks(NodeIterator nodeIter) {
        final List<Link> shares = new ArrayList<Link>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final Link link = new Link();
                    link.setCss(currLink.hasProperty("css") ? currLink.getProperty("css").getString() : "");
                    link.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : "");
                    link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : "");
                    link.setParent(currLink.hasProperty("parent") ? currLink.getProperty("parent").getString() : "");
                    link.setToolTip(currLink.hasProperty("toolTip") ? currLink.getProperty("toolTip").getString() : "");
                    link.setLinkOption(currLink.hasProperty("linkOption") ? currLink.getProperty("linkOption").getString() : "");
                    
                    if(link.getText() != null || link.getPath() != null) {
                        shares.add(link);   
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }

%>

<%
    List<Link> links = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
    }
    
    final Map<String, Object> linkList = new HashMap<String, Object>();
    linkList.put(LINKS_PROPERTY, links);
    
    //get absolute parent
    String parentPath = "";
    
    if(currentPage.getDepth() == 3) {
        parentPath = currentPage.getAbsoluteParent(2).getPath();
    }else if (currentPage.getDepth() > 3) {
        parentPath = currentPage.getAbsoluteParent(3).getPath();
    }
    
    //hack because en_US is actually /meeting 
    parentPath = parentPath.replace("/meeting", "");
    
%>

<c:set var="linkList" value="<%= linkList %>"/>
<c:set var="parentPath" value="<%= parentPath %>"/>

<c:choose>
    <c:when test="${fn:length(linkList.links) > 0}">
        <ul>
            <c:forEach items="${linkList.links}" var="link">
                <c:choose>
                    <c:when test="${not empty link.parent}">
                        <c:if test="${link.parent eq currentPage.path}">
                            <li><a href="${not empty link.path ? link.path : ''}" class="sub"><fmt:message key="${link.text}"/></a></li>
                        </c:if>
                    </c:when>
					<c:when test="${(not empty link.toolTip) && (parentPath ne link.path)}">
                    	<c:set var="cssParts" value="${fn:split(link.css, ' ')}" />
                        <li class="tips">
                        	<a href="${not empty link.path ? link.path : ''}" ${link.path eq currentPage.path ? 'onclick="return false;"' : ''} class="${link.path eq currentPage.path ? 'current' : ''}">
                                <span class="${link.css}"></span>
                                <fmt:message key="${link.text}"/>
                            </a>
                            <div class="tipsContainer tipsData" >
                                <div class="tip-arrow"></div>
                                <span class="show-for-medium-down"><a class="icon-up"></a></span>
                                <p class="desc">${link.toolTip}</p>
                                <p class="learnmore ${cssParts[0]}"><a href="${link.path}"><fmt:message key="learn.more"/><span>&#9654;</span></a></p>
                            </div>
                        </li>
					</c:when>
                    <c:otherwise>
                    	<li>
				        	<c:if test="${not empty link.path}">
					        		<c:if test="${not empty link.linkOption}">
	                    				<c:choose>
											<c:when test="${link.linkOption eq '_blank'}">
												<a href="${link.path}" class="${link.path eq currentPage.path ? 'current' : ''} ${link.css eq 'sub' ? 'sub' : ''}" target="${link.linkOption}">
											</c:when>
											<c:otherwise>
											    <a href="${link.path}" class="${link.path eq currentPage.path ? 'current' : ''} ${link.css eq 'sub' ? 'sub' : ''}" rel="${link.linkOption}">
											</c:otherwise>
										</c:choose>
									</c:if>
									<c:if test="${empty link.linkOption}">
										<a href="${link.path}" class="${link.path eq currentPage.path ? 'current' : ''} ${link.css eq 'sub' ? 'sub' : ''}">
									</c:if>
									<c:if test="${link.css ne 'sub'}">
		                           		<span class="${link.css}"></span>
		                           	</c:if>
									<fmt:message key="${link.text}"/>
                            	</a>
							</c:if>
                            <c:if test="${empty link.path}">
                            	<a href="#" class="${link.css}"><fmt:message key="${link.text}"/></a>
                            </c:if>
                 		</li>
               		</c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>

<!-- More content other than links -->
<cq:include path="sideBarContent" resourceType="foundation/components/parsys"/> 