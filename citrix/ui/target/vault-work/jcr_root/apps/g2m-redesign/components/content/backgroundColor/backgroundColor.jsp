<%--
    Background Color Component
    - Container component which holds the css class for the background color and can be dimensions of each
      color can be altered in CSS.

    vishal.gupta@citrix.com - Added option to specify different color
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:choose>
    <c:when test="${not empty properties['userColor']}">
    	<c:choose>
	    	<c:when test="${not empty properties['userColorGradient']}">
		        <div style = "background-color: ${properties['userColor']};
		        			  background: -o-linear-gradient(top, ${properties['userColor']} 50%, ${properties['userColorGradient']} 100%);
							  background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, ${properties['userColorGradient']}));
							  background: -webkit-linear-gradient(top, ${properties['userColor']} 50%, ${properties['userColorGradient']} 100%);
							  background: -moz-linear-gradient(50% 100% 90deg, ${properties['userColorGradient']}, ${properties['userColor']});
							  background: -ms-linear-gradient(top, ${properties['userColor']} 50%, ${properties['userColorGradient']} 100%);
							  background: linear-gradient(to bottom, ${properties['userColor']} 50%, ${properties['userColorGradient']} 100%);
							  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='${properties['userColor']}',
							  endColorstr='${properties['userColorGradient']}', GradientType=0);"
		        >
		    </c:when>
		    <c:otherwise>
		        <div style = "background-color: ${properties['userColor']};">
		    </c:otherwise>
	    </c:choose>
    </c:when>
    <c:otherwise>
        <div class = ${properties['color']}>
    </c:otherwise>
</c:choose>

    <cq:include script="content.jsp"/>
</div>
