<%--
	Image Renditions
	- vishal.gupta@citrix.com
--%>

<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@page import="com.day.cq.wcm.foundation.Image"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>

<%! 
    private static final String FILE_REFERENCE_PROPERTY = "fileReference";
    private static final String TABLET_FILE_REFERENCE_PROPERTY = "tabletfileReference";
    private static final String MOBILE_FILE_REFERENCE_PROPERTY = "mobilefileReference";
    private static final String RENDITION_PROPERTY = "rendition";
%>

<c:set var="bgImage" value="<%= ContextRootTransformUtil.transformedPath(properties.get("fileReference", ""),request) %>" />
<c:set var="desktopsrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(FILE_REFERENCE_PROPERTY, "")),request) %>" />
<c:set var="tabletsrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(TABLET_FILE_REFERENCE_PROPERTY, "")),request) %>" />
<c:set var="mobilesrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(MOBILE_FILE_REFERENCE_PROPERTY, "")),request) %>" />

<c:set var="desktopstyle" value="background-position: center; background-size: cover;height:531px; background-image:url('${desktopsrc}') " />
<c:if test="${empty tabletsrc}">
    <c:set var="tabletsrc" value="${desktopsrc}" />
</c:if>
<c:set var="tabletstyle" value="background-position: center; background-size: cover;height:531px;background-image:url('${tabletsrc}')"/>
<c:if test="${empty mobilesrc}">
    <c:set var="mobilesrc" value="${desktopsrc}" />
</c:if>
<c:set var="mobilestyle" value="background-position: center; background-size: cover;height:531px;background-image:url('${mobilesrc}')" />

<c:choose>
	<c:when test="${not empty properties.fileReference}">
		<div class="styleswitch"  style="background-image:url('${bgImage}');${desktopstyle}" data-large="${desktopstyle}" data-medium="${tabletstyle}" data-small="${mobilestyle}">     </div>
	</c:when>
	<c:otherwise>
		<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) %>">
			<img class="cq-image-placeholder" src="/libs/cq/ui/resources/0.gif">
		</c:if>
		<%-- Do not draw image... not in edit mode & it's not set. --%>
	</c:otherwise>
</c:choose>
