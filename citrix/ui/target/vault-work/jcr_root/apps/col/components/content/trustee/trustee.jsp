<%--
	Trustee

	Static content using the trustee image and path. Currently, there is no authoring of 
	this component and used to keep consistency throughout the site.
	
	achew@siteworx.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="logoPrivacy" id="8404f224-42b9-4a09-962f-f782baa917e4">
	<script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/Citrix-Systems,-Inc-/asc?rid=8404f224-42b9-4a09-962f-f782baa917e4"></script>
	<a href="//privacy.truste.com/privacy-seal/Citrix-Systems,-Inc-/validation?rid=1d5a0c5d-4797-4e1e-8972-9635479fc6dc&amp;width=770&amp;height=380" title="TRUSTe European Safe Harbor certification" rel="winPopup">
		<img src="//privacy-policy.truste.com/privacy-seal/Citrix-Systems,-Inc-/seal?rid=91646604-0403-4ced-ba76-82cc693cdbcb" alt="TRUSTe European Safe Harbor certification">
	</a>
</div>
<div class="clearBoth"></div>