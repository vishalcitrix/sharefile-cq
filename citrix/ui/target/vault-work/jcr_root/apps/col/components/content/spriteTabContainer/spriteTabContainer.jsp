<%--
	Sprite Tab Container
	
	This container can have a sprite.
	
	vishal.gupta.82@citrix.com
	ingrid.tseng@citrix.com
	achew@siteworx.com
  
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="sprite-tab-container-content-container">
	<div class="sprite-tab-container-sprite-container"><div class="sprite-tab-container-sprite ${properties['sprite']}"></div></div>
	<cq:include script="content.jsp"/>
</div>