<%--
	Country Selector

	Programmatically load the countries and country groups from 'content/website/locale'.
	This component will have the ability to reorder the groups and countries. Although
	if this content should be changed, do so in the page properties of the locale.

	Dev: This component is based on how the site structure is placed and if there is a
	siteroot mixin. The country group and name is i18n where the authors will have to set
	up the translations. The data retrieve from the query will populated in the
	<code>Country</code> class to hold all the data to be used to compare and display to
	the end user. The domain property will automatically override the page link if there is one
	set.

	This is the list of instructions which this component will execute:
	1: Get the current locale page, from absolute parent 2. (content/website/locale)
	2: Query all the pages with the mixin:SiteRoot.
	3: Create this component node if it does not already exist.
	4: Create the component child nodes (groups, countries) if it does not already exist.
	5: If WCM is in edit mode:
		a: Create/update groups based on all the country found from the query.
		b: Create/update countries based on the unique path of the country.
	6: Order the groups and countries based on the order of nodes in the JCR.
	7. Append additional groups and countries if they do not exist in the JCR.

  	achew@siteworx.com

--%>

<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="javax.jcr.query.Query"%>
<%@page import="javax.jcr.query.QueryResult"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="javax.jcr.query.QueryManager" %>
<%@page import="javax.jcr.Session" %>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String LOCALE_SELECTOR_PATH_PROPERTY = "localeSelectorPath";
	private static final String COUNTRY_GROUP_PROPERTY = "countryGroup";
	private static final String COUNTRY_NAME_PROPERTY = "countryName";
	private static final String COUNTRY_FLAG_PROPERTY = "countryFlag";
	private static final String COUNTRY_CURRENCY_PROPERTY = "currency";
	private static final String COUNTRY_DOMAIN_PROPERTY = "domain";
	private static final String WIDTH_PROPERTY = "width";
	private static final String GROUP_ROOT_NODE = "groups";
	private static final String GROUP = "group";
	private static final String COUNTRY_ROOT_NODE = "countries";
	private static final String COUNTRY = "country";
	private static final String LINK = "link";

	public class Country {
		private String group;
		private String name;
		private String flag;
		private String currency;
		private String link;
		private String domain;

		public Country() {
		}

		public void setGroup(String group) {
			this.group = group;
		}
		public String getGroup() {
			return this.group;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getName() {
			return this.name;
		}
		public void setFlag(String flag) {
			this.flag = flag;
		}
		public String getFlag() {
			return this.flag;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getCurrency() {
			return this.currency;
		}
		public void setLink(String link) {
			this.link = link;
		}
		public String getLink() {
			return this.link;
		}
		public void setDomain(String domain) {
			this.domain = domain;
		}
		public String getDomain() {
			return this.domain;
		}
	}

	/**
	* Delete country nodes that do not exist anymore. Retrieve the max index of the last inserted node.
	* Use the max index to add new county node that does not exist in the JCR and save the link path to
	* be used as a unique identifier.
	*/
	public void updateCountryNode(final Node countriesNode, final List<Country> countries) throws RepositoryException {
		int maxIndex = 0;
		final List<Node> deleteCountryNodes = new ArrayList<Node>();

		final NodeIterator countryNodes = countriesNode.getNodes();
		while(countryNodes.hasNext()) {
			boolean deleteNode = true;
			final Node currentCountryNode = countryNodes.nextNode();
			for(int i = 0; i < countries.size(); i ++) {
				final Country country = countries.get(i);
				if(currentCountryNode.hasProperty(LINK) ? currentCountryNode.getProperty(LINK).getString().equals(country.getLink()) : false) {
					countries.remove(i);
					deleteNode = false;
					break;
				}
			}
			if(deleteNode) {
				deleteCountryNodes.add(currentCountryNode);
			}else { //Max Index
				final String[] countrySplit = currentCountryNode.getName().split(COUNTRY);
				if(countrySplit != null && countrySplit.length > 1) {
					final int countryIndex = Integer.parseInt(countrySplit[1]);
					if(countryIndex >= maxIndex) {
						maxIndex = countryIndex;
					}
				}
			}
		}

		//Delete
		for(int i = 0; i < deleteCountryNodes.size(); i++) {
			final Node country = deleteCountryNodes.get(i);
			country.remove();
			countriesNode.save();
		}

		//Add
		for(int i = 0; i < countries.size(); i++) {
			final String link = countries.get(i).getLink();
			if(link != null) {
				final Node newCountryNode = countriesNode.addNode(COUNTRY + ++maxIndex);
				countriesNode.save();
				newCountryNode.setProperty(LINK, link);
				newCountryNode.save();
			}
		}
	}

	/**
	* Delete group nodes that do not exist anymore. Retrieve the max index of the last inserted node.
	* Check to make sure the the groups need to be added do not duplicate. Use the max index to add
	* new group node that does not exist in the JCR and save the group text to be used as a unique
	* identifier.
	*/
	public void updateGroupNode(final Node groupsNode, final List<Country> countries) throws RepositoryException {
		int maxIndex = 0;
		final List<Node> deleteGroupNodes = new ArrayList<Node>();
		final List<String> listOfNewGroups = new ArrayList<String>();

		final NodeIterator groupNodes = groupsNode.getNodes();
		while(groupNodes.hasNext()) {
			boolean deleteNode = true;
			final Node currentGroupNode = groupNodes.nextNode();
			for(int i = 0; i < countries.size(); i ++) {
				final Country country = countries.get(i);
				if(currentGroupNode.hasProperty(GROUP) ? currentGroupNode.getProperty(GROUP).getString().equals(country.getGroup()) : false) {
					countries.remove(i);
					i--;
					deleteNode = false;
				}
			}
			if(deleteNode) {
				deleteGroupNodes.add(currentGroupNode);
			}else { //Max Index
				final String[] groupSplit = currentGroupNode.getName().split(GROUP);
				if(groupSplit != null && groupSplit.length > 1) {
					final int groupIndex = Integer.parseInt(groupSplit[1]);
					if(groupIndex >= maxIndex) {
						maxIndex = groupIndex;
					}
				}
			}
		}

		//Delete
		for(int i = 0; i < deleteGroupNodes.size(); i++) {
			final Node group = deleteGroupNodes.get(i);
			group.remove();
			groupsNode.save();
		}

		//Filter out repeated groups
		for(int i = 0; i < countries.size(); i++) {
			final String group = countries.get(i).getGroup();
			if(!listOfNewGroups.contains(group)) {
				listOfNewGroups.add(group);
			}
		}

		//Add
		for(int i = 0; i < listOfNewGroups.size(); i++) {
			final String group = listOfNewGroups.get(i);
			if(group != null) {
				final Node newGroupNode = groupsNode.addNode(GROUP + ++maxIndex);
				groupsNode.save();
				newGroupNode.setProperty(GROUP, group);
				newGroupNode.save();
			}
		}
	}
%>

<%
	final Map<String, Object> countrySelector = new HashMap<String, Object>();
	final Page currentLocalePage = currentPage.getAbsoluteParent(2); //'content/website/en_uS'
	final List<Node> addGroupNode = new ArrayList<Node>();
	final List<Node> addCountryNode = new ArrayList<Node>();

	if(currentLocalePage != null) {
		countrySelector.put("currentLocalePage" , currentLocalePage);

		final List<Country> countries = new ArrayList<Country>();

		final String queryExpression = "SELECT * FROM [mix:CitrixSiteRoot] AS node WHERE ISDESCENDANTNODE([" + currentPage.getAbsoluteParent(1).getPath() + "])";
		final QueryManager queryManager = resource.getResourceResolver().adaptTo(Session.class).getWorkspace().getQueryManager();
		final Query query = queryManager.createQuery(queryExpression, "JCR-SQL2");
		final QueryResult result = query.execute();
		final NodeIterator nodeIter = result.getNodes();

		while(nodeIter.hasNext()) {
			final Node node = nodeIter.nextNode();
			final Country country = new Country();
			country.setGroup(node.hasProperty(COUNTRY_GROUP_PROPERTY) ? node.getProperty(COUNTRY_GROUP_PROPERTY).getString() : null);
			country.setName(node.hasProperty(COUNTRY_NAME_PROPERTY) ? node.getProperty(COUNTRY_NAME_PROPERTY).getString() : null);
			country.setFlag(node.hasProperty(COUNTRY_FLAG_PROPERTY) ? node.getProperty(COUNTRY_FLAG_PROPERTY).getString() : null);
			country.setCurrency(node.hasProperty(COUNTRY_CURRENCY_PROPERTY) ? node.getProperty(COUNTRY_CURRENCY_PROPERTY).getString() : null);
			country.setDomain(node.hasProperty(COUNTRY_DOMAIN_PROPERTY) ? node.getProperty(COUNTRY_DOMAIN_PROPERTY).getString() : null);
			final Node pageNode = node.getParent();
			country.setLink(pageNode.getPath());
			countries.add(country);
		}
		Collections.reverse(countries); //Show by ascending order

		Node countrySelectorNode = currentNode;

		if(WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)) {

			//Create component node if it does not exist
	        if(countrySelectorNode == null) {
	        	final Session session = resource.getResourceResolver().adaptTo(Session.class);
	        	countrySelectorNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
	        	session.save();
	        }

			//Add group root node
			if(countrySelectorNode.hasNode(GROUP_ROOT_NODE)) {
				//There is a groups node already, do nothing;
			}else {
				countrySelectorNode.addNode(GROUP_ROOT_NODE);
				countrySelectorNode.save();
			}

			//Add country root node
			if(countrySelectorNode.hasNode(COUNTRY_ROOT_NODE)) {
				//There is a countries node already, do nothing;
			}else {
				countrySelectorNode.addNode(COUNTRY_ROOT_NODE);
				countrySelectorNode.save();
			}

			//Groups update, adds and delete nodes
			final Node groupsNode = countrySelectorNode.getNode(GROUP_ROOT_NODE);
			updateGroupNode(groupsNode, new ArrayList<Country>(countries));

			//Countries update adds and delete nodes
			final Node countriesNode = countrySelectorNode.getNode(COUNTRY_ROOT_NODE);
			updateCountryNode(countriesNode, new ArrayList<Country>(countries));
		}


		final List<String> listOfGroups = new ArrayList<String>();
		final List<Country> listOfCountries = new ArrayList<Country>();

		if(countrySelectorNode != null) {
		//Retrieve all the groups
		final Node finalizedGroupsNode = countrySelectorNode.getNode(GROUP_ROOT_NODE);
		final NodeIterator finalizedGroupNodes = finalizedGroupsNode.getNodes();
		while(finalizedGroupNodes.hasNext()) {
			final Node currentGroup = finalizedGroupNodes.nextNode();
			listOfGroups.add(currentGroup.hasProperty(GROUP) ? currentGroup.getProperty(GROUP).getString() : null);
		}

		//Retrieve all the countries
		final Node finalizedCountryNode = countrySelectorNode.getNode(COUNTRY_ROOT_NODE);
		final NodeIterator finalizedCountryNodes = finalizedCountryNode.getNodes();
		while(finalizedCountryNodes.hasNext()) {
			final Node currentCountry = finalizedCountryNodes.nextNode();
			final String link = currentCountry.hasProperty(LINK) ? currentCountry.getProperty(LINK).getString() : null;
			if(link != null) {
				for(int i = 0; i < countries.size(); i++) {
					final Country country = countries.get(i);
					if(country.getLink().equals(link)) {
						listOfCountries.add(country);

						final String group = country.getGroup();
						if(group != null && !listOfGroups.contains(group)) {
							listOfGroups.add(group);
						}
						countries.remove(i);
						break;
					}
				}
			}
		}
		}

		//Countries not in jcr and check if groups exist, add temporary until author the page.
		for(int i = 0; i < countries.size(); i++) {
			final Country country = countries.get(i);
			listOfCountries.add(country);
			final String group = country.getGroup();
			if(group != null && !listOfGroups.contains(group)) {
				listOfGroups.add(group);
			}
		}

		countrySelector.put("groups", listOfGroups);
		countrySelector.put("countries", listOfCountries);
		countrySelector.put("width", properties.get(WIDTH_PROPERTY, 0) > 0 ? properties.get(WIDTH_PROPERTY) + "px" : "auto");
	}
%>

<c:set var="countrySelector" value="<%= countrySelector %>"/>

<a id="localeSelector" rel="lightbox" class="country ${countrySelector.currentLocalePage.properties['countryFlag']}" href="${not empty countrySelector.currentLocalePage.properties['domain'] ? countrySelector.currentLocalePage.properties['domain'] : countrySelector.currentLocalePage.path}"><fmt:message key="change.location"/></a><span> (<fmt:message key="${not empty countrySelector.currentLocalePage.properties['countryName'] ? countrySelector.currentLocalePage.properties['countryName'] : 'Country'}"/>)</span>

<%--TODO: Extract JS and add the metadata to id="localSelector" to do a lazy initialization --%>
<script>
	jQuery(document).ready(function() {
		var countrySelectorContainer = '<div id="localeContainer" style="display:none; width: ${countrySelector.width}">';
			countrySelectorContainer += '<ul>';
			<c:forEach items="${countrySelector.groups}" var="group">
				countrySelectorContainer +=	'<li class="groups">';
				countrySelectorContainer +=	'<ul>';
				<c:forEach items="${countrySelector.countries}" var="country">
					<c:if test="${country.group eq group}">
						countrySelectorContainer += '<li>';
						countrySelectorContainer += '<a href="${not empty country.domain ? country.domain : country.link}" class="country ${country.flag}"><fmt:message key="${not empty country.name ? country.name : \'Country\'}"/></a> ';
						countrySelectorContainer += '</li>';
					</c:if>
				</c:forEach>
				countrySelectorContainer += '</ul>';
			</c:forEach>
			countrySelectorContainer += '</ul>';
			countrySelectorContainer += '<div class="clearBoth"></div>';
			countrySelectorContainer += '</div>';
		jQuery('#lightbox-container div#lightbox-border').append(countrySelectorContainer);
		
		<%--TODO: add tracker based on products entered --%>
		<c:forEach var="i" begin="1" end="4">
			if(typeof(tracker${i})!='undefined')
				tracker${i}.appendJumpParamsToLinks();
		</c:forEach>
		if(typeof(tracker)!='undefined')
			tracker.appendJumpParamsToLinks();
	});
</script>