<%--
	Ordered List
	
	This list can have sprite and text and can also be numbered.
	
	vishal.gupta.82@citrix.com
	ingrid.tseng@citrix.com
	achew@siteworx.com
  
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
    
<ul class="ordered-list-container">
    <c:forEach begin="1" end="${not empty properties['listCount'] ? properties['listCount'] : 1}" varStatus="i">
        <li class="${i.first ? 'first' : i.last ? 'last' : ''}">
			<div class="ordered-list-sprite ${not empty properties['sprite'] ? properties['sprite'] : 'plain'}">
			    <c:if test="${not empty properties['numbered'] ? properties['numbered'] : false}">
			        <span class="number">
			            ${properties.startCount == 1 ? i.index : properties.startCount + (i.index - 1)}
			        </span>
			    </c:if>
			</div>
			<div class="${not empty properties['sprite'] ? properties['sprite'] : 'plain'} include">
				<cq:include path="ordered-list_item_${i.count}" resourceType="col/components/content/text"/>
			</div>
			<div class="clearBoth"></div>
        </li>
    </c:forEach>
</ul>