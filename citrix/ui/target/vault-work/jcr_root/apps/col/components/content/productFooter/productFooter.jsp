<%--

  Footer component.

  

--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public final String HIDE_BUTTON = "hideButton";
    public final String HIDE_COUNTRY_SELECTOR = "hideCountrySelector";
%>

<c:set var="hideButton" value="<%= properties.get(HIDE_BUTTON, "") %>"/>
<c:set var="hideCountrySelector" value="<%= properties.get(HIDE_COUNTRY_SELECTOR, "") %>"/>
    
<div class="spriteList">
    <cq:include path="leftLogo" resourceType="col/components/content/logo"/>
</div>

<div class="middle">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="copyright" resourceType="col/components/content/text"/>
	</swx:setWCMMode>
</div>

<div class="product-footer-links-container">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="footerLink" resourceType="citrixosd/components/content/footerLinkSet"/>
	</swx:setWCMMode>
	<c:if test="${hideCountrySelector == ''}">
		<swx:setWCMMode mode="READ_ONLY">
			<cq:include path="countrySelector" resourceType="col/components/content/countrySelector"/>
		</swx:setWCMMode>
</c:if>
</div>
    
    <div class="product-footer-sitemap-container" style="<c:if test="${not empty properties['sitemapRightOffSet']}">right: ${properties['sitemapRightOffSet']}px</c:if>">
    	<div class="product-footer-sitemap-item first">
    		<cq:include path="sitemap_1" resourceType="citrixosd/components/content/linkList"/>
    	</div>
    	<div class="product-footer-sitemap-item">
    		<cq:include path="sitemap_2" resourceType="citrixosd/components/content/linkList"/>
    	</div>
    	<div class="product-footer-sitemap-item">
    		<cq:include path="sitemap_3" resourceType="citrixosd/components/content/linkList"/>
    	</div>
    	<div class="product-footer-sitemap-item">
    		<cq:include path="sitemap_4" resourceType="citrixosd/components/content/linkList"/>
    	</div>
    	<div class="product-footer-sitemap-item last">
    		<cq:include path="sitemap_5" resourceType="citrixosd/components/content/linkList"/>
    	</div>
    	<div class="clearBoth"></div>
    </div>
    
    <div class="clearBoth"></div>
 <div class="container" id="footer-dnt"></div>   