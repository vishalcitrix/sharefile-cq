<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<div class="rowC columns content">
		    <div class="large-8 columns">
                <cq:include path="leftContent" resourceType="foundation/components/parsys"/>
            </div>
		    <div class="large-4 columns">
		    	<cq:include path="rightRailiPar" resourceType="foundation/components/iparsys"/>
		    </div>
		</div>
		<div class="clearBoth"></div>
		<cq:include path="footerContent" resourceType="swx/component-library/components/content/single-ipar"/> 
	</div>  
    <cq:include script="footer.jsp" />
</body>
