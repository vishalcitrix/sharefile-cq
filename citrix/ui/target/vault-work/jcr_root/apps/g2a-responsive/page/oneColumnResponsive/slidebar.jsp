<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

<div id="slidebar-container"></div>
<div id="slidebar-menu">
    <nav>
        <div id="close-wrapper">
            <div class="close"></div>
        </div>
        <div class="clearBoth"></div>
        <div id="slidebar-content">
            <cq:include path="sideBar" resourceType="swx/component-library/components/content/single-ipar"/>
        </div>
    </nav>
</div>