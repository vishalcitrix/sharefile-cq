<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
	ArrayList jsLibsList = (ArrayList)request.getAttribute("jsLibsList");
%>
<c:set var="designPath" value="<%= ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request) %>" />

<cq:include script="lightbox.jsp"/>
<cq:include script="geoSegmentation.jsp"/>

<footer>
	<cq:include path="sitemapFooter" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="staticFooter" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="legalFooter" resourceType="swx/component-library/components/content/single-ipar"/>
</footer>

<%-- DNT --%>
<%
    Locale locale = null;
    final Page localePage = currentPage.getAbsoluteParent(2);
    if (localePage != null) {
        try {
            locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
        } catch (Exception e) {
            locale = request.getLocale();
        }
    }
    else
        locale = request.getLocale();
%>
<script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
<script type="text/javascript">
	dnt.setMsgClass('footer-dnt');
	dnt.setMsgLocId('footer-dnt');
	dnt.setLocale('<%=locale.toString()%>');
	dnt.dntInit();
</script>
<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>

<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>

<c:if test="${not isEditMode}">
	<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js" defer="true"></script>
</c:if>

<%
	if( jsLibsList != null ) {
		int i=0;
		for(i=0; i<jsLibsList.size(); i++)
		{
			out.print("<script type=\"text/javascript\" src=\""+jsLibsList.get(i)+"\"></script>");
		}
	}
%>