<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    Note: Depends on logo and linkSet component.
    
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String SIDE_NAV_LABEL = "sideNavLabel";
	public static final String DISPLAY_HOMEPAGE_LINKS = "displayHomepageLinks";
	public static final String SECONDARY_LINK_TITLE = "secondaryLinkTitle";
	public static final String SECONDARY_LINK = "secondaryLink";
%>

<c:set var="sideNavLabel" value="<%= properties.get(SIDE_NAV_LABEL, "global.menu") %>"/>
<c:set var="displayHomepageLinks" value="<%= properties.get(DISPLAY_HOMEPAGE_LINKS) %>"/>
<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>
<c:set var="secondaryLinkTitle" value="<%= properties.get(SECONDARY_LINK_TITLE, "") %>"/>
<c:set var="secondaryLink" value="<%= properties.get(SECONDARY_LINK, "#") %>"/>

<c:choose>
    <c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed)}">     
        <div class="topNav topNavTransition">
    </c:when>
    <c:otherwise>
        <div class="topNav" style="position:inherit !important;">
    </c:otherwise>
</c:choose>
	<header>
	    <swx:setWCMMode mode="READ_ONLY">
	        <cq:include script="logo.jsp"/>
	    </swx:setWCMMode>
	    <div class="link-group <c:if test="${properties.hideHamburger}">link-group-rt</c:if>">
	        <div class="left">
	        	 <%-- Phone links display  --%>
	            <swx:setWCMMode mode="READ_ONLY">
	                <div class="left show-for-small-only">
	                    <cq:include script="phoneLinkSet.jsp"/>
	                </div>
	            </swx:setWCMMode>
				
	            <swx:setWCMMode mode="READ_ONLY">
	                <div class="left show-for-medium-up">
	                	<div class="left">
	                		<cq:include script="mainLinkSet.jsp"/>
	                	</div>

	                	<%-- Secondary links display --%>
						<c:if test="${not empty secondaryLinkTitle}">
						<div class="left secondaryLinkList">
						<ul>
							<li class="menu">
							<a href="${secondaryLink}"><span class="text">${secondaryLinkTitle}</span><span class="icon"></span></a>
				            <cq:include script="secondaryLinkSet.jsp"/>
				            </li>
				        </ul>
				        </div>
			            </c:if>

	                    <div class="left menu">
		                    <cq:include script="signinLinkSet.jsp"/>
		                </div>
	                </div>
	            </swx:setWCMMode>
				
	            <div class="clearBoth"></div>
	        </div>
	
	        <%-- Side nav --%>
	        <c:if test="${not properties.hideHamburger}">
		        <div id="menu">
		            <div class="menu hide-for-medium-up"></div>
		            <div class="menu show-for-medium-up">
		                <fmt:message key="${sideNavLabel}"/>
		            </div>
		        </div>
		  	</c:if>
	        <div class="clearBoth"></div>
	    </div>
	    <div class="show-for-small-only sec-name"></div>
	    <div class="clearBoth"></div>
	</header>
</div>