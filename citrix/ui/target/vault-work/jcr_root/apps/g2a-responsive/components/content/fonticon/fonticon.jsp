<%--
    Logo
    
    Fixed CSS classes will contain the logo and the logo can also 
    become a link. Default spite will be a placeholder and will only 
    appear in the wcm edit or read only mode. Option to add logo for small
    devices as well.
        
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String CSS_PROPERTY = "css";
    public static final String SIZE_OPTION_PROPERTY = "size";
    public static final String COLOR_OPTION_PROPERTY = "color";
%>
<c:set var="cssName" value="<%= properties.get(CSS_PROPERTY, null) %>"/>
<c:set var="colorOption" value="<%=properties.get(COLOR_OPTION_PROPERTY, "")%>"/>
<c:set var="sizeOption" value="<%=properties.get(SIZE_OPTION_PROPERTY, "")%>"/>

<c:if test="${empty sizeOption}">
    <c:set var="sizeOption" value="1"/>
</c:if>

<c:choose>
    <c:when test="${not empty cssName}">
        <div class="${colorOption}" style="font-size:${sizeOption}rem;">
            <div class="icon-${cssName}"></div>
        </div>
    </c:when>
    <c:otherwise>
        <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-image-placeholder">
        </c:if>
    </c:otherwise>
</c:choose>