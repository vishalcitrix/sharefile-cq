<%--
	Belt Navigation

	Component for slidedown nav that appears below Top Nav
	Note: Depends on linkSet component.
	
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page trimDirectiveWhitespaces="true" %>

<%!
	public static final String SIDE_NAV_LABEL = "sideNavLabel";
	public static final String DISPLAY_HOMEPAGE_LINKS = "displayHomepageLinks";
%>
<%
	Node linksNode1 = null;
	Node buttonNode = null;
	Boolean hasLinks1 = false;
	Boolean hasButton = false;
	
	if( currentNode != null ){
		if( currentNode.hasNode("linkSet1") ){
			linksNode1 = currentNode.getNode("linkSet1");
		}
		if( linksNode1 != null ){
			hasLinks1 = linksNode1.hasNodes();
		}

		if( currentNode.hasNode("buttonLink") ){
			buttonNode = currentNode.getNode("buttonLink");
			String buttonCss = buttonNode.getProperty("css").getString();

			if( !buttonCss.equalsIgnoreCase("none") ){
				hasButton = true;
			}
		}
	}
%>

<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>
<c:set var="linkSet1Title" value="<%= properties.get("linkSet1Title", "") %>"/>
<c:set var="linkSet1Link" value="<%= properties.get("linkSet1Link", "#") %>"/>
<c:set var="linkSet1Target" value="<%= properties.get("linkSet1Target", "") %>"/>
<c:set var="hasLinks1" value="<%= hasLinks1 %>"/>
<c:set var="hasButton" value="<%= hasButton %>"/>

<c:choose>
	<c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed)}">     
		<div class="primary-nav primary-nav-transition">
	</c:when>
	<c:otherwise>
		<div class="primary-nav" style="position:inherit !important;">
	</c:otherwise>
</c:choose>

	<div class="link-group primary-list-nav">
		<div class="right<c:if test="${not hasButton}"> noButton</c:if>">
			<swx:setWCMMode mode="READ_ONLY">
				
				<c:if test="${hasLinks1}">
					<cq:include script="linkset1.jsp"/>
				</c:if>
				<c:if test="${hasButton}">
					<cq:include script="buttonLink.jsp"/>
				</c:if>
				
			</swx:setWCMMode>
			
			<div class="clearBoth"></div>
		</div>       
	</div>
	<div class="clearBoth"></div>

	<div class="primary-ham-button"><a class="icon-hamburger" href="#"></a></div>
	<div class="link-group primary-ham-nav">
		<div class="list-title">${linkSet1Title}</div>
		<div class="primary-secondary-list"<c:if test="${not empty linkSet1Target}"> data-target="${linkSet1Target}"</c:if>></div>
			<swx:setWCMMode mode="READ_ONLY">
				
				<%--
				<c:if test="${hasLinks1}">
					<cq:include script="linkset1.jsp"/>
				</c:if> --%>
			</swx:setWCMMode>
			
	</div>

</div>


