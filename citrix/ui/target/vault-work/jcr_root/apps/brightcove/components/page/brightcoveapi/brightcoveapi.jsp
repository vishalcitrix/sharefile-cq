<%@page trimDirectiveWhitespaces="true"%>

<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="org.apache.sling.commons.json.JSONException" %>
<%@page import="org.apache.sling.commons.json.JSONObject" %>

<%@include file="/apps/brightcove/global/configuration.jsp"%>
<%@include file="/apps/brightcove/global/brightcove-common.jsp"%>

<%!
	private static final String BRIGHTCOVE_PARMS_ID = "id";
	private static final String BRIGHTCOVE_PARMS_LENGTH = "length";
	private static final String BRIGHTCOVE_PARMS_NAME = "name";
	private static final String BRIGHTCOVE_PARMS_THUMBNAIL_URL = "thumbnailUrl";
	
	private static final String EMPTY_JSON_RESPONSE = "\"items\":[],\"results\":0}";
%>

<%
	if(request.getParameter("action") != null) {
		int requestAction = Integer.parseInt(request.getParameter("action"));
		response.setContentType("application/json");
		
		final String start = request.getParameter("start");
    	final String limit = request.getParameter("limit");
    	final String query = request.getParameter("query");
		switch(requestAction) {
            case 0: // No command
                out.write("There is no action command. /apps/brightcove/components/page/brightcoveapi");
                break;
            case 1: // Get list without exporting CSV
                out.write(getList(READ_TOKEN, BRIGHTCOVE_PARMS_NAME + "," + BRIGHTCOVE_PARMS_ID, false));
                break;
            case 2: // Get list of videos for content finder
                out.write(getListSideMenu(READ_TOKEN, limit));
                break;
            case 3: // Exports data in csv for case 2
                response.reset();
                response.setHeader("Content-type","application/xls");
                response.setHeader("Content-disposition","inline; filename=Brightcove-Library-Export.csv");
                out.write(getList(READ_TOKEN, BRIGHTCOVE_PARMS_NAME + "," + BRIGHTCOVE_PARMS_ID, true));
                break;
            case 4: // Retrieve the all the playlist video in the brightcove account for content finder
                out.write(getListPlaylistsSideMenu(READ_TOKEN));
                break;
            case 5: // Exports data in csv for case 3
            	//TODO
                break;
            case 6: // Retreive the list based on query
            	out.write(getList(READ_TOKEN, BRIGHTCOVE_PARMS_NAME + "," + BRIGHTCOVE_PARMS_ID + "," + BRIGHTCOVE_PARMS_LENGTH + "," + BRIGHTCOVE_PARMS_THUMBNAIL_URL, false, start, limit, query));
                break;        	
            default: // No command found
            	out.write("There is no action command found. /apps/brightcove/components/page/brightcoveapi");
				break;
        }
	}else {
		out.write(EMPTY_JSON_RESPONSE); //Empty JSON response
	}
%>
