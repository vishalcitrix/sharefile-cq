<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
%>
<c:set var="designPath" value="<%= designPath %>" />
<cq:include script="lightbox.jsp"/>
<%-- Javascripts - Place after including jQuery Lib --%>
<script async="" type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
<div id="footerSection">
	<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>
</div>
<cq:include script="channelTracking.jsp"/>
<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
   <script async="" type="text/javascript" src="${designPath}/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
<script async="" type="text/javascript" src="https://sadmin.brightcove.com/js/APIModules_all.js"></script>
<script async="" type="text/javascript" src="https://files.brightcove.com/bc-mapi.js"></script>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>