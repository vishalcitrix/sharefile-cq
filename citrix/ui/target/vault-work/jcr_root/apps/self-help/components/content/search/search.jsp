<%--
    Serach

    self help search component

--%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:set var="formAction" value="${properties.formAction}"/>
<c:set var="cssClass" scope="request" value="${properties.cssClass}"/>

<c:if test="${not empty formAction}">
<c:if test="${not empty cssClass}"><div class="${cssClass}"></c:if>
<form action="${properties.formAction}" target="_blank">
    <div class="searchsection clearfix collapse">
            <div class="left">
                <input type="text" placeholder="${properties.placeholder}" id="${properties.id}" name="search" class="form-control">
                <input type="hidden" value="${properties.product}" name="prod" class="form-control">
            </div>
            <div class="left">
                <button type="submit" class="button postfix icon-search"></button>
            </div>
        </div>
    </div>              
</form>
<c:if test="${not empty cssClass}"></div></c:if>
</c:if>