<%--
    Logo        
    
    - Contain logo and option to link logo
    - Default sprite will be a placeholder and will only appear in the wcm edit or read only mode. 
    - Option to add logo for small devices as well.
    
    sahana.rai@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String CSS_PROPERTY = "css";
    public static final String CSSMEDIUM_PROPERTY = "cssMedium";
    public static final String CSSSMALL_PROPERTY = "cssSmall";
    public static final String PATH_PROPERTY = "path";
    public static final String LINK_OPTION_PROPERTY = "linkOption";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, null) %>"/>
<c:set var="cssMedium" value="<%= properties.get(CSSMEDIUM_PROPERTY, null) %>"/>
<c:set var="cssSmall" value="<%= properties.get(CSSSMALL_PROPERTY, null) %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, null) %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>
<li class="">
<c:choose>
    <c:when test="${not empty css}">
        <c:choose>
            <c:when test="${not empty path && empty linkOption}">
                <c:choose>
                    <c:when test="${not empty cssSmall && not empty cssMedium}">
                        <div class="show-for-small-only">
                            <a href="${path}"><span class="${cssSmall} logos" id="xslogo"></span></a>
                        </div>
                        <div class="show-for-medium-only">
                            <a href="${path}"><span class="${cssMedium} logos" id="smlogo"></span></a>
                        </div>
                        <div class="show-for-large-up">
                            <a href="${path}"><span class="${css} logos" id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:when test="${not empty cssSmall && empty cssMedium}">
                        <div class="show-for-small-only">
                            <a href="${path}"><span class="${cssSmall} logos"  id="xslogo"></span></a>
                        </div>
                        <div class="show-for-medium-up">
                            <a href="${path}"><span class="${css} logos"  id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:when test="${empty cssSmall && not empty cssMedium}">
                        <div class="hide-for-large-up">
                            <a href="${path}"><span class="${cssMedium} logos" id="smlogo"></span></a>
                        </div>
                        <div class="show-for-large-up">
                            <a href="${path}"><span class="${css} logos"  id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <a href="${path}"><span class="${css} logos"  id="lglogo"></span></a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${not empty path && not empty linkOption}">
                <c:choose>
                    <c:when test="${not empty cssSmall && not empty cssMedium}">
                        <div class="show-for-small-only left">
                            <a href="${path}" rel="${linkOption}"><span class="${cssSmall} logos" id="xslogo"></span></a>
                        </div>
                        <div class="show-for-medium-only left">
                            <a href="${path}" rel="${linkOption}"><span class="${cssMedium} logos" id="smlogo"></span></a>
                        </div>
                        <div class="show-for-large-up left">
                            <a href="${path}" rel="${linkOption}"><span class="${css} logos"  id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:when test="${not empty cssSmall && empty cssMedium}">
                        <div class="show-for-small-only left">
                            <a href="${path}" rel="${linkOption}"><span class="${cssSmall} logos" id="xslogo"></span></a>
                        </div>
                        <div class="show-for-medium-up left">
                            <a href="${path}" rel="${linkOption}"><span class="${css} logos"  id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:when test="${empty cssSmall && not empty cssMedium}">
                        <div class="hide-for-large-up left">
                            <a href="${path}" rel="${linkOption}"><span class="${cssMedium} logos" id="smlogo"></span></a>
                        </div>
                        <div class="show-for-large-up left">
                            <a href="${path}" rel="${linkOption}"><span class="${css} logos"  id="lglogo"></span></a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <a href="${path}" rel="${linkOption}"><span class="${css} logos"  id="lglogo"></span></a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <span class="${css} logos" id="lglogo" rel="${linkOption}"></span>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-image-placeholder"/>
        </c:if>
    </c:otherwise>
</c:choose>
<c:if test="${not empty properties.supportlink}">
    <div class="logoaddlink left">
    <a href="${properties.supportlinkpath}"><span class="text">${properties.supportlink}</span></a>
    </div>
</c:if>
</li>