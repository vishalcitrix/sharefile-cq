<%--
	Footer component.
	- Includes Logo and County Selector Component	

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="left">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="countrySelector" resourceType="/apps/self-help/components/content/countrySelector"/>
	</swx:setWCMMode>
    ${properties.copyRightText}
</div>
<div class="right">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="logo" resourceType="/apps/citrixosd-responsive/components/content/logo"/>
	</swx:setWCMMode>
</div>
<div class="clearBoth"></div>