<%--
    footerConfig component shows available languages in the project structure and current page product name will be added to the language url. 
    About us, Terms of service, Privacy policy and Research urls needs to be configure based on the requirement.
    If check box is selected, research url will be concatenated with product name 
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="java.util.Date"%>
<%@page import="com.citrix.marketing.cq.commerce.service.selfhelp.Navigation"%>

<%Page parentPage = currentPage.getAbsoluteParent(1);
String currentPagePath= currentPage.getPath();%>
                <div class="left">
                    <div class="text parbase">
                        <p class="footer-lang">
                            <span class="citrix-rg-12px-lang">
                                <span><fmt:message key="Language"/>: </span>
                                 <c:set var="stringPath" value="<%= Navigation.getStringPath(currentPagePath)%>"/>
                                  <c:set var="stringPath" value="${stringPath eq 404? 'support' : stringPath}"/>
                                 <c:set var="forwardSlash" value="${empty stringPath ? '' : '/'}"/>
                                 <c:set var="pages" value="<%= Navigation.getNavigationPages(parentPage)%>"/>                            
                                <c:forEach var="page" items="${pages }" varStatus="status">
	                                <c:set var="pagelink" value="${page.link}${forwardSlash}${stringPath}"/>
	                                <c:set var="currentpage" value="<%=currentPage.getPath()%>"/>
	                                <c:set var="pagelinkPath" value="${page.link}${forwardSlash}${stringPath}"/>
	                                <c:set var="isValidNavigationPage" value="<%=Navigation.isValidNavigationPage(resource,pageContext)%>"/>                               
	                            <c:choose>
                                    <c:when test="${not empty page.pages}">
	                                    <c:choose>
		                                    <c:when test="${currentpage eq pagelinkPath}">
		                                     <span><b>${page.title}</b>
		                                     <c:if test="${!status.last}">  </c:if></span>                                    
		                                    </c:when>
		                                    <c:otherwise>
		                                    <c:if test="${isValidNavigationPage}">
		                                        <span><a href="${empty page.thirdPartyLink ? pagelink : page.thirdPartyLink}" title="${page.title}">${page.title}</a>
		                                        <c:if test="${!status.last}">  </c:if></span>  
		                                    </c:if>                                  
		                                    </c:otherwise>                                  
	                                    </c:choose>                            
	                                 </c:when>
	                                    <c:otherwise>
	                                        <li><a href="${empty page.thirdPartyLink ? pagelink : page.thirdPartyLink }">${page.title}</a></li>                                        
	                                    </c:otherwise>                                  
                                </c:choose>
                            </c:forEach>
                            </span>
                            </p>
                        <p class="footer-links">
                            <span class="citrix-rg-12px links">
                            <c:set var="aboutUsURL" value="${properties.aboutUsURL}"/>
                            <c:if test="${not empty aboutUsURL}">
                                <a href="${properties.aboutUsURL}" rel="external"><fmt:message key="AboutUs"/></a>
                                </c:if>                                
                                <c:set var="researchURL" value="${properties.researchURL}" />
                                <c:set var="researchUrlCheck" value="${properties.researchUrlCheck}" />
                                <c:set var="stringPathSlash" value="${stringPath eq '' ? '' : '/'}" />                                
                                <c:if test="${not empty researchURL}"> 
                                <%
                                String researchURL= (String)pageContext.getAttribute("researchURL");
                                String stringPath= (String)pageContext.getAttribute("stringPath");
                                if(stringPath.matches("gotoassistmonitoringfull|gotoassistmonitoringlite")){
                                   stringPath = "assistmonitoring"; 
                                }else {
                                        stringPath = stringPath ;
                                  } 
                                String stringPathSlash= (String)pageContext.getAttribute("stringPathSlash");                               
                                int lastIndexOfSlash = researchURL.lastIndexOf('/');
                                String productResearchURL= researchURL.substring(0, lastIndexOfSlash + 1) + stringPath + stringPathSlash + researchURL.substring(lastIndexOfSlash + 1);
                                pageContext.setAttribute("productResearchURL", productResearchURL);                                                              
                                %>                                                              
                                <a href="${researchUrlCheck ne true ? researchURL : productResearchURL}" rel="external" target="_blank"><fmt:message key="ResearchParticipation"/></a>
                                </c:if>
                                <c:set var="termsURL" value="${properties.termsURL}" />
                                <c:if test="${not empty termsURL}">
                                <a href="${properties.termsURL}" rel="external"><fmt:message key="TermsService"/></a>
                                </c:if>
                                <c:set var="privacyURL" value="${properties.privacyURL}" />
                                <c:if test="${not empty privacyURL}">
                                <a href="${properties.privacyURL}" rel="external"><fmt:message key="PrivacyPolicy"/></a>
                                </c:if>
                            </span>
                        </p>
                        <p class="footer-copy">
                        <c:set var="dateVal" value="<%=new java.util.Date()%>" />
                        <c:set var="copyrightSymbol" value="&copy" />
                            <span class="citrix-rg-12px">${copyrightSymbol}<fmt:formatDate pattern="yyyy" value="${dateVal}"/> <fmt:message key="Copyright"/></span>
                        </p>
                    </div>
                </div>
                
                
               