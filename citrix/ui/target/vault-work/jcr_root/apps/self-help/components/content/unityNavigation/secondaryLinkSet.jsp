<%--
    Secondary Nav Link Set
    - krishna.selvaraj@citrix.com
    
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrix.marketing.cq.commerce.service.selfhelp.Navigation"%>

<% 
    ArrayList<Map<String, Property>> links = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("secondaryLinkSet")) {
        final Node baseNode = currentNode.getNode("secondaryLinkSet");
        links = Utilities.parseStructuredMultifield(baseNode);
    }
    String currentPagePath= currentPage.getPath(); 
%>

<c:set var="links" value="<%= links %>" />

<div class="secondaryLinkSet link-group">
    <ul>
        <c:forEach var="entry" items="${links}">                   
            <c:if test="${not empty entry.paragraphReference.string}">
                <li class="more expand">
                    <a href="#"><fmt:message key="${entry.text.string}"/></a>
                    <span></span>
                </li>
            </c:if>
                       
            <c:if test="${empty entry.paragraphReference.string}">
            <c:set var="serviceStatusLink" value="${entry.path.string}"/>
                <c:choose>
                <c:when test="${not empty entry.classOption.string && entry.classOption.string eq 'cssStatusLink'}">
                <c:set var="meetingStatusLink" value="${properties.meetingStatusLink}"/>
                <c:set var="trainingStatusLink" value="${properties.trainingStatusLink}"/>
                <c:set var="webinarStatusLink" value="${properties.webinarStatusLink}"/>
                <c:set var="ovStatusLink" value="${properties.ovStatusLink}"/>
                <c:set var="scStatusLink" value="${properties.scStatusLink}"/>
                <c:set var="sfStatusLink" value="${properties.sfStatusLink}"/>
                <c:set var="mypcStatusLink" value="${properties.mypcStatusLink}"/>
                <c:set var="assistStatusLink" value="${properties.assistStatusLink}"/>
                <c:set var="conStatusLink" value="${properties.conStatusLink}"/>
                <c:set var="productName" value="<%= Navigation.getStringPath(currentPagePath)%>"/> 
                  <%                      
                         String serviceStatusLink = (String)pageContext.getAttribute("serviceStatusLink");
                         String meetingStatusLink= (String)pageContext.getAttribute("meetingStatusLink");
                         String trainingStatusLink= (String)pageContext.getAttribute("trainingStatusLink");
                         String webinarStatusLink= (String)pageContext.getAttribute("webinarStatusLink");
                         String ovStatusLink= (String)pageContext.getAttribute("ovStatusLink");
                         String scStatusLink= (String)pageContext.getAttribute("scStatusLink");
                         String sfStatusLink= (String)pageContext.getAttribute("sfStatusLink");
                         String mypcStatusLink= (String)pageContext.getAttribute("mypcStatusLink");
                         String assistStatusLink= (String)pageContext.getAttribute("assistStatusLink");
                         String conStatusLink= (String)pageContext.getAttribute("conStatusLink");
                         String productName= (String)pageContext.getAttribute("productName");
                         if(productName.matches("meeting|webinar|training|openvoice|gotomypc|sharefile|shareconnect|gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate|concierge")){
                             if(productName.matches("gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate")){
                             serviceStatusLink = assistStatusLink;
                             } else if(productName.equals("meeting")){
                              serviceStatusLink = meetingStatusLink;
                             }else if(productName.equals("sharefile")){
                              serviceStatusLink = sfStatusLink;
                             }else if(productName.equals("shareconnect")){
                              serviceStatusLink = scStatusLink;
                             }else if(productName.equals("training")){
                              serviceStatusLink = trainingStatusLink;
                             }else if(productName.equals("openvoice")){
                              serviceStatusLink = ovStatusLink;
                             }else if(productName.equals("webinar")){
                              serviceStatusLink = webinarStatusLink;
                             }else if(productName.equals("concierge")){
                              serviceStatusLink = conStatusLink;
                             }else if(productName.equals("gotomypc")){
                              serviceStatusLink = mypcStatusLink;
                             }
                           }else {
                             serviceStatusLink = serviceStatusLink;
                         }
                         pageContext.setAttribute("serviceStatusLink", serviceStatusLink);
                          %>
                   <c:if test="${not empty serviceStatusLink}">
                   <li>
                   <a href="${serviceStatusLink}" rel="${entry.linkOption.string}">
                       <c:if test="${not empty entry.classOption.string}">
                           <span class="${entry.classOption.string}"></span>
                       </c:if>
                       <fmt:message key="${entry.text.string}"/>
                   </a>
               </li> </c:if>
                    </c:when>
                    <c:otherwise>
                   <li>
                   <a href="${entry.path.string}" rel="${entry.linkOption.string}">
                       <c:if test="${not empty entry.classOption.string}">
                           <span class="${entry.classOption.string}"></span>
                       </c:if>
                       <fmt:message key="${entry.text.string}"/>
                   </a>
               </li>
               </c:otherwise>
                </c:choose> 
            </c:if>
        </c:forEach>
    </ul>
</div>
<div class="tertiaryNavContainer">
    <c:forEach var="entry" items="${links}">
        <c:if test="${not empty entry.paragraphReference.string}">
            <div class="tertiaryNav">
                <sling:include path="${entry.paragraphReference.string}"/>
            </div>
        </c:if>
    </c:forEach>
</div>