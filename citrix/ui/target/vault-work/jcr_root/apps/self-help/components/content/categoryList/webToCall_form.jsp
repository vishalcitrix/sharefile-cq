 <%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
 
 <form id="emailForm" method="post" action="">
	<ul>
	    <li>
	        <label for="fname"><fmt:message key="Contact.Call.web2call.name"/></label>
	        <input type="text" placeholder="<fmt:message key="Contact.Call.web2call.name"/>*"  name="fname" id="fname" title="<fmt:message key="Contact.CreateTicket.NameRequired"/>"  class="fname required special-char"  maxlength="80"/>
	</li>                                        
	<li>
	    <label for="emailAddress"><fmt:message key="Contact.Call.web2call.email"/></label>
	    <input type="text" placeholder="<fmt:message key="Contact.Call.web2call.email"/>*" name="emailAddress" id="emailAddress" title="<fmt:message key="Contact.CreateTicket.EmailRequired"/>" class="required email">
	</li>
	<li>
	    <label for="subject" class=""><fmt:message key="Contact.Call.web2call.subject"/></label>
	    <input type="text"  placeholder="<fmt:message key="Contact.Call.web2call.subject"/>*" id="subject" name="subject" title="<fmt:message key="Contact.Call.web2call.topicRequired"/>"  class="required special-char" maxlength="235"/>
	</li>
	<li>
	    <label for="question" class=""><fmt:message key="Contact.Call.web2call.detail"/></label>
	    <textarea id="question"  placeholder="<fmt:message key="Contact.Call.web2call.detail"/>*" name="question" cols="3" title="<fmt:message key="Contact.Call.web2call.descRequired"/>"  maxlength="2000" class="required special-char"></textarea>
	    </li>
	    
	    <li>
	        <input type="hidden" name="emailForm_product" class="caseproduct" id="caseproduct">
	        <input type="hidden" name="emailForm_category" class="casecategory" id="casecategory">
	        <input type="hidden" name="emailForm_subcategory" class="casesubcategory" id="casesubcategory">
	        <input type="hidden" name="from_call" value="0" id="from_call">
	    </li>
	</ul>
	<span class="web2call-mandatory">* All fields are mandatory</span>
	
	 <div class="call-submit">
	    <div class="large-4 medium-4">
	        <input type="submit"  id="submitticket" value="<fmt:message key="Contact.Call.web2call.SubmitTicket"/>" class="button submit"/>                                            
	    </div>
	    <div class="large-8 medium-8 small-12 citrix-14">
	        <p class="call-sub-or large-1 medium-1 small-12">Or</p>
	        <p class="large-11 medium-11 small-12"><fmt:message key="Contact.Call.web2call.quickCallText"/> <a class="call-quick" href="#"><fmt:message key="Contact.Call.web2call.quickCallLink"/></a></p>                                            
	    </div>
	</div>                                    
</form>