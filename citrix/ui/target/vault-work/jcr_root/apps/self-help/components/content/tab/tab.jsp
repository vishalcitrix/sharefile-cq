<%--
    Tabs
    
    It will handle javascript  show and hide feature to display the content. 
    This will also contain a parsys so authors can put content in the tab. If javascript is selected 
    the content will be split into as many links as the author specified.
    
    sahana.rai@citrix.com
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String TABS_PROPERTY = "tabs";
%>

<%
    ArrayList<Map<String, Property>> tabs = null;
    if(currentNode != null) {
        if(currentNode.hasNode(TABS_PROPERTY)) {
           tabs = Utilities.parseStructuredMultifield(currentNode.getNode(TABS_PROPERTY));          
        }
    }
%>

<c:set var="tabs" value="<%= tabs %>"/>

<section class="tabsection">
    <div id="parentHorizontalTab">
        <div class="row">
            <ul class="resp-tabs-list hor_1 show-for-large-up">
                <c:forEach items="${tabs}" var="tab" varStatus="i">
                    <li><%--<i class="${tab.icon.string}"></i><br>--%><h3>${tab.title.string}</h3><p class="show-for-large-up">${tab.subtitle.string}</p></li>
                </c:forEach>
            </ul>
        </div>

        <div class="resp-tabs-container hor_1">
            <c:forEach items="${tabs}" var="tab" varStatus="i">
                <div class="row">
                    <c:if test="${isEditMode}"><div class="warning">Tab: Starting tab [${i.count}] container</div></c:if>
                    <div class="large-1 right close-btn show-for-large-up"><i class="icon-closes"> </i> </div>
                    <div class="clearfix"></div>
                    <cq:include path="tab_${i.count}-content" resourceType="foundation/components/parsys"/>
                    <div class="clearfix"></div>
                    <c:if test="${isEditMode}"><div class="warning">Tab: End tab [${i.count}] container</div></c:if>
                </div>
            </c:forEach>
        </div>
    </div>
</section>