 <%--

  Sub Header component.

  Sub Header Component contains header on left side and search box on right side of page.
  Component is managed by two fields

  1)The 'header' field displays text below the logo.
  2)The 'search homepath' field. 
  The values are changed based on the product click.

    sahana.rai@citrix.com

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %>
<%@include file="/apps/citrixosd/global.jsp"%><%
%>

<div id="searchLink" meeting="${properties.g2m_search}" gotomypc="${properties.g2pc_search}" training="${properties.g2t_search}" webinar="${properties.g2w_search}" openvoice="${properties.ov_search}"  shareconnect="${properties.sc_search}" sharefile="${properties.sf_search}" gotoassist="${properties.g2a_search}" gotoassistcorporate="${properties.g2ac_search}" gotoassistmonitoring="${properties.g2am_search}" gotoassistremotesupport="${properties.g2ars_search}" gotoassistservicedesk="${properties.g2asd_search}" concierge="${properties.g2a_con_search}"></div>
<div id="serviceStatusLink" meeting="${properties.g2m_serviceStatus}" gotomypc="${properties.g2pc_serviceStatus}" training="${properties.g2t_serviceStatus}" webinar="${properties.g2w_serviceStatus}" openvoice="${properties.ov_serviceStatus}"  shareconnect="${properties.sc_serviceStatus}" sharefile="${properties.sf_serviceStatus}" gotoassist="${properties.g2a_serviceStatus}"></div>

<section class="search-container">
        <div class="large-12 medium-12 small-12">          
            <div class="large-8 medium-8 small-10 left">         
                <span class="prod-name">${properties.header}</span>
                <span class="change"></span>
            </div>   
             <form  id="contactussearch" method="GET" action="">
                 <input type="hidden" id="valsearch"/>
                 <input type="hidden" id="searchurl" value=""/>
            <div  id="searchbox"> 
                 <div class="large-4 medium-4 small-1 right hide-for-large-up">
                        <span class="mediumserchbox"></span>
                </div> 
                <div class="closeicon left"><i class="icon-closes" id="close-icon"> </i></div>
                <div class="large-3 medium-10 small-11 left show-for-large-up input-group" id="search">
                 
                    <div class="row collapse large-12 large-offset-2">                    
                        <div class="small-11 columns">
                            <input name="search" type="text" placeholder="<fmt:message key="Contact.SearchSupport"/>" class="form-control" id="searchboxdata">
                        </div>
                        <div class="small-1 columns">
                            <a href="#" class="button postfix" id="seachbtn"></a>
                        </div>
                    </div>
                </div>
            </div> 
                  </form>
        </div>

    </section>  