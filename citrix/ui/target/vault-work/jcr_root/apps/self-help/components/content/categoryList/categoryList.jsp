<%--

categoryList component.

It will handle javascript  show and hide feature to display the content. 

sahana.rai@citrix.com

--%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrix.marketing.cq.commerce.service.selfhelp.SelfHelpConstants"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
    String[] countryList=SelfHelpConstants.COUNTRYNAME_LIST;
%>
<c:set var="countryList" value="<%= countryList %>"/>
<div id="communityLink" meeting="${properties.com_meeting}" gotomypc="${properties.com_gotomypc}" training="${properties.com_training}" webinar="${properties.com_webinar}" openvoice="${properties.com_openvoice}"  shareconnect="${properties.com_shareconnect}" sharefile="${properties.com_sharefile}" gotoassistcorporate="${properties.com_gotoassistcorporate}" gotoassistremotesupport="${properties.com_gotoassistremotesupport}" gotoassistmonitoring="${properties.com_gotoassistmonitoring}" gotoassistservicedesk="${properties.com_gotoassistservicedesk}"></div>

<section class="contact-category">
    <div class="contact-options">
        <div class="option-links"  id="parentHorizontalTab">
            <div class="row large-12 medium-12 small-12">
                <h3 class="section-title">${properties.mainheading}</h3>
                 <ul class="tabs resp-tabs-list tooltip-container">
                    <li class="ask-tab large-4 medium-4 small-12 tooltip-wrapper" >
                        <div class="mask fade "> </div>
                        <div class="tooltip">
                            <span class="tooltip-title"><fmt:message key="Contact.Ask.tooltip.title"/></span>
                            <span><fmt:message key="Contact.Ask.tooltip.content"/></span>
                        </div>  
                        <div class="community-box"></div>
                        <i class="icon-chevron resp-arrow show-for-small-only"></i>                               
                        <h4>${properties.communityheading}</h4>
                    </li>
                    <li class="case-tab large-4 medium-4 small-12 tooltip-wrapper">
                        <div class="mask fade"> </div>
                        <div class="tooltip">
                           <span class="tooltip-title"><fmt:message key="Contact.CreateTicket.tooltip.title"/></span>
                            <span><fmt:message key="Contact.CreateTicket.tooltip.content"/></span>
                        </div>
                        <div class="case-box"></div>
                        <i class="icon-chevron resp-arrow show-for-small-only"></i>
                        <h4>${properties.ticketheading}</h4>                        
                    </li>
                    <li class="call-tab large-4 medium-4 small-12 tooltip-wrapper">
                        <div class="mask fade "> </div>
                        <div class="tooltip">
                            <span class="tooltip-title"><fmt:message key="Contact.Call.tooltip.title"/></span>
                            <span><fmt:message key="Contact.Call.tooltip.content"/></span></div>
                        <div class="call-box"></div>
                        <i class="icon-chevron resp-arrow show-for-small-only"></i>
                        <h4>${properties.callheading}</h4>                        
                    </li>
                </ul>

                <ul class="tabs resp-tabs-list hor_1 hide">
                    <li class="ask-tab large-4 medium-4 small-12" >
                        <a href="#" id="com_linkpath" target="_blank">
                            <div class="community-box"></div>                               
                            <h4>${properties.communityheading}</h4>
                        </a>
                    </li>

                    <li class="case-tab large-4 medium-4 small-12">
                        <div class="case-box"></div>
                        <h4>${properties.ticketheading}</h4>
                    </li>
                    <!-- Disable call icon feature  -->
                   
                        <c:if test="${not empty properties.disableCallFeature and properties.disableCallFeature eq 'true'}">
                           <li class="large-4 medium-4 small-12" style="display:none;" >
                                <a  class="call-disabled " href="${properties.helpPageLink}" rel="lightbox"> 
                                    <div class="call-box"></div>
                                    <h4><i class="icon-lock-closed"></i> ${properties.callheading}</h4>
                                </a>
                          </li> 
                        </c:if>
                         <!--Disable call icon feature    -->
                        <li class="call-tab large-4 medium-4 small-12"> 
                            <div class="call-box"></div>
                            <h4>${properties.callheading}</h4>
                        </li>
                
                </ul>
                <div id="cont-opt"></div>
                <div class="resp-tabs-container hor_1 hideformobile">
                    <div  class="ask-community"></div>
                    <div class="emailbox" data-form="email">
                             <!-- form for ticket creation -->
                             <cq:include script="form.jsp" />
                        <div id="createTicketResult" class="formdata">
                            <p class="green-text">${properties.successMessageGreen}</p>
                            <p class="grey-text-regular">${properties.successMessageGrey}</p>
                            <p class="grey-text-light casenumber">${properties.ticketText}</p>
                            <p class="errormessage">${properties.errorMessage}</p>
                        </div>                          
                    </div>
                    <c:if test="${not empty properties.disableCallFeature and properties.disableCallFeature eq 'true'}">
                        <div class="call-disable"></div>
                    </c:if>
                    <div class="call ${not empty properties.isWebCallEnabled and properties.isWebCallEnabled eq 'true'? 'web2call' :''}">
                        <c:if test="${not empty properties.isWebCallEnabled and properties.isWebCallEnabled eq 'true'}">
                            <div class="web2call-header" data-success-toll="${properties.phoneSuccessToll}" data-success-long="${properties.phoneSuccessLong}" data-regular-toll="${properties.phoneRegularToll}" data-regular-long="${properties.phoneRegularLong}">
                                <h1><fmt:message key="Contact.Call.web2call_header1"/></h1>
                                <p><fmt:message key="Contact.Call.web2call_header2"/></p>
                                <span class="citrix-14"><a href="#" class="call-out"><fmt:message key="Contact.Call.web2call_headerlink"/></a><span> 
                            </div>
                            
                            <div class="emailbox callbox" data-form="call">
                                <!-- form for web to call  -->
                                 <cq:include script="webToCall_form.jsp" />
                                 
                                  <div id="createTicketResult" class="formdata callResult">
                                    <p class="case-no"><fmt:message key="Contact.Call.web2call.sucessMessage1"/> <span class="green-text casenumber"></span></p>                            
                                    <p class="citrix-14"><fmt:message key="Contact.Call.web2call.sucessMessage2"/></p>                                    
                                </div>
                            </div>       
                        </c:if>
                        
                         <div class="calldetail">
                            <div id="callinfo"></div>
                            <p><span class="rfont16">United States </span><span><a href="#" class="changecountry"><fmt:message key="Contact.Call.Change"/></a></span></p>
                            <p><span><fmt:message key="Contact.Call.Tollfree"/></span><span class="bfont12 call-free">(855) 352-9002</span></p>
                            <p><span><fmt:message key="Contact.Call.LongDistance"/></span><span class="bfont12 call-long">+1 (805) 617-7000</span></p>
                        </div>
                        <div class="ui-widget">
                            <select name="Country" id="country-selector" autofocus="autofocus" autocorrect="off" autocomplete="off">
                                <option value="" ></option>
                                <c:forEach var="country" items="${countryList}">
                                    <option value="${country}" product-name='' plan="common" >${country}</option>
                                </c:forEach>   
                            </select>
                            <i class="icon-closes"> </i>
                            <p class="call_error error">Please Enter Valid Country Name</p>
                        </div>
                   
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>