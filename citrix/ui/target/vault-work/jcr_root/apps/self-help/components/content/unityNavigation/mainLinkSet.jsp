<%--
    Main Nav Links
    - vishal.gupta@citrix.com 
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrix.marketing.cq.commerce.service.selfhelp.Navigation"%>

<% 
    ArrayList<Map<String, Property>> links = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("mainNavlinks")) {
        final Node baseNode = currentNode.getNode("mainNavlinks");
        links = Utilities.parseStructuredMultifield(baseNode);
    }
    String currentPagePath= currentPage.getPath(); 
%>

<c:set var="linkList" value="<%= links %>"/>

<c:choose>
    <c:when test="${fn:length(linkList) > 0}">
        <ul>
            <c:forEach var="link" items="${linkList}"> 
                <li <c:if test="${not empty link.css}">id="${link.css.string}"</c:if>>
                <c:set var="signInLink" value="${link.path.string}" />
                <c:choose>
                <c:when test="${not empty link.css && link.css.string eq 'signIn'}">
                <c:set var="meetingSignLink" value="${properties.meetingSignLink}"/>
                <c:set var="sfSignLink" value="${properties.sfSignLink}"/>
                <c:set var="scSignLink" value="${properties.scSignLink}"/>
                <c:set var="mypcSignLink" value="${properties.mypcSignLink}"/>
                <c:set var="assistSignLink" value="${properties.assistSignLink}"/>
                <c:set var="webinarSignLink" value="${properties.webinarSignLink}"/>
                <c:set var="trainingSignLink" value="${properties.trainingSignLink}"/>
                <c:set var="ovSignLink" value="${properties.ovSignLink}"/>
                <c:set var="conSignlink" value="${properties.conSignLink}"/>
                      <c:set var="productName" value="<%= Navigation.getStringPath(currentPagePath)%>"/>
                        <%
                         String signInLink = (String)pageContext.getAttribute("signInLink");
                         String meetingSignLink= (String)pageContext.getAttribute("meetingSignLink");
                         String sfSignLink= (String)pageContext.getAttribute("sfSignLink");
                         String scSignLink= (String)pageContext.getAttribute("scSignLink");
                         String mypcSignLink= (String)pageContext.getAttribute("mypcSignLink");
                         String assistSignLink= (String)pageContext.getAttribute("assistSignLink");
                         String webinarSignLink= (String)pageContext.getAttribute("webinarSignLink");
                         String trainingSignLink= (String)pageContext.getAttribute("trainingSignLink");
                         String ovSignLink= (String)pageContext.getAttribute("ovSignLink");
                         String conSignlink= (String)pageContext.getAttribute("conSignlink");
                         String productName= (String)pageContext.getAttribute("productName");
                         if(productName.matches("meeting|webinar|training|openvoice|gotomypc|sharefile|shareconnect|gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate|concierge")){
                             if(productName.matches("gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate")){                                 
                             signInLink = assistSignLink;
                             } else if(productName.equals("meeting")){
                              signInLink = meetingSignLink;
                             }else if(productName.equals("sharefile")){
                              signInLink = sfSignLink;
                             }else if(productName.equals("shareconnect")){
                              signInLink = scSignLink;
                             }else if(productName.equals("training")){
                              signInLink = trainingSignLink;
                             }else if(productName.equals("openvoice")){
                              signInLink = ovSignLink;
                             }else if(productName.equals("webinar")){
                              signInLink = webinarSignLink;
                             }else if(productName.equals("concierge")){
                              signInLink = conSignlink;
                             }else if(productName.equals("gotomypc")){
                              signInLink = mypcSignLink;
                             }
                           }else {
                             signInLink = signInLink;
                         }
                         pageContext.setAttribute("signInLink", signInLink );
                          %>
                     <c:if test="${not empty signInLink}">
                       <a href="${signInLink}" rel="${link.linkOption.string}" <c:if test="${empty link.css}">class="single"</c:if>>
                     </c:if>
                    </c:when>
                    <c:otherwise>
                    <a href="${signInLink}" rel="${link.linkOption.string}" <c:if test="${empty link.css}">class="single"</c:if>>
                    </c:otherwise>
                </c:choose>
                        <c:if test="${not empty link.css && link.css.string ne 'signIn'}">
                            <span class="${link.css.string}"></span>
                        </c:if>
                        <span>${link.text.string}</span>
                    </a>
                </li>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>