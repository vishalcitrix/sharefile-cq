<%--
     
   This component is used to select required title, icon and url.    
 
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String ICONHEADING_PROPERTY = "iconHeading";
%>

<%
    ArrayList<Map<String, Property>> iconHeading= null;
    if(currentNode != null) {
        if(currentNode.hasNode(ICONHEADING_PROPERTY)) {
           iconHeading= Utilities.parseStructuredMultifield(currentNode.getNode(ICONHEADING_PROPERTY));                    
        }
    }
%>

<c:set var="iconHeading" value="<%=iconHeading%>"/>

<div class="iconHeading-container">
    <ul class="icon-heading">
        <c:forEach items="${iconHeading}" var="iconHeading" varStatus="i">
        <c:set var="cssClass" value="${iconHeading.cssClass.string}" />            
            <li class="${cssClass}">
                <a href="${iconHeading.url.string}" class="">
                    <i class="${iconHeading.icon.string}"></i>
                    <h3>${iconHeading.title.string}</h3>
                </a>
            </li>
        </c:forEach>
    </ul>
    <c:set var="browseMoreUrl" value="${properties.browseMoreUrl}" />
    <c:if test="${not empty browseMoreUrl}">
    <div class="browse-more"><a rel='external' href="${browseMoreUrl}"><fmt:message key="BrowseMoreCategories"/></a></div>
    </c:if>
</div>