<%--

  productDetailsView component.
  The Component displays the Products,Categories,SubCategories and articles.
  It will handle javascript  show and hide feature to display the content.
  Component is managed by six fields
      1)The 'tagPath' field to get the tag location.
      2)The 'articlePath' field to get the contact us article location.
      3)The 'productheading' field Displays the Title for Products Section.
      4)The 'categoryheading' field Displays the Title for Categories Section.
      5)The 'subcategoryheading' field Displays the Title for SubCategories Section.
      6)The 'categorylandingtheading' field Displays the Title for Articles Section.

      sahana.rai@citrix.com
--%><%
%><%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%
%><%@page session="false" %><%
%>

<%
  String tagPath=properties.get("tagpath","");
  String articlePath=properties.get("articlePath","");
  String productname=request.getParameter("product");

 %>
 
<c:choose>
    <c:when test="${empty (properties.tagpath)}">
        <c:if test="${isEditMode}"><c:out value="Right click here and Edit to browse category tag path"/> </c:if>
    </c:when>
    <c:otherwise>
    <form id="contactusCategories" name="contactusCategories" action="/bin/citrix/selfhelp/productDetailsView" method="POST">
        <input type="hidden" name="tagpath" id="tagpath" value="<%=tagPath%>" />
        <input type="hidden" name="tagpath" id="articlePath" value="<%=articlePath%>" />
        <input type="hidden" name="action" id="action"  value="product"/>
        <input type="hidden" name="product"  id="product" value="<%=productname%>"/>
         <div class="loader">Loading...</div>
    </form>

    <section class="product-select" >
        <div class="row product-box">
            <div class="large-12 medium-12 small-12">     
                <h3 class="product-Logo" id="product-Logo"></h3>      
                <h3 class="section-title" id="productheading">${properties.productheading}</h3>
                <h3 class="section-title" id="subproductheading">${properties.subproductheading}</h3> 
                <h3 class="section-title" id="categoryheading">${properties.categoryheading}</h3>
                <h3 class="section-title" id="subcategory">${properties.subcatheading}</h3>
                <h3 class="section-title" id="landingpage">${properties.landingheading}</h3>  


                <div class="prod-dropdown">
                    <div class="large-12 medium-12 small-12 columns">
                     <div class="crumb">
                         <p></p>
                    </div>
                        <ul id="contact-list"></ul>
                    </div>
                </div>
            </div>
        </div>        
    </section>
        <div id="productheight" class="lg-h150 sm-h150"></div>
    </c:otherwise>
</c:choose>
<c:if test="${not empty properties.jscode}">
    <script type="text/javascript">
        ${properties.jscode}
    </script>
</c:if>
