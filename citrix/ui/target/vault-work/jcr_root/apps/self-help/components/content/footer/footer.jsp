<%--
    Footer component for Self Help
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<div id="footerSection">
  <div class="footer single-ipar">
  <div class="section productFooter">
    <div class="left">
     <cq:include path="text" resourceType="/apps/self-help/components/content/text"/>
    </div>
    <div class="right">
    <swx:setWCMMode mode="READ_ONLY">
     <cq:include path="logo" resourceType="/apps/self-help/components/content/logo"/>
    </swx:setWCMMode>
 </div>
<div class="clearBoth"></div>
</div>
</div>
</div>