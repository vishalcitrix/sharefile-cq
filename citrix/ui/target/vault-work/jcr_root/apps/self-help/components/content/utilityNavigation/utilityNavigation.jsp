<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    Note: Depends on logo and linkSet component.    
    
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
     public static final String HEADING_PROPERTY = "heading";
%>

<c:set var="linkHeading" value="<%= properties.get(HEADING_PROPERTY, "") %>"/>

<nav class="top-bar" data-topbar role="navigation">
   <ul class="title-area">
        <cq:include script="logo.jsp"/>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>
    <section class="top-bar-section">
        <div class="nav-list">
            <div class="list">
                <c:if test="${not empty linkHeading}">
                <ul>
                    <li class="has-dropdown">
                        <a href="#">${linkHeading}</a>
                        <ul class="dropdown">
                            <li><cq:include script="productlinks.jsp"/></li>
                        </ul>
                    </li>
                </ul>
                </c:if>
           </div>
            <div class="list" id="serviceStatus">
                <cq:include script="linkSet2.jsp"/>
             </div>
        </div>
    </section>
</nav>
