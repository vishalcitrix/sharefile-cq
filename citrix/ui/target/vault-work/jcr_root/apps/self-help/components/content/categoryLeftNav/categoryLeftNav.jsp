<%--
     
   This component is used to select required title, icon and url.
    
    ratna.babu@citrix.com
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String LEFTNAV_PROPERTY = "categoryLeftNav";
%>

<%
    ArrayList<Map<String, Property>> categoryLeftNav= null;
    if(currentNode != null) {
        if(currentNode.hasNode(LEFTNAV_PROPERTY)) {
           categoryLeftNav= Utilities.parseStructuredMultifield(currentNode.getNode(LEFTNAV_PROPERTY));                    
        }
    }
%>
<c:set var="categoryLeftNav" value="<%=categoryLeftNav%>"/>

<div class="columns menu-container">
        <h5 class="subnav-title"><fmt:message key="Categories"/></h5>
           <div class="cat-drpdown">
           <c:forEach items="${categoryLeftNav}" var="catLeftNav" varStatus="i">
           <c:set var="cssClass" value="${catLeftNav.cssClass.string}"/>
           <c:if test="${not empty cssClass}">
           <span class="cat-selected show-for-small-only"><i class="${catLeftNav.icon.string}"></i>${catLeftNav.title.string}</span>
           </c:if>
           </c:forEach>
               <ul id="jump-menu">
                <c:forEach items="${categoryLeftNav}" var="categoryLeftNav" varStatus="i">
                <c:set var="cssClass" value="${categoryLeftNav.cssClass.string}"/>
                   <li class="${not empty cssClass ? cssClass : ''}"> <a class="citrix-rg-14px-darkgrey" href="${categoryLeftNav.url.string}"><i class="${categoryLeftNav.icon.string}"></i>${categoryLeftNav.title.string}</a></li>
                  </c:forEach>
                </ul> 
              
            </div>  
            <c:if test="${not empty properties.browseMoreLink}">
                	<a href="${not empty properties.browseMoreLinkURL ? properties.browseMoreLinkURL : '#' }" rel="${properties.browseMoreLinkOption}" class="browse">${properties.browseMoreLink}</a>
            </c:if>                       
</div>