<%--
    Unity Navigation
    - vishal.gupta@citrix.com
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrix.marketing.cq.commerce.service.selfhelp.Navigation"%>

<cq:include script="products.jsp"/>

<% 
    //get text from cloud nodes
      boolean isSecondaryLinkSet= false;
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("clouds")) {
        final Node baseNode = currentNode.getNode("clouds");
        values = Utilities.parseStructuredMultifield(baseNode);
        isSecondaryLinkSet = currentNode.hasNode("secondaryLinkSet");
       
    }
    
    String currentPagePath= currentPage.getPath();    
%>
 <c:set var="isSecondaryLinkSet" value="<%= isSecondaryLinkSet %>"/>
<c:set var="values" value="<%= values %>"/>
<c:set var="domain" value="<%= SiteUtils.getPropertyFromSiteRoot("domain",currentPage) %>"/>
    <c:set var="productName" value="<%= Navigation.getStringPath(currentPagePath)%>"/> 
        <%
         String productName= (String)pageContext.getAttribute("productName");
         if(productName.matches("meeting|webinar|training|openvoice|gotomypc|sharefile|shareconnect|gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate|concierge")){
             if(productName.matches("gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate")){                                 
             productName = "gotoassist";
             } else {
             productName = productName;      
             }    
           }else {
        	   productName = currentPage.getAbsoluteParent(3) !=null ? Navigation.getStringPath(currentPage.getAbsoluteParent(3).getPath()) : "" ;
        	   if(productName.matches("meeting|webinar|training|openvoice|gotomypc|sharefile|shareconnect|gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate|concierge")){
                   if(productName.matches("gotoassistmonitoringfull|gotoassistmonitoringlite|gotoassistservicedesk|gotoassistremotesupport|gotoassistcorporate")){                                 
                   productName = "gotoassist";
                   } else {
                   productName = productName;      
                   }    
                 }
        	   else{
        		   productName="";
        	   }
         }
         pageContext.setAttribute("productlogo", productName);     
          %>

<div class="topNav">
    <header>
        <a href="${properties.citrixLink}"><span class="citrix logo show-for-medium-up"></span></a>
        <c:choose>
            <c:when test="${not empty productlogo}"> 
                <a href="${properties.productLogoLink}"><span id="smalllogo" class="${productlogo}mobile logo hide-for-medium-up"></span></a>
            </c:when>                                  
            <c:otherwise>
                <a href="${properties.citrixLink}"><span id="smalllogo" class="citrix logo hide-for-medium-up"></span></a>
            </c:otherwise>
        </c:choose>
        <div class="link-group">
            <div class="left">
                <swx:setWCMMode mode="READ_ONLY">
                    <cq:include script="mainLinkSet.jsp"/>
                </swx:setWCMMode>
            </div>
            <%-- Side nav --%>
            <div id="menu">
                <div class="menu-small hide-for-medium-up">
                    <span class="open"><fmt:message key="unityNav.menu.labelsmall"/></span>
                </div>
                <div class="menu show-for-medium-up"><fmt:message key="unityNav.menu.labelmedup"/></div>
            </div>
        </div>
        <div class="clearBoth"></div>
    </header>
    <div class="product-drawer-small">
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include script="secondaryLinkSetSmall.jsp"/>
            <cq:include script="mainLinkSet.jsp"/>
            <c:choose>
            <c:when test="${fn:length(values) > 0}">
                <div class="more expand">
                    <fmt:message key="unityNav.menu.moreFromCitrix"/>
                    <span></span>
                </div>
                <div class="products">
                    <c:forEach items="${values}" var="item">
                        <div class="columns">
                            ${item.cloudtext.string}
                        </div>
                    </c:forEach>
                    <div class="clearBoth"></div>
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Please check for cloud nodes under global header.</div></c:if>
            </c:otherwise>
        </c:choose>
        </swx:setWCMMode>
    </div>
   <c:if test="${isSecondaryLinkSet}">
    <div class="secondary-link-group">
        <c:choose>
            <c:when test="${not empty productlogo}"> 
                <a href="${properties.productLogoLink}"><span id="sublogo" class="${productlogo} logos sublogo"></span></a>
            </c:when>                                  
            <c:otherwise>       
                <a href="${properties.productLogoLink}"><span id="sublogo" class=""></span></a>
            </c:otherwise>
        </c:choose>        
        
        <%-- Secondary links display --%>
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include script="secondaryLinkSet.jsp"/>
        </swx:setWCMMode>
        <div class="clearBoth"></div>
    </div>
    </c:if>
</div>