<%--
    Global Footer component have the Configuration and Logo tabs for footer configuration.
    
--%> 

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@include file="/libs/foundation/global.jsp" %>
<div id="footerSection">
  <div class="footer single-ipar">
  <div class="section productFooter">
    <div class="left">
    <swx:setWCMMode mode="READ_ONLY">  
    <cq:include path="footerConfig" resourceType="/apps/self-help/components/content/footerConfig"/>   
    </swx:setWCMMode>
    </div>
    <div class="right">
    <swx:setWCMMode mode="READ_ONLY">
     <cq:include path="logo" resourceType="/apps/self-help/components/content/logo"/>
    </swx:setWCMMode>
 </div>
<div class="clearBoth"></div>
</div>
</div>
</div>