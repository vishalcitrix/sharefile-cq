<%--
     
   This component is used to select required product title, logoicon and url for the unity nav choose product
    
    ratna.babu@citrix.com
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String PRODUCT_PROPERTY = "navchooseproducts";
%>

<%
    ArrayList<Map<String, Property>> navchooseproducts= null;
    if(currentNode != null) {
        if(currentNode.hasNode(PRODUCT_PROPERTY)) {
           navchooseproducts= Utilities.parseStructuredMultifield(currentNode.getNode(PRODUCT_PROPERTY));                    
        }
    }
%>

<c:set var="navchooseproducts" value="<%=navchooseproducts%>"/>
<section class="productList">
    <ul>
        <c:forEach items="${navchooseproducts}" var="navchooseproduct" varStatus="i">            
            <li>
                <a href="${navchooseproduct.url.string}" class="${navchooseproduct.logoicon.string}"> 
                    ${navchooseproduct.title.string}
                </a>
            </li>
        </c:forEach>
    </ul>
</section>