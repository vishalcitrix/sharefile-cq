<%--
    contactFlow  component.

    It will handle Self Service Contact Flow.

    sahana.rai@citrix.com
--%>

<%@page import="java.util.*"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page
    import="com.citrix.marketing.cq.commerce.service.selfhelp.SelfHelpUtilities"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String LIST_PROPERTY = "list";

%>

<%
    ArrayList<Map<String, Property>> list = null;
    if(currentNode != null) {
        if(currentNode.hasNode(LIST_PROPERTY)) {
           list = Utilities.parseStructuredMultifield(currentNode.getNode(LIST_PROPERTY));          
        }
    }
%>

<c:set var="list" value="<%= list %>" />
<section class="product-select" id="selfServiceCategory">
    <div class="row product-box">
        <div class="large-12 medium-12 small-12">
            <h3 class="category-title">${properties.categoryheading}</h3>
            <div class="prod-dropdown">
                <div class="large-12 medium-12 small-12 columns">
                    <ul>
                        <c:forEach items="${list}" var="link">
                            <c:if test="${not empty link.subcategories.string}">
                                <li>${link.categories.string}</li>
                            </c:if>
                            <c:if test="${empty link.subcategories.string}">
                                <c:choose>
                                    <c:when
                                        test="${(link.enablecall.string eq 'true') && (link.enableticket.string eq 'true')}">
                                        <c:set var="dispaly" value="callticket" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${link.enablecall.string eq 'true'}">
                                            <c:set var="dispaly" value="call" />
                                        </c:if>
                                        <c:if test="${link.enableticket.string eq 'true'}">
                                            <c:set var="dispaly" value="ticket" />
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                                <li contactflow="${dispaly}">${link.categories.string}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
            </div>
            <div class="section-text">${properties.sectiontext}</div>
        </div>
    </div>
    <div class="bottom">
        <cq:include path="bottomtext"
            resourceType="/apps/self-help/components/content/text" />
    </div>
</section>

<section class="product-select" id="selfServiceSubCategory"
    style="display: none;">
    <div class="row product-box">
        <div class="large-12 medium-12 small-12">
            <h3 class="category-title">${properties.subcatheading}</h3>
            <div class="prod-dropdown">
                <div class="crumb">
                    <p></p>
                </div>
                <div class="large-12 medium-12 small-12 columns">
                    <c:forEach items="${list}" var="link">
                        <c:set var="dispaly" value="" />
                        <c:if test="${not empty link.subcategories.string}">
                            <c:set var="subCategoryPath" value="${link.subcategories.string}" />
                            <%
                                      List<String> subCategoryList=null;
                                      String subPath=(String)pageContext.getAttribute("subCategoryPath");
                                      subCategoryList=SelfHelpUtilities.getTagChildList(subPath,slingRequest.getResourceResolver());

                                  %>
                            <c:set var="subList" value="<%=subCategoryList%>" />
                            <ul id="${fn:replace(link.categories.string,' ', '')}"
                                style="display: none;">
                                <c:choose>
                                    <c:when
                                        test="${(link.enablecall.string eq 'true') && (link.enableticket.string eq 'true')}">
                                        <c:set var="dispaly" value="callticket" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${link.enablecall.string eq 'true'}">
                                            <c:set var="dispaly" value="call" />
                                        </c:if>
                                        <c:if test="${link.enableticket.string eq 'true'}">
                                            <c:set var="dispaly" value="ticket" />
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach items="${subList}" var="subCategory">
                                    <li contactflow="${dispaly}" category-Name="${link.categories.string}">${subCategory}</li>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-category product-select" id="selfServiceContact"
    style="display: none;">
    <div class="contact-options">
        <div class="option-links" id="parentHorizontalTab">
            <div class="row large-12 medium-12 small-12">
                <h3 class="category-title">${properties.landingheading}</h3>
                <div class="crumb prod-dropdown">
                    <p></p>
                </div>
                <ul class="tabs resp-tabs-list hor_1" id="selfServiceContactIcons">
                    <li class="case-tab large-4 medium-4 small-12">
                        <div class="case-box"></div>
                        <h4>${properties.ticketheading}</h4>
                    </li>
                    <li class="call-tab large-4 medium-4 small-12">
                        <div class="call-box"></div>
                        <h4>${properties.callheading}</h4>
                    </li>
                </ul>
                <div id="cont-opt"></div>
                <div class="resp-tabs-container hor_1"
                    id="selfServiceContactDetails">
                    <div class="emailbox" id="selfServicecreateTicket">
                        <p class="selfServiceSubHeading">${properties.ticketheading}</p>
                        <form id="emailForm" method="post" action="">
                            <ul>
                                <li><label for="subject" class="">Subject</label>
                                <input type="text" id="subject" name="subject" title="Subject is required" class="required special-char" /></li>
                                <li><label for="question" class="">Your question</label>
                                <textarea id="question" name="question" cols="3" title="Question is required" class="required special-char"></textarea></li>
                                <li><label for="name">Your name</label> 
                                <input type="text" name="fname" id="fname" title="Name is required" class="required special-char" /></li>
                                <li><label for="companyName">Company name</label>
                                <input type="text" name="companyName" id="companyName" title="Company name is required" class="required special-char" /> </li>
                                <li><label for="accountNumber">Account number</label> 
                                <input type="text" name="accountNumber" id="accountNumber" title="Account number is required" class="required special-char" /></li>
                                <li><label for="invoiceNumber">Invoice number</label>
                                <input type="text" name="invoiceNumber" id="invoiceNumber" title="Invoice number" class="novalidate special-char" /></li>
                                <li><label for="emailAddress">Email</label>
                                <input type="text" name="emailAddress" id="emailAddress" title="Email is required" class="required email"> </li>
                                <li><label for="phoneNumber">Phone (optional)</label>
                                <input type="text" name="phoneNumber" id="phoneNumber" title="Phone number" class="phone novalidate"></li>
                                <li><input type="hidden" name="emailForm_category" id="casecategory"></li>
                                <input type="hidden" name="emailForm_subcategory" id="casesubcategory">
                            </ul>
                            <div class="large-4 medium-4">
                                <div>
                                    <input type="submit" id="submitticket" value="Submit Ticket" class="button submit" />
                                </div>
                            </div>
                        </form>
                        <div id="createTicketResult" class="formdata">
                            <p class="green-text">${properties.successMessageGreen}</p>
                            <p class="grey-text-regular">${properties.successMessageGrey}</p>
                            <p class="grey-text-light casenumber">${properties.ticketText}</p>
                            <p class="errormessage">${properties.errorMessage}</p>
                        </div>
                    </div>
                    <div class="call" id="selfServiceCall">
                        <p class="selfServiceSubHeading">${properties.callheading}</p>
                            <c:forEach items="${list}" var="link">
                                <c:if test="${link.enablecall.string eq 'true'}">
                                    <c:set var="callid" value="${fn:replace(link.categories.string,' ', '')}" />
                                    <div id="call${callid}" style="display:none;">
                                        <cq:include path="call/${callid}" resourceType="/apps/self-help/components/content/text"/>
                                    </div>
                                </c:if>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>