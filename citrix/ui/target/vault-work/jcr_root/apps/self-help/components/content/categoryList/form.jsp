 <%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
 <%

 String[] time = {"12:00 AM","12:30 AM","1:00 AM","1:30 AM","2:00 AM","2:30 AM","3:00 AM","3:30 AM","4:00 AM","4:30 AM","5:00 AM","5:30 AM","6:00 AM","6:30 AM","7:00 AM","7:30 AM","8:00 AM","8:30 AM","9:00 AM","9:30 AM","10:00 AM","10:30 AM","11:00 AM","11:30 AM","12:00 PM","12:30 PM","1:00 PM","1:30 PM","2:00 PM","2:30 PM","3:00 PM","3:30 PM","4:00 PM","4:30 PM","5:00 PM","5:30 PM","6:00 PM","6:30 PM","7:00 PM","7:30 PM","8:00 PM","8:30 PM","9:00 PM","9:30 PM","10:00 PM","10:30 PM","11:00 PM","11:30 PM"};

%>
 <c:set var="time" value="<%= time %>"/>
 <form id="emailForm" method="post" action="">
                                  <ul class="only-audio-radio">
                                        <li>
                                            <h3 class="audio-head2 center"><fmt:message key="Contact.CreateTicket.Audio.Head"/></h3><br>
                                   
                                            <div class="radio">
                                                <h3 class="audio-head1"><fmt:message key="Contact.CreateTicket.Audio.Head1"/></h3>
                                                <input type="radio" name="is_audio" id="is_audio1" value="Yes"><span class="audio-head1">Yes</span>
                                                <input type="radio" name="is_audio" id="is_audio2" value="No"><span class="audio-head1">No</span>
                                            </div>
                                            <div class="audio-24-yes audio-24">
                                                <h3 class="audio-head2"><fmt:message key="Contact.CreateTicket.Audio.Head2"/></h3>
                                                <h5 class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.YourDetail"/></h4>
                                            </div>
                                            <div class="audio-24-no audio-24">
                                                <h3 class="audio-head2 audio-24-message1"><fmt:message key="Contact.CreateTicket.Audio.NoMessage1"/></h3>
                                                <p class="audio-24-message2"><fmt:message key="Contact.CreateTicket.Audio.NoMessage2"/><a href="#" class="audio-24-no-call"><fmt:message key="Contact.CreateTicket.Audio.Call"/></a></p>
                                            </div>                                            
                                        </li>
                                    </ul>
                                    
                                    <ul class="not-only-audio">
                                        <li>
                                            <label for="subject" class=""><fmt:message key="Contact.CreateTicket.Subject"/></label>
                                            <input type="text" id="subject" name="subject" title="<fmt:message key="Contact.CreateTicket.SubjectRequired"/>"  class="required special-char" maxlength="235"/>
                                        </li>                                        
                                        <li>
                                            <label for="fname"><fmt:message key="Contact.CreateTicket.Name"/></label>
                                            <input type="text"  name="fname" id="fname" title="<fmt:message key="Contact.CreateTicket.NameRequired"/>"  class="fname required special-char"  maxlength="80"/>
                                        </li>
                                        <li>
                                            <label for="emailAddress"><fmt:message key="Contact.CreateTicket.Email"/></label>
                                            <input type="text" name="emailAddress" id="emailAddress" title="<fmt:message key="Contact.CreateTicket.EmailRequired"/>" class="required email">
                                        </li>
                                        <li>
                                            <label for="phoneNumber"><fmt:message key="Contact.CreateTicket.Phoneoptional"/></label>
                                            <input type="text" name="phoneNumber" id="phoneNumber" title="<fmt:message key="Contact.CreateTicket.PhoneNumber"/>" class="phone novalidate">
                                            <span class="form-helper-text"><fmt:message key="Contact.CreateTicket.Phone.Help"/></span>
                                        </li>
                                    </ul>
                                    
                                    <ul class="only-audio">
                                        <li><h5 class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.issueDetail"/></h5></li>
                                        
                                        <li>                                            
                                            <label for="audio_num"><fmt:message key="Contact.CreateTicket.Audio.PhoneCalled"/></label>
                                            <input type="text" name="audio_num" id="audio_num" title="<fmt:message key="Contact.CreateTicket.PhoneRequired"/>"  class="phone required" maxlength="20" />
                                            <span class="form-helper-text"><fmt:message key="Contact.CreateTicket.Audio.PhoneCalledHelp"/></span>
                                        </li>
                                        <li>
                                            <label for="audio_num_frm"><fmt:message key="Contact.CreateTicket.Audio.PhoneFrom"/></label>
                                            <input type="text" name="audio_num_frm" id="audio_num_frm" title="<fmt:message key="Contact.CreateTicket.PhoneRequired"/>"  class="phone required" maxlength="20" />
                                            <span class="form-helper-text"><fmt:message key="Contact.CreateTicket.Audio.PhoneFromHelp"/></span>
                                        </li>
                                        <li>
                                            <label for="audio_access"><fmt:message key="Contact.CreateTicket.Audio.Access"/></label>
                                            <input type="text" name="audio_access" id="audio_access" title="<fmt:message key="Contact.CreateTicket.Audio.AccessRequired"/>"  class="phone required" maxlength="20" />
                                            <span class="form-helper-text"><fmt:message key="Contact.CreateTicket.Audio.AccessHelp"/></span>
                                        </li>
                                        
                                        <li>
                                            <div class="audio-date-container audio-dropdown">
                                                <p class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.Date"/></p>
                                                <select class="form-control audio-date">
                                                      <option value="1">1 Feb 2016</option>
                                                      <option value="2">2 Feb 2016</option>
                                                </select>
                                            </div>
                                            <div class="audio-time-container audio-dropdown">
                                                <p class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.Time"/></p>
                                                <select class="form-control audio-time">
                                                   <c:forEach var="timeItem" items="${time}">
                                                        <option value="${timeItem}">${timeItem}</option>
                                                    </c:forEach>
                                                </select>
                                               
                                            </div>
                                        </li>
                                        <li>
                                            <div class="audio_country-container audio-dropdown">
                                                <p class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.Country"/></p>
                                                <select class="form-control audio-country">
                                                    <option class="audio-country-list" value="Austria">Austria</option>
                                                    <option class="audio-country-list" value="Belgium">Belgium</option>
                                                    <option class="audio-country-list" value="Brazil">Brazil</option>
                                                    <option class="audio-country-list" value="Canada (English)">Canada (English)</option>
                                                    <option class="audio-country-list" value="Canada (French)">Canada (French)</option>
                                                    <option class="audio-country-list" value="China North">China North</option>
                                                    <option class="audio-country-list" value="China South">China South</option>
                                                    <option class="audio-country-list" value="Denmark">Denmark</option>
                                                    <option class="audio-country-list" value="Finland">Finland</option>
                                                    <option class="audio-country-list" value="France">France</option>
                                                    <option class="audio-country-list" value="Germany">Germany</option>
                                                    <option class="audio-country-list" value="Hong Kong">Hong Kong</option>
                                                    <option class="audio-country-list" value="Iceland">Iceland</option>
                                                    <option class="audio-country-list" value="India">India</option>
                                                    <option class="audio-country-list" value="Ireland">Ireland</option>
                                                    <option class="audio-country-list" value="Israel">Israel</option>
                                                    <option class="audio-country-list" value="Italy">Italy</option>
                                                    <option class="audio-country-list" value="Japan">Japan</option>
                                                    <option class="audio-country-list" value="Mexico">Mexico</option>
                                                    <option class="audio-country-list" value="Netherlands">Netherlands</option>
                                                    <option class="audio-country-list" value="New Zealand">New Zealand</option>
                                                    <option class="audio-country-list" value="Norway">Norway</option>
                                                    <option class="audio-country-list" value="Portugal">Portugal</option>
                                                    <option class="audio-country-list" value="Singapore">Singapore</option>
                                                    <option class="audio-country-list" value="South Africa">South Africa</option>
                                                    <option class="audio-country-list" value="Spain">Spain</option>
                                                    <option class="audio-country-list" value="Sweden">Sweden</option>
                                                    <option class="audio-country-list" value="Switzerland">Switzerland</option>
                                                    <option class="audio-country-list" value="United Kingdom">United Kingdom</option>
                                                    <option class="audio-country-list" selected="" value="United States">United States</option>
                                                </select>
                                            </div>
                                            <div class="audio-timezone-container audio-dropdown">
                                                <p class="audio-head3"><fmt:message key="Contact.CreateTicket.Audio.Timezone"/></p>
                                                <select class="form-control audio-timezone" required>
                                                    <option value="(GMT -12:00) Eniwetok, Kwajalein">(GMT -12:00) Eniwetok, Kwajalein</option>
                                                    <option value="(GMT -11:00) Midway Island, Samoa">(GMT -11:00) Midway Island, Samoa</option>
                                                    <option value="(GMT -10:00) Hawaii">(GMT -10:00) Hawaii</option>
                                                    <option value="(GMT -9:00) Alaska">(GMT -9:00) Alaska</option>
                                                    <option value="(GMT -8:00) Pacific Time (US &amp; Canada)" selected="">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                                                    <option value="(GMT -7:00) Mountain Time (US &amp; Canada)">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                                                    <option value="(GMT -6:00) Central Time (US &amp; Canada), Mexico City">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                                                    <option value="(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                                                    <option value="(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                                                    <option value="(GMT -3:30) Newfoundland">(GMT -3:30) Newfoundland</option>
                                                    <option value="(GMT -3:00) Brazil, Buenos Aires, Georgetown">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                                                    <option value="(GMT -2:00) Mid-Atlantic">(GMT -2:00) Mid-Atlantic</option>
                                                    <option value="(GMT -1:00 hour) Azores, Cape Verde Islands">(GMT -1:00 hour) Azores, Cape Verde Islands</option>
                                                    <option value="(GMT) Western Europe Time, London, Lisbon, Casablanca">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                                                    <option value="(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris">(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
                                                    <option value="(GMT +2:00) Kaliningrad, South Africa">(GMT +2:00) Kaliningrad, South Africa</option>
                                                    <option value="(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                                                    <option value="(GMT +3:30) Tehran">(GMT +3:30) Tehran</option>
                                                    <option value="(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                                                    <option value="(GMT +4:30) Kabul">(GMT +4:30) Kabul</option>
                                                    <option value="(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                                                    <option value="(GMT +5:30) Bombay, Calcutta, Madras, New Delhi">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                                    <option value="(GMT +5:45) Kathmandu">(GMT +5:45) Kathmandu</option>
                                                    <option value="(GMT +6:00) Almaty, Dhaka, Colombo">(GMT +6:00) Almaty, Dhaka, Colombo</option>
                                                    <option value="(GMT +7:00) Bangkok, Hanoi, Jakarta">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                                                    <option value="(GMT +8:00) Beijing, Perth, Singapore, Hong Kong">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                                                    <option value="(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                                                    <option value="(GMT +9:30) Adelaide, Darwin">(GMT +9:30) Adelaide, Darwin</option>
                                                    <option value="(GMT +10:00) Eastern Australia, Guam, Vladivostok">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                                                    <option value="(GMT +11:00) Magadan, Solomon Islands, New Caledonia">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                                                    <option value="(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                                                </select>
                                               
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="not-only-audio">
                                        <li>
                                            <label for="question" class=""><fmt:message key="Contact.CreateTicket.Howcanwehelpyou"/></label>
                                            <textarea id="question" name="question" cols="3" title="<fmt:message key="Contact.CreateTicket.QuestionRequired"/>"  maxlength="2000" class="required special-char"></textarea>
                                            <span class="form-helper-text"><fmt:message key="Contact.CreateTicket.Audio.HowcanwehelpyouHelp"/></span>
                                        </li>
                                        <li>
                                            <input type="hidden" name="emailForm_product" class="caseproduct" id="caseproduct">
                                            <input type="hidden" name="emailForm_category" class="casecategory" id="casecategory">
                                            <input type="hidden" name="emailForm_subcategory" class="casesubcategory" id="casesubcategory">
                                            <input type="hidden" name="from_call" value="0" id="from_call">
                                        </li>
                                    </ul>

                                     <div class="large-4 medium-4 not-only-audio">
                                        <div>
                                            <input type="submit"  id="submitticket" value="<fmt:message key="Contact.CreateTicket.SubmitTicket"/>" class="button submit"/>
                                        </div>
                                    </div>
                                </form>