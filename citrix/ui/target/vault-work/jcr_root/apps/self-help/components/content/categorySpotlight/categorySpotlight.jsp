<%--
    Category Spotlight component

    This component includes an icon on the left side and title,text, link on the right.

    sahana.rai@citrix.com

--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

   <c:if test="${properties.iconAlignment eq 'right'}">
          <c:set var="cssClass" value="onright"/>
    </c:if>

<div class="row">
    <div class="large-12 medium-12 columns">
        <div class="boxservice ${cssClass}">
            <c:if test="${not empty properties.categoryIcon}">
            <div class="icon-s"><i class="${properties.categoryIcon}"></i></div>
            </c:if>
                <div class="servicelist">
                    <c:if test="${not empty properties.title}">
                        <h2>${properties.title}</h2>
                    </c:if>  
                    <c:if test="${not empty properties.subtitle}">
                        <p>${properties.subtitle}</p>
                    </c:if> 
                    <c:if test="${not empty properties.linkLabel}">
                        <a href="${properties.linkPath}" rel="${properties.linkOption}">${properties.linkLabel}</a>
                    </c:if>
                </div>
        </div>  
    </div>
</div>
