<%--

categoryArticles component.

This component used to enter content for category specific. This component have 4 tabs for articles titles and urls and 1 configuration tab.

    ratna.babu@citrix.com

--%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%!
    public static final String TAB1_ARTICLES = "tab1articles";
    public static final String TAB2_ARTICLES = "tab2articles";
    public static final String TAB3_ARTICLES = "tab3articles";
    public static final String TAB4_ARTICLES = "tab4articles";
    public static final String TAB5_ARTICLES = "tab5articles";
    public static final String TAB6_ARTICLES = "tab6articles";
%>

<%    
    ArrayList<Map<String, Property>> tab1articles = null;
        if(currentNode != null) {
        if(currentNode.hasNode(TAB1_ARTICLES)) {
           tab1articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB1_ARTICLES));          
        }
    }
    ArrayList<Map<String, Property>> tab2articles = null; 
    if(currentNode != null) {
        if(currentNode.hasNode(TAB2_ARTICLES)) {
           tab2articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB2_ARTICLES));          
        }
    }

    ArrayList<Map<String, Property>> tab3articles = null;
        if(currentNode != null) {
        if(currentNode.hasNode(TAB3_ARTICLES)) {
           tab3articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB3_ARTICLES));          
        }
    }
        ArrayList<Map<String, Property>> tab4articles = null;
        if(currentNode != null) {
        if(currentNode.hasNode(TAB4_ARTICLES)) {
           tab4articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB4_ARTICLES));          
        }
    }
        ArrayList<Map<String, Property>> tab5articles = null;
        if(currentNode != null) {
        if(currentNode.hasNode(TAB5_ARTICLES)) {
           tab5articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB5_ARTICLES));          
        }
    }
        ArrayList<Map<String, Property>> tab6articles = null;
        if(currentNode != null) {
        if(currentNode.hasNode(TAB6_ARTICLES)) {
           tab6articles = Utilities.parseStructuredMultifield(currentNode.getNode(TAB6_ARTICLES));          
        }
    }
        //Check Landing user Type
        String data_flag_for_user=null;
        String defaultUser=request.getParameter("userType") != null  ? request.getParameter("userType").trim() :"";
        if(defaultUser != null && !defaultUser.isEmpty()){
            data_flag_for_user="data-default-user='"+defaultUser +"'";
        }
%>

<c:set var="data_flag_for_user" value="<%= data_flag_for_user %>"/>
<c:set var="tab1articles" value="<%= tab1articles %>"/>
<c:set var="tab2articles" value="<%= tab2articles %>"/>
<c:set var="tab3articles" value="<%= tab3articles %>"/>
<c:set var="tab4articles" value="<%= tab4articles %>"/>
<c:set var="tab5articles" value="<%= tab5articles %>"/>
<c:set var="tab6articles" value="<%= tab6articles %>"/>

  <section class="category-help category-page"  ${data_flag_for_user}  >
     <h1 class=" citrix-lt-36px-grey"> <i class="${properties.headingIcon}"></i>${properties.mainheading}</h1>
      <div class="row">             
        <c:set var="tab1heading" value="${properties.tab1heading}"/>
        <c:if test="${not empty tab1heading}">
        <div class="large-6 medium-6 small-12 columns">
        <h2>${properties.tab1heading}</h2> 
            <ul  class="article-list"> 
                <c:forEach items="${tab1articles}" var="tab1articles" varStatus="i">
                    <c:choose>
                        <c:when test="${not empty tab1articles.url.string and not empty tab1articles.title.string}" >
                           <li> <a rel='external' href="${tab1articles.url.string}"> ${tab1articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab1articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>        
     <c:set var="tab2heading" value="${properties.tab2heading}"/>
     <c:if test="${not empty tab2heading}">
      <div class="large-6 medium-6 small-12 columns">
       <h2>${properties.tab2heading}</h2>
            <ul  class="article-list">
                <c:forEach items="${tab2articles}" var="tab2articles" varStatus="i">
                    <c:choose>
                        <c:when test="${not empty tab2articles.url.string and not empty tab2articles.title.string}" >
                           <li> <a rel='external' href="${tab2articles.url.string}"> ${tab2articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab2articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>
        </div>
        <div class="row">
          <c:set var="tab3heading" value="${properties.tab3heading}"/>
        <c:if test="${not empty tab3heading}">                
          <div class="large-6 medium-6 small-12 columns">
          <h2>${properties.tab3heading}</h2>
            <ul class="article-list"> 
                <c:forEach items="${tab3articles}" var="tab3articles" varStatus="i">
                    <c:choose>
                        <c:when test="${not empty tab3articles.url.string and not empty tab3articles.title.string}" >
                           <li> <a rel='external' href="${tab3articles.url.string}"> ${tab3articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab3articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>
        <c:set var="tab4heading" value="${properties.tab4heading}"/>
        <c:if test="${not empty tab4heading}">
        <div class="large-6 medium-6 small-12 columns">
          <h2>${properties.tab4heading}</h2>
            <ul class="article-list"> 
                <c:forEach items="${tab4articles}" var="tab4articles" varStatus="i">
					<c:choose>
                        <c:when test="${not empty tab4articles.url.string and not empty tab4articles.title.string}" >
                           <li> <a rel='external' href="${tab4articles.url.string}"> ${tab4articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab4articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>
        </div>
         <div class="row">
          <c:set var="tab5heading" value="${properties.tab5heading}"/>
        <c:if test="${not empty tab5heading}">                
          <div class="large-6 medium-6 small-12 columns">
          <h2>${properties.tab5heading}</h2>
            <ul class="article-list"> 
                <c:forEach items="${tab5articles}" var="tab5articles" varStatus="i">
					<c:choose>
                        <c:when test="${not empty tab5articles.url.string and not empty tab5articles.title.string}" >
                           <li> <a rel='external' href="${tab5articles.url.string}"> ${tab5articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab5articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>
        <c:set var="tab6heading" value="${properties.tab6heading}"/>
        <c:if test="${not empty tab6heading}">
        <div class="large-6 medium-6 small-12 columns">
          <h2>${properties.tab6heading}</h2>
            <ul class="article-list"> 
                <c:forEach items="${tab6articles}" var="tab6articles" varStatus="i">
                    <c:choose>
                        <c:when test="${not empty tab6articles.url.string and not empty tab6articles.title.string}" >
                           <li> <a rel='external' href="${tab6articles.url.string}"> ${tab6articles.title.string}</a></li> 
                        </c:when>
                        <c:otherwise>
                             <li>  ${tab6articles.title.string}</li> 
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </div>
        </c:if>
        </div>
        </section>
         <c:set var="communityid" value="${properties.communityid}"/>
        <c:if test="${not empty communityid}">
        <section class="category-community category-page">
                                <div class="row">
                                  <div class="large-12 columns">
                                    <h2>${properties.communityTitle}</h2>
                                    <div id="gsfn_list_widget"></div>
                                    <div id="gsfn_content"></div>
                                    <script src="https://getsatisfaction.com/gotomeeting/widgets/javascripts/b84fa64/widgets.js" type="text/javascript"></script>
                                    <script src="https://getsatisfaction.com/gotomeeting/topics.widget?callback=gsfnTopicsCallback&amp;length=0&amp;limit=5&amp;sort=recently_active&amp;style=question&amp;user_defined_code=${communityid}" type="text/javascript"></script>
                                    
                                          <section class="gsfn_search_container">
                                            <div class="large-12 medium-12 small-12">
                                                <form id="gsfn_search_form" accept-charset="utf-8" action="https://getsatisfaction.com/gotomeeting" method="get" onsubmit="gsfn_search(this); return false;">
                                                    <input name="product" type="hidden" value="collaboration_gotomeeting">
                                                    <input name="style" type="hidden" value="question">
                                                    <input name="limit" type="hidden" value="5">
                                                    <input name="utm_medium" type="hidden" value="widget_search">
                                                    <input name="utm_source" type="hidden" value="widget_gotomeeting">
                                                    <input name="callback" type="hidden" value="gsfnResultsCallback">
                                                    <input name="format" type="hidden" value="widget">
                                                    <div id="gsfn_search">
                                                        <div class="large-7 medium-10 small-12 input-group" id="search">
                                                            <div class="row collapse large-12">                    
                                                                <div class="small-10 columns error-field"><div class="hidden error icon-attention"></div>
                                                                    <input name="query" type="text" placeholder="<fmt:message key="Askthecommunity"/>" class="form-control" id="gsfn_search_query">
                                                                </div>
                                                                <div class="small-2 columns">
                                                                    <button id="continue" type="submit" class="btn btn-primary community-btn"><fmt:message key="Ask"/></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="gsfn_search_results" style="height: auto;"></div> 
                                                </form>
                                            </div>
                                        </section>
                                  </div>
                                </div>                                
                          </section>
                          </c:if>
                         <c:set var="contactSupportUrl" value="${properties.contactSupportUrl}"/>
                           <c:if test="${not empty contactSupportUrl}">
                          <div class="sh-contact"><fmt:message key="Stillneedhelp"/> <a rel='external' href="${contactSupportUrl}"><fmt:message key="ContactSupport"/></a></div>
                        </c:if>
 <div class="clearfix"></div>