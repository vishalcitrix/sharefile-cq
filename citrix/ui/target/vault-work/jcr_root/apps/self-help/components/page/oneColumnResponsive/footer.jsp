<%@page import="java.util.Locale"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed; 
    }
%>
<c:set var="designPath" value="<%= designPath %>" />
<div class="smileySurvey">
   <script type="text/javascript" >document.write('<script src="http' + ( ("https:" == document.location.protocol) ? "s" : "") + '://www.surveygizmo.com/s3/2338346/testing-survey?__output=embedjs&__ref=' + escape(document.location.origin + document.location.pathname) + '" type="text/javascript" ></scr'  + 'ipt>');</script>
</div>

<div class="surveyTaken" style="display: none;">
    <img src="/etc/designs/self-help/css/static/images/feedback-button.png">
</div>

<div id="footerSection">
    <cq:include path="footerSiteMap" resourceType="swx/component-library/components/content/single-ipar"/>
    <cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>
    <cq:include path="footerLinks" resourceType="swx/component-library/components/content/single-ipar"/>   
</div>

<%-- Javascripts - Place after including jQuery Lib --%>
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
<%-- DNT --%>
<%
      Locale locale = null;
    final Page localePage = currentPage.getAbsoluteParent(2);
if (localePage != null) {

        try {
           locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));   
        } catch (Exception e) {
            locale = request.getLocale();
        }
    }
    else
         locale = request.getLocale();
%>
<script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
<script type="text/javascript">
    dnt.setMsgClass('footer-dnt');
    dnt.setMsgLocId('footer-dnt');
    dnt.setLocale('<%=locale.toString()%>');
    dnt.dntInit();
</script>
<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
    <cq:include script="analytics.jsp"/>
</c:if>

<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
   <script async="" type="text/javascript" src="${designPath}/js/respond.js"></script>
<![endif]-->

<%-- Include Parallax JS (To Do: add in properties section)--%>
<c:if test="${not empty properties.showParallax}">
    <script async="" type="text/javascript" src="<%= currentDesign.getPath() %>/js/parallax.js"></script>
</c:if>

<%-- script Include for channelTracking--%>
<script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/support-sites/dev/utag.js"></script>
    