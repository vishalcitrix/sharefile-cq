<%--

  Dev Only component.
  
  This component allows developers to add custom HTML on the page.
  However it takes care of the ANTISamy Policy and takes out 
  the HTML that is not consistent with the policy (eg: iframes)
  
  vishal.gupta.82@citrix.com

--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@page contentType="text/html; charset=utf-8" 
    import="com.adobe.granite.xss.XSSAPI"
    import="com.day.cq.wcm.api.WCMMode"
%>

<%!
    public static final String PARENTCLASS_PROPERTY = "parentClass";
    public static final String HTMLCODE_PROPERTY = "htmlcode";
    public static final String CSSCODE_PROPERTY = "csscode";
    public static final String JSCODE_PROPERTY = "jscode";
    public static final String JSURL_PROPERTY = "jsURL";
%>

<c:set var="parentClass" value="<%= properties.get(PARENTCLASS_PROPERTY, "") %>"/>
<c:set var="htmlCode" value="<%= properties.get(HTMLCODE_PROPERTY, "") %>"/>
<c:set var="cssCode" value="<%= properties.get(CSSCODE_PROPERTY, "") %>"/>
<c:set var="jsCode" value="<%= properties.get(JSCODE_PROPERTY, "") %>"/>
<c:set var="jsURL" value="<%= properties.get(JSURL_PROPERTY, "") %>"/>

<%
    String htmlCode = (String)pageContext.getAttribute("htmlCode");
    String parentClass = (String)pageContext.getAttribute("parentClass");
    String cssCode = (String)pageContext.getAttribute("cssCode");
    String jsURL = (String)pageContext.getAttribute("jsURL");
%>


<c:if test="${not empty jsURL}">
   <script src="<%= jsURL %>" type="text/javascript"></script>
</c:if>
<c:if test="${not empty cssCode}">
    <style type="text/css">
        <%= cssCode %>
    </style>
</c:if>

<c:choose>
     <c:when test="${not empty parentClass}">
        <div class="<%= parentClass  %>">
     </c:when>
     <c:otherwise>
         <div>
    </c:otherwise>
</c:choose>

<c:if test="${not empty jsCode}">
    <script type="text/javascript">
        ${jsCode}
    </script>
</c:if>
 
<c:choose>
     <c:when test="${not empty htmlCode}">
        <%= htmlCode.replaceAll("></", "> </") %> <!-- fix for allowing empty tags (i.e, <span></span>) -->
     </c:when>
     <c:otherwise>
        <c:if test="${isEditMode || isReadOnlyMode}"><span class="warning">Dev Only: Please enter your custom HTML here.</span></c:if>
    </c:otherwise>
</c:choose>

</div>