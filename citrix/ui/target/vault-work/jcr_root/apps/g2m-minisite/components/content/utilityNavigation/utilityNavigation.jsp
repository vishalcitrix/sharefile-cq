<%--
	Utility Navigation

  	Header contains logo component on the right and link set component on the right.
  	
  	Note: Depends on logo, countrySelector, listSet component.
  	
  	achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>


<div class="container">
	<div style="float:left;">
		<swx:setWCMMode mode="READ_ONLY">
 			<cq:include script="logo.jsp"/>
 		</swx:setWCMMode>
	</div>

	<div style="float:right;">
		<div class="links">
		    <ul>
   		    	<c:if test="${empty properties['hideCountrySelector'] || properties['hideCountrySelector'] == false}">
			        <li>
			        	<swx:setWCMMode mode="READ_ONLY">
			        		<cq:include path="countrySelector" resourceType="citrixosd/components/content/countrySelector"/>
			        	</swx:setWCMMode>
			        </li>
		        </c:if>
		        <c:if test="${empty properties['hideCountrySelectorSlash'] || properties['hideCountrySelectorSlash'] == false}">
		        	<li><span>|</span></li>
		        </c:if>
		        <li style="padding: 0; ${isDesignMode ? 'min-width: 200px;' : ''}">
		        	<swx:setWCMMode mode="READ_ONLY">
        				<%-- Link Set component --%>
		        		<cq:include script="linkSet.jsp"/>
		        	</swx:setWCMMode>
		        </li>
			</ul>
		</div>
	</div>
	
	<div class="clearBoth"></div>
</div>
