<%--
  Contact Sales component.
  Lets users create contact sales form for G2PC, G2MWT, G2A
  vishal.gupta.82@citrix.com
--%>

<%@page import="java.util.Locale"%>
<%@page import="com.adobe.granite.xss.XSSAPI"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final String css = properties.get("css", "");
    final String buttonColor = properties.get("buttonColor", "blue");
    final String website = properties.get("website", "GoToMyPC");
    final String jscode = properties.get("jscode","");
    final boolean useDemandBase = properties.get("useDemandBase", false);
%>   
    <c:set var="leadGeo" value="<%= properties.get("leadGeo", true) %>"/>
    <c:set var="collabration" value="<%= properties.get("collabration", false) %>"/> 
    <c:set var="useDemandBase" value="<%= useDemandBase %>"/> 
    <c:set var="website" value="<%= website %>"/> 
    <c:set var="count" value="0" />   
    <c:set var="jscode" value="<%= jscode %>"/> 


    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${currentDesign.path}/js/form.js"></script>
    <c:if test="${useDemandBase}">
        <c:if test="${not isEditMode}">
            <script type="text/javascript" src="http://autocomplete.demandbase.com/autocomplete/v1.7/widget.js"></script>
        </c:if>
        <script type="text/javascript" src="${currentDesign.path}/js/demandBase.js"></script>
        <script type="text/javascript" src="http://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358&amp;callback=DemandGen.DemandBase.processIPInfo"></script>    
    </c:if>
    <c:if test="${not empty jscode}">
        <script type="text/javascript">
            ${jscode}
        </script>
    </c:if>
    
    <!--  adding cookie evaluator -->
    <cq:include script="cookieEval.jsp"/>
    
    <div class="contactSales <%= css %>">
    <c:choose>
        <c:when test="${collabration}">
            <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" onSubmit="contact(this,${leadGeo});" name="contactSales">
        </c:when>
        <c:otherwise>
            <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" name="contactSales">
        </c:otherwise>
    </c:choose>
    <div class="form-box">

        <!--  adding hidden fields -->
        <cq:include script="hiddenFields.jsp" />

        <!-- adding collaboration checkboxes -->
        <c:if test="${collabration}">
            <label>
                <fmt:message key="contactSales.needs.label" />
            </label>
        </c:if>
        <cq:include script="isCollaboration.jsp" />

        <ul>
            <li class="left-col">
                <label for="FirstName">
                    <fmt:message key="minisite.form.label.firstName" />
                </label>
                <input id="FirstName" name="FirstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>">
            </li>
            <li class="right-col">
                <label for="LastName">
                    <fmt:message key="minisite.form.label.lastName" />
                </label>
                <input id="LastName" name="LastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>">
            </li>
            <li class="left-col">
                <label for="Email">
                    <fmt:message key="minisite.form.label.email" />
                </label>
                <input id="Email" name="Email" type="text" class="required email" title="<fmt:message key="form.validate.email"/>">
            </li>
            <li class="right-col">
                <label for="Company">
                    <fmt:message key="minisite.form.label.company" />
                </label>
                <input id="Company" name="Company" type="text" class="required ">
            </li>
            <li class="left-col">
                <label for="Phone">
                    <fmt:message key="minisite.form.label.phone" />
                </label>
                <input id="Phone" name="Phone" type="text" class="phone" title="<fmt:message key="form.validate.phone"/>">
            </li>
            <li class="right-col">
                <label for="Department__c">
                    <fmt:message key="minisite.form.label.department" />
                </label>
                <!-- adding department list -->
                <cq:include script="departmentList.jsp" />
            </li>
            <li class="left-col">
                <label for="Industry">
                    <fmt:message key="minisite.form.label.industry" />
                </label>
                <!-- adding industry list -->
                <cq:include script="industryList.jsp" />
            </li>
            <li class="right-col">
                <label for="Country">
                    <fmt:message key="minisite.form.label.country" />
                </label>
                <select id="Country" name="Country" class="required">
                    <!-- adding country list -->
                    <cq:include script="countryList.jsp" />
                </select>
            </li>
            <c:if test="${properties.IsZipRequired}">
                <li class="left-col">
                    <label for="PostalCode">
                        <fmt:message key="minisite.form.label.zipcode" />
                    </label>
                    <input id="PostalCode" name="PostalCode" type="text" class="required" title="<fmt:message key="form.validate.zipcode" />" placeholder="<fmt:message key="form.placeholder.zipcode"/>">
                </li>
            </c:if>
        </ul>

        <div class="emp-message">
            <fmt:message key="form.label.numberOfEmployees.header.message" />
        </div>
        <ul>
            <li class="left-col">
                <label for="NumberofEmployees">
                    <fmt:message key="form.label.numberOfEmployees3" />
                </label>
            </li>
            <li class="right-col">

                <!-- adding no. of employee list -->
                <cq:include script="employeeList.jsp" />
            </li>

        </ul>
        <!--  adding form comments -->

        <div class="frmComment">
            <label for="form_comments">
                <fmt:message key="form.label.question1" />
            </label>
            <textarea id="form_comments" name="form_comments" class="validate" cols="42" rows="3"></textarea>
        </div>

        <input type="submit" value="<fmt:message key="form.label.registernow"/>" class="button <%= buttonColor %>" title="<fmt:message key="form.label.submit"/>">
        <!-- adding Privacy Policy text -->
        <cq:include path="privacyPolicyText" resourceType="citrixosd/components/content/text" />

    </div>
    </form>
</div>
