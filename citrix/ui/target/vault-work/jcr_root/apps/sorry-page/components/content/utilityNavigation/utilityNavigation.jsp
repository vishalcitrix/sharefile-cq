<%--
	Utility Navigation
  	Header contains logo component on the right and link set component on the right.
  	Note: Depends on logo, listSet component.
  	vishal.gupta.82@citrix.com

--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="container">
	<div style="float:left;">
	
		<swx:setWCMMode mode="READ_ONLY">
 			<cq:include script="logo.jsp"/>
 		</swx:setWCMMode>
	</div>

	<div style="float:right;">
		<div class="links">
		    <ul>
		        <li style="padding: 0; ${isDesignMode ? 'min-width: 200px;' : ''}">
		        	<swx:setWCMMode mode="READ_ONLY">
        				<%-- Link Set component --%>
		        		<cq:include script="linkSet.jsp"/>
		        	</swx:setWCMMode>
		        </li>
			</ul>
		</div>
	</div>
	
	<div class="clearBoth"></div>
</div>
