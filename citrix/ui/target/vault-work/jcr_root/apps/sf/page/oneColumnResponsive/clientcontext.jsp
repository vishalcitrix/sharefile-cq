<%@page import="com.citrixosd.utils.ContextRootTransformUtil,com.citrixosd.SiteUtils"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="disablePersonalization" value="<%= SiteUtils.getPropertyFromSiteRoot("disablePersonalization",currentPage) %>" />
<c:set var="designPath" value="<%= ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request) %>" />

<c:if test="${(disablePersonalization && properties.enablePersonalization) || (not disablePersonalization)}">
	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</c:if>

<c:choose>
	<c:when test="${(disablePersonalization && properties.enablePersonalization) || (not disablePersonalization)}">
		<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
	</c:when>
	<c:otherwise>
		<script type="text/javascript" src="${designPath}/js/foundation/personalization.js"></script>
	</c:otherwise>
</c:choose>