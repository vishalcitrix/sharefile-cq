<%@page import="com.citrixosd.utils.ContextRootTransformUtil,
				com.citrixosd.SiteUtils,
				org.apache.commons.lang3.StringEscapeUtils,
				com.day.cq.wcm.api.WCMMode" %>

<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%
    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request) %>" />
<c:set var="disableAddThis" value="<%= SiteUtils.getPropertyFromSiteRoot("disableAddThis",currentPage) %>" scope="request" />
<c:set var="headJs" value="<%= SiteUtils.getPropertyFromSiteRoot("headJs",currentPage) %>" />

<head>
	<c:if test="${not empty properties.addOptimizely}">
		<cq:include script="abtest.jsp"/>
	</c:if>
	<title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <%-- Meta tags --%>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width" />
    <cq:include script="meta.jsp"/>
    <link rel="Shortcut Icon" href="${designPath}/css/static/images/favicon.ico">
    <link rel="stylesheet" href="${designPath}/css/app.css" type="text/css">
	
    <%--CSS --%>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="${designPath}/css/ie8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" href="${designPath}/css/ie9.css" type="text/css">
    <![endif]-->

    <script type="text/javascript" src="${designPath}/js/common.js"></script>
    <c:if test="${not empty headJs}">${headJs}</c:if>	
</head>