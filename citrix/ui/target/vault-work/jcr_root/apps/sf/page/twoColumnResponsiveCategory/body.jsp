<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
 	<cq:include script="ga.jsp" />
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="filter.jsp"/>
		<div class="rowC columns content">
		    <div class="large-8 columns">
                <cq:include script="resourceContent.jsp"/>
                <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
            </div>
		    <div class="large-4 columns">
		    	<c:if test="${isEditMode}">
		    		<div class="warning">This will be inherited to detail page (via Landing Page)</div>
		    	</c:if>
		        <cq:include path="rightRailiPar" resourceType="foundation/components/iparsys"/>
		        <c:if test="${isEditMode}">
		    		<div class="warning">This will inherited to detail page</div>
		    	</c:if>
		        <cq:include path="rightRailContent" resourceType="foundation/components/iparsys"/>
		        <c:if test="${isEditMode}">
		    		<div class="warning">This will not be inherited to detail page</div>
		    	</c:if>
		        <cq:include path="rightRailCurrContent" resourceType="foundation/components/parsys"/>
		    </div>
		</div>
		<div class="clearBoth"></div>
		<cq:include path="footerContent" resourceType="swx/component-library/components/content/single-ipar"/> 
	</div>  
    <cq:include script="footer.jsp" />
    <cq:include script="clientcontext.jsp" />  
</body>