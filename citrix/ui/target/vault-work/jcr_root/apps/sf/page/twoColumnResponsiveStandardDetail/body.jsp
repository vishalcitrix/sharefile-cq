<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.SiteUtils"%>

<c:set var="fullColumnDetailPage" value="<%= SiteUtils.getPropertyFromSiteRoot("fullColumnDetailPage",currentPage) %>" />

<%
    response.setHeader("Dispatcher", "no-cache");
%>
<body>
	<cq:include script="ga.jsp" />
    <div id="content-body" class="resourcePage detail">
        <cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="filter.jsp"/>
       	<c:choose>
       		<c:when test="${fullColumnDetailPage && not isEditMode}">
       			<div class="large-6 columns fullCol">
	                <cq:include script="resourceContent.jsp"/>
	                <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
	                <c:if test="${isEditMode}">
			            <div class="warning">Sub Main Content (inheriting from landing page)</div>
			        </c:if>
	                <cq:include path="subMainContent" resourceType="swx/component-library/components/content/single-ipar"/>
	                <c:if test="${not isEditMode}">
	                	<cq:include script="threeShips.jsp"/>
	                </c:if>      
	            </div>
       		</c:when>
       		<c:otherwise>
       			<div class="rowC columns content">
       				<div class="large-8 columns">
		                <cq:include script="resourceContent.jsp"/>
		                <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
		                <c:if test="${isEditMode}">
				            <div class="warning">Sub Main Content (inheriting from landing page)</div>
				        </c:if>
		                <cq:include path="subMainContent" resourceType="swx/component-library/components/content/single-ipar"/>
		                <c:if test="${not isEditMode}">
		                	<cq:include script="threeShips.jsp"/>
		                </c:if>      
		            </div>
		            <div class="large-4 columns">
		                <c:if test="${isEditMode}">
		                    <div class="warning">Inheriting from Landing Page</div>
		                </c:if>
		                <cq:include path="rightRailiPar" resourceType="foundation/components/iparsys"/>
		                <c:if test="${isEditMode}">
		                    <div class="warning">Inheriting from Category Page</div>
		                </c:if>
		                <cq:include path="rightRailContent" resourceType="foundation/components/iparsys"/>
		                <c:if test="${isEditMode}">
		                    <div class="warning">Detail Page Content</div>
		                </c:if>
		                <cq:include path="rightRailCurrContent" resourceType="foundation/components/parsys"/>
		            </div>
       			</div>
       			<div class="clearBoth"></div>
       		</c:otherwise>
       	</c:choose>
        <cq:include path="footerContent" resourceType="swx/component-library/components/content/single-ipar"/> 
    </div>  
    <cq:include script="footer.jsp" />
    <cq:include script="clientcontext.jsp" /> 
</body>