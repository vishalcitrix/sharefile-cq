<%--
	3 Ships Implementation
	vishal.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.citrixosd.utils.ContextRootTransformUtil" %>
<%@ page import="com.citrixosd.SiteUtils" %>

<%	
	final String CUSTOMER_IDENTIFIER = "sharefile.com";
	final String CHARSET = "utf-8";
	
	try {
	    // Get data about the orginal request
	    final String userAgent = request.getHeader("User-Agent");
	    final String referer = request.getHeader("Referer");
	    final String ipAddress = request.getHeader("ClientHost");
		final String currentDomain = SiteUtils.getPropertyFromSiteRoot("domain",currentPage);
		
	    // Get the orginal request URL and query string
	    //StringBuffer requestURL = new StringBuffer(SiteUtils.getPropertyFromSiteRoot("domain",currentPage));
	    StringBuffer requestURL = new StringBuffer();
	    requestURL.append(ContextRootTransformUtil.transformedPath(request.getRequestURI(),request).split(".html")[0]);
	    
	    String queryString = request.getQueryString();
	
	    if(queryString != null) {
	        requestURL.append('?').append(queryString).toString();
	    }
		
	    final String navigatorUrl = "http://nav.3s-cdn.com/api/content/";
	    String query = String.format("url=%s", URLEncoder.encode(requestURL.toString(), CHARSET));
	
	    // Make an connection
	    URLConnection connection = new URL(navigatorUrl + "?" + query).openConnection();
	
	 	// Set headers if available. Do not send nulls.
        connection.setRequestProperty("X-Customer-Identifier", CUSTOMER_IDENTIFIER);
        if (userAgent != null) {
            connection.setRequestProperty("X-Original-User-Agent", userAgent);
        }
        if (referer != null) {
            connection.setRequestProperty("X-Original-Referer", referer);
        }
        if (ipAddress != null) {
            connection.setRequestProperty("X-Original-IP", ipAddress);
        }

        // Get the response stream
        InputStream navigatorResponse = connection.getInputStream();

        // Create the variable to hold the returned Navigator content
        StringBuilder htmlBuffer = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(navigatorResponse, CHARSET));
        try {
            for (String line; (line = reader.readLine()) != null;) {
                htmlBuffer.append( line );
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
	
	    // Write out the returned HyperDrive content
	    out.write(htmlBuffer.toString());
	
	}catch (Exception e) {
	    e.printStackTrace();
	}
%>