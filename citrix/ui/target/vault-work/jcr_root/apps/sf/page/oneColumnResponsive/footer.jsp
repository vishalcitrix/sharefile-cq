<%@page import="com.citrixosd.SiteUtils"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="globalJs" value="<%= SiteUtils.getPropertyFromSiteRoot("globalJs",currentPage) %>" />
<c:set var="globalCSS" value="<%= SiteUtils.getPropertyFromSiteRoot("globalCSS",currentPage) %>" />
<c:set var="noGTMforExtScripts" value="<%= SiteUtils.getPropertyFromSiteRoot("noGTMforExtScripts",currentPage) %>" />
<c:set var="useUnityNav" value="<%= SiteUtils.getPropertyFromSiteRoot("useUnityNav",currentPage) %>" />

<c:if test="${empty useUnityNav}">
	<cq:include script="slidebar.jsp"/>
</c:if>
<cq:include script="lightbox.jsp"/>
<cq:include script="geoSegmentation.jsp"/>

<footer>
	<cq:include path="staticFooter" resourceType="swx/component-library/components/content/single-ipar"/> 	
</footer>

<c:if test="${not isEditMode}">
	<%-- Extole master library --%>
	<c:if test="${not empty noGTMforExtScripts}">
		<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
		<script src ="//tags.extole.com/22556/core.js"></script>
	</c:if>
	<script type="extole/widget">
		{"zone": "homepage","reference_element":"#extole-placeholder-global_footer"}
	</script>
</c:if>

<c:if test="${not empty globalCSS}">
	<style type="text/css">
		${globalCSS}
	</style>
</c:if>

<script type="text/javascript">
	<c:if test="${not (isEditMode || isReadOnlyMode)}">  
	    menuScrollBar.init();
	</c:if>
	<c:if test="${not empty globalJs}">
    ${globalJs}
    </c:if>
</script>