<%-- 
	Locale chekbox
	vishal.gupta@citrix.com 
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="java.util.List,java.util.regex.Matcher,java.util.regex.Pattern" %>

<%
	final String delimiter = "([a-z]{2})-([a-z]{2})";
	final String language = (request.getHeader("Accept-Language")).toLowerCase();
	String locale = null;
	
	Pattern pattern = Pattern.compile(delimiter);
	Matcher matcher = pattern.matcher(language);
	
	if(matcher.find()) {
		locale= matcher.group(0); 
	}	
%>

<c:set var="locale" value="<%= locale %>"/>

<div<c:if test="${properties.darkBg}"> class="darkBg"</c:if>>
	<c:if test="${empty properties.hideDefaultText}">
		<div class="defaultMessage"><fmt:message key="sf.form.locale.defaultText"/></div>
	</c:if>
	
	<c:choose>
		<c:when test="${locale ne 'en-us'}">
			<div class="localeCheck">
			    <c:choose>
			        <c:when test="${locale eq 'de-de'}">
			            <input type="checkbox" value="true" name="mktgoptin" unchecked><label for="mktgoptin"><fmt:message key="sf.form.locale.optin.text"/></label>
			        </c:when>
			        <c:otherwise>
			            <input type="checkbox" value="true" name="mktgoptin" checked><label for="mktgoptin"><fmt:message key="sf.form.locale.optin.text"/></label>
			        </c:otherwise>
			    </c:choose>
		    </div>
		</c:when>
		<c:otherwise>
			<fmt:message key="sf.form.locale.UStext"/>
			<input type="hidden" name="mktgoptin" value="true">
		</c:otherwise>
	</c:choose>
	
	<cq:include path="tncText" resourceType="sf/components/content/text"/>
</div>