<%--
  Price Spotlight component.
  - This component can be used to display price information of a plan as well as it can also be used to display plan features in small view-port .
  - Plan features details will be fetched form 'Plan Features Container' component and this Plan features container component has to be present on the same page.
--%>

<%@include file="/apps/citrixosd/global.jsp" %>
<%@ page import="com.citrixosd.utils.Utilities,
				java.util.Map,java.util.ArrayList,
				javax.jcr.Property" %>

<%!
    private static final String PLAN_DETAILS_PATH = "jcr:content/mainContent/planfeaturescontaine";
    private static final String PLAN_DATA_NODE = "features";
    private static final String BASIC = "basic";
    private static final String PROFESSIONAL = "professional";
    private static final String CORPORATE = "corporate";
    private static final String VIRTUAL_DATA_ROOM = "virtualDataRoom";
 %>
 
 <%
    //using Utilities to parse structured multi field
    ArrayList<Map<String,Property>> values = new ArrayList<Map<String,Property>>();
    if(currentNode != null && currentNode.hasNode("links")) {
        final Node baseNode = currentNode.getNode("links");
        values = Utilities.parseStructuredMultifield(baseNode);
    }
%>
<c:set var="values" value="<%= values %>"/>

<div class="planAndPricing">
    <c:if test="${not empty properties.mobileHeaderText}">
        <h3>${properties.mobileHeaderText}</h3>
    </c:if>
    <ul class="theme ${properties.spotlight eq 'true'? 'active' :''}" >
        <c:if test="${not empty properties.spotlighttext and properties.spotlight eq 'true'}">
            <li class="callout">${properties.spotlighttext}</li>
        </c:if>
        <c:if test="${not empty properties.title}">
            <li class="head">${properties.title}</li>
        </c:if>
        <li class="content">
            <div class="pricing">
                <p class="messages">${properties.priceHeaderFirst}</p>
                <c:if test="${not empty properties.priceHeaderSecond}">
                    <p class="messages">${properties.priceHeaderSecond}</p>
                </c:if>
                <div class="price clearfix">
                    <span class="number"> <sup>${properties.priceCurrency}</sup>${properties.price}</span>
                      <c:if test="${not empty properties.pricePostfix}">
                         <span class="division">/</span>
                            <span class="message">${properties.pricePostfix}</span>
                      </c:if>
                </div>
                <p class="messages clearfix">${properties.priceFooter}</p>
              </div>     
        </li>
        <li class="head view-features show-for-small only" id="${properties.planFeatureID}">${properties.footerLinkLabel}</li>
        <c:if test="${not empty values}">
            <div class="features">
                <c:forEach var="item" items="${values}">
                    <p class="featuresList">${item.text.string}</p>
                </c:forEach>
            </div>
        </c:if>
    </ul>
    
    <%--display selected plan details form Plan features container component, if Plan details or present on the page in maincontent paragraph  --%>
    <c:if test="${not empty properties.showPlan and properties.showPlan eq 'true' and not empty properties.selectedPlan}">
        <%
            final ArrayList<Map<String, Property>> planFetures = new ArrayList<Map<String, Property>>();
             if (currentPage!= null ) {
                 Resource childResource = resourceResolver.getResource(currentPage.getPath());
                 Node childNode = childResource.adaptTo(Node.class);
                try {
                     Node jcrContent = childNode.getNode(PLAN_DETAILS_PATH);
                     Node childrenNodes = jcrContent.getNode(PLAN_DATA_NODE);
                     planFetures.addAll(Utilities.parseStructuredMultifield(childrenNodes));
                } catch (RepositoryException re) {
                     re.printStackTrace();
                }
              }
        %>
        <c:set var="planFetures" value="<%= planFetures %>"/>
        <c:set var="basic" value="<%= BASIC %>"/>
        <c:set var="professional" value="<%= PROFESSIONAL %>"/>
        <c:set var="corporate" value="<%= CORPORATE %>"/>
        <c:set var="virtualDataRoom" value="<%= VIRTUAL_DATA_ROOM %>"/>
        
        <div class="compareTable">
            <c:forEach items="${planFetures}" var="feature">
             <div class="row">
                <div class="small-6 columns">
                    <div class="left-row plus">
                        ${feature.featureLabel.string}
                    </div>
                </div>
                  <div class="small-6 columns">
                    <c:if test="${not empty properties.selectedPlan and properties.selectedPlan eq basic}">
                        <c:choose>
                            <c:when test="${not empty feature.basicTextValue.string and feature.showTextValue.string eq 'true'}">
                                <p>${feature.basicTextValue.string}</p>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${feature.enableFeatureForBasic.string eq 'true'}">
                                        <p class="check"><span class="icon-SolidCheck"></span></p>
                                    </c:when>
                                    <c:when test="${empty feature.enableFeatureForBasic.string}">
                                        <p class="check icon-X"></p>
                                    </c:when>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <c:if test="${not empty properties.selectedPlan and properties.selectedPlan eq professional}">
                      <c:choose>
                          <c:when test="${not empty feature.professionalTextValue.string and feature.showTextValue.string eq 'true'}">
                              <p>${feature.professionalTextValue.string}</p>
                          </c:when>
                          <c:otherwise>
                              <c:choose>
                                  <c:when test="${feature.enableFeatureForProfessional.string eq 'true'}">
                                      <p class="check"><span class="icon-SolidCheck"></span></p>
                                  </c:when>
                                  <c:when test="${empty feature.enableFeatureForProfessional.string}">
                                      <p class="check icon-X"></p>
                                  </c:when>
                              </c:choose>
                          </c:otherwise>
                      </c:choose>
                   </c:if>
                   <c:if test="${not empty properties.selectedPlan and properties.selectedPlan eq corporate}">
                       <c:choose>
                           <c:when test="${not empty feature.corporateTextValue.string and feature.showTextValue.string eq 'true'}">
                               <p>${feature.corporateTextValue.string}</p>
                           </c:when>
                           <c:otherwise>
                               <c:choose>
                                   <c:when test="${feature.enableFeatureForCorporate.string eq 'true'}">
                                       <p class="check"><span class="icon-SolidCheck"></span></p>
                                   </c:when>
                                   <c:when test="${empty feature.enableFeatureForCorporate.string}">
                                       <p class="check icon-X"></p>
                                   </c:when>
                               </c:choose>
                           </c:otherwise>
                       </c:choose>
                   </c:if>
                   <c:if test="${not empty properties.selectedPlan and properties.selectedPlan eq virtualDataRoom}">
                       <c:choose>
                           <c:when test="${not empty feature.virtualDataRoomTextValue.string and feature.showTextValue.string eq 'true'}">
                               <p>${feature.virtualDataRoomTextValue.string}</p>
                           </c:when>
                           <c:otherwise>
                               <c:choose>
                                   <c:when test="${feature.enableFeatureForVirtualDataRoom.string eq 'true'}">
                                       <p class="check"><span class="icon-SolidCheck"></span></p>
                                   </c:when>
                                   <c:when test="${empty feature.enableFeatureForVirtualDataRoom.string}">
                                       <p class="check icon-X"></p>
                                   </c:when>
                               </c:choose>
                           </c:otherwise>
                       </c:choose>
                    </c:if>
                 </div>
                 <div class="row">
                    <div class="small-12 columns features" >
                        <p>${feature.featureDesc.string}</p>
                    </div>
                </div>
             </div>
            </c:forEach>
        </div>
    </c:if>
</div>
<div class="clearBoth"></div>