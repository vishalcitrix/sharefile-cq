<%--
    Unity Navigation - vishal.gupta@citrix.com
--%>

<%@page import="java.util.ArrayList,
				java.util.Map,
				com.citrixosd.utils.Utilities,
				com.citrixosd.SiteUtils"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<cq:include script="products.jsp"/>

<% 
    //get text from cloud nodes
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("clouds")) {
        final Node baseNode = currentNode.getNode("clouds");
        values = Utilities.parseStructuredMultifield(baseNode);
    }
%>

<c:set var="values" value="<%= values %>"/>
<c:set var="domain" value="<%= SiteUtils.getPropertyFromSiteRoot("domain",currentPage) %>"/>
<c:set var="unityNavProductUrl" value="<%= SiteUtils.getUnityNavProductUrl(currentPage) %>"/>
<c:set var="unityNavUrl" value="<%= SiteUtils.getPropertyFromSiteRoot("unityNavUrl",currentPage) %>"/>
<c:choose>
  <c:when test="${not empty unityNavProductUrl}">
    <c:set var="prodLogoLink" value="${unityNavProductUrl}"/>
  </c:when>
  <c:when test="${not empty unityNavUrl}">
    <c:set var="prodLogoLink" value="${unityNavUrl}"/>
  </c:when>
  <c:otherwise>
    <c:set var="prodLogoLink" value="${domain}"/>
  </c:otherwise>
</c:choose>

<div id="body-overlay"></div>
<div class="topNav">
    <header>
    	<div class="logo">
        	<a class="hide-for-small-only" href="${prodLogoLink}"><span></span></a>
        	<a class="show-for-small-only" href="${prodLogoLink}"><span></span></a>
        </div>
        <div class="link-group">
            <div class="left">
                <swx:setWCMMode mode="READ_ONLY">
                    <cq:include script="mainLinkSet.jsp"/>
                </swx:setWCMMode>
            </div>
            <%-- Side nav --%>
            <div id="menu">
                <div class="menu-small hide-for-medium-up">
                    <span class="open"><fmt:message key="unityNav.menu.labelsmall"/></span>
                    <span class="close"><fmt:message key="unityNav.menu.close.labelsmall"/></span>
                </div>
                <div class="menu show-for-medium-up"><fmt:message key="unityNav.menu.labelmedup"/></div>
            </div>
        </div>
        <div class="clearBoth"></div>
    </header>
    <div class="product-drawer-small">
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include script="secondaryLinkSetSmall.jsp"/>
            <cq:include script="mainLinkSet.jsp"/>
            <c:choose>
            <c:when test="${fn:length(values) > 0}">
                <div class="more expand">
                	<fmt:message key="unityNav.menu.moreFromCitrix"/>
                    <span></span>
                </div>
                <div class="products">
                    <c:forEach items="${values}" var="item">
                        <div class="columns">
                            ${item.cloudtext.string}
                        </div>
                    </c:forEach>
                    <div class="clearBoth"></div>
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Please check for cloud nodes under global header.</div></c:if>
            </c:otherwise>
        </c:choose>
        </swx:setWCMMode>
    </div>
    <div class="secondary-link-group">
        <%-- Secondary links display --%>
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include script="secondaryLinkSet.jsp"/>
        </swx:setWCMMode>
        <div class="clearBoth"></div>
    </div>
</div>