<%-- Event Resource Filters --%>

<%@ page import="com.citrixosdRedesign.constants.ResourceConstants,
				 com.day.cq.tagging.TagManager,
				 com.citrixosdRedesign.utils.ResourceUtils"%>
				
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

    String[] products = properties.get(ResourceConstants.PRODUCTS, String[].class);
    String[] industries = properties.get(ResourceConstants.INDUSTRIES, String[].class);
    String[] topics = properties.get(ResourceConstants.TOPICS, String[].class);    
%>

<c:set var="products" value="<%= ResourceUtils.getTags(products, tagManager)%>"/>
<c:set var="industries" value="<%= ResourceUtils.getTags(industries, tagManager)%>"/>
<c:set var="topics" value="<%= ResourceUtils.getTags(topics, tagManager)%>"/>

<div class="search-refinements-container row">
	<%-- Products --%>
	<c:if test="${not empty products && fn:length(products) > 1}">
		<select id="event-product" class="event-filter colums large-3 medium-3">
			<option value=""><b><fmt:message key="select.products"/></b></option>
			<c:forEach items="${products}" var= "product">
                <option value="${product.tagID}">${product.title}</option>
            </c:forEach>
		</select>
	</c:if>
	
	<%-- Industries --%>
	<c:if test="${not empty industries && fn:length(industries) > 1}">
		<select id="event-industry" class="event-filter colums large-3 medium-3">
			<option value=""><b><fmt:message key="select.industries"/></b></option>
			<c:forEach items="${industries}" var= "industry">
                <option value="${industry.tagID}">${industry.title}</option>
            </c:forEach>
		</select>
	</c:if>
	
	<%-- Topics --%>
	<c:if test="${not empty topics && fn:length(topics) > 1}">
		<select id="event-topic" class="event-filter colums large-3 medium-3">
			<option value=""><b><fmt:message key="select.topics"/></b></option>
			<c:forEach items="${topics}" var= "topic">
                <option value="${topic.tagID}">${topic.title}</option>
            </c:forEach>
		</select>
	</c:if>
</div>