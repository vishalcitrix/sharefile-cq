<%--
  Category/Feature Heading component
  kaverappa.subramanya@citrix.com
  vishal.gupta@citrix.com - added option to align icon on top of text and color
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<c:set var="verticalAlign" value="${properties.verticalAlign}"/>
<c:set var="color" value="${properties.color}"/> 
<c:set var="categoryValue" value="${properties.categoryIcon}"/> 
<c:set var="headingValue" value="${properties.headingText}"/> 
<c:set var="warningMessage" value="Please finish editing"/> 

<c:if test="${not empty categoryValue and not empty headingValue}">
	<c:choose>
		<c:when test="${not verticalAlign}">
			<div class="${categoryValue} ${color}">     
				<cq:text value="${headingValue}" tagName="h2" escapeXml="false"/>   
				<c:if test="${empty categoryValue or empty headingValue}"> <h2 class="warningMessage">${warningMessage}</h2></c:if>
			</div>
			<div class="clearBoth"></div>	
		</c:when>
		<c:otherwise>
			<div class="vAlign ${color}">
				<div class="${categoryValue}"></div>
				<cq:text value="${headingValue}" tagName="h2" escapeXml="false"/>
				<cq:include path="iconHeadingText" resourceType="sf/components/content/text"/>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>