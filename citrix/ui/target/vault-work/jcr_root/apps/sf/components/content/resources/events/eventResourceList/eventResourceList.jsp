<%-- Events Resource List --%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils,
                 com.day.cq.wcm.api.WCMMode,
                 org.apache.jackrabbit.commons.JcrUtils,
                 com.day.cq.commons.jcr.JcrConstants,
                 java.util.Comparator,
                 java.util.Collections,
                 java.util.List,
                 com.citrixosdRedesign.models.resource.Pagination" %>

<%@include file="/apps/citrixosd/global.jsp"%>
<%
    response.setHeader("Dispatcher", "no-cache");
%>
<%!
    public static class dateComparator implements Comparator<com.citrixosdRedesign.models.resource.Resource> {
        public int compare(com.citrixosdRedesign.models.resource.Resource r1, com.citrixosdRedesign.models.resource.Resource r2) {
            if(r1.getEventStartDate() == null || r1.getEventStartDate() == null)
            	return 0;
            else
            	return r1.getEventStartDate().compareTo(r2.getEventStartDate());
        }
    }
%>
<%
    //Create itself if it is not already created because this is required for filtering
    if(currentNode == null && WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
        final Session session = resourceResolver.adaptTo(Session.class);
        final Node newNode = JcrUtils.getOrCreateByPath(resource.getPath(), JcrConstants.NT_UNSTRUCTURED, session);
        newNode.setProperty("sling:resourceType", component.getResourceType());
        session.save();
        response.sendRedirect(currentPage.getPath());
    }

    Pagination<com.citrixosdRedesign.models.resource.Pagination> pagination = ResourceUtils.getResourceList(resource, currentPage, request, null, currentDesign);
    List<com.citrixosdRedesign.models.resource.Resource> resources = ResourceUtils.getResourceList(resource, currentPage, request, null, currentDesign).getList();
    Collections.sort(resources,new dateComparator());
    pagination.setList(resources);
%>

<c:set var="resourceList" value="<%= pagination %>" scope="request"/>

<div id="${resourceList.id}">
    <cq:include script="content.jsp"/>
</div>