<%--
    Secondary Nav Link Set - krishna.selvaraj@citrix.com
--%>

<%@page import="java.util.Map,
				java.util.ArrayList,
				com.day.cq.wcm.api.WCMMode,
				com.citrixosd.utils.Utilities"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<% 
    ArrayList<Map<String, Property>> links = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("secondaryLinkSet")) {
        final Node baseNode = currentNode.getNode("secondaryLinkSet");
        links = Utilities.parseStructuredMultifield(baseNode);
    }
%>

<c:set var="links" value="<%= links %>" />
<c:set var="currentPagePath" value="<%= currentPage.getPath() %>"/>
<c:set var="currentPageParentPath" value="<%= currentPage.getParent().getPath() %>"/>

<div class="secondaryLinkSet link-group">
	<ul>
		<c:forEach var="entry" items="${links}">                   
            <c:if test="${not empty entry.paragraphReference.string}">
				<li class="more expand<c:if test="${entry.path.string eq currentPagePath || entry.path.string eq currentPageParentPath}"> active</c:if>">
					<a href="javascript:void(0)"><fmt:message key="${entry.text.string}"/></a>
				</li>
            </c:if>
                       
            <c:if test="${empty entry.paragraphReference.string}">
               <li class="${entry.classOption.string}<c:if test="${entry.path.string eq currentPagePath || entry.path.string eq currentPageParentPath}"> active</c:if>">
                   <a href="${entry.path.string}" rel="${entry.linkOption.string}">
                       <c:if test="${not empty entry.classOption.string}">
                           <span></span>
                       </c:if>
                       <fmt:message key="${entry.text.string}"/>                       
                   </a>
               </li>
            </c:if>
		</c:forEach>
	</ul>
</div>
<div class="tertiaryNavContainer">
	<c:forEach var="entry" items="${links}">                   
		<c:if test="${not empty entry.paragraphReference.string}">
			<div class="tertiaryNav">
				<sling:include path="${entry.paragraphReference.string}"/>
			</div>
		</c:if>
	</c:forEach>
</div>