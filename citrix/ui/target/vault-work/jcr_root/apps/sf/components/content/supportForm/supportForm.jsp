<%-- SF Support Request Form component krishna.Selvaraj@citrix.com --%>
<%@page import="java.util.ArrayList,java.util.List,
				java.util.Map,java.util.HashMap,
				com.citrixosd.utils.Utilities,
				com.citrixosd.utils.ContextRootTransformUtil" %>
<%@include file="/apps/citrixosd/global.jsp"%>

<% 
    //using Utilities to parse structured multi field 
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>(); 
    if(currentNode != null && currentNode.hasNode("radio")) { 
        final Node baseNode = currentNode.getNode("radio");
        values = Utilities.parseStructuredMultifield(baseNode);
    } 
%>

<c:set var="values" value="<%= values %>" />
<c:set var="fieldId" value="<%= properties.get("fieldId","") %>"/>
<c:set var="fieldName" value="<%= properties.get("fieldName","") %>"/>
<div class="custom-form supportForm">
    <form enctype="multipart/form-data" id="support">
        <input type="hidden" name="fromEmail" id="fromEmail" value="${properties.fromEmail}" />
        <input type="hidden" name="toEmail" id="toEmail" value="${properties.toEmail}" />
        <input type="hidden" name="emailSubject" id="emailSubject" value="${properties.emailSubject}" />
        <input type="hidden" name="confirmationMessage" id="confirmationMessage" value="${properties.confirmationMessage}" />
        <input type="hidden" name="errorMessage" id="errorMessage" value="${properties.errorMessage}" />
        <input type="hidden" name="successUrl" id="successUrl" value="<%= ContextRootTransformUtil.transformedPath(properties.get("successUrl",""),request) %>" />          
        <input type="hidden" name="maxAttemptMesssage" id="maxAttemptMesssage" value="${properties.maxAttemptMesssage}" />
        
        <div class="heading"><fmt:message key="sf.form.heading"/></div>
        <div class="subHeading"><fmt:message key="sf.form.subheading"/></div>
        
        <div class="formErrorMsg" id="formErrorMsg"> 
            <p> Please review the errors below </p>
        </div>
        
        <input errormsg="<fmt:message key="sf.form.error.contact"/>" trackerror="true" class="required" constraint="name" placeholder="<fmt:message key="sf.form.placeholder.name"/>" type="text" name="Name" id="firstName" />
        <input errormsg="<fmt:message key="sf.form.error.contact"/>" trackerror="true" class="required" constraint="contact" placeholder="<fmt:message key="sf.form.placeholder.contact"/>" type="text" name="Contact information" id="contact" />
        
        <label><fmt:message key="sf.form.issue"/></label>
        <div class="radioGroup">
            <c:choose>
                <c:when test="${fn:length(values) > 0}">
                    <c:set var="count" value="0" />
                    <c:forEach items="${values}" var="item">
                        <c:set var="count" value="${count +1}" />
                        <div class="radioOption"><input type="radio" <c:if test="${fieldId ne ''}">id="${fieldId}"</c:if> name="${fieldName}" value="${item.optionValue.string}">
                            ${item.optionLabel.string}
                        </div>
                        <c:if test="${fn:length(values) > count}">
                            <div class="link-section">
                                <cq:include path="text${count}" resourceType="/apps/sf/components/content/text" />
                            </div>
                        </c:if>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:if test="${isEditMode || isReadOnlyMode}">
                        <div class="warning">Radio: Please enter radio options</div>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </div>
    
        <textarea name="Optional information" row="5" columns="6" placeholder="<fmt:message key="sf.form.error.description"/>"></textarea>
    
        <div class="section-heading"><fmt:message key="sf.form.upload"/></div>
    
        <!-- The file to upload -->
        
        <div class="formField">
            <div class="fileErrorMessage" id="fileErrorMessage"> That file format wont work. Please try a jpg, gif or png</div>
            <div class="preupload" id="preupload">
               <p class="nofile"> No file selected </p>
               <p class ="formats"> (jpg,gif and png)</p> 
            </div>
            <div class="postUpload" id="postupload">
               <p class="filepath" id="filepath"></p>
            </div>
            <input type="file" accept="text/plain,video/x-ms-wmv,video/mp4,image/jpeg,image/png,image/gif" name="upload" id="upload" class="upload"/>
            <div class="fileButtonWrapper">
                <button type="button" class="fileButton" id="fileButton">Choose File</button>
            </div>
        </div>
                
        <p>
            <input class="submitButton" type="submit" value="Contact Me" id="upload-button-id" />
        </p>
    
        <!-- Placeholders for messages set by event handlers -->
        <p id="upload-status"></p>
        <p id="progress"></p>
        <pre id="result"></pre>
    </form>
</div>