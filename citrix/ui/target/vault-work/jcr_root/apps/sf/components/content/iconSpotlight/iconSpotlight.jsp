<%--
  Icon Spotlight - Option to add icon with RTE component
  	- depends on Icon Heading component to get Icon list and color list
  	vishal.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%
    final String txt = currentNode != null ? currentNode.hasNode("iconSpotLightText") ? currentNode.getNode("iconSpotLightText").getProperty("text").getString() : null : null;
%>

<c:set var="color" value="${properties.color}"/>
<c:set var="iconValue" value="${properties.categoryIcon}"/>
<c:set var="txt" value="<%= txt %>"/>

<c:if test="${not empty iconValue}">
	<c:choose>
		<c:when test="${properties.iconBelow}">
			<div class="hide-for-medium-only ${color}">
				<c:if test="${not empty properties.linkPath}"><a href="${properties.linkPath}"></c:if>
					<div class="${properties.categoryIcon}"></div>
				<c:if test="${not empty properties.linkPath}"></a></c:if>
				<cq:include path="iconSpotLightText" resourceType="sf/components/content/text"/>
			</div>
			<div class="show-for-medium-only ${color}">
				${txt}
				<c:if test="${not empty properties.linkPath}"><a href="${properties.linkPath}"></c:if>
					<div class="${properties.categoryIcon} icon"></div>
				<c:if test="${not empty properties.linkPath}"></a></c:if>
			</div>
		</c:when>
		<c:otherwise>
			<div class="${color}">
				<c:if test="${not empty properties.linkPath}"><a href="${properties.linkPath}"></c:if>
					<div class="${properties.categoryIcon}"></div>
				<c:if test="${not empty properties.linkPath}"></a></c:if>
				<cq:include path="iconSpotLightText" resourceType="sf/components/content/text"/>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>