<%@include file="/apps/citrixosd/global.jsp"%>

<div class="row<c:if test="${not empty properties.class}"> class java.util.Properties</c:if>">
	<div>
	    <cq:include path="accordiontitle" resourceType="foundation/components/parsys"/>
	    <div class="toggle-all">
	    	<a href="#" id="showAll">
	    		<fmt:message key="show.all"/>
	    	</a>
	    	<span class="toggle-splitter">|</span>
	    	<a href="#" id="hideAll">
	    		<fmt:message key="hide.all"/>
	    	</a>
	   	</div>
	</div>
	<div class="accordian-container ${(isPreviewMode || isDisabledMode || isPublishInstance) ? 'active' : 'inactive'}">
	    <cq:include path="accordionpar" resourceType="citrixosd/components/content/accordion/accordionparsys" />
	</div>
</div>