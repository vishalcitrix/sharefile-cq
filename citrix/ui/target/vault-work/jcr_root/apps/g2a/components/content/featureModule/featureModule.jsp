<%--
    General Spotlight Two component.
  
    This component is comprised of 1) a Logo Component 2) Two text fields, one of which is a
    custom <h5> and another is a <p> 3) a parsys area for main content. The main responsibility 
    of this component is to edit and manage the title and description portion of this component.
    The parsys provides the most flexible features, allowing authors to add versatile and necessary
    content.
    
    The layout of the component is as follows: 
        - The top portion has an image sprite and can be managed through its own single image sprite dialog. 
        - The middle portion includes optional text.          
        
    *** The component has a dialog with two tabs (Text Area, Bottom-Left Sprite). This dialog manage the text contained
        in the component and the sprite at the BOTTOM-LEFT of the component. There is also the check box for including 
        GO TO TOP Banner. 
        
    *** Logo component (single sprite image) has it's own dialog and can be used by clicking on it's respective 
        dialog outline.
    
     
    * Layout Notes
        - Title text field holds max of 21 characters to prevent overflow
        - Description text field holds max of 85 characters to prevent overflow
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String TITLE_PROPERTY = "title";
    private static final String DESCRIPTION_PROPERTY = "description";
%>

<div class="container">
    <div class="topsection-gradient">
        <div class="icon">
            <cq:include path="sprite" resourceType="g2a/components/content/spotlightLogo" />
        </div>    
        <h5><%=properties.get(TITLE_PROPERTY, "")%></h5>
        <p><%=properties.get(DESCRIPTION_PROPERTY, "")%></p>    
    </div>
    <hr/>
    <div class="content-container">
        <cq:include path="par" resourceType="foundation/components/parsys" />
    </div>

</div>

<div class="clearBoth"></div>