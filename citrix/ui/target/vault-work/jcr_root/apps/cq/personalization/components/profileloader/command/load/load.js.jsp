<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.


@deprecated. Use json load.json instead. XSS non property supported.

--%><%@ page import="com.day.cq.collab.commons.CollabUtil,
                    com.day.cq.security.User,
                    com.day.cq.security.profile.Profile,
                    com.day.cq.security.profile.ProfileManager,
                    com.day.cq.xss.ProtectionContext,
                    com.day.cq.xss.XSSProtectionService,
                    java.util.Date,
                    com.day.cq.commons.Externalizer" %><%!
%><%@include file="/libs/foundation/global.jsp" %><%

    String authorizableId = request.getParameter("authorizableId");
    Profile profile = null;
    ProfileManager pMgr = sling.getService(ProfileManager.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);    

    //anonymous - special case
    if( !"anonymous".equals(authorizableId)) {
        if (authorizableId != null) {
            try {
                profile = pMgr.getProfile(authorizableId, resourceResolver.adaptTo(Session.class));
            } catch (RepositoryException e) {
                slingResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED, "");
            }
        } else {
                    profile = resourceResolver.adaptTo(User.class).getProfile();
        }
        if (profile != null) {
            out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('avatar','" + CollabUtil.getAvatar(profile) + "');");
            out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('authorizableId','" +
                    xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getAuthorizable().getID()) + "');");
            out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('path','" + profile.getPath() + "');");
            out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('formattedName','" +
                    xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getFormattedName()) + "');");
            for (String key : profile.keySet()) {
                if (!key.startsWith("jcr:") && !key.startsWith("sling:") && !key.startsWith("cq:last")) {
                    Object o = profile.get(key);
                    if (o != null && o instanceof String) {
                        String v = o.toString().replaceAll("'","\\\\'");
                        out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('" + key + "','" +
                                xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,v) + "');");
                    }
                }
            }

            Date created = profile.get("memberSince", Date.class);
            if( created == null) {
                created = profile.get("jcr:created", Date.class);
            }
            if( created != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy h:mm a", slingRequest.getLocale());
                out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('memberSince','" + df.format(created) + "');");
            }

            Date birthday = profile.get("birthday", Date.class);
            if( birthday != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy", slingRequest.getLocale());
                out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('birthday','" + df.format(birthday) + "');");
            }
        }
    } else {
        Externalizer externalizer = sling.getService(Externalizer.class);

        String absoluteDefaultAvatar = "";
        if(externalizer != null){
            absoluteDefaultAvatar = externalizer.relativeLink(slingRequest, CollabUtil.DEFAULT_AVATAR);
        }

        out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('authorizableId','anonymous');");
        out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('formattedName','Anonymous Surfer');");
        out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('path','/home/users/a/anonymous');");
        out.println("CQ_Analytics.ProfileDataMgr.addInitProperty('avatar','" + absoluteDefaultAvatar + "');");
    }
%>