<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<body>

    <cq:include script="header.jsp" />
    
    <div id="main" class="content">
    
		<cq:include path="par" resourceType="swx/component-library/components/content/single-par"/>
 
    </div>

    <cq:include script="footer.jsp" />
    
    <cq:include script="lightbox.jsp" />
    
</body>