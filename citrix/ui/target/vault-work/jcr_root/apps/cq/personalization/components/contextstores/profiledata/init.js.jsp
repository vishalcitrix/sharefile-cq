<%--
  ~
  ~ ADOBE CONFIDENTIAL
  ~ __________________
  ~
  ~  Copyright 2011 Adobe Systems Incorporated
  ~  All Rights Reserved.
  ~
  ~ NOTICE:  All information contained herein is, and remains
  ~ the property of Adobe Systems Incorporated and its suppliers,
  ~ if any.  The intellectual and technical concepts contained
  ~ herein are proprietary to Adobe Systems Incorporated and its
  ~ suppliers and are protected by trade secret or copyright law.
  ~ Dissemination of this information or reproduction of this material
  ~ is strictly forbidden unless prior written permission is obtained
  ~ from Adobe Systems Incorporated.
  --%><%@ page import="com.day.cq.collab.commons.CollabUtil,
                       com.day.cq.commons.Externalizer,
                       com.day.cq.commons.JSONWriterUtil,
                       com.day.cq.commons.TidyJSONWriter,
                       com.day.cq.commons.date.DateUtil,
                       com.day.cq.security.profile.Profile,
                       com.day.cq.security.profile.ProfileManager,
                       com.day.cq.wcm.api.WCMMode,
                       com.day.cq.xss.ProtectionContext,
                       com.day.cq.xss.XSSProtectionService,
                       org.apache.sling.api.SlingHttpServletRequest,
                       org.apache.sling.commons.json.io.JSONWriter,
                       java.io.StringWriter,
                       java.text.DateFormat,
                       java.util.Date"
        contentType="text/javascript" %><%!
%><%@ include file="/libs/foundation/global.jsp" %><%
%><%!

    void setProfileInitialData(JSONWriter writer, ProfileManager pMgr,
                               SlingHttpServletRequest slingRequest,
                               String absoluteDefaultAvatar,
                               XSSProtectionService xss) throws Exception {

        writer.object();
        final Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
        Profile profile = pMgr.getProfile(session.getUserID(), session);
        if (profile != null) {
            String avatar = CollabUtil.getAvatar(profile, profile.getPrimaryMail(),absoluteDefaultAvatar);
            //increate avatar size
            avatar = avatar == null ? "" : avatar.replaceAll("\\.32\\.",".80.");
            writer.key("avatar").value(avatar);
            writer.key("path").value(profile.getPath());

            Boolean isLoggedIn = profile.getAuthorizable().getID() != null && !profile.getAuthorizable().getID().equals("anonymous");
            writer.key("isLoggedIn").value(isLoggedIn);
            writer.key("isLoggedIn" + JSONWriterUtil.KEY_SUFFIX_XSS)
            		.value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,isLoggedIn.toString()));

            writer.key("authorizableId")
                    .value(profile.getAuthorizable().getID());
            writer.key("authorizableId" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getAuthorizable().getID()));

            writer.key("formattedName")
                    .value(profile.getFormattedName());
            writer.key("formattedName" + JSONWriterUtil.KEY_SUFFIX_XSS)
                    .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getFormattedName()));

            for (String key : profile.keySet()) {
                if (!key.startsWith("jcr:") && !key.startsWith("sling:") && !key.startsWith("cq:last")) {
                    String s = profile.get(key, String.class);
                    s = s != null ? s : "";
                    writer.key(key)
                            .value(s);
                    writer.key(key + JSONWriterUtil.KEY_SUFFIX_XSS)
                            .value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,s));
                }
            }

            Date created = profile.get("memberSince", Date.class);
            if( created == null) {
                created = profile.get("jcr:created", Date.class);
            }
            if( created != null ) {
                DateFormat df = DateUtil.getDateFormat("d MMM yyyy h:mm a", slingRequest.getLocale());
                writer.key("memberSince")
                    .value(df.format(created));
            }

            Date birthday = profile.get("birthday", Date.class);
            if( birthday != null ) {
                DateFormat df = DateUtil.getDateFormat("d MMM yyyy", slingRequest.getLocale());
                writer.key("birthday")
                    .value(df.format(birthday));
            }
        }
        writer.endObject();
    }

%><%
    Externalizer externalizer = sling.getService(Externalizer.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);
    boolean isDisabled = WCMMode.DISABLED.equals(WCMMode.fromRequest(slingRequest));

    String absoluteDefaultAvatar = "";
    if(externalizer != null){
        absoluteDefaultAvatar = externalizer.relativeLink(slingRequest, CollabUtil.DEFAULT_AVATAR);
    }

    ProfileManager pMgr = sling.getService(ProfileManager.class);
    StringWriter buf = new StringWriter();

    TidyJSONWriter writer = new TidyJSONWriter(buf);
    writer.setTidy(true);
    try {
        setProfileInitialData(writer, pMgr, slingRequest, absoluteDefaultAvatar, xss);
    } catch (Exception e) {
        log.error("Error while generating JSON profile initial data", e);
    }

%>if (CQ_Analytics && CQ_Analytics.ProfileDataMgr) {
    CQ_Analytics.ProfileDataMgr.addListener("update", function(event, property) {
        var authorizableId = this.getProperty("authorizableId");
        if (!authorizableId || authorizableId == "anonymous") {
            jQuery(".cq-cc-profile-not-anonymous").hide();
            jQuery(".cq-cc-profile-anonymous").show();
        } else {
            jQuery(".cq-cc-profile-not-anonymous").show();
            jQuery(".cq-cc-profile-anonymous").hide();
        }
    });

    <%if (!isDisabled) { %>
        CQ_Analytics.ProfileDataMgr.loadInitProperties({
            "authorizableId": "anonymous",
            "formattedName": "Anonymous Surfer",
            "path": "/home/users/a/anonymous",
            "avatar": "<%=absoluteDefaultAvatar%>"
        });
    <%} else {%>
        CQ_Analytics.ProfileDataMgr.loadInitProperties(<%=buf%>);
    <%}%>

    CQ_Analytics.ProfileDataMgr.init();
}
