<%@include file="/libs/foundation/global.jsp"%>

<%-- FOOTER PLACE HOLDER --%>
<c:if test="${env == 'desktop'}">
<div class="stickyHeader">
    <div id="action-header" class="action-nav">
        <div class="container">
            <div style="width: 540px; float:left">
                <h2>Get fast, easy and secure remote access from any device.</h2>
            </div>
            <ul class="action">
                <li>            
                    <div class="button buttonLeft">
                        <div class="button-container">
                            <a class="button orange" href="">Try It Free</a>
                            <div class="subText">
                                <a href="">30-Day GoToMyPC Trial</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="divider"><span></span></li>
                <li class="split">or</li>
                <li>
                    <div class="buttonRight button">
                        <div class="button-container">
                            <a class="button green" href="">Buy It Now</a>
                            <div class="subText">
                                <a href="">Buy GoToMyPC Online</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
</c:if>