<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="java.util.Locale, com.day.cq.wcm.api.WCMMode"%>
<%@page import="org.osgi.service.cm.ConfigurationAdmin"%>
<%@page import="org.osgi.service.cm.Configuration"%>



<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
Configuration conf = sling.getService(org.osgi.service.cm.ConfigurationAdmin.class).getConfiguration("com.citrix.marketing.cq.commerce.configuration.SelfServiceConfigurationServiceImpl");
String oauth_base_url = (String) conf.getProperties().get("oauth.baseurl");
String oauth_client_id = (String) conf.getProperties().get("oauth.clientid");
String oauth_redirect_url = (String) conf.getProperties().get("oauth.redirecturl");
%>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
    public static final String ASYNCHRONOUS_ANALYTICS_PROPERTY = "asynchronousAnalytics";
%>

<%
    response.setHeader("Dispatcher", "no-cache");
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String designPathOrig = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
    
    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>

<%-- Customer Home Page Redirect --%>
<cq:include script="customerRedirect.jsp" />

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="favIcon" value="<%= SiteUtils.getFavIcon(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />
<c:set var="canonicalUrl" value="<%= SiteUtils.getPropertyFromSiteRoot("canonicalUrl", currentPage) %>"/>
<c:set var="oauth_base_url" value="<%=oauth_base_url %>" />
<c:set var="oauth_client_id" value="<%=oauth_client_id %>"/>
<c:set var="oauth_redirect_url" value="<%=oauth_redirect_url %>"/>

<head>
     <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
      <script type="text/javascript" src="https://weblibrary.cdn.citrixonline.com/oauthclientlibrary/1.0.2/oauth-client-library.min.js"></script>

    <script>
    var settings = {
            //Authentication service base URL
            oauth_base_url: "${oauth_base_url}",
            //The client identifier
            oauth_client_id: "${oauth_client_id}",
            //The redirect uri as registered by the client
            oauth_redirect_url: "${oauth_redirect_url}"
        };
    </script>
    <cq:includeClientLib categories="cq.selfservice"/>

    <%--Canonical tag --%>
    <c:if test="${(not empty canonicalUrl)}">
        <link rel="canonical" href="${canonicalUrl}" />
    </c:if>
    
    <%-- Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width" />
    <cq:include script="meta.jsp"/>
    <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/static/css/static.css" type="text/css">
    
    <%--CSS --%>
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="${designPath}/css/app-ie.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" href="${designPath}/css/app.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie9.css" type="text/css">
    <![endif]-->
    <!--[if gte IE 9]><link rel="stylesheet" href="${designPath}/css/app.css" type="text/css"><![endif]-->
    <!--[if !IE]><!--> <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/app.css" type="text/css"><!--<![endif]-->
    
    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/css/static/images/${not empty favIcon ? favIcon : 'favicon'}.ico">
    
    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">

    <%-- DNT --%>
    <%
        Locale locale = null;
        final Page localePage = currentPage.getAbsoluteParent(2);
        if (localePage != null) {
            try {
                locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
            } catch (Exception e) {
                locale = request.getLocale();
            }
        }
        else
            locale = request.getLocale();
    %>
  

    
    <%-- Javascripts - Place after including jQuery Lib
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script> --%>
    
    <%-- Adding Pollyfil --%>
    <!--[if lt IE 9]>
        <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/respond.js"></script>
    <![endif]-->



	 <script type="text/javascript" src="https://apisandboxstatic.zuora.com/Resources/libs/hosted/1.2.0/zuora-min.js"></script>
</head>