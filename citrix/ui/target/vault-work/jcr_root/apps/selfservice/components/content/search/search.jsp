<%--
    Search Component - The search functionality for Support Home Page

    prabhu.shankar@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>


<c:set var="searchTitle" value="${properties.searchTitle}"/>

<form action="http://support.citrixonline.com/en_US/Meeting/search">
    <div class="row">
        <div class="column small-12">
            <div class="search">
                <div class="row collapse">
                    <div class="small-10 column">
                        <input placeholder="${searchTitle}" id="tags" name="search" class="form-control">
                    </div>
                    <div class="small-2 column">
                        <button type="submit" class="button postfix"><i class="citrix-icon citrix-icon-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>