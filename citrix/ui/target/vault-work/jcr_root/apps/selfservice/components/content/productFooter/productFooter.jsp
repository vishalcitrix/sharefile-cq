<%--
    Footer component.
    - Includes Logo and County Selector Component
    
    prabhu.shankar@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<footer id="footer-stick">
  <div class="footer-logo-container">
    <div class="row">
      <div class="col-sm-7 col-xs-7">
        <p class="language"><i class="citrix-icon citrix-icon-down-arrow"> </i><i class="citrix-icon citrix-icon-globe"> </i>English</p>
        <p class="copyright">&copy;1997-2014 Citrix Online, LLC. All rights reserved.</p>
      </div>
    </div>
  </div>
</footer>