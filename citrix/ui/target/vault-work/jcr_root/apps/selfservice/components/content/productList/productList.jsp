<%--
    Search Component - The search functionality for Support Home Page

    prabhu.shankar@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<script>
    $(function () {
        ProductList.init();
    });
</script>
<section class="productListTable content-block">
    <div role="grid" class="standard-list productList">
      <div class="tbody">
          <%--<div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-gotomeeting">&nbsp;</span>GoToMeeting Pro</div>
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-gototraining">&nbsp;</span>GoToTraining</div> 
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-gotowebinar">&nbsp;</span>GoToWebinar</div> 
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>             
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-podio">&nbsp;</span>Podio</div>
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>   
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-sharefile">&nbsp;</span>ShareFile</div> 
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div> 
              </div>
          </div>
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-shareconnect">&nbsp;</span>ShareConnect</div> 
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>  
          <div class="trow">
              <div class="row">
                  <div class="small-6 columns"><span class="icon-gotopc">&nbsp;</span>GoToMyPC</div> 
                  <div class="small-2 columns">10 units</div>
                  <div class="small-2 columns">$36/unit</div>
                  <div class="small-2 columns">$360/mo</div>
              </div>
          </div>  --%>
      </div>
    </div>
</section>