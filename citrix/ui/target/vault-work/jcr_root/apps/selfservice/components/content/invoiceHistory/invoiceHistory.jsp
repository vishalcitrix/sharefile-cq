<%--

Invoice History Component

--%>

<%@include file="/libs/foundation/global.jsp"%>

<script>
    $(function(){
        InvoiceHistory.init();
    });
</script>

<div class="row" id="invoiceHistory">
    <div class="tabs large-12 columns animated fadeIn">
        <div class="large-12 columns" id="tabList">
            <ul class="tab-links">

            </ul>
        </div>

        <div class="row">
            <div class="large-12 columns">
                <div class="row paymentPage">
                    <div class="invoiceView">
                        <div class="tab-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>