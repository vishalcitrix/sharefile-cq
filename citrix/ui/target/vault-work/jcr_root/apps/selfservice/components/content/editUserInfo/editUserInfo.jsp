<%--

  editUserInfo component.

  this form will be used to update the user details.

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
    // TODO add you code here
%>
<%@page import="org.osgi.service.cm.ConfigurationAdmin"%>
<%@page import="org.osgi.service.cm.Configuration"%>
<%
Configuration conf = sling.getService(org.osgi.service.cm.ConfigurationAdmin.class).getConfiguration("com.citrix.marketing.cq.commerce.configuration.SelfServiceConfigurationServiceImpl");
String scim_password_recovery = (String) conf.getProperties().get("scim.identity.password.recovery.url"); 
%>
<c:set var="scim_password_recovery" value="<%=scim_password_recovery %>"/>  
<script>
    
   $(document).ready(function(){
        editUserInfo.init();
     Lightbox.init();
   });

</script> 




            
                <a rel="lightbox" href="/content/selfservice/en_us/homepage/userinfo/jcr:content/mainContent/" id="editInfo" class="btn btn-invis editBtn editInfoButton" style="right:0px;">Edit</a>
              
                <div class = "noBillID hide">
                   <p>You have no account with us.</p>
                </div>
              
               <div class="account-page"> <ul  id="account-display" class="large-12 columns">
                    <li>

                        <p id="account-name"></p>
                    </li><br>
                    <li>
                        <p id="account-bus-name"></p>
                    </li>
                    <li>
                        <p id="account-address1" ></p>
                    </li>
                    <li>
                        <p id="account-address2" ></p>
                    </li>
                    <li>
                        <p id="account-address" ></p>
                    </li>
                    <li><br>
                        <p id="account-phone" ></p>
                    </li>
                    <li>
                        <a id="account-email" ></a>
                    </li><br>
                </ul>
               
                <a href="${scim_password_recovery}" target="_blank" class="btn btn-invis" id="changePASS">Change Password</a>
               </div>
              
         