<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    
    Note: Depends on logo, countrySelector, listSet component.


--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="org.osgi.service.cm.ConfigurationAdmin"%>
<%@page import="org.osgi.service.cm.Configuration"%>

<%
boolean isEditMode = WCMMode.fromRequest(request).equals(WCMMode.EDIT);
%>
<%
Configuration conf = sling.getService(org.osgi.service.cm.ConfigurationAdmin.class).getConfiguration("com.citrix.marketing.cq.commerce.configuration.SelfServiceConfigurationServiceImpl");
String scim_password_recovery = (String) conf.getProperties().get("scim.identity.password.recovery.url"); 
%>

<c:set var="logoPath" value="${properties.logoPath!= null ? properties.logoPath : ''}"/>
<c:set var="logoText" value="${properties.logoText!= null ? properties.logoText : 'Account'}"/>
<c:set var="logoutText" value="${properties.logoutText!= null ? properties.logoutText : 'Logout'}"/>
<c:set var="changePasswordText" value="${properties.changePasswordText!= null ? properties.changePasswordText : 'Change Password'}"/>
<c:set var="scim_password_recovery" value="<%=scim_password_recovery %>"/>	
	



<nav class="header">
     <div class="logo-box citrix-logo">
          <a href="${logoPath}" class=""></a>
          <span class="logo-text">${logoText}</span>
     </div>
     <ul class="navbar-right">
          <li>
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="caret"></span>
               </a>
               <ul class="dropdown-menu" role="menu">
                   <li><a href="${scim_password_recovery}" target="_blank" >${changePasswordText}</a></li>               
                    <li><a onclick="oauthclient.logout()">${logoutText}</a></li>                                  
               </ul>
          <li>
     </ul>
</nav>
 
<script>

   accessToken = sessionStorage.getItem("accessToken"); 
   var json;
    $.ajax({
            type: "GET",
            async: "false",
            url: "/bin/citrix/singleIdentity",
            data:'acessToken='+accessToken ,
            success: function (msg) {
                json = jQuery.parseJSON(msg);
                sessionStorage.setItem("userid", json.id);
                (typeof Mediator !== "undefined") ? Mediator.run() : false;

                // Get Billing Accounts
                $.ajax({
                    type: "GET",
                    url: "/bin/citrix/selfservice/billingAccountList",
                    data: {
                        userID: sessionStorage.getItem("userid")
                    },
                    success: function (data) {
                        sessionStorage.setItem("billingAccounts", data);
                    },
                    failure: function (data) {
                        console.log('No Billing Accounts found');
                    }
                });

            $('.dropdown-toggle').html(json.displayName  +'<span class="caret"></span>');
            },failure: function(data) {
    
            }
        });
        
    $(function(){
        utilityNavigation.init();
    });
        
</script>