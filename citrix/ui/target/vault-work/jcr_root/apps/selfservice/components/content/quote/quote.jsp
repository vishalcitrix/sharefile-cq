<%--
    Search Component - The search functionality for Support Home Page

    prabhu.shankar@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<script>
    $(function(){
        Mediator.add(Quote.init, ['<%= request.getParameter("quoteKey") %>']);
    })
</script>

<c:set var="quoteKey" value="${properties.quoteKey}"/>
<div role="grid" class="standard-quote quoteTable" id="quoteView">
    <div class="expired">
        <h2>This quote has expired</h2>
    </div>
    <div class="thead">
        <div class="row">
            <div class="small-5 columns text-left">Service</div>
            <div class="small-2 columns">Period</div>
            <div class="small-1 columns">Units</div>
            <div class="small-2 columns">Price/unit</div>
            <div class="small-2 columns">Total Price</div>
        </div>
    </div>
    <div class="tbody">

    </div>
</div>