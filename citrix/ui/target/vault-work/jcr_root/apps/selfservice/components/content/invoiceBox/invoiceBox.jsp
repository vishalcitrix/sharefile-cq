<%--

  Invoice Box component.

--%>

<%@include file="/libs/foundation/global.jsp"%>


<div class="invoiceBox">
    <div class="section-title">
      <a class="btn btn-invis editBtn">Edit</a>
    </div>
    <div class="section-content">
      <div class="row">
        <div class="float-left">
          <p class="balancetxt">Balance Due:</p>
          <p> <span>Due Date: </span>3/15/15</p>
          <p> <span>Auto Billed To: </span><i class="icon-cc-visa"> </i> ...2929</p>
          <p><a>View Invoice</a><span class="gray-txt">|</span><a class="invoice-view" rel="lightbox" href="/content/selfservice/en_us/homepage/invoicehistory/jcr:content/mainContent/">Invoice History</a></p>
          <p class="payLink"><a href="#">Pay Now</a></p>
        </div>
        <div>
          <h3 class="balance">$1056.00</h3>
        </div>
      </div>
    </div>
</div>