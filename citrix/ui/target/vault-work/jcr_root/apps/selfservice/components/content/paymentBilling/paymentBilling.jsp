<%--

  editUserInfo component.

  this form will be used to update the user details.

--%><% 
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
    // TODO add you code here
%>   

<script>
    
   $(document).ready(function(){
        paymentBilling.init();
     
   });

</script> 


    <div class="approvePay">
         <ul class="selectedPayment  hide" id="selectedPay">
         </ul>
         
         <button class="btn btn-invis" id="addPOno">Add PO number</button>   
         
         <div class="addingPO hide" >
             <div>
                 <label class="poLabel hide">PO Number</label>
                 <input type="text" class="formInput" name="poNumber" placeholder = "PO number(Optional)" onblur="this.placeholder = 'PO number(Optional)'"  onfocus="this.placeholder = '' ">
              </div>
              <div>
                 <label class="poLabel hide">(MM/DD/YY)</label>
                 
                 <input type="text" class="formInput" name="poDate" placeholder = "PO exp date(Optional)" onblur="this.placeholder = 'PO exp date(Optional)'"  onfocus="this.placeholder = '' " style="text-align:center;">
                  <i class="icon-menu"></i>
              </div>
         </div>
         <div class = "noBillID hide">
               <p>You have no account with us.</p>
         </div>
    
    </div> 
            