<!--
    Conditions:
        All     ==> Display site specific 404 page. If we can't, display a generic 404 page.

 -->

<%@ page import="org.apache.jackrabbit.util.Text,
    com.day.cq.wcm.api.WCMMode,
    java.util.regex.Pattern,
    java.util.regex.Matcher,
    com.siteworx.rewrite.transformer.ContextRootTransformer,
    org.apache.sling.api.scripting.SlingScriptHelper,
    org.apache.sling.api.scripting.SlingBindings" %>
<%
    Pattern sitePattern = Pattern.compile("^\\/content/.*\\/.*");
    String currentPathInfo = request.getPathInfo();
    final Matcher siteMatcher = sitePattern.matcher(currentPathInfo);
    boolean isSitePage = siteMatcher.matches() && !currentPathInfo.contains("dam");
%>

<c:set var="isSitePage" value="<%=isSitePage%>"/>
<c:choose>
    <c:when test="${isSitePage}">
        <%-- Send to site specific 404 page. --%>
        <%
            SlingBindings slingBindings = (SlingBindings) request.getAttribute("org.apache.sling.api.scripting.SlingBindings");
            SlingScriptHelper slingScripter = slingBindings.getSling();
            String sitePath = Text.getAbsoluteParent(currentPathInfo, 2);
            String errorPage = sitePath + "/404";
            response.setStatus(404);
            request.getRequestDispatcher(errorPage).forward(request, response);
        %>
    </c:when>
    <c:otherwise>
        <%
            response.setStatus(404);
        %>
        404: Page Not Found.
    </c:otherwise>
</c:choose>