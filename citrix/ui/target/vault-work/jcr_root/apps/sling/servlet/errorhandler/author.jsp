<!-- 
	Conditions:
		If 'forbidden', aka user isn't logged in 	==> send them to login page.\
		If 'forbidden', aka user has no permissions ==> let them know that.
		If '404' within a site, page not found   	==> send them to site's 404 page, non-editable.

 -->

<%@ page import="
    java.net.URLEncoder,
    org.apache.sling.api.scripting.SlingBindings,
    org.apache.sling.api.resource.Resource,
    org.apache.sling.engine.auth.Authenticator,
    org.apache.sling.engine.auth.NoAuthenticationHandlerException,
    com.day.cq.wcm.api.WCMMode,
    org.apache.jackrabbit.util.Text,
    java.util.regex.Pattern,
    java.util.regex.Matcher,
    javax.jcr.Session,
    org.apache.sling.api.scripting.SlingScriptHelper,
    org.apache.sling.api.SlingHttpServletRequest,
    org.apache.sling.api.resource.ResourceResolver"%>
<%!

    private boolean isAnonymousUser(HttpServletRequest request) {
        return request.getAuthType() == null
            || request.getRemoteUser() == null;
    }
    
%>
<%
	SlingBindings slingBindings = (SlingBindings) request.getAttribute("org.apache.sling.api.scripting.SlingBindings");
	SlingScriptHelper slingScripter = slingBindings.getSling();
	SlingHttpServletRequest slingRequest2 = slingScripter.getRequest();
	ResourceResolver slingResourceResolver = slingRequest2.getResourceResolver();
	javax.jcr.Session jcrSession = slingResourceResolver.adaptTo(javax.jcr.Session.class);
	Pattern sitePattern = Pattern.compile("^\\/content/.*\\/.*");
	boolean sendToLogin = isAnonymousUser(request);
	boolean resourceNotFound = false;
	boolean hasPermission = false;
	
	//user is logged in, check if they have permissions on path
	Resource r = slingRequest2.getResource();
	if (r == null)
		resourceNotFound = true;
	else {
		if (jcrSession.hasPermission(r.getPath(), "read"))
			hasPermission = true;
	}

	String currentPathInfo = request.getPathInfo();
	final Matcher siteMatcher = sitePattern.matcher(currentPathInfo);
	boolean isSitePage = siteMatcher.matches();
%>
<c:set var="sendToLogin" value="<%=sendToLogin%>"/>
<c:set var="isSitePage" value="<%=isSitePage%>"/>
<c:set var="hasPermission" value="<%=hasPermission %>"/>
<c:choose>
	<c:when test="${sendToLogin}">
		<%-- User is not logged in, send them to the login page. --%>
		<%
	        Authenticator auth = slingScripter.getService(Authenticator.class);
	        if (auth != null) {
	            try {
	                auth.login(request, response);
	                return;
	            } catch (NoAuthenticationHandlerException nahe) {
	                slingBindings.getLog().warn("Cannot login: No Authentication Handler is willing to authenticate");
	            }
	        } else {
	           slingBindings.getLog().warn("Cannot login: Missing Authenticator service");
	        }
		%>
	</c:when>
	<c:when test="${hasPermission}">
		<%
			String sitePath = Text.getAbsoluteParent(currentPathInfo, 2);
		   	String errorPagePath = sitePath + "/404";
		   	Resource errorPageResource = slingResourceResolver.getResource(errorPagePath);
		%>
		<c:set var="errorPage" value="<%=errorPageResource %>"/>
		<c:choose>
			<c:when test="${isSitePage && not empty errorPage}">
				<%-- Send to site specific 404 page. --%>
				<swx:setWCMMode mode="READ_ONLY">
		    		<sling:include resource="${errorPage}"/>
				</swx:setWCMMode>
			</c:when>
			<c:otherwise>
				<%
					response.setStatus(404);
				%>
				404: Page Not Found.
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<%
			response.setStatus(404);
		%>
		You have no permissions to view this page, please contact your administrator.
	</c:otherwise>
</c:choose>