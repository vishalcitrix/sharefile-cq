<%--
	Footer component.
	- Includes Logo and County Selector Component
	
	vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="left">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="countrySelector" resourceType="/apps/citrixosd-responsive/components/content/countrySelector"/>
	</swx:setWCMMode>
</div>
<div class="right">
	<swx:setWCMMode mode="READ_ONLY">
		<cq:include path="logo" resourceType="/apps/g2a-responsive/components/content/logo"/>
	</swx:setWCMMode>
</div>

<div class="clearBoth"></div>