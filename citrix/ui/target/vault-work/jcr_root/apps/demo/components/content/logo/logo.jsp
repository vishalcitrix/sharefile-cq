<%--
    Logo
    
    Fixed CSS classes will contain the logo and the logo can also 
    become a link. Default spite will be a placeholder and will only 
    appear in the wcm edit or read only mode. Option to add logo for small
    devices as well.
        
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String PATH_PROPERTY = "path";
    public static final String LINK_OPTION_PROPERTY = "linkOption";
%>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, null) %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>

<c:choose>
    <c:when test="${not empty path}">
        <a href="${path}" class="show-for-small-only" <c:if test="${not empty linkOption}">rel="${linkOption}"</c:if>><span class="${properties.cssSmall}"></span></a>
        <a href="${path}" class="show-for-medium-only" <c:if test="${not empty linkOption}">rel="${linkOption}"</c:if>><span class="${properties.cssMedium}"></span></a>
        <a href="${path}" class="show-for-large-up" <c:if test="${not empty linkOption}">rel="${linkOption}"</c:if>><span class="${properties.css}"></span></a>
    </c:when>
    <c:otherwise>
        <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-image-placeholder">
        </c:if>
    </c:otherwise>
</c:choose>