<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<cq:include script="slidebar.jsp"/>
<cq:include script="lightbox.jsp"/>
<cq:include script="geoSegmentation.jsp"/>

<footer>
	<cq:include path="staticFooter" resourceType="swx/component-library/components/content/single-ipar"/>
</footer>

<%-- Javascripts - Place after including jQuery Lib --%>
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>

<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>

<c:if test="${not isEditMode}">
	<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js" defer="true"></script>
</c:if>
