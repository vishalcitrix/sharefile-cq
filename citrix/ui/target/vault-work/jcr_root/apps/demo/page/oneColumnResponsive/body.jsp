<%@page import="com.citrixosd.SiteUtils"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>
<%-- Add optional product theme --%>
<c:set var="productTheme" value="<%= SiteUtils.getProductTheme(currentPage) %>"/>
<c:if test="${not empty productTheme}">
	<c:set var="productTheme" value=' class="${productTheme}"'/>
</c:if>
 <body${productTheme}>
    <div id="content-body">
        <cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
        <cq:include path="floatingFooter" resourceType="swx/component-library/components/content/single-ipar"/>
    </div>  
    <cq:include script="footer.jsp" />
</body>
