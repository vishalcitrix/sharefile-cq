<%--
  Video Spotlight component.
  
  This is a Video Spotlight component developed to display images and configured  gradient color on hover.
  
  Here image contains reference for bright cove video. 
  
  In order to place video link with image and corresponding gradient a valid video container id 
  should be configured in component dialog.

  Component displays only image gradient for small and medium view port. 
  Also option to only have background color instead of background image.

--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:choose>
	<c:when test="${properties.isBackGroundColor eq 'true'}">
		<cq:include script="videospotlightWithBGColor.jsp"/>
	</c:when>
	<c:otherwise>
		<cq:include script="videospotlightWithImage.jsp"/>
	</c:otherwise>
</c:choose>