<%--
    Visibility component.
    - This component manages visibility for different viewports.
    
    vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final boolean showForLarge = properties.get("showForLarge",false);
    final boolean showForMedium = properties.get("showForMedium",false);
    final boolean showForSmall = properties.get("showForSmall",false);
    StringBuilder builderToolbar = new StringBuilder();
    StringBuilder builderDiv = new StringBuilder();
    
    if(showForLarge && showForMedium && !(showForSmall)){
        builderToolbar.append("Hide for Small");
        builderDiv.append("<div class='show-for-medium-up'>");
    }
    
    else if(!(showForLarge) && !(showForMedium) && showForSmall){
        builderToolbar.append("Show for Small only");
        builderDiv.append("<div class='show-for-small-only'>");
    } 
    
    else if(showForLarge && !(showForMedium) && showForSmall){
        builderToolbar.append("Hide for Medium only"); 
        builderDiv.append("<div class='hide-for-medium-only'>");
    }
    
    else if(!(showForLarge) && showForMedium && !(showForSmall)){
        builderToolbar.append("Show for Medium only");
        builderDiv.append("<div class='show-for-medium-only'>");
    }
    
    else if(!(showForLarge) && showForMedium && showForSmall){
        builderToolbar.append("Hide for Large only"); 
        builderDiv.append("<div class='hide-for-large-up'>");
    } 

    else if(showForLarge && !(showForMedium) && !(showForSmall)){
        builderToolbar.append("Show for Large only");  
        builderDiv.append("<div class='show-for-large-up'>");
    } 
    
    else {
        builderToolbar.append("Show for All");  
        builderDiv.append("<div>");
    }
        
    if (editContext != null && editContext.getEditConfig() != null) {    	
    	if(editContext.getEditConfig().getToolbar() != null && editContext.getEditConfig().getToolbar().size() >= 4) {
	        editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label(builderToolbar.toString()));    
	        editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
    	}
    }
%>

<c:choose>
    <c:when test="${isEditMode}">
        <div>
    </c:when>
    <c:otherwise>
        <%= builderDiv %>
    </c:otherwise>
</c:choose>

    <cq:include path="visibility-content" resourceType="foundation/components/parsys"/>
</div>