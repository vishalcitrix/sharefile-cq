<%--
    Separator Component - Creates an <div> tag with border bottom, adjustable width, separator type and alignment options.

    vishal.gupta@citrix.com
    kaverappa.subramanya@citrix.com - Customized for ShareFile.
--%>

<%@include file="/libs/foundation/global.jsp" %>

<c:if test="${properties.seperatorType eq 'arrow'}">
	<div class="arrowImage ${alignmentCss}" style="<c:if test="${not empty properties.width}">width:${properties.width};</c:if>"></div>
</c:if>

<c:if test="${not empty properties.userColor && properties.seperatorType ne 'arrow'}">
    <hr class="${properties.seperatorType} ${properties.alignment}" style="<c:if test="${not empty properties.width}">width:${properties.width};</c:if>border-color:${properties.userColor};">
</c:if>

<c:if test="${fn:contains(alignmentCss,'left') || fn:contains(alignmentCss,'right')}">
    <div style="clear:both"></div>
</c:if>            