<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
    
    <div id="main" class="popup content">
    
        <div class="container">
        
            <cq:include path="mainContent" resourceType="foundation/components/parsys"/>

        </div>
        
    </div>
    
    <cq:include script="footer.jsp" />
    
</body>