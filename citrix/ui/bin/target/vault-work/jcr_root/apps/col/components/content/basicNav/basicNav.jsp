<%--
    Sub Navigation
    
    Navigation is used either choosing the pages to show or using page tags. When 
    the current page is selected it will can not be clicked. 
    
    Dev: The request will return a List object of the current pages that matches 
    the parameters. Set the list object so the other list item jsp can use it.

    achew@siteworx.com
 --%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.day.cq.wcm.api.PageManager"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String NAVIGATION_LINKS_PROPERTY = "navigationLinks";

    public class NavigationLink {
        private String text;
        private String path;
        private String linkOption;
        
        public NavigationLink() {
            
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<NavigationLink> getNavigationLinks(NodeIterator nodeIter, PageManager pageManager) {
        final List<NavigationLink> navigationLinks = new ArrayList<NavigationLink>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final NavigationLink navigationLink = new NavigationLink();
                    navigationLink.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : null);
                    navigationLink.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : null);
                    navigationLink.setLinkOption(currLink.hasProperty("linkOption") ? currLink.getProperty("linkOption").getString() : "");
                    if(navigationLink.getPath() != null && navigationLink.getText() == null) {
                        final Page page = pageManager.getPage(navigationLink.getPath());
                        if(page != null) {
                            navigationLink.setText(page.getNavigationTitle() != null ? page.getNavigationTitle() : page.getTitle() != null ? page.getTitle() : page.getName());
                        }
                    }
                    
                    if(navigationLink.getText() != null || navigationLink.getPath() != null) {
                        navigationLinks.add(navigationLink);    
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return navigationLinks;
    }

%>

<%
    List<NavigationLink> navigationLinks = null;
    if (currentNode != null && currentNode.hasNode(NAVIGATION_LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(NAVIGATION_LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        navigationLinks = getNavigationLinks(nodeIter, currentPage.getPageManager());
    }
    
    final Map<String, Object> subNavigation = new HashMap<String, Object>();
    subNavigation.put(NAVIGATION_LINKS_PROPERTY, navigationLinks);
%>

<c:set var="subNavigation" value="<%= subNavigation %>"/>

<c:choose>
    <c:when test="${not empty subNavigation && fn:length(subNavigation.navigationLinks) > 0}">
        <ul class="default">
            <c:forEach items="${subNavigation.navigationLinks}" var="itemPage" varStatus="i">
                <c:choose>
                    <c:when test="${empty itemPage.path}">
                        <li class="${i.first ? 'first' : i.last ? 'last' : 'item'} splitText">${itemPage.text}</li>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${currentPage.path eq itemPage.path}">
                                <li class="${i.first ? 'first' : i.last ? 'last' : 'item'} selected">${itemPage.text}</li>
                            </c:when>
                            <c:otherwise>
                                 <li class="${i.first ? 'first' : i.last ? 'last' : 'item'}">
                                     <a href="${itemPage.path}" title="${itemPage.text}">${itemPage.text}</a>
                                 </li>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <div style="clear: both;"></div>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode || isReadOnlyMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">
        </c:if>
    </c:otherwise>
</c:choose>