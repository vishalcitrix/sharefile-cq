<%--

  Sub Column Component component.
  This component lets create multi column layout inside of existing column control component or a parsys component
  
  vishal.gupta.82@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<div class="subcontainer for-<%=properties.get("columns")%>-columns">

<%

    int columns = properties.get("columns", 2);
    int width = 100/columns;

    for (int i = 1; i <= columns; i++) {    
%>    
        <div class="columns-<%=i%>" style="float: left; width: <%= width %>%;">
        
            <cq:include path="<%= "columns_par" + i %>" resourceType="foundation/components/parsys"/>
        </div>
<%
    }
%>

</div>
 
<div class="clearBoth"></div>