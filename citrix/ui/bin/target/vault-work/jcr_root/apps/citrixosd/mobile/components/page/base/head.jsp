<%--
	Retrieve the current mobile device group from the request in the following order:
    1) group defined by <path>.<groupname-selector>.html
    2) if not found and in author mode, get default device group as defined in the page properties
       (the first of the mapped groups in the mobile tab)

    If a device group is found, use the group's drawHead method to include the device group's associated
    emulator init component (only in author mode) and the device group's rendering CSS.
 --%>

<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="com.day.cq.wcm.mobile.api.device.DeviceGroup" %>
<%@page import="com.citrixosd.SiteUtils"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%! public static final String ASYNCHRONOUS_ANALYTICS_PROPERTY = "asynchronousAnalytics"; %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>
<c:set var="designPath" value="<%= currentDesign.getPath() %>" />
<c:set var="showChannelTracking" value="<%= SiteUtils.getPropertyFromProductSiteRoot("showChannelTracking", currentPage) %>"/>

<%
	final DeviceGroup deviceGroup = slingRequest.adaptTo(DeviceGroup.class);
	if (null != deviceGroup) {
	    deviceGroup.drawHead(pageContext);
	}
%>
<head>
    <%--Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="content-language" content="en-US">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <cq:include script="meta.jsp"/>

    
    <%--CSS --%>
    <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/static.css" type="text/css">
    
    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/images/favicon.ico">
    
    <%--Title --%>
    <title><%= currentPage.getTitle() == null ? StringEscapeUtils.escapeHtml4(currentPage.getName()) : StringEscapeUtils.escapeHtml4(currentPage.getTitle()) %>${not empty sitename ? ' | ' : ''}${sitename}</title>
    
    <%--Javascripts --%>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>

    <c:if test="${showChannelTracking == 'true'}">
	<cq:include script="channelTracking.jsp"/>
    </c:if>

    <c:if test="<%= !properties.get(ASYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
        <cq:include script="analytics.jsp"/>
    </c:if>

    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</head>
