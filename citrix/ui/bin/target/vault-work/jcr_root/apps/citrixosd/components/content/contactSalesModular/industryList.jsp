<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select id="Industry" name="Industry" class="required">
    <option value=""><fmt:message key="form.option.default"/></option>
    <option value="Accounting"><fmt:message key="form.option.industry.accounting"/></option>
    <option value="Advertising/Marketing/PR"><fmt:message key="form.option.industry.advertisingMarketingPR"/></option>
    <option value="Aerospace &amp; Defense"><fmt:message key="form.option.industry.aerospaceAndDefense"/></option>
    <option value="Call Center Outsourcing"><fmt:message key="form.option.industry.callCenterOutsourcing"/></option>
    <option value="Consulting"><fmt:message key="form.option.industry.consulting"/></option>
    <option value="Education"><fmt:message key="form.option.industry.education"/></option>
    <option value="Energy, Chemical, Utilities"><fmt:message key="form.option.industry.energyChemicalUtilities"/></option>
    <option value="Financial Services"><fmt:message key="form.option.industry.financialService"/></option>
    <option value="Government - Federal"><fmt:message key="form.option.industry.governmentFederal"/></option>
    <option value="Government - State &amp; Local"><fmt:message key="form.option.industry.governmentStateAndLocal"/></option>
    <option value="Healthcare"><fmt:message key="form.option.industry.healthcare"/></option>
    <option value="High Tech - Hardware"><fmt:message key="form.option.industry.highTechHardware"/></option>
    <option value="High Tech - ISP"><fmt:message key="form.option.industry.highTechISP"/></option>
    <option value="High Tech - Software"><fmt:message key="form.option.industry.highTechSoftware"/></option>
    <option value="High Tech - Other"><fmt:message key="form.option.industry.highTechOther"/></option>
    <option value="Hospitality/Travel/Tourism"><fmt:message key="form.option.industry.hospitalityTravelTourism"/></option>
    <option value="Insurance"><fmt:message key="form.option.industry.insurance"/></option>
    <option value="Legal"><fmt:message key="form.option.industry.legal"/></option>
    <option value="Manufacturing"><fmt:message key="form.option.industry.manufacturing"/></option>
    <option value="Pharmaceuticals &amp; Biotechnology"><fmt:message key="form.option.industry.pharmaceuticalsAndBiotechnology"/></option>
    <option value="Real Estate"><fmt:message key="form.option.industry.realEstate"/></option>
    <option value="Retail"><fmt:message key="form.option.industry.retail"/></option>
    <option value="Support Outsourcing"><fmt:message key="form.option.industry.supportOutsourcing"/></option>
    <option value="Telecommunications"><fmt:message key="form.option.industry.telecommunications"/></option>
    <option value="Transportation"><fmt:message key="form.option.industry.transportation"/></option>
    <option value="VAR/Systems Integrator"><fmt:message key="form.option.industry.VARSystemIntegrator"/></option>
    <option value="Other"><fmt:message key="form.option.industry.other"/></option>
</select>