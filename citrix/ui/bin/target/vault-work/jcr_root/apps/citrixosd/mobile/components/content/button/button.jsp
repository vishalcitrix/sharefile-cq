<%--
	Button
	
	This component will consist of custom css buttons which will contain a 
	label and a link. The button also allows an optional sub text displayed 
	under the button. Default button style is set to none.
	
	achew@siteworx.com
  
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="button-container">
    <div class="button button-${empty properties.css ? 'white' : properties.css}">
        <c:if test="${not empty properties.buttonSprite}"><div class="button-sprite ${properties.buttonSprite}"></div></c:if>
        <a href="${properties.path}">
        	<c:choose>
                <c:when test="${not empty properties.label}">
                    <fmt:message key="${properties.label}"/>
                </c:when>
                <c:otherwise>
                    <c:if test="${isEditMode or isReadOnlyMode }">Add Text</c:if>
                </c:otherwise>
            </c:choose>
        </a>
    </div>
</div>

<c:if test="${not empty properties.subText}">
    <div class="subText ${properties.style}">
        <c:choose>
            <c:when test="${not empty properties.path}">
                <a href="${properties.path}"><fmt:message key="${properties.subText}"/></a>
            </c:when>
            <c:otherwise>
                <p><fmt:message key="${properties.subText}"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</c:if>