<%--
	Carousel
	
	This holds multiple hero components. Authors can add as many hero component as they like as long as 
	this does not break the UX. The default number of hero is 2 and height of the carousel is 340px. This 
	component's javascript will not execute in WCM mode edit because it is not author friendly.
	
	Dev: When WCM mode is not in edit, the additional div's will hide only showing the first div. Javascript 
	will inject the html for the dots so users can scroll through the carousel. The container height will 
	have effect if WCMMode is not in edit.
	
	Dependencies: Hero, carousel.js
	
	achew@siteworx.com

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%!
    private static final String NUMBER_OF_ITEMS_PROPERTY = "numberOfItems";
	private static final String HEIGHT_PROPERTY = "height";
%>

<c:set var="numberOfItems" value="<%= properties.get(NUMBER_OF_ITEMS_PROPERTY, 2) %>"/>
<c:set var="height" value="<%= properties.get(HEIGHT_PROPERTY, 340) %>"/>

<div id="carousel" class="banner-home">
    <ul class="banner-home" style="<c:if test="${!isEditMode}">min-height: ${height}px</c:if>">
        <c:forEach begin="1" end="${numberOfItems}" varStatus="i">
        	<c:if test="${isEditMode || isReadOnlyMode}"><li style="display: list-item;"><div class="warning">Item #${i.count}/${numberOfItems}</div></li></c:if>
            <li class="${i.first ? 'active ' : ''}heroContainer_${i.count}" style="display:${isEditMode ? 'list-item' : i.first ? 'list-item' : 'none'}; margin-top: 0px;">
               	<cq:include path="hero_${i.count}" resourceType="citrixosd/components/content/hero"/>
            </li>
        </c:forEach>
    </ul>
</div>

<div class="banner-home-dots container"></div>

<c:if test="${isPreviewMode || isDisabledMode || isPublishInstance}">
	<script>
	    $(document).ready( function(){
	    	Carousel.init();
	    });
	</script>
</c:if>
