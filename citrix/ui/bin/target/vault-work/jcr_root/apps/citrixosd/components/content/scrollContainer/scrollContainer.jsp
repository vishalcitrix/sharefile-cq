<%--
	Scroll Container
	
	This is a container that allows content to be scroll-able based on the height and width.
	
	achew@siteworx.com
 --%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
    
<div class="scrollContainer-container" style="overflow: auto; height: ${not empty properties['height'] ? properties['height'] : 'auto'}${not empty properties['height'] ? (fn:contains(properties['height'], '%') ? '' : 'px') : ''}; width: ${not empty properties['width'] ? properties['width'] : 'auto'}${not empty properties['width'] ? (fn:contains(properties['width'], '%') ? '' : 'px') : ''};">
    <cq:include script="single-par.jsp"/>
</div>