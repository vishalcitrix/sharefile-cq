<%--
    Brightcove Webinar
    
    Display the brightcove video player in a unique container. Player requires player id and key 
    for the video to be displayed in the UI. Since each video could have different sizes each player 
    is created independently. The player would also have to have a video id to be view correctly. If 
    there is a image rendition used, then this will automatically override the default brightcove video 
    thumbnail. Authors can also adjust the size of the video which will take affect on the lightbox as well. 
    If there is a title, then this will override the original brightcove title.
    
    Author image is handled by putting the image to the image rendition dialog. If there is no image used, the 
    resource page will automatically take the brightcove thumbnail image and display it to the end user. Author 
    title, and description are also displayed to the end user via the resource page.
    
    Start date is required and used to do the query. This date is used based on the timezone set on the server. 
    
    Dev: There is a brightcove javascript clientlibs injected in this component from 'etc/design/brightcove/
    brightcoveExperience'. The height and the width of the component is based on the author input which is displayed 
    on the lightbox container. The javascript will put the brightcove object in the lightbox container 
    when the page loads. The object is in the lightbox container because we want to restrict the use of 
    multiple lightbox containers. Since there is one lightbox containers, we don't run into conflicts with 
    multiple lightbox appearing. There is also a container id for each video container to differentiate each 
    video player. The length of the video is calculated with mathematical logic to determine the hours, minutes, 
    seconds.
    
    achew@siteworx.com

 --%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.exceptions.BrightcoveException"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.apiobjects.Video"%>
<%@page import="java.util.EnumSet"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.VideoFieldEnum"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.ReadApi"%>
<%@page import="com.day.cq.wcm.api.components.DropTarget" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.UUID" %>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%@include file="/apps/brightcove/global/configuration.jsp"%>

<%!
	private static final String RETURN_TEXT_PROPERTY = "returnText";
	private static final String RETURN_PATH_PROPERTY = "returnPath";
    private static final String CONTAINER_ID = "containerId";
    private static final String HEIGHT_PROPERTY = "height";
    private static final String WIDTH_PROPERTY = "width";
    private static final String VIDEO_PLAYER_PROPERTY = "videoPlayer";
    private static final String VIDEO_TITLE_PROPERTY = "videoTitle";
    private static final String VIDEO_LENGTH_PROPERTY = "videoLength";
    private static final String VIDEO_HUMAN_LENGTH_PROPERTY = "videoHumanLength";
    private static final String VIDEO_THUMBNAILURL_PROPERTY = "videoThumbnailUrl";
    private static final String VIDEO_TITLE_OVERRIDE_PROPERTY = "videoTitleOverride";
    
    private static final String FILE_REFERENCE_PROPERTY="fileReference";
    private static final String RENDITION_PROPERTY="rendition";
    private static final String AUTHOR_TITLE_PROPERTY = "authorTitle";
    private static final String AUTHOR_DESCRIPTION_PROPERTY = "authorDescription";
	private static final String CREDIT_PROPERTY = "credit";
	private static final String CREDIT_PATH_PROPERTY = "creditPath";
    
    private static final String PLAYER_ID_PROPERTY = "playerId";
    private static final String PLAYER_KEY_PROPERTY = "playerKey";
    private static final String UPDATE_VIDEO_METADATA_PROPERTY = "updateVideoMetadata";
    
    private static final String DD_CLASS_NAME = DropTarget.CSS_CLASS_PREFIX + "brightcove";
    
    /**
    * Retrieve the video data from brightcove servers and store the metadata to the current node
    */
    private void updateVideoMetadata(String readToken, String videoId, Node node) {
        final ReadApi read = new ReadApi();
        try {
            final Video video = read.FindVideoById(readToken, Long.parseLong(videoId), EnumSet.of(VideoFieldEnum.NAME, VideoFieldEnum.LENGTH, VideoFieldEnum.THUMBNAILURL, VideoFieldEnum.ID), null);
            updateNodeProperty(node, VIDEO_TITLE_PROPERTY, video.getName());
            updateNodeProperty(node, VIDEO_LENGTH_PROPERTY, video.getLength());
            updateNodeProperty(node, VIDEO_HUMAN_LENGTH_PROPERTY, getHumanVideoTime(video.getLength()));
            updateNodeProperty(node, VIDEO_THUMBNAILURL_PROPERTY, video.getThumbnailUrl());
            node.setProperty(UPDATE_VIDEO_METADATA_PROPERTY, false);
            node.save();
        }catch (BrightcoveException be) {
            be.printStackTrace();
        }catch (RepositoryException re) {
            re.printStackTrace();
        }
    }
    
    /**
    * Updates the node property if there is a change in the value for string object
    */
    private void updateNodeProperty(Node node, String parameter, String value) {
        try {
            if(node.hasProperty(parameter)) {
                if(node.getProperty(parameter).equals(value)) {
                    //Do nothing, property is correct
                }else {
                    node.setProperty(parameter, value);
                    node.save();
                }
            }else {
                node.setProperty(parameter, value);
                node.save();
            }
        }catch(RepositoryException re) {
            re.printStackTrace();
        }
    }
    
    /**
    * Updates the node property if there is a change in the value for long object
    */
    private void updateNodeProperty(Node node, String parameter, Long value) {
        try {
            if(node.hasProperty(parameter)) {
                if(node.getProperty(parameter).equals(value)) {
                    //Do nothing, property is correct
                }else {
                    node.setProperty(parameter, value);
                    node.save();
                }
            }else {
                node.setProperty(parameter, value);
                node.save();
            }
        }catch(RepositoryException re) {
            re.printStackTrace();
        }
    }
    
    /**
    * Converts milliseconds to hours, minutes, and seconds ex:(1:20:32)
    */
    private String getHumanVideoTime(long milliseconds) {
        final StringBuilder builder = new StringBuilder();
        if(milliseconds < 60000L) {
            builder.append("0").append(":"); //Append the 0 minutes for ui
        }
        if(milliseconds >= 3600000) {
            int hours = (int)(milliseconds / 3600000L);
            milliseconds = milliseconds - (hours * 3600000);
            builder.append(hours).append(":");
        }
        if(milliseconds >= 60000) {
            int minutes = (int)(milliseconds / 60000L);
            milliseconds = milliseconds - (minutes * 60000);
            builder.append(minutes).append(":");
        }
        int seconds = (int)(milliseconds / 1000L);
        if(seconds  < 10) {
            builder.append("0");
        }
        milliseconds = milliseconds - (seconds * 1000);
        builder.append(seconds);
        
        return builder.toString();
    }
%>

<%
    String containerId = properties.get(CONTAINER_ID, null);
    if(containerId == null && (WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY))) {
        Node brightcoveLightboxNode = currentNode;
        
        if(brightcoveLightboxNode == null) {
            final Session session = resource.getResourceResolver().adaptTo(Session.class);
            brightcoveLightboxNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
            session.save();
        }
        
        if(!brightcoveLightboxNode.hasProperty(CONTAINER_ID)) {
            final String generatedContainerId = ClientContextUtil.getId(resource.getPath()).replaceAll("-", "");
            brightcoveLightboxNode.setProperty(CONTAINER_ID, generatedContainerId);
            brightcoveLightboxNode.save();
        }
        
        containerId = properties.get(CONTAINER_ID, null);
    }
    
    if(WCMMode.fromRequest(request) == WCMMode.EDIT) {
        final boolean updateVideoMetadata = properties.get(UPDATE_VIDEO_METADATA_PROPERTY, false);
        final String videoId = properties.get(VIDEO_PLAYER_PROPERTY, null);
        if(updateVideoMetadata && videoId != null) {
            updateVideoMetadata(READ_TOKEN, videoId, currentNode);
        }
    }
    
	final String returnText = properties.get(RETURN_TEXT_PROPERTY, "video.label.return.text");
	final String returnPath = properties.get(RETURN_PATH_PROPERTY, currentPage.getParent().getPath() + "#ResourceWebinar");
	
    final Map<String, Object> brightcoveWebinar = new HashMap<String, Object>();
    brightcoveWebinar.put(RETURN_TEXT_PROPERTY, returnText);
    brightcoveWebinar.put(RETURN_PATH_PROPERTY, returnPath);
    brightcoveWebinar.put(CONTAINER_ID, containerId);
    brightcoveWebinar.put(HEIGHT_PROPERTY, properties.get(HEIGHT_PROPERTY, 416));
    brightcoveWebinar.put(WIDTH_PROPERTY, properties.get(WIDTH_PROPERTY, 740));
    brightcoveWebinar.put(VIDEO_TITLE_PROPERTY, properties.get(VIDEO_TITLE_PROPERTY, null));
    brightcoveWebinar.put(VIDEO_TITLE_OVERRIDE_PROPERTY, properties.get(VIDEO_TITLE_OVERRIDE_PROPERTY, null));
    brightcoveWebinar.put(VIDEO_LENGTH_PROPERTY, properties.get(VIDEO_LENGTH_PROPERTY, 0));
    brightcoveWebinar.put(VIDEO_HUMAN_LENGTH_PROPERTY, properties.get(VIDEO_HUMAN_LENGTH_PROPERTY, "0:00"));
    brightcoveWebinar.put(VIDEO_THUMBNAILURL_PROPERTY, properties.get(VIDEO_THUMBNAILURL_PROPERTY, null));
    brightcoveWebinar.put(PLAYER_ID_PROPERTY, properties.get(PLAYER_ID_PROPERTY, null));
    brightcoveWebinar.put(PLAYER_KEY_PROPERTY, properties.get(PLAYER_KEY_PROPERTY, null));
    brightcoveWebinar.put(VIDEO_PLAYER_PROPERTY, properties.get(VIDEO_PLAYER_PROPERTY, null));
%>

<c:set var="brightcoveWebinar" value="<%= brightcoveWebinar %>"/>

<c:choose>
	<c:when test="${not empty brightcoveWebinar.videoPlayer}">
		<div class="webinar-title">
			<h2>${not empty brightcoveWebinar.videoTitleOverride ? brightcoveWebinar.videoTitleOverride : brightcoveWebinar.videoTitle}</h2>
			<a href="${brightcoveWebinar.returnPath}"><span class="small-arrow-left"></span><fmt:message key="${brightcoveWebinar.returnText}"/></a>
		</div>
		
		<div class="brightcove-container" id="${brightcoveWebinar.containerId}">
			
		</div>
		
		<div class="webinar-author">
			<c:if test="${not empty properties['fileReference']}">
				<div class="webinar-author-image">
					<img src="${not empty properties['rendition'] ? properties['rendition'] : properties['fileReference']}"/>
				</div>
			</c:if>
			
			<c:if test="${not empty properties['authorTitle'] || not empty properties['authorDescription']}">
				<div class="webinar-author-description">
					<c:if test="${not empty properties['authorTitle']}">
						<div class="author-title"><strong>${properties['authorTitle']}</strong></div>
					</c:if>
					<c:if test="${not empty properties['authorDescription']}">
						<div class="author-description">${properties['authorDescription']}</div>
					</c:if>
					
					<c:if test="${not empty properties['credit'] || not empty properties['creditPath']}">
						<div class="credits-container"><fmt:message key="webinar.label.credit"/>
							<c:choose>
								<c:when test="${not empty properties['creditPath']}">
									<c:choose>
										<c:when test="${not empty properties['credit']}">
											<a href="${properties['creditPath']}"><span>${properties['credit']}</span></a>
										</c:when>
										<c:otherwise>
											<a href="${properties['creditPath']}"><span>${properties['creditPath']}</span></a>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<span>properties['credit']</span>
								</c:otherwise>
							</c:choose>
						</div>
					</c:if>
				</div>
			</c:if>
			<div class="clearBoth"></div>
		</div>
		<cq:include path="par" resourceType="foundation/component/parsys"/>
	</c:when>
	<c:otherwise>
		<c:if test="${isEditMode || isReadOnlyMode}">
			<%-- Asset does not exist --%>
			<div class="warning">Webinar Video: Please select a brightcove video</div>
			<img src="/libs/cq/ui/resources/0.gif" class="cq-video-placeholder <%= DD_CLASS_NAME %>"/>
		</c:if>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
    jQuery(document).ready(function() {
        BCL.createVideo("${brightcoveWebinar.width}",
                        "${brightcoveWebinar.height}",
                        "${brightcoveWebinar.playerId}",
                        "${brightcoveWebinar.playerKey}",
                        "${brightcoveWebinar.videoPlayer}",
                        "${brightcoveWebinar.containerId}");
        brightcove.createExperiences();
    });
</script>