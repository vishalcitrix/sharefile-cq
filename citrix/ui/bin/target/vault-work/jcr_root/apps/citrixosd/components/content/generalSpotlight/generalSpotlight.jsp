
<%--

  General Spotlight component.
  
    This component is comprised of 1) Share List 2) RichText and 3) Brightcove Component. 
    The main responsiblility of this component is to edit and manage the title, subtitle, and 
    description portion of this component. Title Area includes the option of including a Title 
    and a subtitle.
    
    * Layout Notes
    	- The component itself has no set width or size so it should be used with a column control to
    	  manage size
    
    
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String TITLE_PROPERTY = "title";
	private static final String SUBTITLE_PROPERTY = "subtitle";
%>
<div>
	<div class="title-area"> 
		<div class="icon">
			<cq:include path="sprite" resourceType="g2pc/components/content/spotlightLogo" />
		</div>

		<div class="title-text">
			<h3><%=properties.get(TITLE_PROPERTY, "")%></h3>
			<h4><%=properties.get(SUBTITLE_PROPERTY, "")%></h4>
		</div>
	</div>	

	<div class="clearBoth"></div>

	<%
		WCMMode mode = WCMMode.fromRequest(request);

		if (mode.equals(WCMMode.EDIT)) {
			mode = WCMMode.READ_ONLY.toRequest(request);
		}
	%>
	<cq:include path="richtext"	resourceType="citrixosd/components/content/text" />

	<%
		mode.toRequest(request);
	%>

	<div class="video-container">
		<cq:include path="brightcove" resourceType="citrixosd/components/content/brightcoveLightbox" />
	</div>
</div>