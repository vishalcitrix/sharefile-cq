<%--
	Body: structure of the content
 --%>

<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
 
<body>

    <cq:include script="header.jsp"/>
    
    <div id="main" class="content">
    
        <cq:include path="bannerParsys" resourceType="swx/component-library/components/content/single-ipar"/>
    
        <div class="content-body container">
        
            <cq:include path="contentParsys" resourceType="foundation/components/parsys"/>
                        
        </div>
    </div>

    <cq:include script="footer.jsp" />
    
</body>