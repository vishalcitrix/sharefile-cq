<%--
    Moblie Spotlight

    ingrid.tseng@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    public static final String CSS_PROPERTY = "iconSprite/css";
    public static final String IS_EVEN_PROPERTY = "iconSprite/isEven";
    public static final String TEXT_PROPERTY = "richtext/text";

%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, null) %>"/>
<c:set var="isEven" value="<%= properties.get(IS_EVEN_PROPERTY) %>"/>
<c:set var="text" value="<%= properties.get(TEXT_PROPERTY, null) %>"/>

<div class="content ${isEven ? 'even' : ''}">
    <c:choose>
        <c:when test="${not empty css}">
            <div class="icon-holder">
                <div class="icon ${css}"></div>
            </div>
        </c:when>
        <c:otherwise>
            <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
                <img src="/libs/cq/ui/resources/0.gif" class="cq-image-placeholder">
            </c:if>
        </c:otherwise>
    </c:choose>
    <div class="text-container">
        
        <c:if test="${empty text}">
            <c:if test="${isEditMode || isReadOnlyMode}">
                <span class="cq-text-placeholder-ipe">&para;</span>
            </c:if>
        </c:if>
    
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include path="richtext" resourceType="citrixosd/mobile/components/content/text"/>
        </swx:setWCMMode>
    </div>
</div>