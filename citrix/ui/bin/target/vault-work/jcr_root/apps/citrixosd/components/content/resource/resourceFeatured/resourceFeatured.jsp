<%--
	Resource Feature
	
	Feature uses the property in the resource page properties dialog. Authors can select 
	which resource type, feature and categories they choose to display. If there is no results, 
	the Resource javascript will hide the component from the end-user.
	
	Dependencies: ResourceContainer (Component), Resource (Javascript)

	achew@siteworx.com
--%>

<%@page import="java.util.List"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
	private static final String RESOURCE_TYPE_PROPERTY = "resourceType";
	private static final String CATEGORIES_PROPERTY = "categories";
	private static final String FEATURED_PROPERTY = "featured";
%>

<%
	final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	
	//Resource type
	final String[] resourceTypes = properties.get(RESOURCE_TYPE_PROPERTY, String[].class);
	final List<Tag> resourceTypeTags = Utilities.getTags(resourceTypes, tagManager);
	final StringBuilder resourceTypesBuilder = new StringBuilder();
	for(int i = 0; i < resourceTypeTags.size(); i++) {
		final Tag resourceTypeTag = resourceTypeTags.get(i);
		resourceTypesBuilder.append(resourceTypeTag.getName());
		resourceTypesBuilder.append(i == resourceTypeTags.size() - 1 ? "" : ",");
	}
	final String resourceTypeStr = resourceTypesBuilder.toString();
	
	//Categories 
	final String[] categories = properties.get(CATEGORIES_PROPERTY, String[].class);
	final List<Tag> categoryTags = Utilities.getTags(categories, tagManager);
	final StringBuilder categoriesBuilder = new StringBuilder();
	for(int i = 0; i < categoryTags.size(); i++) {
		final Tag categoryTag = categoryTags.get(i);
		categoriesBuilder.append(categoryTag.getTagID());
		categoriesBuilder.append(i == categoryTags.size() - 1 ? "" : ",");
	}
	final String categoriesStr = categoriesBuilder.toString();
	
	//Featured
	final String[] featured = properties.get(FEATURED_PROPERTY, String[].class);
	final List<Tag> featuredTags = Utilities.getTags(featured, tagManager);
	final StringBuilder featuredBuilder = new StringBuilder();
	for(int i = 0; i < featuredTags.size(); i++) {
		final Tag featuredTag = featuredTags.get(i);
		featuredBuilder.append(featuredTag.getTagID());
		featuredBuilder.append(i == featuredTags.size() - 1 ? "" : ",");
	}
	final String featuredStr = featuredBuilder.toString();
%>

<c:set var="containerId" value="<%= containerId %>"/>
<c:set var="resourceTypeStr" value="<%= resourceTypeStr %>"/>
<c:set var="categoriesStr" value="<%= categoriesStr %>"/>
<c:set var="featuredStr" value="<%= featuredStr %>"/>

<div id="${containerId}" class="resource-type-container-featured" resource-featured-container="${containerId}" resource-type="${resourceTypeStr}" resource-categories="${categoriesStr}" resource-featured="${featuredStr}" resource-limit="4" pagination-index="0" order-by="${properties['orderBy']}">
	<%-- AJAX will populate this component --%>
</div>