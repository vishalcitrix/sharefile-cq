<%--
    Tool Tip
    
    This component is used as a tool tip placeholder. Authors would place this component anywhere 
    on the page and reference this functionality through the link behavior with the correct option.
    In edit or read only mode, this component will display the anchor link which this can be used as 
    reference.
    
    achew@siteworx.com    
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String CONTAINER_ID = "containerId";
    private static final String TEXT_PROPERTY = "text";
%>

<%
	final Map<String, Object> toolTip = new HashMap<String, Object>();

	String containerId = properties.get(CONTAINER_ID, null);
	if(containerId == null && (WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY))) {
		Node tooltipNode = currentNode;
		
		if(tooltipNode == null) {
	    	final Session session = resource.getResourceResolver().adaptTo(Session.class);
	    	tooltipNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
	    	session.save();
		}
		
		if(!tooltipNode.hasProperty(CONTAINER_ID)) {
			final String generatedContainerId = ClientContextUtil.getId(resource.getPath()).replaceAll("-", "");
			tooltipNode.setProperty(CONTAINER_ID, generatedContainerId);
			tooltipNode.save();
		}
		containerId = properties.get(CONTAINER_ID, null);
	}
	
	toolTip.put(CONTAINER_ID, containerId);
	toolTip.put(TEXT_PROPERTY, properties.get(TEXT_PROPERTY, null));
%>

<c:set var="toolTip" value="<%= toolTip %>"/>

<c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Tool Tip: Hidden container ID: <a class="warning" onclick="Utilities.copyToClipboard('#${toolTip.containerId}');"><i>#${toolTip.containerId}</i></a></div></c:if>

<div id="${toolTip.containerId}" style="display:none">
	${toolTip.text}
</div>