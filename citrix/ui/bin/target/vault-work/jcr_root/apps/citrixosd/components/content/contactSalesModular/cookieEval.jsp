<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final String website = properties.get("website", "GoToMyPC");
    String salesForceId = properties.get("saleForceId","");
    
    String visitorCookie = null;
    boolean visitorCookieSet = false;
    
    String cookieStr = "";
    
    final String salesForceChannelParam = request.getParameter("c_cmp");
    final String salesForceIdParam = request.getParameter("sfid");
    
    //get cookie string
    if(website == "GoToMyPC"){
        cookieStr = "ercVisitor";
    }else{
        cookieStr = "__col_mkt_" + website;
    }
    
    //get salesForceId
    if(salesForceChannelParam != null && salesForceChannelParam.indexOf("sf-") > -1) {
        salesForceId = salesForceChannelParam.substring(3);    
    }
    if(salesForceIdParam != null) {
        salesForceId = salesForceIdParam;
    }
    
%>

<script type="text/javascript">
    var defaultID = '<%= properties.get("saleForceId","") %>';
    theForm = document.forms['contactSales'];
    jQuery(document).ready(function(){
        theForm = document.forms['contactSales'];
        var sfid = '';
        
        var mktCookie = getmktCookie('<%= cookieStr  %>');
        if(mktCookie != null && mktCookie != '') {
            sfid = getCookieSFID(mktCookie,'<%= website %>');
        }
        
        var c_cmp = getURLParam('c_cmp') ;
        if(c_cmp != null && c_cmp.indexOf('sf-') > -1) {
            sfid = c_cmp.substring(3);
        }
       
        var sfParam = getURLParam('sfid') ;
        if(sfParam != null) {
            sfid = sfParam;
        }
       
        if(theForm.sfdc_campaign_id.value == defaultID && sfid != '')
            theForm.sfdc_campaign_id.value = sfid;
    });
</script>