<%--
    Resource Container
    
    Container will hold any resource related components. Authors will have to choose which 
    resource type they want to display. Every resource type will automatically make a  container 
    for which authors can insert other resource components in which can be used to query.
    
    Dependencies: Resource New (Javascript)

    vishal.gupta@citrix.com
--%>

<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="java.util.Properties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    private static final String ALLOWED_RESOURCE_TYPES_PROPERTY = "allowedResourceTypes";
    private static final String TITLE_PROPERTY = "title";
%>

<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    final String[] resourceTypes = properties.get(ALLOWED_RESOURCE_TYPES_PROPERTY, String[].class);
    final List<Tag> resourceTypeTags = Utilities.getTags(resourceTypes, tagManager);
    
%>
<c:set var="resourceTypeTags" value="<%= resourceTypeTags %>"/>


    <div class="resource-toolbar" style="${fn:length(resourceTypeTags) == 1 ? 'margin: 0 0 0 0;' : ''}">
        <c:if test="${not empty properties['title']}">
            <h4>${properties['title']}</h4>
        </c:if>
        <ul class="resource-link-type-container">
            <c:forEach items="${resourceTypeTags}" var="resourceTypeTag" varStatus="i">
                <li class="resource-type-item" style="${fn:length(resourceTypeTags) == 1 ? 'display:none' : ''}">
                    <c:choose>
                        <c:when test="${i.first}">
                            <%--Do not display pipe --%>
                        </c:when>
                        <c:otherwise>
                            <span class="resource-type-pipe">/</span>
                        </c:otherwise>
                    </c:choose>
                    <a class="resource-type-hash" href="#${resourceTypeTag.name}"><fmt:message key="resource.type.${resourceTypeTag.name}"/></a>
                </li>
            </c:forEach>
        </ul>
        <div class="clearBoth"></div>
    </div>
    <c:forEach items="${resourceTypeTags}" var="resourceTypeTag" varStatus="i">
        <c:if test="${isEditMode}"><div class="warning">${resourceTypeTag.title} Container</div></c:if>
        <div resource-type-container="#${resourceTypeTag.name}" style="${isEditMode ? '' : 'display: none;'}">
            <cq:include path="${resourceTypeTag.name}_container" resourceType="foundation/components/parsys"/>
        </div>
    </c:forEach>


<c:if test="${isEditMode}">
    <div class="warning">Resource Container: End</div>
</c:if>
