
<%--
	Solution Component
	
	kguamanquispe@siteworx.com
	
 --%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="text" value="<%= properties.get("richtext/text", null) %>"/>

<div class="${properties.showBorder ? 'show-top-border' : ' '}">
	<div class="logo">
		<div style="top:${properties.top}px; left: ${properties.left}px; position: relative;">
			<cq:include path="image" resourceType="citrixosd/components/content/imageRenditions" />
		</div>
	</div>
	<div class="description">
		<c:choose>
			<c:when test="${not empty text }">
				<%	WCMMode mode = WCMMode.fromRequest(request); 
					if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} 
				%>
					<cq:include path="richtext" resourceType="citrixosd/components/content/text" />
				<%	mode.toRequest(request); %>
			</c:when>
			<c:otherwise>
				<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
					<span class="cq-text-placeholder-ipe">&para;</span>
       			</c:if>
			</c:otherwise>	
		</c:choose>
	</div>
</div>

<div class="clearBoth"></div>
