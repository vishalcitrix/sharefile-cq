<%--
	Page: includes head, body, and foot
--%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.foundation.ELEvaluator"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%-- ensuring that the request is not fulfilled here if it's already been fulfilled --%>
<c:set var="isForwarded" value="<%=request.getAttribute("isForwarded")%>"/>
<c:if test="${not isForwarded}">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<%= currentPage.getLanguage(false) %>">

<cq:include script="head.jsp"/>

<cq:include script="body.jsp"/>

</html>
</c:if>