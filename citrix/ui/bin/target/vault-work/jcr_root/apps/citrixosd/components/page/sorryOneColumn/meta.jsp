<%@page import="javax.jcr.Node"%>
<%@page import="javax.jcr.NodeIterator"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
	String language = "";
	String[] array = null;
	final Page localePage = currentPage.getAbsoluteParent(2);
	if (localePage != null) {
	    language = localePage.getName();
	    array = language.split("-"); 
	}
%>
<meta http-equiv="content-language" content="<%= array[1] %>">
<%

	if (currentNode.hasNode("meta")) {
		final Node metaNode = currentNode.getNode("meta");
		
	    NodeIterator iter = metaNode.getNodes();
	    while (iter.hasNext()) {
	        Node metaItemNode = iter.nextNode();
	        if (metaItemNode.hasProperty("http") && metaItemNode.hasProperty("content"))
	            %> <meta http-equiv="<%=metaItemNode.getProperty("http").getString() %>" content="<%=metaItemNode.getProperty("content").getString() %>"/><%
	    }
	}
%>