<%--

  Selectbox component.
  Selectbox  for Custom Form
  
  vishal.gupta.82@citrix.com

--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    private final static String SELECTBOX_PROPERTY = "selectbox"; 
%>
<% 
    final String fieldLabel = properties.get("fieldLabel","");   
    final String fieldName = properties.get("fieldName","");
    final String fieldId = properties.get("fieldId","");
    final boolean isRequired = properties.get("isRequired",false);
    final String defaultOptionText = properties.get("defaultOptionText","");
    final String defaultOptionValue = properties.get("defaultOptionValue","");

    if (editContext != null && editContext.getEditConfig() != null) {
        if(fieldName == ""){
              editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Selectbox - (Please select field name)"));
        }else{
              editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Selectbox - (name: " + fieldName + ")"));
        }

        editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
    }
  
    //using Utilities to parse structured multi field
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode(SELECTBOX_PROPERTY)) {
        final Node baseNode = currentNode.getNode(SELECTBOX_PROPERTY);
        values = Utilities.parseStructuredMultifield(baseNode);
    }
%>

<c:set var="values" value="<%= values %>"/>
<c:set var="isRequired" value="<%= isRequired %>" />
<c:set var="fieldId" value="<%= fieldId %>"/>
<c:set var="fieldName" value="<%= fieldName %>"/>
<c:set var="fieldLabel" value="<%= fieldLabel %>"/>
<c:set var="defaultOptionText" value="<%= defaultOptionText %>"/>
<c:set var="defaultOptionValue" value="<%= defaultOptionValue %>"/>

<c:if test="${empty defaultOptionText}">
    <c:set var="defaultOptionText" value="Choose one..."/>
</c:if>
    
<c:if test="${fieldLabel ne ''}">
    <label for="${fieldName}">${fieldLabel}</label>
</c:if>
 
<c:choose>
    <c:when test="${fn:length(values) > 0}">
        <c:choose>
            <c:when test="${not empty fieldId}">
                <c:choose>
                    <c:when test="${isRequired}">
                         <select id="${fieldId}" name="${fieldName}" class="required">
                    </c:when>
                    <c:otherwise>
                         <select id="${fieldId}" name="${fieldName}">
                    </c:otherwise>  
                </c:choose>   
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${isRequired}">
                        <select name="${fieldName}" class="required">
                    </c:when>
                    <c:otherwise>
                        <select name="${fieldName}">
                    </c:otherwise>  
                </c:choose> 
            </c:otherwise>  
        </c:choose>
        <option value="${defaultOptionValue}">${defaultOptionText}</option>
        <c:forEach items="${values}" var="item"> 
            <option value="${item.optionValue.string}" <c:if test="${not empty param[fieldName] ? param[fieldName] eq item.optionValue.string : false}">selected</c:if>>${item.optionLabel.string}</option>
        </c:forEach>
        </select>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">SelectBox: Please enter selectbox options</div></c:if>
    </c:otherwise>  
</c:choose>
