<%--

  dateField component.
  vishal.gupta.82@citrix.com
  
--%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>

<%
	final String fieldLabel = properties.get("dateFieldLabel","");  
	final String fieldId = properties.get("dateFieldId", "");
	final String fieldName = properties.get("dateFieldName", ""); 
	final boolean isRequired = properties.get("isRequired",false);
	
	if (editContext != null && editContext.getEditConfig() != null) {
		if(fieldName == ""){
		    editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Date Field - (Please select field name)"));  
		}else{
		    editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Date Field - (name: " + fieldName + ")"));   
		}
		
		editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator()); 
	}
%>

	<c:set var="isRequired" value="<%= isRequired %>" />
	<c:set var="fieldId" value="<%= fieldId %>" />
	<c:set var="fieldName" value="<%= fieldName %>" />
	<c:set var="fieldLabel" value="<%= fieldLabel %>" />
    
	<c:if test="${fieldLabel ne ''}">
        <label for="${fieldName}">${fieldLabel}</label>
    </c:if>
	<c:choose>
        <c:when test="${fieldId ne ''}">
        	<c:choose>
		        <c:when test="${isRequired}">
		            <input class="required" type="text" id="<%= fieldId %>" name="<%= fieldName %>" class="datePicker">
		        </c:when>
		        <c:otherwise>
		           	<input type="text" id="<%= fieldId %>" name="<%= fieldName %>" class="datePicker">
		        </c:otherwise>  
		    </c:choose>
        </c:when>
        <c:otherwise>
        	<c:choose>
		        <c:when test="${isRequired}">
		            <input type="text" name="<%= fieldName %>" class="datePicker required" constraint="date">
		        </c:when>
		        <c:otherwise>
		           	<input type="text" name="<%= fieldName %>" class="datePicker" constraint="date">
		        </c:otherwise>  
		    </c:choose>
        </c:otherwise>  
    </c:choose>
	
