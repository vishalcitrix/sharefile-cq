<%--
	Next Webinar
	
	Next Webinar will automatically find the next webinar based on the langauge. If there is a 
	manual webinar path set and the webinar page is valid; this will not do an AJAX call, instead 
	this will get the webinar details before it hits the end-user.

	achew@siteworx.com
--%>

<%@page import="com.citrixosd.models.ResourceWebinar"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.citrixosd.servlets.ResourceServlet"%>
<%@page import="java.util.List"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>


<%!
	private static final String MANUAL_WEBINAR_PATH_PROPERTY = "manualWebinarPath";
	private static final String CATEGORIES_PROPERTY = "categories";
%>

<%
	final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

	//Check for manual webinar path
	final String manualWebinarPath = properties.get(MANUAL_WEBINAR_PATH_PROPERTY, null);
	ResourceWebinar manualResourceWebinar = null;
	if(manualWebinarPath != null) {
		final Page webinarPage = currentPage.getPageManager().getPage(manualWebinarPath);
		final String[] webinarPageMixinTypes = webinarPage.getProperties().get("jcr:mixinTypes", String[].class);
		if(webinarPageMixinTypes != null && webinarPageMixinTypes.length > 0) {
			for(String mixinType : webinarPageMixinTypes) {
				if(mixinType.equals("mix:ResourceWebinar")) {
					final Node manualWebinar = webinarPage.adaptTo(Node.class);
					if(manualWebinar != null) {
						manualResourceWebinar = ResourceServlet.getResourceWebinar(manualWebinar, resource.getResourceResolver().adaptTo(TagManager.class), slingRequest.getResourceBundle(request.getLocale()), sling.getService(ContextRootTransformer.class), DateFormat.getDateInstance(DateFormat.LONG, request.getLocale()), DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, request.getLocale()), DateFormat.getTimeInstance(DateFormat.SHORT, request.getLocale()), "true");
					}
					break;
				}
			}
		}
	}
	
	//Categories 
	final String[] categories = properties.get(CATEGORIES_PROPERTY, String[].class);
	final List<Tag> categoryTags = Utilities.getTags(categories, tagManager);
	final StringBuilder categoriesBuilder = new StringBuilder();
	for(int i = 0; i < categoryTags.size(); i++) {
		final Tag categoryTag = categoryTags.get(i);
		categoriesBuilder.append(categoryTag.getTagID());
		categoriesBuilder.append(i == categoryTags.size() - 1 ? "" : ",");
	}
	final String categoriesStr = categoriesBuilder.toString();

%>

<c:set var="containerId" value="<%= containerId %>"/>
<c:set var="manualResourceWebinar" value="<%= manualResourceWebinar %>"/>
<c:set var="categoriesStr" value="<%= categoriesStr %>"/>

<div id="${containerId}" class="resource-type-container-next-webinar" <c:if test="${empty manualResourceWebinar}">resource-next-webinar-container="${containerId}"</c:if> resource-type="ResourceWebinar" resource-categories="${categoriesStr}" resource-limit="1" order-by="asc" webinar-upcoming-only="true">
	<div class="next-webinar-container">
	
		<div class="start-gradient"></div>
		<div class="end-gradient"></div>
		<div class="content-container">
			<div class="webinar-content">
				<c:if test="${not empty properties['title']}">
					<div class="title-container"><strong>${properties['title']}</strong></div>
				</c:if>
				<div class="webinar-content-container">
					<div class="webinar-title"><p><strong class="webinar-title-placeholder">${not empty manualResourceWebinar.title ? manualResourceWebinar.title : ''}</strong></p></div>
					<div class="webinar-author-title"><p><fmt:message key="webinar.guest.speaker"/><span class="webinar-author-title-placeholder">${not empty manualResourceWebinar.authorTitle ? manualResourceWebinar.authorTitle : ''}</span></p></div>
					<div class="webinar-author-description ellipsis"><p class="webinar-author-description-placeholder">${not empty manualResourceWebinar.authorDescription ? manualResourceWebinar.authorDescription : ''}</p></div>
				</div>
				<div class="webinar-datetime-container">
					<strong><fmt:message key="webinar.label.live.on"/><span class="webinar-upcoming-long-placeholder">${not empty manualResourceWebinar ? manualResourceWebinar.upcomingLong : ''}</span>
					</strong>
				</div>
				<div class="webinar-button-container">
					<a href="${not empty manualResourceWebinar ? manualResourceWebinar.path : '#'}<%-- AJAX will populate this field --%>" class="webinar-registration-button button orange"><fmt:message key="webinar.label.register"/></a>
				</div>
			</div>
			
			<div class="webinar-thumbnail">
				<div class="webinar-thumbnail-container">
					<c:choose>
						<c:when test="${not empty manualResourceWebinar}">
							<img src="${manualResourceWebinar.authorThumbnailPath}"/>
						</c:when>
						<c:otherwise>
							<%-- AJAX will populate this field --%>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			
			<div class="clearBoth"></div>
		</div>
		
	</div>
</div>