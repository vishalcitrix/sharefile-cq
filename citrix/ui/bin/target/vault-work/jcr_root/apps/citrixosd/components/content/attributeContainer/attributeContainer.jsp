<%--
	Attribute Container
	
	A container containing specific id, class, and or attributes. This is mostly used as 
	a component dealing with java-script because of the ID property.

	achew@siteworx.com
 --%>
 
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities" %>
 
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String ATTRIBUTES_PROPERTY = "attributes";
%>

<%
	ArrayList<Map<String, Property>> attributes = null;
	if(currentNode != null && currentNode.hasNode(ATTRIBUTES_PROPERTY)) {
		attributes = Utilities.parseStructuredMultifield(currentNode.getNode(ATTRIBUTES_PROPERTY));
	}
%>

<c:set var="attributes" value="<%= attributes %>"/>

<div id="${properties['id']}" class="${properties['class']}"
	<c:forEach items="${attributes}" var="attribute">
		${attribute.key.string}="${attribute.value.string}" 
	</c:forEach>
>
	<cq:include script="content.jsp"/>
</div>