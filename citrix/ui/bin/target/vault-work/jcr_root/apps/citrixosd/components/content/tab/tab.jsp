<%--
	Tab
	
	This will hold either reference to another page on the site, or it will handle javascript
	show and hide feature to display the content. Default will send the users to another page. 
	This will also contain a parsys so authors can put content in the tab. If javascript is selected 
	the content will be split into as many links as the author specified.
	
	achew@siteworx.com
--%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%!
	public static final String ACTION_PROPERTY = "action";
	public static final String TABS_PROPERTY = "tabs";
%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
	ArrayList<Map<String, Property>> tabs = null;
	if(currentNode != null) {
		if(currentNode.hasNode(TABS_PROPERTY)) {
			tabs = Utilities.parseStructuredMultifield(currentNode.getNode(TABS_PROPERTY));		
		}
	}
	
	final String generatedContainerId = ClientContextUtil.getId(resource.getPath()).replaceAll("-", "");
%>

<c:set var="tabs" value="<%= tabs %>"/>
<c:set var="generatedContainerId" value="<%= generatedContainerId %>"/>

<div id="${generatedContainerId}" class="tab-container">
	<c:choose>
		<c:when test="${properties['action'] eq 'renderInJavascript'}">
			<ul class="tab-link-container">
				<c:forEach items="${tabs}" var="tab" varStatus="i">
					<li class="${i.first ? 'first active' : ''}"><a href="#" class="tab-button ${tab.sprite.string}" rel="tab" tab-container-id-link="${generatedContainerId}" index="${i.count}">${tab.text.string}</a></li>
				</c:forEach>
			</ul>
			<div class="clearBoth"></div>
			<c:forEach items="${tabs}" varStatus="i">
				<c:if test="${isEditMode}"><div class="warning">Tab: Starting tab [${i.count}] container</div></c:if>
					<div class="tab-content-container ${(isEditMode || isReadOnlyMode) || i.first ? '' : 'hide'}"  rel="tab" tab-container-id-content="${generatedContainerId}" index="${i.count}">
						<cq:include path="tab_${i.count}-content" resourceType="foundation/components/parsys"/>
					</div>
				<c:if test="${isEditMode}"><div class="warning">Tab: End tab [${i.count}] container</div></c:if>	
			</c:forEach>
		</c:when>
		
		<c:otherwise>
			<ul class="tab-link-container">
				<c:forEach items="${tabs}" var="tab" varStatus="i">
					<li class="${i.first ? 'first' : ''} ${currentPage.path eq tab.path.string ? 'active' : ''}"><a href="${tab.path.string}" class="tab-button ${tab.sprite.string}"  rel="tab">${tab.text.string}</a></li>
				</c:forEach>
			</ul>
			<div class="clearBoth"></div>
			<c:if test="${isEditMode}"><div class="warning">Tab: Starting tab container</div></c:if>
				<div>
					<cq:include path="tab-content" resourceType="foundation/components/parsys"/>
				</div>
			<c:if test="${isEditMode}"><div class="warning">Tab: End tab container</div></c:if>	
		</c:otherwise>
	</c:choose>
</div>
