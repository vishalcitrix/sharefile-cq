<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String HIDE_SITEMAP_PROPERTY = "hideSitemap"; %>
<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>

<c:if test="<%= !properties.get(HIDE_SITEMAP_PROPERTY, false) %>">
    <cq:include path="sitemap" resourceType="swx/component-library/components/content/single-ipar"/>
</c:if>

<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>

<cq:include script="lightbox.jsp"/>

<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>