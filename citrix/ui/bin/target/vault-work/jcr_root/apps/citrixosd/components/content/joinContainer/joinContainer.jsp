<%--
	Join Container
	
	Container containing a RTE and a form component. This also holds 
	the sprite which is shown near the description.
	
	achew@siteworx.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="join-container-container ${properties['sprite']}">
	<c:if test="${empty properties['hideContent'] || properties['hideContent'] == false}">
		<div class="join-container-text-container">
			<div class="join-container-sprite ${properties['sprite']}"></div>
			<cq:include script="content.jsp"/>
		</div>
	</c:if>
	
	<div class="join-container-form-container">
		<cq:include script="form.jsp"/>
	</div>
</div>