<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select name="Department__c" id="Department__c" class="required">
    <option value=""><fmt:message key="form.option.default"/></option>
    <option value="Consulting"><fmt:message key="form.option.department.consulting"/></option>
    <option value="Customer Service"><fmt:message key="form.option.department.customerService"/></option>
    <option value="Engineering"><fmt:message key="form.option.department.engineering"/></option>
    <option value="Finance/Legal/Administration"><fmt:message key="form.option.department.financialLegalAdministration"/></option>
    <option value="Human Resources"><fmt:message key="form.option.department.humanResource"/></option>
    <option value="IT"><fmt:message key="form.option.department.it"/></option>
    <option value="Marketing/PR"><fmt:message key="form.option.department.marketingPR"/></option>
    <option value="Operations"><fmt:message key="form.option.department.operations"/></option>
    <option value="Professional Services"><fmt:message key="form.option.department.professionalService"/></option>
    <option value="Sales/Business Development"><fmt:message key="form.option.department.salesBusinessDevelopment"/></option>
    <option value="Training"><fmt:message key="form.option.department.training"/></option>
    <option value="Other"><fmt:message key="form.option.department.other"/></option>
</select>