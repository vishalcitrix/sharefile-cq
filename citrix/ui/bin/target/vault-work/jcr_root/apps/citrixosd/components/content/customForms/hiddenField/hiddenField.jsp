<%--

  Hidden field component.
  Hidden field for Custom Form
  
  vishal.gupta.82@citrix.com

--%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@include file="/libs/foundation/global.jsp"%>
<%
    // TODO add you code here
    final String fieldId = properties.get("fieldId","");   
    final String fieldName = properties.get("fieldName","");
	final String fieldValue = properties.get("fieldValue","");
	
	if (editContext != null && editContext.getEditConfig() != null) {
		if(fieldName == ""){
	        editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Hidden Field - (Please select field name)"));  
	    }else{
	        editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Hidden Field - (name: " + fieldName + ")"));     
	    }
	    
	    editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
	}
%>
    
    <c:set var="fieldId" value="<%= fieldId %>" />
    
    <c:choose>
        <c:when test="${fieldId ne ''}">
            <input type="hidden" id="${fieldId}" name="<%= fieldName %>" value="<%= fieldValue %>">
        </c:when>
        <c:otherwise>
            <input type="hidden" name="<%= fieldName %>" value="<%= fieldValue %>">
        </c:otherwise>  
    </c:choose>
