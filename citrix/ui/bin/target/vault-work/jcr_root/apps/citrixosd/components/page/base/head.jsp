<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="java.util.Locale"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
	public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics";
%>
<%
	response.setHeader("Dispatcher", "no-cache");
	final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
	String designPath = currentDesign.getPath();
	String designPathOrig = currentDesign.getPath();
	String transformed = transformer.transform(designPath);
	if (transformed != null) {
		designPath = transformed;
	}
%>

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="favIcon" value="<%= SiteUtils.getFavIcon(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />
<c:set var="showMobileRedirect" value="<%= properties.get("showMobileRedirect", false) %>"/>

<head>
    <%--Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <cq:include script="abtest.jsp"/>
    <cq:include script="meta.jsp"/>
    <c:if test="${showMobileRedirect}">
        <cq:include script="mobileRedirect.jsp"/>
    </c:if>

    <%--CSS --%>
    <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/static.css" type="text/css">

    <!--[if IE 7]>
        <link rel="stylesheet" href="${designPath}/css/IE7.css" type="text/css">
    <![endif]-->
    <!--[if IE 8]>
        <link rel="stylesheet" href="${designPath}/css/IE8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" href="${designPath}/css/IE9.css" type="text/css">
    <![endif]-->

    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/css/static/images/${not empty favIcon ? favIcon : 'favicon'}.ico">
    <link rel="image_src" href="<%= currentDesign.getPath() %>/css/static/images/logo_fb.jpg">

    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    <meta name="keywords" content="${not empty siteName ? siteName : ''}">

    <%-- DNT --%>
    <%
        Locale locale = null;
        final Page localePage = currentPage.getAbsoluteParent(2);
        if (localePage != null) {
            try {
                locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
            } catch (Exception e) {
                locale = request.getLocale();
            }
        }
        else
            locale = request.getLocale();
    %>
    <script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
    <script type="text/javascript">
        dnt.setMsgClass('footer-dnt');
        dnt.setMsgLocId('footer-dnt');
        dnt.setLocale('<%=locale.toString()%>');
        dnt.dntInit();
    </script>

    <script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
		<script type="text/javascript" src="https://sadmin.brightcove.com/js/APIModules_all.js"></script>
		<script type="text/javascript" src="https://files.brightcove.com/bc-mapi.js"></script>
    <c:if test="<%= properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
        <cq:include script="analytics.jsp"/>
    </c:if>

     <%--Javascripts - Place after including jQuery Lib --%>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.tools.min.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/scripts.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.stationaryTip.js"></script>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.dotdotdot-1.5.9.min.js"></script>

 	<cq:include script="channelTracking.jsp"/>

    <%--IE10 stylesheet--%>
    <script>
        jQuery(function() {
            if (jQuery.browser.msie && jQuery.browser.version == 10) {
                jQuery("head").append("<link rel='stylesheet' href='${designPath}/css/IE10.css' type='text/css'/>");
            }
        });

    </script>

    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</head>