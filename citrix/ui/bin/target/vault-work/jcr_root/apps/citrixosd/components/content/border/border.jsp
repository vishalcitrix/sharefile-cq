<%--
	Border
	
	Border is a container that has the ability to put a border around the content.
	There is also an option to select the border color and width.

	achew@siteworx.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="border-container" style="
	<c:if test="${properties.borderLeft == true}">border-left: ${properties.borderWidth}px solid #${not empty properties.borderColor ? properties.borderColor : '000000'};</c:if>
	<c:if test="${properties.borderRight == true}">border-right: ${properties.borderWidth}px solid #${not empty properties.borderColor ? properties.borderColor : '000000'};</c:if>
	<c:if test="${properties.borderTop == true}">border-top: ${properties.borderWidth}px solid #${not empty properties.borderColor ? properties.borderColor : '000000'};</c:if>
	<c:if test="${properties.borderBottom == true}">border-bottom: ${properties.borderWidth}px solid #${not empty properties.borderColor ? properties.borderColor : '000000'};</c:if>
">
	<c:if test="${properties.borderLeft == true}">
		<div class="border-container-top-left"></div>
		<div class="border-container-bottom-left"></div>
	</c:if>
	<c:if test="${properties.borderRight == true}">
		<div class="border-container-top-right"></div>
		<div class="border-container-bottom-right"></div>
	</c:if>
	<c:if test="${properties.borderTop == true}">
		<div class="border-container-left-top"></div>
		<div class="border-container-right-top"></div>
	</c:if>
	<c:if test="${properties.borderBottom == true}">
		<div class="border-container-left-bottom"></div>
		<div class="border-container-right-bottom"></div>
	</c:if>
		
	<div class="border-content-container" style="
		<c:if test="${not empty properties.paddingLeft}">padding-left: ${properties.paddingLeft}px;</c:if>
		<c:if test="${not empty properties.paddingRight}">padding-right: ${properties.paddingRight}px;</c:if>
		<c:if test="${not empty properties.paddingTop}">padding-top: ${properties.paddingTop}px;</c:if>
		<c:if test="${not empty properties.paddingBottom}">padding-bottom: ${properties.paddingBottom}px;</c:if>
	">
		<cq:include script="content.jsp"/>
	</div>
</div>