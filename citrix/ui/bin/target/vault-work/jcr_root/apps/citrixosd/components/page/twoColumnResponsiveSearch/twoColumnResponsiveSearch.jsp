<%--
	Page: includes head, body, and foot
--%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.foundation.ELEvaluator"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<c:if test="${not empty properties['redirectTarget']}">
    <cq:include script="redirect.jsp"/>
</c:if>

<%-- ensuring that the request is not fulfilled here if it's already been fulfilled --%>
<c:set var="isForwarded" value="<%=request.getAttribute("isForwarded")%>"/>
<c:if test="${not isForwarded}">

<!DOCTYPE html>
 <html lang="<%= currentPage.getLanguage(false) %>">

<cq:include script="head.jsp"/>

<%-- workaround for putting redirect include above the head for performance sake --%>
<c:if test="${not empty properties['redirectTarget']}">
    <cq:include script="redirectMessage.jsp"/>
</c:if>

<cq:include script="body.jsp"/>

</html>
</c:if>