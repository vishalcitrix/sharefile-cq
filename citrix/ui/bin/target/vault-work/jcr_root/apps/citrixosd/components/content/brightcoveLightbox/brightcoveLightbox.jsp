<%--
    Brightcove Lightbox
    
    Display the brightcove video player in a lightbox container. Player requires player id and key 
    for the video to be displayed in the UI. Since each video could have different sizes each player 
    is created independently. The player would also have to have a video id to be view correctly. If 
    there is a image rendition used, then this will automatically override the default brightcove video 
    thumbnail. Authors can also adjust the size of the video which will take affect on the lightbox as well. 
    If there is a title, then this will override the original brightcove title.
    
    Dev: There is a brightcove javascript clientlibs injected in this component from 'etc/design/brightcove/
    brightcoveExperience'. The height and the width of the component is based on the author input which is displayed 
    on the lightbox container. The javascript will put the brightcove object in the lightbox container 
    when the page loads. The object is in the lightbox container because we want to restrict the use of 
    multiple lightbox containers. Since there is one lightbox containers, we don't run into conflicts with 
    multiple lightbox appearing. There is also a container id for each video container to differentiate each 
    video player. The length of the video is calculated with mathematical logic to determine the hours, minutes, 
    seconds.
    
    achew@siteworx.com

 --%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.exceptions.BrightcoveException"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.apiobjects.Video"%>
<%@page import="java.util.EnumSet"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.VideoFieldEnum"%>
<%@page import="com.brightcove.proserve.mediaapi.wrapper.ReadApi"%>
<%@page import="com.day.cq.wcm.api.components.DropTarget" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.UUID" %>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%@include file="/apps/brightcove/global/configuration.jsp"%>

<%!
    private static final String CONTAINER_ID = "containerId";
    private static final String HEIGHT_PROPERTY = "height";
    private static final String WIDTH_PROPERTY = "width";
    private static final String VIDEO_PLAYER_PROPERTY = "videoPlayer";
    private static final String VIDEO_TITLE_PROPERTY = "videoTitle";
    private static final String VIDEO_LENGTH_PROPERTY = "videoLength";
    private static final String VIDEO_THUMBNAILURL_PROPERTY = "videoThumbnailUrl";
    private static final String VIDEO_TITLE_OVERRIDE_PROPERTY = "videoTitleOverride";
    private static final String TITLE_PROPERTY = "title";
    private static final String LAYOUT_POSITION_PROPERTY = "layoutPosition";
    private static final String PLAYER_ID_PROPERTY = "playerId";
    private static final String PLAYER_KEY_PROPERTY = "playerKey";
    private static final String UPDATE_VIDEO_METADATA_PROPERTY = "updateVideoMetadata";

    private static final String DD_CLASS_NAME = DropTarget.CSS_CLASS_PREFIX + "brightcove";

    /**
     * Retrieve the video data from brightcove servers and store the metadata to the current node
     */
    private void updateVideoMetadata(String readToken, String videoId, Node node) {
        final ReadApi read = new ReadApi();
        try {
            final Video video = read.FindVideoById(readToken, Long.parseLong(videoId), EnumSet.of(VideoFieldEnum.NAME, VideoFieldEnum.LENGTH, VideoFieldEnum.THUMBNAILURL, VideoFieldEnum.ID), null);
            updateNodeProperty(node, VIDEO_TITLE_PROPERTY, video.getName());
            updateNodeProperty(node, VIDEO_LENGTH_PROPERTY, video.getLength());
            updateNodeProperty(node, VIDEO_THUMBNAILURL_PROPERTY, video.getThumbnailUrl());
            node.setProperty(UPDATE_VIDEO_METADATA_PROPERTY, false);
            node.save();
        }catch (BrightcoveException be) {
            be.printStackTrace();
        }catch (RepositoryException re) {
            re.printStackTrace();
        }
    }

    /**
     * Updates the node property if there is a change in the value for string object
     */
    private void updateNodeProperty(Node node, String parameter, String value) {
        try {
            if(node.hasProperty(parameter)) {
                if(node.getProperty(parameter).equals(value)) {
                    //Do nothing, property is correct
                }else {
                    node.setProperty(parameter, value);
                    node.save();
                }
            }else {
                node.setProperty(parameter, value);
                node.save();
            }
        }catch(RepositoryException re) {
            re.printStackTrace();
        }
    }

    /**
     * Updates the node property if there is a change in the value for long object
     */
    private void updateNodeProperty(Node node, String parameter, Long value) {
        try {
            if(node.hasProperty(parameter)) {
                if(node.getProperty(parameter).equals(value)) {
                    //Do nothing, property is correct
                }else {
                    node.setProperty(parameter, value);
                    node.save();
                }
            }else {
                node.setProperty(parameter, value);
                node.save();
            }
        }catch(RepositoryException re) {
            re.printStackTrace();
        }
    }

    /**
     * Converts milliseconds to hours, minutes, and seconds ex:(1:20:32)
     */
    private String getHumanVideoTime(long milliseconds) {
        final StringBuilder builder = new StringBuilder();
        if(milliseconds < 60000L) {
            builder.append("0").append(":"); //Append the 0 minutes for ui
        }
        if(milliseconds >= 3600000) {
            int hours = (int)(milliseconds / 3600000L);
            milliseconds = milliseconds - (hours * 3600000);
            builder.append(hours).append(":");
        }
        if(milliseconds >= 60000) {
            int minutes = (int)(milliseconds / 60000L);
            milliseconds = milliseconds - (minutes * 60000);
            builder.append(minutes).append(":");
        }
        int seconds = (int)(milliseconds / 1000L);
        if(seconds  < 10) {
            builder.append("0");
        }
        milliseconds = milliseconds - (seconds * 1000);
        builder.append(seconds);

        return builder.toString();
    }
%>

<%
    String containerId = properties.get(CONTAINER_ID, null);
    if(containerId == null && (WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY))) {
        Node brightcoveLightboxNode = currentNode;

        if(brightcoveLightboxNode == null) {
            final Session session = resource.getResourceResolver().adaptTo(Session.class);
            brightcoveLightboxNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
            session.save();
        }

        if(!brightcoveLightboxNode.hasProperty(CONTAINER_ID)) {
            final String generatedContainerId = ClientContextUtil.getId(resource.getPath()).replaceAll("-", "");
            brightcoveLightboxNode.setProperty(CONTAINER_ID, generatedContainerId);
            brightcoveLightboxNode.save();
        }

        containerId = properties.get(CONTAINER_ID, null);
    }

    if(WCMMode.fromRequest(request) == WCMMode.EDIT) {
        final boolean updateVideoMetadata = properties.get(UPDATE_VIDEO_METADATA_PROPERTY, false);
        final String videoId = properties.get(VIDEO_PLAYER_PROPERTY, null);
        if(updateVideoMetadata && videoId != null) {
            updateVideoMetadata(READ_TOKEN, videoId, currentNode);
        }
    }

    final Map<String, Object> brightcoveLightbox = new HashMap<String, Object>();
    brightcoveLightbox.put(CONTAINER_ID, containerId);
    brightcoveLightbox.put(HEIGHT_PROPERTY, properties.get(HEIGHT_PROPERTY, 270));
    brightcoveLightbox.put(WIDTH_PROPERTY, properties.get(WIDTH_PROPERTY, 480));
    brightcoveLightbox.put(VIDEO_PLAYER_PROPERTY, properties.get(VIDEO_PLAYER_PROPERTY, null));
    brightcoveLightbox.put(VIDEO_TITLE_PROPERTY, properties.get(VIDEO_TITLE_PROPERTY, ""));
    brightcoveLightbox.put(VIDEO_LENGTH_PROPERTY, getHumanVideoTime(properties.get(VIDEO_LENGTH_PROPERTY, 0)));
    brightcoveLightbox.put(VIDEO_THUMBNAILURL_PROPERTY, properties.get(VIDEO_THUMBNAILURL_PROPERTY, ""));
    brightcoveLightbox.put(VIDEO_TITLE_OVERRIDE_PROPERTY, properties.get(VIDEO_TITLE_OVERRIDE_PROPERTY, null));
    brightcoveLightbox.put(LAYOUT_POSITION_PROPERTY, properties.get(LAYOUT_POSITION_PROPERTY, "bottom"));
    brightcoveLightbox.put(TITLE_PROPERTY, properties.get(TITLE_PROPERTY, null));
    brightcoveLightbox.put(PLAYER_ID_PROPERTY, properties.get(PLAYER_ID_PROPERTY, null));
    brightcoveLightbox.put(PLAYER_KEY_PROPERTY, properties.get(PLAYER_KEY_PROPERTY, null));
    brightcoveLightbox.put("videoThumbnailOverride", currentNode != null ? currentNode.hasNode("imageRenditions") ? currentNode.getNode("imageRenditions").hasProperty("fileReference") : false : false);
%>

<c:set var="brightcoveLightbox" value="<%= brightcoveLightbox %>"/>

<c:choose>
    <c:when test="${not empty brightcoveLightbox.videoPlayer}">
        <c:choose>
            <%-- Style: Hidden --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'hidden'}">
                <c:if test="${isEditMode}">
                    <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Brightcove Lightbox: Hidden container ID: <a class="warning" onclick="Utilities.copyToClipboard('#${brightcoveLightbox.containerId}');"><i>#${brightcoveLightbox.containerId}</i></a></div></c:if>
                </c:if>
            </c:when>

            <%-- Style: Daisy --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'daisy'}">
                <div class="daisy-container">
                    <span class="${properties.daisy}"></span>
                    <a class="daisy" href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}<span class="arrow"></span></a>
                </div>
            </c:when>

            <%-- Style: Text only --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'textOnly'}">
                <div>
                    <c:if test="${not empty brightcoveLightbox.title}">
                        <p class="brightcove-title">${brightcoveLightbox.title}</p>
                    </c:if>
                    <p class="brightcove-top-text">
                        <a href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}</a>
                        <span class="brightcove-video-length">(${brightcoveLightbox.videoLength})</span>
                    </p>
                </div>
            </c:when>

            <%-- Style: Image only --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'imageOnly'}">
                <div>
                    <c:choose>
                        <c:when test="${brightcoveLightbox.videoThumbnailOverride}">
                            <div style="float:left">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <swx:setWCMMode mode="READ_ONLY">
                                        <cq:include path="imageRenditions" resourceType="citrixosd/components/content/imageRenditions"/>
                                    </swx:setWCMMode>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="video-thumb" style="float:left;">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <img alt="${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}" src="${brightcoveLightbox.videoThumbnailUrl}" height="100%" width="100%"/>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:when>

            <%-- Style: Image bottom text top--%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'bottom'}">
                <div>
                    <c:if test="${not empty brightcoveLightbox.title}">
                        <p class="brightcove-title">${brightcoveLightbox.title}</p>
                    </c:if>
                    <p class="brightcove-top-text">
                        <a href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}</a>
                        <span class="brightcove-video-length">(${brightcoveLightbox.videoLength})</span>
                    </p>
                    <c:choose>
                        <c:when test="${brightcoveLightbox.videoThumbnailOverride}">
                            <div style="float:left">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <swx:setWCMMode mode="READ_ONLY">
                                        <cq:include path="imageRenditions" resourceType="citrixosd/components/content/imageRenditions"/>
                                    </swx:setWCMMode>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="video-thumb" style="float:left;">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <img alt="${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}" src="${brightcoveLightbox.videoThumbnailUrl}" height="100%" width="100%"/>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:when>

            <%-- Style: Image left text right --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'left'}">
                <div>
                    <c:choose>
                        <c:when test="${brightcoveLightbox.videoThumbnailOverride}">
                            <div style="float:left; margin-right: 15px;">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <swx:setWCMMode mode="READ_ONLY">
                                        <cq:include path="imageRenditions" resourceType="citrixosd/components/content/imageRenditions"/>
                                    </swx:setWCMMode>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="video-thumb" style="float:left;">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <img alt="${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}" src="${brightcoveLightbox.videoThumbnailUrl}" height="100%" width="100%"/>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <div class="brightcove-right-text">
                        <c:if test="${not empty brightcoveLightbox.title}">
                            <p class="brightcove-title">${brightcoveLightbox.title}</p>
                        </c:if>
                        <a href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}</a>
                        <span class="brightcove-video-length">(${brightcoveLightbox.videoLength})</span>
                    </div>
                </div>
            </c:when>

            <%-- Style: Image top text bottom --%>
            <c:when test="${brightcoveLightbox.layoutPosition eq 'top'}">
                <div>
                    <c:choose>
                        <c:when test="${brightcoveLightbox.videoThumbnailOverride}">
                            <div style="float:left">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <swx:setWCMMode mode="READ_ONLY">
                                        <cq:include path="imageRenditions" resourceType="citrixosd/components/content/imageRenditions"/>
                                    </swx:setWCMMode>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="video-thumb" style="float:left;">
                                <a href="#${brightcoveLightbox.containerId}" rel="lightbox">
                                    <img alt="${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}" src="${brightcoveLightbox.videoThumbnailUrl}" height="100%" width="100%"/>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <div class="clearBoth">
                        <c:if test="${not empty brightcoveLightbox.title}">
                            <p class="brightcove-title">${brightcoveLightbox.title}</p>
                        </c:if>
                        <a href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}</a>
                        <span class="brightcove-video-length">(${brightcoveLightbox.videoLength})</span>
                    </div>
                </div>
            </c:when>

            <%-- Style: Text Only --%>
            <c:otherwise>
                <div>
                    <p class="brightcove-top-text">
                        <a href="#${brightcoveLightbox.containerId}" rel="lightbox">${not empty brightcoveLightbox.videoTitleOverride ? brightcoveLightbox.videoTitleOverride : brightcoveLightbox.videoTitle}</a>
                        <span class="brightcove-video-length">(${brightcoveLightbox.videoLength})</span>
                    </p>
                </div>
            </c:otherwise>
        </c:choose>
    </c:when>

    <c:otherwise>
        <%--Display Brightcove placeholder --%>
        <c:if test="${isEditMode || isReadOnlyMode}">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-video-placeholder <%= DD_CLASS_NAME %>"/>
            </div>
        </c:if>
    </c:otherwise>
</c:choose>

<div class="clearBoth"></div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var brightcoveVideoContainer = "<div id='${brightcoveLightbox.containerId}' class='brightcove-lightbox' brightcove-width='${brightcoveLightbox.width}' brightcove-height='${brightcoveLightbox.height}' brightcove-player-id='${brightcoveLightbox.playerId}' brightcove-player-key='${brightcoveLightbox.playerKey}' brightcove-video-player='${brightcoveLightbox.videoPlayer}' style='width:${brightcoveLightbox.width}px;'></div>";
        jQuery('#lightbox-container div#lightbox-border').append(brightcoveVideoContainer);
    });
</script>
