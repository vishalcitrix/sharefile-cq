<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>


<div style="margin: ${properties.margin};">
	<c:choose>
		<c:when test="${properties.disableContent == true}">
			<div>&nbsp;</div>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${properties.renderParsys == true}">
					<cq:include path="margin_container_content_parsys" resourceType="foundation/components/parsys"/>
				</c:when>
				<c:otherwise>
				    <cq:include script="single-par.jsp"/>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>	
</div>
