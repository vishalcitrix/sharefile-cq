<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
 
    <cq:include script="header.jsp" />
    
    <div id="main" class="content">

        <cq:include path="banner" resourceType="swx/component-library/components/content/single-par"/>
        
        <cq:include path="stickyHeader" resourceType="swx/component-library/components/content/single-ipar"/>
    
        <div class="content-body container">
        
            <cq:include path="mainContent" resourceType="foundation/components/parsys"/>

        </div>
        
    </div>

    <cq:include script="footer.jsp" />
    
    <c:set var="redirectLink" value="<%= properties.get("redirectTarget","") %>"></c:set>
    
    <c:choose>
        <c:when test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) %>">
            <c:choose>
                <c:when test="${not empty redirectLink}">
                    <div>This page will be redirected to ${redirectLink}</div>
                </c:when>
                <c:otherwise>
                   <div>No redirect link has been mentioned.</div>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:if test="${not empty redirectLink}">
                <script type="text/javascript">
                    setTimeout('timeout_trigger()', 20000);
                    function timeout_trigger(){
                        window.location.href = "${redirectLink}";
                    }
                </script>
            </c:if>    
        </c:otherwise>
    </c:choose>
</body>