<%--
    Breadcrumb Toolbar
    
    This tool bar will generate breadcrum toolbar based on links selected via dialog.
      
    vishal.gupta@citrix.com
    
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private final static String BREADCRUMB_PROPERTY = "breadcrumbLinks"; 
%>

<%
    // TODO add you code here
	//using Utilities to parse structured multi field
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode(BREADCRUMB_PROPERTY)) {
        final Node baseNode = currentNode.getNode(BREADCRUMB_PROPERTY);
        values = Utilities.parseStructuredMultifield(baseNode);
    }
%>
<c:set var="values" value="<%= values %>"/>

<c:choose>
       <c:when test="${fn:length(values) > 0}">
       	<c:choose> 
           	<c:when test="${currentPage.depth > 3}">
           		<div class="breadCrumbs">
					<div class="container">
						<ul>
							<c:forEach items="${values}" var="item"> 
				            	<c:choose> 
					            	<c:when test="${currentPage.path eq item.path.string}">
								        <li class="selected"><a href="${item.path.string}">${item.text.string}</a></li>
								    </c:when>
								    <c:otherwise>
								         <li><a href="${item.path.string}">${item.text.string}</a></li>
								    </c:otherwise>    
								</c:choose>     
				            </c:forEach>    
				       	</ul>
					</div>
				</div>               
		    </c:when>
		    <c:otherwise>
		         <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Breadcrumbs will not be shown on home page</div></c:if>
		    </c:otherwise>    
		</c:choose>  
       </c:when>
       <c:otherwise>
           <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Please select breadcrumb links</div></c:if>
       </c:otherwise>  
   </c:choose>