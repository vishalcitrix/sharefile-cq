<%--
    Main Navigation
  
    This component will take all the child pages below the locale page and display it
    on the navigation. This will also display the current parent by using a highlighted
    css class. 
    
    Authors can also add try it free icon and text inside it.
    
     vishal.gupta@citrix.com
     
--%>

<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.wcm.api.PageFilter"%>
<%@page import="com.day.cq.wcm.api.components.Component"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final int ROOT_PAGE_DEPTH = 2; // 'content/website/language'
  
    private List<Page> getNavigationPages(Page currentPage, ServletRequest request, int navigationRootDepth) {
        List<Page> navigationPages = null;
    
        // Get all the pages that are only in the 2nd level of the web site eg. g2m/en
        Page navigationRootPage = currentPage.getAbsoluteParent(navigationRootDepth);
        if (navigationRootPage == null && currentPage != null) {
            // Use is in root page we can't determine which language the user is in, throw an error.
        }else if (navigationRootPage != null) {
            final Iterator<Page> children = navigationRootPage.listChildren(new PageFilter(request));
            navigationPages = new ArrayList<Page>();
            while (children.hasNext()) {
                Page child = children.next();
                if (!child.isHideInNav()) {
                    navigationPages.add(child);
                } else {
                    continue; //Skip this page does not have a navigation title
                }
            }
        }
        return navigationPages;
    }

%>

<%
    boolean isNavigationRoot = false;
    final Page rootPage = currentPage.getAbsoluteParent(ROOT_PAGE_DEPTH);
    if (rootPage != null && rootPage.equals(currentPage))
        isNavigationRoot = true;
    final Page currentNavigationPage = currentPage.getAbsoluteParent(ROOT_PAGE_DEPTH + 1);
    final List<Page> navigationPages = getNavigationPages(currentPage, request, ROOT_PAGE_DEPTH);
    
    final Map<String, Object> globalNavigation = new HashMap<String, Object>();
    globalNavigation.put("rootPage", rootPage);
    globalNavigation.put("currentNavigationPage", currentNavigationPage);
    globalNavigation.put("navigationPages", navigationPages);
    globalNavigation.put("isNavigationRoot", isNavigationRoot);
    
%>

<c:set var="globalNavigation" value="<%= globalNavigation %>"/>
<c:set var="tryItFree" value="<%= properties.get("tryItFree", false) %>"/> 
<c:set var="tryItFreeLinkPath" value="<%= properties.get("tryItFreeLinkPath", "") %>"/> 
<c:set var="firstLine" value="<%= properties.get("firstLine", "") %>"/>
<c:set var="secondLine" value="<%= properties.get("secondLine", "") %>"/>
<c:set var="thirdLine" value="<%= properties.get("thirdLine", "") %>"/>

<div class="nav">
    <div class="container">
        <ul>
            <li><a href="${globalNavigation.rootPage.path}" class="${empty globalNavigation.currentNavigationPage ? 'current' : ''}" id="home"><span>${globalNavigation.rootPage.navigationTitle}</span></a></li>
            <c:choose>
                <c:when test="${empty globalNavigation.navigationPages && not globalNavigation.isNavigationRoot}">
                    <%--User is at root page, this component can not determine which links to display because locale is not set --%>
                    <li><a>Root page can not have a main navigation</a></li>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${globalNavigation.navigationPages}" begin="0" var="navigationPage" varStatus="i">
                        <li>
                            <a href="${navigationPage.path}" class="${i.last ? 'last' : ''} ${empty globalNavigation.currentNavigationPage ? '' : globalNavigation.currentNavigationPage.path eq navigationPage.path ? 'current' : ''}">
                                ${empty navigationPage.navigationTitle ? navigationPage.title : navigationPage.navigationTitle}
                            </a>
                        </li>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>
<c:if test="${tryItFree}">
	<div class="container blurb">
    	<a href="${tryItFreeLinkPath}">
	       <span class="tryItFree">
	       		<c:if test="${not empty firstLine}">
       				<span class="firstLine"><fmt:message key="${firstLine}"/></span>
       			</c:if>
       			<c:if test="${not empty secondLine}">
       				<span class="secondLine"><fmt:message key="${secondLine}"/></span>
       			</c:if>
       			<c:if test="${not empty thirdLine}">
       				<span class="thirdLine"><fmt:message key="${thirdLine}"/></span>
       			</c:if>
	       </span>
	  	</a>		
	</div>
</c:if>