<%--
	Voice of the Customer component.
    
	This component is comprised of 1) Single-ipar and 2) Quote component. The component 
	itself is responsible for editing title and description fields. Both of these <b>field</b> 
	[fields] are optional. The dialog of this component includes the Quote tab attained from 
	the Quote component. The Description Tab edits the title and <b>description</b> [description] 
	fields along with the a <b>selection</b> [selection] field that manages the placement of the 
	Single-ipar (Media Left, Media Right). The Single-ipar manages its own dialog.
  
  	* Editing Notes :
  		- Title field holds a maximum of 65 characters
  	*
  	
    kguamanquispe@siteworx.com 
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String TITLE_PROPERTY = "title";
	private static final String DESCRIPTION_PROPERTY = "description";
	private static final String POSITION_PROPERTY = "position";

%>

<c:set var="leftMargin" value="margin-left: -${properties.extraText}"/>
<c:set var="rightMargin" value="margin-right: -${properties.extraText}"/>

<div class="voc-container" style="margin-top:${properties.topSpacing}px;">
	<div class="media-container float-<%= properties.get(POSITION_PROPERTY, "left") %>" 
		 style="margin-top: ${properties.marginTop}px;  		 
		 ${properties.position eq 'right' ?  'padding-left: 5' : 'padding-right:5'}px;
		 ${properties.position eq 'right' ? rightMargin : leftMargin}px;
		 ${isEditMode or isDesignMode ? 'min-width: 150px' : ''}">
				
		<cq:include path="media" resourceType="swx/component-library/components/content/single-par"/>
	</div>
	
	<div class="text-container">
		<div class="description">
		<c:if test="${not empty properties.title}"> <h3><%= properties.get(TITLE_PROPERTY, "") %></h3> </c:if>
		
		<%	WCMMode mode = WCMMode.fromRequest(request); 
			if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} %>
		<cq:include path="richtext" resourceType="g2m/components/content/text"/>
		</div>		
		
		<cq:include path="quote" resourceType="citrixosd/components/content/voiceOfTheCustomer/vocQuote"/>
	   	<%	mode.toRequest(request); %>	
			
	</div>
</div>

<div class="clearBoth"></div>