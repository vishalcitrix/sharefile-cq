<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<cq:include script="slidebar.jsp"/>
<cq:include script="lightbox.jsp"/>
<cq:include script="geoSegmentation.jsp"/>

<footer>
	<cq:include path="staticFooter" resourceType="swx/component-library/components/content/single-ipar"/> 	
</footer>

<c:if test="${not isEditMode}">
	<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
	
	<%-- Extole master library --%>
	<script src ="//tags.extole.com/22556/core.js"></script>
	<script type="extole/widget">
		{"zone": "homepage","reference_element":"#extole-placeholder-global_footer"}
	</script>
</c:if>

<script type="text/javascript">
    <c:if test="${not (isEditMode || isReadOnlyMode)}">  
        menuScrollBar.init();
    </c:if>
</script>