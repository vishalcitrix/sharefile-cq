<%--
    SMS Form Component - Sends message defined by authors to users phone number.

    prabhu.shankar@citrix.com
    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<c:set var="phoneNumber" value="${properties.phoneNumber}"/>
<c:set var="submitLink" value="${properties.submitLink}"/>
<c:set var="messageText" value="${properties.messageText}"/>
<c:set var="returnMessageDelivered" value="${properties.returnMessageDelivered}"/>
<c:set var="returnMessageFail" value="${properties.returnMessageFail}"/>
<c:set var="errorMessageMaxAttempt" value="${properties.errorMessageMaxAttempt}"/>

<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/utils/intlTelInput.js"></script>
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/utils/smsForm.js"></script>

<div class="registerFormContainer hide-for-small-only">
	<div class="smsForm">
	  <form class="${properties.shade}" name="smsForm" onSubmit="sendform(this); return false;">
	    <input type="hidden" name="MessageText" value="${messageText}"/>
	        <ul>
	            <li>
	            	<div class="hide error icon-Warning"></div>
	                <label for="phone"><input id="mobNumber" name="mobNumber" type="tel" class="phone required" title="Phone Number" placeholder="${phoneNumber}" /></label>
	            </li>
	        </ul>
	        <input class="button sendSMS" name="submit" id="submit" type="submit" value="${submitLink}" />
	    </form>
	</div>
</div>

<script>
	$(".smsForm #mobNumber").intlTelInput({
	   autoFormat: false
	});

	function sendform(theForm) {
		sendSms(theForm,'${returnMessageDelivered}','${returnMessageFail}','${errorMessageMaxAttempt}');
	}
</script>