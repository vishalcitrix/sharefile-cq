<%--
    Floating Footer component.
    - Contains two buttons. Position fixed to bottom, but will not be displayed in footer try/buy section.
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>

<div class="show-for-medium-up">
	<div class="floatingFooterContent <c:if test="${properties.hasMainOnly}">mainMessageOnly</c:if>">
		<c:if test="${not empty properties.mainMessage}">
		    <div class="message main <c:if test="${properties.hasMainOnly}">only</c:if>">
		    	<c:choose>
				   	<c:when test="${not empty properties.mainMsgPath}">
				   		<a href="${properties.mainMsgPath}" rel="${properties.mainMsgLinkOption}">
					    	<c:if test="${not empty properties.mainMsgCss && properties.mainMsgCss ne 'none'}"><span class="${properties.mainMsgCss}"></span></c:if>
					        ${properties.mainMessage}
					    </a>
					</c:when>
					<c:otherwise>
				    	<c:if test="${not empty properties.mainMsgCss && properties.mainMsgCss ne 'none'}"><span class="${properties.mainMsgCss}"></span></c:if>
				        ${properties.mainMessage}
					</c:otherwise>
			    </c:choose>
		    </div>
		</c:if>
		<c:if test="${!properties.hasMainOnly}">
			<swx:setWCMMode mode="READ_ONLY">
				<cq:include path="button" resourceType="/apps/sf/components/content/button"/>
			</swx:setWCMMode>
			<c:if test="${not empty properties.supportMessage}">
			    <div class="message support">
			    	<c:choose>
					   	<c:when test="${not empty properties.supportMsgPath}">
					   		<a href="${properties.supportMsgPath}" rel="${properties.supportMsgLinkOption}">
						    	<c:if test="${not empty properties.supportMsgCss && properties.supportMsgCss ne 'none'}"><span class="${properties.supportMsgCss}"></span></c:if>
						        ${properties.supportMessage}
						    </a>
						</c:when>
						<c:otherwise>
					    	<c:if test="${not empty properties.supportMsgCss && properties.supportMsgCss ne 'none'}"><span class="${properties.supportMsgCss}"></span></c:if>
					        ${properties.supportMessage}
						</c:otherwise>
				    </c:choose>
			    </div>
		    </c:if>
		</c:if>
    </div>
</div>
<div class="show-for-small-only">
	<c:choose>
		<c:when test="${!properties.hasMainOnly}">
			<a href="${properties.mainMsgPath}" rel="${properties.mainMsgLinkOption}" class="floatingFooterContent">
				<div class="message main <c:if test="${properties.isTwoLinePhone}">two-line</c:if>">
					<c:if test="${not empty properties.mainMsgCss && properties.mainMsgCss ne 'none'}"><span class="${properties.mainMsgCss}"></span></c:if>
			        <c:if test="${not empty properties.mainMessage}">
			        	${properties.mainMessage}
			        </c:if>
				</div>
				<div class="arrow"><span></span></div>
			</a>
		</c:when>
		<c:otherwise>
			<div class="floatingFooterContent mainMessageOnly">
				<div class="message main <c:if test="${properties.isTwoLinePhone}">two-line</c:if>">
					<c:if test="${not empty properties.mainMsgCss && properties.mainMsgCss ne 'none'}"><span class="${properties.mainMsgCss}"></span></c:if>
			        <c:if test="${not empty properties.mainMessage}">
			        	${properties.mainMessage}
			        </c:if>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>
 <div class="clearBoth"></div>