<%--
  SF Support Request Form component
  
  kaverappa.subramanya@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<c:set var="categoryLabel1" value="${properties['categoryLabel']}"/>
<c:set var="needWebAppDropdown" value="${properties['webappdropdown']}"/>
<c:set var="needUploadDropdown" value="${properties['uploaddropdown']}"/>
<c:set var="needSyncDropdown" value="${properties['syncdropdown']}"/>
<c:set var="needPluginDropdown" value="${properties['plugindropdown']}"/>
<c:set var="needWidgetDropdown" value="${properties['dwidgetdropdown']}"/>
  
<form method="post" action="#"  id="supportRequestform" name="form" class="custom-form">

    <div class="tooltip" id="tooltip">Please correct the fields below highlighed in red</div>

    <h2 class="formHeading"> ${not empty properties['formHeadingLabel'] ? properties['formHeadingLabel'] : 'How can we help you?'} </h2>
    <h2 class="formSubHeading"> ${not empty properties['formSubHeadingLabel'] ? properties['formSubHeadingLabel'] : 'Complete this form to help us understand your support needs'} </h2>

    <div class="errorMessage" id="errorMessage"> 
        <p> Please review the errors below </p>
    </div>
    
    <div class="formField">
        <label for="name"> ${not empty properties['namelabel'] ? properties['namelabel'] : 'What should we call you?'}</label>	
        <div class="nameErrorMessage" id="nameErrorMessage"> Please Enter a name</div>
        <input name="" type="text" id="name" class="name" placeholder="${not empty properties['nameplaceholder'] ? properties['nameplaceholder'] : 'Name'}">
    </div>
    
    <div class="formField">
        <label for="contact">${not empty properties['contactlabel'] ? properties['contactlabel'] : 'How should we contact you?'}</label> 
        <div class="contactErrorMessage" id="contactErrorMessage"> <p>Please Enter a email or phone</p></div>
        <input name="" type="text" id="email" class="contact" placeholder ="${not empty properties['contactplaceholder'] ? properties['contactplaceholder'] : 'Phone or email'}">
    </div>
    
    
    <div class="formField">
         <label for="trouble">${not empty properties['troubleplaceholder'] ? properties['troubleplaceholder'] : 'What are you having trouble with?'}</label>
         <div class="radioErrorMessage" id="radioErrorMessage"> <p>Selection required</p></div>
         
         <div class="radioWrapper">
        <p><input type="radio" class="radioButton" value="web Applciaiton" id="webapp" data-code="${needWebAppDropdown}"/><label class="radioLabel"> &nbsp;Web Applciaiton</label></p>
        <div id='webapp2' class="check">
            <p>${not empty properties['webheading'] ? properties['webheading'] :'&nbsp;'}</p>
             <cq:include script="webLinklist.jsp"/>
        </div>
            
        <p><input type="radio" class="radioButton" value="uplaods" id="uploads" data-code="${needUploadDropdown}" /><label class="radioLabel">&nbsp;Uploads/downloads</label></p>
        <div id='uploads2' style='display:none'  class="check">
            <p>${not empty properties['uploadheading'] ? properties['uploadheading'] :'&nbsp;'}</p>
            <cq:include script="uploadLinklist.jsp"/>
        </div>
           
        <p><input type="radio" class="radioButton" value="sync" id="sync" data-code="${needSyncDropdown}"/><label class="radioLabel">&nbsp;Sync Tool</label></p>
        <div id='sync2' style='display:none' class="check">
             <p>${not empty properties['syncheading'] ? properties['syncheading'] :'&nbsp;'}</p>
             <cq:include script="syncLinklist.jsp"/>
        </div>
       
        <p><input type="radio" class="radioButton" value="plugin" id="plugin" data-code="${needPluginDropdown}"/><label class="radioLabel">&nbsp;Plugin for Microsoft Outlook</label></p>
        <div id='plugin2' style='display:none' class="check">
            <p>${not empty properties['pluginheading'] ? properties['pluginheading'] :'&nbsp;'}</p>
            <cq:include script="pluginLinklist.jsp"/>
        </div>
        
        <p><input type="radio" class="radioButton" value="widget" id="widget" data-code="${needWidgetDropdown}"/><label class="radioLabel">&nbsp;Desktop widget</label></p>
        <div id='widget2' class="check">
            <p>${not empty properties['dwidgetheading'] ? properties['dwidgetheading'] :'&nbsp;'}</p>
            <cq:include script="dwidgetLinklist.jsp"/>
        </div>
        
        <p><input type="radio" class="radioButton" value="other" /><label class="radioLabel">&nbsp;Other</label></p>
        </div>
    </div>
      
    <div class="formField">
        <label for="description">${not empty properties['questionplaceholder'] ? properties['questionplaceholder'] : 'Additional description (optional) '}</label>
        <textarea id="description" class="textarea" ></textarea>
    </div>
    
     <div class="formField">
        <label for="upload" class="fileLabel">${not empty properties['uploadlabel'] ? properties['uploadlabel'] : 'Upload a screen shot (optional) '}</label>
        <div class="fileErrorMessage" id="fileErrorMessage"> That file format wont work. Please try a jpg, gif or png</div>
        <div class="preupload" id="preupload">
           <p class="nofile"> No file selected </p>
           <p class ="formats"> (jpg,gif and png)</p> 
        </div>
        <div class="postUpload" id="postupload">
           <p class="filepath" id="filepath"></p>
        </div>
        <input type="file" name="upload" id="upload" class="upload"/>
        <div class="fileButtonWrapper">
            <button type="button" class="fileButton" id="fileButton">Choose File</button>
        </div>
    </div>
    
    <div class="formField">
        <label>&nbsp;</label>
        <div class="buttonWrapper">
            <input type="submit" name="button" value="${not empty properties['buttontext'] ? properties['buttontext'] : 'Contact Me'}" class="supportButton"></input>
        </div>
    </div>   
</form>

<div class="clearBoth"></div>

   
   