<%--
    Category link selector componenet
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>

<%!
    private static final String CATEGORY_DATA_NODE = "categoryLinks";
    private static final int DEFAULT_MAX_ITEMS = 5;
 %>
<%
    final ArrayList<Map<String, Property>> categoryLinks = new ArrayList<Map<String, Property>>();
    final int maxItemFirstRow = properties.get("maxFirstRowItems", DEFAULT_MAX_ITEMS);
    final int maxItemSecondRow = properties.get("maxSecondRowItems", DEFAULT_MAX_ITEMS);
 
    if(currentNode.hasNode(CATEGORY_DATA_NODE)) {
        categoryLinks.addAll(Utilities.parseStructuredMultifield(currentNode.getNode(CATEGORY_DATA_NODE))); 
    }
%>

<c:set var="categoryLinks" value="<%= categoryLinks %>"/>
<c:set var="noOfItems" value="<%= categoryLinks.size() %>"/>
<c:set var="maxItemFirstRow" value="<%= maxItemFirstRow %>"/>
<c:set var="maxItemSecondRow" value="<%= maxItemSecondRow %>"/>

<div id="wrapper" class="row categoryLinkSelector ${not empty properties.isHiddenInMobile and properties.isHiddenInMobile eq 'true' ? 'hide-for-small-only' : ''}">
    <div class="large-12 large-centered columns">
        <ul class="show-for-small-only">
            <li class="filter-data">
                <a href="javascript:void(0);">
                	<span class="drop-down-header-text">
                		${properties.defaultLabel}
                	</span>
                	<span class="drop-down-arrow"></span>
                	<div class="clearBoth"></div>
                </a>
            </li>
        </ul>
        <div class="drop-down-container">
            <div class="drop-down-list text-center ${not empty properties.isHorizontallyAligned and properties.isHorizontallyAligned eq 'true' ? 'horizontalAlign' : ''}">
               <ul class="${noOfItems le maxItemFirstRow ? 'onlyRow' :''}">
                    <c:forEach items="${categoryLinks}" var="categoryLink" begin="0" end="${maxItemFirstRow-1}">
	                    <li>
	                       <c:if test="${not empty categoryLink.linkLabel.string }">
	                         <a href="${categoryLink.linkPath.string}" rel="${categoryLink.linkOption.string}" class="${categoryLink.categoryIcon.string}">
	                         	${categoryLink.linkLabel.string}
	                         </a>
	                         <p class="show-for-large-up copytext"><c:if test="${not empty categoryLink.copytext.string }"> ${categoryLink.copytext.string}</c:if></p>
	                       </c:if>
	                    </li>
                    </c:forEach>
                </ul>
                <c:if test="${noOfItems gt maxItemFirstRow}">
                    <div class="row-seprator"></div>
                    <ul>
                       <c:forEach items="${categoryLinks}" var="categoryLink" begin="${maxItemFirstRow}" end="${maxItemFirstRow+maxItemSecondRow-1}">
                            <li>
                               <c:if test="${not empty categoryLink.linkLabel.string }">
                                 <a href="${categoryLink.linkPath.string}" rel="${categoryLink.linkOption.string}" class="${categoryLink.categoryIcon.string}">
                                 	${categoryLink.linkLabel.string}
                                 </a>
                                 <p class="show-for-large-up copytext"><c:if test="${not empty categoryLink.copytext.string }"> ${categoryLink.copytext.string}</c:if></p>
                               </c:if>
                            </li>
                        </c:forEach>
                    </ul>  
                </c:if>
                <c:if test="${noOfItems gt maxItemFirstRow+maxItemSecondRow}">
                    <div class="row-seprator"></div>
                    <ul>
                       <c:forEach items="${categoryLinks}" var="categoryLink" begin="${maxItemFirstRow+maxItemSecondRow}">
                            <li>
                               <c:if test="${not empty categoryLink.linkLabel.string}">
                                 <a href="${categoryLink.linkPath.string}" rel="${categoryLink.linkOption.string}" class="${categoryLink.categoryIcon.string}">
                                 	${categoryLink.linkLabel.string}
                                 </a>
                                 <p class="show-for-large-up copytext"><c:if test="${not empty categoryLink.copytext.string }"> ${categoryLink.copytext.string}</c:if></p>
                               </c:if>
                            </li>
                        </c:forEach>
                    </ul>  
                </c:if> 
            </div>
        </div>
    </div>
</div>
<div class="clearBoth"></div>