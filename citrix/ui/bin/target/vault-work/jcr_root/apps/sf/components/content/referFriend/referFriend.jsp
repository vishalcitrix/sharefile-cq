<%--
  Refer Friend
  -	This uses Extole Script to create Refer a Friend lightbox link
  
  vishal.gupta@citrix.com 
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:if test="${not empty properties.zone}">
	<c:if test="${not empty properties.additionalText}">
		${properties.additionalText}
	</c:if>
	<script type="extole/widget"> 
		{"zone": "${properties.zone}"}
	</script>
</c:if>