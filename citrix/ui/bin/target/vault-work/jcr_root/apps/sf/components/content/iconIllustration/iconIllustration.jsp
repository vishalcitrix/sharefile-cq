<%--
  Icon Illustration component.
--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<div class="icon-illustration">
	
	
	<c:if test="${properties.illustration eq 'quickEditEdit'}">
		<div class="edit">
				<!--animation-->
				<div class="edit_bg">
					<div class="edit1 animate"></div>
					<div class="edit2 animate"></div>
				</div>
				<!--end-->
			</div>
	</c:if>
	
	<c:if test="${properties.illustration eq 'quickEditAccess'}">
		<div class="access">
				<!--animation-->
				<div class="access_bg">
					 <div class="imgsquare animate"></div>
					<div class="btmlines animate"></div>
				</div>
				<!--end-->
			</div>
	</c:if>
	
	<c:if test="${properties.illustration eq 'quickEditStorage'}">
		<div class="storage">
				<!--animation-->
				<div class="storage_bg">
					<div class="storage1 animate"></div>
					<div class="storage2 animate"></div>
					<div class="storage3 animate"></div>
				</div>
				<!--end-->
			</div>
	</c:if>
	
	<c:if test="${properties.illustration eq 'quickEditWorkflow'}">
		<div class="workflow">
				<!--animation-->
				<div class="workflow_bg">
					<div class="workflow1 animate"></div>
					<div class="workflow2 animate"></div>
					<div class="workflow3 animate"></div>
				</div>
				<!--end-->
		</div>
	</c:if>
	
	<c:if test="${properties.illustration eq 'enterprise-compliance'}">
		<div id="${properties.illustration}">
			<div class="doc1"></div>
			<div class="doc2"></div>
			<div class="doc3"></div>
			<div class="shield"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="enterprise-compliance-layer1"></div>
				<div class="enterprise-compliance-layer2"></div>
				<div class="enterprise-compliance-layer3"></div>
				<div class="enterprise-compliance-layer4"></div>
				<div class="enterprise-compliance-layer5"></div>
				<div class="enterprise-compliance-layer6"></div>
				<div class="enterprise-compliance-layer7"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'enterprise-protectdata'}">
		<div id="${properties.illustration}">
			<div class="folderopen"></div>
			<div class="folderfront"></div>
			<div class="folderclose"></div>
			<div class="doc1"></div>
			<div class="doc2"></div>
			<div class="lock"></div>
			<div class="locktop"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="case-icon-container"></div>
			<div class="medium-container">
				<div class="lock-icon-container"></div>
				<div class="key-hole-container"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'enterprise-storagedata'}">
		<div id="${properties.illustration}">
			<div class="logocloud"></div>
			<div class="building"></div>
			<div class="phone"></div>
			<div class="house"></div>
			<div class="screen"></div>
			<div class="phone2"></div>
			<div class="house2"></div>
			<div class="screen2"></div>
			<div class="dots1"></div>
			<div class="dots2"></div>
			<div class="dots3"></div>
			<div class="dots4"></div>
			<div class="greendot1"></div>
			<div class="greendot2"></div>
			<div class="greendot3"></div>
			<div class="greendot4"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="storageData-layer1"></div>
				<div class="storageData-layer2"></div>
				<div class="storageData-layer3"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'features-bypass'}">
		<div id="${properties.illustration}">
			<div class="screen">
				<div class="screen_contain">
					<div class="mouse"></div>
					<div class="at"></div>
					<div class="envelope"></div>
					<div class="sfenvelope"></div>
				</div>
			</div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-bypass-layer1"></div>
				<div class="feature-bypass-layer2"></div>
				<div class="feature-bypass-layer3"></div>
				<div class="feature-bypass-layer4"></div>
				<div class="feature-bypass-layer5"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'features-esig'}">
		<div id="${properties.illustration}">
			<div class="doc"></div>
			<div class="sig"></div>
			<div class="seal"></div>
			<div class="pen"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-esig-layer1"></div>
				<div class="feature-esig-layer3"></div>
				<div class="feature-esig-layer4"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'features-sync'}">
		<div id="${properties.illustration}">
			<div class="logocloud"></div>
			<div class="dots"></div>
			<div class="screen"></div>
			<div class="phone"></div>
			<div class="laptop"></div>
			<div class="tablet"></div>
			<div class="screen2"></div>
			<div class="phone2"></div>
			<div class="laptop2"></div>
			<div class="tablet2"></div>
			<div class="greendot"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-sync-layer1"></div>
				<div class="feature-sync-layer2"></div>
				<div class="feature-sync-layer3"></div>
				<div class="feature-sync-layer4"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'features-tracksharing'}">
		<div id="${properties.illustration}">
			<div class="docs"></div>
			<div class="dots"></div>
			<div class="people"></div>
			<div class="people2"></div>
			<div class="person"></div>
			<div class="person2"></div>
			<div class="greendot"></div>
			<div class="greendot2"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-tracksharing-layer1"></div>
				<div class="feature-tracksharing-layer2"></div>
				<div class="feature-tracksharing-layer3"></div>
				<div class="feature-tracksharing-layer4"></div>
				<div class="feature-tracksharing-layer5"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'vdr-audit'}">
		<div id="${properties.illustration}">
			<div class="outertable">
				<div class="table">
					<div class="thead"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
					<div class="yrow"></div>
					<div class="grow"></div>
					<div class="wrow"></div>
					<div class="grow"></div>
				</div>
			</div>
			<div class="check"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-vdr-audit-layer1"></div>
				<div class="feature-vdr-audit-layer2"></div>
				<div class="feature-vdr-audit-layer3"></div>
				<div class="feature-vdr-audit-layer4"></div>
				<div class="feature-vdr-audit-layer5"></div>
				<div class="feature-vdr-audit-layer6"></div>
			</div>
		</div>
	</c:if>

	<c:if test="${properties.illustration eq 'vdr-sync'}">
		<div id="${properties.illustration}">
			<div class="logocloud"></div>
			<div class="dots"></div>
			<div class="screen"></div>
			<div class="phone"></div>
			<div class="laptop"></div>
			<div class="tablet"></div>
			<div class="screen2"></div>
			<div class="phone2"></div>
			<div class="laptop2"></div>
			<div class="tablet2"></div>
			<div class="greendot"></div>
		</div>
		<div id="${properties.illustration}-static">
			<div class="medium-container">
				<div class="feature-vdr-sync-layer1"></div>
				<div class="feature-vdr-sync-layer2"></div>
				<div class="feature-vdr-sync-layer3"></div>
				<div class="feature-vdr-sync-layer4"></div>
				<div class="feature-vdr-sync-layer5"></div>
				<div class="feature-vdr-sync-layer6"></div>
			</div>
		</div>
	</c:if>

</div>