<%--
	Floating Footer component.
	- Contains two buttons. Position fixed to bottom, but will not be displayed in footer try/buy section.
	vishal.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>

<div class="floatingFooterContent">
	<ul>
		<li>            
			<swx:setWCMMode mode="READ_ONLY">
				<cq:include path="buttonLeft" resourceType="/apps/g2m-redesign/components/content/button"/>
			</swx:setWCMMode>
		</li>
		<li>
			<swx:setWCMMode mode="READ_ONLY">
				<cq:include path="buttonRight" resourceType="/apps/g2m-redesign/components/content/button"/>
			</swx:setWCMMode>
		</li>
	</ul>
</div>
 <div class="clearBoth"></div>