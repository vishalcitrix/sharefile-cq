<%--
  Video Spotlight component.
  
  This is a Video Spotlight component developed to display images and configured  gradient color on hover .Here image contains reference for bright cove video . in order to place video link with image and corresponding gradient a valid video container id   should be configured in component dialog.

  Component displays only image gradient for small and medium view port.

  This component depends on SWX image rendition component. 

--%>

<%
%><%@page import="com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%@include file="/apps/citrixosd/global.jsp" %><%
%>
<%
boolean hasImageRef =currentNode != null ? currentNode.hasNode("imageRenditions") ? currentNode.getNode("imageRenditions").hasProperty("fileReference") : false : false;
pageContext.setAttribute("hasImageRef", hasImageRef );
%>
<div style="float:auto">
    <a href="${properties.videocontainerId}" rel="lightbox">
        <swx:setWCMMode mode="READ_ONLY">
            <div class="videos-spotlight-container ${(isEditMode || isReadOnlyMode) ? 'shrinkImage' : ''}" data-enablejs="${not (isEditMode || isReadOnlyMode)}">
                <cq:include path="imageRenditions" resourceType="g2m-redesign/components/content/imageRenditions"/>
                <cq:include script="gradientholder.jsp" />
            </div>   
        </swx:setWCMMode>
    </a>
</div>

  <%--Display placeholder --%>
  
        <c:if test="${(isEditMode || isReadOnlyMode) && (not hasImageRef)  }">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">          
            </div>
        </c:if>
            
<div class="clearBoth"></div>  
 