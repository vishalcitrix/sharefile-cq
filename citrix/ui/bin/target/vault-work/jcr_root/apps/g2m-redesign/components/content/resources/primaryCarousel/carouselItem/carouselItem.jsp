<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String ICON = "icon";
    public static final String ALTLINKPATH = "linkPath";
    public static final String ALTLINKLABEL = "linkLabel";  
    public static final String SLIDELINK = "slideLink";
    public static final String LINKBEHAVIOR = "linkBehavior";
%>

<c:set var="icon" value="<%= properties.get(ICON, "none") %>"/>
<c:set var="linkPath" value="<%= properties.get(ALTLINKPATH, "") %>"/>
<c:set var="linkLabel" value="<%= properties.get(ALTLINKLABEL, "") %>"/>
<c:set var="slideLink" value="<%= properties.get(SLIDELINK, "") %>"/>
<c:set var="linkBehavior" value="<%= properties.get(LINKBEHAVIOR, "") %>"/>

<c:if test="${not empty slideLink && not isEditMode}">
    <c:choose>
        <c:when test="${not empty linkBehavior || linkBehavior != 'none'}">
            <a rel="${linkBehavior}"  href="${slideLink}">
        </c:when>
        <c:otherwise>
            <a href="${slideLink}">
        </c:otherwise>
    </c:choose>
</c:if>
    <cq:include path="image" resourceType="/apps/g2m-redesign/components/content/resources/primaryCarousel/imageRenditions" />
    <div class="owl-item-info">
        <cq:include path="rte" resourceType="/apps/g2m-redesign/components/content/resources/resourceText" /> 
        <c:if test="${icon != '' || icon != 'none'}">
            <span class="icon ${icon}"></span>
        </c:if>
    </div>   
<c:if test="${not empty slideLink && not isEditMode}">
    </a>
</c:if>
<c:if test="${linkLabel != ''}">
    <div class="link-sub-container">
        <a href="${linkPath}">${linkLabel}<span class="arrow-pointer"></span></a>
    </div>
</c:if>