<%@ page import="com.citrixosd.service.rssfeed.RssFeedReader" %>
<%@ page import="org.jsoup.Jsoup" %>
<%@ page import="org.jsoup.nodes.Document" %>
<%@ page import="org.jsoup.select.Elements" %>
<%@ page import="com.citrixosd.models.RssFeedItem" %>
<%@ page import="com.citrixosd.models.RssFeedChannel" %>
<%@ page import="com.citrixosd.models.RssFeedItemCollection" %>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<%! public String removeImage(String dec){
        StringBuffer sb = new StringBuffer(dec);
        String str = "";
        int start = sb.indexOf("<img");
        int end = sb.indexOf("/>", start);
        int end2 = sb.indexOf("</img>", start);
        int end3 = sb.indexOf(">", start);

        if (start >= 0 && end >= 0) {
            str = sb.replace(start, end + 2, "").toString();
        } else if (start >= 0 && end2 >= 0) {
            str = sb.replace(start, end2 + 6, "").toString();
        } else if (start >= 0 && end3 >= 0) {
            str = sb.replace(start, end3 + 1, "").toString();
        } else {
            str = dec;
        }
        return str; 
    } 
%>
<%
    final int numberOfFeeds = 4;
    final String path = properties.get("path", null);
    final RssFeedReader rssFeedReader=  sling.getService(RssFeedReader.class);


    RssFeedChannel channel = null;
    if(path != null) {
        try {
            channel = rssFeedReader.getChannel(path);
            if(channel != null) {
                final RssFeedItemCollection items = channel.getItems();
                for(int i = 0; i < items.size(); i++) {
                    final RssFeedItem item = items.get(i);
                    final Document description = Jsoup.parse(item.getContent(), "UTF-8");
                    final Elements images = description.select("img[src]");
                    if(images != null && images.size() > 0) {
                        item.setImage(images.get(0).absUrl("src"));
                        item.setDescription(removeImage(item.getDescription()));//remove image from desreption 
                    }
                }
            }

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
%>
<%!
    public static final String COLOR_SCHEME_PROPERTY = "colorScheme";
%>
<c:set var="colorScheme" value="<%=properties.get(COLOR_SCHEME_PROPERTY, "g2m")%>"/>
<c:set var="channel" value="<%= channel %>"/>
<c:set var="numberOfFeeds" value="<%= numberOfFeeds %>"/>
<c:set var="blogDefaultImage" value="<%= ContextRootTransformUtil.transformedPath("/etc/designs/g2m-redesign/css/static/images/blog_default_image_square.png",request) %>"/>

<c:choose>
    <c:when test="${not empty channel}">
        <div class="secondary-carousel ${colorScheme}">
            <div class="row full-width">
                <div class="columns large-6 medium-12 th-selectors">
                    <c:forEach items="${channel.items}" end="${numberOfFeeds - 1}" var="item" varStatus="i">
                        <div class="${i.first ? 'current': ''} selector ${i.last ? 'last': ''}">
                            <img src="${not empty item.image ? item.image : blogDefaultImage}"/>
                        </div>
                    </c:forEach>
                </div>

                <div class="columns large-6 medium-12 blog-info">
                    <div id="textCarousel" class="text-section">
                        <c:forEach items="${channel.items}" end="${numberOfFeeds - 1}" var="item">
                            <div class="item">
                              <a rel="external" class="bloglink" href="${item.link}">
                                  <h2><fmt:message key="FEATURED BLOG POST"/></h2>
                                  <h3>${item.title}</h3>
                                  <span>${item.description}</span>
                              </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="sub-container">
                <a rel="external" href="${channel.link}"><fmt:message key="Visit the Blog"/><span class="arrow-pointer"></span></a>
            </div>
        </div>
        <div class="secondary-carousel right-rail ${colorScheme}">
            <div class="feed-header">${channel.title}</div>
            <c:forEach items="${channel.items}" end="${numberOfFeeds - 1}" var="item">
            <div class="feed-container">
                <a rel="external" href="${item.link}" class="feed-link">
                    <div class="feed-item">
                        <div class="image-container">
                            <div class="image-feed">
                                <img src="${not empty item.image ? item.image : blogDefaultImage}"/>
                            </div>
                        </div>
                        <div class="item">
                            <h3>${item.title}</h3>
                        </div>
                    </div>
                </a>
                 <!-- AddThis Button BEGIN -->
                 <div class="addthis_toolbox addthis_default_style" addthis:url="${item.link}" addthis:title="${item.title}"> 
                     <a class="addthis_button_preferred_1"></a>
                     <a class="addthis_button_preferred_2"></a>
                     <a class="addthis_button_preferred_3"></a>
                     <a class="addthis_button_preferred_4"></a>
                     <a class="addthis_button_compact"></a>
                     <a class="addthis_counter addthis_bubble_style"></a>
                 </div>
                 <!-- AddThis Button END -->
            </div>
            </c:forEach>
            <div class="sub-container">
                <a rel="external" href="${channel.link}"><fmt:message key="RSS Feed"/></a>
            </div>
        </div>

        <div class="clearBoth"></div>
    </c:when>
    <c:otherwise>
        <c:if test="${(isEditMode || isReadOnlyMode) }">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">
            </div>
        </c:if>
    </c:otherwise>
</c:choose>