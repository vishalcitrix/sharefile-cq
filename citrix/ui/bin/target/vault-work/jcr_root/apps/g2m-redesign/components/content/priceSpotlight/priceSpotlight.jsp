<%--
  Price Spotlight component.
--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<div class="spotlightCircle" id="${properties.theme}">
   <c:if test="${not empty properties.mostPopular && properties.spotlight eq 'true'}">
    <p class="mostPopular">${properties.mostPopular}</p>
    <div class="arrow"></div>
   </c:if>
    <h3 class="theme ${properties.spotlight eq 'true'? 'SpotLight' :'noSpotlight'}">${properties.title}</h3>
    <div class="circle ${properties.spotlight eq 'true'? 'spotlight' :''}">
        <p class="attendees">${properties.priceHeaderFirst}<br>${properties.priceHeaderSecond}</p>

        <div class="pricestyle clearfix">
            <span class="price ${empty properties.priceCurrency ? 'free' :''}"> <sup>${properties.priceCurrency}</sup>${properties.price}</span>
            <c:if test="${not empty properties.pricePostfix}">
                <span class="division">/</span>
                <span class="message">${properties.pricePostfix}</span>
            </c:if>
        </div>

        <p class="annualprice">${properties.priceFooter}</p>

        <c:if test="${not empty properties.buttonLabel}">
            <a class="trialfree" href="${properties.buttonURL}">${properties.buttonLabel}</a>
        </c:if>
        
        <c:choose>
		    <c:when test="${not empty properties.linkURL}">
		        <a class="buynow" href="${properties.linkURL}">
                    ${properties.link}
                    <c:if test="${not empty properties.linkicon}">
                        <span class="${properties.linkicon}"></span>
                    </c:if>
                </a>
		    </c:when>
		    <c:otherwise>
		        <div class="offer">${properties.link}</div>
		    </c:otherwise>
		</c:choose>
    </div>
</div>
<div class="clearBoth"></div>