<%--
   Small Box Container   
    
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="right-rail smallbox-container  ${properties.backGroundcolorTheme}">
     <div class="feed-header"> 
        <span> ${properties.headerTitle}</span> 
     </div>
     <div class="container">
        <cq:include path="boxContent" resourceType="swx/component-library/components/content/single-par"/>
     </div>

    <c:if test="${properties.backGroundcolorTheme eq 'twitter-theme'}">
          <div class="sub-container">
            <a  target="_blank" href="${properties.footerLink}">${properties.footerTitle}</a>
         </div>
    </c:if>
</div>
