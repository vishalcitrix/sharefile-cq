<%--
     Triple Stack component.

    
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%!
    public static final String ICON_PROPERTY = "icon";
    public static final String FEATURE_TYPE_PROPERTY = "featureType";
    public static final String CONTENT_TITLE_PROPERTY = "contentTitle";
    public static final String PATH_PROPERTY = "path";
    public static final String ACTION_LINK_PROPERTY = "actionLink";
    public static final String ACTION_LINK_PATH_PROPERTY = "actionPath";   
%>

<%
   String currentMode = WCMMode.fromRequest(slingRequest).toString();
   pageContext.setAttribute("pageMode", currentMode);
%>

<!-- Stack -->
<c:set var="icon" value="<%= properties.get(ICON_PROPERTY, "white-paper") %>"/>
<c:set var="featureType" value="<%= properties.get(FEATURE_TYPE_PROPERTY, "") %>"/>
<c:set var="contentTitle" value="<%= properties.get(CONTENT_TITLE_PROPERTY, "") %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, "") %>"/>
<c:set var="actionLink" value="<%= properties.get(ACTION_LINK_PROPERTY, "") %>"/>
<c:set var="actionPath" value="<%= properties.get(ACTION_LINK_PATH_PROPERTY, "") %>"/>


<c:choose>
    <c:when test="${empty featureType && empty contentTitle}">
	    <c:if test="${pageMode eq 'EDIT'}">
		    <style>
		    .stacker {
		        height: 183px !important;
		    }
		    </style>
	        <div style="height: 70px;">
	            <p>Please add a Feature Type or Content Title to this component</p>
	        </div>
	    </c:if>
    </c:when>
    <c:otherwise>
        <span class="icon ${icon}"></span>
        <h4>${featureType}</h4>
        <h3 class="content-title"><a href="${path}">${contentTitle}</a></h3>
        <div class="sub-container">
            <a href="${actionPath}">${actionLink}<span class="arrow-pointer"></span></a>
        </div>        
    </c:otherwise>
</c:choose>