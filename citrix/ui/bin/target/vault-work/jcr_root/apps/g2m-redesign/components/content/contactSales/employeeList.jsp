<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select name="NoofEmployees__c" id="NumberofEmployees" class="required" title="<fmt:message key="form.validate.required"/>">
    <option value=""><fmt:message key="form.option.employees"/></option>
    <option value="1-10"><fmt:message key="form.option.numberOfEmployees.10"/></option>
    <option value="11-20"><fmt:message key="form.option.numberOfEmployees.20"/></option>
    <option value="21-50"><fmt:message key="form.option.numberOfEmployees.50"/></option>
    <option value="51-100"><fmt:message key="form.option.numberOfEmployees.100"/></option>
    <option value="101-250"><fmt:message key="form.option.numberOfEmployees.250"/></option>
    <option value="251-500"><fmt:message key="form.option.numberOfEmployees.500"/></option>
    <option value="501-1,000"><fmt:message key="form.option.numberOfEmployees.1000"/></option>
    <option value="1,001-2,500"><fmt:message key="form.option.numberOfEmployees.2500"/></option>
    <option value="2,501-5,000"><fmt:message key="form.option.numberOfEmployees.5000"/></option>
    <option value="5,001-7,500"><fmt:message key="form.option.numberOfEmployees.7500"/></option>
    <option value="7,501-10,000"><fmt:message key="form.option.numberOfEmployees.10000"/></option>
    <option value="10,001+"><fmt:message key="form.option.numberOfEmployees.10001"/></option>
</select>