<%-- <%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.citrixosdRedesign.models.resource.*" %>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String PATH_PROPERTY = "path";
%>

<%
    final String path = properties.get(PATH_PROPERTY, String.class);

    Document document = null;
    Video video = null;
    Webinar webinar = null;
    CustomerStory customerStory = null;
    PressAward pressAward = null;

    if(path != null) {
        final Class clazz = ResourceUtils.getResourcePageClass(resource, path);
        if(clazz.equals(Document.class)) {
            document = ResourceUtils.getDocument(ResourceUtils.getResourcePath(resource, path), ResourceUtils.findLandingPage(resource, path), currentDesign);
        }else if(clazz.equals(Video.class)) {
            video = ResourceUtils.getVideo(ResourceUtils.getResourcePath(resource, path), ResourceUtils.findLandingPage(resource, path), currentDesign);
        }else if(clazz.equals(Webinar.class)) {
            webinar = ResourceUtils.getWebinar(ResourceUtils.getResourcePath(resource, path), ResourceUtils.findLandingPage(resource, path), currentDesign);
        }else if(clazz.equals(CustomerStory.class)) {
            customerStory = ResourceUtils.getCustomerStory(ResourceUtils.getResourcePath(resource, path), ResourceUtils.findLandingPage(resource, path), currentDesign);
        }else if(clazz.equals(PressAward.class)) {
            pressAward = ResourceUtils.getPressAward(ResourceUtils.getResourcePath(resource, path), ResourceUtils.findLandingPage(resource, path), currentDesign);
        }else {
            //Null
        }
    }

%>

<c:set var="document" value="<%= document %>"/>
<c:set var="video" value="<%= video %>"/>
<c:set var="webinar" value="<%= webinar %>"/>
<c:set var="customerStory" value="<%= customerStory %>"/>
<c:set var="pressAward" value="<%= pressAward %>"/>

<c:choose>
    <c:when test="${not empty properties.path}">
        <div class="press-awards">
            <div class="press-header">
                <h2>${properties.title}</h2>
            </div>
            <div class="main-press">
                <div class="press-image">
                    <c:choose>
                        <c:when test="${not empty document}"><img src="${document.imagePath}"/></c:when>
                        <c:when test="${not empty video}"><img src="${video.imagePath}"/></c:when>
                        <c:when test="${not empty webinar}"><img src="${webinar.imagePath}"/></c:when>
                        <c:when test="${not empty customerStory}"><img src="${customerStory.imagePath}"/></c:when>
                        <c:when test="${not empty pressAward}"><img src="${pressAward.imagePath}"/></c:when>
                        <c:otherwise></c:otherwise>
                    </c:choose>
                </div>
                <div class="press-info">
                    <p class="date">
                        <c:choose>
                            <c:when test="${not empty document}"><fmt:formatDate value="${document.date}" type="DATE" pattern="MMMM, yyyy"/></c:when>
                            <c:when test="${not empty webinar}"><fmt:formatDate value="${webinar.date}" type="DATE" pattern="MMMM, yyyy"/></c:when>
                            <c:when test="${not empty pressAward}"><fmt:formatDate value="${pressAward.date}" type="DATE" pattern="MMMM, yyyy"/></c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                    </p>
                    <a class="title-link" href="
                        <c:choose>
                            <c:when test="${not empty document}">${document.path}</c:when>
                            <c:when test="${not empty video}">${video.path}</c:when>
                            <c:when test="${not empty webinar}">${webinar.path}</c:when>
                            <c:when test="${not empty customerStory}">${customerStory.path}</c:when>
                            <c:when test="${not empty pressAward}">${pressAward.path}</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                    ">
                        <p class="press-title">
                            <c:choose>
                                <c:when test="${not empty document}">${document.title}</c:when>
                                <c:when test="${not empty video}">${video.title}</c:when>
                                <c:when test="${not empty webinar}">${webinar.title}</c:when>
                                <c:when test="${not empty customerStory}">${customerStory.title}</c:when>
                                <c:when test="${not empty pressAward}">${pressAward.title}</c:when>
                                <c:otherwise></c:otherwise>
                            </c:choose>
                        </p>
                    </a>
                    <p>
                        <c:choose>
                            <c:when test="${not empty document}">${document.description}</c:when>
                            <c:when test="${not empty video}">${video.description}</c:when>
                            <c:when test="${not empty webinar}">${webinar.description}</c:when>
                            <c:when test="${not empty customerStory}">${customerStory.description}</c:when>
                            <c:when test="${not empty pressAward}">${pressAward.description}</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                    </p>

                    <c:if test="${not empty properties.ctaText}">
                        <a href="${properties.ctaPath}">${properties.ctaText}<span class="arrow-pointer"></span></a>
                    </c:if>
                </div>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style"
                     addthis:url="
                        <c:choose>
                            <c:when test="${not empty document}">${document.path}</c:when>
                            <c:when test="${not empty video}">${video.path}</c:when>
                            <c:when test="${not empty webinar}">${webinar.path}</c:when>
                            <c:when test="${not empty customerStory}">${customerStory.path}</c:when>
                            <c:when test="${not empty pressAward}">${pressAward.path}</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                     "
                     addthis:title="
                        <c:choose>
                            <c:when test="${not empty document}">${document.title}</c:when>
                            <c:when test="${not empty video}">${video.title}</c:when>
                            <c:when test="${not empty webinar}">${webinar.title}</c:when>
                            <c:when test="${not empty customerStory}">${customerStory.title}</c:when>
                            <c:when test="${not empty pressAward}">${pressAward.title}</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                     "
                     addthis:description="
                        <c:choose>
                            <c:when test="${not empty document}">${document.description}</c:when>
                            <c:when test="${not empty video}">${video.description}</c:when>
                            <c:when test="${not empty webinar}">${webinar.description}</c:when>
                            <c:when test="${not empty customerStory}">${customerStory.description}</c:when>
                            <c:when test="${not empty pressAward}">${pressAward.description}</c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                     ">
                    <a class="addthis_button_preferred_1"></a>
                    <a class="addthis_button_preferred_2"></a>
                    <a class="addthis_button_preferred_3"></a>
                    <a class="addthis_button_preferred_4"></a>
                    <a class="addthis_button_compact"></a>
                    <a class="addthis_counter addthis_bubble_style"></a>
                </div>
                <!-- AddThis Button END -->
            </div>
        </div>
        <div class="clearBoth"></div>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">
            </div>
        </c:if>
    </c:otherwise>
</c:choose> --%>