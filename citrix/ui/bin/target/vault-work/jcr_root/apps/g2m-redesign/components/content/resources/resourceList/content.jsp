<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@ page import="com.citrixosdRedesign.constants.ResourceConstants"%>

<c:set var="mimeTypePDF" value="<%= ResourceConstants.Document.PDF_MIMETYPE %>"/>
<c:set var="mimeTypePPT" value="<%= ResourceConstants.Document.PPT_MIMETYPE %>"/>
<c:set var="designPath" value="<%= ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request) %>"/>
<c:set var="baseURL" value="<%= request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath()) %>"/>

<section class="resourcesFeature-container">
    <ul id="resourcesFeatureList" class="resourcesFeature-list">
        <c:choose>
            <c:when test="${fn:length(resourceList.list) > 0}">
                <c:forEach items="${resourceList.list}" var="resourceItem">
                    <li>
                        <div class="resourcesFeature">
                            <aside class="featured-image">
                                <a href="${((resourceItem.resourceType eq 'Awards') || (resourceItem.resourceType eq 'Webinar' && resourceItem.isGated) || (resourceItem.isGated)) && (not isEditMode) ? (resourceItem.resourceType eq 'Webinar' ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : (not empty resourceItem.externalLink ? resourceItem.externalLink : '#')) : resourceItem.path}">
                                    <div style="background-image:url('${resourceItem.hasImage ? resourceItem.smallImagePath : resourceItem.hasVideo && not empty resourceItem.thumbnailPath ? resourceItem.thumbnailPath : resourceItem.smallImagePath}'); background-position: center center; background-size: cover; height: 200px;"></div>
                                    <c:if test="${resourceItem.hasVideo}">
                                        <div class="asset-icon"></div>
                                    </c:if>
                                </a>
                            </aside>
                            <section class="content-description">
                                <div class="content-wrapper">
                                    <h2>
                                        ${resourceItem.resourceType}
                                        <c:if test="${resourceItem.resourceType eq 'Webinar' && not empty resourceItem.date}">
                                            <span class="resource-date"><fmt:formatDate value="${resourceItem.date}" type="DATE" pattern="dd MMMM, yyyy"/></span>
                                        </c:if>
                                    </h2>
                                    <a href="${((resourceItem.resourceType eq 'Awards') || (resourceItem.resourceType eq 'Webinar' && resourceItem.isGated) || (resourceItem.isGated)) && (not isEditMode) ? (resourceItem.resourceType eq 'Webinar' ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : (not empty resourceItem.externalLink ? resourceItem.externalLink : '#')) : resourceItem.path}">
                                        <h3>${resourceItem.title}</h3>
                                    </a>
                                    <p class="resources-feature-description">${resourceItem.description}</p>
                                </div>

                                <%-- PDF Type --%>
                                <c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePDF}">
                                    <div class="doc-icon"></div>
                                </c:if>

                                <%-- PPT Type --%>
                                <c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePPT}">
                                    <div class="ppt-icon"></div>
                                </c:if>

                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style" addthis:url="${baseURL}${resourceItem.path}" addthis:title="${resourceItem.title}" addthis:description="${resourceItem.description}">
                                    <a class="addthis_button_preferred_1"></a>
                                    <a class="addthis_button_preferred_2"></a>
                                    <a class="addthis_button_preferred_3"></a>
                                    <a class="addthis_button_preferred_4"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <!-- AddThis Button END -->
                            </section>
                        </div>
                    </li>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <li>
                    <div class="resource-list-no-results">
                        <span><fmt:message key="resource.list.no.results"/></span>
                    </div>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>

    <c:if test="${resourceList.totalPages > 1}">
        <div class="pagination">
            <c:if test="${resourceList.currentPage > 0}">
                <a class="previous active" href="${resource.path}.pagination.${(resourceList.currentPage - 1) > 0 ? resourceList.currentPage - 1 : 0}${resourceList.suffixPathWithQuery}">
                    <span></span> <fmt:message key="Previous"/>
                </a>
            </c:if>
            <div class="pagination-wrapper">
                <ul>
                    <li class="${resourceList.currentPage == 0 ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.0${resourceList.suffixPathWithQuery}">1</a></li>
                    <c:choose>
                        <c:when test="${resourceList.totalPages > 5}">
                            <c:if test="${resourceList.currentPage > 2}">
                                <li class="margin-right"> ... </li>
                            </c:if>
                            <c:forEach begin="${(resourceList.currentPage - 1) > 1 ? ((resourceList.currentPage + 4) > resourceList.totalPages ? resourceList.totalPages - 4 : resourceList.currentPage - 1) : 1}" end="${(resourceList.currentPage) > 2 ? (resourceList.currentPage + 2 >= resourceList.totalPages ? resourceList.totalPages - 2 : resourceList.currentPage + 1) : resourceList.currentPage + (3 - resourceList.currentPage)}" varStatus="i">
                                <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}">${i.index + 1}</a></li>
                            </c:forEach>
                            <c:if test="${(resourceList.totalPages - 3) > resourceList.currentPage}">
                                <li class="last-three"> ... </li>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:forEach begin="1" end="${resourceList.totalPages - 2}" varStatus="i">
                                <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}">${i.index + 1}</a></li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <li class="last-three ${resourceList.currentPage == (resourceList.totalPages - 1) ? 'active' : ''}"><a href="${resource.path}.pagination.${resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}">${resourceList.totalPages}</a></li>
                </ul>
            </div>
            <c:if test="${(resourceList.currentPage + 1) < resourceList.totalPages}">
                <a class="next" href="${resource.path}.pagination.${(resourceList.currentPage + 1) < resourceList.totalPages ? resourceList.currentPage + 1 : resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}">
                    <fmt:message key="Next"/> <span></span>
                </a>
            </c:if>
        </div>
        <c:forEach begin="1" end="${resourceList.totalPages - 1}" varStatus="i">
            <a class="load-more mobile ${i.index == 1 ? '': 'hide'}" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}" data-pagination-index="${i.index}">
                <fmt:message key="Load more ..."/>
            </a>
        </c:forEach>
    </c:if>
</section>

<script>
    $(document).ready(function() {
        var resourceListContainer = $("#" + "${resourceList.id}");
        var resourceListPagination = resourceListContainer.find(".pagination");
        resourceListPagination.find("a").each(function() {
            var paginationAnchor = $(this);
            paginationAnchor.unbind("click").click(function() {
                resourceListPagination.find('li').removeClass('active');
                resourceListContainer.parent().load(paginationAnchor.attr("href"), function(){
                    dotDotDot.init();
                    if (window.addthis) {
                        window.addthis = null;
                        window._adr = null;
                        window._atc = null;
                        window._atd = null;
                        window._ate = null;
                        window._atr = null;
                        window._atw = null;
                    }
                    $.getScript(addthis_script);
                });
                $("html, body").animate({ scrollTop: 150});
                return false;
            });
        });
        resourceListContainer.find("a.load-more").each(function() {
            var loadMoreAnchor = $(this)
            loadMoreAnchor.unbind("click").click(function() {
                $("a.load-more").html("<img id='resourcesFeatureAnimation' src='${designPath}/css/static/images/loading.gif' />");
                $.get(loadMoreAnchor.attr("href"), function(data){
                    loadMoreAnchor.addClass("hide");
                    loadMoreAnchor.next("a.load-more").removeClass("hide");
                    $("a.load-more").html('<fmt:message key="load.more"/>');
                    $(data).find("#resourcesFeatureList li").appendTo("#resourcesFeatureList");
                    dotDotDot.init();
                    if (window.addthis) {
                        window.addthis = null;
                        window._adr = null;
                        window._atc = null;
                        window._atd = null;
                        window._ate = null;
                        window._atr = null;
                        window._atw = null;
                    }
                    $.getScript(addthis_script);
                });
                return false;
            });
        });
    });
</script>