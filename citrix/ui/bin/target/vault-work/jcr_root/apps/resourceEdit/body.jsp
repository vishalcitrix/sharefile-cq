<%--
	vishal.gupta@citrix.com
--%>

<%@page contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"
        import="javax.jcr.Node,
                java.util.Iterator,
                org.apache.commons.lang.StringEscapeUtils,
                org.apache.sling.api.resource.Resource"%><%
%>
<%@include file="/libs/foundation/global.jsp"%>

<%
    String id = currentPage.getName();
    String title = StringEscapeUtils.escapeHtml(properties.get("jcr:title", id)); 
    String description = StringEscapeUtils.escapeHtml(properties.get("jcr:description", ""));
    

    String path = resource.getPath();
    String resourceType = resource.getResourceType();
    //String dialogPath = (!resourceType.startsWith("/libs") ? "/libs/" : "") + resourceType + "/dialog";  
    String dialogPath = resourceType + "/dialog"; 
    
%>
<link type="text/css" rel="stylesheet" href="<%= resourceType %>/css/style.css">
<script type="text/javascript" src= "<%= resourceType %>/js/jslib.js"></script>
<body style="background-color:#fff;">
	
	<h1><%= title %></h1>
    <div><%= description %></div>
    <br>
    <div>
    	<script type="text/javascript">
	        CQ.WCM.edit({
	            "path":"<%= path %>",
	            "dialog":"<%= dialogPath %>",
	            "type":"<%= resourceType %>",
	            "editConfig":{
	                "xtype":"editbar",
	                "listeners":{
	                    "afteredit":"function(){jQuery.post('/bin/resourceconsole', {'method':'add'},function(data){console.log(data);alert(data); location.reload();});}"
	                },
	                "actions":[
	                    CQ.I18n.getMessage("Add Resource Page"),
	                    {
	                        "xtype": "tbseparator"
	                    },
	                    
	                    {
	                    	"jcr:primaryType":"nt:unstructured",
	                    	"handler":"function() {CQ.wcm.EditBase.showDialog(this);}",
	                    	"text":"Add"            	
	                    }
	                    
	                ]
	            }
	        });
        </script>     
    </div>
    <br><br>
    <div id="CQ">
    	<div id="resourcesData"></div>
	</div>
	<br><br>
</body>