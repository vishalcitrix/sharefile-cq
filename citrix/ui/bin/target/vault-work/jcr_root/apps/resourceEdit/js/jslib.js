var Common = Common || {
    init: function() {
        //jQuery.get('/bin/resourceconsole',{"listType": ""}, function(data) {
        jQuery.get('/bin/resourceconsole', function(data) {
            var html = "";
            html+= "<div style='width: 1629px;' class='x-panel-body x-panel-body-noheader'>";
                html+= "<div class='x-grid3-header'>";
                    html+= "<table cellspacing='0' cellpadding='0' border='0'>";
                        html+= "<thead>";
                            html+= "<tr class='x-grid3-hd-row'>";
                                html+= "<td style='width: 22px;'></td>";
                                html+= "<td style = 'width: 400px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Title</div>";
                                html+= "</td>";
                                html+= "<td style = 'width: 450px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Path</div>";
                                html+= "</td>";
                                html+= "<td style = 'width: 115px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Resource Type</div>";
                                html+= "</td>";
                                html+= "<td style = 'width: 335px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Categories</div>";
                                html+= "</td>";
                                html+= "<td style = 'width: 100px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Language</div>";
                                html+= "</td>";
                                html+= "<td style = 'width: 100px;'>";
                                    html+= "<div class='x-grid3-hd-inner'>Activated</div>";
                                html+= "</td>";
                            html+= "</tr>";
                        html+= "</thead>";
                    html+= "</table>";
                html+= "</div>";
                jQuery("#resourcesData").html(html);
                html = "<div class='x-grid3-body'></div>";
                jQuery("#resourcesData").append(html);
                        for(var i = 0; i < data.length; i++) {
                                html = "<div class='x-grid3-row' style='width: 1629px;'>";
                            if(i % 2 === 0){
                                html+= "<table cellspacing='0' cellpadding='0' border='0' class='x-grid3-row-alt'>";
                            }else{
                                html+= "<table cellspacing='0' cellpadding='0' border='0'>";
                            }
                            
                                html+= "<thead>";
                                    html+= "<tr>";
                                            html+= "<td class ='x-grid3-cell-inner x-grid3-td-numberer'><div class='x-grid3-cell-inner' style='width: 16px;'>" + (i+1) + "</div></td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 391px;' class='x-grid3-hd-inner'><a href='" + data[i].path + "' target='_blank'>" + data[i].title + "</a></div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 441px;' class='x-grid3-hd-inner'>" + data[i].path + "</div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 107px;' class='x-grid3-hd-inner'>" + data[i].resourceType + "</div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 321px;' class='x-grid3-hd-inner'>" + data[i].categories + "</div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 92px;' class='x-grid3-hd-inner'>" + data[i].language + "</div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 92px;' class='x-grid3-hd-inner'>" + data[i].activated + "</div>";
                                            html+= "</td>";
                                            html+= "<td>";
                                                html+= "<div style = 'width: 92px;' class='x-grid3-hd-inner' id='button" + i + "'>";
                                                
                                                html+= "</div>";
                                                html+= "</td>";
                                        html+= "</tr>";
                                html+= "</thead>";
                            html+= "</table>";
                            jQuery(".x-grid3-body").append(html);
                            
                            var container = document.getElementById('button'+i);
                            var script   = document.createElement("script");
                            script.type  = "text/javascript";
                            script.innerHTML = 'CQ.WCM.edit({"path":"' + data[i].path + '","dialog":"' + data[i].dialogPath +'","type":"'+ data[i].superResourceType +'","editConfig":{"xtype":"editbar","listeners":{"afteredit":"REFRESH_PAGE","afterdelete":"function(){jQuery.post(\'/bin/resourceconsole\', {\'method\':\'delete\',\'path\':\'' + data[i].path + '\'},function(data){console.log(data);alert(data); location.reload();});}"},"actions":[CQ.wcm.EditBase.EDIT,CQ.wcm.EditBase.DELETE]}});';                            
                            container.appendChild(script);
                        }
                   
                html = "</div>";
            html+= "</div>";
            jQuery("#resourcesData").append(html);
        });
    }
};

jQuery(function() {
    Common.init();
});