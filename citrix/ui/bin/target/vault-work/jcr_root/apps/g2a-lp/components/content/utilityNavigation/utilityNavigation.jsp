<%--
    Utility Navigation

    Header contains logo and link set component on the right. Option to add phone number.
    Note: Depends on linkSet component (citrixosd).
    
    vishal.gupta@citrix.com
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="topNav">
	<div class="row">
		<div class="left">
			<div class="logo">
				<a rel="external" href="http://www.citrix.com"><span class="citrix-dark"></span></a>
			</div>
		</div>
		<div class="right">
			<swx:setWCMMode mode="READ_ONLY">
				<div class="show-for-medium-up">
					<cq:include script="linkList.jsp"/>
				</div>
			</swx:setWCMMode>
			<div id="menu">
				<div class="hamburger-icon hide-for-medium-up"></div>
			</div>
		</div>
		<div class="clearBoth"></div>
	</div>	
</div>
<div class="secondaryNav">
	<div class="row">
		<div class="left">
			<swx:setWCMMode mode="READ_ONLY">
		    	<cq:include script="logo.jsp"/>
			</swx:setWCMMode>
		</div>
		<c:if test="${not empty properties.phoneNumber}">
			<div class="right phoneNumber">
				<span class="icon-outline-phone">&nbsp;</span>
				<span class="sales"><fmt:message key="contact.sales.at"/></span>
				<span><b>${properties.phoneNumber}</b></span>
			</div>
		</c:if>
		<div class="clearBoth"></div>
	</div>	
</div>

<div id="slidebar-container"></div>
<div id="slidebar-menu">
	<div class="icon-close"></div>
	<div class="clearBoth"></div>
	<div id="slidebar-content">
		<cq:include script="linkList.jsp"/>
	</div>
</div>