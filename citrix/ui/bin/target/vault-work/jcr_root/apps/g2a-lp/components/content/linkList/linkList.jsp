<%--
    Link List
    
    A collection of links which can have customizable properties. The link list 
    will also have a css class for the first and last item so it would be easier 
    to apply css classes to them. Link text is i18n.
    
    Also option to check if link is same as current page and add 'current' class to li
    
    vishal.gupta@citrix.com 
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String LINKS_PROPERTY = "links";
    private static final String STYLE_SPLITTER_PROPERTY = "splitter";

    public class Link {
        private String css;
        private String text;
        private String path;
        private String linkOption;
        
        public Link() {
            
        }
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<Link> getLinks(NodeIterator nodeIter) {
        final List<Link> shares = new ArrayList<Link>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final Link link = new Link();
                    link.setCss(currLink.hasProperty("css") ? currLink.getProperty("css").getString() : "");
                    link.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : "");
                    link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : "");
                    link.setLinkOption(currLink.hasProperty("linkOption") ? currLink.getProperty("linkOption").getString() : "");
                    
                    if(link.getCss() != null || link.getText() != null || link.getPath() != null) {
                        shares.add(link);	
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }

%>

<%
    List<Link> links = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
    }
    
    final Map<String, Object> linkList = new HashMap<String, Object>();
    linkList.put(LINKS_PROPERTY, links);
    linkList.put(STYLE_SPLITTER_PROPERTY, currentStyle.get(STYLE_SPLITTER_PROPERTY, String.class));
%>

<c:set var="linkList" value="<%= linkList %>"/>

<c:choose>
    <c:when test="${fn:length(linkList.links) > 0}">
        <ul>
            <c:forEach items="${linkList.links}" begin="0" var="link" varStatus="i">
                <c:choose>
                    <c:when test="${link.linkOption eq '_blank'}">
                        <li class="${i.first ? 'first' : ''} ${i.last ? 'last' : ''}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} target="${link.linkOption}"></a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="${i.first ? 'first' : ''} ${i.last ? 'last' : ''}"><a href="${not empty link.path ? link.path : ''}" ${empty link.path ? 'onclick="return false;"' : ''} rel="${link.linkOption}"><fmt:message key="${link.text}"/></a></li>
                    </c:otherwise>
                </c:choose>
                <c:if test="${not empty linkList.splitter}">
                    <c:choose>
                        <c:when test="${i.last}">
                            <li style="display:inline;"><%--Do nothing --%></li>
                        </c:when>
                        <c:otherwise>
                            <li class="splitter" style="display:inline;"><span>${linkList.splitter}</span></li>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>