<%--

Invoice History Component

--%>

<%@include file="/libs/foundation/global.jsp"%>

<script>

    $(document).ready(function(){

        $('.tabs .tab-links a').on('click', function(e)  {
            var currentAttrValue = $(this).attr('href');
            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).show().siblings().hide();
    
            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

        });
        
    });

</script>

<div class="row" id="invoiceHistory">
    <h2 class="text-center">Invoice history</h1>
    <div class="close_modal">
        <i class="icon-close-large"></i>
    </div>
    <div class="tabs large-12 columns animated fadeIn">

        <div class="large-12 columns" id="tabList">

            <ul class="tab-links">
                <li class="active">
                    <a href="#year2015">2015</a>
                </li>
                <li>
                    <a href="#year2014">2014</a>
                </li>
                <li>
                    <a href="#year2013">2013</a>
                </li>
                <li>
                    <a href="#year2012">2012</a>
                </li>
            </ul>
        </div>


        <div class="row">
            <div class="large-12 columns">

                <div class="row paymentPage">


                    <div class="invoiceView">
                        <div class="tab-content">
                            <div id="year2015" class="tab show animated fadeIn active">
                                <ul>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Mar 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right">Due:</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="green-txt">$1056.00</h3>
                                          <p> <a>Pay now </a></p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Feb 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right">Past Due:</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="red-txt">$1056.00</h3>
                                          <p> <a>Pay now </a></p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Jan 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li>                                                                    
                                </ul>                                
                            </div>
                            <div id="year2014" class="tab show animated fadeIn">
                                <ul>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Dec 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Nov 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Oct 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Sep 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li>                                                                                                                      
                                </ul>
                            </div>
                            <div id="year2013" class="tab show animated fadeIn">
                                <ul>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Dec 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Nov 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Oct 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li>                                                                                                     
                                </ul>
                            </div>
                            <div id="year2012" class="tab show animated fadeIn">
                                <ul>
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Dec 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Nov 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li> 
                                  <li>
                                    <div class="invoice-title">
                                      <div class="row">
                                        <div class="small-8 columns">
                                          <h3 class="text-left"> <a href="">Invoice 1200012003</a></h3>
                                          <p>Date issued: Oct 15, 2015</p>
                                        </div>
                                        <div class="small-2 columns">
                                          <p class="text-right"></p>
                                        </div>
                                        <div class="small-2 columns">
                                          <h3 class="paid">Paid</h3>
                                        </div>
                                      </div>
                                    </div>
                                  </li>                                                                                                     
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>