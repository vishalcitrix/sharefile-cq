<%--
    Search Component - The search functionality for Support Home Page

    prabhu.shankar@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<script>
    $(function(){
        var quoteUserId = (typeof json !== "undefined") ? json.id : 'null';
        Mediator.add(QuoteList.init, [quoteUserId]);
    })
</script>

<section class="quoteListTable content-block" id="quoteList">
    <div role="grid" class="standard-list quoteList">
      <div class="tbody">

      </div>
    </div>
</section>