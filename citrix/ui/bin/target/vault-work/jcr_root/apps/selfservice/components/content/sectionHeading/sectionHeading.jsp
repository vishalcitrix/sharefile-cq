<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="headingValue" value="${not empty properties['heading'] ? properties['heading'] : 'Heading goes here'}"/>
<c:set var="headingClassValue" value="${not empty properties['headingclass'] ? properties['headingclass'] : ''}"/>

<c:if test="${(isEditMode or isReadOnlyMode) or
           (not empty properties['heading'])}">
    <cq:text value="${headingValue}" tagName="h2" tagClass="${headingClassValue}" escapeXml="false"/>
</c:if>
<div class="clearBoth"></div>