<%--

  userInfoModal component.

  this form will be used to update the user details.

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
    // TODO add you code here
%>

<script>
    
   $(document).ready(function(){
        
        userInfoModal.init();
   });

</script> 




            <div class="row">
                <div id="editUserInfo" class="large-12 columns">
                    <h3 class="formHeading">Your Account</h3>
                    <p class="fieldHeading">Profile</p>
                    <form id="userForm"  name="serForm">
                        <ul class="userForm" class="large-12 columns"> 
                            <div class="large-12 columns">
                            <li>
                                <label>Full Name</label>
                                <input type="text" class="formInput" name="name" id="account-form-name">
                            </li>
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Business Name</label>
                                <input type="text" class="formInput" name="busname" id="account-form-busname">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Phone</label>
                                <input type="text" class="formInput"  name="phone" id="account-form-phone">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Email</label>
                                <input type="text" class="formInput"  name="email" id="account-form-email">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <br>
                            <p class="fieldHeading">Address</p>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Address 1</label>
                                <input type="text" class="formInput"  name="address1" id="account-form-address1">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label id="address2focus">Address 2</label>
                                <input type="text" class="formInput"  name="address2" id="account-form-address2" placeholder="Optional">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">


                            <li>
                                <label>City</label>
                                <input type="text" class="formInput"  name="city" id="account-form-city">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>State</label>
                                <input type="text" class="formInput"  name="state" id="account-form-state">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Zip Code</label>
                                <input type="text" class="formInput"  name="zip" id="account-form-zipcode">
                            </li>
                            
                            </div>
                            <div class="large-12 columns">
                            <li>
                                <label>Country</label>
                                <input type="text" class="formInput"  name="country" id="account-form-country">
                            </li>
                            </div>
                            


                            
                        </ul>
                        <div class="squaredFour large-12 columns">
                        <input type="checkbox" id="checkBillToAddress" value="None" /><label for="checkBillToAddress"></label>
                        <p>Use as billing address</p>
                                    </div>  
                        <div class="squaredFour large-12 columns">        
                        <input type="checkbox" id="checkShipToAddress" value="None" /><label for="checkShipToAddress"></label>
                        <p>Use as sold to address</p>
                            </div>
                        <div class="large-12 columns">
                        <input type="submit" id="saveInfo" class="btn btn-secondary" value="Save Changes"onclick="editUserInfo.myValidateFunction()">
                        <div id="cancelInfo" class="btn btn-secondary" onclick="userInfoModal.cancelButton()">Cancel</div>
                        </div>
                    </form>


                </div>
            </div>
      