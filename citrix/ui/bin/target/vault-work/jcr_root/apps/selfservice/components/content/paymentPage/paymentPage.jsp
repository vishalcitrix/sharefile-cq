<%--

  editUserInfo component.

  this form will be used to update the user details.

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
    // TODO add you code here
%>

<!-- Manually overide Zuora Iframe Style-->
<style>
#z_hppm_iframe {
  background-color: #fff;
}
</style>

<script type="text/javascript" src="/etc/designs/selfservice/clientlib/js/components/paymentPage.js"></script>

<script>

    $(document).ready(function(){        
        //if(Mediator) {
            //Mediator.add(PaymentPage.init);
        //} else {
            PaymentPage.init();
        //}
    })

</script>
<div class="row" id="paymentPage">
        <div id="mainPaymentTabs" class="tabs large-12 columns animated fadeIn">
            
            <div class="large-12 columns" id="tabList">
                
                <ul class="tab-links">
                    <li class="active"><a id="creditCard_Tab" class="outerTabs">Credit Card</a>
                    </li>
                    <li><a id="check_Tab" class="outerTabs">Check/Cheque</a>
                    </li>
                    <li><a id="directDebit_Tab" class="outerTabs">Direct Debit</a>
                    </li>
                    <li><a id="bankTransfer_Tab" class="outerTabs">Bank Transfer</a>
                    </li>

                </ul>
            </div>


            <div class="row">
                <div class="large-12 columns" >
       
                   <div class="row paymentPage">
                               
                        <div class="paymentView">


                            <!-- Payment Method Error Box -->
                            <div id="paymentMethodError" class="large-9 columns hide fadeIn alert" style="background-color: #f2dede; border: 1px solid #ebccd1; line-height: 1.42857143; color: #a94442; border-radius: 4px; margin-top: 20px; margin-left: 15px; padding: 15px;">
                                <p>Please select a payment method to continue.</p>
                            </div>




                            <!-- Quote Error Box -->
                            <div id="quoteError" class="large-9 columns hide fadeIn alert" style="background-color: #f2dede; border: 1px solid #ebccd1; line-height: 1.42857143; color: #a94442; border-radius: 4px; margin-top: 20px; margin-left: 15px; padding: 15px;">
                                <p>Please select a quote to continue.</p>
                            </div>




                            <!-- Payment Method Success -->
                            <div id="paymentMethodSuccess" class="large-9 columns hide fadeIn alert" style="background-color: #dff0d8; border: 1px solid #d6e9c6; line-height: 1.42857143; color: #3c763d; border-radius: 4px; margin-top: 20px; margin-left: 15px; padding: 15px;">
                                <p>Your payment method has been added successfully! <br>You can now continue to the next step.</p>
                            </div>


                            <div style="clear: both; height:1px;">&nbsp;</div>

                            <div class="tab-content">
                                <div class="tab show animated fadeIn active" id="creditCard">
                                    
                                    <div class="existing-cards large-12 columns">

                                    <h3>Credit Card(S)</h3>
                                    
                                    <div class="noexisting"><br><p>You have no existing credit cards.</p><br></div>

                                    <ul class="existing large-12 columns hide"></ul>
                                    
                                    </div>

                                    <button class="btn btn-secondary" id="newCard" style="clear: both; display: block;">Add New Card</button>

                                 <div id="termsAgreementCreditCard" class="terms-agree large-12 columns">                                          
                                           <div class="squaredFour"><input type="checkbox" id="agreeCredit" class="agreementBox" value="None"/>
                                           <label for="agreeCredit"></label>
                                           <p>By changing my payment method to credit card, I am agreeing to use this payment method for all future recurring invoice charges to my account.</p>
                                           </div>
                                 </div>
                                </div>
                                <div class="tab show animated fadeIn" id="check">
                                    <ul class="existing large-12 columns hide"></ul>
                                    <h3>Check/Cheque with Remittance</h3>
                                    <p class="sub-copy">Available for Citrix Online LLC (Americas) contracts. Checks in currencies other than U.S. dollars may be delayed.</p>
                                    <p class="sub-copy">Send payments to the following lockbox address: Please include invoice and customer number.</p>


                                    <ul class="accordion">
                                        <li class="accordion-section large-12 columns">
                                            <a class="accordion-section-title large-12 columns" href="#america-expand"><i class="icon-down-open"></i>Americas</a>
                                            <div id="america-expand" class="accordion-section-content">


                                                <div class="check-cont">
                                                   
                                                    <p>Citrix Systems Inc.
                                                      <br>P.O. Box 50264
                                                      <br>Los Angeles, CA 90074-0264
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title large-12 columns" href="#asia-expand"><i class="icon-down-open"></i>Asia Pacific</a>
                                            <div id="asia-expand" class="accordion-section-content">
                                               <div class="check-cont">
                                                   
                                                    <p>Citrix Systems Asia Pacific Pty. Ltd.
                                                      <br>GPO Box 1160,
                                                      <br>Sydney NSW 2001
                                                      <br>Australia
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title large-12 columns" href="#europe-expand"><i class="icon-down-open"></i>Europe, Middle East, Africa</a>
                                            <div id="europe-expand" class="accordion-section-content">

                                                  <div class="check-cont">
                                                   
                                                    <p>Citrix Systems UK Ltd.
                                                      <br>Chalfont Park House
                                                      <br>Chalfont Park
                                                      <br>Gerrards Cross
                                                      <br>SL9 0DZ United Kingdom
                                                    </p>
                                                </div>

                                            </div>
                                        </li>
                                    </ul>

                                        <div class="terms-agree large-12 columns">
                                          
                                             <div class="squaredFour"><input type="checkbox" id="agreeCheck" class="agreementBox" value="None"/>
                                           <label for="agreeCheck"></label>
                                           <p>By changing my payment method to check, I am agreeing to use this payment method for all future recurring invoice charges to my account.</p>
                                           </div>
                                         
                                          
                                    </div>
                                </div>
                                <div class="tab show animated fadeIn" id="directDebit">

                                    <div id="existingDD" class="existing-cards large-12 columns">

                                    <h3>Direct Debit Account(S)</h3>
                                    
                                    <div class="noexisting"><br><p>You have no existing direct debit accounts.</p><br></div>

                                    <ul class="existing large-12 columns"></ul>                                 
                                    
                                    </div>

                                    
                                    <p class="sub-copy">Direct Debit by Paymentech on behalf of Citrix Systems UK LTD</p>
                                    <p class="sub-copy">How to pay using Direct Debit/ ACH (Automated Clearing House)</p>

                                    <ul class="accordion">
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#usa-dd"><i class="icon-down-open"></i>USA</a>
                                            <div id="usa-dd" class="accordion-section-content">

                                                 <div class="deposit-cont">
                                                    <p>Download: <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    <p>Secure upload to:<a href="https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459">https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459</a>
                                                    </p>
                                                </div>

                                                <button class="btn btn-secondary" id="newDD-US" style="clear: both; display: block; margin-top: 15px;">Add New Account</button>

                                            </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#germany-dd"><i class="icon-down-open"></i>Germany</a>
                                            <div id="germany-dd" class="accordion-section-content">

                                                <div class="deposit-cont">
                                                    <p>Download form (German): <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    <p>Download form (English): <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    <p>Secure upload to:<a href="https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459">https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459</a>
                                                    </p>
                                                </div>

                                                <button class="btn btn-secondary" id="newDD-DE" style="clear: both; display: block; margin-top: 15px;">Add New Account</button>
                                            </div>
                                        </li>
                                           <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#france-dd"><i class="icon-down-open"></i>France</a>
                                            <div id="france-dd" class="accordion-section-content">

                                                <div class="deposit-cont">
                                                    <p>Download form (Francais): <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    <p>Download form (English): <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    <p>Secure upload to:<a href="https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459">https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459</a>
                                                    </p>
                                                </div>

                                                <button class="btn btn-secondary" id="newDD-FR" style="clear: both; display: block; margin-top: 15px;">Add New Account</button>
                                            </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#uk-dd"><i class="icon-down-open"></i>United Kingdom</a>
                                            <div id="uk-dd" class="accordion-section-content">
                                                <div class="deposit-cont">
                                                    <p>Download form (German): <a href="">Direct Debit/ACH Authorization Form (PDF)</a>
                                                    </p>
                                                    
                                                    <p>Secure upload to:<a href="https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459">https://saasfinance.sharefile.com/r/r3ddb6bdd5d647459</a>
                                                    </p>
                                                </div>

                                                <button class="btn btn-secondary" id="newDD-UK" style="clear: both; display: block; margin-top: 15px;">Add New Account</button>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                        <div class="terms-agree large-12 columns">
                                           <div class="squaredFour"><input type="checkbox" id="agreeDD" class="agreementBox" value="None"/>
                                           <label for="agreeDD"></label>
                                           <p>By selecting the Direct Debit/ACH Payment type, we authorize Citrix Systems to initiate debit entries to the Company's bank account indicated above and to automatically initiate these payments in accordance with the billing frequency selected for this account. To opt out of automatic processing, a different payment method will need to be selected.</p>
                                           </div>


                                          
                                    </div>

                                </div>
                                <div class="tab show animated fadeIn" id="bankTransfer">
                                    <ul class="existing large-12 columns hide"></ul>
                                    

                                    <h3>WIRE/Bank transfer (based on region and currency)</h3>
                                    <p class="sub-copy">Please reference invoice and customer number on all payments.</p>

                                    <ul class="accordion">
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#america-transfer"><i class="icon-down-open"></i>Americas</a>
                                            <div id="america-transfer" class="accordion-section-content">


                                                <div class="accordion-table large-6 columns">
                                                <table>
                                                 
                                                  <tbody>
                                                    <tr>
                                                      <td>Bank/Wire Transfer</td>
                                                      <td>$ U.S. Dollars</td>
                                                    </tr>
                                                    <tr>
                                                      <td>Account number</td>
                                                      <td>3756222346</td>
                                                 
                                                    </tr>
                                                    <tr>
                                                      <td>Beneficiary</td>
                                                      <td>Citrix Systems Inc.</td>
                                                     
                                                    </tr>
                                                    <tr>
                                                      <td>Wire Routing #</td>
                                                      <td>0260-0959-3</td>
                                                     
                                                    </tr><tr>
                                                      <td>ACH Routing #</td>
                                                      <td>111000012</td>
                                                     
                                                    </tr><tr>
                                                      <td>SWIFT</td>
                                                      <td>BOFAUS3N</td>
                                                     
                                                    </tr><tr>
                                                      <td>CHIPS Address</td>
                                                      <td>959</td>
                                                     
                                                    </tr><tr>
                                                      <td>Bank</td>
                                                      <td>Bank of America<br>100 West 33rd Street<br>New York, NY 10001</p></td>
                                                     
                                                    </tr>

                                                  </tbody>
                                                </table>

                                            </div>

                                          
                                         </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#asia-transfer"><i class="icon-down-open"></i>Asia Pacific</a>
                                            
                                            <div id="asia-transfer" class="accordion-section-content">



                                                <div class="tabs large-12 columns">
                                                    <ul class="tab-links accordion-tabs" data-tab>
                                                        <li class="active"><a id="aus-nz-transfer_Tab" class="innerTabs">$ AUS/NZ Dollar</a>
                                                        </li>
                                                        <li><a id="usd-transfer_Tab" class="innerTabs">$ U.S. Dollar</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">

                                                        <div class="tab" id="aus-nz-transfer">

                                                  <div class="accordion-table large-12 columns">
                                                     <table>
                                                     
                                                      <tbody>
                                                        <tr>
                                                          <td>Bank/Wire Transfer</td>
                                                          <td>$ AUS/NZ Dollar</td>
                                                        </tr>
                                                        <tr>
                                                          <td>Account number</td>
                                                          <td>149 31 024</td>
                                                     
                                                        </tr>
                                                        <tr>
                                                          <td>Beneficiary</td>
                                                          <td>Citrix Systems Asia Pacific Pty. Ltd.</td>
                                                         
                                                        </tr>
                                                        <tr>
                                                          <td>SWIFT</td>
                                                          <td>BOFAAUSX</td>
                                                         
                                                        </tr><tr>
                                                          <td>BACS/BSB</td>
                                                          <td>232-001</td>
                                                         
                                                        </tr><tr>
                                                          <td>Bank</td>
                                                          <td>Bank of America N.A.<br>Level 64, MCL Center<br>19 Martin Place<br>Sydney, NSW 2000 AUS</td>
                                                         
                                                        </tr>
                                                       </tbody>
                                                      </table>

                                                 </div>

                                                        
                                                        </div>




                                                        <div class="tab active" id="usd-transfer">
                                                           <div class="accordion-table large-12 columns">
                                                             <table>
                                                             
                                                              <tbody>
                                                                <tr>
                                                                  <td>Bank/Wire Transfer</td>
                                                                  <td>$ U.S. Dollar</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Account number</td>
                                                                  <td>149 31 032</td>
                                                             
                                                                </tr>
                                                                <tr>
                                                                  <td>Beneficiary</td>
                                                                  <td>Citrix Systems Asia Pacific Pty. Ltd.</td>
                                                                 
                                                                </tr>
                                                                <tr>
                                                                  <td>SWIFT</td>
                                                                  <td>BOFAAUSX</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>BACS/BSB</td>
                                                                  <td>232-001</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Bank</td>
                                                                  <td>Bank of America N.A.<br>Level 64, MCL Center<br>19 Martin Place<br>Sydney, NSW 2000 AUS</td>
                                                                 
                                                                </tr>
                                                               </tbody>
                                                              </table>

                                                             </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="accordion-section  large-12 columns">
                                            <a class="accordion-section-title  large-12 columns" href="#europe-transfer"><i class="icon-down-open"></i>Europe, Middle East, Africa</a>
                                            <div id="europe-transfer" class="accordion-section-content">

                                               <div class="tabs large-12 columns">
                                                    <ul class="tab-links accordion-tabs" data-tab>
                                                        <li class="active"><a id="pound-transfer_Tab" class="innerTabs">&pound; Pounds Sterling</a>
                                                        </li>
                                                        <li><a id="euro-transfer_Tab" class="innerTabs">&euro; Euro</a>
                                                        </li>
                                                        <li><a id="usd-dd-transfer_Tab" class="innerTabs">$ U.S. Dollar</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">

                                                        <div class="tab active" id="pound-transfer">
                                                          <div class="accordion-table large-12 columns">
                                                             <table>
                                                             
                                                              <tbody>
                                                                <tr>
                                                                  <td>Bank/Wire Transfer</td>
                                                                  <td>&pound; Pounds Sterling</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Account number</td>
                                                                  <td>28497023</td>
                                                             
                                                                </tr>
                                                                <tr>
                                                                  <td>Beneficiary</td>
                                                                  <td>Citrix Systems UK Ltd.</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>IBAN</td>
                                                                  <td>GB70 BOFA 1650 5028 4970 23</td>
                                                                 
                                                                </tr>
                                                                <tr>
                                                                  <td>SWIFT</td>
                                                                  <td>BOFA GB22</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>CHIPS/CHAPS</td>
                                                                  <td>165 050</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>BACS/BSB</td>
                                                                  <td>301 635</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Bank</td>
                                                                  <td>Bank of America N.A.<br>5 Canada Square<br>London E14 5AQ, UK</td>
                                                                 
                                                                </tr>
                                                               </tbody>
                                                              </table>

                                                             </div>






                                                        </div>
                                                        <div class="tab" id="euro-transfer">

                                                          <div class="accordion-table large-12 columns">
                                                             <table>
                                                             
                                                              <tbody>
                                                                <tr>
                                                                  <td>Bank/Wire Transfer</td>
                                                                  <td>&euro; Euro</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Account number</td>
                                                                  <td>28497057</td>
                                                             
                                                                </tr>
                                                                <tr>
                                                                  <td>Beneficiary</td>
                                                                  <td>Citrix Systems UK Ltd.</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>IBAN</td>
                                                                  <td>GB25 BOFA 1650 5028 4970 57</td>
                                                                 
                                                                </tr>
                                                                <tr>
                                                                  <td>SWIFT</td>
                                                                  <td>BOFA GB22</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>CHIPS/CHAPS</td>
                                                                  <td>165 050</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Branch Code</td>
                                                                  <td>6008</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Bank</td>
                                                                  <td>Bank of America N.A.<br>5 Canada Square<br>London E14 5AQ, UK</td>
                                                                 
                                                                </tr>
                                                               </tbody>
                                                              </table>

                                                             </div>

                                                        </div>
                                                        <div class="tab" id="usd-dd-transfer">

                                                            <div class="accordion-table large-12 columns">
                                                             <table>
                                                             
                                                              <tbody>
                                                                <tr>
                                                                  <td>Bank/Wire Transfer</td>
                                                                  <td>$ U.S. Dollar</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Account number</td>
                                                                  <td>28497049</td>
                                                             
                                                                </tr>
                                                                <tr>
                                                                  <td>Beneficiary</td>
                                                                  <td>Citrix Systems UK Ltd.</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>IBAN</td>
                                                                  <td>GB47 BOFA 1650 5028 4970 49</td>
                                                                 
                                                                </tr>
                                                                <tr>
                                                                  <td>SWIFT</td>
                                                                  <td>BOFA GB22</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Branch Code</td>
                                                                  <td>6008</td>
                                                                 
                                                                </tr><tr>
                                                                  <td>Bank</td>
                                                                  <td>Bank of America N.A.<br>5 Canada Square<br>London E14 5AQ, UK</td>
                                                                 
                                                                </tr>
                                                               </tbody>
                                                              </table>

                                                             </div>

                                                        </div>

                                                      </div>
                                                    </div>                                                    

                                            </div>
                                        </li>
                                    </ul>

                                    <div class="terms-agree large-12 columns">
                                           <div class="squaredFour"><input type="checkbox" id="agreeBT" class="agreementBox" value="None"/>
                                           <label for="agreeBT"></label>
                                           <p>By changing my payment method to bank transfer, I am agreeing to use this payment method for all future recurring invoice charges to my account.</p>
                                           </div>
                                  
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="navbuttons">

                <button class="btn btn-primary" id="prevPage">back</button>
                <button class="btn btn-primary" id="nextPage">JS</button>

            </div>


        </div>
    </div>
      
       

<div id="lightbox-container">
    <div id="lightbox-border">
        <div id="lightbox-close"><i class="icon-close-large"></i></div>
        <div id="lightbox-dynamic">
           <p class="pageTitle">Add Credit Card & Billing Info</p>                   
           <div id="ccform" class="hide" style="height:970px;"></div>
           <div id="ddform" class="hide" style="height:820px;"></div>
           <div id="achform" class="hide" style="height:430px;"></div>
        </div>
    </div>
</div>