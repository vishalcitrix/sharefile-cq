<%--
    SMS Form Component - Sends message defined by authors to users phone number.

    prabhu.shankar@citrix.com
    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<script type="text/javascript" src="/etc/designs/g2m/js/jquery/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/etc/designs/selfservice/js/utils/oauth-client-library.min.js"></script>

<c:set var="phoneNumber" value="${properties.phoneNumber}"/>
<c:set var="submitLink" value="${properties.submitLink}"/>
<c:set var="password" value="${properties.password}"/>
<c:set var="returnMessageDelivered" value="${properties.returnMessageDelivered}"/>


<div class="registerFormContainer">
    <div class="hide-for-small-only large-8 large-centered row columns">


<h1 class="icon-Warning" style="color: #000000; font-size: 50px; " ></h1>
<h2 class="icon-Warning2" style="color: #000000; font-size: 40px; " ></h2>
<h2 class="icon-Warning3" style="color: #000000; font-size: 40px; " ></h2>

    </div>
</div>



    <script>

 accessToken = sessionStorage.getItem("accessToken");
    
    
    
   $.ajax({
            type: "GET",
            url: "/bin/citrix/singleIdentity",
            data:'acessToken='+accessToken ,
            success: function(msg) {
                             
                var json = jQuery.parseJSON(msg);
                var statusId = json.displayName;
                 
        $('.icon-Warning').addClass("success icon-Check").html("Welcome   "+json.displayName);
         $('.icon-Warning2').addClass("success icon-Check").html("     Email id - "+json.userName);
          $('.icon-Warning3').addClass("success icon-Check").html("     User key - "+json.id);
        $('.icon-Warning').show();
        $('.icon-Warning2').show();
            $('.icon-Warning3').show();
            },failure: function(data) {

            }

        }); 
        
        
   </script>
     
    