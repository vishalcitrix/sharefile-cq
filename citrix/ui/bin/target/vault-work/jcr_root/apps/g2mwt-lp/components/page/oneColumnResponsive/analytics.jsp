<%@page import="com.citrixosd.SiteUtils"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%-- Set environment variable --%>
<c:choose>
    <c:when test="${isDev || isQA}">
            <c:set var="analyticsEnv" value="dev"/>
    </c:when>
    <c:when test="${isStage}">
        <c:set var="analyticsEnv" value="qa"/>
    </c:when>
    <c:when test="${isProd}">
        <c:set var="analyticsEnv" value="prod"/>
    </c:when>
    <c:otherwise>
        <c:set var="analyticsEnv" value="dev"/>
    </c:otherwise>
</c:choose>
<%
    String analyticsEnv = pageContext.getAttribute("analyticsEnv").toString();
%>
<c:set var="analyticsSrcUrl" value="<%= SiteUtils.getAnalyticsURL(currentPage,analyticsEnv) %>"/>
<c:if test="${not empty analyticsSrcUrl}">
    <script <c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">async=""</c:if> type="text/javascript" src="${analyticsSrcUrl}"></script>
</c:if>
<div class="mktgContainer">
	<img src="<%= currentDesign.getPath() %>/css/static/images/1x1.gif" id="mktgPixelImg" width="0" height="0" alt="">
</div>