<%--

  Top Navigation component.

  Top Navigation component for LPs

--%><%
%><%@include file="/libs/foundation/global.jsp"%>

<c:set var="noBgColor" value="bgColorTransparent"/>
<c:set var="bgColor" value="<%= properties.get("color", "#fff") %>"/>

<div class="lpNavigation">
	<c:choose>
	    <c:when test="${not empty properties.browserWidth}">     
	        <div class="topNav" style="width: 100%; <c:if test='${bgColor ne noBgColor}'>background:${bgColor};</c:if>">
	    </c:when>
	    <c:otherwise>
	        <div class="topNav styleswitch" data-large="<c:if test='${not empty properties.widthDesktop}'>width:${properties.widthDesktop}px;</c:if><c:if test='${bgColor ne noBgColor}'>background:${bgColor};</c:if>" data-medium="<c:if test='${not empty properties.widthTablet}'>width:${properties.widthTablet}px;</c:if><c:if test='${bgColor ne noBgColor}'>background:${bgColor};</c:if>" data-small="<c:if test='${not empty properties.widthSmall}'>width:${properties.widthSmall}px;</c:if><c:if test='${bgColor ne noBgColor}'>background:${bgColor};</c:if>">
	    </c:otherwise>
	</c:choose>
				<div class="left">
					<cq:include path="logo" resourceType="/apps/citrixosd-responsive/components/content/logo"/>
				</div>
				<div class="right">
					<cq:include script="linkList.jsp"/>
				</div>
	        </div>
</div>