<%--
  ************************************************************************
  ADOBE CONFIDENTIAL
  ___________________

  Copyright 2011 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
  ************************************************************************

  ==============================================================================

--%>

<%@page import="com.day.cq.i18n.I18n"%>
<%@page import="com.day.cq.personalization.TeaserUtils"%>
<%@page import="com.day.cq.wcm.core.stats.PageViewStatistics"%>
<%@page import="com.day.cq.wcm.api.WCMException"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="com.day.text.Text"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.sling.api.resource.ResourceUtil"%>
<%@page import="com.day.cq.commons.Filter"%>
<%@page import="com.day.cq.wcm.api.Page"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="com.day.cq.widget.HtmlLibraryManager" %>


<%@include file="/libs/foundation/global.jsp" %>
<%	final String campaignTitle = StringEscapeUtils.escapeHtml4(currentPage.getTitle());%>

<html>
<head>
    <title>CQ5 Campaign | <%= campaignTitle %></title>
    <meta http-equiv="Content-Type" content="text/html; utf-8"/>
    <%
        final HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
        if (htmlMgr != null) {
            htmlMgr.writeIncludes(slingRequest, out, "cq.wcm.edit", "cq.security", "cq.personalization");
        }
    %>
    <script src="/libs/cq/ui/resources/cq-ui.js" type="text/javascript"></script>
</head>

<body>
	<h1><%= campaignTitle %></h1>
<%
    final Iterator<Page> iterator = currentPage.listChildren();
    while (iterator.hasNext()) {
        final Page child = iterator.next();
        final I18n i18n = new I18n(slingRequest);
        final String imagePath = TeaserUtils.getImage(child);

        final String name = child.getName();
        final String title = StringEscapeUtils.escapeHtml4(child.getTitle());

        ValueMap content = child.getProperties();
        if (content == null) {
            content = ValueMap.EMPTY;
        }
        final String description = StringEscapeUtils.escapeHtml4(content.get("jcr:description", ""));

        String iconClass = "cq-teaser-header-on";
        if (!child.isValid()) {
        	iconClass = "cq-teaser-header-off";
        }

        long monthlyHits = 0;
        try {
            final PageViewStatistics statistics = sling.getService(PageViewStatistics.class);
            final Object[] hits = statistics.report(child);
            if (hits != null && hits.length > 2) {
                monthlyHits = (Long) hits[2];
            }
        } catch (WCMException ex) {
            monthlyHits = -1;
        }

    %>
    
    <h2 class="<%= iconClass %>"><a class="cq-teaser-header" href="<%= child.getPath() %>.html"><%= title %></a></h2>
  	<p><%= description %></p>
    
    <img class="cq-teaser-img" src="<%= imagePath %>" alt="<%= title %>" border="0">
    <ul class="cq-teaser-data">
        <li>
            <div class="li-bullet cq-teaser-status-<%= (child.isValid() ? "active" : "inactive") %>">
                <%= child.isValid() ? i18n.get("Teaser is <strong>active</strong>:") : i18n.get("Teaser is <strong>inactive</strong>:") %>
                <%= i18n.get("on/off times are") %><strong><%= formatDate(child.getOnTime()) %> / <%= formatDate(child.getOffTime()) %></strong>
            </div>
        </li>
        <li><%= i18n.get("Page tags are: ") %>
            <%
              	final Tag[] tags = child.getTags();
                for (Tag tag : tags) {
            %>
            	<strong title="<%= tag.getTitlePath() %>"><%= title %></strong>&nbsp;
            <%
                }
            %>
        </li>
        <li><%= i18n.get("Segments are:") %>&nbsp;
            <%
                String[] segments = new String[]{};
                if (content.containsKey("cq:segments")) {
                    segments = content.get("cq:segments", segments);
                }
                for (String segment : segments) {
            %>
            		<strong><a href="<%= segment %>.html"><%= Text.getName(segment) %></a></strong>&nbsp;
            <%
                }
            %>
        </li>
        <li><%= i18n.get("Teaser has been viewed <strong>{0}</strong> times last month", null, monthlyHits) %></li>

    </ul>
    <br>
    
<%
    }
%>
</body>
</html>

<%!
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public static String formatDate(Calendar date) {
        if (date == null) {
            return "not defined";
        } else {
            return dateFormat.format(date.getTime());
        }
    }
%>