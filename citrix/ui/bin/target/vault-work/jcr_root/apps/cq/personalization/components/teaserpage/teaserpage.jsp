<%@include file="/libs/foundation/global.jsp"%>

<% 
    String product = (String)properties.get("product");
    request.setAttribute("product", product);
    String env = (String) properties.get("env");
    if ("mobile".equals(env)) {
        request.setAttribute("isMobile", true);
    }
    if ("desktop".equals(env)) {
        request.setAttribute("isDesktop", true);
    }
    request.setAttribute("env", env);
%>

<cq:include script="page.jsp"/>