<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Mbox 'end' component

  Draws the end of a Test&Target Mbox

--%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page import="javax.jcr.Node,
                  javax.jcr.Property,
                  javax.jcr.Value,
                  javax.jcr.PathNotFoundException,
                  javax.jcr.RepositoryException,
                  com.day.cq.analytics.AnalyticsConfiguration,
                  com.day.cq.analytics.testandtarget.util.MboxHelper,
                  com.day.cq.wcm.webservicesupport.ConfigurationManager,
                  com.day.cq.wcm.webservicesupport.Configuration,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.commons.json.JSONArray,
                  com.day.cq.wcm.api.WCMMode" %>
<%
    final WCMMode wcmMode = WCMMode.fromRequest(request);

    ConfigurationManager cfgMgr = sling.getService(ConfigurationManager.class);
    Configuration configuration = null;
    String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});
    if(cfgMgr != null) {
        configuration = cfgMgr.getConfiguration("testandtarget", services);
    }
    final AnalyticsConfiguration analyticsConfig = resource.adaptTo(AnalyticsConfiguration.class);
    final Boolean isValidConfig = ( (analyticsConfig != null && analyticsConfig.get("cq:ttclientcode") != null) || 
            (configuration != null && configuration.getInherited("clientcode", null) != null) );
  
    final Resource startMbox = MboxHelper.searchStartElement(resource);
    final ValueMap resourceConfig = startMbox.adaptTo(ValueMap.class); 
    final String mboxId = MboxHelper.getMboxId(resource);
    final String mboxName = MboxHelper.getMboxName(resource);
    final String clientCode;
    if (analyticsConfig != null && analyticsConfig.get("cq:ttclientcode") != null) {
        clientCode = analyticsConfig.get("cq:ttclientcode", null);
    } else if (configuration != null) {
        clientCode = configuration.getInherited("clientcode", null);
    } else {
    	clientCode = null;
   	}

    // draw the edit bar
    if (editContext != null) {
        editContext.includeEpilog(slingRequest, slingResponse, wcmMode);
    }
    
    // turn of decoration and close the decorating DIV
    componentContext.setDecorate(false);
    %> 
    </div>  
</div>
<%
if(isValidConfig && (wcmMode != WCMMode.EDIT)) {
  String[] mappings = resourceConfig.get("cq:mappings", new String[0]);  
  String[] isProfile = resourceConfig.get("cq:isprofile", new String[0]);      
%>
<script type="text/javascript">
CQ_Analytics.TestTarget.init('<%= clientCode %>');
<% if (mappings.length > 0 ) { %><%
%>mboxDefine("<%= mboxId %>", "<%= mboxName %>"<%
%><% } else { %><%
%>mboxCreate("<%= mboxName %>"<%
%><% } %><%
    //page parameters
    for(String key: resourceConfig.keySet() ) {
        if(key.indexOf(":") == -1) {
            %>,<%
            %>"<%= key %> =<%=resourceConfig.get(key, "") %>"<%
        }
    }
    //static parameters
    String[] staticparams = resourceConfig.get("cq:staticparams", new String[0]);
    Node resNode = currentPage.adaptTo(Node.class);
    for(String arr : staticparams) {
        JSONArray jsonElem = new JSONArray(arr);
        String key = jsonElem.getString(0);
        String value = jsonElem.getString(1);
        if(value.startsWith("./") && resNode != null) {
            try {
                Property prop = resNode.getProperty(value);
                value = getPropertyValue(prop);
            }catch(PathNotFoundException e) {}
        }
        %>,<%
        %>"<%= xssAPI.encodeForJSString(key + "=" + value)%>"<%
    }
%>)<%
	if (mappings.length > 0) {
%>    
if (!CQ_Analytics.mboxes) { CQ_Analytics.mboxes = new Array(); }
CQ_Analytics.mboxes.push({id: "<%= mboxId %>", name: "<%= mboxName %>",
    mappings: [<%
        for(int i=0; i < mappings.length; i++) {
            if(i>0) {
                %>,<%
            }
            %>'<%= xssAPI.encodeForJSString(mappings[i]) %>'<%
        }
%>],
    isProfile: [<%
        for(int i=0; i < isProfile.length; i++) {
            if(i>0) {
                %>,<%
            }
            %>'<%= xssAPI.encodeForJSString(isProfile[i]) %>'<%
        }
%>]
});<%
	}
%>
</script><%
%><%
} else {
// WCM.EDITMODE
%><%
%><script type="text/javascript">
//force sidekick to reload in preview mode
CQ.Ext.onReady(function(){
    CQ.WCM.getSidekick().previewReload = true;
});
</script><%
%><%
}
%>
<%!
/**
 * Returns a {@link Property} value and handles multi-value properties by concatenating
 * the values separated by comma.
 *
 * @param property The property to get the value from
 * @return A string representation of the value of this property
 */
protected String getPropertyValue(Property property) throws RepositoryException {
    if(!property.isMultiple()) {
        return property.getString();
    }else{
        String v = "";
        Value[] values = property.getValues();
        for(int i=0; i<values.length; i++) {
            if(i>0) {
                v += ",";
            }
            v += values[i].getString();
        }
        return v;
    }
}
%>