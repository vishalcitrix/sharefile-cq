#base=source

#license
license.js

analytics.js

#shared
shared/operators.js
shared/shadedborder.js
shared/utils.js

shared/sessionpersistence.js
shared/clientcontextmanager.js
shared/serverstorage.js

#segmentmanager
segmentmanager/segmentmanager.js

#strategies
strategies/strategy.js

strategies/list/clickstream-score.js
strategies/list/first.js
strategies/list/random.js

#clickstreamcloud (order defines how items are displayed)
clickstreamcloud/clickstreamcloudui.js
clickstreamcloud/pagedata.js
clickstreamcloud/browserinfo.js
clickstreamcloud/mouseposition.js
clickstreamcloud/eventdata.js

shared/context.js
teaser/teaser-client.js
landingpage/landingpage-client.js

shared/persistedjsonstore.js
shared/jsonstore.js
shared/persistedjsonpstore.js
shared/jsonpstore.js
shared/slider.js

shared/eventtracking.js