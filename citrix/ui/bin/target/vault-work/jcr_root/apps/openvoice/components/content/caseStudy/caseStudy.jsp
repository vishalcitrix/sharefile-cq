<%--

  Case Study component.

  This component includes an logo on the left panel and text and text link on the right. The logo will be rendered
  using an Image Rendition. The text portion includes 3 parts (Title, RichText and Link). There 
  is one dialog to manage the content which includes configuration for the Image Rendition and Text. 
  You have the option to include a Title, Rich Text and Link. The Link is a pathfield which is defaulted 
  to "/content/dam".
  
  
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>


<%!
	private final static String PDF_PATH_PROPERTY = "pdfPath";
	private static final String TITLE_PROPERTY = "title";
	private static final String DESCRIPTION_PROPERTY = "description";
%>

<div>
	<div class="logo">	
		<cq:include path="image" resourceType="citrixosd/components/content/imageRenditions" />
	</div>
	
	<div class="description">
		<c:if test="${not empty properties.title}"> <h4><%= properties.get(TITLE_PROPERTY, "") %></h4> </c:if>
		<c:if test="${not empty properties.description}"><p><%= properties.get(DESCRIPTION_PROPERTY, "") %></p></c:if>
	
		<%	WCMMode mode = WCMMode.fromRequest(request); 
			if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} %>
				<cq:include path="richtext" resourceType="openvoice/components/content/text" />
		<%	mode.toRequest(request);%>	

		<c:choose>
		<c:when test="${ not empty properties.pdfPath }">
			<p class="view-case-study">
				<a rel="external" href="<%=properties.get(PDF_PATH_PROPERTY) %>">View Case Study</a> (PDF)
			</p>
		</c:when>
		<c:otherwise>
			<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
                <img src="/libs/cq/linkchecker/resources/linkcheck_o.gif" alt="invalid link: null" title="invalid link: null" border="0">View Case Study<img src="/libs/cq/linkchecker/resources/linkcheck_c.gif" border="0"> (PDF)
			</c:if>
		</c:otherwise>
		</c:choose>
	</div>
</div>

<div class="clearBoth"></div>
