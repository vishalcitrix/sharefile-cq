<%--

	Press and Awards Component

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="press-award">
	<div class="award-image">
		<cq:include path="image"
			resourceType="/apps/swx/component-library/components/content/imageRenditions" />
	</div>

	<div class="award-description">
		<div class="date smallText">${properties.date}</div>
		<h4>
			${properties.title}
		</h4>

		<p class="award-text">${properties.description}</p>

		<c:choose>

			<c:when test="${not empty properties.link}">

				<a href="${properties.link}" rel="external" class="award-link smallText"><span>Read More</span>
				<span class="sprite ${properties.sprite}"> ${properties.sprite == 'none' ? ' ' : ' '}</span></a>

			</c:when>

			<c:otherwise>
				<c:if test="${isEditMode or isDesignMode}">
					<img src="/libs/cq/linkchecker/resources/linkcheck_o.gif" alt="invalid link: null" title="invalid link: null" border="0">Read More<img src="/libs/cq/linkchecker/resources/linkcheck_c.gif" border="0">
				</c:if>
			</c:otherwise>

		</c:choose>

	</div>
	<div class="clearBoth"></div>
</div>