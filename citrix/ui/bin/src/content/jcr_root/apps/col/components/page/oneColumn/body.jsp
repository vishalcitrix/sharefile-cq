<% %>

<%@include file="/apps/citrixosd/global.jsp"%>

<link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/print.css" type="text/css" media="print" />

 <body>

    <cq:include script="header.jsp" />
    
    <div id="main" class="content">
        <div class="gradient-background"><div><div class="gradient-filter"></div></div></div>
        <div class="content-body container">
        
            <cq:include path="mainContent" resourceType="foundation/components/parsys"/>

        </div>
        
    </div>

    <cq:include script="footer.jsp" />
    
</body>