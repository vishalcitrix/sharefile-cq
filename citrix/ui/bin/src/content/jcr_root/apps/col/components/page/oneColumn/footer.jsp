<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>


<div class="sitemap-container">
	<div class="container">
		<cq:include path="sitemap" resourceType="foundation/components/parsys"/>
	</div>
</div>

<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>

<cq:include script="lightbox.jsp"/>

<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
    <cq:include script="analytics.jsp"/>
</c:if>