<%--
	Image Text List
	- This list can have numbered/plain/logo list.
	
	vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
    
<ul class="list-container">
    <c:forEach begin="1" end="${not empty properties.listCount ? properties.listCount : 1}" varStatus="i">
        <li>
        	<c:if test="${properties.sprite ne 'plain'}">
        		<c:choose>
	        		<c:when test="${properties.sprite eq 'numbered'}">
	        			<div class="pointer number">
			        		<span>${i.count}</span>
				        </div>
	        		</c:when>
	        		<c:otherwise>
	        			<div class="pointer">
	        				<cq:include path="pointer_item_${i.count}" resourceType="/apps/citrixosd/components/content/imageRenditions"/>
	        			</div>
	        		</c:otherwise>
	        	</c:choose>
        	</c:if>
        	
			<div class="text">
				<cq:include path="text_item_${i.count}" resourceType="/apps/g2a-lp/components/content/text"/>
			</div>
        </li>
    </c:forEach>
</ul>