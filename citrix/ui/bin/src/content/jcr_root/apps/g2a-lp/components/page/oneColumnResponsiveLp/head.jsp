<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="java.util.Locale, com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
    public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics";
%>

<%
    response.setHeader("Dispatcher", "no-cache");
    final String designPath = ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request);
    final String designPathOrig = currentDesign.getPath();

    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />

<head>
	<cq:include script="abtest.jsp"/>

    <%-- Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width" />
    <cq:include script="meta.jsp"/>

    <%--CSS --%>
    <!--[if lt IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/lp/lp-ie.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/lp/ie8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/lp/lp.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/lp/ie9.css" type="text/css">
    <![endif]-->
    <!--[if gte IE 9]><link rel="stylesheet" href="${designPath}/css/lp/lp.css" type="text/css"><![endif]-->
    <!--[if !IE]><!--> <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/lp/lp.css" type="text/css"><!--<![endif]-->

    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/css/static/images/favicon.ico">

    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">

    <%-- DNT --%>
    <%
        Locale locale = null;
        final Page localePage = currentPage.getAbsoluteParent(2);
        if (localePage != null) {
            try {
                locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
            } catch (Exception e) {
                locale = request.getLocale();
            }
        }
        else
            locale = request.getLocale();
    %>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/lp/preload.js"></script>
    <script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
    <script type="text/javascript">
        dnt.setMsgClass('footer-dnt');
        dnt.setMsgLocId('footer-dnt');
        dnt.setLocale('<%=locale.toString()%>');
        dnt.dntInit();
    </script>

    <c:if test="<%= properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
        <cq:include script="analytics.jsp"/>
    </c:if>

    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</head>