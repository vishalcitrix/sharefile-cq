<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>

<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/lp/common.js"></script>

<cq:include script="lightbox.jsp"/>

<div class="footer">
	<div class="row">
		<div class="logo">
			<a rel="external" href="http://www.citrix.com">
				<span class="citrix"></span>
			</a>
		</div>
	</div>
</div>

<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>

<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/respond.js"></script>
<![endif]-->

<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
<script type="text/javascript" src="https://sadmin.brightcove.com/js/APIModules_all.js"></script>
<script type="text/javascript" src="https://files.brightcove.com/bc-mapi.js"></script>

<script type="text/javascript">
	function sideBar() {
		if(typeof slideBar != "undefined" && typeof slideBar.defaultHide == "function")
			slideBar.defaultHide();
		else
			setTimeout(sideBar,200);
	}
	sideBar();
</script>