<%--
    Sticky Header.
    
    Javascript will change position css attribute to absolute top when the user
    scrolls below that component. The component will also have an option to display the or in 
    i18n format. Authors can input the description in the Description text field which will 
    display the text in the correct css.
    
    Note: Javascript is disabled in edit mode because it could be in the way for the author.
    
    vishal.gupta@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.api.Page"%>
<%@page import="com.day.cq.wcm.api.components.Component"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String DESCRIPTION_PROPERTY = "description";
    public static final String DESCRIPTIONTWO_PROPERTY = "descriptionTwo";
%>

<c:set var="stickyHeader" value="<%= properties %>"/>

<div class="show-for-large-up">
	<div id="action-header" class="action-nav ${(isPreviewMode || isDisabledMode || isPublishInstance) ? 'active' : 'inactive'}">
        <div class="row">
            <div class="medium-6 large-6 columns text">
                <h2 class="${not empty stickyHeader.descriptionTwo ? 'double-line' : ''}">
                    ${stickyHeader.description}
                    <c:if test="${not empty stickyHeader.descriptionTwo}">            
                         <br>
                         ${stickyHeader.descriptionTwo}
                    </c:if>
                </h2>
            </div>
            
            <div class="medium-6 large-6 columns right text-center action">
                <swx:setWCMMode mode="READ_ONLY">
                    <cq:include path="button" resourceType="/apps/g2a-lp/components/content/button"/>
                </swx:setWCMMode>
            </div>
        </div>
    </div>
    <div class="arrow-down"></div>
</div>