<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="java.util.Locale, com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
    public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics";
%><%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String designPathOrig = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }

    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>
<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="favIcon" value="<%= SiteUtils.getFavIcon(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />

<head>
    <cq:include script="analytics_tag.jsp"/>
	<cq:include script="abtest.jsp"/>
    <%-- Meta tags --%>
    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width" />
    <cq:include script="meta.jsp"/>
    <%--Icons --%>
    <link rel="Shortcut Icon" href="${designPath}/css/static/images/${not empty favIcon ? favIcon : 'favicon'}.ico">
    <%--CSS --%>
    <link rel="stylesheet" href="${designPath}/css/static.css" type="text/css">
    <!--[if lt IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/app-ie.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/app.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie9.css" type="text/css">
    <![endif]-->
    <!--[if gte IE 9]><link rel="stylesheet" href="${designPath}/css/app.css" type="text/css"><![endif]-->
    <!--[if !IE]><!--> <link rel="stylesheet" href="${designPath}/css/app.css" type="text/css"><!--<![endif]-->
    <%-- DNT --%>
    <%
        Locale locale = null;
        final Page localePage = currentPage.getAbsoluteParent(2);
        if (localePage != null) {
            try {
                locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
            } catch (Exception e) {
                locale = request.getLocale();
            }
        }
        else
            locale = request.getLocale();
    %>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/preload.js"></script>
    <script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
    <script type="text/javascript">
        dnt.setMsgClass('footer-dnt');
        dnt.setMsgLocId('footer-dnt');
        dnt.setLocale('<%=locale.toString()%>');
        dnt.dntInit();
    </script>
    <c:if test="<%= properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
        <cq:include script="analytics.jsp"/>
    </c:if>
</head>