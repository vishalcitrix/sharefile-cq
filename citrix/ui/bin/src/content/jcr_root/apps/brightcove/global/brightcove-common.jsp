<%
/*    
    Adobe CQ5 Brightcove Connector  
    
    Copyright (C) 2011 Coresecure Inc.
        
        Authors:    Alessandro Bonfatti
                    Yan Kisen
        
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
%>

<%@page import="org.apache.sling.commons.json.JSONArray"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.apache.sling.commons.json.io.JSONWriter,
                java.io.*, 
                java.net.*,
                java.util.concurrent.TimeUnit,
                org.apache.sling.commons.json.JSONException,
                org.apache.sling.commons.json.JSONObject,
                java.util.*,
                com.brightcove.proserve.mediaapi.wrapper.apiobjects.*,
                com.brightcove.proserve.mediaapi.wrapper.apiobjects.enums.*,
                com.brightcove.proserve.mediaapi.wrapper.utils.*,
                org.slf4j.LoggerFactory,
                org.slf4j.Logger"%>


<%!
static Logger loggerBR = LoggerFactory.getLogger("Brightcove");

static List sortByValue(final Map m) {
    List keys = new ArrayList();
    keys.addAll(m.keySet());
    Collections.sort(keys, new Comparator() {
        public int compare(Object o1, Object o2) {
        try{
            JSONObject v1 = (JSONObject)m.get(o1);
            String s1 = (String)v1.get("name");
            JSONObject v2 = (JSONObject)m.get(o2);
            String s2 = (String)v2.get("name");

            if (s1 == null) {
                return (s2 == null) ? 0 : 1;
            }
            else if (s1 instanceof Comparable) {
                return ((Comparable) s1).compareTo(s2);
            }
            else {
                return 0;
            }
           
           } catch (JSONException e) {
                   return 0;
            }
        }
    });
    return keys;
}

static String getLength(String videoId, String tokenID) {
   String result = "";
   
   
       HttpURLConnection connection = null;
       OutputStreamWriter wr = null;
       BufferedReader rd  = null;
       StringBuilder sb = null;
       String line = null;
       URL serverAddress = null;
       
       try {
           
           serverAddress = new URL("http://api.brightcove.com/services/library?command=find_video_by_id&video_id="+videoId+"&video_fields=name,length&token="+tokenID);
           //set up out communications stuff
           connection = null;
          
           //Set up the initial connection
           connection = (HttpURLConnection)serverAddress.openConnection();
           connection.setRequestMethod("GET");
           connection.setDoOutput(true);
           connection.setReadTimeout(10000);
                     
           connection.connect();
          
           rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
           sb = new StringBuilder();
          
           while ((line = rd.readLine()) != null)
           {
               sb.append(line + '\n');
           }
           
           JSONObject js = new JSONObject(sb.toString());
            
           String temp_result = js.getString("length");
           long millis = Long.parseLong(temp_result);
           result = String.format("%02d:%02d", 
                   TimeUnit.MILLISECONDS.toMinutes(millis),
                   TimeUnit.MILLISECONDS.toSeconds(millis) - 
                   TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
               );
           
       } catch (JSONException e) {
           e.printStackTrace();
       } catch (MalformedURLException e) {
           e.printStackTrace();
       } catch (ProtocolException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       finally
       {
           //close the connection, set all objects to null
           if (connection!=null)connection.disconnect();
           rd = null;
           sb = null;
           wr = null;
           connection = null;
       }
    return result;
}
static String getName(String videoId, String tokenID) {
       String result = "";
       
       
           HttpURLConnection connection = null;
           OutputStreamWriter wr = null;
           BufferedReader rd  = null;
           StringBuilder sb = null;
           String line = null;
           URL serverAddress = null;
           
           try {
               
               serverAddress = new URL("http://api.brightcove.com/services/library?command=find_video_by_id&video_id="+videoId+"&video_fields=name,length&token="+tokenID);
               //set up out communications stuff
               connection = null;
              
               //Set up the initial connection
               connection = (HttpURLConnection)serverAddress.openConnection();
               connection.setRequestMethod("GET");
               connection.setDoOutput(true);
               connection.setReadTimeout(10000);
                         
               connection.connect();
              
               rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               sb = new StringBuilder();
              
               while ((line = rd.readLine()) != null)
               {
                   sb.append(line + '\n');
               }
               
               JSONObject js = new JSONObject(sb.toString());
                
               result = js.getString("name");
              
               
           } catch (JSONException e) {
               e.printStackTrace();
           } catch (MalformedURLException e) {
               e.printStackTrace();
           } catch (ProtocolException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           }
           finally
           {
               //close the connection, set all objects to null
               connection.disconnect();
               rd = null;
               sb = null;
               wr = null;
               connection = null;
           }
        return result;
    }
static String getList(String token, String params, Boolean exportCSV) {
  
    String result = "";

    HttpURLConnection connection = null;
    OutputStreamWriter wr = null;
    BufferedReader rd  = null;
    StringBuilder sb = null;
    String line = null;
    URL serverAddress = null;
    JSONObject js =null;
    try {            
      
        java.util.Map<String, JSONObject> sortedjson = new HashMap();
        
        int pageNumber = 0;
        double totalPages = 0;
        
        while(pageNumber <= totalPages){
        
            serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields="+params+"&get_item_count=true&page_number="+pageNumber+"&token="+token);
            //set up out communications stuff
            connection = null;
           
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
                        
            js = new JSONObject(sb.toString());
            
            totalPages = Math.floor(js.getInt("total_count")/100);
            
            JSONArray jsa = new JSONArray(js.get("items").toString());
            for (int i = 0; i < jsa.length(); i++) {
                JSONObject row = jsa.getJSONObject(i);
                sortedjson.put(row.getString("id"), row);
            }
        
            pageNumber ++;
        }
        
        if(exportCSV){
             JSONObject tempJSON;
             String csvString = "\"Video Name\",\"Video ID\"\r\n";    
     
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                tempJSON =  sortedjson.get(key);
                csvString += "\""+tempJSON.getString("name") + "\",\""+tempJSON.getString("id") + "\"\r\n";
            }
            result = csvString;
        }
        else{
            JSONObject jsTotal = new JSONObject();
            
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                jsTotal.accumulate("items", sortedjson.get(key));
            }
            jsTotal.put("totals", js.getInt("total_count"));
            result = jsTotal.toString();
        }
       
    } catch (JSONException e) {
        e.printStackTrace();
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    finally
    {
        //close the connection, set all objects to null
        connection.disconnect();
        rd = null;
        sb = null;
        wr = null;
        connection = null;
    }
     return result;
 }
static String getList(String token, String params, Boolean exportCSV, String start, String limit, String query) {
	  
	String result = "";
    
    HttpURLConnection connection = null;
    OutputStreamWriter wr = null;
    BufferedReader rd  = null;
    StringBuilder sb = null;
    String line = null;
    URL serverAddress = null;
    JSONObject jsTotal = new JSONObject();
    try {
        
       java.util.Map<String, JSONObject> sortedjson = new HashMap();
        
        int pageNumber = 0;
        int firstElement=0;
        if (start!= null && !start.trim().isEmpty() && Integer.parseInt(start)>0) {
        	firstElement = Integer.parseInt(start);
        	if (limit!= null && !limit.trim().isEmpty()) pageNumber = (firstElement+Integer.parseInt(limit))/20;
            
        }
        int totalPages = 0;
        
        	if (query != null && !query.trim().isEmpty()) {
        		serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&all=display_name:="+query.trim()+"&video_fields=" + params + "&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
                	
        	}else{
        	    serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields=" + params + "&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
        	}
        	//set up out communications stuff
            connection = null;
           
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
            
             
            JSONObject js = new JSONObject(sb.toString());
            totalPages = js.getInt("total_count");
            
            if (firstElement<totalPages) {
            	jsTotal.put("items",js.get("items"));
            	jsTotal.put("results", js.getInt("total_count"));
            }else{
                jsTotal = new JSONObject("{\"items\":[],\"results\":0}");
            }

        
        
        
        result = jsTotal.toString();
        
        
    } catch (JSONException e) {
        e.printStackTrace();
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    finally
    {
        //close the connection, set all objects to null
        connection.disconnect();
        rd = null;
        sb = null;
        wr = null;
        connection = null;
    }
 return result;
 }
 
static String getListSideMenu(String token, String limit) {
    String result = "";
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        JSONObject jsTotal = new JSONObject();
        try {
            
           java.util.Map<String, JSONObject> sortedjson = new HashMap();
            
            int pageNumber = 0;
            int firstElement=0;
            if (limit!= null && !limit.trim().isEmpty() && limit.split("\\.\\.")[0] != null) {
            	pageNumber = Integer.parseInt(limit.split("\\.\\.")[0])/20;
            	firstElement = Integer.parseInt(limit.split("\\.\\.")[0]);
            }
            int totalPages = 0;
            
                serverAddress = new URL("http://api.brightcove.com/services/library?command=search_videos&sort_by=DISPLAY_NAME&video_fields=name,id,thumbnailURL&get_item_count=true&page_size=20&page_number="+pageNumber+"&token="+token);
                //set up out communications stuff
                connection = null;
               
                //Set up the initial connection
                connection = (HttpURLConnection)serverAddress.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setReadTimeout(10000);
                          
                connection.connect();
               
                rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sb = new StringBuilder();
               
                while ((line = rd.readLine()) != null)
                {
                    sb.append(line + '\n');
                }
                
                 
                JSONObject js = new JSONObject(sb.toString());
                totalPages = js.getInt("total_count");
                if (firstElement<totalPages) {
	                JSONArray jsa = new JSONArray(js.get("items").toString());
	                for (int i = 0; i < jsa.length(); i++) {
	                    JSONObject row = jsa.getJSONObject(i);
	                    sortedjson.put(row.getString("id"), row);
	                }
	                
	               
	                for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
		                String key = (String) i.next();
		                jsTotal.accumulate("items", sortedjson.get(key));
		            } 
	                jsTotal.put("results", js.getInt("total_count"));
                }else{
                	jsTotal = new JSONObject("{\"items\":[],\"results\":0}");
                }
                
            
            
            
            result = jsTotal.toString().replace("\"id\"", "\"videoid\"").replaceAll("\"videoid\":([0-9]*)", "\"path\":\"$1\"");
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return result;
 }

//Returns JSON of the video information based on a comma separated string of their ids.
static JSONArray getVideosJsonByIds(String videoIds, String videoProperties, String tokenID) {
    JSONArray jsa = new JSONArray();
    
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        
        try {
            
            serverAddress = new URL("http://api.brightcove.com/services/library?command=find_videos_by_ids&video_ids="+videoIds+"&video_fields="+videoProperties+"&token="+tokenID);
            
            //set up connection
            connection = null;
            
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
                      
            connection.connect();
           
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
           
            while ((line = rd.readLine()) != null)
            {
                sb.append(line + '\n');
            }
            
           
            JSONObject js = new JSONObject(sb.toString());
            jsa = new JSONArray(js.get("items").toString());
            
            
            
             
           
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return jsa;
 }
 
 
 //FindAllPlaylists(String readToken, Integer pageSize, Integer pageNumber, SortByTypeEnum sortBy, SortOrderTypeEnum sortOrderType, EnumSet<VideoFieldEnum> videoFields, Set<String> customFields, EnumSet<PlaylistFieldEnum> playlistFields)
static String getListPlaylistsSideMenu(String token) {
    String result = "";
    
    
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
        URL serverAddress = null;
        
        try {
            
           java.util.Map<String, JSONObject> sortedjson = new HashMap();
            
            int pageNumber = 0;
            double totalPages = 0;
            
            while(pageNumber <= totalPages){
                serverAddress = new URL("http://api.brightcove.com/services/library?command=find_all_playlists&playlist_fields=name,id,thumbnailURL&get_item_count=true&page_number="+pageNumber+"&token="+token);
                //set up out communications stuff
                connection = null;
               
                //Set up the initial connection
                connection = (HttpURLConnection)serverAddress.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setReadTimeout(10000);
                          
                connection.connect();
               
                rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sb = new StringBuilder();
               
                while ((line = rd.readLine()) != null)
                {
                    sb.append(line + '\n');
                }
                
                 
                JSONObject js = new JSONObject(sb.toString());
                totalPages = Math.floor(js.getInt("total_count")/100);
                JSONArray jsa = new JSONArray(js.get("items").toString());
                for (int i = 0; i < jsa.length(); i++) {
                    JSONObject row = jsa.getJSONObject(i);
                    sortedjson.put(row.getString("id"), row);
                }
                pageNumber ++;
                
            }
            
            JSONObject jsTotal = new JSONObject();
            for (Iterator i = sortByValue(sortedjson).iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            jsTotal.accumulate("items", sortedjson.get(key));
        } 
            
            result = jsTotal.toString().replaceAll("\"id\":([0-9]*)", "\"path\":\"$1\"").replaceAll("\"thumbnailURL\":null", "\"thumbnailURL\":\"#\"");
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
     return result;
}
%>