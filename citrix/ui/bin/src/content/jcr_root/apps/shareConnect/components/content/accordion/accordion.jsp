<%@include file="/apps/citrixosd/global.jsp"%>

<div>
	<div>
	    <cq:include path="accordiontitle" resourceType="foundation/components/parsys"/>
	</div>
	<div class="accordian-container ${(isPreviewMode || isDisabledMode || isPublishInstance) ? 'active' : 'inactive'}">
	    <cq:include path="accordionpar" resourceType="citrixosd/components/content/accordion/accordionparsys" />
	</div>
</div>