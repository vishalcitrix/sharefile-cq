<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

Sitemap Nav List component

--%>
<%@page import="java.util.List"%>
<%@ page import="com.day.cq.wcm.api.WCMMode"%>
<%@ page import="com.day.cq.wcm.api.components.DropTarget"%>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@ page import="com.day.cq.wcm.api.Page"%>
<%@ page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Arrays"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.ArrayList"%>                   
                   <%
%><%@include file="/libs/foundation/global.jsp"%>
<%

    WCMMode mode = WCMMode.fromRequest(request);
    String listItemClassGlobal = null;
    String MAIN_TITLE = "mainTitle";
    String MAIN_LINK = "mainLink";
    String MAIN_LINK_OPTION = "mainLinkOption";
%>
<%!
    private final static String ALL_LINKS_PROPERTY = "allPaths";
    
    public class AllPath {
        private String path;
        private String navTitle;
        private String linkLabel;
        private String linkOption;
    
        public AllPath() {
        }
        public void setLinkLabel(String linkLabel) {
            this.linkLabel = linkLabel;
        }
        public String getLinkLabel() {
            return this.linkLabel;
        }
        public void setPath(String path) {
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setNavTitle(String navTitle) {
            this.navTitle = navTitle;
        }
        public String getNavTitle() {
            return this.navTitle;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }        
    }
    
    public List<AllPath> getAllPaths(NodeIterator nodeIter, Resource resource) throws RepositoryException {
        final List<AllPath> allPaths = new ArrayList<AllPath>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currAllPath = nodeIter.nextNode();
                    final AllPath allPath = new AllPath();
                 
                    allPath.setPath(currAllPath.hasProperty("path") ? currAllPath.getProperty("path").getString() : "");
                    allPath.setLinkLabel(currAllPath.hasProperty("linkLabel") ? currAllPath.getProperty("linkLabel").getString() : null);
                    allPath.setLinkOption(currAllPath.hasProperty("linkOption") ? currAllPath.getProperty("linkOption").getString() : "");

                    if(resource != null && allPath.getPath() != null && allPath.getLinkLabel() == null) {
                        final Resource currentResource = resource.getResourceResolver().getResource(allPath.getPath());
                        if(currentResource != null) {
                            final Page currentPage = currentResource.adaptTo(Page.class);
                            allPath.setLinkLabel(currentPage.getNavigationTitle() != null ? currentPage.getNavigationTitle() : currentPage.getTitle());                              
                            }   
                    }
                    allPaths.add(allPath);
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return allPaths;
    }
%>
<%
    List<AllPath> allPaths = null;
    if (currentNode != null && currentNode.hasNode(ALL_LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(ALL_LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        allPaths = getAllPaths(nodeIter, resource);
    }

    final Map<String, Object> siteMapNavList = new HashMap<String, Object>();
    siteMapNavList.put(ALL_LINKS_PROPERTY, allPaths);

%>
<c:set var="siteMapNavList" value="<%= siteMapNavList %>"/>
<c:set var="mainTitle" value="<%= properties.get(MAIN_TITLE, "") %>"/>
<c:set var="mainLink" value="<%= properties.get(MAIN_LINK, "") %>"/>
<c:set var="mainLinkOption" value="<%= properties.get(MAIN_LINK_OPTION, "") %>"/>
<ul class="list">
    <c:choose>
        <c:when test="${fn:length(siteMapNavList.allPaths) > 0 || not empty mainTitle}">
            <c:if test="${not empty mainTitle && not empty mainLink}">
                <c:choose>
                    <c:when test="${not empty mainLinkOption}">
                        <li><h3><a href="${mainLink}" rel="${mainLinkOption}">${mainTitle}</a></h3></li>  
                    </c:when>
                    <c:otherwise>
                            <li><h3><a href="${mainLink}">${mainTitle}</a></h3></li>                                                 
                    </c:otherwise>  
                </c:choose>         
            </c:if>
            <c:if test="${not empty mainTitle && empty mainLink}">
                <li><h3>${mainTitle}</h3></li>
            </c:if>
            <c:forEach items="${siteMapNavList.allPaths}" begin="0" var="paths">
                <c:choose>
                    <c:when test="${not empty paths.linkOption}">
                        <li><a href="${paths.path}" rel="${paths.linkOption}">${paths.linkLabel}</a></li> 
                    </c:when>
                    <c:otherwise>
                            <li><a href="${paths.path}">${paths.linkLabel}</a></li>                                     
                    </c:otherwise>  
                </c:choose>      
            </c:forEach>
        </c:when>
        <c:otherwise>
            <cq:include script="empty.jsp"/>
        </c:otherwise>
    </c:choose>
</ul>