<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%
    final String munchkinId = properties.get("munchkinId", "");
    final String formid = properties.get("formid", "");
    final String offer = properties.get("offer","");
    final String leadChannel = properties.get("leadChannel","");
    final String leadSource = properties.get("leadSource","");
    final String leadDetails = properties.get("leadDetails","");
    String salesForceId = properties.get("saleForceId","");
    final String product = properties.get("product", "");
    final String buyerPersona = properties.get("buyerPersona","");
    String thankYouPage = properties.get("thankYouPage","");

    String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath());

    //taking care of thank you page url as per context root
    ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String transformed = null;
    transformed = transformer.transform(thankYouPage);
    if (transformed != null)
        thankYouPage = transformed;

    //taking care of current page url as per context root
    transformed = null;
    String currentpage = currentPage.getPath().toString();
    transformed = transformer.transform(currentpage);
    if (transformed != null)
        currentpage = transformed;

    //using util function to display language
    final String lang = Utilities.getLocale(currentPage,request).getDisplayLanguage();
%>

<input type="hidden" name="munchkinId" value="<%= munchkinId %>">
<input type="hidden" name="formid" value="<%= formid %>">
<input type="hidden" name="Lead_Offer__c" value="<%= offer %>">
<input type="hidden" name="LeadChannel__c" value="<%= leadChannel %>">
<input type="hidden" name="Lead_Source_Most_Recent__c" value="<%= leadSource %>">
<input type="hidden" name="LeadDetails__c" value="<%= leadDetails %>">
<input type="hidden" name="Lead_Assignment__c" value="All">
<input type="hidden" name="sfdc_campaign_id" value="<%= salesForceId %>">
<input type="hidden" name="Product__c" value="<%= product %>">
<input type="hidden" name="Buyer_Persona__c" value="<%= buyerPersona %>">
<input type="hidden" name="Marketing_Landing_Page__c" value="<%= baseURL + currentpage %>">
<input type="hidden" name="Thank_You_Page__c" value="<%= baseURL + thankYouPage %>">
<input type="hidden" name="returnURL" value="<%= baseURL + thankYouPage %>">
<input type="hidden" name="_mkt_trk" value="">
<input type="hidden" name="lpId" value="-1">
<input type="hidden" name="returnLPId" value="-1">

