<%--
  Twitter Feed component.
  - Displays feed from twitter account. 
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.citrixosd.service.social.twitter.TwitterHelper" %>
<%@ page import="java.util.List" %>

<%
	int noOfTweets= properties.get("noOfTweets", 3);
	String twitterAccountType= properties.get("accountType", "g2m");
	
	TwitterHelper  twitterHelper = new TwitterHelper();
	twitterHelper.initTwitterCredentialsConfig(twitterAccountType);
	List<String> tweets= twitterHelper.getFormatedStatus(noOfTweets);
%>

<c:set var="tweets" value="<%= tweets %>"/>

<div id="twitter-feed">
	<c:forEach items="${tweets}" var="tweet" varStatus="i">
		<div class="feed-item">
		   <div class="image-container">
			   <div class="image-feed"></div>
		   </div>
		   <div class="item">
				<p>${tweet}</p>
		   </div>
		</div>
	</c:forEach>
</div>