<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final boolean collabration = properties.get("collabration", false);
%>   

<c:set var="collabration" value="<%= collabration %>"/> 
<c:set var="prod" value="<%= request.getParameter("prod") != null ? request.getParameter("prod") : properties.get("manualProduct", null) %>"/>

<c:if test="${collabration}">
    <ol>
        <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2m"/>" <c:if test="${prod == 'g2m'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.g2m"/></li>
        <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2w"/>" <c:if test="${prod == 'g2w'}">checked</c:if> onclick="unCheck(this)" alt="G2W - Marketing"> <fmt:message key="contactSales.needs.g2w"/></li>
        <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2t"/>" <c:if test="${prod == 'g2t'}">checked</c:if> onclick="unCheck(this)" alt="G2T - Training"> <fmt:message key="contactSales.needs.g2t"/></li>
        <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.hidef"/>" <c:if test="${prod == 'audio'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.hdc"/></li>
        <li><input type="checkbox" name="Products" value="G2M & G2T & G2W" onClick="checkAll(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.all"/></li>
    </ol>
</c:if>