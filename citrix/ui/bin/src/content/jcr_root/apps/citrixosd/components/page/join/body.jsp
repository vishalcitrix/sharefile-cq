<%@include file="/apps/citrixosd/global.jsp"%>

 <body>

    <cq:include script="header.jsp" />
    
    <div id="join" class="content">
    
        <div class="content-body container">
        
            <cq:include path="mainContent" resourceType="foundation/components/parsys"/>

        </div>
        
    </div>

    <cq:include script="footer.jsp" />
    
</body>