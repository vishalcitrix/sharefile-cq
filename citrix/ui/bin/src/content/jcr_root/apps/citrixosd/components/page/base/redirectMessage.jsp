<%@include file="/libs/foundation/global.jsp" %>
<c:set var="errorMessage" value="<%=request.getAttribute("redirectErrorMessage")%>"/>
<c:set var="redirectLocation" value="<%=request.getAttribute("redirectLocation")%>"/>
<style>
    .redirect-text {
        background-color: #fff;
        padding: 10px;
        font: 14px;
    }
</style>
<div class="redirect-text">
    <p>This page redirects to <a href="${redirectLocation}">${redirectLocation}</a>.</p>
    <c:if test="${not empty errorMessage}">
    <p style="color: red;">${errorMessage}</p>
    </c:if>
</div>