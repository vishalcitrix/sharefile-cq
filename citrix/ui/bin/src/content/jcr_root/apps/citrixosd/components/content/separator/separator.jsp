<%--
    Separator Component
    
    Creates an <div> tag with border bottom, adjustable width and alignment options.

    ingrid.tseng@citrix.com
    vishal.gupta.82@citrix.com

--%>

<%@include file="/libs/foundation/global.jsp" %>

<c:set var="alignmentCss" value="float : ${properties['alignment']}" />
<div class="hr" style="width : ${not empty properties['width'] ? properties['width'] : '100%'}; ${fn:contains(alignmentCss, 'left') || fn:contains(alignmentCss, 'right') ? alignmentCss : 'margin-left : auto; margin-right : auto;'}"></div>
<c:if test="${fn:contains(alignmentCss, 'left') || fn:contains(alignmentCss, 'right')}">
    <div style="clear:both"></div>
</c:if>