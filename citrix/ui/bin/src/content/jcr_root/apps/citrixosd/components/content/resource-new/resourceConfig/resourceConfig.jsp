<%--
    Resource Container
    //fixed code the way serlvet was called, no more dependent on context root
    //not dependent on JS to load data
    vishal.gupta@citrix.com
--%>

<%@page import="org.apache.sling.api.SlingHttpServletRequest"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.citrixosd.resources.ResourceUtils"%>
<%@page import="com.citrixosd.resources.models.ResourceCategoryeModel"%>
<%@page import="com.citrixosd.enums.ResourceType"%>
<%@page import="java.util.Properties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="javax.jcr.Value"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.citrixosd.utils.TranslationUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	final String title = properties.get("title", "");
	final String subtitle = properties.get("subtitle", "");
	
	//Resource type
	final String[] resourceTypes = properties.get("resourceType", String[].class);
	final Tag resourceTypeTag = Utilities.getFirstTag(resourceTypes, tagManager);
	
	//Categories
	final String[] categories = properties.get("categories", String[].class);
	final List<Tag> categoryTags = Utilities.getTags(categories, tagManager);
	final StringBuilder builder = new StringBuilder();
	for(int i = 0; i < categoryTags.size(); i++) {
		final Tag categoryTag = categoryTags.get(i);
		builder.append(categoryTag.getTagID());
		builder.append(i == categoryTags.size() - 1 ? "" : ",");
	}
	final String categoriesStr = builder.toString();
	
	final String[] defaultLanguage = properties.get("defaultLanguage", String[].class);
	final Tag defaultLanguageTag = Utilities.getFirstTag(defaultLanguage, tagManager);
	
	final String timeZone = properties.get("timeZone", "PST");
	final int limit = properties.get("limit", 0);
	final boolean webinarUpcomingOnly = properties.get("upcomingOnly", false);
	final Locale locale = Utilities.getLocale(currentPage,request);
%>

<%!
	public static String resourceDocument(Iterator<Node> nodeIter, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
		
		String finalHtml = "";
		
		//pagination
		final int pagination = limit > 0 ? (int) Math.ceil((double)size/limit) : 1;
		limit = limit > 0 ? limit : size;
		
		for(int page = 1; page <= pagination; page++){
			String pageShow = page == 1 ? "" : "style='display:none'";
			finalHtml = finalHtml.concat("<ul class='document-container' data-resource='#document-container-" + page + "' " + pageShow + ">");
			for(int limitSet = 0; limitSet < limit; limitSet++){
				if(nodeIter.hasNext()) {
					String credit = null;
					String creditPath = null;
					String nodeLink = null;
					String nodeTitle = "";
					boolean isGated = false;
					
					Node currNode = nodeIter.next();
				
					isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
					nodeLink = isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath();
			
					if(currNode.hasNode("resourceContainer")){
						final Node resContainerNode = currNode.getNode("resourceContainer");
						nodeTitle = resContainerNode.hasProperty("documentTitle") ? resContainerNode.getProperty("documentTitle").getString() : null;
						credit = resContainerNode.hasProperty("credit") ? resContainerNode.getProperty("credit").getString() : null;
						creditPath = resContainerNode.hasProperty("creditPath") ? resContainerNode.getProperty("creditPath").getString() : null;
					}
			
					//creating output
					if(limitSet % 2 == 0){
						finalHtml = finalHtml.concat("<li class='even'>");
					}else {
						finalHtml = finalHtml.concat("<li class='odd'>");
					}
			
					finalHtml = finalHtml.concat("<div class='title'>");
						if(isGated){
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>" + nodeTitle + "</a>");
						}else {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>" + nodeTitle + "</a>");
						}
					finalHtml = finalHtml.concat("</div>");
		
					finalHtml = finalHtml.concat("<div class='credit'>");
						if(credit != null) {
							if(creditPath != null){
								finalHtml = finalHtml.concat("<a href='" + creditPath + "' target='_blank'>" + credit + "</a>");
							}else {
								finalHtml = finalHtml.concat(credit);
							}
						}
					finalHtml = finalHtml.concat("</div>");
		
					if(isGated){
						finalHtml = finalHtml.concat("<div class='gated'><span class='lock'></span></div>");
					}
		
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
					finalHtml = finalHtml.concat("</li>");
				}
			}
			finalHtml = finalHtml.concat("</ul>");	
		}
		
		if(limit != size){
			finalHtml = finalHtml.concat("<div class='pagination' style='display:block;'>");
				finalHtml = finalHtml.concat("<div class='pagination-border'>");
					finalHtml = finalHtml.concat("<ul class='pagination-container'>");
						for(int page = 1; page <= pagination; page++){
							finalHtml = finalHtml.concat("<li><a href='#document-container-" + page + "'>" + page + "</a></li>");
						}
					finalHtml = finalHtml.concat("</ul>");
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</div>");
		}
		
		return finalHtml;
	}
	
	public static String resourceWebinar(Iterator<Node> nodeIter, boolean webinarUpcomingOnly, TagManager tagManager, Locale locale, String timeZone, SlingHttpServletRequest slingRequest, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
	
		String finalHtml = "";
		
		final DateFormat longDateFormatter = DateFormat.getDateInstance(DateFormat.LONG, locale);
		final DateFormat shortTimeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		
		//Set the time zone for the dates
		if(timeZone != null && timeZone.trim().length() > 0) {
			longDateFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
			shortTimeFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
		}
		
		//pagination
		final int pagination = limit > 0 ? (int) Math.ceil((double)size/limit) : 1;
		limit = limit > 0 ? limit : size;

		for(int page = 1; page <= pagination; page++) {
			String pageShow = page == 1 ? "":"style='display:none'";
			finalHtml = finalHtml.concat("<ul class='webinar-container' data-resource='#webinar-container-" + page + "' " + pageShow + ">");
			for(int limitSet = 0; limitSet < limit; limitSet++){
				if(nodeIter.hasNext()) {
					String nodeLink = null;
					String nodeTitle = "";
					String fileReference = "";
					String authorTitle = null;
					String authorDescription = null;
					String upcoming = null;
					String upcomingTime = null;
					boolean isGated = false;
					
					Node currNode = nodeIter.next();
					isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
					
					if(currNode.hasNode("resourceContainer")){
						final Node resContainerNode = currNode.getNode("resourceContainer");
						
						nodeTitle = resContainerNode.hasProperty("videoTitleOverride") ? resContainerNode.getProperty("videoTitleOverride").getString() : resContainerNode.hasProperty("videoTitle") ? resContainerNode.getProperty("videoTitle").getString() : null;
						fileReference = resContainerNode.hasProperty("fileReference") ? resContainerNode.hasProperty("rendition") ? resContainerNode.getProperty("rendition").getString() : resContainerNode.getProperty("fileReference").getString() : resContainerNode.hasProperty("videoThumbnailUrl") ? resContainerNode.getProperty("videoThumbnailUrl").getString(): null; 
						authorTitle = resContainerNode.hasProperty("authorTitle") ? resContainerNode.getProperty("authorTitle").getString() : null;
						authorDescription = resContainerNode.hasProperty("authorDescription") ? resContainerNode.getProperty("authorDescription").getString() : null;
						
						if(resContainerNode.hasProperty("webinarStartDate")) {
				    		final Date currentDate = new Date();
				    		final Date webinarDate = resContainerNode.getProperty("webinarStartDate").getDate().getTime();
				    		if(currentDate.before(webinarDate) || currentDate.equals(webinarDate)) {
				    			nodeLink = resContainerNode.hasProperty("registrationPath") ? resContainerNode.getProperty("registrationPath").getString() : null;
				    		}
						}
						
						if(webinarUpcomingOnly) {
		                    Date upcomingDate = resContainerNode.getProperty("webinarStartDate").getDate().getTime();
		                    upcoming = resContainerNode.hasProperty("webinarStartDate") ? longDateFormatter.format(upcomingDate) : null;
		                    upcomingTime = resContainerNode.hasProperty("webinarStartDate") ? shortTimeFormatter.format(upcomingDate) + " (" + shortTimeFormatter.getTimeZone().getDisplayName(shortTimeFormatter.getTimeZone().inDaylightTime(upcomingDate), TimeZone.SHORT) + ")" : null;
				    	}
					}
					
					nodeLink = nodeLink != null ? nodeLink : isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath();
					
					//get Categories
					final ResourceBundle resourceBundle = slingRequest.getResourceBundle(locale);							
					final String[] categories = TranslationUtil.translateArray(Utilities.getStringArrayFromArrayProperty(currNode, "resourceCategories"), resourceBundle);
				
					//creating output
					if(limitSet % 2 == 0){
						finalHtml = finalHtml.concat("<li class='even'>");
					}else {
						finalHtml = finalHtml.concat("<li class='odd'>");
					}
						finalHtml = finalHtml.concat("<div class='author-container'>");
							finalHtml = finalHtml.concat("<div class='author-thumbnail-container'>");
								finalHtml = finalHtml.concat("<img src='" + fileReference + "'/>");
							finalHtml = finalHtml.concat("</div>");
								
							finalHtml = finalHtml.concat("<div class='author-description-container'>");
								finalHtml = finalHtml.concat("<div class='title'>");
									if(isGated){
										finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>" + nodeTitle + "</a>");
									}else {
										finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>" + nodeTitle + "</a>");
									}
									
									if(authorTitle != null) {
										finalHtml = finalHtml.concat("<div class='author-title'><strong>" + authorTitle + "</strong></div>");	
									}
									
									if(authorDescription != null) {
										finalHtml = finalHtml.concat("<div class='author-description ellipsis'>" + authorDescription + "</div>");	
									}
								finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
						finalHtml = finalHtml.concat("</div>");
						
						finalHtml = finalHtml.concat("<div class='categories-container'><div class='categories-container-margin'>");
							if(upcoming != null){
								finalHtml = finalHtml.concat("<div>");
								finalHtml = finalHtml.concat("<strong>" + upcoming + "</strong>");
								if(upcomingTime != null) {
									finalHtml = finalHtml.concat("<br/>");
									finalHtml = finalHtml.concat("<strong>" + upcomingTime + "</strong>");
								}
								finalHtml = finalHtml.concat("</div>");
							}
							
							if(categories != null && categories.length > 0) {
								for(int j = 0; j < categories.length; j++) {
									finalHtml = finalHtml.concat("<div>" + categories[j] + "</div>");
								}
							}
							finalHtml = finalHtml.concat("</div>");
							finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
					finalHtml = finalHtml.concat("</li>");
				}
			}
			finalHtml = finalHtml.concat("</ul>");
		}
		
		if(limit != size){
			finalHtml = finalHtml.concat("<div class='pagination' style='display:block;'>");
				finalHtml = finalHtml.concat("<div class='pagination-border'>");
					finalHtml = finalHtml.concat("<ul class='pagination-container'>");
						for(int page = 1; page <= pagination; page++){
							finalHtml = finalHtml.concat("<li><a href='#webinar-container-" + page + "'>" + page + "</a></li>");
						}
					finalHtml = finalHtml.concat("</ul>");
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</div>");
		}
		
		return finalHtml;
	}
	
	public static String resourceVideo(Iterator<Node> nodeIter, int size, int limit) throws ValueFormatException, PathNotFoundException, RepositoryException{
		
		int i = 0;
		String finalHtml = "<ul class='video-container'>";
		
		while(nodeIter.hasNext()) {
			if(i %3 == 0){
				if(i > 0){
					finalHtml = finalHtml.concat("</ul></li>");
				}
				finalHtml = finalHtml.concat("<li><ul class='video-row'>");
			}
			
			Node currNode = nodeIter.next();
			String nodeLink = "";
			String nodeTitle = "";
			String thumbnailPath = "";
			String videoLength = "";
			boolean isGated = false;
			
			isGated = currNode.hasProperty("resourceGatedPath") ? currNode.hasProperty("resourceGatedTill") ? currNode.getProperty("resourceGatedTill").getDate().getTime().after(new Date()) ? true : false : true : false;
			nodeLink = isGated && currNode.hasProperty("resourceGatedPath") ? currNode.getProperty("resourceGatedPath").getString() : currNode.getParent().getPath(); 
					
			if(currNode.hasNode("resourceContainer")){
				final Node resContainerNode = currNode.getNode("resourceContainer");
				
				nodeTitle = resContainerNode.hasProperty("videoTitleOverride") ? resContainerNode.getProperty("videoTitleOverride").getString() : resContainerNode.hasProperty("videoTitle") ? resContainerNode.getProperty("videoTitle").getString() : null;
				thumbnailPath = resContainerNode.hasProperty("fileReference") ? resContainerNode.hasProperty("rendition") ? resContainerNode.getProperty("rendition").getString() : resContainerNode.getProperty("fileReference").getString() : resContainerNode.hasProperty("videoThumbnailUrl") ? resContainerNode.getProperty("videoThumbnailUrl").getString(): null;
				videoLength = resContainerNode.hasProperty("videoHumanLength") ? resContainerNode.getProperty("videoHumanLength").getString() : null;
			}
			
			finalHtml = finalHtml.concat("<li>");
				if(isGated){
					finalHtml = finalHtml.concat("<a href='" + nodeLink + "' target='_blank'>");
				}else {
					finalHtml = finalHtml.concat("<a href='" + nodeLink + "'>");
				}
				
						finalHtml = finalHtml.concat("<div class='thumbnail-container'>");
							finalHtml = finalHtml.concat("<img src='" + thumbnailPath + "'/>");
							finalHtml = finalHtml.concat("<div class='video-play-sprite'></div>");
							finalHtml = finalHtml.concat("<div class='video-duration'>" + videoLength + "</div>");
						finalHtml = finalHtml.concat("</div>");
					finalHtml = finalHtml.concat("</a>");
				
					finalHtml = finalHtml.concat("<div class='title-container'>");
					finalHtml = finalHtml.concat("<span class='small-right-arrow-orange'></span>");
						if(isGated) {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' class='video-title ellipsis' target='_blank'>" + nodeTitle + "</a>");
						}else {
							finalHtml = finalHtml.concat("<a href='" + nodeLink + "' class='video-title ellipsis'>" + nodeTitle + "</a>");
						}
					finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
				finalHtml = finalHtml.concat("</div>");
			finalHtml = finalHtml.concat("</li>");
			i++;
		}
	
		finalHtml = finalHtml.concat("</ul>");
		finalHtml = finalHtml.concat("<div class='clearBoth'></div>");
		return finalHtml;
	}
%>

<c:set var="title" value="<%= title %>"/>
<c:set var="subtitle" value="<%= subtitle %>"/>
<c:set var="resourceTypeTag" value="<%= resourceTypeTag %>"/>

<div class="resourceCategories">
	<div class="${resourceTypeTag.name}">
		<c:if test="${not empty title}">
			<div class="resource-categories-header">
				<div class="resource-categories-title">
   					${title}
				</div>
				<div class="resource-categories-subtitle">
					${subtitle}
				</div>
				<div class="clearBoth"></div>
				
				<div class="resource-category-title-arrow-container">
					<span class="resource-category-title-arrow"></span>
				</div>
				
				<div class="resource-category-subtitle-arrow-container">
					<span class="resource-category-title-arrow"></span>
				</div>
			</div>
		</c:if>
		
		<!-- Displaying Resources -->
		<%
			if(resourceTypeTag != null && defaultLanguageTag != null) {	
				//ResourceTypeModel is a model in core/resources
				ResourceCategoryeModel resourcesSet = new ResourceCategoryeModel();
		
				resourcesSet.setResourceType(resourceTypeTag.getName());
				resourcesSet.setCategories(categoriesStr);
				resourcesSet.setDefaultLanguage(defaultLanguageTag.getTagID());
				resourcesSet.setUpComingOnly(webinarUpcomingOnly);
				
			    //call Resources Utilities
			    List<Node> nodeList = ResourceUtils.getAllResources(resourcesSet, slingRequest);
			   	
			    ResourceType rt = ResourceType.getEnum(resourceTypeTag.getName());
			    
			    if(rt != null && nodeList.size() > 0) {
			    	Iterator<Node> nodeIter = nodeList.iterator();
			    	String finalHtml = "";
					switch(rt) {
						case ResourceDocument:
							finalHtml = resourceDocument(nodeIter, nodeList.size(), limit);
							break;
						case ResourceWebinar:
							finalHtml = resourceWebinar(nodeIter,webinarUpcomingOnly, tagManager, locale, timeZone, slingRequest, nodeList.size(), limit);
							break;
						case ResourceVideo:
							finalHtml = resourceVideo(nodeIter, nodeList.size(), limit);
							break;
						default:
							System.out.println("Resource is not defined");
							break;
					}
					out.println(finalHtml);
				}else {
					out.println("No Resources Found");
				}		    
			}
		%>
	</div>
</div>
