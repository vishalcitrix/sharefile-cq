<%--
    Case Study Filter
  
	The 'Case Study Filter' is a container designated for encasing 'Case Study Containers'. It consists of a drop-down list and 
	parsys. The text content of this Component is managed by two fields.
	
	1) The 'Filter Text' field displays text above the drop-down list.
	2) The 'Default Selection Text' Displays the default text for the drop-down list. 
	(Selecting this option will show all Case Study Containers)
	
	** FILTERING **
	This component contains a drop-down list that is populated by content in the Case Study Containers within this component, 
	specifically Tag Titles. Choosing a category from the drop-down list will SHOW the Case Study Container with the designated 
	title and HIDE all others.
	
	This component retrieves a "tag" property from the child nodes of the parsys this is used to create a list of the Tags.
	The Tag Titles populate the drop-down. The 'Case Study Container' components within this component must have distinct tags. No duplicate
	tags are allowed.

	kguamanquispe@siteworx.com
  
--%>

<%@page import="java.util.Arrays"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<%
	TagManager tagManager = resource.getResourceResolver().adaptTo(TagManager.class);
	Node parsys = currentNode.hasNode("parsys") ? currentNode.getNode("parsys") : null;
	ArrayList<Tag> categories = new ArrayList<Tag>();
	
	if (parsys != null) {
		NodeIterator in = parsys.getNodes();
		while (in.hasNext()) {
			Node node = in.nextNode();
			Value[] prop = node.hasProperty("tag") ? node.getProperty("tag").getValues() : null;
			Tag tag = tagManager.resolve(prop != null && prop.length > 0 ? prop[0].getString() : null);
			
/* 	Adds tag to the ArrayList(categories) of tags. If the categories contains tag it will delete the Case Study Container Node 
	that contains the tag. 
	***** This is done to prevent DUPLICATE tags ************
*/
			if (tag != null) {
				if(categories.contains(tag)){
					node.setProperty("tag", new String[]{ });
				}else{
					categories.add(tag);
				}
			}
		}
	}

%>

<div>
	<div class="category-selection">
		<c:if test="${not empty properties.message}"><h4>${properties.message}</h4></c:if>
		<select id="use-case">
			<option value="viewAll"><fmt:message key="${not empty properties.defaultSelection ? properties.defaultSelection : 'all.industries' }"/></option>
			<%for(Tag t: categories){ %>	
			<option value="<%= t.getName() %>"> <%= t.getTitle(currentPage.getLanguage(false)) %></option>
			<% } %>
		</select>
	</div>
	<div id="case-study-filter-body">
		<cq:include path="parsys" resourceType="foundation/components/parsys" />
	</div>
</div>