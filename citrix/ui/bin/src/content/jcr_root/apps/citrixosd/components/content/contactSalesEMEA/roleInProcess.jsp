<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select name="Purchasing_Role__c" id="Purchasing_Role__c" class="required">
    <option value=""><fmt:message key="form.option.default"/></option>
    <option value="Decision Maker"><fmt:message key="form.role.influencer"/></option>
    <option value="Influencer/Recommender"><fmt:message key="form.role.decisionmaker"/></option>
    <option value="Not involved"><fmt:message key="form.role.notinvolved"/></option>
</select>