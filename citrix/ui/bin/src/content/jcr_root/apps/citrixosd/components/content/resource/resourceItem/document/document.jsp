<%-- 
	Document (Resource)
	
	This is a resource page type that displays asset information. Document path should 
	point to an asset in the dam and the asset title is used if the document title is 
	not available. Document type is a tag which the CMS authors can add different document 
	types. If the asset is not available, this page would display a error or blank page.
	This component will also calculate the size of the asset when WCMMode is in edit.
	
	I18N Keys:
	- Document Label Credit: document.label.credit
	- Document Label Type: document.label.type
	- Document Label Return: document.label.return.text
	- Document type: document.type.{documentType}
	- Document mimeType: download.mime.{documentMimeType}
	
	achew@siteworx.com
--%>

<%@page import="com.day.cq.dam.api.Rendition"%>
<%@page import="java.io.File"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.day.cq.dam.api.Asset"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>

<%!
	private static final String RETURN_TEXT_PROPERTY = "returnText";
	private static final String RETURN_PATH_PROPERTY = "returnPath";
	private static final String DOCUMENT_PATH_PROPERTY = "documentPath";
	private static final String DOCUMENT_TYPE_PROPERTY = "documentType";
	private static final String DOCUMENT_TITLE_PROPERTY = "documentTitle";
	private static final String DOCUMENT_SIZE_PROPERTY = "documentSize";
	private static final String CREDIT_PROPERTY = "credit";
	private static final String CREDIT_PATH_PROPERTY = "creditPath";
	private static final String RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.370.480.png";
	
    /**
    * Format the filesize in KB, or MB, and or GB.
    */
	private static String calculateFileSize(long bytes) {
		final double KB = 1024;
		if(bytes < Math.pow(KB, 1)) {
			return "0 KB";
		}else {
			final double MB = Math.pow(KB, 2);
			if(bytes < MB) {
				final int kb = (int)(bytes / KB);
				return kb + " KB";
			}else {
				final double GB = Math.pow(KB, 3);
				if(bytes < GB) {
					final int mb = (int)(bytes / MB);
					return mb + " MB";
				}else {
					final int gb = (int)(bytes / GB);
					return gb + " GB";
				}
			}
		}
	}
	
    /**
    * Updates the node property if there is a change in the value for string object
    */
    private void updateNodeProperty(Node node, String parameter, String value) {
        try {
            if(node.hasProperty(parameter)) {
                if(node.getProperty(parameter).equals(value)) {
                    //Do nothing, property is correct
                }else {
                    node.setProperty(parameter, value);
                    node.save();
                }
            }else {
                node.setProperty(parameter, value);
                node.save();
            }
        }catch(RepositoryException re) {
            re.printStackTrace();
        }
    }
%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
	final String returnText = properties.get(RETURN_TEXT_PROPERTY, "document.label.return.text");
	final String returnPath = properties.get(RETURN_PATH_PROPERTY, currentPage.getParent() != null ? currentPage.getParent().getPath() : currentPage.getPath() + "#ResourceDocument");
	final String documentPath = properties.get(DOCUMENT_PATH_PROPERTY, null);
	final String documentTitle = properties.get(DOCUMENT_TITLE_PROPERTY, null);
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	final String[] documentTypes = properties.get(DOCUMENT_TYPE_PROPERTY, String[].class);
	final Tag documentTypeTag = Utilities.getFirstTag(documentTypes, tagManager);
	final String documentType = documentTypeTag != null ? documentTypeTag.getName() : null; 
	
	/**Retrieve the asset to use the asset path, mime type.*/
	Asset asset = null;
	if(documentPath != null) {
		final Resource assetResource = resource.getResourceResolver().getResource(documentPath);
		if(assetResource != null) {
			asset = assetResource.adaptTo(Asset.class);
		}
	}
	
	/** Calculate the rendition file size and store it in the jcr */
	if(WCMMode.fromRequest(request).equals(WCMMode.EDIT) && asset != null) {
		final Rendition rendition = asset.getOriginal();
		if(rendition != null) {
			final String fileSize = calculateFileSize(rendition.getSize());
			if(currentNode != null) {
				updateNodeProperty(currentNode, DOCUMENT_SIZE_PROPERTY, fileSize);
			}
		}
	}
	
	final String documentSize = properties.get(DOCUMENT_SIZE_PROPERTY, null);
	final String credit = properties.get(CREDIT_PROPERTY, null);
	final String creditPath = properties.get(CREDIT_PATH_PROPERTY, null);
	
	final Map<String, Object> document = new HashMap<String, Object>();
	document.put(RETURN_TEXT_PROPERTY, returnText);
	document.put(RETURN_PATH_PROPERTY, returnPath);
	document.put(DOCUMENT_PATH_PROPERTY, documentPath);
	document.put(DOCUMENT_TITLE_PROPERTY, documentTitle);
	document.put(DOCUMENT_TYPE_PROPERTY, documentType);
	document.put(DOCUMENT_SIZE_PROPERTY, documentSize);
	document.put(CREDIT_PROPERTY, credit);
	document.put(CREDIT_PATH_PROPERTY, creditPath);
	
	document.put("asset", asset);
	document.put("renditionPath", asset != null ? asset.getPath() + RENDITION_PATH: null);
%>

<c:set var="document" value="<%= document %>" />

<c:choose>
	<c:when test="${not empty document.asset}">
		<div class="document-title">
			<h2>${not empty document.documentTitle ? document.documentTitle : document.asset.metadata['dc:title']}</h2>
			<a href="${document.returnPath}"><span class="small-arrow-left"></span><fmt:message key="${document.returnText}"/></a>
		</div>
		<div class="document-file ${document.documentType}">
			<a class="document-download" href="${document.asset.path}" rel="external">
				<img class="document-thumbnail" src="${document.renditionPath}"/>
				<span class="document-download-overlay ${document.asset.mimeType}">
					<fmt:message key="download.mime.${document.asset.mimeType}"/>
					<c:if test="${not empty document.documentSize}">(${document.documentSize})</c:if>
				</span>
			</a>
		</div>
		<div class="document-description">
			<cq:include path="beforeDescription" resourceType="foundation/components/parsys"/>
			
			<c:if test="${not empty document.documentType}">
				<div class="document-metadata document-document-type"><strong><fmt:message key="document.label.type"/></strong> <fmt:message key="document.type.${document.documentType}"/></div>
			</c:if>
			
			<c:if test="${not empty document.credit || not empty document.creditPath}">
				<div class="document-metadata document-document-reference">
					<fmt:message key="document.label.credit"/>
					<c:choose>
						<c:when test="${not empty document.creditPath}">
							<a href="${document.creditPath}" rel="external">
								<c:choose>
									<c:when test="${not empty document.credit}">
										<fmt:message key="${document.credit}"/>
									</c:when>
									<c:otherwise>
										${document.creditPath}
									</c:otherwise>
								</c:choose>
							</a>
						</c:when>
						<c:otherwise>
							<fmt:message key="${document.credit}"/>	
						</c:otherwise>
					</c:choose>
				</div>
			</c:if>
			
			<div class="document-metadata">
				<a href="${document.asset.path}" rel="external">
					<fmt:message key="download.mime.${document.asset.mimeType}"/>
					<c:if test="${not empty document.documentSize}">(${document.documentSize})</c:if>
					<span class="small-arrow-right"></span>
				</a>
			</div>
			
			<cq:include path="afterDescription" resourceType="foundation/components/parsys"/>
		</div>
		
		<div class="clearBoth"></div>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${isEditMode || isReadOnlyMode}">
				<%-- Asset does not exist --%>
				<div class="warning">Document: Please select a document path</div>
			</c:when>
			<c:otherwise>
				<h1><fmt:message key="document.label.asset.error"/></h1>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>