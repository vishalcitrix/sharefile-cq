<%@include file="/libs/foundation/global.jsp" %>{"name":"./styles","features":"*","jcr:primaryType":"cq:Widget","styles":{<%
   response.setContentType("application/json");
   response.setHeader("Content-Disposition", "inline");
	if (currentNode.hasNode("style")) {
		final Node styleNode = currentNode.getNode("style");
		NodeIterator iter = styleNode.getNodes();
		while (iter.hasNext()) {
			String fontFamily = "";
			String fontSize = "";
			String lineHeight = "";
			String fontColor = "";
			String fontIcon = "";
			String fontClass = "";
			String fontName = "";
			Node styleItemNode = iter.nextNode();
			if (styleItemNode.hasProperty("family") || styleItemNode.hasProperty("size")|| styleItemNode.hasProperty("color") || styleItemNode.hasProperty("class")) {
				if(styleItemNode.hasProperty("family"))
					fontFamily = styleItemNode.getProperty("family").getString();
				if(styleItemNode.hasProperty("size"))
					fontSize = styleItemNode.getProperty("size").getString();
				if(styleItemNode.hasProperty("lineHeight"))
					lineHeight = styleItemNode.getProperty("lineHeight").getString();
				if(styleItemNode.hasProperty("color"))
					fontColor = styleItemNode.getProperty("color").getString();
				if(styleItemNode.hasProperty("icon"))
					fontIcon = styleItemNode.getProperty("icon").getString();
				if(styleItemNode.hasProperty("class"))
					fontClass = styleItemNode.getProperty("class").getString();
				if(styleItemNode.hasProperty("fontName"))
					fontName = styleItemNode.getProperty("fontName").getString();
				else
					fontName = "Lorem ipsum";
				%>"jcr:primaryType":"cq:WidgetCollection","<%= styleItemNode.getName() %>":{"jcr:primaryType":"nt:unstructured","text":"<%= fontName %>","cssName":"<%= fontFamily %> <%= fontSize %> <%= lineHeight %> <%= fontColor %> <%= fontIcon %> <%= fontClass %>"}<% if(iter.hasNext()) %>,<%
			}
		}
	}
%>}}