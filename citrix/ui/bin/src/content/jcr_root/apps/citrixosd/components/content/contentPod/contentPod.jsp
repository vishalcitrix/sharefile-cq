<%--
	Content Pod
  	
  	This should be placed on the left rail because there is a fixed image background
  	associated with the component. This will have a round border with a shadow background 
  	image. Authors will have the ability to hide the shadow
  	
	achew@siteworx.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="content-pod-container">
	<cq:include script="content.jsp"/>
</div>

<c:if test="${empty properties['hideShadow'] || properties['hideShadow'] == false}">
	<div class="content-pod-shadow">
		<div class="content-pod-shadow-left"></div>
		<div class="content-pod-shadow-right"></div>
		<div class="clearBoth"></div>
	</div>
</c:if>