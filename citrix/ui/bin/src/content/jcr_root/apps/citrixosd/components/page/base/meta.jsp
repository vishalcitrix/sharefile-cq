<%@page import="javax.jcr.Node"%>
<%@page import="javax.jcr.NodeIterator"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
	String language = "";
	final Page localePage = currentPage.getAbsoluteParent(2);
	if (localePage != null) {
		language = localePage.getName();
	}
%>
<meta http-equiv="content-language" content="<%=language %>">
<%
	if (currentNode.hasNode("metaHttp")) {
		final Node metaHttpNode = currentNode.getNode("metaHttp");
		NodeIterator iter = metaHttpNode.getNodes();
		while (iter.hasNext()) {
			Node metaItemNode = iter.nextNode();
				if (metaItemNode.hasProperty("http") && metaItemNode.hasProperty("content"))
				%><meta http-equiv="<%=metaItemNode.getProperty("http").getString() %>" content="<%=metaItemNode.getProperty("content").getString() %>">
<%
		}
	}
	if (currentNode.hasNode("meta")) {
		final Node metaNode = currentNode.getNode("meta");
		NodeIterator iter1 = metaNode.getNodes();
		while (iter1.hasNext()) {
			Node metaItemNode = iter1.nextNode();
				if (metaItemNode.hasProperty("metaName") && metaItemNode.hasProperty("metaContent"))
				%><meta name="<%=metaItemNode.getProperty("metaName").getString() %>" content="<%=metaItemNode.getProperty("metaContent").getString() %>">
<%
		}
	}
	if (currentNode.hasNode("link")) {
		final Node linkNode = currentNode.getNode("link");
		NodeIterator iter2 = linkNode.getNodes();
		while (iter2.hasNext()) {
			Node linkItemNode = iter2.nextNode();
			if (linkItemNode.hasProperty("linkRel") && linkItemNode.hasProperty("linkHref"))
			%>	<link rel="<%=linkItemNode.getProperty("linkRel").getString()%>" href="<%=linkItemNode.getProperty("linkHref").getString()%>"<c:if test="<%= linkItemNode.hasProperty("linkType") %>"> type="<%=linkItemNode.getProperty("linkType").getString()%>"</c:if><c:if test="<%= linkItemNode.hasProperty("linkHrefLang") %>"> hreflang="<%=linkItemNode.getProperty("linkHrefLang").getString()%>"</c:if><c:if test="<%= linkItemNode.hasProperty("linkMedia") %>"> media="<%=linkItemNode.getProperty("linkMedia").getString()%>"</c:if> ><%
		}
	}
%>