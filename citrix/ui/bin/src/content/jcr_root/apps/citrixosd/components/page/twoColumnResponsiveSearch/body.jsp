<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    <cq:include script="noscript.jsp"/>
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<div class="row full-width">
		    <div class="large-2 columns">
                <cq:include script="filter.jsp"/>
            </div>
		    <div class="large-10 columns">
                <cq:include script="resourceContent.jsp"/>
		    </div>
		</div>
	</div>  
    <cq:include script="footer.jsp" />
</body>
