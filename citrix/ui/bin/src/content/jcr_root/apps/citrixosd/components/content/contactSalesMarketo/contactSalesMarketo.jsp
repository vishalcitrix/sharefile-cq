<%--
  Contact Sales component.
  Lets users create contact sales form for G2PC, G2MWT, G2A
  vishal.gupta@citrix.com
--%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="java.util.Locale"%>
<%@page import="com.adobe.granite.xss.XSSAPI"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%!
    private final static String CSS_PROPERTY = "css";
    private final static String COLLABRATION_PROPERTY = "collabration";
    private final static String DEMANDBASE_PROPERTY = "useDemandBase";
    private final static String WEBSITE_PROPERTY = "website";
    private final static String MUNCHKIN_ID = "munchkinId";
    private final static String MKTO_FORM_ID = "formid";
    private final static String OFFER_PROPERTY = "offer";
    private final static String LEAD_CHANNEL_PROPERTY = "leadChannel";
    private final static String LEAD_SOURCE_PROPERTY = "leadSource";
    private final static String LEAD_DETAILS_PROPERTY = "leadDetails";
    private final static String SALE_FORCE_ID_PROPERTY = "saleForceId";
    private final static String PRODUCT_PROPERTY = "product";
    private final static String OPT_IN_PROPERTY = "optInText";
    private final static String BUTTON_COLOR = "buttonColor";
    private final static String BUYER_PERSONA_PROPERTY = "buyerPersona";
    private final static String THANK_YOU_PAGE_PROPERTY = "thankYouPage";
    private final static String JS_CODE = "jscode";
%>

<%
    final String css = properties.get(CSS_PROPERTY, "");
    final boolean collabration = properties.get(COLLABRATION_PROPERTY, false);
    final boolean useDemandBase = properties.get(DEMANDBASE_PROPERTY, false);
    final String website = properties.get(WEBSITE_PROPERTY, "GoToMyPC");
    final String munchkinId = properties.get(MUNCHKIN_ID, "");
    final String formid = properties.get(MKTO_FORM_ID, "");
    final String offer = properties.get(OFFER_PROPERTY,"");
    final String leadChannel = properties.get(LEAD_CHANNEL_PROPERTY,"");
    final String leadSource = properties.get(LEAD_SOURCE_PROPERTY,"");
    final String leadDetails = properties.get(LEAD_DETAILS_PROPERTY,"");
    final String product = properties.get(PRODUCT_PROPERTY, "");
    final String optInText = properties.get(OPT_IN_PROPERTY,"");
    final String buttonColor = properties.get(BUTTON_COLOR, "blue");
    final String buyerPersona = properties.get(BUYER_PERSONA_PROPERTY,"");
    String thankYouPage = properties.get(THANK_YOU_PAGE_PROPERTY,"");
    final String jscode = properties.get(JS_CODE,"");
    String salesForceId = properties.get(SALE_FORCE_ID_PROPERTY,"");

    final String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath());

    //taking care of thank you page url as per context root
    ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String transformed = null;
    transformed = transformer.transform(thankYouPage);
    if (transformed != null)
        thankYouPage = transformed;

    //taking care of current page url as per context root
    transformed = null;
    String currentpage = currentPage.getPath().toString();
    transformed = transformer.transform(currentpage);
    if (transformed != null)
        currentpage = transformed;

    String cookieStr = "";

    final String salesForceChannelParam = request.getParameter("c_cmp");
    final String salesForceIdParam = request.getParameter("sfid");

    if(website.equals("GoToMyPC")){
        cookieStr = "ercVisitor";
    }else{
        cookieStr = "__col_mkt_" + website;
    }

    if(salesForceChannelParam != null && salesForceChannelParam.indexOf("sf-") > -1) {
        salesForceId = salesForceChannelParam.substring(3);
    }
    if(salesForceIdParam != null) {
        salesForceId = salesForceIdParam;
    }
    //using util function to display language
    final String lang = Utilities.getLocale(currentPage,request).getDisplayLanguage();
%>

<c:set var="leadGeo" value="<%= properties.get("leadGeo", true) %>"/>
<c:set var="collabration" value="<%= collabration %>"/>
<c:set var="useDemandBase" value="<%= useDemandBase %>"/>
<c:set var="prod" value="<%= request.getParameter("prod") != null ? request.getParameter("prod") : properties.get("manualProduct", null) %>"/>
<c:set var="website" value="<%= website %>"/>
<c:set var="count" value="0" />
<c:set var="jscode" value="<%= jscode %>"/>

<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.validate.min.js"></script>
<script type="text/javascript">
    var defaultID = '<%= properties.get(SALE_FORCE_ID_PROPERTY,"") %>';
    jQuery(document).ready(function(){
    	theForm = document.forms['contactSales'];
        var mktCookie = getmktCookie('<%= cookieStr  %>');
        var sfid = '';
        if(mktCookie != '') {
            sfid = getCookieSFID(mktCookie,'<%= website  %>');
        }
        if(theForm.sfdc_campaign_id.value == defaultID && sfid !='')
            theForm.sfdc_campaign_id.value = sfid;
    });
</script>
<script type="text/javascript" src="${currentDesign.path}/js/form.js"></script>
<c:if test="${useDemandBase}">
    <c:if test="${not isEditMode}">
            <script type="text/javascript" src="http://autocomplete.demandbase.com/autocomplete/v1.7/widget.js"></script>
        </c:if>
        <script type="text/javascript" src="${currentDesign.path}/js/demandBase.js"></script>
        <script type="text/javascript" src="http://api.demandbase.com/api/v2/ip.json?key=851275e3832f80a88e468a191a65984648201358&amp;callback=DemandGen.DemandBase.processIPInfo"></script>
</c:if>
<c:if test="${not empty jscode}">
    <script type="text/javascript">
        ${jscode}
    </script>
</c:if>


<div class="contactSales <%= css %>">
    <c:choose>
        <c:when test="${collabration}">
            <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" onSubmit="contact(this,${leadGeo});" name="contactSales">
        </c:when>
        <c:otherwise>
            <form action="http://app-sj04.marketo.com/index.php/leadCapture/save" method="POST" name="contactSales">
        </c:otherwise>
    </c:choose>
        <div class="form-box">
            <input type="hidden" name="munchkinId" value="<%= munchkinId %>">
            <input type="hidden" name="formid" value="<%= formid %>">
            <input type="hidden" name="Lead_Offer__c" value="<%= offer %>">
            <input type="hidden" name="LeadChannel__c" value="<%= leadChannel %>">
            <input type="hidden" name="Lead_Source_Most_Recent__c" value="<%= leadSource %>">
            <input type="hidden" name="LeadDetails__c" value="<%= leadDetails %>">
            <input type="hidden" name="Lead_Assignment__c" value="All">
            <input type="hidden" name="sfdc_campaign_id" value="<%= salesForceId %>">
            <input type="hidden" name="email_notification__c" value="true">
            <input type="hidden" name="Product__c" value="<%= product %>">
            <input type="hidden" name="Buyer_Persona__c" value="<%= buyerPersona %>">
            <input type="hidden" name="sfdc_campaign_status" value="responded">
            <input type="hidden" name="Marketing_Landing_Page__c" value="<%= baseURL + currentpage %>">
            <input type="hidden" name="Thank_You_Page__c" value="<%= baseURL + thankYouPage %>">
            <input type="hidden" name="returnURL" value="<%= baseURL + thankYouPage %>">
            <input type="hidden" name="Language_Preference__c" value="<%= lang %>">
            <input type="hidden" name="_mkt_trk" value="">
            <h2 class="first">
                <c:set var="count" value="${count + 1}" />
                <c:out value="${count}."/>
                <c:choose>
                  <c:when test="${website eq 'GoToMeeting'}">
                    <fmt:message key="form.title.g2m.question1"/>
                  </c:when>
                  <c:when test="${website eq 'GoToManageRemoteSupport'}">
                    <fmt:message key="form.title.g2a.question1"/>
                  </c:when>
                  <c:otherwise>
                    <fmt:message key="form.title.question1"/>
                  </c:otherwise>
                </c:choose>
            </h2>
            <c:if test="${collabration}">
                <label><fmt:message key="contactSales.needs.label"/></label>
                <ol>
                    <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2m"/>" <c:if test="${prod == 'g2m'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.g2m"/></li>
                    <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2w"/>" <c:if test="${prod == 'g2w'}">checked</c:if> onclick="unCheck(this)" alt="G2W - Marketing"> <fmt:message key="contactSales.needs.g2w"/></li>
                    <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2t"/>" <c:if test="${prod == 'g2t'}">checked</c:if> onclick="unCheck(this)" alt="G2T - Training"> <fmt:message key="contactSales.needs.g2t"/></li>
                    <li><input type="checkbox" name="Products" value="<fmt:message key="form.product.hidef"/>" <c:if test="${prod == 'audio'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.hdc"/></li>
                    <li><input type="checkbox" name="Products" value="G2M & G2T & G2W" onClick="checkAll(this)" alt="G2MC - Collaboration"> <fmt:message key="contactSales.needs.all"/></li>
                </ol>
            </c:if>
            <label for="form_comments"><fmt:message key="form.label.question"/></label>
            <textarea id="form_comments" name="form_comments" class="validate" cols="42" rows="3"></textarea>

            <h2>
                <c:set var="count" value="${count + 1}" />
                <c:out value="${count}."/>
                <fmt:message key="form.title.question2"/>
            </h2>
            <ul>
                <li class="left-col">
                    <label for="FirstName"><fmt:message key="form.label.firstName"/></label>
                    <input id="FirstName" name="FirstName" type="text" class="required" title="<fmt:message key="form.validate.firstName"/>" placeholder="<fmt:message key="form.placeholder.firstName"/>">
                </li>
                <li class="right-col">
                    <label for="LastName"><fmt:message key="form.label.lastName"/></label>
                    <input id="LastName"  name="LastName" type="text" class="required" title="<fmt:message key="form.validate.lastName"/>" placeholder="<fmt:message key="form.placeholder.lastName"/>">
                </li>
                <li class="left-col">
                    <label for="Email"><fmt:message key="form.label.email"/></label>
                    <input id="Email" name="Email" type="text" class="required email" title="<fmt:message key="form.validate.email"/>">
                </li>
                <li class="right-col">
                    <label for="Phone"><fmt:message key="form.label.phone"/></label>
                    <input id="Phone" name="Phone" type="text" class="phone" title="<fmt:message key="form.validate.phone"/>">
                </li>
            </ul>


            <h2>
                <c:set var="count" value="${count + 1}" />
                <c:out value="${count}."/>
                <fmt:message key="form.title.question3"/>
            </h2>
            <label for="Company"><fmt:message key="form.label.company"/></label>
            <input id="Company" name="Company" type="text" class="required long-text">

            <ul>
                <li class="left-col">
                    <label for="Industry"><fmt:message key="form.label.industry"/></label>
                    <select id="Industry" name="Industry" class="required">
                        <option value=""><fmt:message key="form.option.default"/></option>
                        <option value="Accounting"><fmt:message key="form.option.industry.accounting"/></option>
                        <option value="Advertising/Marketing/PR"><fmt:message key="form.option.industry.advertisingMarketingPR"/></option>
                        <option value="Aerospace &amp; Defense"><fmt:message key="form.option.industry.aerospaceAndDefense"/></option>
                        <option value="Call Center Outsourcing"><fmt:message key="form.option.industry.callCenterOutsourcing"/></option>
                        <option value="Consulting"><fmt:message key="form.option.industry.consulting"/></option>
                        <option value="Education"><fmt:message key="form.option.industry.education"/></option>
                        <option value="Energy, Chemical, Utilities"><fmt:message key="form.option.industry.energyChemicalUtilities"/></option>
                        <option value="Financial Services"><fmt:message key="form.option.industry.financialService"/></option>
                        <option value="Government - Federal"><fmt:message key="form.option.industry.governmentFederal"/></option>
                        <option value="Government - State &amp; Local"><fmt:message key="form.option.industry.governmentStateAndLocal"/></option>
                        <option value="Healthcare"><fmt:message key="form.option.industry.healthcare"/></option>
                        <option value="High Tech - Hardware"><fmt:message key="form.option.industry.highTechHardware"/></option>
                        <option value="High Tech - ISP"><fmt:message key="form.option.industry.highTechISP"/></option>
                        <option value="High Tech - Software"><fmt:message key="form.option.industry.highTechSoftware"/></option>
                        <option value="High Tech - Other"><fmt:message key="form.option.industry.highTechOther"/></option>
                        <option value="Hospitality/Travel/Tourism"><fmt:message key="form.option.industry.hospitalityTravelTourism"/></option>
                        <option value="Insurance"><fmt:message key="form.option.industry.insurance"/></option>
                        <option value="Legal"><fmt:message key="form.option.industry.legal"/></option>
                        <option value="Manufacturing"><fmt:message key="form.option.industry.manufacturing"/></option>
                        <option value="Pharmaceuticals &amp; Biotechnology"><fmt:message key="form.option.industry.pharmaceuticalsAndBiotechnology"/></option>
                        <option value="Real Estate"><fmt:message key="form.option.industry.realEstate"/></option>
                        <option value="Retail"><fmt:message key="form.option.industry.retail"/></option>
                        <option value="Support Outsourcing"><fmt:message key="form.option.industry.supportOutsourcing"/></option>
                        <option value="Telecommunications"><fmt:message key="form.option.industry.telecommunications"/></option>
                        <option value="Transportation"><fmt:message key="form.option.industry.transportation"/></option>
                        <option value="VAR/Systems Integrator"><fmt:message key="form.option.industry.VARSystemIntegrator"/></option>
                        <option value="Other"><fmt:message key="form.option.industry.other"/></option>
                    </select>
                </li>
                <li class="right-col">
                    <label for="NumberofEmployees"><fmt:message key="form.label.numberOfEmployees"/></label>
                    <select name="NoofEmployees__c" id="NumberofEmployees" class="required">
                        <option value=""><fmt:message key="form.option.default"/></option>
                        <option value="1-10"><fmt:message key="form.option.numberOfEmployees.10"/></option>
                        <option value="11-20"><fmt:message key="form.option.numberOfEmployees.20"/></option>
                        <option value="21-50"><fmt:message key="form.option.numberOfEmployees.50"/></option>
                        <option value="51-100"><fmt:message key="form.option.numberOfEmployees.100"/></option>
                        <option value="101-250"><fmt:message key="form.option.numberOfEmployees.250"/></option>
                        <option value="251-500"><fmt:message key="form.option.numberOfEmployees.500"/></option>
                        <option value="501-1,000"><fmt:message key="form.option.numberOfEmployees.1000"/></option>
                        <option value="1,001-2,500"><fmt:message key="form.option.numberOfEmployees.2500"/></option>
                        <option value="2,501-5,000"><fmt:message key="form.option.numberOfEmployees.5000"/></option>
                        <option value="5,001-7,500"><fmt:message key="form.option.numberOfEmployees.7500"/></option>
                        <option value="7,501-10,000"><fmt:message key="form.option.numberOfEmployees.10000"/></option>
                        <option value="10,001+"><fmt:message key="form.option.numberOfEmployees.10001"/></option>
                    </select>
                </li>
                <li class="left-col">
                    <label for="Department__c"><fmt:message key="form.label.department"/></label>
                    <select name="Department__c" id="Department__c" class="required">
                        <option value=""><fmt:message key="form.option.default"/></option>
                        <option value="Consulting"><fmt:message key="form.option.department.consulting"/></option>
                        <option value="Customer Service"><fmt:message key="form.option.department.customerService"/></option>
                        <option value="Engineering"><fmt:message key="form.option.department.engineering"/></option>
                        <option value="Finance/Legal/Administration"><fmt:message key="form.option.department.financialLegalAdministration"/></option>
                        <option value="Human Resources"><fmt:message key="form.option.department.humanResource"/></option>
                        <option value="IT"><fmt:message key="form.option.department.it"/></option>
                        <option value="Marketing/PR"><fmt:message key="form.option.department.marketingPR"/></option>
                        <option value="Operations"><fmt:message key="form.option.department.operations"/></option>
                        <option value="Professional Services"><fmt:message key="form.option.department.professionalService"/></option>
                        <option value="Sales/Business Development"><fmt:message key="form.option.department.salesBusinessDevelopment"/></option>
                        <option value="Training"><fmt:message key="form.option.department.training"/></option>
                        <option value="Other"><fmt:message key="form.option.department.other"/></option>
                    </select>
                </li>
                <li class="right-col">
                    <label for="Country"><fmt:message key="form.label.country"/></label>
                    <select id="Country" name="Country" class="required">
                        <cq:include script="countryList.jsp"/>
                    </select>
                </li>
            </ul>
            <div class="opt-in">
                <div class="checkbox"><input type="checkbox" name="email_opt_in" value="true" checked></div>
                <div><%= optInText %></div>
            </div>
            <input type="submit" value="<fmt:message key="form.label.submit"/>" class="button <%= buttonColor %>" title="<fmt:message key="form.label.submit"/>">
        </div>
    </form>
</div>