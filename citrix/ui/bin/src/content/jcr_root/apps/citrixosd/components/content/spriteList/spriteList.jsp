<%--
    Sprite List
  
    List of sprite where authors can select from to display the functionality of a 
    service. If there is any value blank, it will not display. If text is not provided
    then there will not be any text but the icon/sprite will still have the functionality. 
    The sprite will display in a horizontal position with the order of the list of sprite 
    respectively.
  
    Dev: The sprite object holds all the data that gets transfered from the JSON object. The 
    object is then used for JSTL and EL to display the logic.
    
    - (optional)Sprite CSS
    - (optional)Sprite Link Path
    - (optional)Sprite Pop-up
    - (optional)Sprite Text
    
    - Text Bottom (default: false)
    
    Note: This component is dependent on the multi-node multifield widget.
    
    achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Arrays"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private final static String SPRITE_PROPERTY = "sprites"; 
    private final static String TEXT_BOTTOM_PROPERTY = "textBottom";
    
    public class Sprite {
        private String css;
        private String text;
        private String path;
        private String linkOption;
    
        public Sprite() {
        }
        
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) {
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<Sprite> getSprites(NodeIterator nodeIter) {
        final List<Sprite> sprites = new ArrayList<Sprite>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currSprite = nodeIter.nextNode();
                    final Sprite sprite = new Sprite();
                    sprite.setCss(currSprite.hasProperty("css") ? currSprite.getProperty("css").getString() : null);
                    sprite.setText(currSprite.hasProperty("text") ? currSprite.getProperty("text").getString() : null);
                    sprite.setPath(currSprite.hasProperty("path") ? currSprite.getProperty("path").getString() : null);
                    sprite.setLinkOption(currSprite.hasProperty("linkBehavior") ? currSprite.getProperty("linkBehavior").getString() : null);
                    if(sprite.getCss() != null || sprite.getText() != null || sprite.getPath() != null || sprite.getLinkOption() != null) {
                    	sprites.add(sprite);
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return sprites;
    }
%>

<%
    final Map<String, Object> spriteList = new HashMap<String, Object>();

    if(currentNode != null && currentNode.hasNode(SPRITE_PROPERTY)) {
        final Node baseNode = currentNode.getNode(SPRITE_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        List<Sprite> sprites = null;
        sprites = getSprites(nodeIter);

        spriteList.put(SPRITE_PROPERTY, sprites);
    }
%>

<c:set var="spriteList" value="<%= spriteList %>"/>
<c:set var="textBottom" value="<%= properties.get(TEXT_BOTTOM_PROPERTY, false) %>"/>

<c:choose>
    <c:when test="${fn:length(spriteList.sprites) > 0}">
        <ul class="spriteList">
            <c:forEach items="${spriteList.sprites}" begin="0" var="sprite">
                <li>
                    <c:choose>
                        <c:when test="${empty sprite.path}">
                            <c:if test="${not empty sprite.css}">
                                <span class="share ${sprite.css}"></span>
                            </c:if>
                            <c:choose>
                                <c:when test="${textBottom}">
                                    <c:choose>
                                        <c:when test="${not empty sprite.text}">
                                            <span class="textBottom">${sprite.text}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <%--Do not display text--%> 
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${not empty sprite.text}">
                                            <span class="text">${sprite.text}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <%--Do not display text--%> 
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${not empty sprite.linkOption && not empty sprite.text}">
                                    <a rel="${sprite.linkOption}" href="${sprite.path}" title="${sprite.text}">                            
                                        <c:if test="${not empty sprite.css}">
                                            <span class="share ${sprite.css}"></span>
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${textBottom}">
                                                <span class="textBottom">${sprite.text}</span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="text">${sprite.text}</span> 
                                            </c:otherwise>                                              
                                        </c:choose>
                                    </a>
                                </c:when>
                                <c:when test="${not empty sprite.linkOption && empty sprite.text}">
                                    <a rel="${sprite.linkOption}" href="${sprite.path}"> 
                                    <c:choose>                           
                                        <c:when test="${not empty sprite.css}">
                                            <span class="share ${sprite.css}"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <%-- Do Nothing --%>
                                        </c:otherwise>
                                    </c:choose>
                                    </a>
                                </c:when>
                                <c:when test="${empty sprite.linkOption && not empty sprite.text}">
                                    <a href="${sprite.path}" title="${sprite.text}">                            
                                        <c:if test="${not empty sprite.css}">
                                            <span class="share ${sprite.css}"></span>
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${textBottom}">
                                                <span class="textBottom">${sprite.text}</span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="text">${sprite.text}</span> 
                                            </c:otherwise>                                              
                                        </c:choose>
                                    </a>                                    
                                </c:when>
                                <c:otherwise>
                                    <a href="${sprite.path}">                            
                                        <c:when test="${not empty sprite.css}">
                                            <span class="share ${sprite.css}"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <%-- Do Nothing --%>
                                        </c:otherwise>
                                    </a>                                    
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </li>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
		<c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Sprite List: Please enter a sprite</div></c:if>
    </c:otherwise>  
</c:choose>