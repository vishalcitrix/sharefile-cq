<%--

  RadioButton component.
  RadioButton  for Custom Form
  
  vishal.gupta.82@citrix.com

--%>
<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.citrixosd.utils.Utilities"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    private final static String RADIO_PROPERTY = "radio"; 

%>

<%
    // TODO add you code here
    final String fieldId = properties.get("fieldId","");      
    final String fieldName = properties.get("fieldName","");
    
    if (editContext != null && editContext.getEditConfig() != null) {
	    if(fieldName == ""){
	          editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Radio Group - (Please select field name)"));  
	      }else{
	          editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Radio Group - (name: " + fieldName + ")"));          
	      }
	      
		editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());       
    }
    
	//using Utilities to parse structured multi field
    ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode(RADIO_PROPERTY)) {
        final Node baseNode = currentNode.getNode(RADIO_PROPERTY);
        values = Utilities.parseStructuredMultifield(baseNode);
    }
%>

<c:set var="values" value="<%= values %>"/>
<c:set var="fieldId" value="<%= fieldId %>"/>
<c:set var="fieldName" value="<%= fieldName %>"/>
  
<div class="radioGroup">
    <c:choose>
        <c:when test="${fn:length(values) > 0}">
            <c:forEach items="${values}" var="item">  
                <c:choose>
                    <c:when test="${fieldId ne ''}">
                        <input type="radio" id="${fieldId}" name="${fieldName}" value="${item.optionValue.string}" <c:if test="${not empty param[fieldName] ? param[fieldName] eq item.optionValue.string : false}">checked</c:if>>${item.optionLabel.string}
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="${fieldName}" value="${item.optionValue.string}" <c:if test="${not empty param[fieldName] ? param[fieldName] eq item.optionValue.string : false}">checked</c:if>>${item.optionLabel.string}
                    </c:otherwise>  
                </c:choose>         
                <br>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Radio: Please enter radio options</div></c:if>
        </c:otherwise>  
    </c:choose>
</div>
   