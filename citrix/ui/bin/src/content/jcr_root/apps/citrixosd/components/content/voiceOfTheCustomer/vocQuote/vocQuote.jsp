<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
	

<div class="
	${not empty properties.style ? properties.style : 'small ' }	
	${properties.showDash ? ' ' : 'hideDash '}
">
	<div class="${properties.showBackground ? 'quoteBackground' : ' '}">
		<cq:include script="quote.jsp"/>
	</div>
</div>