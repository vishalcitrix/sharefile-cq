<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select name="Current_Solution_Y_N__c" id="Current_Solution_Y_N__c" class="required">
    <option value=""><fmt:message key="form.option.default"/></option>
    <option value="Yes"><fmt:message key="form.currentsolution.yes"/></option>
    <option value="No"><fmt:message key="form.currentsolution.no"/></option>
    <option value="Don't know"><fmt:message key="form.currentsolution.dontknow"/></option>
</select>