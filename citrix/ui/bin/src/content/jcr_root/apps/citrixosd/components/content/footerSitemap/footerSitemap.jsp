<%--

    Global Sitemap
    
    Site map containing social links and site map links programically and optional.
    
    TODO: Recursively get the links.
    TODO: Handle custom links.
    TODO: Handle social link
    
    achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="footer-nav">
        <ul class="social-media">
            <li>
                <swx:setWCMMode mode="READ_ONLY">
            		<cq:include path="shareList" resourceType="citrixosd/components/content/shareList"/>
           		</swx:setWCMMode>
           	</li>
        </ul>
        <cq:include path="columnOne" resourceType="citrixosd/components/content/sitemapNav"/>
        <cq:include path="columnTwo" resourceType="citrixosd/components/content/sitemapNav"/>
        <cq:include path="columnThree" resourceType="citrixosd/components/content/sitemapNav"/>
        <cq:include path="columnFour" resourceType="citrixosd/components/content/sitemapNav"/>
</div>