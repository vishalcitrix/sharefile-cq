<%--

    Redirect logic.

    Allows for internal redirects via the RequestDispatcher.forward() method,
    used only for internal links. Sends a 404 response on attempts to call this
    on external links.

    Allows for 302 redirects to any domain or context path.

    Will send the redirect in cases of WCMMode.DISABLED. Otherwise, relies on
    redirectMessage.jsp for sending messages to front end about status of properties.


--%><%
%><%@include file="/libs/foundation/global.jsp" %>
<cq:include script="redirectLogic.jsp"/>

<cq:include script="redirectMessage.jsp"/>