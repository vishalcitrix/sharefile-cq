<%--
	Multi Blockquote

	This component contains 1 to 5 image component which will display a quote from that image when
	user hovers over the image.
	
	Dev: Enable the javascript if it is not in edit mode or else there would be bugs, CQ loads the 
	WCM edit mode javascript after this component which  effects the content editing because of the 
	<code>display: none</code> css. This component can be placed multiple times because there is a 
	custom script every time this component is dropped on the page. The component cell id should 
	seperate them correctly.
	
	Note: Since this component is not using the javascript during edit mode, authors would have to
	preview this component in preview mode (refreshed).
    
  	achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="nodeName" value="<%= currentNode.getName() %>"/>
<div>

	<ul class="image-list" data-multiblockquote-image="${nodeName}">
		<li><cq:include path="image1" resourceType="citrixosd/components/content/imageRenditions"/></li> 
		<li><cq:include path="image2" resourceType="citrixosd/components/content/imageRenditions"/></li>
	 	<li><cq:include path="image3" resourceType="citrixosd/components/content/imageRenditions"/></li>
	 	<li><cq:include path="image4" resourceType="citrixosd/components/content/imageRenditions"/></li>
	 	<li><cq:include path="image5" resourceType="citrixosd/components/content/imageRenditions"/></li>
	</ul>
	
	<ul class="quote-list" data-multiblockquote-quote="${nodeName}">
		<li class="quote-list-item-1"><cq:include path="quote1" resourceType="citrixosd/components/content/quote"/><div class="clear"></div><span class="blockquote-sprite"></span></li>
		<li class="quote-list-item-2"><cq:include path="quote2" resourceType="citrixosd/components/content/quote"/><div class="clear"></div><span class="blockquote-sprite"></span></li>
		<li class="quote-list-item-3"><cq:include path="quote3" resourceType="citrixosd/components/content/quote"/><div class="clear"></div><span class="blockquote-sprite"></span></li>
		<li class="quote-list-item-4"><cq:include path="quote4" resourceType="citrixosd/components/content/quote"/><div class="clear"></div><span class="blockquote-sprite"></span></li>
		<li class="quote-list-item-5"><cq:include path="quote5" resourceType="citrixosd/components/content/quote"/><div class="clear"></div><span class="blockquote-sprite"></span></li>
	</ul>
	
</div>

<c:if test="${isPreviewMode || isDisabledMode || isPublishInstance}">
    <script type="text/javascript">
        $(document).ready(function() {
            MultiBlockquote.init();
        });
    </script>
</c:if>
