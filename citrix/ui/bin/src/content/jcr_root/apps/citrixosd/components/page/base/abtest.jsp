<%@page import="com.citrixosd.SiteUtils"%>
<%@include file="/libs/foundation/global.jsp" %>

<c:set var="optimizely" value="<%= SiteUtils.getPropertyFromSiteRoot("optimizelySite",currentPage) %>"/>
<c:if test="${empty optimizely}">
	<c:set var="optimizely" value="<%= properties.get("optimizely", "") %>"/>
</c:if>
<c:if test="${not empty optimizely}">
    <c:choose>
	    <c:when test="${optimizely eq 'emea'}">
	        <script ${properties.optimizelyAsync ? 'async=""' : ''} src="//cdn.optimizely.com/js/253326008.js"></script>
	    </c:when>
	    <c:when test="${optimizely eq 'us'}">
	        <script ${properties.optimizelyAsync ? 'async=""' : ''} src="//cdn.optimizely.com/js/151712929.js"></script>
	    </c:when>
	    <c:otherwise>
	        <script ${properties.optimizelyAsync ? 'async=""' : ''} src="//cdn.optimizely.com/js/${optimizely}.js"></script>
	    </c:otherwise>  
	</c:choose>
</c:if>