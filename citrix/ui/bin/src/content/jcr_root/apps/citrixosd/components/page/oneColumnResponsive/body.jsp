<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
 	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    <div id="content-body">
		<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
		<cq:include script="noscript.jsp"/>
		<cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="mainContent" resourceType="foundation/components/parsys"/> 
	</div>  
    <cq:include script="footer.jsp" />
</body>
