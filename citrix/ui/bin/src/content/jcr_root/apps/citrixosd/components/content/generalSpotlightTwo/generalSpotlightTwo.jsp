<%--
	General Spotlight Two component.
  
    This component is comprised of 1) a Share List Component 2) a Brightcove Component and 
    3) a Sprite List Component. The main responsiblility of this component is to edit and manage 
    the title and description portion of this component. It is like General Spotlight but with 
    a defined background and border.Also no rich text editor is used, instead the description 
    field is edited through a text area.This componen has fixed width and height. The component
    is meant to store small sized data. 
    
    The layout of the component is as follows: 
    	- The top portion has an image sprite and can be managed through its own share list dialog. 
    	- The middle portion includes optional text.
    	- The bottom portion includes a brightcove component that manages its own dialog.
    	- The bottom-left corner includes optional sprites. 
    	- There is an optional banner which displays when the option is checked in the dialog.
    	  This banner slides the page to the top of the page.
    	  
    	
    *** The component has a dialog with two tabs (Text Area, Bottom-Left Sprites). This dialog mangages the text contained
    	in the component and the sprites at the BOTTOM-LEFT of the component. There is also the checkbox for including 
    	GO TO TOP Banner. 
    	
 	*** Other components (Bright Cove, Share List) have there own dialog and can be used by clicking on their respective 
 		dialog outlines.
    
     
    * Layout Notes
    	- Choose small images for bright-cove image to prevent overflow
    	- Title text field holds max of 21 characters to prevent overflow
    	- Description text field holds max of 85 characters to prevent overflow
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final String TITLE_PROPERTY = "title";
	private static final String DESCRIPTION_PROPERTY = "description";
%>

<div class="container">
	<div class="icon">
		<cq:include path="sprite" resourceType="g2pc/components/content/spotlightLogo" />
	</div>

	<h3><%=properties.get(TITLE_PROPERTY, "")%></h3>
	<p><%=properties.get(DESCRIPTION_PROPERTY, "")%></p>

	<div class="video-container">

		<%-- <cq:include path="brightcove" resourceType="citrixosd/components/content/brightcoveLightbox" /> --%>
		<cq:include path="par" resourceType="swx/component-library/components/content/single-par" />
	</div>
	
	 <%	 WCMMode mode = WCMMode.fromRequest(request); 
            if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} %>
            	<cq:include path="sprite" resourceType="citrixosd/components/content/spriteList"/>
     <%	mode.toRequest(request); %>

</div>

<c:if test="${properties.showBanner}"> 
 	<div class="banner">
 		<div class="gotop backtotop">
	 		<div class="fold"></div>
			<div class="ribbon">Back to Top<span></span></div>
		</div>
	</div>
</c:if>

<div class="clearBoth"></div>