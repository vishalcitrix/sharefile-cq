<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="count" value="0" /> 

<h2 class="first">
	<c:set var="count" value="${count + 1}" />    
	<c:out value="${count}."/>
	<c:choose>
		<c:when test="${website eq 'GoToMeeting'}">
			<fmt:message key="form.title.g2m.question1"/>
		</c:when>
		<c:when test="${website eq 'GoToManageRemoteSupport'}">
				<fmt:message key="form.title.g2a.question1"/>
			</c:when>
	  	<c:otherwise>
			<fmt:message key="form.title.question1"/>  
		</c:otherwise>
	</c:choose>
</h2>