<%--
    Quote Component
    
    This component is comprised of 3 editable fields (Quote, Author, Company). The text has 
    a background image sprite and has a set height. The component follows css classes which styles 
    the font and color of the text. The quote text is italized and set to color: #4dae0d.  
    
    * Editing Notes: 
        - Quote text field is limited to 325 characters.
        - In cases that text overflows, the Quote area the output will be hidden.
        - Without any text in the Quote field, open and closed quotes will appear on the page.
        
    kguamanquispe@siteworx.com
    
 --%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%! 
	private final static String QUOTE_PROPERTY = "quote";
	private final static String AUTHOR_PROPERTY = "author";
	private final static String COMPANY_PROPERTY = "company";
	private final static String COMPANY_LINK_PROPERTY = "companyLink";
	private final static String PLACE_PROPERTY = "place";

%>
<div class="quote-container">
	<blockquote>
		<c:if test="${not empty properties.quote}">
			<span class="quote-start"><fmt:message key="quotation.left"/></span></c:if><%=properties.get(QUOTE_PROPERTY, "")%><c:if test="${not empty properties.quote}"><span class="quote-end"><fmt:message key="quotation.right"/>
			</span>
		</c:if>
	</blockquote>		
	
	<div class="text">
		<p>
		<span class="em-dash">&mdash;</span>
		<span class="author"><%= properties.get(AUTHOR_PROPERTY, "") %></span>
		<c:choose>
			<c:when test="${not empty properties['companyLink']}">
				<span class="company"><a href="${properties['companyLink']}" rel="${properties.linkOption}"><%= properties.get(COMPANY_PROPERTY, "") %></a></span>
			</c:when>
			<c:otherwise>
				<span class="company"><%= properties.get(COMPANY_PROPERTY, "") %></span>	
			</c:otherwise>
		</c:choose>
		<span class="place"><%= properties.get(PLACE_PROPERTY, "") %></span>
		</p>
	</div>
</div>