<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="javax.jcr.Node"%>
<%@page import="javax.jcr.NodeIterator"%>
<%@page import="com.citrixosd.SiteUtils"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:set var="trkObjectNamesList" value="<%= SiteUtils.getTrackingObjectNamesAsArray(currentPage) %>"/>
<%-- START TRACKER OBJECT IF --%>
<c:if test="${fn:length(trkObjectNamesList) >= 1}">

<c:set var="trkDomainsList" value="<%= SiteUtils.getTrackingDomains(currentPage,request.getServerName(),"mix:CitrixProductSiteRoot") %>"/>
<c:choose>
    <c:when test="${fn:length(trkDomainsList) > 2}"><c:set var="trkDomains" value="${fn:substring(trkDomainsList,0,(fn:length(trkDomainsList)-2))}\"\]"/></c:when>
    <c:otherwise><c:set var="trkDomains" value="\"\""/></c:otherwise>
</c:choose>
<%--
When this is run on non-production systems we should be setting the value of dataMarketingEnv 
to "dev". This causes the STAGE version of the Marketing Service api.js file to load.
http://stage.marketing.citrixonline.com/marketing/api/2009/api.js

However, as of 09/2013 the STAGE version of api.js has coding issues which cause JavaScript 
errors. So for the interim let's set dataMarketingEnv to "live" for non-production systems.
--%>
<c:choose>
    <c:when test="${isProd}"><c:set var="dataMarketingEnv" value="live"/></c:when>
    <c:otherwise><c:set var="dataMarketingEnv" value="live"/></c:otherwise>
</c:choose>

<script type="text/javascript" src="//static.citrixonlinecdn.com/web-library-2/marketing/channeltracker.js" data-marketing-environment="${dataMarketingEnv}"></script>
<script type="text/javascript">
    var trkDomain<c:forEach var="trkObjectName" items="${trkObjectNamesList}" varStatus="i">,tracker<c:out value="${i.count}" /></c:forEach>;
    dotrack = function() {
        trkDomain = ${trkDomains};<c:forEach var="trkObjectName" items="${trkObjectNamesList}" varStatus="i">
        tracker<c:out value="${i.count}" /> = new ChannelTracker('<c:out value="${trkObjectName}" />');
        tracker<c:out value="${i.count}" />.appendTrackingHosts = trkDomain;
        tracker<c:out value="${i.count}" />.trackChannel();</c:forEach>
    };
    if (typeof dotrack == 'function' && (typeof dnt !== 'undefined' && typeof dnt.dntTrue === 'function') ? !dnt.dntTrue() : true) {
        getChannelTracker(dotrack);
    }
</script>
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/channel/plugins.js"></script>
<script type="text/javascript" src="//marketing.citrixonline.com/api/2009/ChannelTrackingConverter.js"></script>

</c:if>
<%-- END TRACKER OBJECT IF --%>