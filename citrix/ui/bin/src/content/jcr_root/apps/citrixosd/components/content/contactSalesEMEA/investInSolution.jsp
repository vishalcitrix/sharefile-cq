<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<select name="Timeframe__c" id="Timeframe__c" class="required">
    <option value=""><fmt:message key="form.option.default"/></option>
    <option value="Within a month"><fmt:message key="form.option.invest1"/></option>
    <option value="1-3 months"><fmt:message key="form.option.invest2"/></option>
    <option value="4-6 months"><fmt:message key="form.option.invest3"/></option>
    <option value="More than 6 months"><fmt:message key="form.option.invest4"/></option>
    <option value="No time frame"><fmt:message key="form.option.invest5"/></option>
</select>