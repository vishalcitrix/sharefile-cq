<%--
    Title
    
    This will contain the title and the sub title and authors can control  
    the right margin.
    
    - (optional) title (default: page.getTitle())
    - (optional) subtitle
    - (optional) marginRight (default: 0px)
    
    wjsoto@siteworx.com
    achew@siteworx.com
    
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String TITLE_PROPERTY = "title";
    private static final String SUB_TITLE_PROPERTY = "subTitle";
    private static final String MARGIN_RIGHT_PROPERTY = "marginRight";
%>

<c:set var="title" value="<%= properties.get(TITLE_PROPERTY, currentPage.getTitle()) %>"/>
<c:set var="subTitle" value="<%= properties.get(SUB_TITLE_PROPERTY, "") %>"/>
<c:set var="marginRight" value="<%= properties.get(MARGIN_RIGHT_PROPERTY, "0") %>"/>

<%
boolean showSpriteNode = currentNode != null ? currentNode.hasNode("iconSprite") ? currentNode.getNode("iconSprite").hasProperty("css") : false : false;

%>

<c:if test="<%= showSpriteNode %>">
    <%  WCMMode mode = WCMMode.fromRequest(request); 
        if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} 
    %> 
        <cq:include path="iconSprite" resourceType="citrixosd/components/content/singleSprite"/>
    <%  mode.toRequest(request); %>
</c:if>

<div style="margin-right: ${marginRight}px">
    <cq:text value="${title}" tagName="h1" escapeXml="true"/>
    <cq:text value="${subTitle}" tagName="h2" escapeXml="true"/>
</div>

<div class="clearBoth"></div>