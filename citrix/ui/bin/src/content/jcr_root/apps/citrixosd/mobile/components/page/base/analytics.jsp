<%@page import="java.util.Locale"%>
<%@page import="com.day.cq.tagging.Tag" %>
<%@page import="com.day.cq.tagging.TagManager" %>
<%@page import="com.citrixosd.SiteUtils"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
	//Retrieve the tag name
    public String getTag(String[] tags, TagManager tagManager) {
		final StringBuilder builder = new StringBuilder();
		for(int i = 0; i < tags.length; i++) {
	        final Tag tag = tagManager.resolve(tags[i]);
            if (tag != null) {
    	        if(i == 0) {
    	        	builder.append(tag.getName());
    	        }else {
    	        	builder.append(",").append(tag.getName());
    	        }
            }
		}
        return builder.toString();
    }
%>

<%
    String[] analyticsPagesetTags = null;
    String analyticsTemplate = "";
    String[] analyticsProductTags = null;
    String[] analyticsSubSectionTags = null;
    String[] analyticsCountryTags = null;
    String[] analyticsLanguageTags = null;
    String[] analyticsContentTypeTags = null;
    String[] analyticsPageSectionTags = null;
    String analyticsAbTest = null;
    String analyticsAbBranch = null;
	
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    if (currentNode.hasNode("analytics-node")) {
        final Resource analyticsResource = resource.getChild("analytics-node");
        final ValueMap analyticsData = analyticsResource.adaptTo(ValueMap.class);

        analyticsPagesetTags = analyticsData.get("page-set", String[].class);
        analyticsTemplate = analyticsData.get("template", "");
        analyticsProductTags = analyticsData.get("product", String[].class);
        analyticsSubSectionTags = analyticsData.get("sub-section", String[].class);
        analyticsCountryTags = analyticsData.get("country", String[].class);
        analyticsLanguageTags = analyticsData.get("language", String[].class);
        analyticsContentTypeTags = analyticsData.get("content-type", String[].class);
        analyticsPageSectionTags = analyticsData.get("section", String[].class);
        analyticsAbTest = analyticsData.get("ab-test", "");
        analyticsAbBranch = analyticsData.get("ab-branch", "");
    }
    Locale locale = null;
    final Page localePage = currentPage.getAbsoluteParent(2);
    if (localePage != null) {
        try {
            locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
        } catch (Exception e) {
            locale = request.getLocale();
        }
    }
    else
        locale = request.getLocale();



    String defaultProduct = "";
    final Page defaultProductPage = currentPage.getAbsoluteParent(1);
    if (defaultProductPage != null)
        defaultProduct = defaultProductPage.getName();

    final String analyticsPageSetValue = (analyticsPagesetTags != null) ? getTag(analyticsPagesetTags, tagManager) : "";
    final String analyticsProductValue = (analyticsProductTags != null) ? getTag(analyticsProductTags, tagManager) : defaultProduct;
    final String analyticsSubSectionValue = (analyticsSubSectionTags != null) ? getTag(analyticsSubSectionTags, tagManager) : "";
    String analyticsCountryValue = (analyticsCountryTags != null && analyticsCountryTags.length > 0) ? getTag(analyticsCountryTags, tagManager) : locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
    if ("ee".equalsIgnoreCase(analyticsCountryValue)) {
        analyticsCountryValue = "gb_eur";
    }
    final String analyticsLanguageValue = (analyticsLanguageTags != null) ? getTag(analyticsLanguageTags, tagManager) : locale.getLanguage();
    final String analyticsContentTypeValue = (analyticsContentTypeTags != null) ? getTag(analyticsContentTypeTags, tagManager) : "";
    final String analyticsPageSectionValue = (analyticsPageSectionTags!= null) ? getTag(analyticsPageSectionTags, tagManager) : "web";
%>
<script type="text/javascript">
    <c:if test="<%=analyticsAbTest != null && !"".equals(analyticsAbTest) %>">
    var analyticsAbTest=<%=analyticsAbTest%>;
    </c:if>
    <c:if test="<%=analyticsAbBranch != null && !"".equals(analyticsAbBranch) %>">
    var analyticsAbBranch=<%=analyticsAbBranch%>;
    </c:if>

    var utag_data = {
        pageset:"<%= analyticsPageSetValue %>",
        template:"<%= analyticsTemplate %>",
        product:"<%= analyticsProductValue %>",
        sub_section:"<%= analyticsSubSectionValue %>",
        website_country:"<%= analyticsCountryValue %>",
        language:"<%= analyticsLanguageValue %>",
        content_type:"<%= analyticsContentTypeValue %>",
        section:"<%= analyticsPageSectionValue %>"
    };
    if( typeof analyticsAbTest != "undefined" )
        utag_data['ab_test'] = analyticsAbTest;
    if( typeof analyticsAbBranch != "undefined" )
        utag_data['ab_branch'] = analyticsAbBranch;
</script>

<%--<c:choose>
	<c:when test="${isDev}"><script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/remoteaccess-prelogin-v2/dev/utag.js"></script></c:when>
	<c:when test="${isQA}"><script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/remoteaccess-prelogin-v2/qa/utag.js"></script></c:when>
	<c:when test="${isStage}"><script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/remoteaccess-prelogin-v2/qa/utag.js"></script></c:when>
	<c:when test="${isProd}"><script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/remoteaccess-prelogin-v2/prod/utag.js"></script></c:when>
	<c:otherwise><script type="text/javascript" src="//tags.tiqcdn.com/utag/citrix/remoteaccess-prelogin-v2/dev/utag.js"></script></c:otherwise>
</c:choose>--%>

<c:set var="analyticsSrcUrl" value="<%= SiteUtils.getAnalyticsURL(currentPage) %>"/>
<c:if test="${not empty analyticsSrcUrl}">
    <script type="text/javascript" src="${analyticsSrcUrl}"></script>
</c:if>


<div class="mktgContainer">
	<img src="<%= currentDesign.getPath() %>/css/static/images/1x1.gif" id="mktgPixelImg" width="0" height="0" alt="">
</div>