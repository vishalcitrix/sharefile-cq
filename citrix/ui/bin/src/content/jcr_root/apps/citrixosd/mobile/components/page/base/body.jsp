<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<body>
    
    <header id="mobile-header">
    	<cq:include script="header.jsp" />
    </header>
    
    <div id="main" class="content">
        
        <div class="content-body container">
            <cq:include path="contentParsys" resourceType="foundation/components/parsys"/>
        </div>
        
    </div>
    
    <footer id="mobile-footer">
    	<cq:include script="footer.jsp" />
    </footer>

</body>