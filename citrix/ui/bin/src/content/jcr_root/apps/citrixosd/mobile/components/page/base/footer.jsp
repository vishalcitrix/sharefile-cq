<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String ASYNCHRONOUS_ANALYTICS_PROPERTY = "asynchronousAnalytics"; %>

<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>
<c:if test="<%= properties.get(ASYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>