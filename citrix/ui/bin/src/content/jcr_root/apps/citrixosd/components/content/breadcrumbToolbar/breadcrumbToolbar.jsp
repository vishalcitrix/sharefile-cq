<%--
    Breadcrumb Toolbar
    
    This tool bar will programmatically generate the breadcrumbs base on the current page
    to the root page. This will also contain a social links component on the right.
  
    Dev: The bread crumb will display the ${rootPage} and the following child pages. 
    If current page is ${rootPage} then do NOT display the bread crumb otherwise display 
    the bread crumb. Do not display the greater symbol in HTML for the first result to
    display a nice looking breadcrumb. If this is edit mode, show root page toolbar 
    otherwise hide it.
    
    Note that this custom tool bar will not display the context root page because there is 
    another alternative way to retrieve other countries. Therefore, the language site is
    the first root page displayed in the bread crumb. To modify this option, please go to
    the jsp and change the jstl forLoop to begin at x.
    
    Dependencies: shareList (component)
      
    achew@siteworx.com
    
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	private static final int ROOT_PAGE_DEPTH = 2;
%>

<%
    //Retrieve all the parent pages from the current page excluding /content.
    final List<Page> trailPages = new ArrayList<Page>();

    Page parentPage = currentPage;
    while(parentPage != null && parentPage.getDepth() > ROOT_PAGE_DEPTH) {
        trailPages.add(parentPage);
        parentPage = parentPage.getParent();
    }
    Collections.reverse(trailPages);
    
    final Map<String, Object> globalToolbar = new HashMap<String, Object>();
    globalToolbar.put("trailPages", trailPages);
%>

<c:set var="globalToolbar" value="<%= globalToolbar %>"></c:set>

<c:if test="${fn:length(globalToolbar.trailPages) > 1}">
    <div class="breadCrumbs">
        <div class="container">
            
            <div style="float:left">
                <ul>
                    <c:forEach items="${globalToolbar.trailPages}" begin="0" var="trailPage" varStatus="i">
                        <c:if test="${not trailPage.properties['hideInBc']}">
                        <li>
                            <c:choose>
                                <c:when test="${i.count == 1}">
                                    <%--Do not display the 'greater symbol' --%>
                                </c:when>
                                <c:otherwise>
                                    <span>&nbsp;&gt;&nbsp;</span>
                                </c:otherwise>
                            </c:choose>
                            <a href="${trailPage.path}">${not empty trailPage.properties['bcTitle'] ? (trailPage.properties['bcTitle']) : (not empty trailPage.navigationTitle ? trailPage.navigationTitle : trailPage.title)}</a></li>
                        </c:if>
                    </c:forEach>
                </ul>
            </div>
            
            <div style="float:right; overflow:hidden;">
            <%	
            	WCMMode mode = WCMMode.fromRequest(request); 
                if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} 
			%>
				<%--Share List component --%>
                <cq:include script="shareList.jsp"/>
			<%	mode.toRequest(request); %>
            </div>
        </div>
    </div>
</c:if>

<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
    <c:if test="${fn:length(globalToolbar.trailPages) < 2}">
        <div class="breadCrumbs">
            <div class="container">
                <div style="float:left">
                    Toolbar will not display here because this is the root page or language page
                </div>

                <div style="float:right; overflow:hidden;">
                <%	
	            	WCMMode mode = WCMMode.fromRequest(request); 
	                if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} 
				%>
                    <%--Share List component --%>
					<cq:include script="shareList.jsp"/>
				<%	mode.toRequest(request); %>
                </div>
            </div>
        </div>
    </c:if>
</c:if>