<%@page import="java.util.Locale"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>

<%! public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics"; %>
<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
%>
<c:set var="designPath" value="<%= designPath %>" />
<cq:include script="slidebar.jsp"/>
<cq:include script="lightbox.jsp"/>

<div id="footerSection">
	<cq:include path="footerSiteMap" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="footer" resourceType="swx/component-library/components/content/single-ipar"/>
	<cq:include path="footerLinks" resourceType="swx/component-library/components/content/single-ipar"/>
</div>
<%-- Javascripts - Place after including jQuery Lib --%>
<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
<%-- DNT --%>
<%
    Locale locale = null;
    final Page localePage = currentPage.getAbsoluteParent(2);
    if (localePage != null) {
        try {
            locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
        } catch (Exception e) {
            locale = request.getLocale();
        }
    }
    else
        locale = request.getLocale();
%>
<script type="text/javascript" src="https://www.citrixonlinecdn.com/dtsimages/im/fe/dnt/dnt2.js"></script>
<script type="text/javascript">
	dnt.setMsgClass('footer-dnt');
	dnt.setMsgLocId('footer-dnt');
	dnt.setLocale('<%=locale.toString()%>');
	dnt.dntInit();
</script>
<cq:include script="channelTracking.jsp"/>
<c:if test="<%= !properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
	<cq:include script="analytics.jsp"/>
</c:if>

<%-- Adding Pollyfil --%>
<!--[if lt IE 9]>
   <script async="" type="text/javascript" src="${designPath}/js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
<script async="" type="text/javascript" src="https://sadmin.brightcove.com/js/APIModules_all.js"></script>
<script async="" type="text/javascript" src="https://files.brightcove.com/bc-mapi.js"></script>

<%-- Include Parallax JS (To Do: add in properties section)--%>
<c:if test="${not empty properties.showParallax}">
    <script async="" type="text/javascript" src="<%= currentDesign.getPath() %>/js/parallax.js"></script>
</c:if>

<script type="text/javascript">
    <c:if test="${not (isEditMode || isReadOnlyMode)}">
        menuScrollBar.init();
    </c:if>
</script>