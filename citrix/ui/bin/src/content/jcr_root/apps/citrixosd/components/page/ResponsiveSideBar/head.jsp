<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<head>
   <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/app.css" type="text/css">
    
   <style type="text/css">
       #slidebar-container {
           display: block !important;
       }
   </style>
</head>