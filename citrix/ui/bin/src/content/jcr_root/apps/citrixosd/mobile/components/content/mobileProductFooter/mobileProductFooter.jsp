<%--

  Footer component.

  

--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public final String HIDE_LEFT_LOGO = "hideLeftLogo";
    public final String HIDE_SPRITE_LIST = "hideSpriteList";
    
%>

<c:set var="hideLeftLogo" value="<%= properties.get(HIDE_LEFT_LOGO, "") %>"/>
<c:set var="hideSpriteList" value="<%=properties.get(HIDE_SPRITE_LIST, "") %>"/>
    <%
    WCMMode mode = WCMMode.fromRequest(request); 
    if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);}
    %>    
	<c:if test="${hideSpriteList == ''}">
		<cq:include path="footerShare" resourceType="citrixosd/mobile/components/content/shareList"/>
	</c:if>
	<c:if test="${hideLeftLogo == ''}">
		<cq:include path="footerLogo" resourceType="citrixosd/mobile/components/content/logo"/>
	</c:if>
	<cq:include path="footerLink" resourceType="citrixosd/components/content/footerLinkSet"/>
	<cq:include path="copyright" resourceType="citrixosd/components/content/text"/>
    <% mode.toRequest(request); %>
 