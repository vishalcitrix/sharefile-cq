<%--
    Left Side Navigation
    - List of parent and child links can be added via this component. Looks for current page to display
      list of parent and chile links.
    
    vishal.gupta@citrix.com
    krishna.selvaraj@citrix.com
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String LINKS_PROPERTY = "links";

    public class Link {
        private String text;
        private String path;
        private String parent;
      
        public Link() {
            
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setParent(String parent) { 
            this.parent = parent;
        }
        public String getParent() {
            return this.parent;
        }     
    }
    
    public List<Link> getLinks(NodeIterator nodeIter) {        
        final List<Link> sideNavList = new ArrayList<Link>();
        while(nodeIter.hasNext()) {
            try {
                final Node currLink = nodeIter.nextNode();
                final Link link = new Link();               
                link.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : null);
                link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : null);
                link.setParent(currLink.hasProperty("parent") ? currLink.getProperty("parent").getString() : null);
                if(link.getText() != null && link.getPath() != null) {
                
                sideNavList.add(link);   
                }
            } catch (RepositoryException re) {
                re.printStackTrace();
            } 
        }
        return sideNavList;
    }
    
   /**
    * Returns a HashMap with parent links as the key and its respective
    * child nodes stored in a nested Linkedhashmap.
    */
    public static LinkedHashMap<String,LinkedHashMap<String,String>> getChildNodes(List<Link> nodes) {  
        LinkedHashMap<String,LinkedHashMap<String,String>> parentChildMap = new LinkedHashMap<String,LinkedHashMap<String,String>>();
        final Iterator<Link> i = nodes.iterator();     
        while(i.hasNext()){
           final Link nodeCurrent = i.next();           
           if(nodeCurrent.getParent() == null){
               if(!(parentChildMap.containsKey(nodeCurrent.getPath()))) {
                   parentChildMap.put(nodeCurrent.getPath(), new LinkedHashMap<String,String>());       
               }
           } else { 
                LinkedHashMap<String,String> currentChildMap = parentChildMap.get(nodeCurrent.getParent());       
                currentChildMap.put(nodeCurrent.getPath(),nodeCurrent.getText());         
           }   
        }
        return parentChildMap;
    }        
%>

<%
    List<Link> links = null;
    LinkedHashMap<String, LinkedHashMap <String,String>> nodeMap = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
        nodeMap = getChildNodes(links);
    }
    
    final LinkedHashMap<String, Object> linkList = new LinkedHashMap<String, Object>();
    linkList.put(LINKS_PROPERTY, links); 
%>

<c:set var="linkList" value="<%= linkList %>"/>
<c:set var="nodeMap" value="<%= nodeMap %>"/>

<c:choose>
    <c:when test="${fn:length(linkList.links) > 0}">
        <ul>
            <c:forEach items="${linkList.links}" var="link">  
                <c:choose>               
                    <c:when test="${(empty link.parent) && (link.path eq currentPage.path)}">
                        <li><a class="current" href="#">${link.text}</a></li>
                        <div class="block">
                            <c:forEach var="childNodes" items="${nodeMap[link.path]}">
                                <li><a href="${childNodes.key}" class="${currentPage.path eq childNodes.key ? 'child':''}">${childNodes.value}</a></li>
                            </c:forEach>
                        </div>                    
                    </c:when>                   
                    <c:when test="${(empty link.parent) && (link.path ne currentPage.path)}">                           
                        <c:choose>
                            <c:when test="${empty nodeMap[link.path]}">
                                <li><a class="parent" href="${link.path}">${link.text}</a></li>
                            </c:when>
                            <c:otherwise>
                                <c:set var="parentChildTree" value="${nodeMap[link.path]}"/>
                            <c:choose>
                                <c:when test="${not empty parentChildTree[currentPage.path]}">
                                    <li><a class="current" href="${link.path}">${link.text}</a></li>
                                    <div class="block">        
                                        <c:forEach var="childNodes" items="${nodeMap[link.path]}">
                                            <li><a class="${currentPage.path eq childNodes.key ? 'child':''}" href="${childNodes.key}">${childNodes.value}</a></li>
                                        </c:forEach>
                                    </div>                                    
                                </c:when>
                                <c:otherwise>
                                    <li><a class="parent" href="${link.path}">${link.text}</a></li>
                                </c:otherwise>
                            </c:choose>
                            </c:otherwise>
                        </c:choose>           
                    </c:when>                                      
                </c:choose> 
            </c:forEach>
        </ul>
    </c:when>
</c:choose>
