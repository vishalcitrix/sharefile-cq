<%@include file="/apps/citrixosd/global.jsp" %>
 
 <body>

    <cq:include script="header.jsp" />
    
    <div id="main" class="content">
    
    	<cq:include path="banner" resourceType="swx/component-library/components/content/single-par"/>
    	
        <cq:include path="stickyHeader" resourceType="swx/component-library/components/content/single-ipar"/>
    
        <div class="content-body container">
        
            <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
            
        </div>
        
    </div>

    <cq:include script="footer.jsp" />

</body>