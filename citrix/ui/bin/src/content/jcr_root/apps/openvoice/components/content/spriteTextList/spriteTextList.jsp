<%--
	Sprite & Text List
	
	This list can have sprite and text. Dialog lets you add any number of spriteLists.
	Contains a logo and rich text editor that have their own dialogs. 
	
	kguamanquispe@sitewox.com
  
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
    
<ul class="sprite-text-list-container">
    <c:forEach begin="1" end="${not empty properties['listCount'] ? properties['listCount'] : 1}" varStatus="i">
        <li class="${i.first ? 'first' : i.last ? 'last' : ''}">
			<div class="list-sprite">
				<cq:include path="sprite_item_${i.count}" resourceType="openvoice/components/content/featureLogo"/>
			</div>
			<div class="list-text">
				<cq:include path="list_item_${i.count}" resourceType="openvoice/components/content/text"/>
			</div>
        </li>
    </c:forEach>
</ul>