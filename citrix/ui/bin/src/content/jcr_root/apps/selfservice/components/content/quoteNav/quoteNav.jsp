<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>


<%!
    public static final String LABEL_ONE_PROPERTY = "steponelabel";
    public static final String LABEL_TWO_PROPERTY = "steptwolabel";
    public static final String LABEL_THREE_PROPERTY = "stepthreelabel";

    public static final String LINK_ONE_PROPERTY = "pathone";
    public static final String LINK_TWO_PROPERTY = "pathtwo";
    public static final String LINK_THREE_PROPERTY = "paththree";
%>

<c:set var="labelone" value="<%= properties.get(LABEL_ONE_PROPERTY, "") %>"/>
<c:set var="labeltwo" value="<%= properties.get(LABEL_TWO_PROPERTY, "") %>"/>
<c:set var="labelthree" value="<%= properties.get(LABEL_THREE_PROPERTY, "") %>"/>

<c:set var="fullpathone" value="<%= properties.get(LINK_ONE_PROPERTY, "") %>"/>
<c:set var="fullpathtwo" value="<%= properties.get(LINK_TWO_PROPERTY, "") %>"/>
<c:set var="fullpaththree" value="<%= properties.get(LINK_THREE_PROPERTY, "") %>"/>

<c:set var="pathone" value="${fn:split(fullpathone, '/')}"/>
<c:set var="pathtwo" value="${fn:split(fullpathtwo, '/')}"/>
<c:set var="paththree" value="${fn:split(fullpaththree, '/')}"/>

<c:set var="pathonelength" value="${fn:length(pathone)-1}"/>
<c:set var="pathtwolength" value="${fn:length(pathtwo)-1}"/>
<c:set var="paththreelength" value="${fn:length(paththree)-1}"/>

<c:set var="currentPath" value="<%= currentPage.getName() %>"/>

<script type="text/javascript">
    $(document).ready(function(){
            $stepone = $('ul.quote-nav-list li:eq(0)');
            $steptwo = $('ul.quote-nav-list li:eq(1)');
            $stepthree = $('ul.quote-nav-list li:eq(2)');
        <c:choose>
        <c:when test="${currentPath == pathone[pathonelength]}">
            $stepone.children("h4").addClass('selected');
        </c:when>
        <c:when test="${currentPath == pathtwo[pathtwolength]}">
            <c:set var="linkonefinished" value="true"/>
            $stepone.find("a h4").addClass('finished');
            $steptwo.children("h4").addClass('selected');
        </c:when>
        <c:when test="${currentPath == paththree[paththreelength]}">
            <c:set var="linkonefinished" value="true"/>
            <c:set var="linktwofinished" value="true"/>
            $stepone.find("a h4").addClass('finished');
            $steptwo.find("a h4").addClass('finished');
            $stepthree.children("h4").addClass('selected');
        </c:when>
        </c:choose>
    })
</script>

<section class="quoteNav content-block">
    <div class="log" style="display: none">
        <p>currentPath: <c:out value="${currentPath}"/></p>
        <p>pathone: <c:out value="${pathone[0]}"/></p>
        <p>pathonelength: <c:out value="${pathonelength}"/></p>
        <p>pathone[pathonelength]: <c:out value="${pathone[pathonelength]}"/></p>
    </div>
    <ul class="quote-nav-list">
        <li>
            <c:if test="${linkonefinished == 'true'}"><a href="${fullpathone}"></c:if>
            <h4>
                <span>1</span>
                <c:if test="${not empty labelone}">
                    <fmt:message key="${labelone}" />
                </c:if>
            </h4>
            <c:if test="${linkonefinished == 'true'}"></a></c:if>
        </li>
        <li>
            <c:if test="${linktwofinished == 'true'}"><a href="${fullpathtwo}"></c:if>
            <h4>
                <span>2</span>
                <c:if test="${not empty labeltwo}">
                    <fmt:message key="${labeltwo}" />
                </c:if>
            </h4>
            <c:if test="${linktwofinished == 'true'}"></a></c:if>
        </li>
        <li>
            <h4>
                <span>3</span>
                <c:if test="${not empty labelthree}">
                    <fmt:message key="${labelthree}" />
                </c:if>
            </h4>
        </li>
    </ul>

</section>