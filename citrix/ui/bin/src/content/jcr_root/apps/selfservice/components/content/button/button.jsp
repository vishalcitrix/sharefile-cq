<%--
  Button
  - This component will consist of custom css buttons which will contain a 
    label and a link. The button also allows an optional sub text displayed 
    under the button. Default button style is set to none.
  
  ingrid.tseng@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String CSS_PROPERTY = "css";
    public static final String LABEL_PROPERTY = "label";
    public static final String PATH_PROPERTY = "path";
    public static final String LINK_OPTION_PROPERTY = "linkOption";
    public static final String SUB_TEXT_PROPERTY = "subText";
    public static final String CENTER_ALIGN_PROPERTY = "centerAlign";
    public static final String ID_PROPERTY = "linkId";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, "none") %>"/>
<c:set var="label" value="<%= properties.get(LABEL_PROPERTY, "") %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, "") %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>
<c:set var="subText" value="<%= properties.get(SUB_TEXT_PROPERTY, "") %>"/>
<c:set var="centerAlign" value="<%= properties.get(CENTER_ALIGN_PROPERTY, false) %>"/>
<c:set var="linkId" value="<%= properties.get(ID_PROPERTY, "") %>"/>

<c:choose>
    <c:when test="${css eq 'none'}">
        
        <c:choose>
            <c:when test="${empty label}">
                <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) ||  WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
                    <div class="noStyle">Button</div>
                </c:if>
            </c:when>
            <c:otherwise>
                <div class="italicStyle"><fmt:message key="${label}" /></div>       
            </c:otherwise>
        </c:choose>
            
    </c:when>
    <c:otherwise>
        <div class="button-container" <c:if test="${centerAlign}">style="text-align:center;"</c:if>>
        <c:choose>
            <c:when test="${not empty linkOption}">
                <a <c:if test="${not empty linkId}">id="${linkId}"</c:if> class="btn ${css}" href="${path}" rel="${linkOption}">
                    <c:if test="${not empty label}">
                        <fmt:message key="${label}" />
                    </c:if>
                    <c:if test="${not empty subText}">
                        <div class="subText">
                            <fmt:message key="${subText}"/>
                        </div>
                    </c:if>
                </a>
            </c:when>
            <c:otherwise>
                <a <c:if test="${not empty linkId}">id="${linkId}"</c:if> class="btn ${css}" href="${path}">
                    <c:if test="${not empty label}">
                        <fmt:message key="${label}" />
                    </c:if>
                    <c:if test="${not empty subText}">
                        <div class="subText">
                            <fmt:message key="${subText}"/>
                        </div>
                    </c:if>
                </a>
            </c:otherwise>
        </c:choose>
        </div>
    </c:otherwise>
</c:choose>