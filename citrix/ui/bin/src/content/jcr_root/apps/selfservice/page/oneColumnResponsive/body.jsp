<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-XCGM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-XCGM');</script>
    <!-- End Google Tag Manager -->

    <div id="content-body">
        <cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>  
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="mainContent" resourceType="foundation/components/parsys"/>

    </div>  
    <cq:include script="footer.jsp" />
    
    

    <div id="lightbox-container">
    <div id="lightbox-border">
    <div id="lightbox-close"><i class="icon-close-large"></i></div>
    <div id="lightbox-dynamic"></div>
    </div>
    </div> 
    <script type="text/javascript" src="/etc/designs/selfservice/clientlib/js/components/button.js"></script>
      
</body>