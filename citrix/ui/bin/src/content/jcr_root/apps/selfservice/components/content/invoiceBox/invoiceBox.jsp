<%--

  Invoice Box component.

--%>

<%@include file="/libs/foundation/global.jsp"%>

<style type="text/css">
.editBtn { position: absolute; right: 10px; top: 5px; }
.balance { color: #78be20; font-weight: normal; }
.balancetxt { color: #5D5D5D; font-size: 16px; }
p { color:#8997ad; font-size: 14px; }
.invoiceBox .row { margin-left:0; margin-right:0; }
</style>

<div class="invoiceBox">
    <div class="section-title">
      <a class="btn btn-invis editBtn">Edit</a>
    </div>
    <div class="section-content">
      <div class="row">
        <div class="float-left">
          <p class="balancetxt">Balance Due:</p>
          <p> <span>Due Date: </span>3/15/15</p>
          <p> <span>Auto Billed To: </span><i class="icon-cc-visa"> </i> ...2929</p>
          <p><a>View Invoice</a><span class="gray-txt">|</span><a class="invoice-view">Invoice History</a></p>
          <p class="payLink"><a href="#">Pay Now</a></p>
        </div>
        <div>
          <h3 class="balance">$1056.00</h3>
        </div>
      </div>
    </div>
</div>