<%--
    Link Set
    
    A collection of links which can have customizable properties. The link list 
    will also have a css class for the first and last item so it would be easier 
    to apply css classes to them. Link text is i18n.
    
    achew@siteworx.com
   
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%!
    private static final String LINKS_PROPERTY = "links";
    private static final String STYLE_SPLITTER_PROPERTY = "splitter";
    public class Link {
        private String header;
        private String description;
        private String path;
        private String linkText;
        
        public Link() {
            
        }
        public void setHeader(String header) {
            this.header= header;
        }
        public String getHeader() {
            return this.header;
        }
        public void setDescription(String description) {
            this.description= description;
        }
        public String getDescription() {
            return this.description;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkText(String linkText) {
            this.linkText= linkText;
        }
        public String getLinkText() {
            return this.linkText;
        }
    }   
    public List<Link> getLinks(NodeIterator nodeIter) {
        final List<Link> shares = new ArrayList<Link>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final Link link = new Link();
                    link.setHeader(currLink.hasProperty("header") ? currLink.getProperty("header").getString() : "");
                    link.setDescription(currLink.hasProperty("description") ? currLink.getProperty("description").getString() : "");
                    link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : "");
                    link.setLinkText(currLink.hasProperty("linkText") ? currLink.getProperty("linkText").getString() : "");
                    
                    if(link.getHeader() != null || link.getDescription() != null || link.getPath() != null  || link.getLinkText() != null  ) {
                        shares.add(link);   
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }
%>
<%
    List<Link> links = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
    }    
    final Map<String, Object> linkSet = new HashMap<String, Object>();
    linkSet.put(LINKS_PROPERTY, links); 
%>
<c:set var="linkSet" value="<%= linkSet %>"/>
<c:choose>
    <c:when test="${fn:length(linkSet.links) > 0}">
        <section class="contact-options">
            <div class="container">
                <div class="row">
                    <c:forEach items="${linkSet.links}" begin="0" var="link" varStatus="i">             
                        <div class="column medium-4">
                            <div class="option-container">
                                <div class="row">
                                    <div class="column small-3"><i class="citrix-icon citrix-icon-question-2"></i></div>
                                    <div class="column small-9">
                                        <h4>${link.header}</h4>
                                        <P>${link.description}</P><a href="${not empty link.path ? link.path : ''}" ><fmt:message key="${link.linkText}"/>
                                    </div>
                                </div>
                            </div>
                        </div>          
                    </c:forEach>        
                </div>
            </div>
        </section>   
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <span class="warning">Link Set Component</span>
        </c:if>
    </c:otherwise>
</c:choose>