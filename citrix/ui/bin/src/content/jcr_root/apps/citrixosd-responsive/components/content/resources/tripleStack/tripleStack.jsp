<%--
     Triple Stack component.
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>

<c:if test="${isEditMode}">
    <style>
	    .stacker {
	        height: 183px !important;
	    }
    </style>
</c:if>
<div class="triple-container ${properties.colorScheme}">
    <ul class="triple-wrap">
	    <c:forEach var="idx" begin="1" end="3">
		    <li class="stacker singleStack-${idx}">
		        <cq:include path="panel${idx}" resourceType="citrixosd-responsive/components/content/resources/tripleStack/singleStack"/>
		    </li>
	    </c:forEach>
    </ul>
</div>
<hr/>    
<div class="clearBoth"></div>