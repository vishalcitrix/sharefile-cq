<%--
    Background Video component
--%>
<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp" %><%
%><%@ page import="com.day.cq.wcm.api.WCMMode,				   
				   com.day.cq.dam.api.Asset,
                   com.day.cq.wcm.api.components.DropTarget,
                   com.citrixosd.utils.ContextRootTransformUtil" %>
<%! 
    private static final String FILE_REFERENCE_PROPERTY = "fileReference";
    private static final String MOBILE_FILE_REFERENCE_PROPERTY = "mobileFileReference";
    private static final String RENDITION_PROPERTY = "imageRenditions";
    private static final String VIDEO_HTML_TYPE = "dc:format";
%>
<%
    // try find referenced assets
    String  firstVIdeoRef =properties.get("asset1" , null);
    String  firstVIdeoHtmlType="";
    String  secondVIdeoHtmlType="";
    if(firstVIdeoRef != null){
    	Resource assetRes = resourceResolver.getResource(firstVIdeoRef);
        if (assetRes != null) {
        	 Asset asset  = assetRes.adaptTo(Asset.class);
        	 firstVIdeoHtmlType= asset !=null ? asset.getMetadataValue(VIDEO_HTML_TYPE) :"";
        }
        firstVIdeoRef = ContextRootTransformUtil.transformedPath(firstVIdeoRef,request);
    }
    String  secondVIdeoRef =properties.get("asset2" , null);
    if(secondVIdeoRef != null){
        secondVIdeoRef = ContextRootTransformUtil.transformedPath(secondVIdeoRef,request);
        Resource assetRes = resourceResolver.getResource(secondVIdeoRef);
        if (assetRes != null) {
        	 Asset asset  = assetRes.adaptTo(Asset.class);
        	 secondVIdeoHtmlType= asset !=null ? asset.getMetadataValue(VIDEO_HTML_TYPE) :"";
        }
    }
    boolean hasImageRef = currentNode != null ? currentNode.hasNode(RENDITION_PROPERTY) ? currentNode.getNode(RENDITION_PROPERTY).hasProperty(FILE_REFERENCE_PROPERTY) : false : false;
    String  fallbackImage = null;
    if(hasImageRef){
    	fallbackImage = currentNode.getNode(RENDITION_PROPERTY).getProperty(FILE_REFERENCE_PROPERTY).getString();
        fallbackImage=ContextRootTransformUtil.transformedPath(fallbackImage,request);
    }
    String mobileImageRef = ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(MOBILE_FILE_REFERENCE_PROPERTY, "")),request);
%>
    <c:set var="PlayerID" value="<%= "cq-video-html5-" + System.currentTimeMillis() %>" />
    <c:set var="videoFirst" value="<%= firstVIdeoRef %>" />
    <c:set var="videoFirstType" value="<%= firstVIdeoHtmlType %>" />
    <c:set var="videoSecond" value="<%= secondVIdeoRef %>" />
    <c:set var="videoSecondType" value="<%= secondVIdeoHtmlType %>" />
    <c:set var="hasImageRef" value="<%= hasImageRef %>" />
    <c:set var="fallbackImage" value="<%= fallbackImage %>" />
    <c:set var="mobileImageRef" value="<%= mobileImageRef %>" />
    <c:set var="loop" value="<%= properties.get("loop" , null)%>" />
    <c:set var="audio" value="<%= properties.get("audio" , null)%>" />
    <c:set var="playerHeight" value="<%= properties.get("height" , "auto")%>" />
    <c:set var="playerWidth" value="<%= properties.get("width" , "auto")%>" />
	
	<div class="autoVideo">
    <c:choose>
        <c:when test="${not empty videoFirst or not empty videoSecond}">
            <video autoplay  width="${playerWidth} " height="${playerHeight}"  poster="${fallbackImage}" id='${PlayerID}' ${loop eq 'true' ? 'loop' : ''} ${audio eq 'true' ? '' : 'muted'}>
                <source src="${videoFirst}" type="${videoFirstType}">
                <source src="${videoSecond}" type="${videoSecondType}">
            </video>
        </c:when>
        <c:when test="${hasImageRef}">
            <video autoplay width="${playerWidth} " height="${playerHeight}" poster="${fallbackImage}" id='${PlayerID}' ${loop eq 'true' ? 'loop' : ''} ${audio eq 'true' ? '' : 'muted'}>
            </video>
        </c:when>
        <c:otherwise>
            <div class="<%= DropTarget.CSS_CLASS_PREFIX + "video" + (WCMMode.fromRequest(request) == WCMMode.EDIT ? " cq-video-placeholder" : "") %>"></div>
        </c:otherwise>
    </c:choose>
    </div>
    <div class="backgroundVideoContent" data-mobile-bg="${mobileImageRef}">
        <cq:include path="backgroundVideoParsys" resourceType="foundation/components/parsys"/>
    </div>
    <div class="clearBoth"></div> 
   