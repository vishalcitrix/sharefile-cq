<%--
    Box Container
    - Creates box with border and parsys. 
      The border will not be shown/hidden in tablet and mobile based on selection in dialog.
    
    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
	public static final String CSS_BORDER_CLASS = "box";
	public static final String CSS_COLOR_CLASS = "color";
	public static final String CSS_WIDTH_CLASS = "width";   
%>

<c:set var="box" value="<%= properties.get(CSS_BORDER_CLASS, "") %>"/>
<c:set var="color" value="<%= properties.get(CSS_COLOR_CLASS, "") %>"/>
<c:set var="width" value="<%= properties.get(CSS_WIDTH_CLASS, "") %>"/>
<c:set var="borderRadius" value="<%= properties.get("borderRadius", "") %>"/>
<c:set var="borderBottomLeftRadius" value="<%= properties.get("borderBottomLeftRadius", "") %>"/>
<c:set var="borderBottomRightRadius" value="<%= properties.get("borderBottomRightRadius", "") %>"/>
<c:set var="borderTopLeftRadius" value="<%= properties.get("borderTopLeftRadius", "") %>"/>
<c:set var="borderTopRightRadius" value="<%= properties.get("borderTopRightRadius", "") %>"/>
<c:set var="expand" value="${not empty properties.expand ? 'expand' : 'removeBg'}"/>
<c:set var="boxShadow" value="<%= properties.get("boxShadow", "") %>"/>
<c:set var="borderPropertiesMedium" value="${not empty properties.borderPropertiesMedium ? 'borderPropertiesMedium' : 'removeBorderProperties'}"/>
<c:set var="borderPropertiesSmall" value="${not empty properties.borderPropertiesSmall ? 'borderPropertiesSmall' : 'removeBorderproperties'}"/>

<div class="${not empty properties.paddTop ? properties.paddTop : ''}">
	<cq:include path="boxHeader" resourceType="swx/component-library/components/content/single-ipar" />
	
<div class="container ${box} ${width} ">
		<div class=" ${color} ${expand} ${borderPropertiesMedium} ${borderPropertiesSmall} ${borderRadius} ${borderBottomLeftRadius} ${borderBottomRightRadius} ${borderTopLeftRadius} ${borderTopRightRadius} ${boxShadow}">
			<cq:include path="boxContent" resourceType="foundation/components/parsys" />
		</div>
	</div>
</div>