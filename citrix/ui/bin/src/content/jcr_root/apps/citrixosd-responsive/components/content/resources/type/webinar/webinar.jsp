<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.citrixosdRedesign.models.resource.Webinar" %>
<%--
    Webinar Detail Page
    vishal.gupta@citrix.com
--%>
<%@ page import="com.day.cq.tagging.Tag" %>
<%@ page import="com.day.cq.tagging.TagManager" %>

<%@include file="/apps/citrixosd/global.jsp"%>
<%
	final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
	StringBuffer productTagsTitle = new StringBuffer ("");
	
	//Standard Detail Object
	Webinar details = new Webinar();
	details = ResourceUtils.getWebinar(resource, currentPage, currentDesign, request);
	
	//Detail Products
	if(details != null && details.getProducts() != null && details.getProducts().length > 0) {
	    String sep = "";
	    for(String tagStr : details.getProducts()) {
	        productTagsTitle.append(sep);
	        productTagsTitle.append(tagManager.resolve(tagStr).getTitle());
	        sep = " , ";
	    }
	}
%>

<c:set var="webinar" value="<%= details %>"/>
<c:set var="productTagsTitle" value="<%= productTagsTitle %>"/>
<c:set var="timeZone" value="<%= properties.get("timeZone","PST") %>"/>
<c:set var="now" value="<%= new java.util.Date() %>"/>
<c:set var="baseURL" value="<%= request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath()) %>"/>

<article class="featuredPromotion">
    <%-- Video --%>
    <c:choose>
        <c:when test="${not empty webinar.videoPlayer}">
            <%-- Video --%>
            <div id="${webinar.id}" class="main-image brightcove-container" data-video-player="${webinar.videoPlayer}" data-player-id="${webinar.playerId}" data-player-key="${webinar.playerKey}"></div>
        </c:when>
        <c:otherwise>
            <c:if test="${empty properties.hideBanner}">
                <div class="main-image">
                    <img src="${webinar.imagePath}"/>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
    <section class="mainFeaturedContent">
        <%-- Type--%>
        <div class="feature-type">
            <span class="category">${webinar.resourceType}</span>
        </div>

        <%-- Title--%>
        <header>
            <h1 class="feature-title">${webinar.title}</h1>
        </header>
        
        <%-- Products --%>
        <c:if test="${productTagsTitle ne ''}">
            <div class="products">   
                <b><fmt:message key="resource.relevant.products"></fmt:message></b>
                ${productTagsTitle}
            </div>
         </c:if>
         
        <%-- Social links--%>
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style" addthis:url="${baseURL}${webinar.path}" addthis:title="${webinar.title}" addthis:description="${webinar.description}">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_button_linkedin_counter"></a>
            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
            <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <!-- AddThis Button END -->

        <div class="relevant">
            <c:if test="${not empty webinar.speakers && fn:length(webinar.speakers) > 0}">
                <ul>
                    <c:forEach items="${webinar.speakers}" var="speaker" varStatus="i">
                        <li>
                            <cq:include path="speakers/speaker${i.count}" resourceType="citrixosd-responsive/components/content/resources/type/webinar/speaker"/>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>

            <c:if test="${not empty webinar.date}">
                <div class="clearfix bold">
					<span>
					    <fmt:message key="resource.webinar.date"/>
					</span>
					<span>
					    <fmt:formatDate value="${webinar.date}" type="DATE" dateStyle="LONG"/>
					</span>
				</div>
				<div class="resource-webinar clearfix bold">
					<span>
						<fmt:message key="resource.webinar.time"/>
					</span>
					<span>
					  <fmt:formatDate value="${webinar.date}" type="time" timeStyle="SHORT" timeZone="${timeZone}"/>
					  <c:if test="${timeZone == 'US/Eastern'}">
                          &nbsp;${fn:replace(timeZone,'US/Eastern', 'EST')}
                      </c:if>
                      <c:if test="${timeZone == 'America/Los_Angeles'}">
                          &nbsp;${fn:replace(timeZone,'America/Los_Angeles', 'PDT')}
                      </c:if>
                      <c:if test="${timeZone == 'America/New_York'}">
                          &nbsp;${fn:replace(timeZone,'America/New_York', 'EDT')}
                      </c:if>
					</span>
				</div>
            </c:if>
            
            <c:if test="${not empty webinar.description  && empty properties.hideDescription}">
                <div class="feature-description">${webinar.description}</div>
            </c:if>
            
            <c:if test="${not empty webinar.topics && fn:length(webinar.topics) > 0}">
                <div class="clearfix">
                    <%-- Relevant Topics: --%>
                    <span class="bold"><fmt:message key="resource.relevant.topics"></fmt:message></span>
                    <c:forEach items="${webinar.topics}" var="topics" varStatus="i">
                        <span>${topics}</span>
                        <c:choose>
                            <c:when test="${i.last}"></c:when>
                            <c:otherwise>
                                <span>,</span>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </div>
            </c:if>

            <c:if test="${not empty webinar.registerPath}">
                <div class="featured-link resource-webinar clearfix">
                    <a href="${webinar.registerPath}">
                        <%-- Watch the Webinar --%>
                        <fmt:message key="resource.webinar.path"></fmt:message>
                    </a>
                </div>
            </c:if>
        </div>

        <div class="clearfix"></div>
    </section>
</article>

<c:if test="${not empty webinar.videoPlayer}">
    <script type="text/javascript">
        $(document).ready(function() {
            function createVideo() {
                BCL.createVideoWithNoAutoPlay(
                    "100%",
                    "100%",
                    "${webinar.playerId}",
                    "${webinar.playerKey}",
                    "${webinar.videoPlayer}",
                    "${webinar.id}");
                brightcove.createExperiences();
            }
            
            window.addEventListener('orientationchange', createVideo);
            createVideo(); 
        });
    </script>
</c:if>