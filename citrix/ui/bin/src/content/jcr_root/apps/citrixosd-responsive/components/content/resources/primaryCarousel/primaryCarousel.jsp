<%--
     Primary Carousel component.
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosdRedesign.models.resource.*" %>
<%@page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@page import="java.util.*" %>

<c:set var="resourceList" value="<%= ResourceUtils.getFeaturedResources(resource, currentPage, properties.get("itemscount",3), currentDesign, request) %>" scope="request"/>

<c:choose>
    <c:when test="${isEditMode}">
        <div class="warning">
        	<c:choose>
        		<c:when test="${fn:length(resourceList) eq 0}">
        			No Featured Resource Found.
        		</c:when>
        		<c:otherwise>
        			${fn:length(resourceList)} Featured Resources found. Carousel will be displayed in Preview Mode.
        		</c:otherwise>
        	</c:choose>
        </div>
    </c:when>
    <c:otherwise>
        <div id="primaryCarousel" class="primary-carousel ${properties.colorScheme}<c:if test="${properties.itemscount eq 1}"> single</c:if>">
        	<c:forEach items="${resourceList}" var="item">
        		<a href="${item.path}">
            		<div style="background-image:url('${item.imagePath}'); background-position: center center; background-size: cover; height: 531px;"></div>
            		<div class="owl-item-info">
				        ${item.featuredTitle}
				        ${item.featuredDesc}
				        <span class="icon"></span>
				    </div>  
            	</a>
            	<div class="link-sub-container">
			        <a href="${item.path}">Learn More<span class="arrow-pointer"></span></a>
			    </div>
			</c:forEach>
        </div>
    </c:otherwise>
</c:choose>

<div class="clearBoth"></div>
<hr/>
