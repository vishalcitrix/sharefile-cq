<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.citrixosdRedesign.models.resource.StandardDetail" %>
<%@ page import="com.citrixosdRedesign.constants.ResourceConstants" %>
<%@ page import="com.day.cq.dam.api.Asset" %>
<%@ page import="com.day.cq.commons.jcr.JcrConstants" %>
<%@ page import="com.day.cq.dam.api.DamConstants" %>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%@ page import="org.apache.sling.api.SlingConstants" %>
<%@ page import="com.day.cq.tagging.Tag" %>
<%@ page import="com.day.cq.tagging.TagManager" %>
<%@ page import="java.util.Arrays" %>

<%--
    Standard Detail Page (takes care of All Documents types, Customer Story, Video and Case Studies)
    vishal.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String ENABLE_PDF_TO_HTML_PROPERTY = "enablePdfToHtml";
    private static final String PDF_TO_HTML = "pdfToHtml";
    private static String RTE_COMPONENT  = "/components/content/resources/resourceText";
    private static final String CONTENT_TYPE = "contentType";
%>
<%
	RTE_COMPONENT = properties.get("product","sf") + RTE_COMPONENT;
%>
<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

    final boolean enablePdfToHtml = properties.get(ENABLE_PDF_TO_HTML_PROPERTY, false);
    final String[] contentType = properties.get(CONTENT_TYPE, String[].class);
    final boolean documentType = contentType == null ? false : Arrays.asList(contentType).contains("document");
    final boolean videoType = contentType == null ? false : Arrays.asList(contentType).contains("video");
   
    StringBuffer productTagsTitle = new StringBuffer ("");
    
    if(currentNode != null){
        final String damAsset = properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) != null ? properties.get(ResourceConstants.Document.DAM_ASSET_PATH, String.class) : null;
        final Resource damResource = damAsset != null ? resourceResolver.getResource(damAsset) : null;
        final Asset asset = damResource != null ? damResource.adaptTo(Asset.class) : null;
        final String assetType = asset!= null ? asset.getMimeType() : null;
        
        if(WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
            if(enablePdfToHtml && !currentNode.hasNode(PDF_TO_HTML) && documentType) {
                if(asset != null && asset.getMimeType().equals(ResourceConstants.Document.PDF_MIMETYPE)) {
                    final Node assetNode = damResource.adaptTo(Node.class);
                    final Node assetNodeJcr = assetNode.getNode(JcrConstants.JCR_CONTENT);
                    final Node metadata = assetNodeJcr.hasNode(DamConstants.METADATA_FOLDER) ? assetNodeJcr.getNode(DamConstants.METADATA_FOLDER) : null;
                    if(metadata != null) {
                        final String html = metadata.getProperty("html").getString();
                        if(html != null && html.trim().length() > 0) {
                            final Session session = resourceResolver.adaptTo(Session.class);
                            final Node pdfToHtmlNode = currentNode.addNode(PDF_TO_HTML);
                            pdfToHtmlNode.setProperty("text", html);
                            pdfToHtmlNode.setProperty(SlingConstants.PROPERTY_RESOURCE_TYPE, RTE_COMPONENT);
                            session.save();
                        }
                    }
                }
            }
        }
    }
    
    //Standard Detail Object
    StandardDetail details = new StandardDetail();
    details = ResourceUtils.getStandardDetails(resource, currentPage, currentDesign, request);
    
    //Detail Products
    if(details != null && details.getProducts() != null && details.getProducts().length > 0) {
        String sep = "";
        for(String tagStr : details.getProducts()) {
            productTagsTitle.append(sep);
            productTagsTitle.append(tagManager.resolve(tagStr).getTitle());
            sep = " , ";
        }
    }
%>

<c:set var="details" value="<%= details %>"/>
<c:set var="productTagsTitle" value="<%= productTagsTitle %>"/>
<c:set var="documentType" value="<%= documentType %>"/>
<c:set var="videoType" value="<%= videoType %>"/>
<c:set var="mimeTypePDF" value="<%= ResourceConstants.Document.PDF_MIMETYPE %>"/>
<c:set var="mimeTypePPT" value="<%= ResourceConstants.Document.PPT_MIMETYPE %>"/>
<c:set var="baseURL" value="<%= request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath()) %>"/>

<article class="featuredPromotion">
    <c:choose>
        <c:when test="${not empty details.videoPlayer && videoType}">
            <%-- Video --%>
            <div id="${details.id}" class="main-image brightcove-container" data-video-player="${details.videoPlayer}" data-player-id="${details.playerId}" data-player-key="${details.playerKey}"></div>
        </c:when>
        <c:otherwise>
            <%-- Image--%>
            <c:if test="${empty properties.hideBanner}">
                <div class="main-image" style="background-image:url('${details.imagePath}'); background-position: center center; background-size: cover;"></div>
            </c:if>
        </c:otherwise>
    </c:choose>

    <section class="mainFeaturedContent" >
        <%-- Type--%>
        <div class="feature-type">
            <span class="category">${details.resourceType}</span>
            <%-- Date--%>
            <c:if test="${not empty details.date}">
                <span>&nbsp;&nbsp;<fmt:formatDate value="${details.date}"/></span>
            </c:if>
        </div>
        
        <%-- Title--%>
        <header>
            <h1 class="feature-title">${details.title}</h1>
        </header>

        <%-- Watch Video 
        <c:if test="${not empty details.videoPlayer && videoType}">
            <div class="featured-link">
                <a href="${details.videoPlayer}">
                    <fmt:message key="resource.video.watch.video"></fmt:message>
                </a>
                <span>${details.videoLength}</span>
            </div>
        </c:if>
        --%>
        
        <%-- Author --%>
        <c:if test="${not empty details.authorName}">
            <div class="author">
                ${details.authorName}
                <c:if test="${not empty details.authorTitle}">
                 , ${details.authorTitle}
                </c:if>
            </div>
        </c:if>
        
        <%-- Products --%>
        <c:if test="${productTagsTitle ne ''}">
            <div class="products">   
                <b><fmt:message key="resource.relevant.products"></fmt:message></b>
                ${productTagsTitle}
            </div>
         </c:if>
         
		<%-- Social links (AddThis Button BEGIN)--%>
		<div class="addthis_toolbox addthis_default_style" addthis:url="${baseURL}${details.path}" addthis:title="${details.title}" addthis:description="${details.description}">
		    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		    <a class="addthis_button_tweet"></a>
		    <a class="addthis_button_linkedin_counter"></a>
		    <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
		    <a class="addthis_counter addthis_pill_style"></a>
		</div>
		<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
		<!-- AddThis Button END -->
         
        <%-- Download link--%>
        <c:if test="${not empty details.downloadType}">
            <%-- PDF Type --%>
            <c:if test="${details.downloadType eq 'pdf' && details.damAssetType eq mimeTypePDF && documentType}">
                <div class="left document featured-link">
                    <a rel="external" href="${details.damAssetPath}">
                        <fmt:message key="Download this document"></fmt:message>
                    </a>
                </div>
            </c:if>
            
            <%-- PPT Type --%>
            <c:if test="${details.downloadType eq 'ppt' && details.damAssetType eq mimeTypePPT && documentType}">
                <div class="left ppt featured-link">
                    <a href="${details.damAssetPath}">
                        <fmt:message key="Download this document"></fmt:message>
                    </a>
                </div>
            </c:if>
            
            <%-- External Link --%>
            <c:if test="${details.downloadType eq 'external' && not empty details.externalLink && not empty details.externalLinkTitle}">
                <div class="left featured-link">
                    <a href="${details.externalLink}">
                        ${details.externalLinkTitle}
                    </a>
                </div>
            </c:if>
        </c:if>
        
        <div class="clearfix"></div>
        
        <c:if test="${not empty details.description && empty properties.hideDescription}">
            <div class="feature-description">
            	${details.description}
            </div>
        </c:if>
        
        <c:if test="${properties.enablePdfToHtml && documentType && details.damAssetType eq mimeTypePDF}">
            <cq:include path="<%= PDF_TO_HTML %>" resourceType="<%= RTE_COMPONENT %>"/>
        </c:if>
    </section>
</article>

<c:if test="${not empty details.videoPlayer && videoType}">
    <script type="text/javascript">
        $(document).ready(function() {
            function createVideo() {
                BCL.createVideoWithNoAutoPlay(
                    "100%",
                    "100%",
                    "${details.playerId}",
                    "${details.playerKey}",
                    "${details.videoPlayer}",
                    "${details.id}");
                brightcove.createExperiences();
            }
            
            window.addEventListener('orientationchange', createVideo);  
            createVideo(); 
        });
    </script>
</c:if>