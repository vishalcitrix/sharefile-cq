<%--

    Global Sitemap
    
    Site map containing social links and site map links programically and optional.
    
    TODO: Recursively get the links.
    TODO: Handle custom links.
    TODO: Handle social link
    
    achew@siteworx.com

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%!
    public final String NUM_OF_COLUMN = "columns";
%>

<c:set var="columns" value="<%= properties.get(NUM_OF_COLUMN, Integer.class) %>"/>

<div class="footer-nav">
        <ul class="social-media">
            <li>
                <%  WCMMode mode = WCMMode.fromRequest(request); 
                    if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} %>
                <cq:include path="shareList" resourceType="g2a/components/content/shareList"/>
                <%  mode.toRequest(request); %>
            </li>
        </ul>
	<div class="columns-${columns}-container">
		<c:forEach var="i" begin="1" end="${columns}">      
    		<cq:include path="column-${i}" resourceType="citrixosd/components/content/sitemapNav"/> 
		</c:forEach>       
	</div>
</div>