<%--
     Primary Carousel component.
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode,
                com.day.cq.personalization.ClientContextUtil"%>

<%
   String currentMode = WCMMode.fromRequest(slingRequest).toString();
   //final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
   //pageContext.setAttribute("nodeId",containerId);
   pageContext.setAttribute("pageMode", currentMode);
%>
<c:choose>
    <c:when test="${pageMode eq 'EDIT'}">
        <ol id="tempCarousel" style="height:auto; overflow: hidden;">
        <style>
            #tempCarousel .owl-item-info {
                position: relative;
                padding: 1% 1%;              
            }
            #tempCarousel .link-sub-container {
                position: relative;
            }
            #tempCarousel h5 {
                font: bold 21px Arial, sans-serif;
                margin: 5px 0;
                color: #434B58;
            }
            #tempCarousel h4 {
                font-size: 14px;
                color: #2C323A;
                font-family: "helvetica neue md", Helvetica, Arial, sans-serif;
                line-height: 1.5rem;
            }
            #tempCarousel p {
                font: 16px/24px Arial, sans-serif;
                color: #666;
            }  */

            #tempCarousel a { 
                color: #FC6A06;
                font-size: 16px;
                font-family: Arial, sans-serif;
                display: block;
                margin-right: 3%;
                margin-top: 1.5%;
            }  
            #tempCarousel img {
                width: 250px;
            }
            #tempCarousel .icon {
			    background: url("static/images/resource-icons-sprite.png") no-repeat;
			    width: 82px;
			    min-height: 82px;
			    float: left;
			    margin-right: 55px;
			    position: absolute;
			    top: -100px;
			    right: 0;
			    background-color: transparent;
			}
			#tempCarousel .icon.none {
			        display: none;
			}
			#tempCarousel .icon.video {
			        background-position: -477px -48px;             
			}
			#tempCarousel .icon.pdf {
			      background: url("static/images/pdf-icon.svg") no-repeat;
			        background-size: 76%;
			}    
			#tempCarousel .arrow-pointer {
			    width: 0px;
			    height: 0px;
			    border-top: 4px solid transparent;
			    border-bottom: 4px solid transparent;
			    display: inline-block;
			    margin-left: 5px;
			    border-left: 7px solid #fc6a06;
			}
        </style>
            <c:forEach begin="1" end="${properties.itemscount}"
                varStatus="status">
                <li>            
                    <cq:include path="item-${status.count}" resourceType="/apps/g2m-redesign/components/content/resources/primaryCarousel/carouselItem" />
                </li>
            </c:forEach>
       </ol>
    </c:when>
    <c:otherwise>
        <div id="primaryCarousel" class="primary-carousel ${properties.colorScheme}<c:if test="${properties.itemscount eq 1}"> single</c:if>">
            <c:forEach begin="1" end="${properties.itemscount}" varStatus="status">
                <cq:include path="item-${status.count}" resourceType="/apps/g2m-redesign/components/content/resources/primaryCarousel/carouselItem" />
            </c:forEach>
        </div>
    </c:otherwise>
</c:choose>

<div class="clearBoth"></div>
<hr/>
