<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="prod" value="<%= request.getParameter("prod") != null ? request.getParameter("prod") : properties.get("manualProduct", null) %>"/>

<h3><fmt:message key="contactSales.needs.label"/></h3>
            
<ul class="row checkboxes">
    <li class="medium-6 columns"><label><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2m"/>" <c:if test="${prod == 'g2m'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <span><fmt:message key="contactSales.needs.g2m"/></span></label></li>
    <li class="medium-6 columns"><label><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2w"/>" <c:if test="${prod == 'g2w'}">checked</c:if> onclick="unCheck(this)" alt="G2W - Marketing"> <span><fmt:message key="contactSales.needs.g2w"/></span></label></li>
    <li class="medium-6 columns"><label><input type="checkbox" name="Products" value="<fmt:message key="form.product.g2t"/>" <c:if test="${prod == 'g2t'}">checked</c:if> onclick="unCheck(this)" alt="G2T - Training"> <span><fmt:message key="contactSales.needs.g2t"/></span></label></li>
    <li class="medium-6 columns"><label><input type="checkbox" name="Products" value="<fmt:message key="form.product.hidef"/>" <c:if test="${prod == 'audio'}">checked</c:if> onclick="unCheck(this)" alt="G2MC - Collaboration"> <span><fmt:message key="contactSales.needs.hdc"/></span></label></li>
    <li><label><input type="checkbox" name="Products" value="G2M & G2T & G2W" onClick="checkAll(this)" alt="G2MC - Collaboration"> <span><fmt:message key="contactSales.needs.all"/></span></li>
</ul>
