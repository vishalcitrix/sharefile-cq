<%--

   RSS Feed component.

  RSS feed components that display rss feed for given  rss feed url . 

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@ page import="com.citrixosd.models.RssFeedChannel" %><%
%><%@ page import="com.citrixosd.models.RssFeedItemCollection" %><%
%><%@ page import="com.citrixosd.models.RssFeedItem" %><%
%><%@include file="/apps/citrixosd/global.jsp" %><%
%><%@ page import="com.citrixosd.service.rssfeed.RssFeedReader" %><%
%><%@page session="false" %><%
%>

<%
   int noOfFeeds= properties.get("noOfFeeds", 5);
    String rss_url= properties.get("url", null);
    
    if(null!=rss_url){
    	try{
    		RssFeedReader rssFeedReader=  sling.getService(com.citrixosd.service.rssfeed.RssFeedReader.class);
    		RssFeedChannel ch = rssFeedReader.getChannel(rss_url);
    		RssFeedItemCollection items = ch.getItems();
            int index = 0;
            for (RssFeedItem item :items)
            {
                if(index<noOfFeeds){
                %>
                
                 
                 <%= item.getDescription() %>
                 <br>
                
                <% 
                index++;
                }else{break;}
                
            }
    	   }catch(Exception ex){
    		   %>
	        <c:if test="${(isEditMode || isReadOnlyMode) }">
	            <div>
	                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">          
	            </div>
	        </c:if>
    		   
    		   <% 
    	   }
            
        }
%>

<%--Display placeholder when url is not configured in edit mode  --%>
  
        <c:if test="${(isEditMode || isReadOnlyMode) && ( empty properties.url)}">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">          
            </div>
        </c:if>
      
<div class="clearBoth"></div> 
