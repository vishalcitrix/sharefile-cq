<%@include file="/apps/citrixosd/global.jsp"%>

<section class="search-results-container">
    <div>
        <h6>Results ${resourceList.start + 1}-${resourceList.end} of ${resourceList.total} for: <strong>${param['q']}</strong> (${resourceList.executionTime} seconds)</h6>
    </div>
    <div>
        <form id="resourceQueryForm" action="${resource.path}.pagination.0${resourceList.suffixPath}" method="get">
            <input type="text" name="q" placeholder="<fmt:message key="resource.search.resources"/>" value="${param['q']}"/>
            <button class="search" type="submit">Submit</button>
        </form>
    </div>
    <ul class="search-results-list">
        <c:forEach items="${resourceList.list}" var="resourceItem">
            <li class="resource-item">
                <div class="resource-title">
                    <h6>${resourceItem.title}</h6>
                </div>
                <div class="resource-description">
                    <span>${resourceItem.description}</span>
                </div>
            </li>
        </c:forEach>
    </ul>

    <c:choose>
        <c:when test="${resourceList.totalPages > 1}">
            <div class="pagination">
                <a class="previous active" href="${resource.path}.pagination.${(resourceList.currentPage - 1) > 0 ? resourceList.currentPage - 1 : 0}${resourceList.suffixPathWithQuery}">
                    <span>&#9668;</span> <fmt:message key="Previous"/>
                </a>
                <div class="pagination-wrapper">
                    <ul>
                        <li class="${resourceList.currentPage == 0 ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.0${resourceList.suffixPathWithQuery}">1</a></li>
                        <c:choose>
                            <c:when test="${resourceList.totalPages > 4}">
                                <c:if test="${resourceList.currentPage > 2}">
                                    <li class="margin-right"> ... </li>
                                </c:if>
                                <c:forEach begin="${(resourceList.currentPage - 1) > 1 ? ((resourceList.currentPage + 4) > resourceList.totalPages ? resourceList.totalPages - 4 : resourceList.currentPage - 1) : 1}" end="${(resourceList.currentPage) > 2 ? (resourceList.currentPage + 2 >= resourceList.totalPages ? resourceList.totalPages - 2 : resourceList.currentPage + 1) : resourceList.currentPage + (3 - resourceList.currentPage)}" varStatus="i">
                                    <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}">${i.index + 1}</a></li>
                                </c:forEach>
                                <c:if test="${(resourceList.totalPages - 3) > resourceList.currentPage}">
                                    <li class="last-three"> ... </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:forEach begin="1" end="${resourceList.totalPages - 2}" varStatus="i">
                                    <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a class="margin-right" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}">${i.index + 1}</a></li>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                        <li class="last-three ${resourceList.currentPage == (resourceList.totalPages - 1) ? 'active' : ''}"><a href="${resource.path}.pagination.${resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}">${resourceList.totalPages}</a></li>
                    </ul>
                </div>
                <a class="next" href="${resource.path}.pagination.${(resourceList.currentPage + 1) < resourceList.totalPages ? resourceList.currentPage + 1 : resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}">
                    <fmt:message key="Next"/> <span>&#9658;</span>
                </a>
            </div>
        </c:when>
        <c:otherwise>
            <div class="resource-list-no-results">
                <h1><fmt:message key="resource.list.no.results"/></h1>
            </div>
        </c:otherwise>
    </c:choose>
</section>

<script>
    $(document).ready(function() {
        var resourceListContainer = $("#" + "${resourceList.id}");
        resourceListContainer.find(".pagination a").each(function() {
            var paginationAnchor = $(this);
            paginationAnchor.unbind("click").click(function() {
                resourceListContainer.parent().load(paginationAnchor.attr("href"), function(){
                    dotDotDot.init();
                });
                return false;
            });
        });

        var resourceSubmitForm = $("#resourceQueryForm");
        resourceSubmitForm.unbind("submit").submit(function() {
            resourceListContainer.parent().load(resourceSubmitForm.attr("action"), resourceSubmitForm.serialize(), function() {
                dotDotDot.init();
            });
            return false;
        });
    });
</script>