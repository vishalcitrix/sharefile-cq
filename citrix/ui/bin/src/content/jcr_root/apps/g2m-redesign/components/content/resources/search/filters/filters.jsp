<%@ page import="com.citrixosdRedesign.constants.ResourceConstants" %>
<%@ page import="com.day.cq.tagging.TagManager" %>
<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.day.cq.personalization.ClientContextUtil" %>
<%--
    Filters

    achew@siteworx.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<cq:setContentBundle/>

<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

    String[] products = properties.get(ResourceConstants.PRODUCTS, String[].class);
    String[] categories = properties.get(ResourceConstants.CATEGORIES, String[].class);
    String[] featured = properties.get(ResourceConstants.FEATURED, String[].class);
    String[] topics = properties.get(ResourceConstants.TOPICS, String[].class);
    String[] industries = properties.get(ResourceConstants.INDUSTRIES, String[].class);

    String resourceListPath = null;
    String resourceListId = null;
    if(currentNode != null) {
        final Node pageJcrNode = currentNode.getParent();
        if(pageJcrNode.hasNode("searchResults")) {
            final Node resourceNode = pageJcrNode.getNode("searchResults");
            resourceListPath = resourceNode.getPath();
            resourceListId = ClientContextUtil.getId(resourceListPath);

            final ValueMap resourceProperties = resourceResolver.getResource(resourceListPath).adaptTo(ValueMap.class);

            if(products == null || products.length == 0) {
                products = resourceProperties.get(ResourceConstants.PRODUCTS, String[].class);
            }
            if(categories == null || categories.length == 0) {
                categories = resourceProperties.get(ResourceConstants.CATEGORIES, String[].class);
            }
            if(featured == null || featured.length == 0) {
                featured = resourceProperties.get(ResourceConstants.FEATURED, String[].class);
            }
            if(topics == null || topics.length == 0) {
                topics = resourceProperties.get(ResourceConstants.TOPICS, String[].class);
            }
            if(industries == null || industries.length == 0) {
                industries = resourceProperties.get(ResourceConstants.INDUSTRIES, String[].class);
            }
        }
    }
%>

<c:set var="resourceListPath" value="<%= resourceListPath %>"/>
<c:set var="resourceListId" value="<%= resourceListId %>"/>
<c:set var="products" value="<%= ResourceUtils.getTags(products, tagManager)%>"/>
<c:set var="categories" value="<%= ResourceUtils.getTags(categories, tagManager)%>"/>
<c:set var="featured" value="<%= ResourceUtils.getTags(featured, tagManager)%>"/>
<c:set var="topics" value="<%= ResourceUtils.getTags(topics, tagManager)%>"/>
<c:set var="industries" value="<%= ResourceUtils.getTags(industries, tagManager)%>"/>

<div class="filters-container" data-resource-list-path="${resourceListPath}" data-resource-list-id="${resourceListId}" data-products="" data-categories="" data-featured="" data-topics="" data-industries="">
    <%-- Products --%>
    <c:if test="${not empty products && fn:length(products) > 0}">
        <ul class="drop-down-container">
            <li><a class="drop-down-header" href="#"><span class="drop-down-header-text"><fmt:message key="select.products"/></span><span class="drop-down-arrow"></span></a></li>
            <li class="filter" data-filter="products">
                <ul class="drop-down-list">
                    <c:forEach items="${products}" var="product">
                        <li><input type="checkbox" value="${product.tagID}"/>${product.title}</li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:if>

    <%-- Categories --%>
    <c:if test="${not empty categories && fn:length(categories) > 0}">
        <ul class="drop-down-container">
            <li><fmt:message key="select.categories"/></li>
            <li><a class="drop-down-header" href="#"><span class="drop-down-header-text"><fmt:message key="select.categories"/></span><span class="drop-down-arrow"></span></a></li>
            <li class="filter" data-filter="categories">
                <ul class="drop-down-list">
                    <c:forEach items="${categories}" var="category">
                        <li><input type="checkbox" value="${category.tagID}"/>${category.title}</li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:if>

    <%-- Featured --%>
    <c:if test="${not empty featured && fn:length(featured) > 0}">
        <ul class="drop-down-container">
            <li><a class="drop-down-header" href="#"><span class="drop-down-header-text"><fmt:message key="select.featured"/></span><span class="drop-down-arrow"></span></a></li>
            <li class="filter" data-filter="featured">
                <ul class="drop-down-list">
                    <c:forEach items="${featured}" var="feature">
                        <li><input type="checkbox" value="${feature.tagID}"/>${feature.title}</li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:if>

    <%-- Topics --%>
    <c:if test="${not empty topics && fn:length(topics) > 0}">
        <ul class="drop-down-container">
            <li><a class="drop-down-header" href="#"><span class="drop-down-header-text"><fmt:message key="select.topics"/></span><span class="drop-down-arrow"></span></a></li>
            <li class="filter" data-filter="topics">
                <ul class="drop-down-list">
                    <c:forEach items="${topics}" var="topic">
                        <li><input type="checkbox" value="${topic.tagID}"/>${topic.title}</li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:if>

    <%-- Industries --%>
    <c:if test="${not empty industries && fn:length(industries) > 0}">
        <ul class="drop-down-container">
            <li><a class="drop-down-header" href="#"><span class="drop-down-header-text"><fmt:message key="select.industries"/></span><span class="drop-down-arrow"></span></a></li>
            <li class="filter" data-filter="industries">
                <ul class="drop-down-list">
                    <c:forEach items="${industries}" var="industry">
                        <li><input type="checkbox" value="${industry.tagID}"/>${industry.title}</li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:if>
</div>

<script>

    $(".filters-container").each(function() {
        var searchRefinementsContainer = $(this);
        var filters = searchRefinementsContainer.find(".filter");
        filters.each(function(){
            var filter = $(this);
            var filterProperty = filter.attr("data-filter");
            filter.click(function(){
                var listContainer = $(this);
                var listHeader = listContainer.children(".drop-down-header");
                var listHeaderText = listHeader.children(".drop-down-header-text");
                var listDropDown = listContainer.children(".drop-down-list");
                listDropDown.find("a").each(function() {
                    var anchor = $(this);
                    anchor.unbind("click").click(function() {
                        listDropDown.children("li").removeClass("active");
                        anchor.parent().addClass("active");
                        listHeaderText.html(anchor.html());
                        searchRefinementsContainer.attr("data-" + filterProperty, anchor.attr("href"));
                        resourceSearchResultQuery(searchRefinementsContainer);
                        listDropDown.hide();
                        return false;
                    });
                });
                searchRefinementsContainer.find('.drop-down-list').not(listDropDown).hide();
                listDropDown.toggle();
            });
        });
    });

    function resourceSearchResultQuery(container) {
        var searchRefinementsContainer = $(container);

        var resourceListPath = searchRefinementsContainer.attr("data-resource-list-path");
        var resourceListId = searchRefinementsContainer.attr("data-resource-list-id");
        var resourceSubmitForm = $("#resourceQueryForm");

        var products = searchRefinementsContainer.attr("data-products");
        var categories = searchRefinementsContainer.attr("data-categories");
        var featured = searchRefinementsContainer.attr("data-featured");
        var topics = searchRefinementsContainer.attr("data-topics");
        var industries = searchRefinementsContainer.attr("data-industries");

        var path = resourceListPath;
        path += ".pagination.0.startQuery";
        path += ".products";
        path += "." + products;
        path += ".categories";
        path += "." + categories;
        path += ".featured";
        path += "." + featured;
        path += ".topics";
        path += "." + topics;
        path += ".industries"
        path += "." + industries;
        path += ".endQuery"
        path += ".html";

        $("#" + resourceListId).load(path, resourceSubmitForm.serialize(), function() {
            dotDotDot.init();
        });
    }

</script>