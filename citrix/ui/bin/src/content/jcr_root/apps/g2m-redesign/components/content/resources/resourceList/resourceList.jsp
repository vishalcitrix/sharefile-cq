<%--
    Resource List

    achew@siteworx.com
    vishal.gupta@citrix.com
--%>

<%@ page import="com.citrixosdRedesign.utils.ResourceUtils" %>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%@ page import="org.apache.jackrabbit.commons.JcrUtils" %>
<%@ page import="com.day.cq.commons.jcr.JcrConstants" %>

<%@include file="/apps/citrixosd/global.jsp" %>
<cq:setContentBundle/>

<%
    //Create itself if it is not already created because this is required for filtering
    if(currentNode == null && WCMMode.fromRequest(request).equals(WCMMode.EDIT)) {
        final Session session = resourceResolver.adaptTo(Session.class);
        final Node newNode = JcrUtils.getOrCreateByPath(resource.getPath(), JcrConstants.NT_UNSTRUCTURED, session);
        newNode.setProperty("sling:resourceType", component.getResourceType());
        session.save();
        response.sendRedirect(currentPage.getPath());
    }
	
	String colorScheme = null;
	
	final Node pageJcrNode = currentNode.getParent();
	if(pageJcrNode.hasNode("filter")) {
		final Node filterNode = pageJcrNode.getNode("filter");
		if(filterNode.hasProperty("colorScheme")){
			colorScheme = filterNode.getProperty("colorScheme").getString();
		}
	}
%>

<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="resourceList" value="<%= ResourceUtils.getResourceList(resource, currentPage, request, null, currentDesign) %>" scope="request"/>
<c:set var="colorScheme" value="<%= colorScheme %>"/>

<div id="${resourceList.id}" <c:if test="${colorScheme ne null}">class="${colorScheme}"</c:if>>
    <cq:include script="content.jsp"/>
</div>