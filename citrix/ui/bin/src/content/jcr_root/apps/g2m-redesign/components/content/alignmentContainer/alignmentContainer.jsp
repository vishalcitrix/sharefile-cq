<%--
    Alignment Container
    - Options to add padding/margin/alignment for desktop, tablet and mobile. 
      There are various options to align a element.
    
    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="margPad" value="${not empty properties.usePadding ? 'padding' : 'margin'}"/>
<c:choose>
	<c:when test="${not isEditMode}">     
		<div class="styleswitch" style="<c:if test='${not empty properties.marginDesktop}'>${margPad}:${properties.marginDesktop};</c:if>${not empty properties.alignmentDesktop ? properties.alignmentDesktop : ''}" data-large="<c:if test='${not empty properties.marginDesktop}'>${margPad}:${properties.marginDesktop};</c:if>${not empty properties.alignmentDesktop ? properties.alignmentDesktop : ''}" data-medium="<c:if test='${not empty properties.marginTablet}'>${margPad}:${properties.marginTablet};</c:if>${not empty properties.alignmentTablet ? properties.alignmentTablet : ''}" data-small="<c:if test='${not empty properties.marginSmall}'>${margPad}:${properties.marginSmall};</c:if>${not empty properties.alignmentSmall ? properties.alignmentSmall : ''}">
	</c:when>
	<c:otherwise>
		<div>
	</c:otherwise>
</c:choose>
    <c:choose>
        <c:when test="${not isEditMode}">     
            <div class="styleswitch" style="${not empty properties.leftCenterDesktop ? 'display:inline-block; text-align:left;' : ''}<c:if test='${not empty properties.widthDesktop}'>width:${properties.widthDesktop}px; ${properties.alignmentDesktop eq 'text-align:center;' ? 'margin: 0 auto;' : ''}</c:if>" data-large="${not empty properties.leftCenterDesktop ? 'display:inline-block; text-align:left;' : ''}<c:if test='${not empty properties.widthDesktop}'>width:${properties.widthDesktop}px; ${properties.alignmentDesktop eq 'text-align:center;' ? 'margin: 0 auto;' : ''}</c:if>" data-medium="${not empty properties.leftCenterTablet ? 'display:inline-block; text-align:left;' : ''}<c:if test='${not empty properties.widthTablet}'>width:${properties.widthTablet}px; ${properties.alignmentTablet eq 'text-align:center;' ? 'margin: 0 auto;' : ''}</c:if>" data-small="${not empty properties.leftCenterPhone ? 'display:inline-block; text-align:left;' : ''}<c:if test='${not empty properties.widthSmall}'>width:${properties.widthSmall}px; ${properties.alignmentSmall eq 'text-align:center;' ? 'margin: 0 auto;' : ''}</c:if>">
        </c:when>
        <c:otherwise>
            <div>
        </c:otherwise>
    </c:choose>
        <c:choose>
            <c:when test="${properties.disableContent == true}">
                <div>&nbsp;</div>
            </c:when>
            <c:otherwise>
                <cq:include path="margin_container_content_parsys" resourceType="foundation/components/parsys"/>
            </c:otherwise>
        </c:choose> 
    </div>
</div>
<cq:include path="endofsection" resourceType="g2m-redesign/components/content/endComponent"/>
