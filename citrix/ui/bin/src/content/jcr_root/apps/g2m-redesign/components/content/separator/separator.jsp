<%--
    Separator Component - Creates an <div> tag with border bottom, adjustable width, separator type and alignment options.

    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp" %>

<c:set var="alignmentCss" value="float : ${properties.alignment}" />
<div class="hr ${properties.seperatorType}" style="width: ${not empty properties.width ? properties.width : '100%'}; <c:if test="${not empty properties.color}">border-top-color: ${properties.color};</c:if> ${fn:contains(alignmentCss, 'left') || fn:contains(alignmentCss, 'right') ? alignmentCss : 'margin-left : auto; margin-right : auto;'}"></div>
<c:if test="${fn:contains(alignmentCss, 'left') || fn:contains(alignmentCss, 'right')}">
    <div style="clear:both"></div>
</c:if>