<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    Note: Depends on logo and linkSet component.
    
    vishal.gupta@citrix.com
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:choose>
    <c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed) && properties.hideLinkSet}">     
        <div class="topNav topNavSolid">
    </c:when>
    <c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed || properties.hideLinkSet)}">     
        <div class="topNav topNavTransition">
    </c:when>
    <c:otherwise>
        <div class="topNav" style="position:inherit !important;">
    </c:otherwise>
</c:choose>

    <div class="left">
        <swx:setWCMMode mode="READ_ONLY">
            <cq:include script="logo.jsp"/>
        </swx:setWCMMode>
    </div>
    <div class="right">
        <div class="left">
            <ul>
                <%-- showing launcher only for medium and small devices --%>
            	<c:if test="${empty properties.hideLauncher}">
                    <li>
                		<a class="icon-rocket-1 show-for-small-only launcher" href="${not empty properties.launcher ? properties.launcher : ''}"></a>
                    </li>
            	</c:if>
            	<li>
            		<swx:setWCMMode mode="READ_ONLY">
                		<div class="${not empty properties.theme ? properties.theme : 'g2m'}">
                    		<cq:include script="linkSet1.jsp"/>
                		</div>
            		</swx:setWCMMode>
            	</li>
            </ul>
            
            <swx:setWCMMode mode="READ_ONLY">
                <div class="${not empty properties.theme ? properties.theme : 'g2m'}">
                    <cq:include script="linkSet2.jsp"/>
                </div>
            </swx:setWCMMode>
            <div class="clearBoth"></div>
        </div>
        <div id="menu" class="right ${not empty properties.darkMenu ? 'dark' : ''}">
            <div class="hamburger-icon hide-for-medium-up"></div>
            <div class="hamburger-icon show-for-medium-up"><fmt:message key="global.menu"/></div>
        </div>
        <div class="clearBoth"></div>
    </div>
    <div class="show-for-small-only sec-name"></div>
    <div class="clearBoth"></div>
</div>