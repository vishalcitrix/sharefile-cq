<%@include file="/apps/citrixosd/global.jsp"%>
<div>
	<c:if test="${not empty properties.fileReference}">
		<div>
			<img src="${properties.fileReference}" />
		</div>
	</c:if>
    <p>
        ${properties.name}<c:if test="${not empty properties.title}">, ${properties.title}</c:if>
    </p>
    <p>
        ${properties.company}<c:if test="${not empty properties.city}">, ${properties.city}</c:if><c:if test="${not empty properties.state}">, ${properties.state}</c:if>
    </p>
</div>