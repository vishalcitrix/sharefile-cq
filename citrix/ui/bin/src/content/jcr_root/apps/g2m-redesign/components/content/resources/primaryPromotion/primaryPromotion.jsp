<%--
     Primary Promotion component.
--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

 <%!
 	private static final String ICON = "icon";	
 	private static final String ASSET_ICON = "assetIcon";
	private static final String TITLE_PROPERTY = "title";
 	private static final String FEATURE_TITLE_PROPERTY = "featureTitle";
 	private static final String FEATURE_LINK_PROPERTY = "featureLink";
 	private static final String FEATURE_DESC_PROPERTY = "featureDescription";
 	private static final String CALL_TO_ACTION_DESC_PROPERTY = "callToActionDesc";
 	private static final String CALL_TO_ACTION_LINK_PROPERTY = "callToActionLink";
%>

<c:set var="icon" value="<%= properties.get(ICON, "") %>"/>
<c:set var="assetIcon" value="<%= properties.get(ASSET_ICON, "") %>"/>
<c:set var="title" value="<%= properties.get(TITLE_PROPERTY, "") %>"/>
<c:set var="featureTitle" value="<%= properties.get(FEATURE_TITLE_PROPERTY, "") %>"/>
<c:set var="featureLink" value="<%= properties.get(FEATURE_LINK_PROPERTY, "") %>"/>
<c:set var="featureDescription" value="<%= properties.get(FEATURE_DESC_PROPERTY, "") %>"/>
<c:set var="callToActionDesc" value="<%= properties.get(CALL_TO_ACTION_DESC_PROPERTY, "") %>"/>
<c:set var="callToActionLink" value="<%= properties.get(CALL_TO_ACTION_LINK_PROPERTY, "") %>"/>
<div class="primary-promotion"> 
	<h2><span class="icon ${icon}"></span>${title}</h2>
	<cq:include path="image" resourceType="g2m-redesign/components/content/imageRenditions"/>
	<section><section class="primary-promo-desc primary-promo-right">
		<c:choose>
			<c:when test="${not empty featureLink}">
				<a href="${featureLink}">	
		    		<cq:text value="${featureTitle}" tagName="h3" escapeXml="false"/>
		    	</a>
		    </c:when>
		    <c:otherwise>
		    	<cq:text value="${featureTitle}" tagName="h3" escapeXml="false"/>
		    </c:otherwise>
	    </c:choose>
		<cq:text value="${featureDescription}" tagName="p" escapeXml="false"/>
	</section>
	<div class="primary-promo-right primary-promo-footer">
		<a class="link-description" href="${callToActionLink}">${callToActionDesc} &#9658;</a>
		<!-- AddThis Button BEGIN -->
	     <div class="addthis_toolbox addthis_default_style" addthis:url="${featureLink}" addthis:title="${featureTitle}" addthis:description="${featureDescription}">
	         <a class="addthis_button_preferred_1"></a>
	         <a class="addthis_button_preferred_2"></a>
	         <a class="addthis_button_preferred_3"></a>
	         <a class="addthis_button_preferred_4"></a>
	         <a class="addthis_button_compact"></a>
	         <a class="addthis_counter addthis_bubble_style"></a>
	     </div>
	     <!-- AddThis Button END -->

        <c:if test="${assetIcon != '' || assetIcon != 'none'}">
	     <%--   <div class="doc-icon">--%>
	            <div class="asset-icon ${assetIcon}"></div>
	        <%--</div>--%>
        </c:if>	   	
	</div>
</div>
<hr>