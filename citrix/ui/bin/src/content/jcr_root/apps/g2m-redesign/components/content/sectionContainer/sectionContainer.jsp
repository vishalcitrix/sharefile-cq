<%--
    Section Container
    
    Creates section with options to create unique name or predefined name. Option to add margin as well.
    
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com
--%>

<%@page import="com.day.cq.wcm.api.components.Toolbar,
                com.day.cq.tagging.Tag,
                com.day.cq.tagging.TagManager,
                java.util.ArrayList,
                java.util.List,
                com.citrixosd.utils.Utilities"%>
                
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%@page import="com.day.cq.personalization.ClientContextUtil"%>

<%
    final String sectionId = properties.get("id","");   
    final String sectionName = properties.get("name","");
    final String analyticsTemplate = properties.get("template","");
    final String[] contentType = properties.get("contentType", String[].class);
    final String[] subSection = properties.get("subSection", String[].class);
    final String analyticsEnableTracking = properties.get("enableTracking","");
    final String analyticsTrackWhenVisible = properties.get("trackWhenVisible","");
    String contentTypeTagStr = "";
    String subSectionTagStr = "";
    
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    final Tag contentTypeTag = Utilities.getFirstTag(contentType, tagManager);
    final Tag subSectionTag = Utilities.getFirstTag(subSection, tagManager);
    if(contentTypeTag != null){
        contentTypeTagStr = contentTypeTag.getTagID();
    }
    if(subSectionTag != null){
        subSectionTagStr = subSectionTag.getTagID();
    }
    
    final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
    
    if (editContext != null && editContext.getEditConfig() != null) {
        if(sectionId == "" && editContext.getEditConfig().getToolbar().size() > 0){
            editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Section Container - (Please select id)"));  
        }else{
            if(editContext.getEditConfig().getToolbar().size() > 0)
                editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Section Container - (id: " + sectionId + ")"));     
        }
        if(editContext.getEditConfig().getToolbar().size() > 0)
            editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
    }

%>

<c:set var="containerId" value="<%= containerId %>"/>
<c:set var="id" value="<%= sectionId %>"/>
<c:set var="sectionName" value="<%= sectionName %>"/>
<c:set var="template" value="<%= analyticsTemplate %>"/>
<c:set var="contentType" value="<%= contentTypeTagStr %>"/>
<c:set var="subSection" value="<%= subSectionTagStr %>"/>
<c:set var="enableTracking" value="<%= analyticsEnableTracking %>"/>
<c:set var="trackWhenVisible" value="<%= analyticsTrackWhenVisible %>"/>

<section class="targetsection" id="${id eq '' ? containerId : id}"
    <c:if test="${not empty sectionName}">name="${sectionName}"</c:if>
    <c:if test="${not empty template}">data-analytics-template="${template}"</c:if>
    <c:if test="${not empty contentType}">data-analytics-content-type="${fn:replace(contentType, 'analytics-content-type:', '')}"</c:if>
    <c:if test="${not empty subSection}">data-analytics-sub-section="${fn:replace(subSection, 'analytics-subsection:', '')}"</c:if>
    <c:if test="${not empty enableTracking}">data-analytics-enable-tracking="${enableTracking}"</c:if>
    <c:if test="${not empty trackWhenVisible}">data-analytics-track-when-visible="${trackWhenVisible}"</c:if>
>
    <cq:include path="section_container_parsys" resourceType="foundation/components/parsys"/>
</section>

<cq:include path="endofsection" resourceType="g2m-redesign/components/content/endComponent"/>