<%--
  Trial Form
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<c:set var="divAttr" value=""/>            
<c:set var="customClass" value="${properties['customclass']}"/>
<c:set var="formaction" value="${properties['formaction']}"/>
<c:set var="excludePassword" value="${properties['excludepassword']}"/>
<c:set var="buttontext" value="${properties['buttontext']}"/>

<c:if test="${buttontext eq ''}">
	<c:set var="buttontext" value="Start My Free Trial"/>
</c:if>
<c:if test="${formaction eq ''}">
	<c:set var="formaction" value="https://www.sharefile.com/trial/starttrial.aspx"/>
</c:if>

<div class="custom-form trial-form-comp ${customclass}">
	<div id="formErrorMsg" class="error-message">
		<span class="icon-Warning"></span>
		<fmt:message key="sf.form.message.error"/>
		<div class="notch"></div>
	</div>

	<form name="trial-form" action="${formaction}" method="post">
		<div class="rowC">
			<div class="large-6 medium-6 columns">
				<input type="text" class="required" id="firstname" name="FirstName" constraint="None" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.firstName"/>" errormsg="<fmt:message key="sf.form.error.firstName"/>">
			</div>
			<div class="large-6 medium-6 columns columnRight">
				<input type="text" class="required" id="lastname" name="LastName" constraint="None" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.lastName"/>" errormsg="<fmt:message key="sf.form.error.lastName"/>">
			</div>
		</div>
		<div class="rowC">
			<input type="text" class="required" id="email" placeholder="Email" name="Email" constraint="email" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.email"/>" errormsg="<fmt:message key="sf.form.error.email"/>">
		</div>
	
		<c:if test="${excludePassword ne 'yes'}"> 
			<div class="rowC">
				<div class="password-message"><fmt:message key="sf.form.message.password"/><div class="notch"></div></div>
				<input type="password" class="required" id="password" name="Password" constraint="password" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.password"/>">
			</div>
		</c:if>

		<div class="trial-form-button-container">
		    <input class="button green" type="submit" value="${buttontext}">
		    <div class="clearBoth"></div>
		</div>
	</form>
</div>