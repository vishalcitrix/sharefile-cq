<%--
  Button
  - This component will consist of custom css buttons which will contain a 
    label and a link. The button also allows an optional sub text displayed 
    under the button. Default button style is set to none.
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String CSS_PROPERTY = "css";
    public static final String LABEL_PROPERTY = "label";
    public static final String PATH_PROPERTY = "path";
    public static final String LINK_OPTION_PROPERTY = "linkOption";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, "none") %>"/>
<c:set var="label" value="<%= properties.get(LABEL_PROPERTY, "") %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, "") %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>

<c:choose>
    <c:when test="${css eq 'none' || empty label}">
		<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) ||  WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
			<div class="button-container" style="text-align:${properties.alignment};">
				<div class="none">Please Complete Editing</div>
			</div>
        </c:if>
    </c:when>
    <c:otherwise>
    	<div class="button-container" style="text-align:${properties.alignment};">
	        <a class="${css} ${properties.rectangular ? 'rectangle':''}" href="${path}" rel="${linkOption}" <c:if test="${not empty linkOption}">rel="${linkOption}"</c:if>>
	            <c:if test="${not empty label}"> <fmt:message key="${label}"/></c:if>                  
	        </a>
        </div>
    </c:otherwise>
</c:choose>