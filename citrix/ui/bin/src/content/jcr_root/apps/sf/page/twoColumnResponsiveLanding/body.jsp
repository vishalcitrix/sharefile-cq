<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

 <body>
 	<c:if test="${not isEditMode}">
 		<!-- Google Tag Manager -->
	    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-XCGM"
	    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	    })(window,document,'script','dataLayer','GTM-XCGM');</script>
	    <!-- End Google Tag Manager -->
 	</c:if>
 	
 	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
    	<div class="row full-width">
		  <div class="large-8 columns">
		    <cq:include path="leftContent" resourceType="foundation/components/parsys"/>
		  </div>     
		  <div class="large-4 columns">
		     <cq:include path="rightRailiPar" resourceType="foundation/components/iparsys"/>
		  </div>
		</div>
		<div class="row full-width">
		  <div class="large-12 columns">
		    <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
		  </div>
		</div>
		<cq:include path="footerContent" resourceType="swx/component-library/components/content/single-ipar"/> 
	</div>  
    <cq:include script="footer.jsp" />
</body>