<%--
  Section Heading
  - Option to add Section heading and sub heading
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
                
<c:set var="headingValue" value="${not empty properties.heading ? properties.heading : 'Heading goes here'}"/>
<c:set var="subheadingValue" value="${not empty properties.subheading ? properties.subheading : 'Sub Heading goes here'}"/>
<c:set var="hasDarkBg" value="${properties.hasDarkBg}"/>

<c:if test="${(isEditMode or isReadOnlyMode) or (not empty headingValue and not empty subheadingValue)}">
	<div <c:if test="${hasDarkBg}">class="hasDarkBg"</c:if>>
		<cq:text value="${headingValue}" tagName="h2" escapeXml="false"/>
		<cq:text value="${subheadingValue}" tagName="h3" escapeXml="false"/>
	</div>
</c:if>
<div class="clearBoth"></div>