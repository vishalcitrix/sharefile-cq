<%--
    Link List
    
    A collection of links which can have customizable properties. The link list 
    will also have a css class for the first and last item so it would be easier 
    to apply css classes to them. Link text is i18n.
    
    Also option to check if link is same as current page and add 'current' class to li
    
    vishal.gupta@citrix.com 
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String LINKS_PROPERTY = "links";

    public class Link {
        private String css;
        private String text;
        private String path;
        private String linkOption;
        
        public Link() {
            
        }
        public void setCss(String css) {
            this.css = css;
        }
        public String getCss() {
            return this.css;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getText() {
            return this.text;
        }
        public void setPath(String path) { 
            this.path = path;
        }
        public String getPath() {
            return this.path;
        }
        public void setLinkOption(String linkOption) {
            this.linkOption = linkOption;
        }
        public String getLinkOption() {
            return this.linkOption;
        }
    }
    
    public List<Link> getLinks(NodeIterator nodeIter) {
        final List<Link> shares = new ArrayList<Link>();
            while(nodeIter.hasNext()) {
                try {
                    final Node currLink = nodeIter.nextNode();
                    final Link link = new Link();
                    link.setCss(currLink.hasProperty("css") ? currLink.getProperty("css").getString() : "");
                    link.setText(currLink.hasProperty("text") ? currLink.getProperty("text").getString() : "");
                    link.setPath(currLink.hasProperty("path") ? currLink.getProperty("path").getString() : "");
                    link.setLinkOption(currLink.hasProperty("linkOption") ? currLink.getProperty("linkOption").getString() : "");
                    
                    if(link.getCss() != null || link.getText() != null || link.getPath() != null) {
                        shares.add(link);	
                    }
                } catch (RepositoryException re) {
                    re.printStackTrace();
                } 
            }
        return shares;
    }

%>

<%
    List<Link> links = null;
    if (currentNode != null && currentNode.hasNode(LINKS_PROPERTY)) {
        final Node baseNode = currentNode.getNode(LINKS_PROPERTY);
        final NodeIterator nodeIter = baseNode.getNodes();
        links = getLinks(nodeIter);
    }
    
    final Map<String, Object> linkList = new HashMap<String, Object>();
    linkList.put(LINKS_PROPERTY, links);
%>

<c:set var="linkList" value="<%= linkList %>"/>
<c:set var="currentPageCheck" value="<%= properties.get("isCurrentPage", false) %>"/>
<c:set var="currentPagePath" value="<%= currentPage.getPath() %>"/>

<c:choose>
    <c:when test="${fn:length(linkList.links) > 0}">
        <ul>
            <c:forEach items="${linkList.links}" begin="0" var="link" varStatus="i">
            	<c:set var="tel" value="tel:${link.path}" />
                <c:choose>
                    <c:when test="${link.linkOption eq '_blank'}">
                        <li <c:if test="${currentPageCheck && link.path eq currentPagePath}">class="current"</c:if>>
                        	<a href="${link.path}" target="${link.linkOption}" id="${link.css}">
              					<c:if test="${not empty link.css && link.css ne 'signIn'}">
              						<span class="${link.css}"></span>
              					</c:if>
              					${link.text}
              				</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li <c:if test="${currentPageCheck && link.path eq currentPagePath}">class="current"</c:if>>	
                       		<a href="${link.path}" rel="${link.linkOption}" id="${link.css}">
               					<c:if test="${not empty link.css && link.css ne 'signIn'}">
               						<span class="${link.css}"></span>
               					</c:if>
               					${link.text}
               				</a>
						</li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>