<%--
  Plan Features container component.
  This component can be used to display plan features details of configured plans.
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>

<%!
    private static final String PLAN_DATA_NODE = "features";
 %>
<%
    final ArrayList<Map<String, Property>> planFetures = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode(PLAN_DATA_NODE)) {
        planFetures.addAll(Utilities.parseStructuredMultifield(currentNode.getNode(PLAN_DATA_NODE)));
    }
%>
<c:set var="planFetures" value="<%= planFetures %>"/>

<div class="planAndPricing show-for-medium-up">
    <c:forEach items="${planFetures}" var="feature">
        <div class="row">
            <div class="medium-2 large-2 columns">
                <div class="left-row plus">
                    <span>${feature.featureLabel.string}</span>
                </div>
            </div>
            <div class="medium-10 large-10 columns">
            <div class="row">
                <div class="medium-3 large-3 columns basic">
                    <c:choose>
                        <c:when test="${not empty feature.basicTextValue.string and feature.showTextValue.string eq 'true'}">
                            <p>${feature.basicTextValue.string}</p>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${feature.enableFeatureForBasic.string eq 'true'}">
                                    <p class="check"><span class="icon-SolidCheck"></span></p>
                                </c:when>
                                <c:when test="${empty feature.enableFeatureForBasic.string}">
                                    <p class="check icon-X"></p>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="small-12 medium-3 large-3 columns professional">
                    <c:choose>
                        <c:when test="${not empty feature.professionalTextValue.string and feature.showTextValue.string eq 'true'}">
                            <p>${feature.professionalTextValue.string}</p>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${feature.enableFeatureForProfessional.string eq 'true'}">
                                    <p class="check"><span class="icon-SolidCheck"></span></p>
                                </c:when>
                                <c:when test="${empty feature.enableFeatureForProfessional.string}">
                                    <p class="check icon-X"></p>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="small-12 medium-3 large-3 columns corprate">
                    <c:choose>
                        <c:when test="${not empty feature.corporateTextValue.string and feature.showTextValue.string eq 'true'}">
                            <p>${feature.corporateTextValue.string}</p>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${feature.enableFeatureForCorporate.string eq 'true'}">
                                    <p class="check"><span class="icon-SolidCheck"></span></p>
                                </c:when>
                                <c:when test="${empty feature.enableFeatureForCorporate.string}">
                                    <p class="check icon-X"></p>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="small-12 medium-3 large-3 columns VirtualDataRoom">
                    <c:choose>
                        <c:when test="${not empty feature.virtualDataRoomTextValue.string and feature.showTextValue.string eq 'true'}">
                            <p>${feature.virtualDataRoomTextValue.string}</p>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${feature.enableFeatureForVirtualDataRoom.string eq 'true'}">
                                    <p class="check"><span class="icon-SolidCheck"></span></p>
                                </c:when>
                                <c:when test="${empty feature.enableFeatureForVirtualDataRoom.string}">
                                    <p class="check icon-X"></p>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 large-12 columns features" >
                <p>${feature.featureDesc.string}</p>
            </div>
        </div>
        </div>
    </c:forEach>
 </div>
<div class="clearBoth"></div>