<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page import="java.util.Locale, com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    String designPath = currentDesign.getPath();
    String designPathOrig = currentDesign.getPath();
    String transformed = transformer.transform(designPath);
    if (transformed != null) {
        designPath = transformed;
    }
    
    String currentMode = WCMMode.fromRequest(slingRequest).toString();
    pageContext.setAttribute("pageMode", currentMode);
%>

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />

<head>
	<cq:include script="abtest.jsp"/>
    
    <%-- Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width" />
    <cq:include script="meta.jsp"/>
    
    <link rel="stylesheet" href="${designPathOrig}/css/static.css" type="text/css">
    
    <%--CSS --%>
    <!--[if lt IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/app-ie.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
    	<link rel="stylesheet" href="${designPath}/css/app.css" type="text/css">
        <link rel="stylesheet" href="${designPath}/css/ie9.css" type="text/css">
    <![endif]-->
    <!--[if gte IE 9]><link rel="stylesheet" href="${designPath}/css/app.css" type="text/css"><![endif]-->
    <!--[if !IE]><!--> <link rel="stylesheet" href="${designPathOrig}/css/app.css" type="text/css"><!--<![endif]-->
    
    <%--Icons --%>
    <link rel="Shortcut Icon" href="${designPathOrig}/css/static/images/favicon.ico">
    
    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    
    <%-- Javascripts - Place after including jQuery Lib --%>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
	
    <%-- Adding Pollyfil --%>
    <!--[if lt IE 9]>
        <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/respond.js"></script>
    <![endif]-->
    
	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
</head>