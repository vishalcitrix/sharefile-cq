<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Emulator init script.

  Draws the emulator initialization code. This is usually called by the head.jsp
  of the page.

  ==============================================================================

--%><%
%><%@ page import="com.day.cq.wcm.mobile.api.device.DeviceGroup,
                   com.day.cq.wcm.mobile.api.device.DeviceGroupList,
                   com.day.cq.wcm.emulator.Emulator,
                   static com.day.cq.wcm.mobile.api.MobileConstants.HTML_ID_CONTENT_CSS,
                   com.day.cq.widget.HtmlLibraryManager,
                   org.apache.commons.lang3.StringEscapeUtils,
                   java.util.List" %>
<%
%><%@include file="/libs/foundation/global.jsp"%><%

    // include all the necessary CSS and JS for running mobile emulators
    final HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
    if (htmlMgr != null) {
        htmlMgr.writeIncludes(slingRequest, out, "cq.wcm.mobile.emulator");
    }

    // retrieve the device groups assigned to this page or any above in the hierarchy
    final DeviceGroupList deviceGroups = currentPage.adaptTo(DeviceGroupList.class);
    if (null != deviceGroups) {

%><script type="text/javascript">
    CQ.Ext.onReady(function() {
        var config = {
            defaultEmulator: "<%=deviceGroups.get(0).getEmulators().get(0).getName()%>",
            contentCssId: "<%=HTML_ID_CONTENT_CSS%>",
            showCarousel: true,
            emulatorConfigs: {
            <%
            String delim = "";

            for (final DeviceGroup group : deviceGroups) {
                final List<Emulator> emulators = group.getEmulators();
                for (final Emulator emulator : emulators) {
                %><%=delim%>
                    <%=StringEscapeUtils.escapeEcmaScript(emulator.getName())%>: {
                        plugins: {
                        <%
                        String pluginDelim = "";
                        if (emulator.canRotate()) {
                        %>                            "rotation": {
                                ptype: CQ.wcm.emulator.plugins.RotationPlugin.NAME,
                                config: {
                                   defaultDeviceOrientation: "vertical"
                                }
                            }
                        <%
                            pluginDelim = ",";
                        }

                        if (emulator.hasTouchScrolling()) {
                        %>                            <%=pluginDelim%>"touchscrolling": {
                                ptype: CQ.wcm.emulator.plugins.TouchScrollingPlugin.NAME,
                                config: {
                                }
                            }
                        <%
                        }
                        %>
                        },
                        group: "<%=StringEscapeUtils.escapeEcmaScript(group.getName())%>",
                        title: "<%=StringEscapeUtils.escapeEcmaScript(emulator.getTitle())%>",
                        description: "<%=StringEscapeUtils.escapeEcmaScript(emulator.getDescription())%>",
                        contentCssPath: <%=(null != emulator.getContentCssPath()) ? "\""+request.getContextPath()+emulator.getContentCssPath()+"\"": "null"%>
                    }
                <%
                    delim = ",";
                }
            }
            %>
            }
        };

        var emulatorMgr = CQ.WCM.getEmulatorManager();
        emulatorMgr.launch(config);
    });
</script>
<%
    } else {
        log.warn("mobile page [{}]: no device groups, cannot initialize emulators.", currentPage.getPath());
    }
%>