<%--
  Copyright 1997-2011 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.


@deprecated. Use json load.json instead. XSS non property supported.

--%><%@ page import="com.day.cq.collab.commons.CollabUtil,
                    com.day.cq.security.User,
                    com.day.cq.security.profile.Profile,
                    com.day.cq.security.profile.ProfileManager,
                    com.day.cq.xss.ProtectionContext,
                    com.day.cq.xss.XSSProtectionService,
                    org.apache.sling.commons.json.io.JSONWriter,
                    java.util.Date,
                    com.day.cq.commons.Externalizer" %>
<%!
%><%@include file="/libs/foundation/global.jsp" %><%

    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");

    String authorizableId = request.getParameter("authorizableId");
    Profile profile = null;
    ProfileManager pMgr = sling.getService(ProfileManager.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);

    JSONWriter w = new JSONWriter(response.getWriter());

    //anonymous - special case
    if( !"anonymous".equals(authorizableId)) {
        if (authorizableId != null) {
            try {
                profile = pMgr.getProfile(authorizableId, resourceResolver.adaptTo(Session.class));
            } catch (RepositoryException e) {
                slingResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED, "");
            }
        } else {
                    profile = resourceResolver.adaptTo(User.class).getProfile();
        }
        if (profile != null) {
            w.object();
            String avatar = CollabUtil.getAvatar(profile);
            //increate avatar size
            avatar = avatar == null ? "" : avatar.replaceAll("\\.32\\.",".80.");
            w.key("avatar").value(avatar);
            w.key("authorizableId").value(profile.getAuthorizable().getID());
            w.key("authorizableId_xss").value(
                    xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getAuthorizable().getID()));
            w.key("path").value(profile.getPath());
            w.key("formattedName").value(profile.getFormattedName());
            w.key("formattedName_xss").value(
                    xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,profile.getFormattedName()));
            for (String key : profile.keySet()) {
                if (!key.startsWith("jcr:") && !key.startsWith("sling:") && !key.startsWith("cq:last")) {
                    Object o = profile.get(key);
                    if (o != null && o instanceof String) {
                        String v = o.toString();
                        w.key(key).value(v);
                        w.key(key + "_xss").value(
                                xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,v));
                    }
                }
            }

            Date created = profile.get("memberSince", Date.class);
            if( created == null) {
                created = profile.get("jcr:created", Date.class);
            }
            if( created != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy h:mm a", slingRequest.getLocale());
                w.key("memberSince")
                    .value(df.format(created));
            }

            Date birthday = profile.get("birthday", Date.class);
            if( birthday != null ) {
                java.text.DateFormat df = com.day.cq.commons.date.DateUtil.getDateFormat("d MMM yyyy", slingRequest.getLocale());
                w.key("birthday")
                    .value(df.format(birthday));
            }

            w.endObject();
        }
    } else {
        Externalizer externalizer = sling.getService(Externalizer.class);

        String absoluteDefaultAvatar = "";
        if(externalizer != null){
            absoluteDefaultAvatar = externalizer.relativeLink(slingRequest, CollabUtil.DEFAULT_AVATAR);
        }

        w.object();
        w.key("authorizableId").value("anonymous");
        w.key("formattedName").value("Anonymous Surfer");
        w.key("path").value("/home/users/a/anonymous");
        w.key("avatar").value(absoluteDefaultAvatar);
        w.endObject();
    }
%>