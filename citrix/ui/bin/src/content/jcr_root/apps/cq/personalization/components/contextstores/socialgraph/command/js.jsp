<%--
  ~ Copyright 1997-2011 Day Management AG
  ~ Barfuesserplatz 6, 4001 Basel, Switzerland
  ~ All Rights Reserved.
  ~
  ~ This software is the confidential and proprietary information of
  ~ Day Management AG, ("Confidential Information"). You shall not
  ~ disclose such Confidential Information and shall use it only in
  ~ accordance with the terms of the license agreement you entered into
  ~ with Day.
  --%><%@ page import="com.day.cq.security.profile.Profile,
                    com.day.cq.wcm.foundation.forms.FormsHelper,
                    org.apache.sling.commons.json.io.JSONWriter,
                    java.io.StringWriter,
                    com.day.cq.security.profile.ProfileManager,
                    java.util.Arrays, java.util.List,
                    com.day.cq.xss.ProtectionContext,
                    com.day.cq.xss.XSSProtectionService,
                    com.day.cq.collab.commons.CollabUtil" %><%!
%><%@include file="/libs/foundation/global.jsp"%><%

    ProfileManager pMgr = sling.getService(ProfileManager.class);
    XSSProtectionService xss = sling.getService(XSSProtectionService.class);
    Resource userResource = null;

    List<Resource> resources = FormsHelper.getFormEditResources(slingRequest);
    if (resources != null && resources.size() > 0) {
       //1 - we are in formchooser-mode, get the requested resource
        userResource = resources.get(0);
    }

    Profile profile = userResource!= null ? userResource.adaptTo(Profile.class) : null;

    response.setContentType("text/javascript");
    response.setCharacterEncoding("utf-8");

    out.print(request.getParameter("callback"));
    out.print("(");

    if( profile != null) {
        Long limit = Long.parseLong(request.getParameter("limit") != null ? request.getParameter("limit") : "-1");

        StringWriter sw = new StringWriter();
        final JSONWriter w = new JSONWriter(sw);
        w.setTidy(Arrays.asList(slingRequest.getRequestPathInfo().getSelectors()).contains("tidy"));
        w.object();

        String[] friends = profile.get("friends", String[].class);
        if( friends != null ) {
            w.key("friends");
            w.array();
            int count = 0;
            for(String f: friends) {
                try {
                    Profile pf = pMgr.getProfile(f, resourceResolver.adaptTo(Session.class));
                    if( pf != null ) {
                        w.object();
                        w.key("authorizableId").value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,f));
                        w.key("avatar").value(CollabUtil.getAvatar(pf));
                        w.key("formattedName").value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,pf.getFormattedName()));
                        w.endObject();
                        count ++;
                        if( limit > -1 && count >= limit) break;
                    }
                } catch (RepositoryException e) {
                    //unknown user
                }
            }
            w.endArray();
        }

        String[] followers = profile.get("followers", String[].class);
        if( followers != null ) {
            w.key("followers");
            w.array();
            int count = 0;
            for(String f: followers) {
                try {
                    Profile pf = pMgr.getProfile(f, resourceResolver.adaptTo(Session.class));
                    if( pf != null ) {
                        w.object();
                        w.key("authorizableId").value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,f));
                        w.key("avatar").value(CollabUtil.getAvatar(pf));
                        w.key("formattedName").value(xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT,pf.getFormattedName()));
                        w.endObject();
                        count ++;
                        if( limit > -1 && count >= limit) break;
                    }
                } catch (RepositoryException e) {
                    //unknown user
                }
            }
            w.endArray();
        }

        w.endObject();
        sw.close();
        out.print(sw.toString());

    } else {
        out.print("{}");
    }
    out.print(")");
%>
