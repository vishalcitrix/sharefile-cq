<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

--%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page import="
       org.apache.sling.api.resource.Resource,
       org.apache.sling.api.resource.ResourceResolver,
       org.apache.sling.api.resource.ResourceUtil,
       org.apache.sling.api.resource.ValueMap,
       java.util.Map,
       java.util.HashMap,
       java.util.Iterator,
       org.apache.commons.lang3.StringEscapeUtils" %><%
    final ResourceResolver resolver = resource.getResourceResolver();
    final String paths[] = resolver.getSearchPath();
    final String libPath = "cq/personalization/clientlib/source/strategies/list";
    final Map<String, String> entries = new HashMap<String, String>();
    for(final String path : paths) {
        final Resource dir = resolver.getResource(path + libPath);
        if ( dir != null ) {
            final Iterator<Resource> i = ResourceUtil.listChildren(dir);
            while ( i.hasNext() ) {
                final Resource script = i.next();
                final ValueMap vm = ResourceUtil.getValueMap(script);
                final String key = script.getPath().substring(path.length());
                final boolean enabled = vm.get("enabled", true);
                if ( entries.containsKey(key) ) {
                    if ( !enabled ) {
                        entries.remove(key);
                    }
                } else {
                    if ( enabled ) {
                        entries.put(key, vm.get("jcr:title", ResourceUtil.getName(script)));
                    }
                }
            }
        }
    }
    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");
    boolean first = true;
%>[<%
    for(final Map.Entry<String, String> entry : entries.entrySet()) {
        if ( !first ) {%>,<%} first = false;
        %>{"value": "<%=StringEscapeUtils.escapeEcmaScript(entry.getKey()) %>","text":"<%=StringEscapeUtils.escapeEcmaScript(entry.getValue()) %>"}<%
    }
%>]
