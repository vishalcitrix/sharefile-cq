<%@include file="/libs/foundation/global.jsp"%>

<%-- HEADER PLACE HOLDER --%>
<div class="utilityNavigation">
	<div class="topNav topNavTransition navBar">
		<header>
			<div class="logo">
        		<a href="#"><span></span></a>
			</div>
	    	<div class="link-group ">
	        	<div class="left">
	                <div class="show-for-medium-up">
						<div class="linkList mainLinkSet" style="display: block;">
        					<ul>
                        		<li>	
                       				<a id="phone" rel="" href="">
               							<span class="phone"></span>
               							1 855 884 2266
		               				</a>
								</li>
                	            <li>	
                    		   		<a id="chat" rel="popup" href="#">	
               							<span class="chat"></span>
               							Chat
               						</a>
								</li>
                                <li>	
                    		   		<a id="signIn" rel="" href="#">	
               							Sign In
               						</a>
								</li>
        					</ul>
						</div>
	                </div>
	        	</div>
	        	<div class="clearBoth"></div>
	    	</div>
	    	<div class="clearBoth"></div>
		</header>
	</div>
</div>