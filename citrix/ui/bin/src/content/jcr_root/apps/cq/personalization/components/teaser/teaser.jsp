<%--
  ************************************************************************
  ADOBE CONFIDENTIAL
  ___________________

  Copyright 2011 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
  ************************************************************************

--%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.sling.api.resource.ResourceUtil"%>
<%@page import="org.apache.sling.commons.json.JSONException"%>
<%@page import="org.apache.sling.commons.json.io.JSONWriter"%>
<%@page import="com.day.cq.commons.JSONItem"%>
<%@page import="com.day.cq.tagging.Tag"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.day.cq.wcm.core.stats.PageViewStatistics"%>
<%@page import="com.day.text.Text"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.day.cq.i18n.I18n"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="com.day.cq.personalization.TeaserUtils"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>

<%@include file="/libs/foundation/global.jsp"%>

<%-- <cq:includeClientLib categories="citrixosd.foundation.personalization"/> --%>

<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/foundation/personalization.js"></script>

<%! 
	private static final String HEIGHT_PROPERTY = "height"; 
%>

<c:set var="height" value="<%= properties.get(HEIGHT_PROPERTY, 0) %>"/>

<%
    ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
    final ResourceBundle resourceBundle = slingRequest.getResourceBundle(null);
    final I18n i18n = new I18n(resourceBundle);  
    
    final String basePath = properties.get("campaignpath", "/content/campaigns");
    final Page campaignPage = pageManager.getPage(basePath);
    boolean validBasePath = CAMPAIGN_FILTER.includes(campaignPage);
    if (validBasePath) {
        final String strategyPath = properties.get("strategyPath", "");
        String strategy = "";
        if ( strategyPath.length() > 0 ) {
            strategy = Text.getName(strategyPath);
            strategy = strategy.replaceAll(".js","");
        }
        
	    final String targetDivId = ClientContextUtil.getId(resource.getPath());
	
	    final PageViewStatistics pwSvc = sling.getService(PageViewStatistics.class);
	    String trackingURLStr = null;
	    if (pwSvc!=null && pwSvc.getTrackingURI() != null) {
	        trackingURLStr = pwSvc.getTrackingURI().toString();
	    }
	
	    final String TEASER_SUFFIX = "/_jcr_content/par.html";
	    Teaser defaultTeaser = null;
%>

<%
	StringBuffer allTeasers = new  StringBuffer();
	Iterator<Page> teasers = campaignPage.listChildren(TEASER_FILTER);
	boolean first = true;
	while (teasers.hasNext()) {
	    Page teaser = teasers.next();
	    if(teaser.isValid()) {
	        ValueMap teaserProperties = teaser.getProperties();
	        String imagePath = TeaserUtils.getImage(teaser);
	
	           String rewrittenPath = teaser.getPath();
	           String transformed = null;
	           
	           transformed = transformer.transform(rewrittenPath);
	           if (transformed != null)
	               rewrittenPath = transformed;
	
	        Teaser t = new Teaser(
	                teaser.getPath(),
	                   rewrittenPath,
	                teaser.getName(),
	                teaser.getTitle(),
	                teaserProperties.get("cq:segments",String[].class),
	                teaser.getTags(),
	                campaignPage.getName(),
	                imagePath);
	        StringWriter sw = new StringWriter();
	        JSONWriter json = new JSONWriter(sw);
	        t.write(json);
	        if ( ! first ) {
	            allTeasers.append(",");
	        }
	        allTeasers.append(sw.toString());
	        first = false;
	
	        //last teaser with no segment and no tag is the default teaser.
	        if( t.segments.length == 0 && t.tags.length == 0) {
	            defaultTeaser = t;
	        }
	    }
	}
%>
		<script type="text/javascript">    
	    	jQuery(function() {
				initializeTeaserLoader([<%=allTeasers%>], "<%=strategy%>", "<%=targetDivId%>", "<%=(WCMMode.fromRequest(request) == WCMMode.EDIT)%>", "<%=trackingURLStr%>", "<%=resource.getPath()%>");
	        });
    	</script>
    	
    	<div id="<%=targetDivId%>" class="campaign campaign-<%=campaignPage.getName()%>" style="<c:if test="${not empty height && height > 0}">min-height: ${height}px;</c:if>">
<%
			if( defaultTeaser != null) {
                //include a default teaser into a noscript tag in case of no JS (SEO...)
                StringWriter defaultHtml = new StringWriter();
                pageContext.pushBody(defaultHtml);
%>				
				<sling:include replaceSelectors="noscript" path="<%= defaultTeaser.path+TEASER_SUFFIX %>"/>
<%
                pageContext.popBody();
%>				<noscript><%=defaultHtml%></noscript>

<%
            }
%>
		</div>
<%
	    } else if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
%>			<style type="text/css">
	            .cq-teaser-placeholder-off {
	                display: none;
	            }
	        </style>
        <h3 class="cq-texthint-placeholder"><%=i18n.get("Campaign path does not reference a campaign") %></h3>
        <img src="/libs/cq/ui/resources/0.gif" class="cq-teaser-placeholder" alt="">
<%
    }
%>



<%!
    static com.day.cq.commons.Filter<Page> TEASER_FILTER = new com.day.cq.commons.Filter<Page>() {
        public boolean includes(Page page) {
            if (page == null) {
                return false;
            } else {
                Resource r = page.getContentResource();
                return true;
            }
        }
    };

    static com.day.cq.commons.Filter<Page> CAMPAIGN_FILTER = new com.day.cq.commons.Filter<Page>() {
        public boolean includes(Page page) {
            if (page == null) {
                return false;
            } else {
                Resource r = page.getContentResource();
                return r != null && ResourceUtil.isA(r, "cq/personalization/components/campaignpage");
            }
        }
    };

    class Teaser implements JSONItem {
        public String path;
        public String rewrittenPath;
        public String name;
        public String title;
        public String[] segments;
        public Tag[] tags;
        public String campainName;
        public String thumbnail;

        Teaser(String path, String rewrittenPath, String name, String title, String[] segments, Tag[] tags, String campainName, String thumbnail) {
            this.name = name;
            this.path = path;
            this.rewrittenPath = rewrittenPath;
            this.title = title;
            this.segments = segments;
            this.tags = tags;
            this.campainName = campainName;
            this.thumbnail = thumbnail;

            if(this.segments == null) {
                this.segments = new String[]{};
            }

            if(this.tags == null) {
                this.tags = new Tag[]{};
            }

        }

        public String getId() {
            return campainName + "_" + name;
        }


        public void write(JSONWriter out) throws JSONException {
            out.object();
            out.key("path").value(rewrittenPath);
            out.key("name").value(name);
            out.key("title").value(title);
            out.key("campainName").value(campainName);
            out.key("thumbnail").value(thumbnail);
            out.key("id").value(getId());
            out.key("segments");
            out.array();
            for(String s: segments) {
                out.value(s);
            }
            out.endArray();
            out.key("tags");
            out.array();
            for(Tag t: tags) {
                out.object();
                out.key("name").value(t.getName());
                out.key("title").value(t.getTitle());
                out.key("titlePath").value(t.getTitlePath());
                out.key("path").value(t.getPath());
                out.key("tagID").value(t.getTagID());
                out.endObject();
            }
            out.endArray();
            out.endObject();
        }
    }
%>