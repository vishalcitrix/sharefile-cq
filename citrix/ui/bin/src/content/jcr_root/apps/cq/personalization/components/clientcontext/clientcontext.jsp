<%--
ADOBE CONFIDENTIAL
___________________

Copyright 2011 Adobe Systems Incorporated
All Rights Reserved.

NOTICE:  All information contained herein is, and remains
the property of Adobe Systems Incorporated and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Adobe Systems Incorporated and its
suppliers and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Adobe Systems Incorporated.
--%><%@ page import="com.day.cq.wcm.api.WCMMode,com.siteworx.rewrite.transformer.ContextRootTransformer"%><%!
%><%@include file="/libs/foundation/global.jsp"%>
<%
    final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);

    String segmentsPath = "";
    String ccPath = currentStyle.get("path","/etc/clientcontext/default");

    if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
        String designPath = currentDesign.getPath();
        String transformed = null;
        transformed = transformer.transform(designPath);
        if (transformed != null) {
            designPath = transformed;
        }
        segmentsPath = designPath + "/segmentation";

        transformed = transformer.transform(ccPath);
        if (transformed != null) {
            ccPath = transformed;
        }
        if (ccPath.startsWith("/etc")) {
            ccPath = ccPath.replace("/etc", designPath);
        }

        %>
        <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/foundation/personalization.js"></script>
        <%
    } else {
        segmentsPath = "/etc/segmentation";
        //include personalization editing widgets
        %><cq:includeClientLib categories="cq.personalization"/><%
    }

    String currentPath = currentPage != null ? currentPage.getPath() : "";
    %>

<script type="text/javascript">
    jQuery(function() {
        if (CQ_Analytics != undefined && CQ_Analytics.SegmentMgr != undefined) {

        	//Replacement for CORS in IE
        	$(document).ready(function() {
        		var segmentationScript = '<script type="text/javascript" src="<%= segmentsPath %>.segment.js"></scr' + 'ipt>';

        		CQ_Analytics.ClientContextMgr.PATH = "<%= ccPath %>";
                CQ_Analytics.ClientContextMgr.loadConfig(null, true);
        		var clientcontextUtilsInitPath = CQ.shared.HTTP.externalize("<%= ccPath %>" + "/content/jcr:content/stores.init.js");
                	clientcontextUtilsInitPath = CQ.shared.HTTP.addParameter(clientcontextUtilsInitPath, "path", "<%= currentPath %>");
                	clientcontextUtilsInitPath = CQ.shared.HTTP.noCaching(clientcontextUtilsInitPath);
        		var clientcontextUtilsInitScript = '<script type="text/javascript" src="' + clientcontextUtilsInitPath + '"></scr' + 'ipt>';

            	$("head").append(segmentationScript);
            	$("head").append(clientcontextUtilsInitScript);

            	CQ_Analytics.SegmentMgr.areSegmentsLoaded = true;
            	CQ_Analytics.SegmentMgr.fireEvent("segmentsload");
        	});
            //CQ_Analytics.SegmentMgr.loadSegments("<%=segmentsPath%>"); removed because of CORS
            //CQ_Analytics.ClientContextUtils.init("<%=ccPath%>","<%=currentPath%>"); removed because of CORS
            CQ_Analytics.ClientContextUtils.initUI("<%=ccPath%>","<%=currentPath%>");
        }
    });
</script>