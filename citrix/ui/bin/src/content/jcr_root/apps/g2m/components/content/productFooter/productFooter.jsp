<%--

  Footer component.

  

--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public final String HIDE_BUTTON = "hideButton";
    public final String HIDE_LEFT_LOGO = "hideLeftLogo";
    public final String HIDE_RIGHT_LOGO = "hideRightLogo";
    public final String HIDE_RIGHT_BUTTON = "hideRightButton";
%>

<c:set var="hideButton" value="<%= properties.get(HIDE_BUTTON, "") %>"/>
<c:set var="hideLeftLogo" value="<%= properties.get(HIDE_LEFT_LOGO, "") %>"/>
<c:set var="hideRightLogo" value="<%= properties.get(HIDE_RIGHT_LOGO, "") %>"/>
<c:set var="hideRightButton" value="<%= properties.get(HIDE_RIGHT_BUTTON, "") %>"/>
    
    <c:if test="${hideLeftLogo == ''}">
        <div class="left spriteList">
            <cq:include path="leftLogo" resourceType="g2m/components/content/logo"/>
        </div>
    </c:if>
        <div class="middle" style="<c:if test="${not empty properties.middleWidth}">width: ${properties.middleWidth}${fn:contains(properties.middleWidth, '%') ? '' : 'px'};</c:if>">
    <%
    	WCMMode mode = WCMMode.fromRequest(request); 
    	if(mode.equals(WCMMode.EDIT)) {mode = WCMMode.READ_ONLY.toRequest(request);} 
    %>
            <cq:include path="footerLink_1" resourceType="citrixosd/components/content/footerLinkSet"/>
            <cq:include path="footerLink_2" resourceType="citrixosd/components/content/footerLinkSet"/>
            <cq:include path="copyright" resourceType="g2m/components/content/text"/>
    
        </div>
        <div class="right">
        	<c:if test="${hideRightButton == ''}">
           		<cq:include path="footerButton" resourceType="citrixosd/components/content/footerButton"/>
            </c:if>
        <% mode.toRequest(request); %>
            <div class="rightBoxClear"></div>
       <c:if test="${hideRightLogo == ''}">
            <cq:include path="rightLogo" resourceType="g2m/components/content/logo"/>
        </c:if>
        </div>
     <div class="container" id="footer-dnt"></div>   
