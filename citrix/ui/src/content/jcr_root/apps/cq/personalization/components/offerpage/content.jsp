<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.
  
  ==================================================================
  
  This script allows for HTML snippet rendering of the components in
  the parsys by calling /path/to/teaser/page.content.hml
  
  ==================================================================

--%><%@include file="/libs/foundation/global.jsp"%>
<cq:include path="par" resourceType="foundation/components/parsys" />
