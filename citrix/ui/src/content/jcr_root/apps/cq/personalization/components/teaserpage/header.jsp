<%@include file="/libs/foundation/global.jsp"%>

<%-- HEADER PLACE HOLDER --%>
<c:if test="${not empty isDesktop}">
<div class="utilityNavigation">
    <div class="container">
        <div style="float: left;">
            <div class="logo">
                <span class="gotomypcText"></span>
            </div>
        </div>

        <div style="float: right; margin: 5px 0 0 0;">
            <div class="links">
                <ul>
                    <li>
                        <div class="countrySelector">
                            <a id="localeSelector" class="country us" href="">USA</a>
                        </div>
                    </li>
                    <li><span>|</span></li>
                    <li style="">
                        <div class="linkSet">
                            <ul>
                                <li class="first" style="display: inline;"><a href="" class="" rel="">24/7 Support <span></span></a></li>
                                <li class="last" style="display: inline;"><a href="" class="login" rel="">Log In <span></span></a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearBoth"></div>
    </div>
</div>

<div class="mainNavigation">
    <div class="nav">
        <ul class="container">
            <li><a href="" class="current" id="home"><span>Home</span></a></li>
            <li><a href="">How It Works</a></li>
            <li><a href="" >Features</a></li>
            <li><a href="">Mobile Apps</a></li>
            <li><a href="">Compare Plans</a></li>
            <li><a href="" class="last">Team Accounts</a></li>
            <li>
                <ul>
                    <li><a href="">Contact Sales</a></li>
                    <li><span>|</span></li>
                    <li><span class="salesPhone">1 888 646 0016</span></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div class="breadCrumbs">
    <div class="container">
        <div style="float:left">
            <ul>             
                <li><a href="">Home</a></li>
                <li><span>&nbsp;&gt;&nbsp;</span><a href="">Mobile Apps</a></li>
           </ul>
       </div>
       <div style="float:right; overflow:hidden;">
            <div class="shareList">
                <span class="title">Share</span>
                    <ul class="sharelist">
                        <li><span class="share email"></span></li>
                    </ul>
            </div>
        </div>
    </div>
</div>
</c:if>