OSD.form = {};
OSD.form.LinkBehavior = CQ.Ext.extend(CQ.form.Selection, {
    setOptions: function(options) {
            console.log("setOptions in LinkBehavior");
            // select and combo
            // build store from options
            var storeConfig = {
                fields: ["value", "text", CQ.shared.XSS.getXSSPropertyName("text"), "qtip"],
                data: []
            };
            
            var newOptions = new Array();
            var none = {
                text: "None",
                value:""
            };
            var external = {
                text: "External",
                value:"external"
            };
            var lightbox = {
                text: "Lightbox",
                value:"lightbox"
            };
            var popup = {
                text: "Popup",
                value:"popup"
            };
            var custom = {
                text: "Custom",
                value:"custom"
            };
            
            newOptions.push(none);
            newOptions.push(external);
            newOptions.push(lightbox);
            newOptions.push(popup);
            newOptions.push(custom);
            options = newOptions;
 
            
            
            for (var i = 0; i < options.length; i++) {
                var o = options[i];
                if (o.text == '-') {
                    o.value = "menu-separator-item-value";  // Could be anything that's unlikely to match actual input
                } else {
                    o.value = o.value != undefined ? o.value : "";
                }
                o.text = o.text ? CQ.I18n.getVarMessage(o.text) : o.value;
                o[CQ.shared.XSS.getXSSPropertyName("text")] = CQ.shared.XSS.getXSSTablePropertyValue(o, "text") ? CQ.shared.XSS.getXSSTablePropertyValue(o, "text") : o.value;
                o.qtip = o.qtip ? o.qtip : "";
            }

            if (this.sortDir) this.sortOptions(options);

            for (var i = 0; i < options.length; i++) {
                var o = options[i];
                storeConfig.data.push([o.value, o.text, CQ.I18n.getVarMessage(o[CQ.shared.XSS.getXSSPropertyName("text")]), o.qtip]);
            }

            if (!this.comboBox) {
                this.optionsConfig.store = new CQ.Ext.data.SimpleStore(storeConfig);
                this.optionsConfig.listClass = CQ.Util.createId(CQ.DOM.encodeClass(this.name));
                this.comboBox = new CQ.Ext.form.ComboBox(this.optionsConfig);
                this.add(this.comboBox);
            } else {
                this.optionsConfig.store.loadData(storeConfig.data);
            }
        if (this.rendered) {
            this.doLayout();
        }
    }
});

CQ.Ext.reg("linkbehavior", OSD.form.LinkBehavior);

OSD.form.LinkBehaviorTooltip = CQ.Ext.extend(CQ.form.Selection, {
    setOptions: function(options) {
            console.log("setOptions in LinkBehavior");
            // select and combo
            // build store from options
            var storeConfig = {
                fields: ["value", "text", CQ.shared.XSS.getXSSPropertyName("text"), "qtip"],
                data: []
            };
            
            var newOptions = new Array();
            var none = {
                text: "None",
                value:""
            };
            var external = {
                text: "External",
                value:"external"
            };
            var lightbox = {
                text: "Lightbox",
                value:"lightbox"
            };
            var popup = {
                text: "Popup",
                value:"popup"
            };
            var tooltip = {
                text: "Tooltip",
                value: "tooltip"
            };
            
            var inlineVideo = {
                text: "Inline Video",
                value:"inline-video"
            };
            
            newOptions.push(none);
            newOptions.push(external);
            newOptions.push(lightbox);
            newOptions.push(popup);
            newOptions.push(tooltip);
            newOptions.push(inlineVideo);
            options = newOptions;
 
            
            
            for (var i = 0; i < options.length; i++) {
                var o = options[i];
                if (o.text == '-') {
                    o.value = "menu-separator-item-value";  // Could be anything that's unlikely to match actual input
                } else {
                    o.value = o.value != undefined ? o.value : "";
                }
                o.text = o.text ? CQ.I18n.getVarMessage(o.text) : o.value;
                o[CQ.shared.XSS.getXSSPropertyName("text")] = CQ.shared.XSS.getXSSTablePropertyValue(o, "text") ? CQ.shared.XSS.getXSSTablePropertyValue(o, "text") : o.value;
                o.qtip = o.qtip ? o.qtip : "";
            }

            if (this.sortDir) this.sortOptions(options);

            for (var i = 0; i < options.length; i++) {
                var o = options[i];
                storeConfig.data.push([o.value, o.text, CQ.I18n.getVarMessage(o[CQ.shared.XSS.getXSSPropertyName("text")]), o.qtip]);
            }

            if (!this.comboBox) {
                this.optionsConfig.store = new CQ.Ext.data.SimpleStore(storeConfig);
                this.optionsConfig.listClass = CQ.Util.createId(CQ.DOM.encodeClass(this.name));
                this.comboBox = new CQ.Ext.form.ComboBox(this.optionsConfig);
                this.add(this.comboBox);
            } else {
                this.optionsConfig.store.loadData(storeConfig.data);
            }
        if (this.rendered) {
            this.doLayout();
        }
    }
});

CQ.Ext.reg("linkbehaviortooltip", OSD.form.LinkBehaviorTooltip);