<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>


<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<cq:include script="/apps/citrixosd/wcm/core/components/init/init.jsp"/>

<%!
	public static final String SYNCHRONOUS_ANALYTICS_PROPERTY = "synchronousAnalytics";
%>
<%
	response.setHeader("Dispatcher", "no-cache");
	final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
	String designPath = currentDesign.getPath();
	String designPathOrig = currentDesign.getPath();
	String transformed = transformer.transform(designPath);
	if (transformed != null) {
		designPath = transformed;
	}

    final String cqpersonalizationjsOption = SiteUtils.getPropertyFromSiteRoot("cqpersonalizationjs",currentPage);
    final String jquerytoolsOption = SiteUtils.getPropertyFromSiteRoot("jquerytools",currentPage);
    final String jqstationarytipOption = SiteUtils.getPropertyFromSiteRoot("jqstationarytip",currentPage);
    final String jqdotdotdotOption = SiteUtils.getPropertyFromSiteRoot("jqdotdotdot",currentPage);
    final String stickyChatButtonId = SiteUtils.getPropertyFromSiteRoot("stickychatbuttonid",currentPage);
%>

<c:set var="siteName" value="<%= SiteUtils.getSiteName(currentPage) %>"/>
<c:set var="favIcon" value="<%= SiteUtils.getFavIcon(currentPage) %>"/>
<c:set var="pageName" value="<%= currentPage.getPageTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getPageTitle()) : currentPage.getTitle() != null ? StringEscapeUtils.escapeHtml4(currentPage.getTitle()) : StringEscapeUtils.escapeHtml4(currentPage.getName()) %>"/>
<c:set var="pageDescription" value="<%= currentPage.getDescription() %>" />
<c:set var="designPath" value="<%= designPath %>" />
<c:set var="designPathOrig" value="<%= designPathOrig %>" />
<c:set var="showMobileRedirect" value="<%= properties.get("showMobileRedirect", false) %>"/>
<c:set var="cqpersonalizationjsOption" value="<%= cqpersonalizationjsOption %>" scope="request" />
<c:set var="jquerytoolsOption" value="<%= jquerytoolsOption %>" scope="request" />
<c:set var="jqstationarytipOption" value="<%= jqstationarytipOption %>" scope="request" />
<c:set var="jqdotdotdotOption" value="<%= jqdotdotdotOption %>" scope="request" />
<c:set var="stickyChatButtonId" value="<%= stickyChatButtonId %>" scope="request" />
<c:set var="globalCSS" value="<%= SiteUtils.getPropertyFromSiteRoot("globalcss",currentPage) %>" />
<c:set var="globalJS" value="<%= SiteUtils.getPropertyFromSiteRoot("globaljs",currentPage) %>" />
<head>
    <%--Meta tags --%>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <cq:include script="analytics_tag.jsp"/>
    <cq:include script="abtest.jsp"/>

    <cq:include script="meta.jsp"/>
    <c:if test="${showMobileRedirect}">
        <cq:include script="mobileRedirect.jsp"/>
    </c:if>

    <%--CSS --%>
    <link rel="stylesheet" href="<%= currentDesign.getPath() %>/css/static.css" type="text/css">
    <!--[if IE 7]>
        <link rel="stylesheet" href="${designPath}/css/IE7.css" type="text/css">
    <![endif]-->
    <!--[if IE 8]>
        <link rel="stylesheet" href="${designPath}/css/IE8.css" type="text/css">
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" href="${designPath}/css/IE9.css" type="text/css">
    <![endif]-->

    <%--Icons --%>
    <link rel="Shortcut Icon" href="<%= currentDesign.getPath() %>/css/static/images/${not empty favIcon ? favIcon : 'favicon'}.ico">
    <link rel="image_src" href="<%= currentDesign.getPath() %>/css/static/images/logo_fb.jpg">

    <%--Title and meta field--%>
    <title>${pageName}${not empty siteName ? ' | ' : ''}${siteName}</title>
    <meta name="description" content="${not empty pageDescription ? pageDescription : ''}">
    <meta name="keywords" content="${not empty siteName ? siteName : ''}">

    <c:if test="<%= properties.get(SYNCHRONOUS_ANALYTICS_PROPERTY, false) %>">
        <cq:include script="analytics.jsp"/>
    </c:if>

    <%--Javascripts - Place after including jQuery Lib --%>
    <script type="text/javascript" src="<%= currentDesign.getPath() %>/js/common.js"></script>
    <c:if test="${empty jquerytoolsOption}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.tools.min.js"></script></c:if>
    <c:if test="${empty jqstationarytipOption}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.stationaryTip.js"></script></c:if>
    <c:if test="${empty jqdotdotdotOption}"><script type="text/javascript" src="<%= currentDesign.getPath() %>/js/jquery/jquery.dotdotdot-1.5.9.min.js"></script></c:if>

    <%--IE10 stylesheet--%>
    <script>
        jQuery(function() {
            if (jQuery.browser.msie && jQuery.browser.version == 10) {
                jQuery("head").append("<link rel='stylesheet' href='${designPath}/css/IE10.css' type='text/css'/>");
            }
        });
    </script>
    <c:if test="${not empty globalCSS and empty properties.disableGlobalCss}">
        <link rel="stylesheet" href="${globalCSS}" type="text/css">
    </c:if>
    <c:if test="${not empty globalJS and empty properties.disableGlobalJs}">
        <script src="${globalJS}" type="text/javascript"></script>
    </c:if>
    <c:if test="${not empty properties.customPageJs}">
        <script src="${properties.customPageJs}" type="text/javascript"></script>
    </c:if>
    <c:if test="${empty cqpersonalizationjsOption}">
        <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    </c:if>
</head>