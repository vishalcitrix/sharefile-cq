<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="javax.jcr.Node"%>
<%@page import="javax.jcr.NodeIterator"%>
<%@page import="com.citrixosd.SiteUtils"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<c:set var="showChannelTracking" value="<%= SiteUtils.getPropertyFromProductSiteRoot("showChannelTracking", currentPage) %>"/>

<c:if test="${(showChannelTracking eq 'true' && empty pageProperties.hideChannelTracking) || (empty showChannelTracking && empty pageProperties.hideChannelTracking)}">
	<c:set var="trkObjectNamesList" value="<%= SiteUtils.getTrackingObjectNamesAsArray(currentPage) %>"/>
	<%-- START TRACKER OBJECT IF --%>
	<c:if test="${fn:length(trkObjectNamesList) >= 1}">

	<c:set var="trkDomainsList" value="<%= SiteUtils.getTrackingDomains(currentPage,request.getServerName(),"mix:CitrixSiteRoot") %>"/>
	<c:choose>
		<c:when test="${fn:length(trkDomainsList) > 2}">
			<c:set var="trkDomains" value="${fn:substring(trkDomainsList,0,(fn:length(trkDomainsList)-2))}\"\]"/>
		</c:when>
		<c:otherwise>
			<c:set var="trkDomains" value="\"\""/>
		</c:otherwise>
	</c:choose>
	<%--
	When this is run on non-production systems we should be setting the value of dataMarketingEnv
	to "dev". This causes the STAGE version of the Marketing Service api.js file to load.
	http://stage.marketing.citrixonline.com/marketing/api/2009/api.js

	However, as of 09/2013 the STAGE version of api.js has coding issues which cause JavaScript
	errors. So for the interim let's set dataMarketingEnv to "live" for non-production systems.
	--%>
	<c:choose>
		<c:when test="${isProd}"><c:set var="dataMarketingEnv" value="live"/></c:when>
		<c:otherwise><c:set var="dataMarketingEnv" value="live"/></c:otherwise>
	</c:choose>

	<script type="text/javascript" src="//static.citrixonlinecdn.com/web-library-2/marketing/channeltracker.js" data-marketing-environment="${dataMarketingEnv}"></script>
	<script type="text/javascript">
		var trkDomain<c:forEach var="trkObjectName" items="${trkObjectNamesList}" varStatus="i">,tracker<c:out value="${i.count}" /></c:forEach>;
		dotrack = function() {
			window.ChannelTracker.prototype.updateChannel = function (attributes) {
				if(this.dntCheck()) return;
				if (typeof attributes == 'object') {
					var cookieStr = '';
					var va = this.cookieValueToArray();
					jQuery.each(attributes, function(key, value) {
						if (typeof(va['FIS_'+key]) == "undefined") {
							va['FIS_'+key] = value;
						}
						va['LST_'+key] = value;
					});
					jQuery.each(va, function(key, val) {
						if (key.indexOf('FIS_') == 0 || key.indexOf('LST_') == 0) {
							var v = key + '=' + val;
							cookieStr += cookieStr == '' ? v : '&' + v;
						}
					});
					this.writeMktCookie(cookieStr);
					this.appendJumpParamsToForms();
					this.appendJumpParamsToLinks();
				}
			}
			trkDomain = ${trkDomains};<c:forEach var="trkObjectName" items="${trkObjectNamesList}" varStatus="i">
			var pUrl = '' + window.location;
			if (pUrl.match(/@/))
				pUrl = pUrl.replace(/@/g, "%40");
			jQuery.url.setUrl(pUrl)
			var results = jQuery.url.attr("host").match(/([A-Za-z0-9\-]*)((\.[A-Za-z\-]{2,3}){1,2}|(\.[A-Za-z]{2,10}))$/);
			var subdomain = results != null && results.length > 0 ? results[0] : null;
			tracker<c:out value="${i.count}" /> = new ChannelTracker('<c:out value="${trkObjectName}" />',subdomain);
			tracker<c:out value="${i.count}" />.appendTrackingHosts = trkDomain;
			tracker<c:out value="${i.count}" />.trackChannel();
			$( document ).ajaxComplete(function( event, xhr, settings ) {
			    tracker<c:out value="${i.count}" />.appendJumpParamsToForms();
			    tracker<c:out value="${i.count}" />.appendJumpParamsToLinks();
			});
			try {
				if( typeof utag_data.ab_test != "undefined" && typeof utag_data.ab_branch != "undefined") {
					tracker<c:out value="${i.count}" />.updateChannel({"test":utag_data.ab_test+'-'+utag_data.ab_branch});
				}
			} catch(e){}</c:forEach>
		};
		if (typeof dotrack == 'function' && (typeof dnt !== 'undefined' && typeof dnt.dntTrue === 'function') ? !dnt.dntTrue() : true) {
			getChannelTracker(dotrack);
		}
	</script>
	<script type="text/javascript" src="<%= currentDesign.getPath() %>/js/channel/plugins.js"></script>
	<script type="text/javascript" src="//marketing.citrixonline.com/api/2009/ChannelTrackingConverter.js"></script>

	</c:if>
	<%-- END TRACKER OBJECT IF --%>
</c:if>