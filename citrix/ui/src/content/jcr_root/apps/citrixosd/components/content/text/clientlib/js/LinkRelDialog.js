/*
 * Copyright 1997-2009 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */

/**
 * @class CQ.form.rte.plugins.LinkDialog
 * @extends CQ.form.rte.ui.BaseWindow
 * @private
 * The LinkDialog is a dialog for creating a link.
 * @constructor
 * Creates a new LinkDialog.
 * @param {Object} config The config object
 */
LinkRelPlugin.Dialog = CQ.Ext.extend(CQ.form.rte.ui.BaseWindow, {

    constructor: function(config) {
        config = config || { };
        var defaults = {
            "title": CQ.I18n.getMessage("Hyperlink"),
            "modal": true,
            "width": 400,
            "height": 160,
            "dialogItems": [ {
                    "itemId": "href",
                    "name": "href",
                    "parBrowse": false,
                    "anchor": CQ.themes.Dialog.ANCHOR,
                    "fieldLabel": CQ.I18n.getMessage("Link to"),
                    "xtype": "pathfield",
                    "ddGroups": [
                        CQ.wcm.EditBase.DD_GROUP_PAGE,
                        CQ.wcm.EditBase.DD_GROUP_ASSET
                    ],
                    "fieldDescription": CQ.I18n.getMessage("Drop files or pages from the Content Finder"),
                    "listeners": {
                        "dialogselect": {
                            "fn": this.selectAnchor,
                            "scope": this
                        },
                        "render": this.initHrefDragAndDrop
                    },
                    "validator": this.validateLink.createDelegate(this),
                    "validationEvent": "keyup",
                    "escapeAmp": true
                },
                {
                    "itemId": "rel",
                    "fieldLabel": CQ.I18n.getMessage("Behavior"),
                    "name": "rel",
                    "xtype": "linkbehaviortooltip",
                    "type": "select",
                    "anchor": CQ.themes.Dialog.ANCHOR
                }
            ]
        };
        CQ.Util.applyDefaults(config, defaults);
        LinkRelPlugin.Dialog.superclass.constructor.call(this, config);
    },

    /**
     * @private
     */
    selectAnchor: function(pathfield, path, anchor) {
        // custom path + anchor handling
        path = CQ.HTTP.encodePath(path);
        // encodePath will not encode '&', so we're doing it here, as other callees of
        // encodePath might rely on that documented behaviour - see bug #30206
        path = path.replace(/&/g, "%26");
        if (anchor && (anchor.length > 0)) {
            path += ".html#" + anchor;
        }
        pathfield.setValue(path);
    },

    /**
     * <p>Note that this method is executed in the scope of the pathfield.</p>
     * @private
     */
    initHrefDragAndDrop: function() {
        if (this.ddGroups) {
            if (typeof(this.ddGroups) == "string") {
                this.ddGroups = [ this.ddGroups ];
            }
            var field = this;
            var target = new CQ.wcm.EditBase.DropTarget(this.el, {
                "notifyDrop": function(dragObject, evt, data) {
                    if (dragObject && dragObject.clearAnimations) {
                        dragObject.clearAnimations(this);
                    }
                    if (dragObject.isDropAllowed(this)) {
                        if (data.records && data.single) {
                            var record = data.records[0];
                            var path = record.get("path");
                            path = CQ.HTTP.encodePath(path);
                            // again, '&' needs to be encoded explicitly - see bug #30206
                            path = path.replace(/&/g, "%26");
                            field.setValue(path);
                            evt.stopEvent();
                            return true;
                        }
                        return false;
                    }
                }
            });

            var dialog = this.findParentByType(LinkRelPlugin.Dialog);
            dialog.on("activate", function(dialog) {
                if (dialog && dialog.el && this.highlight) {
                    var dialogZIndex = parseInt(dialog.el.getStyle("z-index"), 10);
                    if (!isNaN(dialogZIndex)) {
                        this.highlight.zIndex = dialogZIndex + 1;
                    }
                }
            }, target);
            
            dialog.on("deactivate", function(dialog) {
                if (dialog && dialog.el && this.highlight) {
                    var dialogZIndex = parseInt(dialog.el.getStyle("z-index"), 10);
                    if (!isNaN(dialogZIndex)) {
                        this.highlight.zIndex = dialogZIndex + 1;
                    }
                }
            }, target);
            
            var editorKernel = dialog.getParameter("editorKernel");
            dialog.on("show", function() {
                editorKernel.fireUIEvent("preventdrop");
                CQ.WCM.registerDropTargetComponent(field);
            }, target);
            
            dialog.on("hide", function() {
                CQ.WCM.unregisterDropTargetComponent(field);
                editorKernel.fireUIEvent("reactivatedrop");
            }, target);

            for (var i = 0; i < this.ddGroups.length; i++) {
                target.addToGroup(this.ddGroups[i]);
            }
            target.removeFromGroup(CQ.wcm.EditBase.DD_GROUP_DEFAULT);
            this.dropTargets = [ target ];
        }
    },

    /**
     * Gets a field with the provided key from this panel.
     *
     * @param key Field name
     */
    getField: function(key) {
        var items = this.find("name", "./" + key);
        if( (CQ.Ext.isArray(items)) && (items.length > 0) )
            return items[0];
    },

    preprocessModel: function() {
        if (this.objToEdit && this.objToEdit.dom) {
            this.objToEdit.href = CQ.form.rte.HtmlRules.Links.getLinkHref(
                    this.objToEdit.dom);
            
            var com = CQ.form.rte.Common;
            var attribNames = com.getAttributeNames(this.objToEdit.dom, false,
                function(dom, attribName, attribNameLC) {
                    // exclude href, rte_href & target from generic attribute handling, as
                    // they are handled explicitly and not genrically
                    return attribNameLC == com.HREF_ATTRIB || attribNameLC == "href"
                            || attribNameLC == "target" || attribNameLC == "rel";
                });
            for (var i = 0; i < attribNames.length; i++) {
                var attribName = attribNames[i];
                var value = com.getAttribute(this.objToEdit.dom, attribName);
                if (typeof value !== 'undefined') {
                    this.objToEdit.attributes[attribName] = value;
                }
            }
            if (this.objToEdit.attributes.onclick) {
                showAdvanced = true;
            }
        }
    },

    dlgFromModel: function() {
        var hrefField = this.getFieldByName("href");
        if (hrefField) {
            var value = "";
            if (this.objToEdit) {
                var href = this.objToEdit.href;
                if (href) {
                    value = href;
                }
            }
            hrefField.setValue(value);
        }
        var targetBlankField = this.getFieldByName("targetBlank");
        if (targetBlankField) {
            var target = (this.objToEdit && this.objToEdit.target
                    ? this.objToEdit.target.toLowerCase() : null);
            targetBlankField.setValue(target == "_blank");
            var targetBF = targetBlankField.getValue();
        }
        
        var targetRelField = this.getFieldByName("rel");
        if( targetRelField ){
            var rel = (this.objToEdit && this.objToEdit.dom && this.objToEdit.dom.rel
                    ? this.objToEdit.dom.rel.toLowerCase() : null);
            targetRelField.setValue(rel);
            var relBF = targetRelField.getValue();
        }
    },

    dlgToModel: function() {
        if (this.objToEdit) {
            var hrefField = this.getFieldByName("href");
            if (hrefField) {
                var href = hrefField.getValue();
                if (href) {
                    this.objToEdit.href = href;
                }
            }
            var targetRelField = this.getFieldByName("rel");

            if(targetRelField){
                if(targetRelField.getValue()){
                    this.objToEdit.rel = targetRelField.getValue();
                } else{
                    this.objToEdit.rel = null;
                }
            }
        }
    },

    postprocessModel: function() {
        var linkRules = this.parameters.linkRules;
        if (linkRules) {
            linkRules.applyToObject(this.objToEdit);
        }
    },

    validateLink: function() {
        var href = this.getFieldByName("href");
        if (!href) {
            return false;
        }
        href = href.getValue();
        var linkRules = this.getParameter("linkRules");
        if (!linkRules) {
            return (href.length > 0);
        }
        return linkRules.validateHref(href);
    }

});

// register LinkDialog component as xtype
CQ.Ext.reg("rtelinkreldialog", LinkRelPlugin.Dialog);