<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.net.URLEncoder"%>
<%!
	private static final String JCR_PROPERTY_PROPERTY = "jcrProperty";
	private static final String JCR_PROPERTY_VALUE_PROPERTY = "jcrPropertyValue";
	private static final String UPDATE_JCR_PROPERTY_VALUE_PROPERTY = "updateJcrPropertyValue";
%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<% 
	//Retrieveing the post parameters
	final String redirect= request.getParameter("redirect");
	final String[] updatePaths = request.getParameterValues("paths");
	
	final String jcrProperty = properties.get(JCR_PROPERTY_PROPERTY, null);
	final String updateJcrPropertyValue = properties.get(UPDATE_JCR_PROPERTY_VALUE_PROPERTY, null);
	final List<String> results = new ArrayList<String>();
	
	if(updatePaths != null) {
		for(String updatePath : updatePaths) {
			final Node updateNode = resource.getResourceResolver().getResource(updatePath).adaptTo(Node.class);
			if(jcrProperty != null && updateJcrPropertyValue != null) {
				try {
					updateNode.setProperty(jcrProperty, updateJcrPropertyValue);
					updateNode.save();
					results.add("Success: " + updateNode.getPath() + "");
				} catch(Exception e) {
					results.add("Error: " + updateNode.getPath() + "");
				}
			}	
		}	
	}
	
	final StringBuilder builder = new StringBuilder();
	for (int i = 0; i < results.size(); i++) {
		if(i == 0) {
			builder.append("?");
		}else {
			builder.append("&");	
		}
		builder.append("propertyModiferUpdated=" + URLEncoder.encode(results.get(i),"UTF-8"));
	}
	
	final String redirectStr = redirect + "/" + builder.toString();
	System.out.println(redirectStr);
	response.sendRedirect(redirectStr);
%>
