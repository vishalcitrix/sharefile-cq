<%--
	Locale sniffer
	
	Sniffer will check for the visitor's locale language. This will query through CQ 
	and retrieve all the locale under the website. With the resulting query, this will 
	generate a dynamic script so when the user lands on it, it will redirect them to 
	the correct locale. The default locale is used as a fallback if there is no resulting 
	locale.

	achew@siteworx.com
--%>

<%@page import="com.citrixosd.utils.LinkUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="javax.jcr.nodetype.NodeType"%>
<%@page import="java.util.Iterator"%>

<%! 
	public static final String DEFAULT_PATH_PROPERTY = "defaultPath";
	
	public class LocalePageMeta {
		private String name;
		private String path;
		
		public LocalePageMeta() {
			
		}
		
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPath() {
			return this.path;
		}
		public void setPath(String path) {
			this.path = path;
		}
	}

%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
	final Page rootPage = currentPage.getAbsoluteParent(1);
	final Iterator<Page> localePages = rootPage.listChildren();
	final List<LocalePageMeta> filteredLocalePageMetas = new ArrayList<LocalePageMeta>(); 
	while(localePages.hasNext()) {
		final Page localePage = localePages.next();
		final Node localePageNode = localePage.adaptTo(Node.class);
		if(localePageNode != null && localePageNode.hasNode("jcr:content")) {
			final Node localePageNodeJcr = localePageNode.getNode("jcr:content");
			final NodeType[] mixinTypes = localePageNodeJcr.getMixinNodeTypes();
			for(NodeType mixinType : mixinTypes) {
				if(mixinType.getName().equals("mix:CitrixSiteRoot")) {
					final LocalePageMeta localePageMeta = new LocalePageMeta();
					localePageMeta.setName(localePage.getName());
					localePageMeta.setPath(LinkUtil.getTransformedUrl(localePage.getPath(), sling));
					filteredLocalePageMetas.add(localePageMeta);
				}
			}
		}
	}
	final String defaultPath = LinkUtil.getTransformedUrl(properties.get(DEFAULT_PATH_PROPERTY, String.class), sling);
%>

<c:set var="currentLocalePage" value="<%= currentPage.getAbsoluteParent(2) %>"/>
<c:set var="filteredLocalePageMetas" value="<%= filteredLocalePageMetas %>"/>
<c:set var="defaultPath" value="<%= defaultPath %>"/>

<c:if test="${isEditMode}">
	<div class="warning">Locale Sniffer: Current: ${currentLocalePage.name}</div>
</c:if>

<script>
	var LocaleSniffer = LocaleSniffer || {
		init : function() {
			var locale = navigator.language || navigator.userLanguage;
			var localeLanguage = LocaleSniffer.getLanguage(locale);
			var localeCountry = LocaleSniffer.getCountry(locale);
			LocaleSniffer.redirectLogic(localeLanguage, localeCountry);
		},
		
		getLanguage: function(locale) {
			if(locale != null) {
				var language = locale.substring(0,2);
				return language != null ? language.toLowerCase() : null;
			}else {
				return null;
			}
		},
	
		getCountry: function(locale) {
			if(locale != null) {
				var localeSplitDash = locale.split("-");
				if(localeSplitDash != null && localeSplitDash[1] != null) {
					return localeSplitDash[1].toUpperCase();
				}else{
					var localeSplitUnderscore = locale.split("_");
					if(localeSplitUnderscore != null && localeSplitUnderscore[1] != null) {
						return localeSplitUnderscore[1].toUpperCase();
					}
					return null;
				}
			}else {
				return null;
			}
		},
		
		redirectLogic: function(redirectLanguage, redirectCountry) {
			var redirectLocale = redirectLanguage;
			redirectLocale += redirectCountry != null ? "_" + redirectCountry : "" ;
			switch(redirectLocale) {
			<c:forEach items="${filteredLocalePageMetas}" var="filteredLocalePageMeta">
				case '${filteredLocalePageMeta.name}' :
					if('${filteredLocalePageMeta.name}' === '${currentLocalePage.name}') {
						//Matches
					}else {
						var url = "${filteredLocalePageMeta.path}";
						<c:if test="${isDisabledMode}">
							location.replace(url + location.search);
						</c:if>
					}
				break;
			</c:forEach>
			default : 
				<c:if test="${not empty defaultPath && isDisabledMode}">
					location.replace("${defaultPath}" + location.search);
				</c:if>
				break;
			}
		}
	};
	
	LocaleSniffer.init();
</script>