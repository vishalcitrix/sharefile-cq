<%--
  Text component
  - Creates a page with .txt extension
--%>
<%@include file="/libs/foundation/global.jsp"%>

<c:if test="${not empty properties['redirectTarget']}">
	<cq:include script="redirect.jsp"/>
</c:if>

<html>
	<head>
		<title>Text</title>
		<meta name="description" content="">
		<meta http-equiv="Content-Type" content="text;charset=utf-8">
		<meta name="robots" content="noindex,follow">
	</head>
	<body>
		<c:if test="${not empty properties['redirectTarget']}">
			<cq:include script="redirectMessage.jsp"/>
		</c:if>
		<center>
			<h2> Please use txt extension in URL to generate robots information</h2>
		</center>
	</body>
</html>
