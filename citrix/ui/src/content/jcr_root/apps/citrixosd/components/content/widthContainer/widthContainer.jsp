<%--
    Width Container - Creates div with specified width. Option to add padding as well.
--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div style="${(properties.alignedcentered eq 'true') ? 'margin: 0 auto' : '' }; <c:if test="${not empty properties.padding}">padding: ${properties.padding};</c:if> width: ${properties.width};">
    <cq:include path="width_container_parsys" resourceType="foundation/components/parsys"/>
</div>

