<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.SiteUtils"%>

<%-- Add optional product theme --%>
<c:set var="productTheme" value="<%= SiteUtils.getProductTheme(currentPage) %>"/>
<c:if test="${not empty productTheme}">
    <c:set var="productTheme" value=' class="${productTheme}"'/>
</c:if>
<body${productTheme}>
 	<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
    	<div class="row full-width">
		  <div class="large-8 columns">
		    <cq:include path="leftContent" resourceType="foundation/components/parsys"/>
		  </div>     
		  <div class="large-4 columns">
		     <cq:include path="rightRailiPar" resourceType="foundation/components/iparsys"/>
		  </div>
		</div>
		<div class="row full-width">
		  <div class="large-12 columns">
		    <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
		  </div>
		</div>
		<cq:include path="footerContent" resourceType="swx/component-library/components/content/single-ipar"/> 
	</div>  
    <cq:include script="footer.jsp" />
</body>