<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.SiteUtils"%>

<%-- Add optional product theme --%>
<c:set var="productTheme" value="<%= SiteUtils.getProductTheme(currentPage) %>"/>
<c:if test="${not empty productTheme}">
    <c:set var="productTheme" value=' class="${productTheme}"'/>
</c:if>
<body${productTheme}>
    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>
    <cq:include script="noscript.jsp"/>
    <div id="content-body" class="resourcePage">
    	<cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include path="socialNav" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include path="resourceHeader" resourceType="swx/component-library/components/content/single-ipar"/>
		<div class="row full-width">
		    <div class="large-2 columns">
                <cq:include script="filter.jsp"/>
            </div>
		    <div class="large-10 columns">
                <cq:include script="resourceContent.jsp"/>
		    </div>
		</div>
	</div>  
    <cq:include script="footer.jsp" />
</body>