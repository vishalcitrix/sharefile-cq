<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>
<%@page import="com.citrixosd.SiteUtils"%>
<%--
    Mobile redirect and extract Query String
--%>
<%
    String mobileRedirectURL = properties.get("mobileRedirect", "");
    String queryString = xssAPI.getValidHref(request.getQueryString());
    if(!mobileRedirectURL.equals("") && !queryString.equals("") && !queryString.equals("null")) {
        mobileRedirectURL = mobileRedirectURL + "?" + queryString;
    }
%>
<c:set var="mobileRedirectURL" value="<%= mobileRedirectURL %>"/>
<c:set var="fullsite" value="<%= request.getParameter("fullsite") != null ? xssAPI.getValidHref(request.getParameter("fullsite")) : "false" %>"/>
<script type="text/javascript">
<c:if test="${not empty mobileRedirectURL}">
    var redirect = "<%= mobileRedirectURL %>";
</c:if>
<c:choose>
<c:when test="${not isPublishInstance || empty mobileRedirectURL || fullsite eq 'true'}">
    var sendRedirect = false;
</c:when>
<c:otherwise>
    var sendRedirect = true;
</c:otherwise>
</c:choose>
    var device = false;
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("iphone") > 0 || ua.indexOf("ipod") > 0 || ua.indexOf("googlebot-mobile") > 0) {
        device = true;
    } else if (ua.indexOf("android") > 0) {
        if (ua.indexOf("mobile safari") > 0) {
            device = true;
        } else if (screen.width <= 960 || screen.height <= 960){
            device = true;
        }
    }
    if( ua.indexOf("android") >= 0 && ua.indexOf("mobile safari") < 0 )
        device = false;
    var domain = document.domain.split(".");
    var domainStr;
    if (domain.length == 3)
        domainStr = domain.slice(-2).join(".");
    else domainStr = domain.join(".");
    var referrer = document.referrer;
    if(referrer.indexOf('?') > 0)
        referrer = referrer.substring(0,referrer.indexOf('?'))
    var sameDomain = false;
    var locale = "";
    if( referrer ) {
        if( referrer.indexOf(domainStr) != -1 )
            sameDomain = true;
    }
    if( device && !sameDomain && sendRedirect)
        location.replace( redirect );
</script>