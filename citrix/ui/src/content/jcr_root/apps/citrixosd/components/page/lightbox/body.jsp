<%@page import="javax.jcr.Node"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
    Node node = currentNode.hasNode("lightboxWidthConf") ? currentNode.getNode("lightboxWidthConf") : currentNode.addNode("lightboxWidthConf");
    currentNode.save();
    
    if(!node.hasProperty("sling:resourceType")) {
        node.setProperty("sling:resourceType", "citrixosd/components/page/lightbox/lightboxWidth");
        node.setProperty("lightboxWidth", "");
        node.save();
    }
%>

<body style="overflow: hidden;">
	<section class="lightbox">
    	<cq:include path="lightboxWidthConf" resourceType="citrixosd/components/page/lightbox/lightboxWidth"/>
    </section>
</body>