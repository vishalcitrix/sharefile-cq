<%--
	Logo
	
	Fixed CSS classes will contain the logo and the logo can also 
	become a link. Default spite will be a placeholder and will only 
	appear in the wcm edit or read only mode.
		
	achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
	public static final String CSS_PROPERTY = "css";
	public static final String PATH_PROPERTY = "path";
	public static final String LINK_OPTION_PROPERTY = "linkOption";
%>

<c:set var="css" value="<%= properties.get(CSS_PROPERTY, null) %>"/>
<c:set var="path" value="<%= properties.get(PATH_PROPERTY, null) %>"/>
<c:set var="linkOption" value="<%=properties.get(LINK_OPTION_PROPERTY, "")%>"/>

<c:choose>
	<c:when test="${not empty css}">
		<c:choose>
			<c:when test="${not empty path && empty linkOption}">
				<a href="${path}"><span class="${css}"></span></a>
			</c:when>
			<c:when test="${not empty path && not empty linkOption}">
				<a href="${path}" rel="${linkOption}"><span class="${css}"></span></a>
			</c:when>
			<c:otherwise>
				<span class="${css}" rel="${linkOption}"></span>
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
        <c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)%>">
			<img src="/libs/cq/ui/resources/0.gif" class="cq-image-placeholder"/>
        </c:if>
	</c:otherwise>
</c:choose>