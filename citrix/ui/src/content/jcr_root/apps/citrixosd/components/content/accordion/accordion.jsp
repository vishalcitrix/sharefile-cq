<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<div class="accordian-container ${(isPreviewMode || isDisabledMode || isPublishInstance) ? 'active' : 'inactive'}">
    <div class="toggle-all">
        <a href="#" id="showAll"><fmt:message key="show.all"/></a><span class="toggle-splitter">|</span><a href="#" id="hideAll"><fmt:message key="hide.all"/></a>
    </div>

    <cq:include path="accordionpar" resourceType="citrixosd/components/content/accordion/accordionparsys" />
    
    <p class="gotop"><span class="return-to-top-arrow"> ^ </span><a href="#"><fmt:message key="return.to.top"/></a></p>
</div>