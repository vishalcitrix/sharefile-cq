<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp"%>

<body>
 	<cq:include script="ga.jsp" />
    <div id="content-body">
        <cq:include path="globalHeader" resourceType="swx/component-library/components/content/single-ipar"/>
        <cq:include script="noscript.jsp"/>
        <cq:include script="ieBrowserSupport.jsp"/>
        <cq:include path="mainContent" resourceType="foundation/components/parsys"/>
        <c:if test="${not properties.disableThreeShips && not isEditMode}">
        	<div id="threeShips">
        		<cq:include script="threeShips.jsp"/>
        	</div>
        </c:if>
        <cq:include path="floatingFooter" resourceType="swx/component-library/components/content/single-ipar"/>
    </div>  
    <cq:include script="footer.jsp" />
    <cq:include script="clientcontext.jsp" />
</body>