<%--

    Redirect logic.

    Allows for internal redirects via the RequestDispatcher.forward() method,
    used only for internal links. Sends a 404 response on attempts to call this
    on external links.

    Allows for 301/302 redirects to any domain or context path.

    Will send the redirect in cases of WCMMode.DISABLED. Otherwise, relies on
    redirectMessage.jsp for sending messages to front end about status of properties.


--%><%@ page import="com.day.cq.wcm.api.WCMMode,
                     com.day.cq.wcm.foundation.ELEvaluator,
                     com.citrixosd.utils.ContextRootTransformUtil" %>
<%@ page import="com.citrixosd.SiteUtils" %>
<%@ page import="java.net.URLEncoder" %>
<%
%><%@include file="/libs/foundation/global.jsp" %><%!
    // returns a string (without any ? or & prefix) with the __col_ cookies as request parameters
    String getCookieQueryString (Cookie[] cookies) {
        final String COOKIE_PREFIX = "__col_";
        //request parameter is "__col_mkt_cookies"
        boolean populated = false;
        StringBuilder queryString = new StringBuilder("__col_mkt_cookies=");
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().startsWith(COOKIE_PREFIX)) {
                    //this is a cookie we want to append as a request parameter
                    try {
                        queryString.append(URLEncoder.encode(cookie.getName() + "=" + cookie.getValue() + "; ", "UTF-8"));
                        populated = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (populated)
            return queryString.toString().replaceAll("\\+", "%20");
        else return "";

    }

%><%
    // read the redirect target from the 'page properties'
    String location = properties.get("redirectTarget", "");
    boolean isPermanent = true;
    String redirectType = properties.get("redirectOptions", "301");
    String queryString = request.getQueryString();
    String redirectErrorMessage = "";
    //Set type of redirect.
    if(redirectType.equals("302")) {
            isPermanent = false;
    }
    // resolve variables in location
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);

    // if in 'publish' or mode, handle the redirect
    //if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
    // check for recursion
    if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
        // check for absolute path
        final int protocolIndex = location.indexOf(":/");
        final int queryIndex = location.indexOf('?');
        final String redirectPath;
        if (  protocolIndex > -1 && (queryIndex == -1 || queryIndex > protocolIndex) ) {
            redirectPath = location;
        } else {
            redirectPath = request.getContextPath() + location;
        }
        request.setAttribute("redirectLocation", redirectPath);
        // normal, 301/302 redirect
        // need to handle the case where the redirect path has a query string already
        final int redirectQueryIndex = redirectPath.indexOf("?");
        String redirectLocation = "";
        String[] trackingDomains = SiteUtils.getTrackingDomainsAsArray(currentPage);
        boolean toTrack = false;
        if (trackingDomains != null && trackingDomains.length > 0) {
            for (String domain : trackingDomains) {
                if (redirectPath.contains(domain))
                    toTrack = true;
            }
        }
        if (toTrack) {
            //calculate the query string from request cookies
            Cookie[] cookies = request.getCookies();
            String cookieQueryString = getCookieQueryString(cookies);
            if (queryString != null && queryString.length() > 0) {
                queryString += "&" + cookieQueryString;
            } else {
                queryString = cookieQueryString;
            }
        }
        if (redirectQueryIndex == -1 && queryString != null) {
            redirectLocation = redirectPath + "?" + queryString;
        } else if (queryString != null) {
            // note no checking for duplicate request vars
            redirectLocation = redirectPath + "&" + queryString;
        } else {
            redirectLocation = redirectPath;
        }
        if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
            redirectLocation = ContextRootTransformUtil.transformedPath(redirectLocation,request);
            if(isPermanent) {
                response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
                response.setHeader("Location", redirectLocation);
            } else {
                response.sendRedirect(redirectLocation);
            }
        }
        else
            request.setAttribute("redirectLocation", redirectLocation);
    } else {
        if (WCMMode.fromRequest(request) == WCMMode.DISABLED)
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
    request.setAttribute("redirectErrorMessage", redirectErrorMessage);
    return;
    //}
%>