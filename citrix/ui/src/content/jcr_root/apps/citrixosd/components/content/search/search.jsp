<%--

  Search component
    - vishal.gupta@citrix.com
    - suresh.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@ page import="com.citrixosd.service.search.CustomSearch" %>
<%@ page import="com.day.cq.search.result.SearchResult" %>
<%@ page import="com.day.cq.search.result.Hit" %>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil" %>

<c:set var="customClass" value="<%= properties.get("customClass","") %>"/>

<swx:setWCMMode mode="READ_ONLY">
	<c:choose>
	    <c:when test="${empty properties.searchResults}">
	 	    <c:set var="action" value="<%= ContextRootTransformUtil.transformedPath(properties.get("action","#"),request) %>"/>
	        <c:set var="buttonlabel" value="<%= properties.get("buttonlabel","Search") %>"/>
	        <c:set var="dirPath" value="<%= properties.get("dirPath",currentPage.getAbsoluteParent(2).getPath()) %>"/>
	        <c:set var="dropDownLimit" value="<%= properties.get("resultLimit","10") %>"/>
	        <c:set var="resultsPerPage" value="<%= properties.get("resultsPerPage","10") %>"/>
	        <c:set var="contextRoot" value="<%= properties.get("contextRoot", "") %>"/>
	        <c:if test="${isEditMode}">
	        	<c:set var="contextRoot" value=""/>
			</c:if>

	        <div class="search-box ${customClass}">
	            <form id="searchForm" name="searchForm" action="${action}${(isDisabledMode || isEditMode || isReadOnlyMode) && ! fn:endsWith(action, 'html')  ? '.html': ''}" method="GET">
	                <input id="q" class="searchInput" autocomplete="off" data-suggest="${properties.autoSuggest ? properties.autoSuggest :"false"}" data-limit="${dropDownLimit}" data-path="${dirPath}" data-context="${contextRoot}" name="q" type="text" placeholder="${properties.inputTextLabel}">
	                <div class="suggestion-container">
	                	<div class="suggestionList" id="searchAutoSuggestionsList"></div>
	                </div>
	                <input id="offset" name="offset" type="hidden" value="0">
	                <span class="autosearch-icon icon-citirx-search"></span>
	            </form>
	        </div>
	    </c:when>
	    <c:otherwise>
	        <%
	            SearchResult result = null;
	            String resultHTML = "<div class='resultFound'>Sorry no results were found.</div>";
	            final String query = request.getParameter("q");
	            final String currentOffset = request.getParameter("offset");

	            if(query != null && !query.trim().isEmpty()) {
	              CustomSearch customSearch =  sling.getService(CustomSearch.class);
	              request.setAttribute("resultsPerPage",properties.get("resultsPerPage","10"));
	              request.setAttribute("dir",properties.get("dirPath",currentPage.getAbsoluteParent(2).getPath()));
	              result = customSearch.search(slingRequest);
	              resultHTML = customSearch.getSearchResultHTML(slingRequest);
	              long totalSearchPagesCount = customSearch.getTotalMachesPagesCount();
	              pageContext.setAttribute("result",result);
	              pageContext.setAttribute("query",query);
	              pageContext.setAttribute("currentOffset",currentOffset);
                  pageContext.setAttribute("totalMaches",totalSearchPagesCount);
	            }
	        %>
	        <div<c:if test="${not empty customClass}"> class="${customClass}"</c:if>>
		        <div class="searchResultContainer">
		             <%= resultHTML %>
		        </div>
				<div class="searchPagination">
				    <c:set var="html" value="${(isDisabledMode || isEditMode || isReadOnlyMode) ? '.html': ''}"/>
				    <c:set var="currentPageIndex" value="${(currentOffset/resultsPerPage)}"/>
				    <c:if test="${totalMaches > 1}">
				        <c:choose>
				            <c:when test="${(currentPageIndex + 1) < totalMaches}">
				                <a class="icon-RightArrow2 icon-right-arroyo" href="${currentPage.path}${html}?q=${query}&offset=${((result.nextPage.index )*resultsPerPage)}"></a>
				            </c:when>
				            <c:otherwise>
				                <span class="icon-RightArrow2 icon-right-arroyo"></span>
				            </c:otherwise>
				        </c:choose>
				        <c:choose>
				            <c:when test="${currentPageIndex > 0}">
				                <a class=" active icon-LeftArrow icon-left-arrow" href="${currentPage.path}${html}?q=${query}&offset=${((result.previousPage.index)*resultsPerPage)}"></a>
				            </c:when>
				            <c:otherwise>
				                <span class="icon-LeftArrow icon-left-arrow"></span>
				            </c:otherwise>
				        </c:choose>
				        <div class="pages">
				            <a class="${currentPageIndex == 0 ? 'active' : ''}" href="${currentPage.path}${html}?q=${query}&offset=0"></a>
				            <c:choose>
				                <c:when test="${totalMaches> 5}">
				                    <c:if test="${currentPageIndex > 2}">
				                        <span class="dots">.</span>
				                        <span class="dots">.</span>
				                        <span class="dots">.</span>
				                    </c:if>
				                    <c:forEach begin="${(currentPageIndex- 1) > 1 ? ((currentPageIndex+ 4) > totalMaches ? totalMaches - 4 : currentPageIndex - 1) : 1}" end="${(currentPageIndex) > 2 ? (currentPageIndex + 2 >= totalMaches? totalMaches - 2 : currentPageIndex + 1) : currentPageIndex + (3 - currentPageIndex)}" varStatus="i">
				                        <a class="${currentPageIndex == i.index ? 'active' : ''}" href="${currentPage.path}${html}?q=${query}&offset=${((i.index)*resultsPerPage)}"></a>
				                    </c:forEach>
				                    <c:if test="${(totalMaches - 3) > currentPageIndex}">
				                        <span class="dots">.</span>
				                        <span class="dots">.</span>
				                        <span class="dots">.</span>
				                    </c:if>
				                </c:when>
				                <c:otherwise>
				                    <c:forEach begin="1" end="${totalMaches - 2}" varStatus="i">
				                        <a class="${currentPageIndex == i.index ? 'active' : ''}" href="${currentPage.path}${html}?q=${query}&offset=${((i.index)*resultsPerPage)}"></a>
				                    </c:forEach>
				                </c:otherwise>
				            </c:choose>
				            <a class="last-three ${currentPageIndex == (totalMaches - 1) ? 'active' : ''}" href="${currentPage.path}${html}?q=${query}&offset=${((totalMaches-1)*resultsPerPage)}"></a><%--End --%>
				        </div>
				    </c:if>
				</div>
			</div>
	    </c:otherwise>
	</c:choose>
</swx:setWCMMode>