<%--
	Body: structure of the content
 --%>
<%@page import="com.citrixosd.SiteUtils"%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%-- Add optional product theme --%>
<c:set var="productTheme" value="<%= SiteUtils.getProductTheme(currentPage) %>"/>
<c:if test="${not empty productTheme}">
	<c:set var="productTheme" value=' class="${productTheme}"'/>
</c:if>
<body${productTheme}>

    <cq:include script="header.jsp"/>

    <div id="main" class="content">

        <cq:include path="bannerParsys" resourceType="swx/component-library/components/content/single-ipar"/>

        <div class="content-body container">

            <cq:include path="contentParsys" resourceType="foundation/components/parsys"/>

        </div>
    </div>

    <cq:include script="footer.jsp" />
</body>