<%--
	Property Modifer
	
	achew@siteworx.com
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="javax.jcr.query.QueryResult"%>
<%@page import="javax.jcr.query.Query"%>
<%@page import="javax.jcr.query.QueryManager"%>
<%!
	private static final String JCR_PROPERTY_PROPERTY = "jcrProperty";
	private static final String JCR_PROPERTY_VALUE_PROPERTY = "jcrPropertyValue";
	private static final String UPDATE_JCR_PROPERTY_VALUE_PROPERTY = "updateJcrPropertyValue";
 %>
 
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<%
	final String jcrProperty = properties.get(JCR_PROPERTY_PROPERTY, null);
	final String jcrPropertyValue = properties.get(JCR_PROPERTY_VALUE_PROPERTY, null);

	ArrayList<Node> results = new ArrayList<Node>();
	
	if(jcrProperty != null && jcrPropertyValue != null) {
	  	final String queryExpression = "SELECT * FROM [nt:base] WHERE [nt:base].'" + jcrProperty + "' = '" + jcrPropertyValue + "'";
		final QueryManager queryManager = resource.getResourceResolver().adaptTo(Session.class).getWorkspace().getQueryManager();
		final Query query = queryManager.createQuery(queryExpression, "JCR-SQL2");
		final QueryResult result = query.execute();
		final NodeIterator nodeIter = result.getNodes();
		
		while(nodeIter.hasNext()) {
			results.add(nodeIter.nextNode());
		}
	}
	
	final String[] updatedResults = request.getParameterValues("propertyModiferUpdated");
%>

<c:set var="results" value="<%= results %>"/>
<c:set var="updatedResults" value="<%= updatedResults %>"/>

<div class="property-modifer-container">
	<c:if test="${not empty updatedResults}">
		<c:forEach items="${updatedResults}" var="i">
			<div class="warning" style="text-align:left;">${i}</div>
		</c:forEach>
	</c:if>

	<form action="${resource.path}.update" method="POST">
		<input type="hidden" name="redirect" value="${currentPage.path}"/>
		<c:forEach items="${results}" var="i">
			<div>
				<input type="checkbox" name="paths" value="${i.path}"/><span>${i.path}</span>
			</div>
		</c:forEach>
		<input type="submit" value="Update"/>
	</form>
</div>
 
 