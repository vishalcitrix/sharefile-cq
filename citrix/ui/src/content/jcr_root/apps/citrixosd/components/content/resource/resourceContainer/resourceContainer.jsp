<%--
    Resource Container
    
    Container will hold any resource related components. Authors will have to choose which 
    resource type they want to display. Every resource type will automatically make a  container 
    for which authors can insert other resource components in which can be used to query. the 
    container will manage default language, additional languages. The result will automatically 
    render using default languages. If the end-user checks on any additional languages, this will 
    enable another query for resulting default language and any additional languages checked. 
    
    Dependencies: Resource (Javascript)

    achew@siteworx.com
    
    //fixed code the way serlvet was called, no more dependent on context root
    vishal.gupta@citrix.com
--%>

<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>
<%@page import="com.day.cq.personalization.ClientContextUtil"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="java.util.Properties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.day.cq.tagging.TagManager"%>
<%@page import="com.day.cq.tagging.Tag"%>

<%@include file="/libs/foundation/global.jsp" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%!
    private static final String ALLOWED_LANGUAGES_PROPERTY = "allowedLanguages";
    private static final String DEFAULT_LANGUAGE_PROPERTY = "defaultLanguage";
    private static final String ALLOWED_RESOURCE_TYPES_PROPERTY = "allowedResourceTypes";
    private static final String TITLE_PROPERTY = "title";
%>

<%
    final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    final String[] resourceTypes = properties.get(ALLOWED_RESOURCE_TYPES_PROPERTY, String[].class);
    final String[] languages = properties.get(ALLOWED_LANGUAGES_PROPERTY, String[].class);
    final String[] defaultLanguage = properties.get(DEFAULT_LANGUAGE_PROPERTY, String[].class);
    final List<Tag> resourceTypeTags = Utilities.getTags(resourceTypes, tagManager);
    final List<Tag> languageTags = Utilities.getTags(languages, tagManager);
    final Tag defaultLanguageTags = Utilities.getFirstTag(defaultLanguage, tagManager);
    final String resourceServletPath = "get.resourceservlet.html";
%>
<c:set var="containerId" value="<%= containerId %>"/>
<c:set var="resourceServletPath" value="<%= resourceServletPath %>"/>
<c:set var="resourceTypeTags" value="<%= resourceTypeTags %>"/>
<c:set var="defaultLanguage" value="<%= defaultLanguageTags %>"/>
<c:set var="languageTags" value="<%= languageTags %>"/>

<div resource-container="${containerId}" resource-servlet-path="${resourceServletPath}" default-language="${defaultLanguage.tagID}" time-zone="${properties.timeZone}" current-locale="<%= currentPage.getLanguage(false) %>">
    <div class="resource-toolbar" style="${fn:length(resourceTypeTags) == 1 && fn:length(languageTags) == 0 ? 'margin: 0 0 0 0;' : ''}">
        <c:if test="${not empty properties['title']}">
            <h4>${properties['title']}</h4>
        </c:if>
        <ul class="resource-link-type-container">
            <c:forEach items="${resourceTypeTags}" var="resourceTypeTag" varStatus="i">
                <li class="resource-type-item" style="${fn:length(resourceTypeTags) == 1 ? 'display:none' : ''}">
                    <c:choose>
                        <c:when test="${i.first}">
                            <%--Do not display pipe --%>
                        </c:when>
                        <c:otherwise>
                            <span class="resource-type-pipe">/</span>
                        </c:otherwise>
                    </c:choose>
                    <a class="resource-type-link" href="#${containerId}_${resourceTypeTag.name}"><fmt:message key="resource.type.${resourceTypeTag.name}"/></a>
                </li>
            </c:forEach>
        </ul>
        
        <ul class="resource-link-language-container">
            <c:forEach items="${languageTags}" var="languageTag" varStatus="i">
                <li class="resource-language-item">
                    <input id="resource-language_${languageTag.name}" class="resource-language-link" type="checkbox" value="${languageTag.tagID}"/>
                    <label for="resource-language_${languageTag.name}"><fmt:message key="resource.language.${languageTag.name}"/></label>
                </li>
            </c:forEach>
        </ul>
        
        <div class="clearBoth"></div>
    </div>
    <c:forEach items="${resourceTypeTags}" var="resourceTypeTag" varStatus="i">
        <c:if test="${isEditMode}"><div class="warning">${resourceTypeTag.title} Container</div></c:if>
        <div id="${containerId}_${resourceTypeTag.name}" resource-type-container="${resourceTypeTag.name}" style="${isEditMode ? '' : 'display: none;'}">
            <cq:include path="${resourceTypeTag.name}_container" resourceType="foundation/components/parsys"/>
        </div>
    </c:forEach>
</div>

<c:if test="${isEditMode}">
    <div class="warning">Resource Container: End</div>
</c:if>
