/*
 * Copyright 1997-2009 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */

/**
 * @class CQ.form.rte.plugins.LinkPlugin
 * @extends CQ.form.rte.plugins.Plugin
 * <p>This class implements links and anchors as a plugin.</p>
 * <p>The plugin ID is "<b>links</b>".</p>
 * <p><b>Features</b></p>
 * <ul>
 *   <li><b>modifylink</b> - adds a button to create and modify links</li>
 *   <li><b>unlink</b> - adds a button to remove existing links</li>
 *   <li><b>anchor</b> - adds a button to define anchors</li>
 * </ul>
 * <p><b>Additional config requirements (CQ 5.2)</b></p>
 * <p>Plugin configuration has not been moved to the respective plugin completely yet.
 * Therefore, the following plugin-specific settings must currently (CQ 5.2) be configured
 * through the corresponding {@link CQ.form.RichText} widget:</p>
 * <ul>
 *   <li>The dialog that is used to create and modify links must be configured using
 *     {@link CQ.form.RichText#linkbrowseConfig}.</li>
 *   <li>The dialog that is used to define an anchor must be provided through
 *     {@link CQ.form.RichText#anchordialogConfig}.</li>
 * </ul>
 */
LinkRelPlugin.Plugin = CQ.Ext.extend(CQ.form.rte.plugins.Plugin, {

    /**
     * @cfg {Boolean} trimLinkSelection
     * True if leading and trailing whitespace should removed from the selection (not from
     * the actual text/content!) before creating a new link (defaults to true).
     * @since 5.3
     */

    /**
     * @cfg {Object} linkDialogConfig
     * @since 5.3
     */

    /**
     * @cfg {Object} anchorDialogConfig
     * Configuration of the anchor dialog (defaults to { }). You may specify the same
     * config options as for {@link CQ.Dialog}. Note that the default value
     * of null implies using a default dialog.
     * @since 5.3
     */

    /**
     * @private
     */
    linkDialog: null,

    /**
     * @private
     */
    anchorDialog: null,

    /**
     * @private
     */
    linkUI: null,

    /**
     * @private
     */
    removeLinkUI: null,

    constructor: function(editorKernel) {
        LinkRelPlugin.Plugin.superclass.constructor.call(this, editorKernel);
    },

    getFeatures: function() {
        return [ "modifyrellink", "relunlink" ];
    },

    /**
     * Creates a link using the internal link dialog.
     * @private
     */
    modifyRelLink: function(context) {
        var com = CQ.form.rte.Common;
        if (!this.linkDialog || this.linkDialog.isDestroyed) {
            var linkRules = this.editorKernel.htmlRules.links;
            var dialogConfig = {
                "configVersion": 1,
                "defaultDialog": {
                    "dialogClass": {
                        "xtype": "rtelinkreldialog"
                    }
                },
                "parameters": {
                    "linkRules": linkRules,
                    "editorKernel": this.editorKernel
                }
            };
            if (this.config.linkDialogConfig) {
            //if we had the config node in jcr
                var addDialogConfig = this.config.linkDialogConfig;
                if (addDialogConfig.linkAttributes) {
                    com.removeJcrData(addDialogConfig.linkAttributes);
                    var linkAttribs = com.toArray(addDialogConfig.linkAttributes);
                    dialogConfig.additionalFields = [ ];
                    var attribCnt = linkAttribs.length;
                    for (var a = 0; a < attribCnt; a++) {
                        var attrib = linkAttribs[a];
                        var attribName = attrib.attribute;
                        var itemData = {
                            "item": {
                                "name": attribName,
                                "xtype": attrib.xtype,
                                "fieldLabel": attrib.fieldLabel
                            },
                            "fromModel": function(obj, field) {
                                if (field.xtype == "hidden") return;
                                var attribName = field.getName();
                                var attribValue = com.getAttribute(obj.dom, attribName);
                                
                                if (attribValue) {
                                    field.setValue(attribValue);
                                } else {
                                    field.setValue("");
                                }
                            },
                            "toModel": function(obj, field) {
                                var attribName = field.getName();
                                if (!obj.attributes) {
                                    obj.attributes = { };
                                }
                                var value = field.getValue();
                                if (value && (value.length > 0)) {
                                    obj.attributes[attribName] = value;
                                } else {
                                    obj.attributes[attribName] =
                                        CQ.form.rte.commands.Link.REMOVE_ATTRIBUTE;
                                }
                            }
                        };
                        delete attrib.attribute;
                        delete attrib.xtype;
                        delete attrib.fieldLabel;
                        CQ.Util.applyDefaults(itemData.item, attrib);
                        dialogConfig.additionalFields.push(itemData);
                    }
                    delete addDialogConfig.linkAttributes;
                }
                dialogConfig.dialogProperties = addDialogConfig;
            }
            if(linkRules.relConfig){
                if (!dialogConfig.additionalFields) {
                    dialogConfig.additionalFields = [];
                }
                dialogConfig.additionalFields.push({
                       
                        "fromModel": function(obj, field) {
                            if (obj.dom && obj.dom["rel"]) {
                                field.setValue(obj.dom["rel"]);
                            }
                        },
                        "toModel": function(obj, field) {
                            if (!obj.attributes) {
                                obj.attributes = { };
                            }
                            var value = field.getValue();
                            if (value && (value.length > 0)) {
                                obj.attributes["rel"] = value;
                            } else {
                                obj.attributes["rel"] = null;
                            }

                        }
                    });
            }
            
            CQ.Util.applyDefaults(dialogConfig, this.config.linkDialogConfig || { });
            var dialogHelper = new CQ.form.rte.ui.DialogHelper(dialogConfig,
                    this.editorKernel);
            this.linkDialog = dialogHelper.create();
            dialogHelper.calculateInitialPosition();
        }
        var linkToEdit = null;
        var selectionDef = this.editorKernel.analyzeSelection();
        if (selectionDef.anchorCount == 1) {
            linkToEdit = selectionDef.anchors[0];
        }
        linkToEdit = linkToEdit || { };
        if (typeof linkToEdit.attributes === 'undefined')
            linkToEdit.attributes = { };
          this.linkDialog.initializeEdit(this.editorKernel, linkToEdit,
                this.applyLink.createDelegate(this));
        if (CQ.Ext.isIE) {
            this.savedRange = context.doc.selection.createRange();
        }
        this.linkDialog.show();
    },

    applyLink: function(context) {
        var linkObj = this.linkDialog.objToEdit;
        if (linkObj) {
            var linkUrl = linkObj.href;
            var cssClass = linkObj.cssClass;
            var target = linkObj.target;
            var rel = linkObj.rel;
            if (CQ.Ext.isIE) {
                this.savedRange.select();
            }

            this.editorKernel.relayCmd("modifyrellink", {
                "url": linkUrl,
                "css": cssClass,
                "target": target,
                "rel": rel,
                "attributes": linkObj.attributes
            });
        }
    },

    initializeUI: function(tbGenerator) {
        var plg = CQ.form.rte.plugins;
        var ui = CQ.form.rte.ui;
        if (this.isFeatureEnabled("modifyrellink")) {
            this.linkUI = new ui.TbElement("modifyrellink", this, false,
                    this.getTooltip("modifyrellink"));
            tbGenerator.addElement("rel-link", 140, this.linkUI, 10);
        }
        if (this.isFeatureEnabled("relunlink")) {
            this.removeLinkUI = new ui.TbElement("relunlink", this, false,
                    this.getTooltip("relunlink"));
            tbGenerator.addElement("rel-link", 140, this.removeLinkUI, 20);
        }
    },

    notifyPluginConfig: function(pluginConfig) {
        pluginConfig = pluginConfig || { };
        CQ.Util.applyDefaults(pluginConfig, {
            "features": "*",
            "trimLinkSelection": true,
            "linkDialogConfig": {
                "targetConfig": {
                    "mode": "manual"
                },
                "relConfig":{
                    "mode": "manual"
                }
            },
            "anchorDialogConfig": {
                // empty by default
            },
            "tooltips": {
                "modifyrellink": {
                    "title": CQ.I18n.getMessage("Hyperlink"),
                    "text": CQ.I18n.getMessage("Create or modify a hyperlink.")
                },
                "relunlink": {
                    "title": CQ.I18n.getMessage("Unlink"),
                    "text": CQ.I18n.getMessage("Remove an existing hyperlink from the selected text.")
                }
            }
        });
        this.config = pluginConfig;
    },

    execute: function(cmd, value, env) {
        if (cmd == "modifyrellink") {
            this.modifyRelLink(env.editContext);    
        } else {
            this.editorKernel.relayCmd(cmd);
        }
    },

    updateState: function(selDef) {
        var hasSingleAnchor = selDef.anchorCount == 1;
        var hasNoAnchor = selDef.anchorCount == 0;
        var selectedNode = selDef.selectedDom;
        var isLinkableObject = false;
        if (selectedNode) {
            isLinkableObject = CQ.form.rte.Common.isTag(selectedNode,
                    LinkRelPlugin.Plugin.LINKABLE_OBJECTS);
        }
        var isCreateLinkEnabled = hasSingleAnchor
                || ((selDef.isSelection || isLinkableObject) && hasNoAnchor);
        if (this.linkUI) {
            this.linkUI.getExtUI().setDisabled(!isCreateLinkEnabled);
        }
        if (this.removeLinkUI) {
            this.removeLinkUI.getExtUI().setDisabled(!hasSingleAnchor);
        }
        if (this.anchorUI) {
            var hasSingleNamedAnchor = (selDef.namedAnchorCount == 1);
            this.anchorUI.getExtUI().toggle(hasSingleNamedAnchor);
        }
    }

});

/**
 * Array with tag names that define objects (like images) that are linkable when selected
 * @private
 * @static
 * @final
 * @type String[]
 */
LinkRelPlugin.Plugin.LINKABLE_OBJECTS = [
    "img"
];


// register plugin
CQ.form.rte.plugins.PluginRegistry.register("rel-link", LinkRelPlugin.Plugin);