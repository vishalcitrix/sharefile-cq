<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!public static final String PAR_NAME = "entrypar";%>

<%
	boolean entryparParent = currentNode.getParent().getName().startsWith(PAR_NAME);
%>

<c:set var="entryParent" value="<%=entryparParent%>" />

<div>
	<div class="text ${entryParent ? 'question' : 'plus'} ${properties.isEven ? 'even' : 'odd'} ">
		<c:if test="${entryParent}">
			<span><fmt:message key="accordion.question" /></span>
		</c:if>
		<div>${not empty properties.text ? properties.text : 'Edit Title' }</div>
	</div>

	<div class="category">
		<c:choose>
			<c:when test="${entryParent}">
				<cq:include script="content.jsp" />
			</c:when>
			<c:otherwise>
				<div class="showBorder">
					<cq:include path="entrypar"
						resourceType="foundation/components/parsys" />
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>