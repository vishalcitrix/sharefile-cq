<%--
    vishal.gupta@citrix.com
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.sharefile.utils.SfUtilities"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    private static final String PHONE_PROPERTY = "geoSegments";

    public class GeoSegments {
        private String cname;
        private String country;
        private String updatedtext;
      
        public GeoSegments() {
            
        }
        public void setCname(String cname) {
            this.cname = cname;
        }
        public String getCname() {
            return this.cname;
        }
        public void setCountry(String country) { 
            this.country = country;
        }
        public String getCountry() {
            return this.country;
        }
        public void setUpdatedtext(String updatedtext) { 
            this.updatedtext = updatedtext;
        }
        public String getUpdatedtext() {
            return this.updatedtext;
        }     
    }
    
    public List<GeoSegments> getSegmentsList(NodeIterator nodeIter) {        
        final List<GeoSegments> segments = new ArrayList<GeoSegments>();
        while(nodeIter.hasNext()) {
            try {
                final Node currLink = nodeIter.nextNode();
                final GeoSegments segmentList = new GeoSegments();               
                segmentList.setCname(currLink.hasProperty("class") ? currLink.getProperty("class").getString() : null);
                segmentList.setCountry(currLink.hasProperty("country") ? currLink.getProperty("country").getString() : null);
                segmentList.setUpdatedtext(currLink.hasProperty("updatedText") ? currLink.getProperty("updatedText").getString() : null);
                if(segmentList.getCname() != null && segmentList.getCountry() != null && segmentList.getUpdatedtext() != null) {
                	segments.add(segmentList);   
                }
            } catch (RepositoryException re) {
                re.printStackTrace();
            } 
        }
        return segments;
    }
    
   /**
    * Returns a HashMap with parent links as the key and its respective
    * child nodes stored in a nested hashmap.
    */
    public static HashMap<String,HashMap<String,String>> getChildNodes(List<GeoSegments> nodes) {  
        HashMap<String,HashMap<String,String>> parentChildMap = new HashMap<String,HashMap<String,String>>();
        final Iterator<GeoSegments> i = nodes.iterator();     
        while(i.hasNext()){
           final GeoSegments nodeCurrent = i.next();
           if(parentChildMap.containsKey(nodeCurrent.getCountry())) {
               HashMap<String,String> currentChildMap = parentChildMap.get(nodeCurrent.getCountry());       
               currentChildMap.put(nodeCurrent.getCname(),nodeCurrent.getUpdatedtext());
           }else {
               HashMap<String,String> currentChildMap = new HashMap<String,String>();
               currentChildMap.put(nodeCurrent.getCname(),nodeCurrent.getUpdatedtext());
               parentChildMap.put(nodeCurrent.getCountry(),currentChildMap);
           }
        }
        return parentChildMap;
    }        
%>

<%
    List<GeoSegments> segments = null;
    HashMap<String, HashMap <String,String>> nodeMap = null;
    final Node baseNode = SfUtilities.getGeoSegmentationNode(currentPage);
    
    if (baseNode != null) {
        final NodeIterator nodeIter = baseNode.getNodes();
        segments = getSegmentsList(nodeIter);
        nodeMap = getChildNodes(segments);
    }
%>

<c:set var="nodeMap" value="<%= nodeMap %>"/>

<c:if test="${fn:length(nodeMap) > 0}">
    <script type="text/javascript">
    	function updateSegment() {
            if(!country) {
                setTimeout(updateSegment,200)
            } else {
	            <c:forEach items="${nodeMap}" var="item">
	                if(country == "${item.key}") {
	                    <c:forEach items="${nodeMap[item.key]}" var="childItem">$("${childItem.key}").html("${childItem.value}");</c:forEach>
	                }
	            </c:forEach>
            }
        }
        $(document).ready(function(){
        	updateSegment();
        });
    </script>
</c:if>
