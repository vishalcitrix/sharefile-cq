<%@ page import="com.day.cq.tagging.TagManager,
                 com.citrixosdRedesign.utils.ResourceUtils"%>
                
<%@include file="/apps/citrixosd/global.jsp"%>

<%
    final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    final String[] industries = properties.get("industries", String[].class);  
%>

<c:set var="industries" value="<%= ResourceUtils.getTags(industries, tagManager)%>"/>

<div class="resourcesFeature-container">
    <div class="events <c:forEach items="${industries}" var="industry">${industry.tagID}</c:forEach>"> 
         <div class="logo">  
            <a href="${properties.firmURL}" rel="external">
                <cq:include path="image" resourceType="citrixosd/components/content/imageRenditions" />
            </a> 
            <p class="firmURL"><a href="${properties.firmURL}" rel="external" >${properties.firmUrlText}</a></p>           
        </div>   
        <div class="description">
            <c:if test="${not empty properties.description}">
                <p>${properties.description}</p>
            </c:if>
        </div>
        <div class="moreLink">
            <c:if test="${not empty properties.caseStudyCopy && not empty properties.path}">
                <a href="${properties.path}" class="caseStudyPath" >${properties.caseStudyCopy}</a> 
            </c:if> 
        </div>        
        <c:if test="${properties.hideSeperator ne 'true'}">
            <hr class="seperator">
        </c:if>
    </div>
</div>