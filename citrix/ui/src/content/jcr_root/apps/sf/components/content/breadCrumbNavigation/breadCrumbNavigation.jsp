<%--
Bread crumb navigation component
--%>

<%@page import="com.day.cq.wcm.api.WCMMode,com.citrixosd.utils.ContextRootTransformUtil,java.util.*"%>
<%@include file="/apps/citrixosd/global.jsp"%>
 
<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>

<%
    HashMap<String, String> pageDetails = new LinkedHashMap();
    String path,title = null;
    
    for (int i=currentPage.getDepth(); i>=3 ; i--){
        path = ContextRootTransformUtil.transformedPath(currentPage.getParent(i-3).getPath(),request);
        title = currentPage.getParent(i-3).getTitle();
        pageDetails.put(path, title);
    }
%>

<c:set var="pathArray" value="<%= pageDetails %>"/>
<div class="breadCrumbNav" style="<c:if test="${not empty properties.backgroundColor}">background-color:${properties.backgroundColor};</c:if>                                  
                                  <c:if test="${not empty properties.padding}">padding:${properties.padding};</c:if> ">
    <c:if test="${pageDepth > 3}">
        <div class="links">
            <c:forEach var="details" items="${pathArray}" varStatus="status">               
                <a href="${details.key}">
                    <c:choose>
                        <c:when test="${status.count eq 1}">
                            <c:choose>
                                  <c:when test="${not empty properties.rootLabel}">${properties.rootLabel}</c:when>
                                  <c:otherwise>${details.value}</c:otherwise>
                            </c:choose>
                        </c:when>
                        
                        <c:otherwise>${details.value}</c:otherwise>
                    </c:choose>
                </a>  
            </c:forEach>
        </div>
    </c:if>
</div>