<%@page import="org.apache.jackrabbit.commons.JcrUtils,
				java.lang.StringBuilder,
				javax.jcr.Session,java.util.ArrayList,
				java.util.Map,com.citrixosd.utils.Utilities,
				com.day.cq.wcm.api.WCMMode,
				java.util.Locale,java.util.ResourceBundle"%>

<%@include file="/apps/citrixosd/global.jsp"%>

<%
	final Page localePage = currentPage.getAbsoluteParent(2);
	final Locale locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
	final ResourceBundle resourceBundle = slingRequest.getResourceBundle(locale);   
%>

<%!
	public static final String BASE_CLOUD_NODE = "clouds";
	public static final String CHILD_CLOUD_NODE = "cloud";
	public static final String CHILD_CLOUD_PROPERTY = "cloudname";
	public static final String CHILD_TEXT_PROPERTY = "cloudtext";

	public String getCol1Text(ResourceBundle resourceBundle) {
		StringBuilder col1Text = new StringBuilder();
		col1Text.append("<div>").append(resourceBundle.getString("unityNav.sf.productCol1")).append("</div>");
		col1Text.append("<ul>");
			col1Text.append("<li>");
				col1Text.append("<a target='_blank' href='" + resourceBundle.getString("unityNav.productSC.link") + "'>");
					col1Text.append("<span class='sc'></span>");
					col1Text.append("<div>").append(resourceBundle.getString("unityNav.productSC"));
						col1Text.append("<p>" + resourceBundle.getString("unityNav.productSC.sf.desc") + "</p>");
					col1Text.append("</div>");
				col1Text.append("</a>");
			col1Text.append("</li>");
		col1Text.append("</ul>");
		
		return col1Text.toString();
	}
	
	public String getCol2Text(ResourceBundle resourceBundle) {
		StringBuilder col2Text = new StringBuilder();
		col2Text.append("<div>" + resourceBundle.getString("unityNav.sf.productCol2") + "</div>");
		col2Text.append("<ul>");
			col2Text.append("<li>");
				col2Text.append("<a target='_blank' href='" + resourceBundle.getString("unityNav.productRS.link") + "'>");
					col2Text.append("<span class='rs'></span>");
					col2Text.append("<div>").append(resourceBundle.getString("unityNav.productRS"));
						col2Text.append("<p>" + resourceBundle.getString("unityNav.productRS.sf.desc") + "</p>");
					col2Text.append("</div>");
				col2Text.append("</a>");
			col2Text.append("</li>");
		col2Text.append("</ul>");
		
		return col2Text.toString();
	}
	
	public String getCol3Text(ResourceBundle resourceBundle) {
		StringBuilder col3Text = new StringBuilder();
		col3Text.append("<div>" + resourceBundle.getString("unityNav.sf.productCol3") + "</div>");
		col3Text.append("<ul>");
			col3Text.append("<li>");
				col3Text.append("<a target='_blank' href='" + resourceBundle.getString("unityNav.productPO.link") + "'>");
					col3Text.append("<span class='po'></span>");
					col3Text.append("<div>").append(resourceBundle.getString("unityNav.productPO"));
						col3Text.append("<p>" + resourceBundle.getString("unityNav.productPO.sf.desc") + "</p>");
					col3Text.append("</div>");
				col3Text.append("</a>");
			col3Text.append("</li>");
		col3Text.append("</ul>");
		
		return col3Text.toString();
	}
	
	//function to create nodes for each cloud
	public void createCloudNode(Node parentNode,ResourceBundle resourceBundle) throws RepositoryException {
		if(!parentNode.hasNode(BASE_CLOUD_NODE)) {
			parentNode.addNode(BASE_CLOUD_NODE);
			parentNode.save();
			
			//get text from appropriate cloud text functions
			final String col1 = getCol1Text(resourceBundle);
			final String col2 = getCol2Text(resourceBundle);
			final String col3 = getCol3Text(resourceBundle);
			
			Node baseCloudNode = parentNode.getNode(BASE_CLOUD_NODE);
			
			//add col1 node
			Node newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "1");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Column1");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, col1);
			newCloudNode.save();
			
			//add col2 node
			newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "2");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Column2");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, col2);
			newCloudNode.save();
			
			//add col3 node
			newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "3");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Column3");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, col3);
			newCloudNode.save();
		}
	}
%>
<% 
	if(WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)) {
		//Create component node if it does not exist
	    if(currentNode == null) {
	    	final Session session = resource.getResourceResolver().adaptTo(Session.class);
	    	currentNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
	    	session.save();
	    }
		
	    createCloudNode(currentNode,resourceBundle);
	}

	//get text from cloud nodes
	ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
	if(currentNode != null && currentNode.hasNode("clouds")) {
	    final Node baseNode = currentNode.getNode("clouds");
	    values = Utilities.parseStructuredMultifield(baseNode);
	}
%>

<c:set var="values" value="<%= values %>"/>

<div class="products">
    <span class="close"></span>
    <p><fmt:message key="unityNav.menu.drawerText"/></p>
    <div class="row">
    	<c:choose>
	        <c:when test="${fn:length(values) > 0}">
	            <c:forEach items="${values}" var="item">
	            	 <div class="columns large-4 medium-4">
	            	 	${item.cloudtext.string}
	            	 </div>
	            </c:forEach>
	        </c:when>
	        <c:otherwise>
	            <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Please check for cloud nodes under global header.</div></c:if>
	        </c:otherwise>
        </c:choose>
    </div>
</div>