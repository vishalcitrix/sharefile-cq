<%--
    Category Spotlight component
--%>

<%@page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%
    boolean hasImageRef = currentNode != null ? currentNode.hasNode("imageRenditions") ? currentNode.getNode("imageRenditions").hasProperty("fileReference") : false : false;
    pageContext.setAttribute("hasImageRef", hasImageRef );
%>
<c:set var="titleValue" value="${not empty properties.title ? properties.title : 'Please choose a title'}"/>

<c:if test="${(isEditMode or isReadOnlyMode) or (not empty properties.title and hasImageRef)}">
	<a href="${properties.linkPath}" rel="${properties.linkOption}">  
	    <cq:include path="imageRenditions" resourceType="/apps/citrixosd-responsive/components/content/imageRenditions"/>
	    <div class="hoverContent">
	        <span class="${properties.categoryIcon}"></span>
	        <h3>${titleValue}</h3>
	        <hr>
	        <p>${properties.subtitle}</p>
	    </div>   
	</a>  
</c:if>           
<div class="clearBoth"></div> 