<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil,
                com.citrixosdRedesign.constants.ResourceConstants,
                com.citrixosd.utils.Utilities"%>

<c:set var="resourceType" value="<%= Utilities.getFirstStringArrayFromArrayProperty(currentNode,"types") %>"/>
<c:set var="count" value="0" scope="request" />
<fmt:formatDate type="date" pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>" var="currentDate" />

<section class="resourcesFeature-container row">
	<c:choose>
	    <c:when test="${fn:length(resourceList.list) > 0}">
	    	<c:choose>
	    		<c:when test="${resourceType eq 'resource-category-type:event'}">
	    			<div class="heading">
	    				<div class="large-4 medium-4 columns">
	    					${properties.heading}	
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.type"/>
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.product"/>
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.date"/>
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.location"/>
	    				</div>
	    			</div>
	    			<c:forEach items="${resourceList.list}" var="resourceItem">
	    				<c:choose>
	    					<c:when test="${not empty resourceItem.eventEndDate}">
	    						<fmt:formatDate value="${resourceItem.eventEndDate}" pattern="yyyy-MM-dd" var="eventdate" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatDate value="${resourceItem.eventStartDate}" pattern="yyyy-MM-dd" var="eventdate" />
	    					</c:otherwise>
	    				</c:choose>
	    				<c:if test="${eventdate >= currentDate}">
	    					<c:set var="count" value="${count + 1}" scope="request"/>
		    				<div class="events <c:forEach items="${resourceItem.products}" var="product">${product} </c:forEach><c:forEach items="${resourceItem.topics}" var="topic">${topic} </c:forEach><c:forEach  items="${resourceItem.industries}" var="industry">${industry} </c:forEach><c:if test="${count > 5}"> hide</c:if>">
		    					<div class="large-4 medium-4">
			    					<a href="${(resourceItem.isGated) && (not isEditMode) ? (not empty resourceItem.externalLink ? resourceItem.externalLink : '#') : resourceItem.path}" rel="external">
			    						${resourceItem.title}
			    					</a>
			    				</div>
			    				<div class="large-2 medium-2">
			    					${resourceItem.resourceType}
			    				</div>
			    				<div class="large-2 medium-2">
			    					<c:forEach items="${resourceItem.productsTitle}" var="product">${product}<br></c:forEach>
			    				</div>
			    				<div class="large-2 medium-2">
			    					<fmt:formatDate value="${resourceItem.eventStartDate}" type="date" />
			    					<c:choose>
			    						<c:when test="${not empty resourceItem.eventEndDate}">
			    							- <fmt:formatDate value="${resourceItem.eventEndDate}" type="date" />
			    						</c:when>
			    						<c:otherwise>
			    							<br>
			    							<fmt:formatDate value="${resourceItem.eventStartDate}" pattern="hh:mm a" /> ${resourceItem.eventTimeZone}
			    						</c:otherwise>
			    					</c:choose> 
			    				</div>
			    				<div class="large-2 medium-2">
			    					${resourceItem.eventCities}<c:if test="${not empty resourceItem.eventCountry}"> - ${resourceItem.eventCountry}</c:if>
			    				</div>
		    				</div>
	    				</c:if>
			      	</c:forEach>
			      	<c:if test="${count > 5}">
			      		<div class="more-list">
							<a class="show" href="#${resourceList.id}">${properties.seeMore}</a>
							<a class="hide" href="#${resourceList.id}">${properties.hideMore}</a>
						</div>
			      	</c:if>
	    		</c:when>
	    		
	    		<c:when test="${resourceType eq 'resource-category-type:webinar'}">
	    			<div class="heading">
	    				<div class="large-4 medium-4 columns">
	    					${properties.heading}	
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up show-for-medium-up">
	    					<fmt:message key="resource.event.type"/>
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.product"/>
	    				</div>
	    				<div class="large-2 medium-2 columns show-for-medium-up">
	    					<fmt:message key="resource.event.release.date"/>
	    				</div>
	    				<div class="large-2 medium-2 columns"></div>
	    			</div>
	    			<c:forEach items="${resourceList.list}" var="resourceItem">
	    				<c:choose>
	    					<c:when test="${not empty resourceItem.eventEndDate}">
	    						<fmt:formatDate value="${resourceItem.eventEndDate}" pattern="yyyy-MM-dd" var="webinarDate" />
	    					</c:when>
	    					<c:otherwise>
	    						<fmt:formatDate value="${resourceItem.eventStartDate}" pattern="yyyy-MM-dd" var="webinarDate" />
	    					</c:otherwise>
	    				</c:choose>
    					<c:if test="${not resourceItem.onDemand && webinarDate >= currentDate}">
    						<c:set var="count" value="${count + 1}" scope="request"/>
    						<div class="events <c:forEach items="${resourceItem.products}" var="product">${product} </c:forEach><c:forEach items="${resourceItem.topics}" var="topic">${topic} </c:forEach><c:forEach  items="${resourceItem.industries}" var="industry">${industry} </c:forEach><c:if test="${count > 5}"> hide</c:if>">
		    					<div class="large-4 medium-4">
			    					<a href="${(resourceItem.isGated) && (not isEditMode) ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : resourceItem.path}" rel="external">
			    						${resourceItem.title}
			    					</a>
			    				</div>
			    				<div class="large-2 medium-2">
			    					${resourceItem.resourceType}
			    				</div>
			    				<div class="large-2 medium-2">
			    					<c:forEach items="${resourceItem.productsTitle}" var="product">${product}<br></c:forEach>
			    				</div>
			    				<div class="large-2 medium-2">
			    					<fmt:formatDate value="${resourceItem.eventStartDate}" type="date" />
			    					<c:choose>
			    						<c:when test="${not empty resourceItem.eventEndDate}">
			    							- <fmt:formatDate value="${resourceItem.eventEndDate}" type="date" />
			    						</c:when>
			    						<c:otherwise>
			    							<br>
			    							<fmt:formatDate value="${resourceItem.eventStartDate}" pattern="hh:mm a" /> ${resourceItem.eventTimeZone}
			    						</c:otherwise>
			    					</c:choose>
			    				</div>
			    				<div class="large-2 medium-2">
			    					<fmt:message key="resource.webinar.upcoming"/>
			    				</div>
		    				</div>
    					</c:if>
			      	</c:forEach>
	    			<c:forEach items="${resourceList.list}" var="resourceItem">
    					<c:if test="${resourceItem.onDemand}">
    						<c:set var="count" value="${count + 1}" scope="request"/>
    						<div class="events <c:forEach items="${resourceItem.products}" var="product">${product} </c:forEach><c:forEach items="${resourceItem.topics}" var="topic">${topic} </c:forEach><c:forEach  items="${resourceItem.industries}" var="industry">${industry} </c:forEach><c:if test="${count > 5}"> hide</c:if>">
		    					<div class="large-4 medium-4">
			    					<a href="${(resourceItem.isGated) && (not isEditMode) ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : resourceItem.path}" rel="external">
			    						${resourceItem.title}
			    					</a>
			    				</div>
			    				<div class="large-2 medium-2">
			    					${resourceItem.resourceType}
			    				</div>
			    				<div class="large-2 medium-2">
			    					<c:forEach items="${resourceItem.productsTitle}" var="product">${product}<br></c:forEach>
			    				</div>
			    				<div class="large-2 medium-2">
			    					<fmt:formatDate value="${resourceItem.eventStartDate}" type="date" />
			    					<c:choose>
			    						<c:when test="${not empty resourceItem.eventEndDate}">
			    							- <fmt:formatDate value="${resourceItem.eventEndDate}" type="date" />
			    						</c:when>
			    						<c:otherwise>
			    							<br>
			    							<fmt:formatDate value="${resourceItem.eventStartDate}" pattern="hh:mm a" /> ${resourceItem.eventTimeZone}
			    						</c:otherwise>
			    					</c:choose>
			    				</div>
			    				<div class="large-2 medium-2">
			    					<fmt:message key="resource.webinar.ondemand"/>
			    				</div>
		    				</div>
    					</c:if>
			      	</c:forEach>
			      	<c:if test="${count > 5}">
			      		<div class="more-list">
							<a class="show" href="#${resourceList.id}">${properties.seeMore}</a>
							<a class="hide" href="#${resourceList.id}">${properties.hideMore}</a>
						</div>
			      	</c:if>
	    		</c:when>
	    		<c:otherwise>
	    			<div class="resource-list-no-results">
		                <span><fmt:message key="resource.list.wrong.category"/></span>
		            </div>
	    		</c:otherwise>
	    	</c:choose>
	    </c:when>
	    <c:otherwise>
            <div class="resource-list-no-results">
                <span>${properties.noResultsFound}</span>
            </div>
	    </c:otherwise>
	</c:choose>
</section>