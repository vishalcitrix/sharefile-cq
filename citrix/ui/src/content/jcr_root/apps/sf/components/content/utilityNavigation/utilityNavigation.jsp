<%--
    Utility Navigation

    Header contains logo component on the right and link set component on the right.
    Note: Depends on logo and linkSet component.
    
    vishal.gupta@citrix.com
    ingrid.tseng@citrix.com
--%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!
    public static final String SIDE_NAV_LABEL = "sideNavLabel";
	public static final String DISPLAY_HOMEPAGE_LINKS = "displayHomepageLinks";
%>

<c:set var="sideNavLabel" value="<%= properties.get(SIDE_NAV_LABEL, "global.menu") %>"/>
<c:set var="displayHomepageLinks" value="<%= properties.get(DISPLAY_HOMEPAGE_LINKS) %>"/>
<c:set var="pageDepth" value="<%= currentPage.getDepth() %>"/>

<c:choose>
    <c:when test="${not (isEditMode || isReadOnlyMode || properties.fixed)}">     
        <div class="topNav topNavTransition">
    </c:when>
    <c:otherwise>
        <div class="topNav" style="position:inherit !important;">
    </c:otherwise>
</c:choose>
	<header>
	    <swx:setWCMMode mode="READ_ONLY">
	        <cq:include script="logo.jsp"/>
	    </swx:setWCMMode>
	    <div class="link-group <c:if test="${properties.hideHamburger}">link-group-rt</c:if>">
	        <div class="left">
	            <swx:setWCMMode mode="READ_ONLY">
	                <div class="show-for-medium-up">
	                    <cq:include script="mainLinkSet.jsp"/>
	                </div>
	            </swx:setWCMMode>
	            
	            <%-- Phone links display --%>            
	            <swx:setWCMMode mode="READ_ONLY">
	                <div class="menu show-for-small-only">
	                    <cq:include script="phoneLinkSet.jsp"/>
	                </div>
	            </swx:setWCMMode>
				
				<%-- Secondary links display --%>
				<c:if test="${displayHomepageLinks && pageDepth eq 3}">
		            <swx:setWCMMode mode="READ_ONLY">
		                <cq:include script="secondaryLinkSet.jsp"/>
		            </swx:setWCMMode>
	            </c:if>
	            <div class="clearBoth"></div>
	        </div>
	
	        <%-- Side nav --%>
	        <c:if test="${not properties.hideHamburger}">
		        <div id="menu">
		            <div class="menu hide-for-medium-up"></div>
		            <div class="menu show-for-medium-up">
		                <fmt:message key="${sideNavLabel}"/>
		            </div>
		        </div>
		  	</c:if>
	        <div class="clearBoth"></div>
	    </div>
	    <div class="show-for-small-only sec-name"></div>
	    <div class="clearBoth"></div>
	</header>
</div>