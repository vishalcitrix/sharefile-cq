<%--
  Partner Form Heading
  This forms extends the dialog from Support Form 
--%>

<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil" %>

<div class="custom-form partnerForm">
     <div id="formErrorMsg" class="error-message">
        <span class="icon-Warning"></span>
        <fmt:message key="sf.form.message.error"/>
        <div class="notch"></div>
    </div>
    <form class="pForm " id=support"">
        <input type="hidden" name="fromEmail" id="fromEmail" value="${properties.fromEmail}" />
        <input type="hidden" name="toEmail" id="toEmail" value="${properties.toEmail}" />
        <input type="hidden" name="emailSubject" id="emailSubject" value="${properties.emailSubject}" />
        <input type="hidden" name="confirmationMessage" id="confirmationMessage" value="${properties.confirmationMessage}" />
        <input type="hidden" name="errorMessage" id="errorMessage" value="${properties.errorMessage}" />
        <input type="hidden" name="successUrl" id="successUrl" value="<%= ContextRootTransformUtil.transformedPath(properties.get("successUrl",""),request) %>" />        
        <input type="hidden" name="maxAttemptMesssage" id="maxAttemptMesssage" value="${properties.maxAttemptMesssage}" />
            
        <input type="text" class="required" id="firstname" name="First Name" constraint="None" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.firstName"/>" />
        <input type="text" class="required" id="lastname" name="Last Name" constraint="None" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.lastName"/>" >
        <input type="text" class="required" id="email"  name="Email" constraint="email" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.email"/>" >
        <input type="text" class="required" id="company" name="Company" constraint="Company" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.company"/>">
        <input type="text"  id="phone" name="Phone" constraint="Phone Number" trackerror="true" placeholder="<fmt:message key="sf.form.placeholder.phone"/>">
        <input id="submit" name="submit" type="submit" class="submitButton" value="<fmt:message key="sf.form.placeholder.partnerFormButton"/>"/>        
    </form>
</div>
<div class="clearBoth"></div>