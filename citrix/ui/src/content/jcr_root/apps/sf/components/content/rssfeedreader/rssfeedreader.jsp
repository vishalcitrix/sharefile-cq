<%--

    RSS feed components that display rss feed for given feed url. 
    vishal.gupta@citrix.com
--%>

<%@include file="/apps/citrixosd/global.jsp" %>
<%@ page import="com.citrixosd.models.RssFeedChannel,com.citrixosd.models.RssFeedItemCollection,com.citrixosd.models.RssFeedItem,com.citrixosd.service.rssfeed.RssFeedReader" %>

<%
    final String rss_url = properties.get("url", null);
    RssFeedItemCollection items = null;
%>

<%    
    if(rss_url != null) {
        try {
            RssFeedReader rssFeedReader=  sling.getService(com.citrixosd.service.rssfeed.RssFeedReader.class);
            RssFeedChannel ch = rssFeedReader.getChannel(rss_url);
            items = ch.getItems();
        }catch(Exception ex){
        %>
           <div class="warning">Error while getting Feeds.</div>
        <% 
       } 
    }
%>
<c:set var="items" value="<%= items %>"/>
<c:set var="noOfFeeds" value="<%= properties.get("noOfFeeds",3) %>"/>
<c:set var="rss_url" value="<%= rss_url %>"/>
<c:set var="read_more_url" value="<%= properties.get("readMoreUrl","") %>"/>

<c:if test="${not empty items}">
    <c:forEach items="${items}" var="item" begin="0" end="${noOfFeeds-1}">
		<div class="item">
			<c:choose>
			   <c:when test="${not empty item.pubDateObg}">
			      <div class="feedDate"><fmt:formatDate type="date" value="${item.pubDateObg}" /></div>
			   </c:when>
			   <c:otherwise>
			      <div class="feedDate">${item.pubDate}</div>
			   </c:otherwise>
			</c:choose>  
			<div class="feedTitle"><a href="${item.link}" target="_blank">${item.title}</a></div>
			<div class="citrix-rg-14px-green3-related green3"><a href="${item.link}" target="_blank"><fmt:message key="sf.read.full.article"/></a></div>
		</div>
    </c:forEach>
    <c:if test="${not empty read_more_url}">
    	<div class="read"><a href="${read_more_url}" target="_blank"><fmt:message key="sf.read.more"/></div>
    </c:if>
</c:if>

<%--Display placeholder when url is not configured in edit mode  --%>
<c:if test="${(isEditMode || isReadOnlyMode) && (empty properties.url)}">
    <div class="warning">Please enter Feed URL</div>
</c:if>     