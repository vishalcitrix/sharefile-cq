<%--
  Section Heading - Option to add Section heading and sub heading
--%>

<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@include file="/apps/citrixosd/global.jsp"%>
                
<c:set var="headingValue" value="${not empty properties.heading ? properties.heading : 'Heading goes here'}"/>
<c:set var="subheadingValue" value="${not empty properties.subheading ? properties.subheading : 'Sub Heading goes here'}"/>
<c:set var="hasDarkBg" value="${properties.hasDarkBg}"/>
<c:set var="useH1" value="${properties.useH1}"/>

<c:if test="${(isEditMode or isReadOnlyMode) or (not empty headingValue and not empty subheadingValue)}">
	<div <c:if test="${hasDarkBg}">class='hasDarkBg'</c:if>>
		<p>${headingValue}</p>
		<c:choose>
			<c:when test="${useH1}">
				<cq:text value="${subheadingValue}" tagName="h1" escapeXml="false"/>
			</c:when>
			<c:otherwise>
				<cq:text value="${subheadingValue}" tagName="h2" escapeXml="false"/>	
			</c:otherwise>
		</c:choose>
	</div>
</c:if>