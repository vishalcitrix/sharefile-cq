<%--
    Alignment Container
    - Options to add padding/margin/alignment for desktop, tablet and mobile. 
      There are various options to align a element.
    
    vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%@page import="com.citrixosd.pageInlineCSSListener.PageInlineCSSUtill"%>

<c:set var="margPad" value="${not empty properties.usePadding ? 'padding' : 'margin'}"/>
<c:if test='${not empty properties.marginDesktop}'>
  <c:set var="marginDesktop" value="${margPad}:${properties.marginDesktop};" />
</c:if>
<c:set var="alignmentDesktop" value="${not empty properties.alignmentDesktop ? properties.alignmentDesktop : ''}" />
<c:set var="data_large_wrap" value="${marginDesktop }${alignmentDesktop}" />

<c:if test='${not empty properties.marginTablet}'>
    <c:set var="marginTablet" value="${margPad}:${properties.marginTablet};" />
</c:if>
<c:set var="alignmentTablet" value="${not empty properties.alignmentTablet ? properties.alignmentTablet : ''}" />
<c:set var="data_medium_wrap" value="${marginTablet }${alignmentTablet}" />

<c:if test='${not empty properties.marginSmall}'>
    <c:set var="marginSmall" value="${margPad}:${properties.marginSmall};" />
</c:if>
<c:set var="alignmentSmall" value="${not empty properties.alignmentSmall ? properties.alignmentSmall : ''}" />
<c:set var="data_small_wrap" value="${marginSmall }${alignmentSmall}" />

<c:set var="leftCenterDesktop" value="${not empty properties.leftCenterDesktop ? 'display:inline-block; text-align:left;' : ''}" />
<c:if test='${not empty properties.widthDesktop}'>
    <c:set var="widthDesktop" value="width:${properties.widthDesktop}px; ${properties.alignmentDesktop eq 'text-align:center;' ? 'margin: 0 auto;' : ''}" />
</c:if>
<c:set var="data_large" value="${leftCenterDesktop }${widthDesktop}" />

<c:set var="leftCenterTablet" value="${not empty properties.leftCenterTablet ? 'display:inline-block; text-align:left;' : ''}" />
<c:if test='${not empty properties.widthTablet}'>
    <c:set var="widthTablet" value="width:${properties.widthTablet}px; ${properties.alignmentTablet eq 'text-align:center;' ? 'margin: 0 auto;' : ''}" />
</c:if>
<c:set var="data_medium" value="${leftCenterTablet }${widthTablet}" />

<c:set var="leftCenterPhone" value="${not empty properties.leftCenterPhone ? 'display:inline-block; text-align:left;' : ''}" />
<c:if test='${not empty properties.widthSmall}'>
    <c:set var="widthSmall" value="width:${properties.widthSmall}px; ${properties.alignmentSmall eq 'text-align:center;' ? 'margin: 0 auto;' : ''}" />
</c:if>
<c:set var="data_small" value="${leftCenterPhone }${widthSmall}" />
<%
	//String id=currentNode.getIdentifier();
    String id=PageInlineCSSUtill.getID(currentNode.getPath().toString());
    boolean isDynamicCSSEnabled = PageInlineCSSUtill.isDynamicCSSEnabled(currentPage,resourceResolver);
    if(isDynamicCSSEnabled)
    PageInlineCSSUtill.addInlineCSSProp(currentNode, currentPage, resourceResolver, pageContext);
%>
<c:set var="isDynamicCSSEnabled" value="<%=isDynamicCSSEnabled%>"/>
<c:set var="idnode" value="<%=id%>"/>
<c:choose>
    <c:when test="${not isEditMode || (isEditMode && properties.enableInEdit)}">
        <c:choose>
			<c:when test="${isDynamicCSSEnabled}">
				<div  id="id-${idnode}-wrap" >
			</c:when>
			<c:otherwise>
				<div class="styleswitch" style="${data_large_wrap}" data-large="${data_large_wrap}" data-medium="${data_medium_wrap}" data-small="${data_small_wrap}">
			</c:otherwise>
		</c:choose>
    </c:when>
    <c:otherwise>
        <div>
    </c:otherwise>
</c:choose>
    <c:choose>
        <c:when test="${not isEditMode || (isEditMode && properties.enableInEdit)}">     
            <c:choose>
				<c:when test="${isDynamicCSSEnabled}">
					 <div id="id-${idnode}">
				</c:when>
				<c:otherwise>
					<div class="styleswitch" style="${data_large}" data-large="${data_large}" data-medium="${data_medium}" data-small="${data_small}">
				</c:otherwise>
			</c:choose>
        </c:when>
        <c:otherwise>
            <div>
        </c:otherwise>
    </c:choose>
        <c:choose>
            <c:when test="${properties.disableContent == true}">
                <div>&nbsp;</div>
            </c:when>
            <c:otherwise>
                <cq:include path="margin_container_content_parsys" resourceType="foundation/components/parsys"/>
            </c:otherwise>
        </c:choose> 
    </div>
</div>
<cq:include path="endofsection" resourceType="citrixosd/components/content/endComponent"/>