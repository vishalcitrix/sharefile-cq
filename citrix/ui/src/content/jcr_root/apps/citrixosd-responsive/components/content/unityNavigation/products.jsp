<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="java.lang.StringBuilder"%>
<%@page import="javax.jcr.Session" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.Locale,java.util.ResourceBundle"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%
	final Page localePage = currentPage.getAbsoluteParent(2);
	final Locale locale = new Locale(localePage.getName().substring(0,2), localePage.getName().substring(3,5));
	final ResourceBundle resourceBundle = slingRequest.getResourceBundle(locale);   
%>

<%!
	public static final String BASE_CLOUD_NODE = "clouds";
	public static final String CHILD_CLOUD_NODE = "cloud";
	public static final String CHILD_CLOUD_PROPERTY = "cloudname";
	public static final String CHILD_TEXT_PROPERTY = "cloudtext";

	public String getCommsText(ResourceBundle resourceBundle) {
		StringBuilder commsText = new StringBuilder();
		commsText.append("<div>").append(resourceBundle.getString("unityNav.productCol1")).append("</div>");
		commsText.append("<ul>");
			commsText.append("<li>");
				commsText.append("<a href='" + resourceBundle.getString("unityNav.productG2M.link") + "'>");
					commsText.append("<span class='g2m'></span>");
					commsText.append("<div>").append(resourceBundle.getString("unityNav.productG2M"));
						commsText.append("<p>" + resourceBundle.getString("unityNav.productG2M.desc") + "</p>");
					commsText.append("</div>");
				commsText.append("</a>");
			commsText.append("</li>");
			commsText.append("<li>");
				commsText.append("<a href='" + resourceBundle.getString("unityNav.productG2W.link") + "'>");
					commsText.append("<span class='g2w'></span>");
					commsText.append("<div>").append(resourceBundle.getString("unityNav.productG2W"));
						commsText.append("<p>" + resourceBundle.getString("unityNav.productG2W.desc") + "</p>");
					commsText.append("</div>");
				commsText.append("</a>");
			commsText.append("</li>");
			commsText.append("<li>");
				commsText.append("<a href='" + resourceBundle.getString("unityNav.productG2T.link") + "'>");
					commsText.append("<span class='g2t'></span>");
					commsText.append("<div>").append(resourceBundle.getString("unityNav.productG2T"));
						commsText.append("<p>" + resourceBundle.getString("unityNav.productG2T.desc") + "</p>");
					commsText.append("</div>");
				commsText.append("</a>");
			commsText.append("</li>");
		commsText.append("</ul>");
		
		return commsText.toString();
	}
	
	public String getDocsText(ResourceBundle resourceBundle) {
		StringBuilder docsText = new StringBuilder();
		docsText.append("<div>" + resourceBundle.getString("unityNav.productCol2") + "</div>");
		docsText.append("<ul>");
			docsText.append("<li>");
				docsText.append("<a href='" + resourceBundle.getString("unityNav.productSF.link") + "'>");
					docsText.append("<span class='sf'></span>");
					docsText.append("<div>").append(resourceBundle.getString("unityNav.productSF"));
						docsText.append("<p>" + resourceBundle.getString("unityNav.productSF.desc") + "</p>");
					docsText.append("</div>");
				docsText.append("</a>");
			docsText.append("</li>");
			docsText.append("<li>");
				docsText.append("<a href='" + resourceBundle.getString("unityNav.productSC.link") + "'>");
					docsText.append("<span class='sc'></span>");
					docsText.append("<div>").append(resourceBundle.getString("unityNav.productSC"));
						docsText.append("<p>" + resourceBundle.getString("unityNav.productSC.desc") + "</p>");
					docsText.append("</div>");
				docsText.append("</a>");
			docsText.append("</li>");
			docsText.append("<li>");
				docsText.append("<a href='" + resourceBundle.getString("unityNav.productG2P.link") + "'>");
					docsText.append("<span class='g2p'></span>");
					docsText.append("<div>").append(resourceBundle.getString("unityNav.productG2P"));
						docsText.append("<p>" + resourceBundle.getString("unityNav.productG2P.desc") + "</p>");
					docsText.append("</div>");
				docsText.append("</a>");
			docsText.append("</li>");
			docsText.append("<li>");
				docsText.append("<a href='" + resourceBundle.getString("unityNav.productRS.link") + "'>");
					docsText.append("<span class='rs'></span>");
					docsText.append("<div>").append(resourceBundle.getString("unityNav.productRS"));
						docsText.append("<p>" + resourceBundle.getString("unityNav.productRS.desc") + "</p>");
					docsText.append("</div>");
				docsText.append("</a>");
			docsText.append("</li>");
		docsText.append("</ul>");
		
		return docsText.toString();
	}
	
	public String getWorkflowText(ResourceBundle resourceBundle) {
		StringBuilder workflowText = new StringBuilder();
		workflowText.append("<div>" + resourceBundle.getString("unityNav.productCol3") + "</div>");
		workflowText.append("<ul>");
			workflowText.append("<li>");
				workflowText.append("<a href='" + resourceBundle.getString("unityNav.productG2A.link") + "'>");
					workflowText.append("<span class='g2a'></span>");
					workflowText.append("<div>").append(resourceBundle.getString("unityNav.productG2A"));
						workflowText.append("<p>" + resourceBundle.getString("unityNav.productG2A.desc") + "</p>");
					workflowText.append("</div>");
				workflowText.append("</a>");
			workflowText.append("</li>");
			workflowText.append("<li>");
				workflowText.append("<a href='" + resourceBundle.getString("unityNav.productCO.link") + "'>");
					workflowText.append("<span class='co'></span>");
					workflowText.append("<div>").append(resourceBundle.getString("unityNav.productCO"));
						workflowText.append("<p>" + resourceBundle.getString("unityNav.productCO.desc") + "</p>");
					workflowText.append("</div>");
				workflowText.append("</a>");
			workflowText.append("</li>");
			workflowText.append("<li>");
				workflowText.append("<a href='" + resourceBundle.getString("unityNav.productSI.link") + "'>");
					workflowText.append("<span class='si'></span>");
					workflowText.append("<div>").append(resourceBundle.getString("unityNav.productSI"));
						workflowText.append("<p>" + resourceBundle.getString("unityNav.productSI.desc") + "</p>");
					workflowText.append("</div>");
				workflowText.append("</a>");
			workflowText.append("</li>");
		workflowText.append("</ul>");
		
		return workflowText.toString();
	}
	
	//function to create nodes for each cloud
	public void createCloudNode(Node parentNode,ResourceBundle resourceBundle) throws RepositoryException {
		if(!parentNode.hasNode(BASE_CLOUD_NODE)) {
			parentNode.addNode(BASE_CLOUD_NODE);
			parentNode.save();
			
			//get text from appropriate cloud text functions
			final String comms = getCommsText(resourceBundle);
			final String docs = getDocsText(resourceBundle);
			final String workflow = getWorkflowText(resourceBundle);
			
			Node baseCloudNode = parentNode.getNode(BASE_CLOUD_NODE);
			
			//add comms node
			Node newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "1");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Comms");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, comms);
			newCloudNode.save();
			
			//add docs node
			newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "2");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Docs");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, docs);
			newCloudNode.save();
			
			//add workflow node
			newCloudNode = baseCloudNode.addNode(CHILD_CLOUD_NODE + "3");
			baseCloudNode.save();
			newCloudNode.setProperty(CHILD_CLOUD_PROPERTY, "Workflow");
			newCloudNode.setProperty(CHILD_TEXT_PROPERTY, workflow);
			newCloudNode.save();
		}
	}
%>
<% 
	if(WCMMode.fromRequest(request).equals(WCMMode.EDIT) || WCMMode.fromRequest(request).equals(WCMMode.READ_ONLY)) {
		//Create component node if it does not exist
	    if(currentNode == null) {
	    	final Session session = resource.getResourceResolver().adaptTo(Session.class);
	    	currentNode = JcrUtils.getOrCreateByPath(resource.getPath(), null, session);
	    	session.save();
	    }
		
	    createCloudNode(currentNode,resourceBundle);
	}

	//get text from cloud nodes
	ArrayList<Map<String, Property>> values = new ArrayList<Map<String, Property>>();
	if(currentNode != null && currentNode.hasNode("clouds")) {
	    final Node baseNode = currentNode.getNode("clouds");
	    values = Utilities.parseStructuredMultifield(baseNode);
	}
%>

<c:set var="values" value="<%= values %>"/>

<div class="products">
    <span class="close"></span>
    <p><fmt:message key="unityNav.menu.drawerText"/></p>
    <div class="row">
    	<c:choose>
	        <c:when test="${fn:length(values) > 0}">
	            <c:forEach items="${values}" var="item">
	            	 <div class="columns large-4 medium-4">
	            	 	${item.cloudtext.string}
	            	 </div>
	            </c:forEach>
	        </c:when>
	        <c:otherwise>
	            <c:if test="${isEditMode || isReadOnlyMode}"><div class="warning">Please check for cloud nodes under global header.</div></c:if>
	        </c:otherwise>
        </c:choose>
    </div>
</div>