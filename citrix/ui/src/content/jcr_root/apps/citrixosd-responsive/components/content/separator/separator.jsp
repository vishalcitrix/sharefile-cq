<%--
    Separator Component - Creates an <div> tag with border bottom, adjustable width, separator type and alignment options.

    vishal.gupta@citrix.com
    kaverappa.subramanya@citrix.com - Customized for ShareFile.
    SCSS required: /citrixosd-sass/global-sass/components/global/_separator.scss
    Keeping css class name as arrowImage for compatibility with sharefile and gotoassist
--%>

<%@include file="/libs/foundation/global.jsp" %>

<%
	final String sectionId = "id-"+currentNode.getIdentifier();
	pageContext.setAttribute("nodeId",sectionId);
%>

<c:if test="${properties.seperatorType eq 'arrow'}">
	<div class="arrowImage ${alignmentCss}" style="<c:if test="${not empty properties.width}">width:${properties.width};</c:if>"></div>
</c:if>

<c:if test="${properties.seperatorType eq 'arrowNotch' || properties.seperatorType eq 'arrowNotchWithSolidFill'}">
	<div id="${nodeId}">
		<hr class="${properties.seperatorType} ${properties.alignment}" style="<c:if test="${not empty properties.width}">width:${properties.width};</c:if>border-color:${properties.userColor};"></hr>
	</div>
</c:if>

<c:if test="${not empty properties.userColor && properties.seperatorType ne 'arrow' && properties.seperatorType ne 'arrowNotch' && properties.seperatorType ne 'arrowNotchWithSolidFill'}">
	<hr class="${properties.seperatorType} ${properties.alignment}" style="<c:if test="${not empty properties.width}">width:${properties.width};</c:if>border-color:${properties.userColor};">
</c:if>

<c:if test="${fn:contains(alignmentCss,'left') || fn:contains(alignmentCss,'right')}">
	<div class="clearBoth"></div>
</c:if>

<style type="text/css">
<%--
	Arrow Notch color CSS
--%>
<c:if test="${properties.seperatorType eq 'arrowNotchWithSolidFill'}">
	#${nodeId} .arrowNotchWithSolidFill:after{
		border-top-color: ${properties.userColor};
	}
</c:if>
<c:if test="${properties.seperatorType eq 'arrowNotch'}">
	#${nodeId} .arrowNotch:before{
		border-top-color: ${properties.userColor};
	}
</c:if>
</style>