<%@page import="com.citrixosd.utils.ContextRootTransformUtil,com.citrixosdRedesign.constants.ResourceConstants"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<c:set var="mimeTypePDF" value="<%= ResourceConstants.Document.PDF_MIMETYPE %>"/>
<c:set var="mimeTypePPT" value="<%= ResourceConstants.Document.PPT_MIMETYPE %>"/>
<c:set var="designPath" value="<%= ContextRootTransformUtil.transformedPath(currentDesign.getPath(),request) %>"/>
<c:set var="baseURL" value="<%= request.getRequestURL().toString().replace(request.getRequestURI().substring(0), request.getContextPath()) %>"/>

<section class="resourcesFeature-container">
    <ul id="resourcesFeatureList" class="resourcesFeature-list">
        <c:choose>
            <c:when test="${fn:length(resourceList.list) > 0}">
                <c:forEach items="${resourceList.list}" var="resourceItem">
                	<c:set var="detailPagePath" value="${((resourceItem.resourceType eq 'Awards') || (resourceItem.resourceType eq 'Webinar' && resourceItem.isGated) || (resourceItem.isGated)) && (not isEditMode) ? (resourceItem.resourceType eq 'Webinar' ? (resourceItem.registerPath ne null ? resourceItem.registerPath : '#') : (not empty resourceItem.externalLink ? resourceItem.externalLink : '#')) : resourceItem.path}"/>
                    <li>
                    	<a href="${detailPagePath}">
	                        <div class="resourcesFeature">
	                            <aside class="featured-image">
                                    <div style="background-image:url('${resourceItem.hasImage ? resourceItem.smallImagePath : resourceItem.hasVideo && not empty resourceItem.thumbnailPath ? resourceItem.thumbnailPath : resourceItem.smallImagePath}'); background-position: center center; background-size: cover; height: 200px;"></div>
                                    <c:if test="${resourceItem.hasVideo}">
                                        <span class="notch"></span>
                                        <span class="icon-Play"></span>
                                    </c:if>
	                                <c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePDF && !resourceItem.hasVideo}">
	                                	<span class="notch"></span>
	                                    <span class="icon-PDF-a"></span>
	                                    <span class="icon-PDF-b"></span>
	                                    <span class="icon-PDF-c"></span>
	                                </c:if>
	                                <c:if test="${resourceItem.resourceType ne 'Webinar' && resourceItem.damAssetType eq mimeTypePPT && !resourceItem.hasVideo}">
	                                	<span class="notch"></span>
	                                    <span class="icon-PDF-a ppt"></span>
	                                    <span class="icon-PDF-b"></span>
	                                    <span class="icon-PPT-c"></span>
	                                </c:if>
	                            </aside>
	                            <section class="content-description">
	                                <div class="content-wrapper">
	                                    <h2>
	                                        ${resourceItem.resourceType}
	                                        <c:if test="${not empty resourceItem.date}">
	                                            <span class="resource-date"><fmt:formatDate value="${resourceItem.date}" type="DATE" pattern="dd MMMM, yyyy"/></span>
	                                        </c:if>
	                                    </h2>
	                                    <h3>${resourceItem.title}</h3>
	                                    <p class="resources-feature-description">${resourceItem.description}</p>
	                                </div>
	                                <div class="read-article">
					                	<fmt:message key="sf.read.full.article"/>
					                </div>
	                            </section>
	                        </div>
                        </a>
                    </li>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <li>
                    <div class="resource-list-no-results">
                        <span><fmt:message key="resource.list.no.results"/></span>
                    </div>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>

    <c:if test="${resourceList.totalPages > 1}">
        <div class="pagination">
        	<c:choose>
        		<c:when test="${resourceList.currentPage > 0}">
        			<a class="icon-LeftArrow" href="${resource.path}.pagination.${(resourceList.currentPage - 1) > 0 ? resourceList.currentPage - 1 : 0}${resourceList.suffixPathWithQuery}"></a>
        		</c:when>
        		<c:otherwise>
        			<span class="icon-LeftArrow"></span>
        		</c:otherwise>
        	</c:choose>
            <div class="pagination-wrapper">
                <ul>
                    <li class="${resourceList.currentPage == 0 ? 'active' : ''}"><a href="${resource.path}.pagination.0${resourceList.suffixPathWithQuery}"></a></li>
                    <c:choose>
                        <c:when test="${resourceList.totalPages > 5}">
                            <c:if test="${resourceList.currentPage > 2}">
                                <li class="dots">
                                	<span>.</span>
	                           		<span>.</span>
	                           		<span>.</span>
	                           	</li>
                            </c:if>
                            <c:forEach begin="${(resourceList.currentPage - 1) > 1 ? ((resourceList.currentPage + 4) > resourceList.totalPages ? resourceList.totalPages - 4 : resourceList.currentPage - 1) : 1}" end="${(resourceList.currentPage) > 2 ? (resourceList.currentPage + 2 >= resourceList.totalPages ? resourceList.totalPages - 2 : resourceList.currentPage + 1) : resourceList.currentPage + (3 - resourceList.currentPage)}" varStatus="i">
                                <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}"></a></li>
                            </c:forEach>
                            <c:if test="${(resourceList.totalPages - 3) > resourceList.currentPage}">
                                <li class="dots">
	                           		<span>.</span>
	                           		<span>.</span>
	                           		<span>.</span>
                                </li>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:forEach begin="1" end="${resourceList.totalPages - 2}" varStatus="i">
                                <li class="${resourceList.currentPage == i.index ? 'active' : ''}"><a href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}"></a></li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <li class="last-three ${resourceList.currentPage == (resourceList.totalPages - 1) ? 'active' : ''}"><a href="${resource.path}.pagination.${resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}"></a></li>
                </ul>
            </div>
            <c:choose>
        		<c:when test="${(resourceList.currentPage + 1) < resourceList.totalPages}">
        			<a class="icon-RightArrow2" href="${resource.path}.pagination.${(resourceList.currentPage + 1) < resourceList.totalPages ? resourceList.currentPage + 1 : resourceList.totalPages - 1}${resourceList.suffixPathWithQuery}"></a>
        		</c:when>
        		<c:otherwise>
        			<span class="icon-RightArrow2"></span>
        		</c:otherwise>
        	</c:choose>
        </div>
        <c:forEach begin="1" end="${resourceList.totalPages - 1}" varStatus="i">
            <a class="load-more mobile ${i.index == 1 ? '': 'hide'}" href="${resource.path}.pagination.${i.index}${resourceList.suffixPathWithQuery}" data-pagination-index="${i.index}">
                <fmt:message key="Load more ..."/>
            </a>
        </c:forEach>
    </c:if>
</section>

<script>
    $(document).ready(function() {
        var resourceListContainer = $("#" + "${resourceList.id}");
        var resourceListPagination = resourceListContainer.find(".pagination");
        resourceListPagination.find("a").each(function() {
            var paginationAnchor = $(this);
            paginationAnchor.unbind("click").click(function() {
            	searchRefinements.removeFeatured();
                resourceListPagination.find('li').removeClass('active');
                resourceListContainer.parent().load(paginationAnchor.attr("href"), function(){
                    dotDotDot.init();
                });
                $("html, body").animate({ scrollTop: 150});
                return false;
            });
        });
        resourceListContainer.find("a.load-more").each(function() {
            var loadMoreAnchor = $(this)
            loadMoreAnchor.unbind("click").click(function() {
            	searchRefinements.removeFeatured();
                $("a.load-more").html("<img id='resourcesFeatureAnimation' src='${designPath}/css/static/images/progress.gif' />");
                $.get(loadMoreAnchor.attr("href"), function(data){
                    loadMoreAnchor.addClass("hide");
                    loadMoreAnchor.next("a.load-more").removeClass("hide");
                    $("a.load-more").html('<fmt:message key="load.more"/>');
                    $(data).find("#resourcesFeatureList li").appendTo("#resourcesFeatureList");
                    dotDotDot.init();
                });
                return false;
            });
        });
    });
</script>