<%@ page import="com.citrixosd.service.rssfeed.RssFeedReader,
				org.jsoup.Jsoup,
				org.jsoup.nodes.Document,
				org.jsoup.select.Elements,
				com.citrixosd.models.RssFeedItem,
				com.citrixosd.models.RssFeedChannel,
				com.citrixosd.models.RssFeedItemCollection,
				com.citrixosd.utils.ContextRootTransformUtil" %>
<%@include file="/apps/citrixosd/global.jsp" %>

<%! 
	public String removeImage(String dec){
        StringBuffer sb = new StringBuffer(dec);
        String str = "";
        int start = sb.indexOf("<img");
        int end = sb.indexOf("/>", start);
        int end2 = sb.indexOf("</img>", start);
        int end3 = sb.indexOf(">", start);

        if (start >= 0 && end >= 0) {
            str = sb.replace(start, end + 2, "").toString();
        } else if (start >= 0 && end2 >= 0) {
            str = sb.replace(start, end2 + 6, "").toString();
        } else if (start >= 0 && end3 >= 0) {
            str = sb.replace(start, end3 + 1, "").toString();
        } else {
            str = dec;
        }
        return str; 
    } 
%>
<%
	final String blogDefaultImage = ContextRootTransformUtil.transformedPath(currentDesign.getPath() + "/css/static/images/blog_default_image_square.png",request);
    final String path = properties.get("path", null);
    final RssFeedReader rssFeedReader=  sling.getService(RssFeedReader.class);

    RssFeedChannel channel = null;
    if(path != null) {
        try {
            channel = rssFeedReader.getChannel(path);
            if(channel != null) {
                final RssFeedItemCollection items = channel.getItems();
                for(int i = 0; i < items.size(); i++) {
                    final RssFeedItem item = items.get(i);
                    final Document description = Jsoup.parse(item.getContent(), "UTF-8");
                    final Elements images = description.select("img[src]");
                    if(images != null && images.size() > 0) {
                        item.setImage(images.get(0).absUrl("src"));
                      	//remove image from desreption 
                        item.setDescription(removeImage(item.getDescription()));
                    }
                }
            }

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
%>

<c:set var="channel" value="<%= channel %>"/>
<c:set var="numberOfFeeds" value="<%= properties.get("noOfFeeds",4) %>"/>
<c:set var="blogDefaultImage" value="<%= blogDefaultImage %>"/>

<c:choose>
    <c:when test="${not empty channel}">
        <div<c:if test="${not empty properties.colorScheme}"> class="${properties.colorScheme}"</c:if>>
            <div class="feed-header">${properties.title}</div>
            <div class="feeds">
	            <c:forEach items="${channel.items}" end="${numberOfFeeds - 1}" var="item">
		            <div class="feed-container">
		                <a rel="external" href="${item.link}" class="feed-link">
		                	<c:if test="${not empty properties.showFeedImage}"> 
		                        <div class="image-feed">
	                                <img src="${not empty item.image ? item.image : blogDefaultImage}" />
	                            </div>
	                        </c:if>
	                        <div class="item">
	                            <h3>${item.title}</h3>
	                        </div>
		                </a>
		                <a rel="external" href="${item.link}" class="read-article">
		                	<fmt:message key="read.full.article"/>
		                </a>
		            </div>
	            </c:forEach>
	       	</div>
            <div class="sub-container">
                <a rel="external" href="${channel.link}"><fmt:message key="RSS Feed"/></a>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <c:if test="${(isEditMode || isReadOnlyMode)}">
            <div>
                <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">
            </div>
        </c:if>
    </c:otherwise>
</c:choose>