<%--
    Main Nav Links
    - vishal.gupta@citrix.com 
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.citrixosd.utils.Utilities"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<% 
    ArrayList<Map<String, Property>> links = new ArrayList<Map<String, Property>>();
    if(currentNode != null && currentNode.hasNode("mainNavlinks")) {
        final Node baseNode = currentNode.getNode("mainNavlinks");
        links = Utilities.parseStructuredMultifield(baseNode);
    }
%>

<c:set var="linkList" value="<%= links %>"/>

<c:choose>
    <c:when test="${fn:length(linkList) > 0}">
        <ul>
            <c:forEach var="link" items="${linkList}"> 
                <c:choose>
                    <c:when test="${not empty link.css && link.css.string ne 'none'}">
                        <li id="${link.css.string}">  
                    </c:when>
                    <c:otherwise>
                        <li class="single">  
                    </c:otherwise>
                </c:choose>  	
					<a href="${link.path.string}" rel="${link.linkOption.string}" <c:if test="${empty link.css}">class="single"</c:if>>
						<c:if test="${not empty link.css && link.css.string ne 'signIn'}">
							<span class="${link.css.string}"></span>
						</c:if>
						<span>${link.text.string}</span>
					</a>
				</li>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <c:if test="${isEditMode}">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt=""/>
        </c:if>
    </c:otherwise>
</c:choose>