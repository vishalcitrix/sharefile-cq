<%--
  Greetings Name component.
  - This component will display name from the client's Session Storage with a greeting.
  
  vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>

<%
    String id = currentNode.getIdentifier();
    pageContext.setAttribute("nodeId", id);
%>

<div id="${nodeId}" class = "${not empty properties.class ? properties.class : ''}" >
    <c:if test="${not empty properties.defaulttext}">
        ${properties.defaulttext}
    </c:if>
</div>

<c:if test="${not empty properties.greeting}">
	<script type="text/javascript">
	  var greeting = '<p>${properties.greeting}</p>';
	  var fieldKey = '${properties.fieldKey}';
	  var sessionKey = sessionStorage.getItem(fieldKey);
	  if(sessionKey != null) {
	      nametxt = "<span class='greeting'>" + sessionKey + "</span>";
	      $("#${nodeId}").html(greeting+nametxt);  
	  }
	</script>
</c:if>
