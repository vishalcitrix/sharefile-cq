<%--
    Image Renditions
 
    This component is an alternate image component but removing all the existing functionality and
    encouraging the use of image renditions.
   
    Dev: This component is like the imageRenditions component, but option to add retina images as
    well as option to lazy load images.
 
    vishal.gupta@citrix.com
--%>
<%@page import="com.citrixosd.utils.ContextRootTransformUtil"%>
<%@page import="com.day.cq.wcm.foundation.Image"%>
<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>
<%
    String rendition = properties.get("rendition","");
    String fileReference = properties.get("fileReference",""); 
    String retinaRendition = properties.get("retinaRendition","");
    String retinaFileReference = properties.get("retinaFileReference","");
   
    if(rendition != "") {
    	rendition = ContextRootTransformUtil.transformedPath(rendition,request);
    }
   
    if(fileReference != "") {
    	fileReference = ContextRootTransformUtil.transformedPath(fileReference,request);
    }
   
    if(retinaRendition != "") {
    	retinaRendition = ContextRootTransformUtil.transformedPath(retinaRendition,request);
    }
   
    if(retinaFileReference != "") {
    	retinaFileReference = ContextRootTransformUtil.transformedPath(retinaFileReference,request);
    }
%>

<c:set var="rendition" value="<%= rendition %>"/>
<c:set var="fileReference" value="<%= fileReference %>"/>
<c:set var="retinaRendition" value="<%= retinaRendition %>"/>
<c:set var="retinaFileReference" value="<%= retinaFileReference %>"/>
<c:set var="lazyLoad" value="<%= properties.get("lazyLoad",false) %>"/>

<c:choose>
    <c:when test="${not empty fileReference}">
        <c:choose>
            <c:when test="${lazyLoad eq true}">
                <c:choose>
                    <c:when test="${isEditMode || isReadOnlyMode}">
                        <img src="${not empty rendition ? rendition : fileReference}">
                    </c:when>
                    <c:otherwise>
                        <img class="lazy" title="${properties['jcr:title']}" alt="${not empty properties['alt'] ? properties['alt'] : properties[':title']}" src="<%= currentDesign.getPath() %>/css/static/images/1x1.gif" data-src="${not empty rendition ? rendition : fileReference}" <c:if test="${not empty retinaFileReference}">data-src-retina="${not empty retinaRendition ? retinaRendition : retinaFileReference}"</c:if>>
                        <!-- <noscript><img src="${not empty rendition ? rendition : fileReference}" /></noscript> -->
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <img title="${properties['jcr:title']}" alt="${not empty properties['alt'] ? properties['alt'] : properties[':title']}" src="${not empty rendition ? rendition : fileReference}">
            </c:otherwise>
        </c:choose>
    </c:when>
   <c:otherwise>
        <c:if test="${isEditMode || isReadOnlyMode}">
            <img class="cq-image-placeholder" src="/libs/cq/ui/resources/0.gif">
        </c:if>
        <%-- Do not draw image... not in edit mode & it's not set. --%>
    </c:otherwise>
</c:choose>