<%--
	Container

	Creates a container that includes section, background image, background color and alignment container
	SCSS required: /citrixosd-sass/global-sass/components/global/_container.scss
	JS required: None
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@page import="com.day.cq.wcm.api.WCMMode,
				com.day.cq.wcm.api.components.Toolbar,
				com.day.cq.personalization.ClientContextUtil,
				com.citrixosd.utils.ContextRootTransformUtil"%>
<%@include file="/libs/foundation/global.jsp"%>
<%!
    private static final String FILE_REFERENCE_PROPERTY = "fileReference";
    private static final String TABLET_FILE_REFERENCE_PROPERTY = "fileReferenceMedium";
    private static final String MOBILE_FILE_REFERENCE_PROPERTY = "fileReferenceSmall";
    private static final String RENDITION_PROPERTY = "rendition";
%>
<%
	final String sectionId = properties.get("id","");
	String currentMode = WCMMode.fromRequest(slingRequest).toString();

	if (editContext != null && editContext.getEditConfig() != null) {
		if(sectionId == "" && editContext.getEditConfig().getToolbar().size() > 0){
			editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Section Container - (Please select id)"));
		}else{
			if(editContext.getEditConfig().getToolbar().size() > 0)
				editContext.getEditConfig().getToolbar().add(2, new Toolbar.Label("Section Container - (id: " + sectionId + ")"));
		}
		if(editContext.getEditConfig().getToolbar().size() > 0)
			editContext.getEditConfig().getToolbar().add(3, new Toolbar.Separator());
	}
	final String containerId = "id-"+currentNode.getIdentifier();
	pageContext.setAttribute("nodeId",containerId);
	pageContext.setAttribute("pageMode", currentMode);
%>
<%--
	Section container ID
--%>
<c:set var="attrID" value="<%= sectionId %>"/>
<%--
	Get background image path is available
--%>
<c:set var="defaultsrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(FILE_REFERENCE_PROPERTY, "")),request) %>" />
<c:set var="mediumsrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(TABLET_FILE_REFERENCE_PROPERTY, "")),request) %>" />
<c:set var="smallsrc" value="<%= ContextRootTransformUtil.transformedPath(properties.get(RENDITION_PROPERTY, properties.get(MOBILE_FILE_REFERENCE_PROPERTY, "")),request) %>" />
<c:set var="styleDefault" value="" />
<c:set var="styleMedium" value="" />
<c:set var="styleSmall" value="" />
<c:set var="styleEditMode" value="" />
<c:set var="containerClass" value="" />
<c:set var="userGradientStart" value="50" />
<c:set var="userGradientStop" value="100" />
<%-- Classes to add to container --%>
<c:if test="${not empty properties.sectionSlant}">
	<c:set var="containerClass" value="${containerClass} slant" />
</c:if>
<c:if test="${empty properties.userColor and not empty properties.color}">
	<c:set var="containerClass" value="${containerClass} ${properties.color}" />
</c:if>
<c:if test="${empty properties.borderUserColor and not empty properties.borderColor}">
	<c:set var="containerClass" value="${containerClass} ${properties.borderColor}" />
</c:if>
<c:if test="${not empty properties.borderRadius}">
	<c:set var="containerClass" value="${containerClass} ${properties.borderRadius}" />
</c:if>
<c:if test="${not empty properties.alignmentLarge}">
	<c:set var="containerClass" value="${containerClass} ${properties.alignmentLarge}" />
	<c:if test="${properties.alignmentLarge eq 'align-center' and not empty properties.leftCenterLarge and properties.leftCenterLarge eq 'true'}">
		<c:set var="containerClass" value="${containerClass} text-left" />
	</c:if>
</c:if>
<c:if test="${not empty properties.alignmentMedium}">
	<c:set var="containerClass" value="${containerClass} ${properties.alignmentMedium}" />
	<c:if test="${properties.alignmentMedium eq 'align-center-medium' and not empty properties.leftCenterMedium and properties.leftCenterMedium eq 'true'}">
		<c:set var="containerClass" value="${containerClass} text-left-medium" />
	</c:if>
</c:if>
<c:if test="${not empty properties.alignmentSmall}">
	<c:set var="containerClass" value="${containerClass} ${properties.alignmentSmall}" />
	<c:if test="${properties.alignmentSmall eq 'align-center-small' and not empty properties.leftCenterSmall and properties.leftCenterSmall eq 'true'}">
		<c:set var="containerClass" value="${containerClass} text-left-small" />
	</c:if>
</c:if>
<c:if test="${properties.verticallyCentered eq 'true'}">
	<c:set var="containerClass" value="${containerClass} vertical-center-large" />
</c:if>
<c:if test="${properties.verticallyCenteredMedium eq 'true'}">
	<c:set var="containerClass" value="${containerClass} vertical-center-medium" />
</c:if>
<c:if test="${properties.verticallyCenteredSmall eq 'true'}">
	<c:set var="containerClass" value="${containerClass} vertical-center-small" />
</c:if>
<%-- Visibility --%>
<c:if test="${properties.hideInSmall eq 'true'}">
	<c:set var="containerClass" value="${containerClass} display-none-small-only" />
</c:if>
<c:if test="${properties.hideInMedium eq 'true'}">
	<c:set var="containerClass" value="${containerClass} display-none-medium-only" />
</c:if>
<c:if test="${properties.hideInLarge eq 'true'}">
	<c:set var="containerClass" value="${containerClass} display-none-large-up" />
</c:if>
<%-- Default background--%>
<c:if test="${not empty properties.userColor}">
	<c:set var="styleDefault" value="background: ${properties.userColor}" />
</c:if>
<c:choose>
	<c:when test="${not empty defaultsrc}">
		<c:set var="styleDefault" value="${fn:contains(styleDefault, 'background:') ? styleDefault : 'background:'} url('${defaultsrc}')" />
		<c:set var="styleDefault" value="${styleDefault} ${properties.imageRepeat}" />
		<c:if test="${not empty properties.imagePositionX and not empty properties.imagePositionY}">
			<c:set var="styleDefault" value="${styleDefault} ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'}" />
		</c:if>
		<c:set var="styleDefault" value="${styleDefault};" />
		<c:if test="${not empty properties.imageSize and properties.imageSize ne 'custom'}">
			<c:set var="styleDefault" value="${styleDefault} background-size: ${properties.imageSize};" />
		</c:if>
		<c:if test="${not empty properties.imageSize and properties.imageSize eq 'custom' and not empty properties.imageSizeCustom}">
			<c:set var="styleDefault" value="${styleDefault} background-size: ${properties.imageSizeCustom};" />
		</c:if>
	</c:when>
	<c:otherwise>
		<c:if test="${not empty properties.userColor}">
			<c:if test="${not empty properties.userSpecifiedGradientStart}">
				<c:set var="userGradientStart" value="${properties.userSpecifiedGradientStart}" />
			</c:if>
			<c:if test="${not empty properties.userSpecifiedGradientStop}">
				<c:set var="userGradientStop" value="${properties.userSpecifiedGradientStop}" />
			</c:if>
			<c:choose>
				<c:when test="${not empty properties.userColorGradient}">
					<c:set var="styleDefault" value="background-color: ${properties.userColor};" />
					<c:set var="styleDefault" value="${styleDefault} background: -o-linear-gradient(top, ${properties.userColor} ${userGradientStart}%, ${properties.userColorGradient} ${userGradientStop}%);" />
					<c:set var="styleDefault" value="${styleDefault} background: -webkit-gradient(linear, left top, left bottom, color-stop(${userGradientStart}%, ${properties.userColorGradient}));" />
					<c:set var="styleDefault" value="${styleDefault} background: -webkit-linear-gradient(top, ${properties.userColor} ${userGradientStart}%, ${properties.userColorGradient} ${userGradientStop}%);" />
					<c:set var="styleDefault" value="${styleDefault} background: -moz-linear-gradient(${userGradientStart}% ${userGradientStop}% 90deg, ${properties.userColorGradient}, ${properties.userColor});" />
					<c:set var="styleDefault" value="${styleDefault} background: -ms-linear-gradient(top, ${properties.userColor} ${userGradientStart}%, ${properties.userColorGradient} ${userGradientStop}%);" />
					<c:set var="styleDefault" value="${styleDefault} background: linear-gradient(to bottom, ${properties.userColor} ${userGradientStart}%, ${properties.userColorGradient} ${userGradientStop}%);" />
					<c:set var="styleDefault" value="${styleDefault} filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='${properties.userColor}', endColorstr='${properties.userColorGradient}', GradientType=0);" />
				</c:when>
				<c:otherwise>
					<c:set var="styleDefault" value="background-color: ${properties.userColor};" />
				</c:otherwise>
			</c:choose>
		</c:if>
	</c:otherwise>
</c:choose>
<%-- Default alignment options--%>
<c:if test="${not empty properties.widthLarge}">
	<c:set var="styleDefault" value="${styleDefault} width: ${properties.widthLarge}${fn:contains(properties.widthLarge, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.heightLarge}">
	<c:set var="styleDefault" value="${styleDefault} height: ${properties.heightLarge}${fn:contains(properties.heightLarge, '%') ? '' : 'px'};" />
	<c:set var="styleEditMode" value="${styleEditMode} height: auto; min-height: ${properties.heightLarge}${fn:contains(properties.heightLarge, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.marginLarge}">
	<c:set var="styleDefault" value="${styleDefault} margin: ${properties.marginLarge};" />
</c:if>
<c:if test="${not empty properties.paddingLarge}">
	<c:set var="styleDefault" value="${styleDefault} padding: ${properties.paddingLarge};" />
</c:if>
<%-- Default Border options--%>
<c:if test="${not empty properties.borderWidth}">
	<c:set var="styleDefault" value="${styleDefault} border: ${properties.borderWidth} solid;" />
</c:if>
<c:if test="${not empty properties.borderUserColor}">
	<c:set var="styleDefault" value="${styleDefault} border-color: ${properties.borderUserColor};" />
</c:if>
<%-- Medium background overwrite--%>
<c:if test="${not empty mediumsrc}">
	<c:set var="styleMedium" value="${styleMedium} background-image: url('${mediumsrc}');" />
</c:if>
<c:if test="${not empty properties.imageRepeatMedium}">
	<c:set var="styleMedium" value="${styleMedium} background-repeat: ${properties.imageRepeatMedium};" />
</c:if>
<c:if test="${not empty properties.imageSizeMedium and properties.imageSizeMedium ne 'custom'}">
	<c:set var="styleMedium" value="${styleMedium} background-size: ${properties.imageSizeMedium};" />
</c:if>
<c:if test="${not empty properties.imageSizeMedium and properties.imageSizeMedium eq 'custom' and not empty properties.imageSizeCustomMedium}">
	<c:set var="styleMedium" value="${styleMedium} background-size: ${properties.imageSizeCustomMedium};" />
</c:if>
<c:if test="${not empty properties.imagePositionXMedium and not empty properties.imagePositionYMedium}">
	<c:set var="styleMedium" value="${styleMedium} background-position: ${properties.imagePositionXMedium}${fn:contains(properties.imagePositionXMedium, '%') ? '' : 'px'} ${properties.imagePositionYMedium}${fn:contains(properties.imagePositionYMedium, '%') ? '' : 'px'};" />
</c:if>
<%-- Medium alignment overwrite--%>
<c:if test="${not empty properties.widthMedium}">
	<c:set var="styleMedium" value="${styleMedium} width: ${properties.widthMedium}${fn:contains(properties.widthMedium, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.heightMedium}">
	<c:set var="styleMedium" value="${styleMedium} height: ${properties.heightMedium}${fn:contains(properties.heightMedium, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.marginMedium}">
	<c:set var="styleMedium" value="${styleMedium} margin: ${properties.marginMedium};" />
</c:if>
<c:if test="${not empty properties.paddingMedium}">
	<c:set var="styleMedium" value="${styleMedium} padding: ${properties.paddingMedium};" />
</c:if>
<%-- Small background overwrites--%>
<c:if test="${not empty smallsrc}">
	<c:set var="styleSmall" value="${styleSmall} background-image: url('${smallsrc}');" />
</c:if>
<c:if test="${not empty properties.imageRepeatSmall}">
	<c:set var="styleSmall" value="${styleSmall} background-repeat: ${properties.imageRepeatSmall};" />
</c:if>
<c:if test="${not empty properties.imageSizeSmall and properties.imageSizeSmall ne 'custom'}">
	<c:set var="styleSmall" value="${styleSmall} background-size: ${properties.imageSizeSmall};" />
</c:if>
<c:if test="${not empty properties.imageSizeSmall and properties.imageSizeSmall eq 'custom' and not empty properties.imageSizeCustomSmall}">
	<c:set var="styleSmall" value="${styleSmall} background-size: ${properties.imageSizeCustomSmall};" />
</c:if>
<c:if test="${not empty properties.imagePositionXSmall and not empty properties.imagePositionYSmall}">
	<c:set var="styleSmall" value="${styleSmall} background-position: ${properties.imagePositionXSmall}${fn:contains(properties.imagePositionXSmall, '%') ? '' : 'px'} ${properties.imagePositionYSmall}${fn:contains(properties.imagePositionYSmall, '%') ? '' : 'px'};" />
</c:if>
<%-- Small alignment overwrite--%>
<c:if test="${not empty properties.widthSmall}">
	<c:set var="styleSmall" value="${styleSmall} width: ${properties.widthSmall}${fn:contains(properties.widthSmall, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.heightSmall}">
	<c:set var="styleSmall" value="${styleSmall} height: ${properties.heightSmall}${fn:contains(properties.heightSmall, '%') ? '' : 'px'};" />
</c:if>
<c:if test="${not empty properties.marginSmall}">
	<c:set var="styleSmall" value="${styleSmall} margin: ${properties.marginSmall};" />
</c:if>
<c:if test="${not empty properties.paddingSmall}">
	<c:set var="styleSmall" value="${styleSmall} padding: ${properties.paddingSmall};" />
</c:if>
<%--
	Container CSS
	TODO: get breakpoints from design
--%>
<style type="text/css">
<%--
	Section CSS
--%>
<c:if test="${not empty properties.sectionSlant and not empty properties.slantBG}">
	<c:choose>
		<c:when test="${properties.sectionSlant eq 'section-slant-over'}">
			#${nodeId} .section-slant-over:after {
				background: ${properties.slantBG};
			}
		</c:when>
		<c:otherwise>
			#${nodeId} .section-slant-under:before {
				background: ${properties.slantBG};
			}
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty styleDefault}">
	#${nodeId} {
		${styleDefault}
	}
</c:if>
<c:if test="${not empty styleMedium}">
	@media only screen and (max-width: 64em) and (min-width: 40.063em) {
		#${nodeId} {
			${styleMedium}
		}
	}
</c:if>
<c:if test="${not empty styleSmall}">
	@media only screen and (max-width: 40em) {
		#${nodeId} {
			${styleSmall}
		}
	}
</c:if>
<c:if test="${not empty styleEditMode and pageMode eq 'EDIT'}">
	#${nodeId} {
		${styleEditMode}
	}
</c:if>
</style>
<%--
	Container HTML
--%>
<c:if test="${not empty attrID}">
	<a name="${attrID}" id="${attrID}"></a>
</c:if>
<c:choose>
	<c:when test="${not empty properties.isSection and properties.isSection eq 'true'}">
		<section class="${containerClass}" id="${nodeId}">
			<cq:include path="container_parsys" resourceType="foundation/components/parsys"/>
			<c:if test="${not empty properties.sectionSlant}"><div class="section-slant-container ${properties.sectionSlant}"></div></c:if>
		</section>
	</c:when>
	<c:otherwise>
		<div id="${nodeId}"<c:if test="${not empty containerClass}"> class="${containerClass}"</c:if>>
			<cq:include path="container_parsys" resourceType="foundation/components/parsys"/>
		</div>
	</c:otherwise>
</c:choose>
<% if(WCMMode.fromRequest(slingRequest).toString().equals("EDIT")) { %>
	<cq:include path="endofsection" resourceType="citrixosd/components/content/endComponent"/>
<% } %>