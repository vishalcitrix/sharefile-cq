<%@ page trimDirectiveWhitespaces="true" %>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%!public static final String PAR_NAME = "entrypar";
	public static final String ACCORD_NAME = "accordion";
%>

<%
	boolean entryparParent = currentNode.getParent().getName().startsWith(PAR_NAME);
    Node accordParent = currentNode.getParent().getParent();
    String collIcon = "icon-plus";
    String expIcon = "icon-minus";
    if(!entryparParent && accordParent.getName().equals(ACCORD_NAME)){
        if(accordParent.hasProperty("collapse-icon")){
            collIcon = accordParent.getProperty("collapse-icon").getString();
        }
        if(accordParent.hasProperty("expand-icon")){
            expIcon = accordParent.getProperty("expand-icon").getString();
        }
    }
%>

<c:set var="entryParent" value="<%=entryparParent%>" />
<c:set var="collapseIcon" value="<%=collIcon%>" />
<c:set var="expandIcon" value="<%=expIcon%>" />

<div>
	<div class="text ${entryParent ? 'question' : ' plus'} ${properties.isEven ? 'even' : ' odd'}" <c:if test='${not entryParent}'>data-collapse-icon='${collapseIcon}' data-expand-icon='${expandIcon}'</c:if>>
		<c:if test="${entryParent}">
			<span><fmt:message key="accordion.question" /></span>
		</c:if>
		<div <c:if test='${not entryParent}'>class='${collapseIcon}'</c:if> >${not empty properties.text ? properties.text : 'Edit Title' }</div>
	</div>

	<div class="category">
		<c:choose>
			<c:when test="${entryParent}">
				<cq:include script="content.jsp" />
			</c:when>
			<c:otherwise>
				<div class="showBorder">
					<cq:include path="entrypar"
						resourceType="foundation/components/parsys" />
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>