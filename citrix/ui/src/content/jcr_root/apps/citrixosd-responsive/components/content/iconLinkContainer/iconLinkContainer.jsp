<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/apps/citrixosd/global.jsp"%>

<%@page trimDirectiveWhitespaces="true"%>

<c:set var="color" value="${properties.color}"/>
<c:set var="iconValue" value="${properties.icon}"/>
<c:set var="linkBehavior" value="${properties.linkBehavior}"/>
<c:set var="linkClass" value=""/>
<c:if test="${not empty properties.icon}">
	<c:set var="linkClass" value="${color} ${iconValue}"/>
</c:if>
<a rel="${linkBehavior}" href="${properties.linkPath}"<c:if test="${not empty linkClass}"> class="${linkClass}"</c:if>>
   <cq:include path="arrowScrollText" resourceType="swx/component-library/components/content/single-par"/>
</a>
<div class="clearBoth"></div>