<%--
    Background Image

    This component should contain a background image with transparency otherwise it would look
    ugly for the end user. With this component, authors can put their content over the background 
    image. 
    
    All the style css string in generated with JSTL Tags as per jQuery CSS selector requirements
    This component generates 3 different styles 
        1. desktopstyle :- This String contains styles to be applied for desktops
        2. tabletstyle  :- This String contains styles to be applied for tablet
        3. mobilestyle  :- This String contains styles to be applied for mobiles
        
        vishal.gupta@citrix.com
--%>

<%@ page trimDirectiveWhitespaces="true" %>
<%@page import="com.day.cq.wcm.api.WCMMode,
    com.citrixosdRedesign.constants.ResourceConstants"
%>
<%@include file="/libs/foundation/global.jsp"%>

<%! 
    private static final String FILE_REFERENCE_PROPERTY = "fileReference";
    private static final String TABLET_FILE_REFERENCE_PROPERTY = "tabletfileReference";
    private static final String MOBILE_FILE_REFERENCE_PROPERTY = "mobilefileReference";
    private static final String RENDITION_PROPERTY = "rendition";
%>

<%     
    final String containerId = currentNode.getIdentifier();
    pageContext.setAttribute("nodeId",containerId);
%>

    <c:set var="desktopsrc" value="<%= properties.get(FILE_REFERENCE_PROPERTY, "") %>" />
    <c:set var="tabletsrc" value="<%= properties.get(TABLET_FILE_REFERENCE_PROPERTY, "") %>" />
    <c:set var="mobilesrc" value="<%= properties.get(MOBILE_FILE_REFERENCE_PROPERTY, "") %>" />
    
    <c:set var="desktopstyle" value="overflow:visible;background-repeat: no-repeat;position:relative" />
    <c:set var="tabletstyle" value="overflow:visible;background-repeat: no-repeat;position:relative" />
    <c:set var="mobilestyle" value="overflow:visible;background-repeat: no-repeat;position:relative" />
       
    <c:set var="dttoplayer" value="" />
    <c:set var="tablettoplayer" value="" />
    <c:set var="mobiletoplayer" value="" />
    
    <c:set var="lastElement" value="${not empty properties.lastElement ? 'true' : ''}" />
    
    <%-- style settings for desktop --%>
    <c:set var="desktopstyle" value="${desktopstyle}; background-image : url('${desktopsrc}')"/>
    
    <c:if test="${not empty properties.imagePositionX && not empty properties.imagePositionY}"><c:set var="desktopstyle" value="${desktopstyle}; background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.imagePositionX && empty properties.imagePositionY}"><c:set var="desktopstyle" value="${desktopstyle}; background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} 0" /></c:if>
    <c:if test="${empty properties.imagePositionX && not empty properties.imagePositionY}"><c:set var="desktopstyle" value="${desktopstyle}; background-position: 0 ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.desktopbgcolor}"><c:set var="desktopstyle" value="${desktopstyle}; background-color : ${properties.desktopbgcolor}"/></c:if>
    
    <%-- Set rest of properties like margin top and left, cover --%>
    <c:if test="${not empty properties.containerWidth}"><c:set var="desktopstyle" value="${desktopstyle}; width: ${properties.containerWidth}${fn:contains(properties.containerWidth, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.containerHeight}"><c:set var="desktopstyle" value="${desktopstyle}; height: ${properties.containerHeight}${fn:contains(properties.containerHeight, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.marginTop}"><c:set var="desktopstyle" value="${desktopstyle}; margin-top: ${properties.marginTop}${fn:contains(properties.marginTop, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.marginLeft}"><c:set var="desktopstyle" value="${desktopstyle}; margin-left: ${properties.marginLeft}${fn:contains(properties.marginLeft, '%') ? '' : 'px'}"/></c:if>
    <c:if test="${not empty properties.cover}"><c:set var="desktopstyle" value="${desktopstyle}; background-size: cover;" /></c:if>
    
    <%--Prepare style settings for tablet --%>    
    <c:if test="${not empty properties.enableall}"><c:set var="tabletstyle" value="${tabletstyle}; background-image: url('${desktopsrc}')" /></c:if>
    <c:if test="${not empty properties.tabletfileReference}"><c:set var="tabletstyle" value="${tabletstyle}; background-image: url('${tabletsrc}')" /></c:if>
    <c:if test="${not empty properties.tabletimagePositionX && not empty properties.tabletimagePositionY}"><c:set var="tabletstyle" value="${tabletstyle}; background-position: ${properties.tabletimagePositionX}${fn:contains(properties.tabletimagePositionX, '%') ? '' : 'px'}  ${properties.tabletimagePositionY}${fn:contains(properties.tabletimagePositionY, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.tabletimagePositionX && empty properties.tabletimagePositionY}"><c:set var="tabletstyle" value="${tabletstyle}; background-position: ${properties.tabletimagePositionX}${fn:contains(properties.tabletimagePositionX, '%') ? '' : 'px'}  0" /></c:if>
    <c:if test="${empty properties.tabletimagePositionX && not empty properties.tabletimagePositionY}"><c:set var="tabletstyle" value="${tabletstyle}; background-position: 0  ${properties.tabletimagePositionY}${fn:contains(properties.tabletimagePositionY, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.tabletcover && (not empty properties.enableall || not empty properties.tabletfileReference)}"><c:set var="tabletstyle" value="${tabletstyle}; background-size :cover" /></c:if>
    <c:if test="${not empty properties.tabletbgcolor}"><c:set var="tabletstyle" value="${tabletstyle}; background-color : ${properties.tabletbgcolor}"/></c:if>

    <%-- Set rest of properties like height and width, margin top and left, cover --%>
    <c:if test="${not empty properties.tabletcontainerWidth}"><c:set var="tabletstyle" value="${tabletstyle}; width: ${properties.tabletcontainerWidth}${fn:contains(properties.tabletcontainerWidth, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.tabletcontainerHeight}"><c:set var="tabletstyle" value="${tabletstyle}; height: ${properties.tabletcontainerHeight}${fn:contains(properties.tabletcontainerHeight, '%') ? '' : 'px'}" /></c:if>
    <c:if test="${not empty properties.tabletmarginTop}"><c:set var="tabletstyle" value="${tabletstyle}; margin-top : ${properties.tabletmarginTop}${fn:contains(properties.tabletmarginTop, '%') ? ''  : 'px'}" /></c:if>
    <c:if test="${not empty properties.tabletmarginLeft}"><c:set var="tabletstyle" value="${tabletstyle}; margin-left: ${properties.tabletmarginLeft}${fn:contains(properties.tabletmarginLeft, '%') ? ''  : 'px'}"/></c:if>
    
    <%-- prepare mobile style settings for mobile --%>
    <c:if test="${not empty properties.enableall}"><c:set var="mobilestyle" value="${mobilestyle}; background-image: url('${desktopsrc}')" /></c:if>
    <c:if test="${not empty properties.mobilefileReference}"><c:set var="mobilestyle" value="${mobilestyle}; background-image: url('${mobilesrc}')" /></c:if>
    <c:if test="${(not empty properties.mobileimagePositionX) && (not empty properties.mobileimagePositionY)}"><c:set var="mobilestyle" value="${mobilestyle}; background-position: ${properties.mobileimagePositionX}${fn:contains(properties.mobileimagePositionX, '%') ? ' '  : 'px'} ${properties.mobileimagePositionY}${fn:contains(properties.mobileimagePositionY, '%') ? ' '  : 'px'}" /></c:if>
    <c:if test="${not empty properties.mobileimagePositionX && empty properties.mobileimagePositionY}"><c:set var="mobilestyle" value="${mobilestyle}; background-position: ${properties.mobileimagePositionX}${fn:contains(properties.mobileimagePositionX, '%') ? ' '  : 'px'} 0" /></c:if>
    <c:if test="${empty properties.mobileimagePositionX && not empty properties.mobileimagePositionY}"><c:set var="mobilestyle" value="${mobilestyle}; background-position: 0 ${properties.mobileimagePositionY}${fn:contains(properties.mobileimagePositionY, '%') ? ' '  : 'px'}" /></c:if>
    <c:if test="${not empty properties.mobilecover && (not empty properties.enableall || not empty properties.mobilefileReference)}"><c:set var="mobilestyle" value="${mobilestyle}; background-size: cover" /></c:if>
    <c:if test="${not empty properties.mobilebgcolor}"><c:set var="mobilestyle" value="${mobilestyle}; background-color : ${properties.mobilebgcolor}"/></c:if>

    <%-- Set rest of properties like margin top and left, cover --%>
    <c:if test="${not empty properties.mobilecontainerWidth}"><c:set var="mobilestyle" value="${mobilestyle}; width: ${properties.mobilecontainerWidth}${fn:contains(properties.mobilecontainerWidth, '%') ? ' '  : 'px'}" /></c:if>
    <c:if test="${not empty properties.mobilecontainerHeight}"><c:set var="mobilestyle" value="${mobilestyle}; height: ${properties.mobilecontainerHeight}${fn:contains(properties.mobilecontainerHeight, '%') ? ' '  : 'px'}" /></c:if>
    <c:if test="${not empty properties.mobilemarginTop}"><c:set var="mobilestyle" value="${mobilestyle}; margin-top: ${properties.mobilemarginTop}${fn:contains(properties.mobilemarginTop, '%') ? ' '  : 'px'}" /></c:if>
    <c:if test="${not empty properties.mobilemarginLeft}"><c:set var="mobilestyle" value="${mobilestyle}; margin-left: ${properties.mobilemarginLeft}${fn:contains(properties.mobilemarginLeft, '%') ?''  : 'px'}"/></c:if>

    <%--Top Layer div styles --%>
    <c:if test="${not empty properties.desktopmargintopval}"><c:set var="dttoplayer" value="padding-top: ${properties.desktopmargintopval};"/></c:if>
    <c:if test="${not empty properties.tabletmargintopval}"><c:set var="tablettoplayer" value="padding-top: ${properties.tabletmargintopval};"/></c:if>
    <c:if test="${not empty properties.mobilemargintopval}"><c:set var="mobiletoplayer" value="padding-top: ${properties.mobilemargintopval};"/></c:if>
    
    <div class="styleswitch" id="${nodeId}" style="${desktopstyle}" data-large="${desktopstyle}" data-medium="${tabletstyle}" data-small="${mobilestyle}" data-dtautoheight="${properties.dtautoheight}" data-tabautoheight="${properties.tabautoheight}" data-mobautoheight="${properties.mobileautoheight}" data-lastelement="${lastElement}">
       <c:if test="${not empty properties.disableContent ? not properties.disableContent : true}">
            <div class="styleswitch" id="${nodeId}_1" style="${dttoplayer}" data-large="${dttoplayer}" data-medium="${tablettoplayer}" data-small="${mobiletoplayer}" ><cq:include path="backgroundImageParsys" resourceType="foundation/components/parsys"/></div>     
        </c:if>
    </div>
   