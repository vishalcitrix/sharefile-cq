<%--

  Responsive Column Control Component component.
  - This component lets users add columns adding up to max 12 Grids and configure the behavior on tablet and phone.
  
  vishal.gupta@citrix.com
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,com.day.cq.i18n.I18n"%>

<% 
	String layout = properties.get("layout", "6,6");
	String[] cols = layout.split(",");
	int colCount = cols.length;
	
	String layoutMedium = properties.get("layout-medium", "0,0");
	String[] colsMedium = layoutMedium.split(",");
	
	String layoutSmall = properties.get("layout-small", "0,0");
	String[] colsSmall = layoutSmall.split(",");
	
	int i = 0; //initialized for parsys path
	boolean isColSeparator=properties.get("isColSeparator") != null ? true :false;

%>

<c:choose>
	<c:when test="${not empty properties.browserWidth}">
		<div class="rowC">
	</c:when>
	<c:otherwise>
		<div class="row <c:if test="${properties.isCollapsable}">collapse</c:if> <c:if test="${properties.isLeftAlign}">align-left</c:if>">
	</c:otherwise>
</c:choose>

	<% 
		for(String col : cols) { 
	%>
		    <div class="large-<%= col %> 
		    	<% if(!"0,0".equals(layoutMedium)){ %>
		    		medium-<%= colsMedium[i] %> 
		    	<% } %> 
		    	
		    	<% if(!"0,0".equals(layoutSmall)){ %>
		    		small-<%= colsSmall[i] %> 
		    	<% } %>
		    	
		    	<% if(isColSeparator && (i < colCount-1)){ %>
		    		colSeparator
		    	<% } %> 
		    	
		    	columns">
		           
				<cq:include path="<%= "colPar-" + i %>" resourceType="/apps/citrixosd-responsive/components/content/col-control/colparsys" />
		    </div>
	<% 
			i++; 
		} 
	%>
</div>