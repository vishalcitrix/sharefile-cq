<%--
  Tab Carousel component.
  This is a tab carousel component developed based on owl carousel. The tabs control the carousel for desktop and mobile. In mobile it turns into a simple carousel.
  Requires tabCarousel.scss from global-sass and collaboration
  Requires carousel.js from collaboration
  Requires owlCarousel.scss from global-sass and collaboration
--%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                com.day.cq.personalization.ClientContextUtil" %>
<%@ include file="/apps/citrixosd/global.jsp" %>
<%@ page import="com.citrixosd.utils.Utilities" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.jcr.Property" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%
   String currentMode = WCMMode.fromRequest(slingRequest).toString();
   final String containerId = ClientContextUtil.getId(resource.getPath()).replace("-", "");
   pageContext.setAttribute("nodeId",containerId);
   pageContext.setAttribute("pageMode", currentMode);
%>
<%!
    private static final String TAB_DATA_NODE = "features";
    private static final String ICON_NODE = "icon";
 %>
 <%
    final ArrayList<Map<String, Property>> tabFeatures = new ArrayList<Map<String, Property>>();
    if(currentNode.hasNode(TAB_DATA_NODE)) {
        tabFeatures.addAll(Utilities.parseStructuredMultifield(currentNode.getNode(TAB_DATA_NODE)));
    }

    // Create the icon nodes if they don't exist so that the fonticon dialog works on first use
    int itemsCount = 1;
    if( properties.get("itemscount") != null ){
        itemsCount = Integer.parseInt(properties.get("itemscount").toString());
    }
    for(int i = 1; i <= itemsCount; i+=1){
      if(!currentNode.hasNode("tabIcon"+i)){
        Node tabNode = currentNode.addNode("tabIcon"+i);
        currentNode.save();
        tabNode.setProperty(ICON_NODE, "none");
        tabNode.setProperty("sling:resourceType", "citrixosd-responsive/components/content/fonticon");
        currentNode.save();
      }
    }
%>
<c:set var="tabFeatures" value="<%= tabFeatures %>"/>
<c:set var="currentMode" value="<%= currentMode %>"/> 

<div class="row tab-carousel" data-editmode="${currentMode}">
  <c:choose>
    <c:when test="${properties.tabPlacementBottom eq 'true'}">
      <div class="row feature-section-1">
        <div class="small-12 medium-12 large-12 columns">
            <div id="${nodeId}" class="owl-carousel carousel-primary" data-itemCount="${properties.itemscount}" data-itemslarge="1" data-disablenavinmediumviewport="false" data-itemsmedium="1" data-showsingleitemonsmallviewport="true" data-itemssmall="1" data-navigation="false" data-disablenavinsmallviewport="false"  data-autoplay="true" data-rewindnav="true"  data-theme="" data-ospace data-disableoverlap="true" data-disablenavoverlap="true" data-paginationDotsColor="#d3d3d3" data-tabcarousel="true" data-autoloopspeed="${properties.autoloopSpeed}">
              <c:forEach begin="1" end="${properties.itemscount}" varStatus="status">
                  <div class="item">
                      <cq:include path="item-${status.count}"
                              resourceType="foundation/components/parsys" />
                      </div>
                  </c:forEach>
              </div>
        </div>
      </div>
      <c:choose>
        <c:when test="${properties.itemscount eq '3'}">
          <div class="row feature-section-2">
            <c:forEach var="item" begin="0" end="${properties.itemscount}" items="${tabFeatures}" varStatus="loopIterator">
              <c:choose>
                <c:when test="${loopIterator.count eq '1'}">
                  <div class = "small-12 medium-12 large-4 columns">
                    <div class="tab-1 data-tab current">
                      <c:choose>
                        <c:when test="${(item.icon.string eq 'icon-none') && (currentMode ne 'EDIT')}">
                          <div class="tab-none"></div>
                        </c:when>
                        <c:when test="${(empty item.icon.string) || (item.icon.string eq 'icon-none')}">
                          <div class="tab-icon">
                            <cq:include path="tabIcon${loopIterator.count}" resourceType="/apps/citrixosd-responsive/components/content/fonticon"/>
                          </div>
                        </c:when>
                        <c:otherwise>
                          <div class="tab-icon">
                            <span class="${item.icon.string} tab-carousel-icon"></span>
                          </div>
                        </c:otherwise>
                      </c:choose>
                      <div class="tab-heading">${item.tabHeading.string}</div>
                      <div class="video-heading">
                        <cq:include path="vh-${loopIterator.count}"
                            resourceType="foundation/components/parsys" />
                      </div>
                      <div class="tab-description">${item.descText.string}</div>
                      <div class="clearBoth"></div>
                    </div>
                  </div>
                </c:when>
                <c:otherwise>
                  <div class = "small-12 medium-12 large-4 columns">
                    <div class="tab-${loopIterator.count} data-tab hide-for-small-only">
                    <c:choose>
                      <c:when test="${(item.icon.string eq 'icon-none') && (currentMode ne 'EDIT')}">
                        <div class="tab-none"></div>
                      </c:when>
                      <c:when test="${(empty item.icon.string) || (item.icon.string eq 'icon-none')}">
                          <div class="tab-icon">
                            <cq:include path="tabIcon${loopIterator.count}" resourceType="/apps/citrixosd-responsive/components/content/fonticon"/>
                          </div>
                        </c:when>
                      <c:otherwise>
                        <div class="tab-icon">
                          <span class="${item.icon.string} tab-carousel-icon"></span>
                        </div>
                      </c:otherwise>
                    </c:choose>
                      <div class="tab-heading">${item.tabHeading.string}</div>
                      <div class="video-heading">
                      	<cq:include path="vh-${loopIterator.count}"
                            resourceType="foundation/components/parsys" />
                      </div>
                      <div class="tab-description">${item.descText.string}</div>
                      <div class="clearBoth"></div>
                    </div>
                  </div>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </div>
        </c:when>
      </c:choose>
    </c:when>
    <c:otherwise>
      <div class="section-1 small-12 medium-12 large-5 columns">
          <div class="row">
            <div class="small-12 medium-6 large-12 columns">
              <c:forEach var="item" begin="0" end="${(properties.itemscount)/2}" items="${tabFeatures}" varStatus="loopIterator">
                  <c:choose>
                    <c:when test="${loopIterator.count eq '1'}">
                      <div class="tab-1 current">
                        <div class="tab-row">
                          <div class="tab-column-1">
                            <c:choose>
                              <c:when test="${(empty item.icon.string) || (item.icon.string eq 'icon-none')}">
                                  <div class="tab-icon">
                                    <cq:include path="tabIcon${loopIterator.count}" resourceType="/apps/citrixosd-responsive/components/content/fonticon"/>
                                  </div>
                              </c:when>
                              <c:otherwise>
                                <div class="tab-icon">
                                  <span class="${item.icon.string} tab-carousel-icon"></span>
                                </div>
                              </c:otherwise>
                            </c:choose>
                          </div>
                          <div class="tab-column-2">
                            <p class="tab-heading">${item.tabHeading.string}</p>
                            <p class="tab-description hide-for-small-only">${item.descText.string}</p>
                          </div>
                          <div class="clearBoth"></div>
                        </div>
                      </div>
                    </c:when>
                    <c:when test="${loopIterator.count le (properties.itemscount+1)/2}">
                      <div class="tab-${loopIterator.count} hide-for-small-only">
                        <div class="tab-row">
                          <div class="tab-column-1">
                            <c:choose>
                              <c:when test="${(empty item.icon.string) || (item.icon.string eq 'icon-none')}">
                                  <div class="tab-icon">
                                    <cq:include path="tabIcon${loopIterator.count}" resourceType="/apps/citrixosd-responsive/components/content/fonticon"/>
                                  </div>
                              </c:when>
                              <c:otherwise>
                                <div class="tab-icon">
                                  <span class="${item.icon.string} tab-carousel-icon"></span>
                                </div>
                              </c:otherwise>
                            </c:choose>
                          </div>
                          <div class="tab-column-2">
                            <p class="tab-heading">${item.tabHeading.string}</p>
                            <p class="tab-description hide-for-small-only">${item.descText.string}</p>
                          </div>
                          <div class="clearBoth"></div>
                        </div>
                      </div>
                    </c:when>
                  </c:choose>
              </c:forEach>
            </div>
            <div class="small-12 medium-6 large-12 columns">
              <c:forEach var="item" begin="0" end="${properties.itemscount}" items="${tabFeatures}" varStatus="loopIterator">
                <c:choose>
                  <c:when test="${loopIterator.count gt (properties.itemscount+1)/2}">
                    <div class="tab-${loopIterator.count} hide-for-small-only">
                      <div class="tab-row clearBoth">
                        <div class="tab-column-1">
                          <c:choose>
                            <c:when test="${(empty item.icon.string) || (item.icon.string eq 'icon-none')}">
                                <div class="tab-icon">
                                  <cq:include path="tabIcon${loopIterator.count}" resourceType="/apps/citrixosd-responsive/components/content/fonticon"/>
                                </div>
                            </c:when>
                            <c:otherwise>
                              <div class="tab-icon">
                                <span class="${item.icon.string} tab-carousel-icon"></span>
                              </div>
                            </c:otherwise>
                          </c:choose>
                        </div>
                        <div class="tab-column-2">
                          <p class="tab-heading">${item.tabHeading.string}</p>
                          <p class="tab-description hide-for-small-only">${item.descText.string}</p>
                        </div>
                        <div class="clearBoth"></div>
                      </div>
                    </div>   
                  </c:when>
                </c:choose>
              </c:forEach>
            </div>
          </div>
      </div>
      <div class="section-2 small-12 medium-12 large-7 columns">
          <div id="${nodeId}" class="owl-carousel carousel-primary" data-itemCount="${properties.itemscount}" data-itemslarge="1" data-disablenavinmediumviewport="true" data-itemsmedium="1" data-showsingleitemonsmallviewport="true" data-itemssmall="1" data-navigation="true" data-disablenavinsmallviewport="false"  data-autoplay="true" data-rewindnav="true"  data-theme="" data-ospace data-disableoverlap="true" data-disablenavoverlap="true" data-paginationDotsColor data-tabcarousel="true" data-autoloopspeed="${properties.autoloopSpeed}">
            <c:forEach begin="1" end="${properties.itemscount}" varStatus="status">
                <div class="item">
                    <cq:include path="item-${status.count}"
                            resourceType="foundation/components/parsys" />
                    </div>
                </c:forEach>
            </div>
      </div>
    </c:otherwise>
  </c:choose>
</div>