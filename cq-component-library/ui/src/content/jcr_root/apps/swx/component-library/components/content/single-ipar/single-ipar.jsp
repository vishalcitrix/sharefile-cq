<%--
    Single iPar Component
    @author rboll@siteworx
    
    This component allows for the placement of a single component, using the standard dropzone functionality available in CQ5. Customization is available in the following ways:
        In Design Mode:
            - Customize the "Empty Text" to be displayed in the dropzone.
            - Customize the components that are allowed to be placed into the container.
            
        In Edit Mode:
            - Customize the inheritance properties:
                - If disabled, the component will not inherit from parent pages.
                - If cancelled, the component will not allow inheritance by child pages.
                - If manual, the component will allow the author to choose a page to inherit from.
        
        Standard Inheritance Behavior (Parental Inheritance):
            - Traverse the tree upward from the current component, looking for a valid inheritance source.
                - If found, display it. Update the status indicator in the edit bar to reflect inheritance source.
                - If not found, display an appropriate status message in the indicator.

--%>
<%@page import="com.day.cq.wcm.foundation.Paragraph,
                    com.day.cq.wcm.foundation.ParagraphSystem,
                    com.day.cq.wcm.api.components.IncludeOptions,
                    com.day.cq.commons.jcr.JcrConstants,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.api.components.Toolbar,
                    com.day.text.Text,
                    java.util.Iterator"%><%
%><%@include file="/libs/foundation/global.jsp"%>
<%@taglib prefix="swx" uri="http://siteworx.com/cq/tags" %>
<%
    boolean writeToToolbar = WCMMode.fromRequest(request).equals(WCMMode.EDIT);
    String statusMessage = "-";
    
    String newType = resource.getResourceType() + "/new";
    String inheritanceStatus = properties.get("inheritanceStatus", "normal");
    
    boolean cancelInheritance = "cancel".equals(inheritanceStatus);
    boolean disableInheritance = "disable".equals(inheritanceStatus);
    boolean isInheritanceCancelled = false;
    
    String inheritanceSource = (String)properties.get("manualSource", "");
    boolean manualInheritance = !"".equals(inheritanceSource);
    
    boolean foundInherited = false;
    Resource inheritSource = null;
    
    String parPath = resource.getPath().substring(currentPage.getContentResource().getPath().length() + 1);
    
    if (manualInheritance) {
        //takes presidence
        Page manualSourcePage = pageManager.getPage(inheritanceSource);
        if (manualSourcePage != null) {
            Resource inheritFrom = resourceResolver.getResource(manualSourcePage.getContentResource().getPath() + "/" + parPath);
            if (inheritFrom != null) {
                //logic here to inherit
                Iterator<Resource> inheritSources = resourceResolver.listChildren(inheritFrom);
                if (inheritSources.hasNext()) {
                    inheritSource = inheritSources.next();
                }
            }
        }
        
    } else if (!disableInheritance) {
        Page parent = currentPage.getParent();
        while (parent != null && parent.getContentResource() != null && inheritSource == null && !isInheritanceCancelled) {
			//get the resource to inherit from
            Resource inheritFrom = resourceResolver.getResource(parent.getContentResource().getPath() + "/" + parPath);
			inheritanceSource = parent.getPath();
            if (inheritFrom != null) {
                Node inheritFromNode = inheritFrom.adaptTo(Node.class);
                if (inheritFromNode.hasProperty("inheritanceStatus")) {
                    String status = inheritFromNode.getProperty("inheritanceStatus").getString();
                    //isInheritanceCancelled = "cancel".equals(status) || "disable".equals(status);
                    isInheritanceCancelled = "cancel".equals(status);
                }
                if (inheritFromNode.hasProperty("manualInheritance")) {
                	String manualAsString = inheritFromNode.getProperty("manualInheritance").getString();
                    manualInheritance = "true".equals(manualAsString);
                }
                if (manualInheritance && inheritFromNode.hasProperty("manualSource")) {
                	String manualSource = inheritFromNode.getProperty("manualSource").getString();
                	parent = resourceResolver.getResource(manualSource).adaptTo(Page.class);
                }
                else if (!isInheritanceCancelled) {
                    //logic here to inherit
                    Iterator<Resource> inheritSources = resourceResolver.listChildren(inheritFrom);
                    if (inheritSources.hasNext()) {
                        inheritSource = inheritSources.next();
                    } else {
                    	parent = parent.getParent();
                        inheritFrom = null;
                    }
                }
            } else {
                parent = parent.getParent();
            }
        }
    }
    
    if (!disableInheritance && inheritSource != null) {
        //attempting to inherit
        //coming in, should have inheritSource and not null
        %>
        <swx:setWCMMode mode="READ_ONLY">
        	<sling:include resource="<%=inheritSource %>"/>
        </swx:setWCMMode>
        <%
        foundInherited = true;
    }

    if (!foundInherited) {
        //update status message
        if (manualInheritance) {
            //manual inheritance, but nothing found
            if (!isInheritanceCancelled) {
                //Manual Inheritance: Nothing to inherit from.
                statusMessage = "Manual Inheritance: Nothing to inherit from.";
            } else {
                //Manual Inheritance: Inheritance cancelled at path.
                statusMessage = "Manual Inheritance: Inheritance cancelled at <a href='" + inheritanceSource + "'>" + inheritanceSource +"</a>.";
            }
        } else if (inheritSource == null) {
            if (disableInheritance) {
                //inheritance is disabled for this node
                //Inheritance is disabled.
                statusMessage = "Inheritance is disabled.";
            } else if (!isInheritanceCancelled) {
                //normal inheritance, but nothing found
                //Nothing to inherit from.
                statusMessage = "Nothing to inherit from.";
            } else {
                //normal inheritance, inheritance cancelled
                //Inheritance cancelled at path.
                statusMessage = "Inheritance cancelled at <a href='" + inheritanceSource + "'>" + inheritanceSource +"</a>.";
            }
        }
        
        //include this resource, or dropzone if not found
        ParagraphSystem parSys = ParagraphSystem.create(resource, slingRequest);
        Paragraph par = null;
        if (parSys.paragraphs() != null && parSys.paragraphs().size() > 0)
            par = parSys.paragraphs().get(0);
        if (par != null) {
            if (editContext != null) {
                editContext.setAttribute("currentResource", par);
            }
            IncludeOptions.getOptions(request, true).getCssClassNames().add("section");
            %><sling:include resource="<%= par %>"/><%
        } else {
            if (editContext != null) {
                editContext.setAttribute("currentResource", null);
                // draw 'new' bar
                IncludeOptions.getOptions(request, true).getCssClassNames().add("section");
                %><cq:include path="*" resourceType="<%= newType %>"/><%
            }
        }
    } else {
        //inheriting
        if (manualInheritance) {
            //manual inheritance, found
            //Manual Inheritance: Inheriting from path.
            
            statusMessage = "Manual Inheritance: Inheriting from <a href='" + inheritanceSource + "'>" + inheritanceSource +"</a>.";
        } else {
            //normal inheritance, found
            //Inheriting from parent.
            statusMessage = "Inheriting from parent <a href='" + inheritanceSource + "'>" + inheritanceSource +"</a>.";
        }
    }
    
    if (writeToToolbar) {
    	if(editContext != null && editContext.getEditConfig() != null && editContext.getEditConfig().getToolbar() != null) {
            editContext.getEditConfig().getToolbar().add(4, new Toolbar.Separator());
            editContext.getEditConfig().getToolbar().add(5, new Toolbar.Label("Status:"));
            editContext.getEditConfig().getToolbar().add(6, new Toolbar.Label(statusMessage));	
    	}
    }
%>