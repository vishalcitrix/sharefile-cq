<%--
	Image Renditions

	This component is an alternate image component but removing all the existing functionality and
	encouraging the use of image renditions.
	
	Dev: This component is like the foundation/components/image but with some major differences such
	as there is no crop, resize, rotate of an image. The dialog run a query to a rendition servlet to
	retrieve all the rendition of a root image. Please check <code>com.siteworx.servlet.RenditionsServlet
	</code> for more information. The results will be displayed on the advance tab in the dialog. When 
	the user chooses a new image, there is a javascript function what will compare if the initial part of
	the string matches to the file reference, else it will run a query again to retrieve the data and 
	re-populate the drop down list. This component also removes the '.img' selector from getting hit, 
	therefore better performance, but again... limited functionality. The drag and drop will also reset 
	the rendition value.
  
    achew@siteworx.com

--%>

<%@page import="com.day.cq.wcm.foundation.Image"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<%@include file="/libs/foundation/global.jsp"%>

<c:choose>
	<c:when test="${not empty properties['fileReference']}">
		<c:choose>
			<c:when test="${not empty properties['linkURL']}">
				<a title="${properties['jcr:title']}" href="${properties['linkURL']}"> 
					<img title="${properties['jcr:title']}" alt="${not empty properties['alt'] ? properties['alt'] : properties[':title']}" src="${not empty properties['rendition'] ? properties['rendition'] : properties['fileReference']}">
				</a>
			</c:when>
			<c:otherwise>
				<img title="${properties['jcr:title']}" alt="${not empty properties['alt'] ? properties['alt'] : properties[':title']}" src="${not empty properties['rendition'] ? properties['rendition'] : properties['fileReference']}">
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:if test="<%= WCMMode.fromRequest(request).equals(WCMMode.EDIT) %>">
			<img class="cq-image-placeholder" src="/libs/cq/ui/resources/0.gif">
		</c:if>
		<%-- Do not draw image... not in edit mode & it's not set. --%>
	</c:otherwise>
</c:choose>
