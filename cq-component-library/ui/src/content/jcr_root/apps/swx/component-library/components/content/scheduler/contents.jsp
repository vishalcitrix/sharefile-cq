<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="com.siteworx.component.scheduler.ScheduledComponent"%>
<%@page import="com.siteworx.component.scheduler.config.SchedulerTimeZoneService"%>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );

   //SchedulerTimeZoneService timeZoneService = 
   
   boolean isEditMode = WCMMode.fromRequest(request).equals(WCMMode.EDIT);
   ScheduledComponent scheduledComponent = new ScheduledComponent(currentNode, isEditMode);
%>

<c:set var="schedule" value="<%=scheduledComponent%>" />

<c:if test="${schedule.showing}">
	<cq:include path="par" resourceType="swx/component-library/components/content/scheduler/schedulerparsys"/>
</c:if>