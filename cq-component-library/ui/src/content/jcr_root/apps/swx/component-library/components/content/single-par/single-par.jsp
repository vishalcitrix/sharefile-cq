<%--
	Single Par Component
	@author rboll@siteworx
	
	This component allows for the placement of a single component, using the standard dropzone functionality available in CQ5. Customization is available in the following ways:
		In Design Mode:
			- Customize the "Empty Text" to be displayed in the dropzone.
			- Customize the components that are allowed to be placed into the container.
	
	This component offers the same functionality of the Single iPar Component, without inheritance.
		
--%><%@page import="com.day.cq.wcm.foundation.Paragraph,
                    com.day.cq.wcm.foundation.ParagraphSystem,
                    com.day.cq.wcm.api.components.IncludeOptions,
                    com.day.cq.commons.jcr.JcrConstants,
                    com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    
    ParagraphSystem parSys = ParagraphSystem.create(resource, slingRequest);
    String newType = resource.getResourceType() + "/new";
    Paragraph par = null;
    if (parSys.paragraphs() != null && parSys.paragraphs().size() > 0)
        par = parSys.paragraphs().get(0);
    if (par != null) {
        if (editContext != null) {
            editContext.setAttribute("currentResource", par);
        }

        // include 'normal' paragraph
        IncludeOptions.getOptions(request, true).getCssClassNames().add("section");
                    
        // draw anchor if needed
        if (currentStyle.get("drawAnchors", false)) {
            String path = par.getPath();
            path = path.substring(path.indexOf(JcrConstants.JCR_CONTENT) + JcrConstants.JCR_CONTENT.length() + 1);
            String anchorID = path.replace("/", "_").replace(":", "_");
        }
        %><sling:include resource="<%= par %>"/><%
        
        }  else {   
            if (editContext != null) {
                editContext.setAttribute("currentResource", null);
                // draw 'new' bar
                IncludeOptions.getOptions(request, true).getCssClassNames().add("section");
                if (par == null) {
                    %><cq:include path="*" resourceType="<%= newType %>"/><%
                }
            }
        }
%>