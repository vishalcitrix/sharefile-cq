<%--
    Scheduler
    
    Components within the scheduler will only appear within the current time frame and 
    is not within the excluded time frame. Authors would have to set the time frame for 
    days of the weeks as default settings. If default setting is not set, then it will 
    automatically not display the content. Additional excluded dates can be inserted by 
    authors to exclude certain days (Holidays).

    Dev: All the default weekly dates are in a separate node and when this component is 
    initialized, this will programmatically create the initial nodes. Each node will consist 
    of start date and end date. This component will check for the current day and determine 
    if the current time is within the time frame. The excluded date field is stored in a 
    JSON format which can be parsed to check if the current time is within the excluded date.
    
    (Day.startTime > current < Day.endTime) && (Excluded.startTime > current < Excluded.endTime) = result
    true                                    &&  true                                             = hide
    true                                    &&  false                                            = display
    false                                   &&  true                                             = hide
    false                                   &&  false                                            = hide

    @author achew@siteworx.com
    @author rboll@siteworx
 --%>

<%@page import="com.day.cq.wcm.api.components.Toolbar"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.siteworx.component.scheduler.ScheduledComponent"%>
<%@page import="com.siteworx.component.scheduler.config.SchedulerTimeZoneService"%>

<%@include file="/libs/foundation/global.jsp"%>

<%-- This is assumed to be included, otherwise it must be called here but this could cause rewrite issues when used with the rewriter --%>
<%--<cq:includeClientLib categories="swx.scheduledComponent"/>--%>
<%
	boolean isEditMode = WCMMode.fromRequest(request).equals(WCMMode.EDIT);

	if (isEditMode)
	    editContext.getEditConfig().setEmptyText(currentStyle.get("cq:emptyText", editContext.getEditConfig().getEmptyText()));

	SchedulerTimeZoneService timeZoneService = sling.getService(SchedulerTimeZoneService.class);
	TimeZone timeZone = timeZoneService.getTimeZone();
    ScheduledComponent scheduledComponent = new ScheduledComponent(currentNode, isEditMode, timeZone);
    scheduledComponent.initialize();
    String asyncPath = resource.getPath() + ".contents.html";
%>

<c:set var="writeToToolbar" value="<%=isEditMode%>" />

<c:set var="asyncPath" value="<%= asyncPath%>"/>

<c:set var="schedule" value="<%=scheduledComponent%>" />
<c:choose>
    <c:when test="${writeToToolbar}">
        <cq:include path="par" resourceType="swx/component-library/components/content/scheduler/schedulerparsys"/>
    </c:when>
    
    <c:otherwise>
        <a class="scheduler hide" href="${asyncPath}"></a>
    </c:otherwise>
</c:choose>
<%
    if (isEditMode) {
        editContext.getEditConfig().getToolbar().add(new Toolbar.Separator());
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label("Status:"));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label(scheduledComponent.getStatusMessage()));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Separator());
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label("Current Time: "));
        editContext.getEditConfig().getToolbar().add(new Toolbar.Label(scheduledComponent.getCurrentTime()));
    }
%>