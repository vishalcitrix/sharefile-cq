<%--
	Background Image

  	This component should contain a background image with transparency otherwise it would look
  	ugly for the end user. With this component, authors can put their content over the background 
  	image. 
  	
 	Dev: This component is like the foundation/components/image but with some major differences such
  	as there is no drag and drop feature on the page because authors may want to keep the background
  	image empty and use this component to move to certain places. This component relies on the image 
  	rendition component's dialog.
  	
  	Dependencies: Image rendition
  	
  	achew@siteworx.com

--%>

<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.day.cq.wcm.foundation.Image"%>
<%@page import="com.siteworx.rewrite.transformer.ContextRootTransformer"%>

<%@include file="/libs/foundation/global.jsp"%>

<%! 
	private static final String FILE_REFERENCE_PROPERTY = "fileReference";
	private static final String RENDITION_PROPERTY = "rendition";
%>

<% 
	final ContextRootTransformer transformer = sling.getService(ContextRootTransformer.class);
	String url = properties.get(RENDITION_PROPERTY, properties.get(FILE_REFERENCE_PROPERTY, ""));
	String transformed = null;
	transformed = transformer.transform(url);
	if (transformed != null) {
	    url = transformed;
	}
%>

<c:set var="filePath" value="<%= url %>"/>

<div style="background-image:url('${filePath}');
			<c:choose>
				<c:when test="${not empty properties.imagePositionX && not empty properties.imagePositionY}">
					background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'};
				</c:when>
				<c:when test="${not empty properties.imagePositionX && empty properties.imagePositionY}">
					background-position: ${properties.imagePositionX}${fn:contains(properties.imagePositionX, '%') ? '' : 'px'} 0;
				</c:when>
				<c:when test="${empty properties.imagePositionX && not empty properties.imagePositionY}">
					background-position: 0 ${properties.imagePositionY}${fn:contains(properties.imagePositionY, '%') ? '' : 'px'};
				</c:when>
				<c:otherwise>
					<%--Do not add background position --%>
				</c:otherwise>
			</c:choose>

			<c:if test="${not empty properties.cover}">background-size: cover;</c:if>
			background-repeat:no-repeat;
			<c:if test="${not empty properties.bgcolor}">background-color: ${properties.bgcolor};</c:if>
			<c:if test="${not empty properties.containerHeight}">height: ${properties.containerHeight}${fn:contains(properties.containerHeight, '%') ? '' : 'px'};</c:if>
			<c:if test="${not empty properties.containerWidth}">width: ${properties.containerWidth}${fn:contains(properties.containerWidth, '%') ? '' : 'px'};</c:if>
			<c:if test="${not empty properties.marginTop}">margin-top: ${properties.marginTop}${fn:contains(properties.marginTop, '%') ? '' : 'px'};</c:if>
			<c:if test="${not empty properties.marginLeft}">margin-left: ${properties.marginLeft}${fn:contains(properties.marginLeft, '%') ? '' : 'px'};</c:if>
			overflow:visible;
">
	<c:if test="${not empty properties.disableContent ? not properties.disableContent : true}">
		<cq:include script="content.jsp"/>
	</c:if>
</div>