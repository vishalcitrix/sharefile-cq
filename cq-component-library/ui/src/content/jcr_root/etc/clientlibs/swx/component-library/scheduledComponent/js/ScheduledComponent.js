var ScheduledComponent = {};
ScheduledComponent.loadScheduledComponents = function() {
    $("a.scheduler[href!='']").each(function() {
        var $this = $(this);
        var div = $('<div />');
        div.load($this.attr('href'), function() {
            Lightbox.init();
        });
        $this.after(div);
    });
};

$(function() {
    ScheduledComponent.loadScheduledComponents();
});