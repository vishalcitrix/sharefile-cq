package com.siteworx.calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalendarUtil {
	
	private static final Logger	LOG					= LoggerFactory.getLogger(CalendarUtil.class);
	
	/**
	 * Compares two Calendars, up to {@linkplain Calendar#MONTH}, ignoring the {@linkplain Calendar#YEAR} and {@linkplain Calendar#MILLISECOND} fields.
	 */
	public static int compareUpToMonths(Calendar a, Calendar b){
		return compareCal(a, b, Calendar.MONTH);
	}
	
	/**
	 * Compares two Calendars, up to {@linkplain Calendar#DAY_OF_WEEK}, ignoring the {@linkplain Calendar#YEAR} and {@linkplain Calendar#MILLISECOND} fields.
	 */
	public static int compareUpToDays(Calendar a, Calendar b){
		return compareCal(a, b, Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * Compares two Calendars, up to {@linkplain Calendar#HOUR_OF_DAY}, ignoring the {@linkplain Calendar#YEAR} and {@linkplain Calendar#MILLISECOND} fields.
	 */
	public static int compareUpToHours(Calendar a, Calendar b){
		return compareCal(a, b, Calendar.HOUR_OF_DAY);
	}
	
	/**
	 * Compares two Calendars, up to {@linkplain Calendar#MINUTE}, ignoring the {@linkplain Calendar#YEAR} and {@linkplain Calendar#MILLISECOND} fields.
	 */
	public static int compareUpToMinutes(Calendar a, Calendar b){
		return compareCal(a, b, Calendar.MINUTE);
	}
	
	/**
	 * Compares two Calendars, up to {@linkplain Calendar#SECOND}, ignoring the {@linkplain Calendar#YEAR} and {@linkplain Calendar#MILLISECOND} fields.
	 */
	public static int compareUpToSeconds(Calendar a, Calendar b){
		return compareCal(a, b, Calendar.SECOND);
	}
	
	private static Integer getPrevious(Calendar a, Calendar b, Integer precision){
		switch(precision){
			case Calendar.SECOND : 
				//System.out.print("Comparing seconds: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				//LOG.debug("Comparing seconds: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				return Calendar.MINUTE;
			case Calendar.MINUTE : 
				//System.out.print("Comparing minutes: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				//LOG.debug("Comparing minutes: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				return Calendar.HOUR_OF_DAY;
			case Calendar.HOUR_OF_DAY : 
				//System.out.print("Comparing hours: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				//LOG.debug("Comparing hours: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				return Calendar.DAY_OF_MONTH;
			case Calendar.DAY_OF_MONTH : 
				//System.out.print("Comparing days: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				//LOG.debug("Comparing days: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				return Calendar.MONTH;
			default: 
				//System.out.print("Comparing months: "+a.get(precision)+" comp "+b.get(precision)+ " = ");
				return null;
		}
	}
	
	private static int compareCal(Calendar a, Calendar b, int precision){
		Integer previous = getPrevious(a, b, precision);
		if(previous == null){
			return new Integer(a.get(precision)).compareTo(b.get(precision));
		}
		Integer firstComparison = compareCal(a, b, previous);
		if(firstComparison == 0 || firstComparison == null){
			return new Integer(a.get(precision)).compareTo(b.get(precision));
		}else {
			return firstComparison;
		}
	}
	
	public static void main(String[] args) throws Exception{		
		int[] precisions = {Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND};
		
		for(int month = 0 ; month < 12; month++){
			for(int day = 0; day < 29; day++){
				for(int hour = 0; hour < 24; hour++){
					for(int minute=0; minute < 60; minute++){
						for(int second = 0; second < 60; second++){
							for(int i=0; i < precisions.length; i++){
								Calendar start = new GregorianCalendar();
								start.set(Calendar.MONTH, month);
								start.set(Calendar.DAY_OF_MONTH, day);
								start.set(Calendar.HOUR_OF_DAY, hour);
								start.set(Calendar.MINUTE, minute);
								start.set(Calendar.SECOND, second);
								start.clear(Calendar.MILLISECOND);
								start.clear(Calendar.YEAR);
								
								Calendar end = new GregorianCalendar();
								end.set(Calendar.MONTH, start.get(Calendar.MONTH));
								end.set(Calendar.DAY_OF_MONTH, start.get(Calendar.DAY_OF_MONTH));
								end.set(Calendar.HOUR_OF_DAY, start.get(Calendar.HOUR_OF_DAY));
								end.set(Calendar.MINUTE, start.get(Calendar.MINUTE));
								end.set(Calendar.SECOND, start.get(Calendar.SECOND));
								end.clear(Calendar.MILLISECOND);
								end.clear(Calendar.YEAR);
								
								System.out.flush();
								//System.out.println(start.getTime().toString()+"   "+end.getTime().toString());
								int compareCal = compareCal(start, end, precisions[i]);
								if(compareCal != Integer.signum(start.compareTo(end)) && start.get(Calendar.YEAR) == end.get(Calendar.YEAR) && start.get(Calendar.MILLISECOND) == end.get(Calendar.MILLISECOND) && start.get(Calendar.DST_OFFSET) == end.get(Calendar.DST_OFFSET)){
									System.out.println("Test 1 "+month+"/"+day+" "+hour+":"+minute+":"+second);
									System.out.println(precisions[i]+" "+(compareCal)+" correct == "+Integer.signum(start.compareTo(end))+"   "+start.getTime()+"   "+end.getTime());
									System.out.flush();
									//throw new Exception();
								}
								
								System.out.flush();
								start.add(precisions[i], end.get(precisions[i]-1));
								System.out.flush();
								compareCal = compareCal(start, end, precisions[i]);
								
								if(compareCal != Integer.signum(start.compareTo(end)) && start.get(Calendar.YEAR) == end.get(Calendar.YEAR) && start.get(Calendar.MILLISECOND) == end.get(Calendar.MILLISECOND) && start.get(Calendar.DST_OFFSET) == end.get(Calendar.DST_OFFSET)){
									System.out.println("Test 2 "+month+"/"+day+" "+hour+":"+minute+":"+second);
									System.out.println(precisions[i]+" "+(compareCal)+" correct == "+Integer.signum(start.compareTo(end))+"   "+start.getTime()+"   "+end.getTime());
									System.out.flush();
									//throw new Exception();
								}
								
								start.add(precisions[i], end.get(precisions[i]+1));
								System.out.flush();
								compareCal = compareCal(start, end, precisions[i]);
								if(compareCal != Integer.signum(start.compareTo(end)) && start.get(Calendar.YEAR) == end.get(Calendar.YEAR) && start.get(Calendar.MILLISECOND) == end.get(Calendar.MILLISECOND) && start.get(Calendar.DST_OFFSET) == end.get(Calendar.DST_OFFSET)){
									System.out.println("Test 3 "+month+"/"+day+" "+hour+":"+minute+":"+second);
									System.out.println(precisions[i]+" "+(compareCal)+" correct == "+Integer.signum(start.compareTo(end))+"   "+start.getTime()+"   "+end.getTime());
									System.out.flush();
									//throw new Exception();
								}
							}
						}
					}
				}

			}
		}
		
	}
	
}