package com.siteworx.component.scheduler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siteworx.calendar.CalendarUtil;

public class ScheduledComponent {

    private static final Logger log = LoggerFactory
	    .getLogger(ScheduledComponent.class);

    private static Map<Integer, String> dayToNodeNameMap = new LinkedHashMap<Integer, String>();
    static {
	dayToNodeNameMap.put(Calendar.SUNDAY, "sunday");
	dayToNodeNameMap.put(Calendar.MONDAY, "monday");
	dayToNodeNameMap.put(Calendar.TUESDAY, "tuesday");
	dayToNodeNameMap.put(Calendar.WEDNESDAY, "wednesday");
	dayToNodeNameMap.put(Calendar.THURSDAY, "thursday");
	dayToNodeNameMap.put(Calendar.FRIDAY, "friday");
	dayToNodeNameMap.put(Calendar.SATURDAY, "saturday");
    }

    private static final SimpleDateFormat SDF_TIME = new SimpleDateFormat("hh:mm a");
    private static final SimpleDateFormat SDF_ZONE = new SimpleDateFormat("hh:mm a zzz");
    private static final String START_TIME_PROPERTY = "startTime";
    private static final String END_TIME_PROPERTY = "endTime";
    private static final String EXCLUDED_DATES_PROPERTY = "excludedDates";
    private static final String EXCLUDED_DATES_START_DATE_PROPERTY = "startDate";
    private static final String EXCLUDED_DATES_END_DATE_PROPERTY = "endDate";
    private static final String EXCLUDED_DATES_NOTES_PROPERTY = "notes";

    private Node scheduleNode;
    private String statusMessage;
    private Calendar currentDateCal;

    public ScheduledComponent(Node node) {
	this.scheduleNode = node;
	this.currentDateCal = Calendar.getInstance();
    }

    public ScheduledComponent(Node node, boolean createEmptySchedule) {
	this.scheduleNode = node;
	this.currentDateCal = Calendar.getInstance();
	if (createEmptySchedule)
	    createEmptySchedule();
	if (log.isTraceEnabled())
	    log.trace("Creating a ScheduledComponent.");
    }

    public ScheduledComponent(Node node, boolean createEmptySchedule,
	    TimeZone timeZone) {
	this.scheduleNode = node;
	this.currentDateCal = Calendar.getInstance(timeZone);
	SDF_ZONE.setTimeZone(timeZone);
	if (createEmptySchedule)
	    createEmptySchedule();
	if (log.isTraceEnabled())
	    log.trace("Creating a ScheduledComponent.");
    }

    public String getStatusMessage() {
	return this.statusMessage;
    }

    public String getCurrentTime() {
	return SDF_ZONE.format(currentDateCal.getTime());
    }

    public void initialize() {
	isExcluded();
	isWithinWeeklySchedule();
    }

    public boolean isShowing() {
	return !isExcluded() && isWithinWeeklySchedule();
    }

    protected boolean isExcluded() {
	try {
	    if (scheduleNode.hasNode(EXCLUDED_DATES_PROPERTY)) {
		final Node excludedDatesNode = scheduleNode
			.getNode(EXCLUDED_DATES_PROPERTY);
		final NodeIterator excludedDatesIter = excludedDatesNode
			.getNodes();
		while (excludedDatesIter.hasNext()) {
		    Node currentNode = excludedDatesIter.nextNode();
		    final Calendar exclStart = currentNode.getProperty(
			    EXCLUDED_DATES_START_DATE_PROPERTY).getDate();
		    final Calendar exclEnd = currentNode.getProperty(
			    EXCLUDED_DATES_END_DATE_PROPERTY).getDate();
		    if (CalendarUtil.compareUpToMinutes(exclStart,
			    currentDateCal) < 0
			    && CalendarUtil.compareUpToMinutes(exclEnd,
				    currentDateCal) > 0) {
			this.statusMessage = "Hidden";
			if (currentNode
				.hasProperty(EXCLUDED_DATES_NOTES_PROPERTY)) {
			    this.statusMessage += " - "
				    + currentNode.getProperty(
					    EXCLUDED_DATES_NOTES_PROPERTY)
					    .getString();
			}
			return true;
		    }
		}
	    }
	} catch (RepositoryException e) {
	    e.printStackTrace();
	}
	return false;
    }

    /**
     * Determines whether or not the current time falls within the schedule.
     * 
     * TODO: the methods here for converting from Date to Calendar are really
     * heavy. try to find a better implmentation.
     * 
     * @return
     */
    protected boolean isWithinWeeklySchedule() {
	String startDateString = "";
	String endDateString = "";
	try {
	    final String dayName = dayToNodeNameMap.get(currentDateCal
		    .get(Calendar.DAY_OF_WEEK));
	    final Node dailyScheduleNode = scheduleNode.getNode(dayName);
	    if (dailyScheduleNode != null
		    && dailyScheduleNode.hasProperty(START_TIME_PROPERTY)
		    && dailyScheduleNode.hasProperty(END_TIME_PROPERTY)) {

		Calendar startDateCal = (Calendar) currentDateCal.clone();
		Calendar endDateCal = (Calendar) currentDateCal.clone();

		startDateString = dailyScheduleNode.getProperty(START_TIME_PROPERTY).getString();
		final Date startDate = SDF_TIME.parse(startDateString);
		startDateCal.set(Calendar.MINUTE, startDate.getMinutes());
		startDateCal.set(Calendar.HOUR_OF_DAY, startDate.getHours());

		endDateString = dailyScheduleNode.getProperty(END_TIME_PROPERTY).getString();
		final Date endDate = SDF_TIME.parse(endDateString);
		endDateCal.set(Calendar.MINUTE, endDate.getMinutes());
		endDateCal.set(Calendar.HOUR_OF_DAY, endDate.getHours());
		
		if (CalendarUtil.compareUpToMinutes(startDateCal,currentDateCal) < 0
			&& CalendarUtil.compareUpToMinutes(
				endDateCal, currentDateCal) > 0) {
		    this.statusMessage = "Visible - Today's schedule: "
			    + startDateString + " - " + endDateString + ".";
		    return true;
		}
	    } else {
		this.statusMessage = "Hidden - Configure the weekly schedule.";
		return false;
	    }

	} catch (PathNotFoundException e) {
	    log.error("PathNotFound in scheduled component.");
	    e.printStackTrace();
	} catch (RepositoryException e) {
	    e.printStackTrace();
	} catch (ParseException e) {
	    log.error("Malformed date in scheduled component.");
	    e.printStackTrace();
	}
	this.statusMessage = "Hidden - Today's schedule: " + startDateString
		+ " - " + endDateString + ".";
	return false;
    }

    protected void createEmptySchedule() {
	Session session;
	try {
	    session = scheduleNode.getSession();
	    for (final String createNodeName : dayToNodeNameMap.values()) {
		if (!scheduleNode.hasNode(createNodeName)) {
		    scheduleNode.addNode(createNodeName);
		}
	    }
	    if (session.hasPendingChanges())
		session.save();
	} catch (RepositoryException e) {
	    log.error("Scheduled component could not create empty schedule.");
	    e.printStackTrace();
	}
    }

}
