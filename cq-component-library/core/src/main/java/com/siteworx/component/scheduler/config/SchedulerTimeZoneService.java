package com.siteworx.component.scheduler.config;

import java.util.Dictionary;
import java.util.TimeZone;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate=true, metatype=true, label="Scheduler Component Time Zone Service",
	description="Determines which time zone to use for the scheduler component in " +
			"author mode.")
@Service(value=SchedulerTimeZoneService.class)
public class SchedulerTimeZoneService {

    private static final Logger log = LoggerFactory
	    .getLogger(SchedulerTimeZoneService.class);
    
    private static final String PROPERTY_TIMEZONE = "timezoneservice.timezone";
    
    @Property(name=PROPERTY_TIMEZONE, label="Time Zone for Scheduled Component", value="GMT",
	    description="TimeZone Abbreviation String (GMT, PST, EST...)")
    private String timeZoneString;
    
    private TimeZone timeZone;
    
    /**
     * Defaults to GMT if invalid string passed in.
     */
    protected synchronized void setupTimeZone() {
	timeZone = TimeZone.getTimeZone(timeZoneString);
    }
    
    public TimeZone getTimeZone() {
	return this.timeZone;
    }
    
    @Activate
    public void activate(ComponentContext context) {
	if (log.isDebugEnabled())
	    log.debug("SchedulerTimeZoneService Activated.");

	@SuppressWarnings("rawtypes")
	Dictionary properties = context.getProperties();
	this.timeZoneString = PropertiesUtil.toString(
		properties.get(PROPERTY_TIMEZONE), "GMT");
	if (log.isTraceEnabled()) 
	    log.trace("Trying to apply TimeZone " + timeZoneString + ".");
	this.setupTimeZone();
    }

    @Modified
    protected void modified(ComponentContext context) {
	if (log.isTraceEnabled())
	    log.trace("SchedulerTimeZoneService Modified: Reactivating.");
	activate(context);
    }
    
}
