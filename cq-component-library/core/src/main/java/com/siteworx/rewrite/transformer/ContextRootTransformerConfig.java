package com.siteworx.rewrite.transformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContextRootTransformerConfig {

    private static final Logger log = LoggerFactory.getLogger(ContextRootTransformerConfig.class);

    /**
     * Pattern for reading parsing element attribute pairs in the form of
     * <code>element:attribute</code>.
     */
    private static Pattern PATTERN_KEY_DOMAIN = Pattern.compile("^\\{(.*)\\}:\\{(.*)\\}$");

    private boolean disabled;

    private int maxCacheSize;

    private Map<String, String> domainMap;

    public ContextRootTransformerConfig() {
        this.domainMap = new HashMap<String, String>();
    }

    public ContextRootTransformerConfig(boolean disabled, String[] domainArray) {
        this.disabled = disabled;
        this.domainMap = new HashMap<String, String>();
        this.setupDomainMap(domainArray);
    }

    public int getMaxCacheSize() {
        return maxCacheSize;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public Map<String, String> getDomains() {
        return domainMap;
    }

    /**
     * Parses domains using domainPattern and adds them to domainMap.
     *
     * @param domains String array from OSGi configuration.
     */
    public void setupDomainMap(String[] domains) {
        for (String unparsedDomain : domains) {
            Matcher domainMatcher = PATTERN_KEY_DOMAIN.matcher(unparsedDomain);
            if (domainMatcher.matches()) {
                String key = domainMatcher.group(1);
                String domain = domainMatcher.group(2);
                domainMap.put(key, domain);
            } else {
                log.error("Unable to parse domain entry " + unparsedDomain + ".");
                log.error("Proper format is {key}:{domain}.");
            }
        }
    }

}
