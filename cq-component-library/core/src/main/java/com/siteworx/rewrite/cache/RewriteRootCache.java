package com.siteworx.rewrite.cache;

import com.day.jcr.vault.util.PathComparator;
import com.google.common.cache.Cache;
import com.siteworx.rewrite.mixin.RewriteRoot;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.felix.scr.annotations.*;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

@Component(immediate = true, metatype = true, label = "Siteworx Rewrite Root Cache")
@Service(value = {RewriteRootCache.class, EventHandler.class})
@Properties({@Property(name = "event.topics", propertyPrivate = true, value = {SlingConstants.TOPIC_RESOURCE_CHANGED,
        SlingConstants.TOPIC_RESOURCE_ADDED, SlingConstants.TOPIC_RESOURCE_REMOVED})})
public class RewriteRootCache implements EventHandler {

    private static final Logger log = LoggerFactory.getLogger(RewriteRootCache.class);

    private static final String PROPERTY_SEARCHPATHS = "rewriterootcache.searchPaths";

    private static final String ATTR_PATH = "path";

    private static NavigableMap<String, RewriteRoot> REWRITE_ROOT_CACHE = new ConcurrentSkipListMap<String,
            RewriteRoot>(new PathComparator());

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private ContextRootTransformCache rewriteCacheService;

    private Cache<String, String> rewriteCache;

    @Property(name = PROPERTY_SEARCHPATHS, value = {"/content", "/etc"}, label = "Search Paths",
            description = "Locations where the cache will look for RewriteRoot nodes.")
    private String[] jcrSearchPaths;

    private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

    @Activate
    public void activate(ComponentContext context) {
        if (log.isDebugEnabled()) {
            log.debug("RewriteRootCache Activated: Updating Rewrite Root Cache.");
        }
        @SuppressWarnings("rawtypes")
        final Dictionary properties = context.getProperties();
        this.jcrSearchPaths = PropertiesUtil.toStringArray(properties.get(PROPERTY_SEARCHPATHS), new String[]{""});
        this.rewriteCache = rewriteCacheService.getCache();
        this.updateCache();
    }

    @Modified
    protected void modified(ComponentContext context) {
        if (log.isTraceEnabled()) {
            log.trace("RewriteRootCache Modified: Reactivating.");
        }
        activate(context);
    }

    @Override
    public void handleEvent(Event event) {
        if (log.isTraceEnabled()) {
            log.trace("Handling event in ContextRootCache.");
        }
        final String eventPath = (String) event.getProperty(ATTR_PATH);
        if (eventPath != null && eventPath.length() > 1) {
            boolean foundPath = false;
            for (int i = 0; i < jcrSearchPaths.length && !foundPath; i++) {
                if (eventPath.startsWith(jcrSearchPaths[i])) {
                    foundPath = true;
                }
            }

            if (foundPath) {
                if (log.isTraceEnabled()) {
                    log.trace("Handling an event within the search path, at path {}.", eventPath);
                }
                this.updateCacheEntry(event);
            }
        }
    }

    /**
     * Updates a single cache entry in RewriteRoot cache, based on the eventPath of the passed event.
     * <p/>
     * Note: It is expected that the event has already been validated against jcr search paths before this method is
     * called.
     * <p/>
     * Note: The rewritten url cache (original -> transformed) is totally invalidated after each time a rewrite root is
     * updated.
     *
     * @param event An event, with a path.
     */
    private void updateCacheEntry(final Event event) {
        final String modifiedPath = (String) event.getProperty(ATTR_PATH);
        final String topic = event.getTopic();

        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
            //start critical section
            final WriteLock writeLock = rwLock.writeLock();
            writeLock.lock();
            try {
                final String rootpath = Text.getRelativeParent(modifiedPath, 1);
                if (SlingConstants.TOPIC_RESOURCE_REMOVED.equals(topic)) {
                    // deletion -> just remove
                    if (modifiedPath.endsWith("jcr:content")) {
                        REWRITE_ROOT_CACHE.remove(rootpath);
                        rewriteCache.invalidateAll();

                        if (log.isDebugEnabled()) {
                            log.debug("Removed entry in RewriteRootCache at path {}, because it no longer exists.",
                                    rootpath);
                        }
                    } else {
                        if (log.isTraceEnabled()) {
                            log.debug("Didn't remove entry in RewriteRootCache at path {}, because it does not end in jcr:root.",
                                    rootpath);
                        }
                    }
                } else if (SlingConstants.TOPIC_RESOURCE_ADDED.equals(topic) || SlingConstants.TOPIC_RESOURCE_CHANGED
                        .equals(topic)) {
                    // addition or modification -> add new, replace existing, remove if no longer has mixin
                    try {
                        final Resource resource = resourceResolver.getResource(modifiedPath);
                        final RewriteRoot rewrite = new RewriteRoot(resource);
                        REWRITE_ROOT_CACHE.put(rewrite.getJcrpath(), rewrite);
                        if (log.isDebugEnabled()) {
                            log.debug("Inserted entry in RewriteRootCache at path {} -> {}.", rewrite.getJcrpath(),
                                    rewrite.toString());
                        }
                    } catch (RepositoryException e) {
                        if (modifiedPath.endsWith("jcr:content")) {
                            REWRITE_ROOT_CACHE.remove(rootpath);
                            if (log.isDebugEnabled()) {
                                log.debug("Removed entry in RewriteRootCache at path {}, " +
                                        "because it is no longer a {}.", rootpath, RewriteRoot.MIX_REWRITE_ROOT);
                            }
                        } else {
                            if (log.isDebugEnabled()) {
                                log.debug("Not removing entry at path {}, because this node is not named " +
                                        "'jcr:content'.", rootpath);
                            }
                        }
                    } finally {
                        rewriteCache.invalidateAll();
                    }
                } else {
                    //this should never happen, should only intercept events with above types
                    log.error("An error occurred: event type did not match specified handleable types.");
                }
            } finally {
                writeLock.unlock();
                if (resourceResolver != null) {
                    resourceResolver.close();
                }
            }
            //end critical section
        } catch (LoginException e) {
            e.printStackTrace();
        }
        if (log.isDebugEnabled()) {
            log.debug("ContextRootCache Updated: Length is " + REWRITE_ROOT_CACHE.size() + ".");
        }
    }

    /**
     * Refreshes the entire RewriteRoot cache.
     */
    private void updateCache() {
        if (log.isDebugEnabled()) {
            log.debug("Refreshing RewriteRoot cache; jcr search paths are " + ReflectionToStringBuilder.toString
                    (jcrSearchPaths));
        }

        //start critical section
        final WriteLock writeLock = rwLock.writeLock();
        writeLock.lock();
        try {
            ResourceResolver resourceResolver = null;
            try {
                resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                final List<RewriteRoot> rewrites = RewriteRoot.getAll(resourceResolver, jcrSearchPaths);
                REWRITE_ROOT_CACHE.clear();
                for (RewriteRoot rw : rewrites) {
                    REWRITE_ROOT_CACHE.put(rw.getJcrpath(), rw);
                }
                rewriteCache.invalidateAll();
                // moved this here from below. Only invalidate the rewriteCache if the
                // above is successful
            } catch (RepositoryException e) {
                log.error("Unable to update the ContextRootCache.", e);
            } catch (LoginException e) {
                log.error("Unexpected LoginException", e);
            } finally {
                if (resourceResolver != null) {
                    resourceResolver.close();
                }
            }
        } finally {
            writeLock.unlock();
        }
        //end critical section

        if (log.isDebugEnabled()) {//moved this below for performance
            log.debug("ContextRootCache Updated: Length is " + REWRITE_ROOT_CACHE.size() + ".");
        }
    }

    /**
     * Returns the closest match to the argument that is found in the cache.
     *
     * @param toMatch The string (path) to which a match should be made
     *
     * @return The closest matching RewriteRoot in the cache, or null if none are found.
     */
    public RewriteRoot getClosestMatch(String toMatch) {
        final ReadLock readLock = rwLock.readLock();
        readLock.lock();
        //start critical section
        try {
            final Iterator<String> iter = REWRITE_ROOT_CACHE.descendingKeySet().iterator();
            for (final String key : REWRITE_ROOT_CACHE.descendingKeySet()) {
                if (toMatch.startsWith(key)) {
                    return REWRITE_ROOT_CACHE.get(key);
                }
            }
            return null;
        } finally {
            readLock.unlock();
        }
        //end critical section
    }

}
