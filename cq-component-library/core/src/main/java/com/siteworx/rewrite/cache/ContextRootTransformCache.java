package com.siteworx.rewrite.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;

@Component(immediate = true, metatype = true, label = "Siteworx Context Root Transform Cache",
        description = "A cache for previously rewritten URLs.")
@Service(ContextRootTransformCache.class)
public class ContextRootTransformCache {

    private static final Logger log = LoggerFactory.getLogger(ContextRootTransformCache.class);

    private static final String PROPERTY_CACHE_MAXSIZE = "contextroottransformer.cache.maxsize";

    private static Cache<String, String> rewriteCache;

    @Property(name = PROPERTY_CACHE_MAXSIZE, label = "Maximum Cache Size", value = "300",
            description = "Defaults to 300.")
    private int maxCacheSize = 300;

    public ContextRootTransformCache() {
        this.setupCache();
    }

    protected void setupCache() {
        if (log.isTraceEnabled()) log.trace("Setting up ContextRootTransformCache with size " + maxCacheSize + ".");
        rewriteCache = CacheBuilder.newBuilder().maximumSize(maxCacheSize).build();
    }

    public Cache<String, String> getCache() {
        return rewriteCache;
    }

    @Activate
    public void activate(ComponentContext context) {
        if (log.isTraceEnabled()) log.trace("ContextRootTransformCache Activated.");

        @SuppressWarnings("rawtypes") Dictionary properties = context.getProperties();
        this.maxCacheSize = PropertiesUtil.toInteger(properties.get(PROPERTY_CACHE_MAXSIZE), 300);
        this.setupCache();
    }

    @Modified
    public void modified(ComponentContext context) {
        if (log.isTraceEnabled()) log.trace("ContextRootTransformCache Modified: Reactivating.");
        activate(context);
    }

}
