package com.siteworx.rewrite.transformer;

import com.google.common.cache.Cache;
import com.siteworx.rewrite.cache.ContextRootTransformCache;
import com.siteworx.rewrite.cache.RewriteRootCache;
import com.siteworx.rewrite.mixin.RewriteRoot;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.util.Dictionary;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component(metatype = true, immediate = true, label = "Siteworx Context Root Transformer Service",
        description = "Transformer service for rewriting URLs based on RewriteRoot node configuration.")
@Service(value = ContextRootTransformer.class)
public class ContextRootTransformer {

    private static final Logger log = LoggerFactory.getLogger(ContextRootTransformer.class);

    private static final String PROPERTY_DISABLED = "contextroottransformer.disabled";

    private static final String PROPERTY_DOMAINS = "contextroottransformer.domains";

    @Property(name = PROPERTY_DOMAINS, value = {"", ""}, description = "List of urls that can be referenced by {key} " +
            "in domain attributes of " + "RewriteRoot mixin nodes. Add each entry in the form {urlName}:{url}.",
            label = "Domains")
    static final String[] urlsArray = {""};

    /**
     * Pattern for reading parsing predefined domains in the form of
     * <code>{key}</code>.
     */
    private static Pattern domainPattern = Pattern.compile("^\\{(.*)\\}$");

    @Property(name = PROPERTY_DISABLED, boolValue = true, label = "Disable Rewriting",
            description = "Disable all rewriting by the transformer.")
    private boolean disabled;

    /**
     * Holds configuration variables for this transformer.
     */
    private ContextRootTransformerConfig config;

    /**
     * Holds the RewriteRoot nodes from the JCR repository;
     */
    @Reference
    private RewriteRootCache rewriteRootCache;

    @Reference
    private ContextRootTransformCache rewriteCacheService;

    private Cache<String, String> rewriteCache;

    public ContextRootTransformer() {
    }

    public ContextRootTransformerConfig getConfig() {
        return this.config;
    }

    /**
     * Returns a transformed url based on the rewrite root cache. Returns null if no rewrite node is found.
     *
     * @param originalValue
     *
     * @return
     *
     * @throws RepositoryException
     */
    public String transform(final String originalValue) throws RepositoryException {
        if (!config.isDisabled()) {
            return transformIgnoreDisabled(originalValue);
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Transform called while transformer is disabled. Returning null.");
            }
            return null;
        }
    }

    public String transformIgnoreDisabled(final String originalValue) throws RepositoryException {
        String transformed = rewriteCache.getIfPresent(originalValue);
        if (transformed != null) {
            if (log.isTraceEnabled()) {
                log.trace("Transformed " + originalValue + " to " + transformed + " from transform cache.");
            }
            return transformed;
        } else {
            // transform it
            RewriteRoot rewriteRoot = rewriteRootCache.getClosestMatch(originalValue);
            if (rewriteRoot != null) {
                String rewrite = rewriteRoot.getContextRoot();
                if (rewriteRoot.isDomainRewrite()) {
                    rewrite = getRewriteDomain(rewriteRoot.getDomain()) + rewrite;
                }

                String transformedValue = originalValue.replace(rewriteRoot.getJcrpath(), rewrite);

                if (transformedValue.equals(".html") || "".equals(transformedValue)) transformedValue = "/";
                // this is the only rewriteCache write, ever.
                rewriteCache.put(originalValue, transformedValue);
                return transformedValue;
            } else {
                return null;
            }
        }
    }

    /**
     * @param domain
     *
     * @return The string for the domain portion of a rewrite. Empty string if
     * not a domain rewrite.
     *
     * @throws RepositoryException
     */
    public String getRewriteDomain(String domain) throws RepositoryException {
        Matcher predefinedMatcher = domainPattern.matcher(domain);
        if (predefinedMatcher.matches()) {
            String domainKey = predefinedMatcher.group(1);
            Map<String, String> definedUrls = config.getDomains();
            if (definedUrls.containsKey(domainKey)) {
                return definedUrls.get(domainKey);
            } else {
                throw new RepositoryException("Key " + domainKey + " not found in Domain Cache.");
            }
        } else {
            return domain;
        }
    }

    public void setupRewriteCache() {
        this.rewriteCache = rewriteCacheService.getCache();
    }

    @Activate
    public void activate(ComponentContext context) {
        if (log.isTraceEnabled()) log.trace("ContextRootTransformer Activated.");

        @SuppressWarnings("rawtypes") Dictionary properties = context.getProperties();

        config = new ContextRootTransformerConfig(PropertiesUtil.toBoolean(properties.get(PROPERTY_DISABLED), true),
                PropertiesUtil.toStringArray(properties.get(PROPERTY_DOMAINS), new String[]{""}));
        this.setupRewriteCache();
    }

    @Modified
    public void modified(ComponentContext context) {
        if (log.isTraceEnabled()) log.trace("ContextRootTransformer Modified: Reactivating.");
        activate(context);
    }

}
