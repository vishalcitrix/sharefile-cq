package com.siteworx.rewrite.transformer.sax;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextRootSaxTransformerConfig {

    private static final Logger log = LoggerFactory
	    .getLogger(ContextRootSaxTransformerConfig.class);
    
    /**
     * Pattern for reading parsing element attribute pairs in the form of
     * <code>element:attribute</code>.
     */
    private static Pattern PATTERN_DOM_ELEMENT = Pattern.compile("^(.*):(.*)$");
    
    /**
     * The dom elements that should be checked for rewrites.
     */
    private Map<String, String> elementAttributeMap;

    public ContextRootSaxTransformerConfig() {
	this.elementAttributeMap = new HashMap<String, String>();
    }
    
    public ContextRootSaxTransformerConfig(String[] domElements) {
	this.elementAttributeMap = new HashMap<String, String>();
	this.setupElementAttributeMap(domElements);
    }
    
    protected void setupElementAttributeMap(String[] elements) {
	for (String elementAndAttribute : elements) {
	    Matcher domElementMatcher = PATTERN_DOM_ELEMENT.matcher(elementAndAttribute);
	    if (domElementMatcher.matches()) {
		String element = domElementMatcher.group(1);
		String attribute = domElementMatcher.group(2);
		elementAttributeMap.put(element, attribute);
	    } else {
		log.error("Unable to parse DOM element & attribute pair " + elementAndAttribute +".");
		log.error("Proper format is element:attribute.");
	    }
	}
    }
    
    public Map<String, String> getElements() {
	return this.elementAttributeMap;
    }

}
