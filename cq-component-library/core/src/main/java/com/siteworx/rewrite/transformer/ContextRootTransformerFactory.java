package com.siteworx.rewrite.transformer;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.rewriter.Transformer;
import org.apache.sling.rewriter.TransformerFactory;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siteworx.rewrite.cache.RewriteRootCache;
import com.siteworx.rewrite.transformer.sax.ContextRootSaxTransformer;
import com.siteworx.rewrite.transformer.sax.ContextRootSaxTransformerConfig;

@Component(metatype = true, label = "Siteworx Context Root SAX Transformer", description = "A SAX transformer that uses ContextRootTransformer to rewrite URLs.")
@Service
public class ContextRootTransformerFactory implements TransformerFactory {

    private static final Logger log = LoggerFactory
	    .getLogger(ContextRootTransformerFactory.class);

    private static final String PROPERTY_DOM_ELEMENTS = "contextroottransformer.elements";
    
    @Property(value = "context-root", propertyPrivate = true)
    static final String PIPELINE_TYPE = "pipeline.type";

    @Property(name = PROPERTY_DOM_ELEMENTS, value = {
	    "a:href", "script:src", "link:href", "img:src" }, description = "List of html elements and their attributes which are rewritten. " +
	    		"Add each entry in the form element:attribute.", label = "Elements/Attributes to Rewrite")
    static final String[] elementsArray = { "" };

    @Reference
    private RewriteRootCache rewriteRootCache;

    @Reference
    private ContextRootTransformer contextRootTransformer;

    @Reference
    ResourceResolverFactory resourceResolverFactory;
    ResourceResolver resourceResolver;
    
    private ContextRootSaxTransformerConfig saxConfig;

    @Activate
    protected void activate(ComponentContext ctx) throws LoginException {

	@SuppressWarnings("rawtypes")
	Dictionary properties = ctx.getProperties();
	saxConfig = new ContextRootSaxTransformerConfig(
		PropertiesUtil.toStringArray(properties.get(PROPERTY_DOM_ELEMENTS), new String[] {"", ""}));
	
	
	if (log.isTraceEnabled())
	    log.trace("Activating the ContextRootTransformerFactory.");
    }

    @Modified
    protected void modified(ComponentContext ctx) throws LoginException {
	if (log.isDebugEnabled())
	    log.debug("ContextRootTransformerFactory modified.");
	activate(ctx);
    }

    /**
     * Create the SAX transformer using the existing transformer.
     */
    @Override
    public Transformer createTransformer() {
	if (log.isTraceEnabled())
	    log.trace("Creating a ContextRootTransformer from ContextRootTransformerFactory.");
	return new ContextRootSaxTransformer(contextRootTransformer, saxConfig);
    }

}
