package com.siteworx.rewrite.mixin;

import com.google.common.base.Objects;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A JCR Mixin that defines properties on the root page of a website. Used for
 * rewriting URLs to contain context roots, rather than JCR paths.
 *
 * REFACTORING NOTES: Made this immutable for better semantics for caching.
 *
 * @author rboll@siteworx
 */
public class RewriteRoot {

    private static final Logger log = LoggerFactory.getLogger(RewriteRoot.class);

    public static final String MIX_REWRITE_ROOT = "mix:RewriteRoot";

    public static final String DOMAIN = "domain";

    public static final String CONTEXT_ROOT = "contextroot";

    public static final String INCLUDE_DOMAIN = "isDomainRewrite";

    public static final String MESSAGE_BAD_TYPE = "Resource is not a " + MIX_REWRITE_ROOT + ".";

    private String jcrpath;

    private String domain;

    private String contextRoot;

    private boolean isDomainRewrite;

    /**
     * Instantiates a RewriteRoot, iff the resource passed has the proper mixin.
     * @param resource A resource.
     * @throws RepositoryException if the resource passed does not have the proper mixin applied.
     */
    public RewriteRoot(Resource resource) throws RepositoryException {
        if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            if (node.isNodeType(MIX_REWRITE_ROOT)) {
                Node parentNode = node.getParent();
                if (parentNode == null) {
                    throw new RepositoryException("RewriteRoot has no parent at " + node.getPath() + ".");
                } else {
                    jcrpath = parentNode.getPath();
                    domain = node.getProperty(DOMAIN).getString();
                    contextRoot = node.getProperty(CONTEXT_ROOT).getString();
                    isDomainRewrite = node.getProperty(INCLUDE_DOMAIN).getBoolean();
                }
            } else {
                throw new RepositoryException(MESSAGE_BAD_TYPE);
            }
        } else {
            throw new RepositoryException(MESSAGE_BAD_TYPE);
        }
    }

    public String toString() {
        return Objects.toStringHelper(RewriteRoot.class)
                .add("jcrpath", jcrpath)
                .add("domain", domain)
                .add("contextRoot", contextRoot)
                .add("isDomainRewrite", isDomainRewrite)
                .toString();
    }

    public String getJcrpath() {
        return jcrpath;
    }

    public String getDomain() {
        return domain;
    }

    public String getContextRoot() {
        return contextRoot;
    }

    public boolean isDomainRewrite() {
        return isDomainRewrite;
    }

    public static List<RewriteRoot> getAll(ResourceResolver resourceResolver) throws InvalidQueryException {
        return getAll(resourceResolver, "/");
    }

    /*
        select * from [mix:RewriteRoot] where isdescendantnode(['/content'])
        note that we can't use compounded where clauses with function isdescendantnode
        https://issues.apache.org/jira/browse/JCR-3247
     */
    public static List<RewriteRoot> getAll(ResourceResolver resourceResolver,
                                           final String rootPath) throws InvalidQueryException {
        String queryString = "SELECT * FROM [" + MIX_REWRITE_ROOT + "] WHERE ISDESCENDANTNODE(['" + rootPath + "'])";

        Iterator<Resource> resourceIter = resourceResolver.findResources(queryString, Query.JCR_SQL2);
        List<RewriteRoot> domainRootList = new ArrayList<RewriteRoot>();

        while (resourceIter.hasNext()) {
            try {
                RewriteRoot dr = new RewriteRoot(resourceIter.next());
                domainRootList.add(dr);
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return domainRootList;
    }

    /*
        poor implmentation because of
        https://issues.apache.org/jira/browse/JCR-3247
     */
    public static List<RewriteRoot> getAll(ResourceResolver resourceResolver,
                                           final String... rootPaths) throws InvalidQueryException {
        List<RewriteRoot> rewrites = new ArrayList<RewriteRoot>();
        for (String jcrPath : rootPaths) {
            rewrites.addAll(RewriteRoot.getAll(resourceResolver, jcrPath));
        }
        return rewrites;
    }
}
