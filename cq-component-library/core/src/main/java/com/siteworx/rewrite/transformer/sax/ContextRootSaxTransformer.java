package com.siteworx.rewrite.transformer.sax;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.RepositoryException;

import org.apache.cocoon.xml.sax.AbstractSAXPipe;
import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.rewriter.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.siteworx.rewrite.transformer.ContextRootTransformer;

public class ContextRootSaxTransformer extends AbstractSAXPipe implements
	Transformer {
    
    private static final Pattern PATTERN_EXTERNAL_URL = Pattern.compile("^((ht|f)tp(s?)://|mailto:).*");

    private static final Logger log = LoggerFactory
	    .getLogger(ContextRootSaxTransformer.class);

    private ContextRootTransformer transformer;
    private ContextRootSaxTransformerConfig saxConfig;

    public ContextRootSaxTransformer(ContextRootTransformer transformer,
	    ContextRootSaxTransformerConfig saxConfig) {
	this.transformer = transformer;
	this.saxConfig = saxConfig;
    }

    // limitation only one attribute per element
    @Override
    public void startElement(String nsUri, String localname, String qname,
	    Attributes atts) throws SAXException {
	if (transformer.getConfig().isDisabled())
	    super.startElement(nsUri, localname, qname, atts);
	else {
	    AttributesImpl elementAttributes = new AttributesImpl(atts);
	    if (saxConfig.getElements().containsKey(localname)) {

		String attributeToTransform = saxConfig.getElements().get(
			localname);
		boolean foundAttribute = false;

		for (int i = 0; i < elementAttributes.getLength()
			&& !foundAttribute; i++) {
		    if (attributeToTransform.equalsIgnoreCase(
			    elementAttributes.getLocalName(i))) {
			foundAttribute = true;

			try {
			    Matcher externalUrlMatcher = PATTERN_EXTERNAL_URL.matcher(elementAttributes.getValue(i));
			    if (externalUrlMatcher.matches()) {
				log.warn("Not rewriting external url.");
				break;
			    }
			    String transformed = transformer.transform(elementAttributes.getValue(i));
			    if (transformed != null)
				elementAttributes.setValue(i, 
					transformer.transform(elementAttributes.getValue(i)));
			} catch (RepositoryException e) {
			    log.error("Unable to perform transformation.");
			    e.printStackTrace();
			}
		    }
		}
	    }
	    super.startElement(nsUri, localname, qname, elementAttributes);
	}
    }

    @Override
    public void dispose() {
	this.transformer = null;
	this.saxConfig = null;
	// will this actually make it null or just release the reference?
	if (log.isTraceEnabled())
	    log.trace("Disposing of ContextRootTransformer.");
    }

    @Override
    public void init(ProcessingContext pc,
	    ProcessingComponentConfiguration config) throws IOException {
	if (log.isTraceEnabled())
	    log.trace("Starting a ContextRootTransformer.");
    }

}
