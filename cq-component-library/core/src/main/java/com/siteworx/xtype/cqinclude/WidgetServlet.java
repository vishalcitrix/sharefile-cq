package com.siteworx.xtype.cqinclude;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestDispatcherOptions;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.wrappers.SlingHttpServletResponseWrapper;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, metatype = false)
@Service
@Properties({
	@org.apache.felix.scr.annotations.Property(name = "sling.servlet.resourceTypes", value = {"sling/servlet/default"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.extensions", value = {"json", "xml"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.selectors", value = {"widget"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.methods", value = "GET"),
    @org.apache.felix.scr.annotations.Property(name = "service.description", value = "Servlet to handle all incoming widget modification")
    
})

public class WidgetServlet extends SlingAllMethodsServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(WidgetServlet.class);
    
    /**
     * List of commands to do:
     * 
     * 1. Retrieve the parameters from the URL.
     * 2. Check if the resource is valid.
     * 3. Check if the resource has a node.
     * 4. Retrieve the JSON from the infinity selector
     * 5. Parse the result to a JSON format.
     * 6. Massage the JSON object depending on the parameters.
     */
    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    	final Resource resource = request.getResource();
    	final String prefixName = request.getParameter("prefixName");
    	final String allowedGroups = request.getParameter("allowedGroups");
        
    	JSONObject json = null;
    	if(resource == null) {
    		LOG.error("Resource can not be found");
    		write(response, "Resource can not be found");
    	}else {
    		final Node node = resource.adaptTo(Node.class);
    		if(node == null) {
    			LOG.error("Node can not be found");
    			write(response, "Node can not be found");
    		}else {
				try {
					final ExternalWriterHttpServletResponse externalResponse = new ExternalWriterHttpServletResponse(response);
			    	final RequestDispatcherOptions options = new RequestDispatcherOptions();
			    	options.setReplaceSelectors("infinity");
			    	final RequestDispatcher reqDispatcher = request.getRequestDispatcher(resource, options);
			    	if(reqDispatcher != null) {
			    		reqDispatcher.include(request, externalResponse);
			    		json = new JSONObject(externalResponse.toString());
			    	}else {
			    		LOG.error("Request Dispatcher not found");
		    			write(response, "Request Dispatcher not found");
			    	}
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (JSONException jsone) {
					jsone.printStackTrace();
				} catch (ServletException se) {
					se.printStackTrace();
				}
    		}
    	}
    	
    	if(json != null) {
    		try {
    			
    			//Prefix Name
		    	if(prefixName != null) {
		    		prefixName(prefixName, json);
		    	}
		    	
		    	//AllowGroups
		    	if(allowedGroups != null) {
		    		final Authorizable user = resource.getResourceResolver().adaptTo(Authorizable.class);
    				final Iterator<Group> groups = user.memberOf();
		    		final String[] allowedGroupsArray = allowedGroups.split(","); 
    				boolean allowEditing = false;

    				while(groups.hasNext() && !allowEditing) {
    					final Group group = groups.next();
    					for(int i = 0; i < allowedGroupsArray.length; i++) {
    						if(group.getID().equals(allowedGroupsArray[i])) {
    							allowEditing = true;
    							break;
    						}
    					}
    				}
    				if(allowEditing) {
    					//Do nothing: authorized
    				}else {
    					allowedGroups(json);
    				}
		    	}
		    	
		    	if(true) { //TODO: Add a if statement, to check allowGroups
		    		final Authorizable user = resource.getResourceResolver().adaptTo(Authorizable.class);
    				final Iterator<Group> groups = user.memberOf();
    				allowedGroups(groups, json);    				
		    	}
		    	
		    	
		    	write(response, json.toString(2));
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
    	}else {
    		write(response, "Please refer to com.siteworx.servlet.WidgetServlet to handle this request");
    	}
    }
    
    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    	response.setStatus(SlingHttpServletResponse.SC_NOT_IMPLEMENTED);
    }
    
    /**
     * Handles the output to the request
     */
    private void write(SlingHttpServletResponse response, String output) {
    	response.setContentType("application/json"); //TODO: XML support?
	    try {
			response.getWriter().write(output);
		} catch (IOException e) {
			
		}
    }
    
    /**
     * PrefixName: This will change all the name starting with the prefix of "./example" to "./prefixName/example" 
     * for <code>JSONObject</code> recursively. This is used to store the data in the child node. 
     * <p>Here are some examples which this can be used.</p>
     * <li>prefixName = 'image'; nodeName = './text'; result= './image/text'</li>
     * <li>prefixName = './image'; nodeName = './text'; result= '././image/text'</li>
     * <li>prefixName = 'image/video'; nodeName = './text'; result= './image/video/text'</li>
     */
    private void prefixName(String prefixName, JSONObject json) throws JSONException {
		final Iterator<String> keys = json.keys();
		while(keys.hasNext()) {
			final String key = keys.next();
			final Object obj = json.get(key);
			
			if(obj.getClass().equals(JSONArray.class)) {
				prefixName(prefixName, json.getJSONArray(key));
			}else if(obj.getClass().equals(JSONObject.class)) {
				prefixName(prefixName, json.getJSONObject(key));
			}else if(obj.getClass().equals(String.class)) {
				final String objStr = json.getString(key);
				if(objStr.startsWith("./")) {
					final StringBuilder builder = new StringBuilder();
					builder.append("./").append(prefixName).append("/").append(objStr.replaceFirst("./", ""));
					json.put(key, builder.toString());
				}
			}else if(obj.getClass().equals(Boolean.class)) { 
				//Do nothing;
			}else if(obj.getClass().equals(Integer.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Double.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Long.class)) {
				//Do nothing;
			}else {
				//Object class does not exist
			}
    	}
    }
    
    /** 
     * PrefixName: This handles the <code>JSONArray</code> recursively check if there is more <code>JSONArray</code>
     * or <code>JSONObject</code>.
     */
    private void prefixName(String prefixName, JSONArray array) throws JSONException {
    	for(int i = 0; i < array.length(); i++) {
    		final Object obj = array.get(i);
    		if(obj.getClass().equals(JSONArray.class)) {
    			prefixName(prefixName, array.getJSONArray(i));
    		}else if(obj.getClass().equals(JSONObject.class)) {
				prefixName(prefixName, array.getJSONObject(i));
			}else if(obj.getClass().equals(String.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Boolean.class)) { 
				//Do nothing;
			}else if(obj.getClass().equals(Integer.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Double.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Long.class)) {
				//Do nothing;
			}else {
				//Object class does not exist
			}
    	}
    }
    
    /** 
     * AllowedGroups: This will change all the node under the current node to disabled. for <code>JSONObject</code> 
     */
    private void allowedGroups(JSONObject json) throws JSONException {
    	json.put("disabled", true);
    }
    
    
    /**
     * AllowedGroups: This handles the <code>JSONObject</code> recursively check if there is more <code>JSONArray</code>
     * or <code>JSONObject</code>. There is a two flags to determine if there is allowedGroups in the current node. The
     * <code>permissionEnabled</code> and <code>allowEditing</code>. The allowedGroups property must be in a array format
     * to be read in this method.
     * 
     * <p><code>allowEditing</code> relies on the <code>permissionEnabled</code>, this is the list of results:</p>
     * <li>permissionEnabled = true; allowEditing = true; User is authenticated, disabled = false;</li>
     * <li>permissionEnabled = true; allowEditing = false; User is not authenticated, disabled = true;</li>
     * <li>permissionEnabled = false; allowEditing = false; There is no allowedGroups set; don't do anything.</li>
     */
    private void allowedGroups(Iterator<Group> groups, JSONObject json) throws JSONException, RepositoryException {
    	final Iterator<String> keys = json.keys();
		boolean allowEditing = false;
		boolean permissionEnabled = false;
		while(keys.hasNext()) {
			final String key = keys.next();
			final Object obj = json.get(key);
			
			if(obj.getClass().equals(JSONArray.class)) {
				if(key.equals("allowGroups")) {
					final JSONArray array = json.getJSONArray(key);
					permissionEnabled = true;
					while(groups.hasNext() && !allowEditing) {
						final Group group = groups.next();
						for (int i = 0; i < array.length(); i++) {
							if(group.getID().equals(array.getString(i))) {
								allowEditing = true;
								break;
							}
						}
					}
				}
				allowedGroups(groups, json.getJSONArray(key));
			}else if(obj.getClass().equals(JSONObject.class)) {
				allowedGroups(groups, json.getJSONObject(key));
			}else if(obj.getClass().equals(String.class)) {
				
			}else if(obj.getClass().equals(Boolean.class)) { 
				//Do nothing;
			}else if(obj.getClass().equals(Integer.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Double.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Long.class)) {
				//Do nothing;
			}else {
				//Object class does not exist
			}
    	}
		
		if(allowEditing && permissionEnabled) {
			json.put("disabled", false);
		}else if(!allowEditing && permissionEnabled){
			json.put("disabled", true);
		}else {
			//No-op
		}
    }
    
    /**
     * AllowedGroups: This handles the <code>JSONArray</code> recursively check if there is more <code>JSONArray</code>
     * or <code>JSONObject</code>.
     */
    private void allowedGroups(Iterator<Group> groups, JSONArray array) throws JSONException, RepositoryException {
    	for(int i = 0; i < array.length(); i++) {
    		final Object obj = array.get(i);
    		if(obj.getClass().equals(JSONArray.class)) {
    			allowedGroups(groups, array.getJSONArray(i));
    		}else if(obj.getClass().equals(JSONObject.class)) {
    			allowedGroups(groups, array.getJSONObject(i));
			}else if(obj.getClass().equals(String.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Boolean.class)) { 
				//Do nothing;
			}else if(obj.getClass().equals(Integer.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Double.class)) {
				//Do nothing;
			}else if(obj.getClass().equals(Long.class)) {
				//Do nothing;
			}else {
				//Object class does not exist
			}
    	}
    }
    
//    /**
//     * Adds the properties of node to the JSON object with the correct <code>PropertyType</code> 
//     * and checks if there is a child node in the current node. If there is a child node then do 
//     * an recursion of this method.
//     */
//    private JSONObject getNodeJSON(Node node) throws RepositoryException, JSONException {
//    	final JSONObject json = new JSONObject();
//    	final PropertyIterator properties = node.getProperties();
//    	while(properties.hasNext()) {
//    		final Property property = properties.nextProperty();
//    		
//    		if(property.isMultiple()) {
//    			final JSONArray multipleValues= new JSONArray();
//    			addJSONArrayProperty(multipleValues, property);
//    			json.put(property.getName(), multipleValues);
//    		}else {
//    			addJSONObjectProperty(json, property);
//    		}    	
//    	}
//    	
//    	final NodeIterator nodes = node.getNodes();
//    	while(nodes.hasNext()) {
//    		final Node childNode = nodes.nextNode();
//    		json.put(childNode.getName(), getNodeJSON(childNode));
//    	}
//    	return json;
//    }
//    
//    
//    /**
//     * This handles all the property types and retrieve the data correctly without throwing 
//     * <code>PropertyType</code> exceptions for <code>JSONObject</code>.
//     */
//    private void addJSONObjectProperty(JSONObject json, Property property) throws ValueFormatException, JSONException, RepositoryException {
//    	switch(property.getType()) {    		
//			case PropertyType.BINARY:
//				json.put(property.getName(), property.getBinary());
//				break;
//			case PropertyType.BOOLEAN:
//				json.put(property.getName(), property.getBoolean());
//				break;
//			case PropertyType.DATE:
//				json.put(property.getName(), property.getDate());
//				break;
//			case PropertyType.DECIMAL:
//				json.put(property.getName(), property.getDecimal());
//				break;
//			case PropertyType.DOUBLE:
//				json.put(property.getName(), property.getDouble());
//				break;
//			case PropertyType.LONG:
//				json.put(property.getName(), property.getLong());
//				break;
//			case PropertyType.NAME:
//				json.put(property.getName(), property.getString());
//				break;
//			case PropertyType.PATH:
//				json.put(property.getName(), property.getPath());
//				break;
//			case PropertyType.STRING:
//				json.put(property.getName(), property.getString());
//				break;
//			default:
//				json.put(property.getName(), property.getString());
//		}
//    }
//    
//    /**
//     * This handles all the property types and retrieve the data correctly without throwing 
//     * <code>PropertyType</code> exceptions for exceptions for <code>JSONArray</code>.
//     */
//    private void addJSONArrayProperty(JSONArray array, Property property) throws ValueFormatException, JSONException, RepositoryException {
//    	switch(property.getType()) {    		
//			case PropertyType.BINARY:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getBinary());
//    			}
//				break;
//			case PropertyType.BOOLEAN:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getBoolean());
//    			}
//				break;
//			case PropertyType.DATE:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getDate());
//    			}
//				break;
//			case PropertyType.DECIMAL:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getDecimal());
//    			}
//				break;
//			case PropertyType.DOUBLE:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getDouble());
//    			}
//				break;
//			case PropertyType.LONG:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getLong());
//    			}
//				break;
//			case PropertyType.NAME:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getString());
//    			}
//				break;
//			case PropertyType.PATH:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getBinary());
//    			}
//				break;
//			case PropertyType.STRING:
//				for(int i = 0; i < property.getValues().length; i++) {
//					array.put(property.getValues()[i].getString());
//    			}
//				break;
//			default:
//    			for(int i = 0; i < property.getValues().length; i++) {
//    				array.put(property.getValues()[i].getString());
//    			}
//		}
//    }
    
    
    /**
     * Encapsulate the response object and prevents the writer from printing. Retrieve the result by calling
     * <code>toString</code>. This is used for <code>RequestDispatcher.include(response, externalWriterHttpRequestResponse)</code>
     */
    public class ExternalWriterHttpServletResponse extends SlingHttpServletResponseWrapper {
    	private StringWriter writer = new StringWriter();

    	public ExternalWriterHttpServletResponse(SlingHttpServletResponse response) {
    		super(response);
    	}

    	public PrintWriter getWriter() throws IOException {
    	    return new PrintWriter(writer);
    	}
    	
		public ServletOutputStream getOutputStream() throws IOException {
		   throw new UnsupportedOperationException();
		}
		public String toString() {
			return writer.toString();
		}
    }
}