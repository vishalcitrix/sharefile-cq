package com.siteworx.xtype.mixinpanel;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * Returns the mixins applied to the current node.
 * 
 * @author rboll@siteworx
 * 
 */
@Component(immediate = true, metatype = false, enabled = true)
@Service
@Properties({
		@Property(name = "sling.servlet.resourceTypes", value = { "sling/servlet/default" }),
		@Property(name = "sling.servlet.extensions", value = { "json" }),
		@Property(name = "sling.servlet.selectors", value = { "mixins" }),
		@Property(name = "sling.servlet.methods", value = "GET"),
		@Property(name = "service.description", value = "Handles retreiving the mixins of a page that have a dialog set in the path provided by MixinDialogOptions.") })
public class MixinServlet extends SlingAllMethodsServlet {

	private static final long	serialVersionUID	= 1L;
	private static final Logger	log					= LoggerFactory.getLogger(MixinServlet.class);

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
		final Resource resource = request.getResource();
		JSONArray responseMixins = new JSONArray();

		if (resource != null) {
			try {
				final Node resourceNode = resource.adaptTo(Node.class);
				final Node contentNode = resourceNode.getNode("jcr:content");
				log.error("contentNode path is " + contentNode.getPath());
				final NodeType[] mixins = contentNode.getMixinNodeTypes();
				if (log.isTraceEnabled())
					log.trace("Returning " + mixins.length + " mixins from MixinServlet.");
				
				for (NodeType nt : mixins) {
					responseMixins.put(nt.getName());
				}
				
				JSONObject json = new JSONObject();
				json.put("mixins", responseMixins);
				response.getWriter().write(json.toString());
				
			} catch (RepositoryException re) {
				re.printStackTrace();
				response.sendError(500);
			} catch (JSONException e) {
				e.printStackTrace();
				response.sendError(500);
			}

		} else {
			log.error("MixinServlet called on null resource.");
			response.sendError(404);
		}
	}
}