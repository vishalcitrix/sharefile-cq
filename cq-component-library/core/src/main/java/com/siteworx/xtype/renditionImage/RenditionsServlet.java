package com.siteworx.xtype.renditionImage;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, metatype = false)
@Service
@Properties({
	@org.apache.felix.scr.annotations.Property(name = "sling.servlet.resourceTypes", value = {"sling/servlet/default"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.extensions", value = {"json", "xml"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.selectors", value = {"renditions"}),
    @org.apache.felix.scr.annotations.Property(name = "sling.servlet.methods", value = "GET"),
    @org.apache.felix.scr.annotations.Property(name = "service.description", value = "Servlet to handle all incoming rendition modification")
    
})

public class RenditionsServlet extends SlingAllMethodsServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(RenditionsServlet.class);

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    	final Resource resource = request.getResource();
    	getRendition(resource, response);
    }
    
    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    	response.setStatus(SlingHttpServletResponse.SC_NOT_IMPLEMENTED);
    }
    
    private void write(SlingHttpServletResponse response, String output) {
    	response.setContentType("application/json"); //TODO: XML support?
	    try {
			response.getWriter().write(output);
		} catch (IOException e) {
			
		}
    }
    
    /**
     * This will get all the renditions under the image node.
     */
    private void getRendition(Resource resource, SlingHttpServletResponse response) {
    	if(resource == null) {
    		LOG.error("Resource can not be found");
    		write(response, "Resource can not be found");
    	}else {
    		final Node node = resource.adaptTo(Node.class);
    		if(node == null) {
    			LOG.error("Node can not be found");
    			write(response, "Node can not be found");
    		}else {
    			try {
    				NodeIterator renditions = node.getNode("jcr:content").getNode("renditions").getNodes();
    				JSONArray array = getRenditionJSON(renditions);
					write(response, array.toString(2));
				} catch (RepositoryException re) {
					LOG.error("Renditions node can not be found");
					write(response, "Renditions node can not be found");
					re.printStackTrace();
				} catch (JSONException je) {
					je.printStackTrace();
				}
    		}
    	}
    }
    
    /**
     * Adds the properties of node to the JSON object with the correct <code>option</code> parameters.
     */
    private JSONArray getRenditionJSON(NodeIterator renditions) throws RepositoryException, JSONException {
    	final JSONArray array = new JSONArray();
    	while(renditions.hasNext()) {
    		final Node rendition = renditions.nextNode();
    		final JSONObject json = new JSONObject();
    		json.put("text", rendition.getName());
    		json.put("value", rendition.getPath());
    		array.put(json);
    	}
    	return array;
    }
    
}