package com.siteworx.tags;

import java.util.Set;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.jsp.util.TagUtil;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;

public class DefineObjectsTag extends com.day.cq.wcm.tags.DefineObjectsTag {
	private static final long serialVersionUID = 7555929762235436435L;
	protected static Logger log = LoggerFactory.getLogger(DefineObjectsTag.class);

	private static final String AUTHOR_INSTANCE = "author";
	private static final String PUBLISH_INSTANCE = "publish";
	
	public int doEndTag() {
		int retVal = super.doEndTag();
		
		SlingHttpServletRequest slingRequest = TagUtil.getRequest(pageContext);
		
		if (pageContext.getAttribute("ObjectsDefined") == null) {
			pageContext.setAttribute("WCMMode", WCMMode.fromRequest(slingRequest));
			pageContext.setAttribute("isDesignMode", WCMMode.fromRequest(slingRequest) == WCMMode.DESIGN);
			pageContext.setAttribute("isDisabledMode", WCMMode.fromRequest(slingRequest) == WCMMode.DISABLED);
			pageContext.setAttribute("isEditMode", WCMMode.fromRequest(slingRequest) == WCMMode.EDIT);
			pageContext.setAttribute("isPreviewMode", WCMMode.fromRequest(slingRequest) == WCMMode.PREVIEW);
			pageContext.setAttribute("isReadOnlyMode", WCMMode.fromRequest(slingRequest) == WCMMode.READ_ONLY);
			SlingBindings bindings = (SlingBindings)pageContext.getRequest().getAttribute(SlingBindings.class.getName());
			SlingScriptHelper sling = bindings.getSling();
			SlingSettingsService settings = sling.getService(SlingSettingsService.class);
			Set<String> currentRunModes = settings.getRunModes();

			Boolean isAuthorInstance = new Boolean(false);
			Boolean isPublishInstance = new Boolean(false);
			
			if (currentRunModes.contains(AUTHOR_INSTANCE)) {
				isAuthorInstance = new Boolean(true);
			}
			if (currentRunModes.contains(PUBLISH_INSTANCE)) {
				isPublishInstance = new Boolean(true);
			}

			slingRequest.setAttribute("isAuthorInstance", isAuthorInstance.toString());
			slingRequest.setAttribute("isPublishInstance", isPublishInstance.toString());
			pageContext.setAttribute("ObjectsDefined", "true");
			
		}
		return retVal;
    }
	
	public static void main (String[] args) {
		String str = "true";
		Object obj = str;
		Boolean bool = Boolean.parseBoolean(obj.toString());
		System.out.println(bool);
	}
}